/*     */ package com.hypercom.fpe.jpos.msr;
/*     */ 
/*     */ import java.text.MessageFormat;
import java.util.EventObject;

import jpos.JposException;
import jpos.events.DataEvent;
import jpos.events.ErrorEvent;
import jpos.events.JposEvent;
import jpos.services.EventCallbacks;
import jpos.services.MSRService18;

import com.hypercom.fpe.FPETerminalException;
import com.hypercom.fpe.consts.BaseConstant;
import com.hypercom.fpe.consts.TrackDataSource;
import com.hypercom.fpe.event.EvtErrorResponse;
import com.hypercom.fpe.event.EvtHyperPassData;
import com.hypercom.fpe.event.EvtTrackData;
import com.hypercom.fpe.jpos.DeviceType;
import com.hypercom.fpe.jpos.FPEPosService;
/*     */ 
/*     */ public class MSRService extends FPEPosService
/*     */   implements MSRService18
/*     */ {
/*     */   public static final String PROP_SWAP_EXPIRATION_DATE = "swapExpirationDate";
/*     */   private String accountNumber;
/*     */   private boolean decodeData;
/*     */   private int errorReportingType;
/*     */   private String expirationDate;
/*     */   private String firstName;
/*     */   private String middleInitial;
/*     */   private boolean parseDecodeData;
/*     */   private String serviceCode;
/*     */   private String suffix;
/*     */   private String surname;
/*     */   private String title;
/*     */   private byte[] track1Data;
/*     */   private byte[] track1DiscretionaryData;
/*     */   private byte[] track2Data;
/*     */   private byte[] track2DiscretionaryData;
/*     */   private byte[] track3Data;
/*     */   private byte[] track4Data;
/*     */   private int tracksToRead;
/*     */   private String strGlobalPrompt1;
/*     */   private String strGlobalPrompt2;
/*     */   private String strFormName;
/*     */   private String trackDataSource;
/*     */   private boolean swapExpirationDate;
/*     */   private static final int MAX_EXPIRATION_DATE_YEAR_LENGTH = 2;
/*     */   private static final int MAX_EXPIRATION_DATE_MONTH_LENGTH = 2;
/*     */ 
/*     */   public MSRService()
/*     */   {
/*  30 */     this.accountNumber = "";
/*  31 */     this.expirationDate = "";
/*  32 */     this.firstName = "";
/*  33 */     this.middleInitial = "";
/*  34 */     this.serviceCode = "";
/*  35 */     this.suffix = "";
/*  36 */     this.surname = "";
/*  37 */     this.title = "";
/*  38 */     this.track1Data = new byte[0];
/*  39 */     this.track1DiscretionaryData = new byte[0];
/*  40 */     this.track2Data = new byte[0];
/*  41 */     this.track2DiscretionaryData = new byte[0];
/*  42 */     this.track3Data = new byte[0];
/*  43 */     this.track4Data = new byte[0];
/*  44 */     this.parseDecodeData = true;
/*     */   }
/*     */ 
/*     */   public boolean getCapTransmitSentinels()
/*     */     throws JposException
/*     */   {
/*  50 */     return false;
/*     */   }
/*     */ 
/*     */   public byte[] getTrack4Data()
/*     */     throws JposException
/*     */   {
/*  56 */     return this.track4Data;
/*     */   }
/*     */ 
/*     */   public boolean getTransmitSentinels()
/*     */     throws JposException
/*     */   {
/*  62 */     return false;
/*     */   }
/*     */ 
/*     */   public void setTransmitSentinels(boolean transmitSentinels)
/*     */     throws JposException
/*     */   {
/*  68 */     if (transmitSentinels)
/*  69 */       throw new JposException(106, "Transmit sentinels capability is not supported");
/*     */   }
/*     */ 
/*     */   public boolean getCapISO()
/*     */     throws JposException
/*     */   {
/*  77 */     return true;
/*     */   }
/*     */ 
/*     */   public boolean getCapJISOne()
/*     */     throws JposException
/*     */   {
/*  83 */     return false;
/*     */   }
/*     */ 
/*     */   public boolean getCapJISTwo()
/*     */     throws JposException
/*     */   {
/*  89 */     return false;
/*     */   }
/*     */ 
/*     */   public String getAccountNumber()
/*     */     throws JposException
/*     */   {
/*  95 */     return this.accountNumber;
/*     */   }
/*     */ 
/*     */   public boolean getDecodeData()
/*     */     throws JposException
/*     */   {
/* 101 */     return this.decodeData;
/*     */   }
/*     */ 
/*     */   public void setDecodeData(boolean decodeDataP)
/*     */     throws JposException
/*     */   {
/* 107 */     this.decodeData = decodeDataP;
/*     */   }
/*     */ 
/*     */   public int getErrorReportingType()
/*     */     throws JposException
/*     */   {
/* 113 */     return this.errorReportingType;
/*     */   }
/*     */ 
/*     */   public void setErrorReportingType(int errorReportingTypeP)
/*     */     throws JposException
/*     */   {
/* 119 */     if ((this.errorReportingType == 0) || (this.errorReportingType == 1))
/*     */     {
/* 121 */       this.errorReportingType = errorReportingTypeP;
/*     */     }
/*     */     else {
/* 124 */       String msg = MessageFormat.format("Invalid error reporting type: \"{0}\" .  It must be either \"{1}\" or \"{2}\"", new Object[] { 
/* 125 */         String.valueOf(this.errorReportingType), String.valueOf(0), String.valueOf(1) });
/*     */ 
/* 127 */       throw new JposException(106, msg);
/*     */     }
/*     */   }
/*     */ 
/*     */   public String getExpirationDate()
/*     */     throws JposException
/*     */   {
/* 134 */     return this.expirationDate;
/*     */   }
/*     */ 
/*     */   public String getFirstName()
/*     */     throws JposException
/*     */   {
/* 140 */     return this.firstName;
/*     */   }
/*     */ 
/*     */   public String getMiddleInitial()
/*     */     throws JposException
/*     */   {
/* 146 */     return this.middleInitial;
/*     */   }
/*     */ 
/*     */   public boolean getParseDecodeData()
/*     */   {
/* 151 */     return this.parseDecodeData;
/*     */   }
/*     */ 
/*     */   public void setParseDecodeData(boolean parseDecodeDataP)
/*     */     throws JposException
/*     */   {
/* 157 */     this.parseDecodeData = parseDecodeDataP;
/*     */   }
/*     */ 
/*     */   public String getServiceCode()
/*     */     throws JposException
/*     */   {
/* 163 */     return this.serviceCode;
/*     */   }
/*     */ 
/*     */   public String getSuffix()
/*     */     throws JposException
/*     */   {
/* 169 */     return this.suffix;
/*     */   }
/*     */ 
/*     */   public String getSurname()
/*     */     throws JposException
/*     */   {
/* 175 */     return this.surname;
/*     */   }
/*     */ 
/*     */   public String getTitle()
/*     */     throws JposException
/*     */   {
/* 181 */     return this.title;
/*     */   }
/*     */ 
/*     */   public byte[] getTrack1Data()
/*     */     throws JposException
/*     */   {
/* 187 */     return this.track1Data;
/*     */   }
/*     */ 
/*     */   public byte[] getTrack1DiscretionaryData()
/*     */     throws JposException
/*     */   {
/* 193 */     return this.track1DiscretionaryData;
/*     */   }
/*     */ 
/*     */   public byte[] getTrack2Data()
/*     */     throws JposException
/*     */   {
/* 199 */     return this.track2Data;
/*     */   }
/*     */ 
/*     */   public byte[] getTrack2DiscretionaryData()
/*     */     throws JposException
/*     */   {
/* 205 */     return this.track2DiscretionaryData;
/*     */   }
/*     */ 
/*     */   public byte[] getTrack3Data()
/*     */     throws JposException
/*     */   {
/* 211 */     return this.track3Data;
/*     */   }
/*     */ 
/*     */   public int getTracksToRead()
/*     */     throws JposException
/*     */   {
/* 217 */     return this.tracksToRead;
/*     */   }
/*     */ 
/*     */   public void setTracksToRead(int tracksToReadP)
/*     */     throws JposException
/*     */   {
/* 223 */     this.tracksToRead = tracksToReadP;
/*     */   }
/*     */ 
/*     */   public void open(String logicalName, EventCallbacks cb)
/*     */     throws JposException
/*     */   {
/* 229 */     super.open(logicalName, cb);
/* 230 */     this.strGlobalPrompt1 = ((String)getProperty("gloablPromptString1", "Please Swipe Card"));
/* 231 */     this.strGlobalPrompt2 = ((String)getProperty("gloablPromptString2", "OR Place Speed Pass here"));
/* 232 */     this.strFormName = ((String)getProperty("swipeFormName", "IDLEFRM"));
/* 233 */     this.swapExpirationDate = getBooleanProperty("swapExpirationDate", true);
/* 234 */     this.trackDataSource = TrackDataSource.TRACK_READER.getValue();
/*     */   }
/*     */ 
/*     */   protected DeviceType getDeviceType()
/*     */   {
/* 239 */     return DeviceType.MSR;
/*     */   }
/*     */ 
/*     */   public String getDeviceServiceDescription()
/*     */   {
/* 244 */     return "Hypercom MSR";
/*     */   }
/*     */ 
/*     */   protected void onDeviceEnabledChange(boolean isEnabled)
/*     */     throws JposException
/*     */   {
/*     */     try
/*     */     {
/* 252 */       this.terminal.setMSRPOSEventEnabled(isEnabled);
/*     */     }
/*     */     catch (FPETerminalException ex)
/*     */     {
/* 256 */       throw new JposException(111, "Error switching MSR events", ex);
/*     */     }
/*     */   }
/*     */ 
/*     */   protected void ondataEventEnabledChange(boolean isEnabled)
/*     */     throws JposException
/*     */   {
/*     */   }
/*     */ 
/*     */   protected boolean isEnqueueEvent(EventObject event)
/*     */   {
/* 276 */     if ((event instanceof EvtTrackData))
/* 277 */       return true;
/* 278 */     if ((event instanceof EvtHyperPassData))
/* 279 */       return true;
/* 280 */     return event instanceof EvtErrorResponse;
/*     */   }
/*     */ 
/*     */   protected JposEvent processEvent(EventObject eventObject)
/*     */   {
/* 285 */     int trackStatus = 0;
/* 286 */     this.accountNumber = "";
/* 287 */     this.expirationDate = "";
/* 288 */     this.firstName = "";
/* 289 */     this.middleInitial = "";
/* 290 */     this.serviceCode = "";
/* 291 */     this.suffix = "";
/* 292 */     this.surname = "";
/* 293 */     this.title = "";
/* 294 */     this.track1Data = new byte[0];
/* 295 */     this.track1DiscretionaryData = new byte[0];
/* 296 */     this.track2Data = new byte[0];
/* 297 */     this.track2DiscretionaryData = new byte[0];
/* 298 */     this.track3Data = new byte[0];
/* 299 */     this.track4Data = new byte[0];
/* 300 */     this.trackDataSource = TrackDataSource.TRACK_READER.getValue();
/* 301 */     if ((eventObject instanceof EvtHyperPassData))
/*     */     {
/* 303 */       EvtHyperPassData evt = (EvtHyperPassData)eventObject;
/* 304 */       this.track4Data = evt.getHyperPassData().getBytes();
/* 305 */       trackStatus |= this.track4Data.length << 32;
/*     */     }
/* 307 */     if ((eventObject instanceof EvtErrorResponse))
/*     */     {
/* 309 */       EvtErrorResponse evt = (EvtErrorResponse)eventObject;
/* 310 */       if (evt.getErrorResponseCode() == 77)
/*     */       {
/* 312 */         int errorResponse = 12;
/* 313 */         this.checkHealthText = evt.getErrorResponseText();
/* 314 */         ErrorEvent errorEvent = new ErrorEvent(this, 111, 0, 2, errorResponse);
/* 315 */         return errorEvent;
/*     */       }
/*     */     }
/* 318 */     if ((eventObject instanceof EvtTrackData))
/*     */     {
/* 320 */       EvtTrackData evt = (EvtTrackData)eventObject;
/* 321 */       if (evt.getTrackDataSource() != null)
/* 322 */         this.trackDataSource = evt.getTrackDataSource().getValue();
/* 323 */       this.track1Data = evt.getTrack1Data();
/* 324 */       this.track2Data = evt.getTrack2Data();
/* 325 */       this.track3Data = evt.getTrack3Data();
/* 326 */       trackStatus |= this.track1Data.length;
/* 327 */       trackStatus |= this.track2Data.length << 8;
/* 328 */       trackStatus |= this.track3Data.length << 16;
/* 329 */       if (getParseDecodeData())
/*     */       {
/*     */         try
/*     */         {
/* 333 */           Track1 track1 = new Track1(this.track1Data);
/* 334 */           this.accountNumber = track1.getPan();
/* 335 */           this.expirationDate = track1.getExpirationDate();
/* 336 */           this.firstName = track1.getFirstName();
/* 337 */           this.middleInitial = track1.getMiddleInitial();
/* 338 */           this.surname = track1.getSurName();
/* 339 */           this.title = track1.getTitle();
/* 340 */           this.serviceCode = track1.getServiceCode();
/* 341 */           this.track1DiscretionaryData = track1.getDiscretionaryData();
/*     */         }
/*     */         catch (Exception localException) {
/*     */         }
/*     */         try {
/* 346 */           Track2 track2 = new Track2(this.track2Data);
/* 347 */           this.accountNumber = track2.getPan();
/* 348 */           this.expirationDate = track2.getExpirationDate();
/* 349 */           this.serviceCode = String.valueOf(track2.getServiceCode());
/* 350 */           this.track2DiscretionaryData = track2.getDiscretionaryData();
/*     */         } catch (Exception localException1) {
/*     */         }
/* 353 */         if ((!this.swapExpirationDate) && (this.expirationDate != null))
/*     */         {
/* 355 */           StringBuffer bufferYear = new StringBuffer(2);
/* 356 */           bufferYear.append(this.expirationDate.substring(0, 2));
/* 357 */           StringBuffer bufferMonth = new StringBuffer(2);
/* 358 */           bufferMonth.append(this.expirationDate.substring(2));
/* 359 */           this.expirationDate = (bufferMonth.toString() + bufferYear.toString());
/*     */         }
/*     */       }
/*     */     }
/* 363 */     DataEvent dataEvent = new DataEvent(this, trackStatus);
/* 364 */     return dataEvent;
/*     */   }
/*     */ 
/*     */   public void directIO(int command, int[] data, Object object)
/*     */     throws JposException
/*     */   {
/* 370 */     if (command != 0)
/* 371 */       this.isDirectIOEventExpected = true;
/*     */     else
/* 373 */       this.isDirectIOEventExpected = false;
/* 374 */     StringBuffer strCommand = new StringBuffer();
/* 375 */     for (int i = 0; i < data.length; i++) {
/* 376 */       strCommand.append((char)data[i]);
/*     */     }
/* 378 */     if (strCommand.toString().equals("TS"))
/*     */     {
/* 380 */       int tsCode = Integer.parseInt(this.trackDataSource);
/* 381 */       data[0] = tsCode;
/* 382 */       return;
/*     */     }
/*     */     try
/*     */     {
/* 386 */       this.terminal.SendData(strCommand.substring(0, strCommand.length()));
/*     */     }
/*     */     catch (FPETerminalException ex)
/*     */     {
/* 390 */       throw new JposException(111, "Error directIO ", ex);
/*     */     }
/*     */   }
/*     */
@Override
protected BaseConstant getDeviceCategorySpecificStatistic(String name) {
	return ((com.hypercom.fpe.consts.BaseConstant) (com.hypercom.stat.MSRStatistic
	        .getInstance(name)));
}
@Override
protected BaseConstant[] getManufacturerSpecificStatistics() {
	return super.manufacturerStatistics;
}
@Override
protected BaseConstant[] getUPOSStatistics() {
    return super.uposStatistics;
} }

/* Location:           C:\DEV\Xstore_4.5\workspace\hypercom\
 * Qualified Name:     com.hypercom.fpe.jpos.msr.MSRService
 * JD-Core Version:    0.6.0
 */