//$Id: DoNothingPosPrinterService.java 652 2013-04-25 15:18:42Z dtvdomain\abalane $
package dtv.hardware.service.posprinter;

import jpos.JposException;
import jpos.POSPrinterConst;
import jpos.services.POSPrinterService17;

import dtv.hardware.service.AbstractDeviceService;
import dtv.hardware.service.DtvJposEntry;

/**
 * JavaPOS POSPrinter service implementation that does nothing. See UPOS17.pdf for more information
 * on the methods of this class.<br>
 * Copyright (c) 2002-2007 MICROS Retail
 *
 * @author Doug Berkland
 * @created January 22, 2003
 * @version $Revision: 652 $
 */
public class DoNothingPosPrinterService
    extends AbstractDeviceService
    implements POSPrinterService17, POSPrinterConst {

  public DoNothingPosPrinterService(DtvJposEntry argEntry) {
    super(argEntry);
  }

  /**
   * not supported in this implementation
   *
   * @param argTimeout timeout for insertion (ms)
   * @throws JposException always -- operation not supported
   */
  @Override
  public void beginInsertion(int argTimeout)
      throws JposException {

    throw makeNotSupportedException("beginInsertion");
  }

  /**
   * not supported in this implementation
   *
   * @param argTimeout timeout for removal (ms)
   * @throws JposException always -- operation not supported
   */
  @Override
  public void beginRemoval(int argTimeout)
      throws JposException {

    throw makeNotSupportedException("beginRemoval");
  }

  /**
   * not supported in this implementation
   *
   * @param argSide side to change to
   * @throws JposException always
   */
  @Override
  public void changePrintSide(int argSide)
      throws JposException {

    throw makeNotSupportedException("changePrintSide");
  }

  /**
   * does nothing
   *
   * @throws JposException if not claimed
   */
  @Override
  public void clearOutput()
      throws JposException {

    verifyClaimed();
    /* do nothing */
  }

  /**
   * does nothing
   *
   * @param argPercentage the percentage to cut the paper
   * @throws JposException always -- operation not supported
   */
  @Override
  public void cutPaper(int argPercentage)
      throws JposException {

    //throw makeNotSupportedException("cutPaper");
  }

  /**
   * not supported in this implementation
   *
   * @throws JposException always -- operation not supported
   */
  @Override
  public void endInsertion()
      throws JposException {

    throw makeNotSupportedException("endInsertion");
  }

  /**
   * not supported in this implementation
   *
   * @throws JposException always -- operation not supported
   */
  @Override
  public void endRemoval()
      throws JposException {

    throw makeNotSupportedException("endRemoval");
  }

  /**
   * Gets the asyncMode attribute
   *
   * @return The asyncMode value
   */
  @Override
  public boolean getAsyncMode() {
    return false;
  }

  /**
   * Gets the capCharacterSet attribute
   *
   * @return The capCharacterSet value
   */
  @Override
  public int getCapCharacterSet() {
    return PTR_CCS_UNICODE;
  }

  /**
   * Gets the capConcurrentJrnRec attribute
   *
   * @return The capConcurrentJrnRec value
   */
  @Override
  public boolean getCapConcurrentJrnRec() {
    return false;
  }

  /**
   * Gets the capConcurrentJrnSlp attribute
   *
   * @return The capConcurrentJrnSlp value
   */
  @Override
  public boolean getCapConcurrentJrnSlp() {
    return false;
  }

  /**
   * Gets the capConcurrentRecSlp attribute
   *
   * @return The capConcurrentRecSlp value
   */
  @Override
  public boolean getCapConcurrentRecSlp() {
    return false;
  }

  /**
   * Gets the capCoverSensor attribute
   *
   * @return The capCoverSensor value
   */
  @Override
  public boolean getCapCoverSensor() {
    return false;
  }

  /**
   * Gets the capJrn2Color attribute
   *
   * @return The capJrn2Color value
   */
  @Override
  public boolean getCapJrn2Color() {
    return false;
  }

  /**
   * Gets the capJrnBold attribute
   *
   * @return The capJrnBold value
   */
  @Override
  public boolean getCapJrnBold() {
    return false;
  }

  /**
   * Gets the capJrnCartridgeSensor attribute
   *
   * @return The capJrnCartridgeSensor value
   */
  @Override
  public int getCapJrnCartridgeSensor() {
    return 0;
  }

  /**
   * Gets the capJrnColor attribute
   *
   * @return The capJrnColor value
   */
  @Override
  public int getCapJrnColor() {
    return PTR_COLOR_PRIMARY;
  }

  /**
   * Gets the capJrnDhigh attribute
   *
   * @return The capJrnDhigh value
   */
  @Override
  public boolean getCapJrnDhigh() {
    return false;
  }

  /**
   * Gets the capJrnDwide attribute
   *
   * @return The capJrnDwide value
   */
  @Override
  public boolean getCapJrnDwide() {
    return false;
  }

  /**
   * Gets the capJrnDwideDhigh attribute
   *
   * @return The capJrnDwideDhigh value
   */
  @Override
  public boolean getCapJrnDwideDhigh() {
    return false;
  }

  /**
   * Gets the capJrnEmptySensor attribute
   *
   * @return The capJrnEmptySensor value
   */
  @Override
  public boolean getCapJrnEmptySensor() {
    return false;
  }

  /**
   * Gets the capJrnItalic attribute
   *
   * @return The capJrnItalic value
   */
  @Override
  public boolean getCapJrnItalic() {
    return false;
  }

  /**
   * Gets the capJrnNearEndSensor attribute
   *
   * @return The capJrnNearEndSensor value
   */
  @Override
  public boolean getCapJrnNearEndSensor() {
    return false;
  }

  /**
   * Gets the capJrnPresent attribute
   *
   * @return The capJrnPresent value
   */
  @Override
  public boolean getCapJrnPresent() {
    return false;
  }

  /**
   * Gets the capJrnUnderline attribute
   *
   * @return The capJrnUnderline value
   */
  @Override
  public boolean getCapJrnUnderline() {
    return false;
  }

  /**
   * Gets the capMapCharacterSet attribute
   *
   * @return The capMapCharacterSet value
   */
  @Override
  public boolean getCapMapCharacterSet() {
    return false;
  }

  /**
   * Gets the capRec2Color attribute
   *
   * @return The capRec2Color value
   */
  @Override
  public boolean getCapRec2Color() {
    return false;
  }

  /**
   * Gets the capRecBarCode attribute
   *
   * @return The capRecBarCode value
   */
  @Override
  public boolean getCapRecBarCode() {
    return false;
  }

  /**
   * Gets the capRecBitmap attribute
   *
   * @return The capRecBitmap value
   */
  @Override
  public boolean getCapRecBitmap() {
    return false;
  }

  /**
   * Gets the capRecBold attribute
   *
   * @return The capRecBold value
   */
  @Override
  public boolean getCapRecBold() {
    return false;
  }

  /**
   * Gets the capRecCartridgeSensor attribute
   *
   * @return The capRecCartridgeSensor value
   */
  @Override
  public int getCapRecCartridgeSensor() {
    return 0;
  }

  /**
   * Gets the capRecColor attribute
   *
   * @return The capRecColor value
   */
  @Override
  public int getCapRecColor() {
    return PTR_COLOR_PRIMARY;
  }

  /**
   * Gets the capRecDhigh attribute
   *
   * @return The capRecDhigh value
   */
  @Override
  public boolean getCapRecDhigh() {
    return false;
  }

  /**
   * Gets the capRecDwide attribute
   *
   * @return The capRecDwide value
   */
  @Override
  public boolean getCapRecDwide() {
    return false;
  }

  /**
   * Gets the capRecDwideDhigh attribute
   *
   * @return The capRecDwideDhigh value
   */
  @Override
  public boolean getCapRecDwideDhigh() {
    return false;
  }

  /**
   * Gets the capRecEmptySensor attribute
   *
   * @return The capRecEmptySensor value
   */
  @Override
  public boolean getCapRecEmptySensor() {
    return false;
  }

  /**
   * Gets the capRecItalic attribute
   *
   * @return The capRecItalic value
   */
  @Override
  public boolean getCapRecItalic() {
    return false;
  }

  /**
   * Gets the capRecLeft90 attribute
   *
   * @return The capRecLeft90 value
   */
  @Override
  public boolean getCapRecLeft90() {
    return false;
  }

  /**
   * Gets the capRecMarkFeed attribute
   *
   * @return The capRecMarkFeed value
   */
  @Override
  public int getCapRecMarkFeed() {
    return 0;
  }

  /**
   * Gets the capRecNearEndSensor attribute
   *
   * @return The capRecNearEndSensor value
   */
  @Override
  public boolean getCapRecNearEndSensor() {
    return false;
  }

  /**
   * Gets the capRecPapercut attribute
   *
   * @return The capRecPapercut value
   */
  @Override
  public boolean getCapRecPapercut() {
    return true;
  }

  /**
   * Gets the capRecPresent attribute
   *
   * @return The capRecPresent value
   */
  @Override
  public boolean getCapRecPresent() {
    return true;
  }

  /**
   * Gets the capRecRight90 attribute
   *
   * @return The capRecRight90 value
   */
  @Override
  public boolean getCapRecRight90() {
    return false;
  }

  /**
   * Gets the capRecRotate180 attribute
   *
   * @return The capRecRotate180 value
   */
  @Override
  public boolean getCapRecRotate180() {
    return false;
  }

  /**
   * Gets the capRecStamp attribute
   *
   * @return The capRecStamp value
   */
  @Override
  public boolean getCapRecStamp() {
    return false;
  }

  /**
   * Gets the capRecUnderline attribute
   *
   * @return The capRecUnderline value
   */
  @Override
  public boolean getCapRecUnderline() {
    return false;
  }

  /**
   * Gets the capSlp2Color attribute
   *
   * @return The capSlp2Color value
   */
  @Override
  public boolean getCapSlp2Color() {
    return false;
  }

  /**
   * Gets the capSlpBarCode attribute
   *
   * @return The capSlpBarCode value
   */
  @Override
  public boolean getCapSlpBarCode() {
    return false;
  }

  /**
   * Gets the capSlpBitmap attribute
   *
   * @return The capSlpBitmap value
   */
  @Override
  public boolean getCapSlpBitmap() {
    return false;
  }

  /**
   * Gets the capSlpBold attribute
   *
   * @return The capSlpBold value
   */
  @Override
  public boolean getCapSlpBold() {
    return false;
  }

  /**
   * Gets the capSlpBothSidesPrint attribute
   *
   * @return The capSlpBothSidesPrint value
   */
  @Override
  public boolean getCapSlpBothSidesPrint() {
    return false;
  }

  /**
   * Gets the capSlpCartridgeSensor attribute
   *
   * @return The capSlpCartridgeSensor value
   */
  @Override
  public int getCapSlpCartridgeSensor() {
    return 0;
  }

  /**
   * Gets the capSlpColor attribute
   *
   * @return The capSlpColor value
   */
  @Override
  public int getCapSlpColor() {
    return PTR_COLOR_PRIMARY;
  }

  /**
   * Gets the capSlpDhigh attribute
   *
   * @return The capSlpDhigh value
   */
  @Override
  public boolean getCapSlpDhigh() {
    return false;
  }

  /**
   * Gets the capSlpDwide attribute
   *
   * @return The capSlpDwide value
   */
  @Override
  public boolean getCapSlpDwide() {
    return false;
  }

  /**
   * Gets the capSlpDwideDhigh attribute
   *
   * @return The capSlpDwideDhigh value
   */
  @Override
  public boolean getCapSlpDwideDhigh() {
    return false;
  }

  /**
   * Gets the capSlpEmptySensor attribute
   *
   * @return The capSlpEmptySensor value
   */
  @Override
  public boolean getCapSlpEmptySensor() {
    return false;
  }

  /**
   * Gets the capSlpFullslip attribute
   *
   * @return The capSlpFullslip value
   */
  @Override
  public boolean getCapSlpFullslip() {
    return false;
  }

  /**
   * Gets the capSlpItalic attribute
   *
   * @return The capSlpItalic value
   */
  @Override
  public boolean getCapSlpItalic() {
    return false;
  }

  /**
   * Gets the capSlpLeft90 attribute
   *
   * @return The capSlpLeft90 value
   */
  @Override
  public boolean getCapSlpLeft90() {
    return false;
  }

  /**
   * Gets the capSlpNearEndSensor attribute
   *
   * @return The capSlpNearEndSensor value
   */
  @Override
  public boolean getCapSlpNearEndSensor() {
    return false;
  }

  /**
   * Gets the capSlpPresent attribute
   *
   * @return The capSlpPresent value
   */
  @Override
  public boolean getCapSlpPresent() {
    return false;
  }

  /**
   * Gets the capSlpRight90 attribute
   *
   * @return The capSlpRight90 value
   */
  @Override
  public boolean getCapSlpRight90() {
    return false;
  }

  /**
   * Gets the capSlpRotate180 attribute
   *
   * @return The capSlpRotate180 value
   */
  @Override
  public boolean getCapSlpRotate180() {
    return false;
  }

  /**
   * Gets the capSlpUnderline attribute
   *
   * @return The capSlpUnderline value
   */
  @Override
  public boolean getCapSlpUnderline() {
    return false;
  }

  /**
   * Gets the capTransaction attribute
   *
   * @return The capTransaction value
   */
  @Override
  public boolean getCapTransaction() {
    return false;
  }

  /**
   * Gets the cartridgeNotify attribute
   *
   * @return The cartridgeNotify value
   */
  @Override
  public int getCartridgeNotify() {
    return PTR_CN_DISABLED;
  }

  /**
   * Gets the characterSet attribute
   *
   * @return The characterSet value
   * @throws JposException if not claimed
   */
  @Override
  public int getCharacterSet()
      throws JposException {

    verifyClaimed();
    return PTR_CS_UNICODE;
  }

  /**
   * Gets the characterSetList attribute
   *
   * @return The characterSetList value
   */
  @Override
  public String getCharacterSetList() {
    return "" + PTR_CS_UNICODE;
  }

  /**
   * Gets the coverOpen attribute
   *
   * @return The coverOpen value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public boolean getCoverOpen()
      throws JposException {

    verifyEnabled();
    return false;
  }

  /**
   * Gets the errorLevel attribute
   *
   * @return The errorLevel value
   */
  @Override
  public int getErrorLevel() {
    return PTR_EL_NONE;
  }

  /**
   * Gets the errorStation attribute
   *
   * @return The errorStation value
   */
  @Override
  public int getErrorStation() {
    return 0;
  }

  /**
   * Gets the errorString attribute
   *
   * @return The errorString value
   */
  @Override
  public String getErrorString() {
    return "";
  }

  /**
   * Gets the flagWhenIdle attribute
   *
   * @return The flagWhenIdle value
   */
  @Override
  public boolean getFlagWhenIdle() {
    return false;
  }

  /**
   * Gets the fontTypefaceList attribute
   *
   * @return The fontTypefaceList value
   */
  @Override
  public String getFontTypefaceList() {
    return "";
  }

  /**
   * Gets the jrnCartridgeState attribute
   *
   * @return The jrnCartridgeState value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getJrnCartridgeState()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * Gets the jrnCurrentCartridge attribute
   *
   * @return The jrnCurrentCartridge value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getJrnCurrentCartridge()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * Gets the jrnEmpty attribute
   *
   * @return The jrnEmpty value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public boolean getJrnEmpty()
      throws JposException {

    verifyEnabled();
    return false;
  }

  /**
   * Gets the jrnLetterQuality attribute
   *
   * @return The jrnLetterQuality value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public boolean getJrnLetterQuality()
      throws JposException {

    verifyEnabled();
    return false;
  }

  /**
   * Gets the jrnLineChars attribute
   *
   * @return The jrnLineChars value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getJrnLineChars()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * Gets the jrnLineCharsList attribute
   *
   * @return The jrnLineCharsList value
   */
  @Override
  public String getJrnLineCharsList() {
    return "";
  }

  /**
   * Gets the jrnLineHeight attribute
   *
   * @return The jrnLineHeight value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getJrnLineHeight()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * Gets the jrnLineSpacing attribute
   *
   * @return The jrnLineSpacing value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getJrnLineSpacing()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * Gets the jrnLineWidth attribute
   *
   * @return The jrnLineWidth value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getJrnLineWidth()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * Gets the jrnNearEnd attribute
   *
   * @return The jrnNearEnd value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public boolean getJrnNearEnd()
      throws JposException {

    verifyEnabled();
    return false;
  }

  /**
   * Gets the mapCharacterSet attribute
   *
   * @return The mapCharacterSet value
   */
  @Override
  public boolean getMapCharacterSet() {
    return false;
  }

  /**
   * Gets the mapMode attribute
   *
   * @return The mapMode value
   */
  @Override
  public int getMapMode() {
    return PTR_MM_DOTS;
  }

  /**
   * Gets the outputID attribute
   *
   * @return The outputID value
   */
  @Override
  public int getOutputID() {
    return 0;
  }

  /**
   * Gets the recBarCodeRotationList attribute
   *
   * @return The recBarCodeRotationList value
   */
  @Override
  public String getRecBarCodeRotationList() {
    return "0";
  }

  /**
   * Gets the recBitmapRotationList attribute
   *
   * @return The recBitmapRotationList value
   */
  @Override
  public String getRecBitmapRotationList() {
    return "0";
  }

  /**
   * Gets the recCartridgeState attribute
   *
   * @return The recCartridgeState value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getRecCartridgeState()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * Gets the recCurrentCartridge attribute
   *
   * @return The recCurrentCartridge value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getRecCurrentCartridge()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * Gets the recEmpty attribute
   *
   * @return The recEmpty value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public boolean getRecEmpty()
      throws JposException {

    verifyEnabled();
    return false;
  }

  /**
   * Gets the recLetterQuality attribute
   *
   * @return The recLetterQuality value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public boolean getRecLetterQuality()
      throws JposException {

    verifyEnabled();
    return false;
  }

  /**
   * Gets the recLineChars attribute
   *
   * @return The recLineChars value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getRecLineChars()
      throws JposException {

    verifyEnabled();
    return 40;
  }

  /**
   * Gets the recLineCharsList attribute
   *
   * @return The recLineCharsList value
   */
  @Override
  public String getRecLineCharsList() {
    return "40";
  }

  /**
   * Gets the recLineHeight attribute
   *
   * @return The recLineHeight value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getRecLineHeight()
      throws JposException {

    verifyEnabled();
    return 10;
  }

  /**
   * Gets the recLineSpacing attribute
   *
   * @return The recLineSpacing value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getRecLineSpacing()
      throws JposException {

    verifyEnabled();
    return 1;
  }

  /**
   * Gets the recLinesToPaperCut attribute
   *
   * @return The recLinesToPaperCut value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getRecLinesToPaperCut()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * Gets the recLineWidth attribute
   *
   * @return The recLineWidth value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getRecLineWidth()
      throws JposException {

    verifyEnabled();
    return 400;
  }

  /**
   * Gets the recNearEnd attribute
   *
   * @return The recNearEnd value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public boolean getRecNearEnd()
      throws JposException {

    verifyEnabled();
    return false;
  }

  /**
   * Gets the recSidewaysMaxChars attribute
   *
   * @return The recSidewaysMaxChars value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getRecSidewaysMaxChars()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * Gets the recSidewaysMaxLines attribute
   *
   * @return The recSidewaysMaxLines value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getRecSidewaysMaxLines()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * Gets the rotateSpecial attribute
   *
   * @return The rotateSpecial value
   */
  @Override
  public int getRotateSpecial() {
    return PTR_RP_NORMAL;
  }

  /**
   * Gets the slpBarCodeRotationList attribute
   *
   * @return The slpBarCodeRotationList value
   */
  @Override
  public String getSlpBarCodeRotationList() {
    return "";
  }

  /**
   * Gets the slpBitmapRotationList attribute
   *
   * @return The slpBitmapRotationList value
   */
  @Override
  public String getSlpBitmapRotationList() {
    return "0";
  }

  /**
   * Gets the slpCartridgeState attribute
   *
   * @return The slpCartridgeState value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getSlpCartridgeState()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * Gets the slpCurrentCartridge attribute
   *
   * @return The slpCurrentCartridge value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getSlpCurrentCartridge()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * Gets the slpEmpty attribute
   *
   * @return The slpEmpty value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public boolean getSlpEmpty()
      throws JposException {

    verifyEnabled();
    return false;
  }

  /**
   * Gets the slpLetterQuality attribute
   *
   * @return The slpLetterQuality value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public boolean getSlpLetterQuality()
      throws JposException {

    verifyEnabled();
    return false;
  }

  /**
   * Gets the slpLineChars attribute
   *
   * @return The slpLineChars value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getSlpLineChars()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * Gets the slpLineCharsList attribute
   *
   * @return The slpLineCharsList value
   */
  @Override
  public String getSlpLineCharsList() {
    return "";
  }

  /**
   * Gets the slpLineHeight attribute
   *
   * @return The slpLineHeight value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getSlpLineHeight()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * Gets the slpLinesNearEndToEnd attribute
   *
   * @return The slpLinesNearEndToEnd value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getSlpLinesNearEndToEnd()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * Gets the slpLineSpacing attribute
   *
   * @return The slpLineSpacing value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getSlpLineSpacing()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * Gets the slpLineWidth attribute
   *
   * @return The slpLineWidth value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getSlpLineWidth()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * Gets the slpMaxLines attribute
   *
   * @return The slpMaxLines value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getSlpMaxLines()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * Gets the slpNearEnd attribute
   *
   * @return The slpNearEnd value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public boolean getSlpNearEnd()
      throws JposException {

    verifyEnabled();
    return false;
  }

  /**
   * Gets the slpPrintSide attribute
   *
   * @return The slpPrintSide value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getSlpPrintSide()
      throws JposException {

    verifyEnabled();
    return PTR_PS_UNKNOWN;
  }

  /**
   * Gets the slpSidewaysMaxChars attribute
   *
   * @return The slpSidewaysMaxChars value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getSlpSidewaysMaxChars()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * Gets the slpSidewaysMaxLines attribute
   *
   * @return The slpSidewaysMaxLines value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public int getSlpSidewaysMaxLines()
      throws JposException {

    verifyEnabled();
    return 0;
  }

  /**
   * not supported in this implementation
   *
   * @param argType type of feed
   * @throws JposException always -- operation not supported
   */
  @Override
  public void markFeed(int argType)
      throws JposException {

    throw makeNotSupportedException("markFeed");
  }

  /**
   * does nothing
   *
   * @param argStation The station to print to
   * @param argData Character string to be bar coded.
   * @param argSymbology Bar code symbol type to use.
   * @param argHeight Bar code height. Expressed in the unit of measure given by
   * {@link #getMapMode()}.
   * @param argWidth Bar code width. Expressed in the unit of measure given by {@link #getMapMode()}
   * .
   * @param argAlignment Placement of the bar code.
   * @param argTextPosition Placement of the readable character string.
   * @throws JposException always -- operation not supported
   */
  @Override
  public void printBarCode(int argStation, String argData, int argSymbology, int argHeight, int argWidth,
      int argAlignment, int argTextPosition)
      throws JposException {

    throw makeNotSupportedException("printBarCode");
  }

  /**
   * logs that fact that a bitmap print was requested
   *
   * @param argStation The station to print to
   * @param argFileName File name or URL of bitmap file. Various file formats may be supported, such
   * as bmp (uncompressed format), gif or jpeg files.
   * @param argWidth Printed width of the bitmap to be performed. See values above.
   * @param argAlignment Placement of the bitmap.
   * @throws JposException always -- operation not supported
   */
  @Override
  public void printBitmap(int argStation, String argFileName, int argWidth, int argAlignment)
      throws JposException {

    throw makeNotSupportedException("printBarCode");
  }

  /**
   * not supported in this implementation
   *
   * @param argStation The station to print to
   * @param argData The character data to print
   * @throws JposException always -- operation not supported
   */
  @Override
  public void printImmediate(int argStation, String argData)
      throws JposException {

    throw makeNotSupportedException("printImmediate");
  }

  /**
   * Does nothing
   *
   * @param argStation The station to print to
   * @param argData The character data to print
   * @throws JposException if the station is not the receipt station or if not claimed and enabled
   */
  @Override
  public void printNormal(int argStation, String argData)
      throws JposException {

    if (argStation == PTR_S_RECEIPT) {
      verifyEnabled();
      /* do nothing */
    }
    else {
      throw makeNotSupportedException("printNormal for station='"
          + dtv.hardware.types.PosPrintErrorStationType.forJposEnum(argStation).toString() + "'");
    }
  }

  /**
   * not supported in this implementation
   *
   * @param argStations The stations to print to
   * @param argData1 The character data to print to the first station
   * @param argData2 The character data to print to the second station
   * @throws JposException always -- operation not supported
   */
  @Override
  public void printTwoNormal(int argStations, String argData1, String argData2)
      throws JposException {

    throw makeNotSupportedException("printTwoNormal");
  }

  /**
   * not supported in this implementation
   *
   * @param argStation The station to change
   * @param argRotation the rotation to use
   * @throws JposException always -- operation not supported
   */
  @Override
  public void rotatePrint(int argStation, int argRotation)
      throws JposException {

    throw makeNotSupportedException("rotatePrint");
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argAsyncMode The new asyncMode value
   */
  @Override
  public void setAsyncMode(boolean argAsyncMode) {
    /*do nothing by default*/
  }

  /**
   * does nothing
   *
   * @param argBitmapNumber the index to store the bitmap in
   * @param argStation staion to print to
   * @param argFileName name of bitmap
   * @param argWidth width to print bitmap
   * @param argAlignment how to align the bitmap
   */
  @Override
  public void setBitmap(int argBitmapNumber, int argStation, String argFileName, int argWidth,
      int argAlignment) {
    /* do nothing */
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argNotify The new cartridgeNotify value
   * @throws JposException if not claimed
   */
  @Override
  public void setCartridgeNotify(int argNotify)
      throws JposException {

    verifyClaimed();
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argCharacterSet The new characterSet value
   * @throws JposException if not claimed
   */
  @Override
  public void setCharacterSet(int argCharacterSet)
      throws JposException {

    verifyClaimed();
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argFlagWhenIdle The new flagWhenIdle value
   */
  @Override
  public void setFlagWhenIdle(boolean argFlagWhenIdle) {
    /*do nothing by default*/
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argCartridge The new jrnCurrentCartridge value
   * @throws JposException if not claimed
   */
  @Override
  public void setJrnCurrentCartridge(int argCartridge)
      throws JposException {

    verifyClaimed();
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argJrnLetterQuality The new jrnLetterQuality value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public void setJrnLetterQuality(boolean argJrnLetterQuality)
      throws JposException {

    verifyEnabled();
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argJrnLineChars The new jrnLineChars value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public void setJrnLineChars(int argJrnLineChars)
      throws JposException {

    verifyEnabled();
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argJrnLineHeight The new jrnLineHeight value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public void setJrnLineHeight(int argJrnLineHeight)
      throws JposException {

    verifyEnabled();
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argJrnLineSpacing The new jrnLineSpacing value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public void setJrnLineSpacing(int argJrnLineSpacing)
      throws JposException {

    verifyEnabled();
  }

  /**
   * does nothing
   *
   * @param argLocation index of location to store the logo
   * @param argData the data for the logo
   */
  @Override
  public void setLogo(int argLocation, String argData) {
    /* do nothing */
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argMapCharacterSet The new mapCharacterSet value
   */
  @Override
  public void setMapCharacterSet(boolean argMapCharacterSet) {
    /* do nothing */
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argMapMode The new mapMode value
   */
  @Override
  public void setMapMode(int argMapMode) {
    /*do nothing by default*/
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argCartridge The new recCurrentCartridge value
   * @throws JposException if not claimed
   */
  @Override
  public void setRecCurrentCartridge(int argCartridge)
      throws JposException {

    verifyClaimed();
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argRecLetterQuality The new recLetterQuality value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public void setRecLetterQuality(boolean argRecLetterQuality)
      throws JposException {

    verifyEnabled();
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argRecLineChars The new recLineChars value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public void setRecLineChars(int argRecLineChars)
      throws JposException {

    verifyEnabled();
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argRecLineHeight The new recLineHeight value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public void setRecLineHeight(int argRecLineHeight)
      throws JposException {

    verifyEnabled();
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argRecLineSpacing The new recLineSpacing value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public void setRecLineSpacing(int argRecLineSpacing)
      throws JposException {

    verifyEnabled();
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argRotateSpecial The new rotateSpecial value
   */
  @Override
  public void setRotateSpecial(int argRotateSpecial) {
    /*do nothing by default*/
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argCartridge The new slpCurrentCartridge value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public void setSlpCurrentCartridge(int argCartridge)
      throws JposException {

    verifyEnabled();
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argRecLetterQuality The new slpLetterQuality value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public void setSlpLetterQuality(boolean argRecLetterQuality)
      throws JposException {

    verifyEnabled();
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argRecLineChars The new slpLineChars value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public void setSlpLineChars(int argRecLineChars)
      throws JposException {

    verifyEnabled();
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argRecLineHeight The new slpLineHeight value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public void setSlpLineHeight(int argRecLineHeight)
      throws JposException {

    verifyEnabled();
  }

  /**
   * Does nothing (field not modifiable)
   *
   * @param argRecLineSpacing The new slpLineSpacing value
   * @throws JposException if not claimed and enabled
   */
  @Override
  public void setSlpLineSpacing(int argRecLineSpacing)
      throws JposException {

    verifyEnabled();
  }

  /**
   * not supported in this implementation
   *
   * @param argStation The station to print to
   * @param argControl begin or end a transaction
   * @throws JposException always -- operation not supported
   */
  @Override
  public void transactionPrint(int argStation, int argControl)
      throws JposException {

    throw makeNotSupportedException("transactionPrint");
  }

  /**
   * does nothing
   *
   * @param argStation The station to validate data for
   * @param argData The character data to print
   * @throws JposException if the station is not the receipt station or if not claimed and enabled
   */
  @Override
  public void validateData(int argStation, String argData)
      throws JposException {

    if (argStation != PTR_S_RECEIPT) {
      throw makeNotSupportedException("validateData for station='"
          + dtv.hardware.types.PosPrintErrorStationType.forJposEnum(argStation).toString() + "'");
    }
    else {
      verifyEnabled();
      /* does nothing */
    }
  }

  /**
   * Gets the actualHardwareDescription attribute of the DoNothingPosPrinterService object
   *
   * @return The actualHardwareDescription value
   */
  @Override
  protected String getActualHardwareDescription() {
    return "Do Nothing POS Printer";
  }

  /**
   * Gets the actualHardwareName attribute of the DoNothingPosPrinterService object
   *
   * @return The actualHardwareName value
   */
  @Override
  protected String getActualHardwareName() {
    return "Do Nothing POS Printer";
  }

  /**
   * Gets the actualServiceDescription attribute of the DoNothingPosPrinterService object
   *
   * @return The actualServiceDescription value
   */
  @Override
  protected String getActualServiceDescription() {
    return "MICROS Retail Do Nothing POSPrinter Service";
  }

  /**
   * Gets the deviceType attribute of the DoNothingPosPrinterService object
   *
   * @return The deviceType value
   */
  @Override
  protected String getDeviceType() {
    return "posprinter";
  }
}
