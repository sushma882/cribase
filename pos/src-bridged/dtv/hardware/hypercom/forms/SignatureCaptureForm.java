//$Id: SignatureCaptureForm.java 215 2012-08-06 14:49:04Z dtvdomain\aabdul $
package dtv.hardware.hypercom.forms;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2009 MICROS Retail
 * 
 * @author czhou
 * @created Jul 9, 2009
 * @version $Revision: 215 $
 */
public class SignatureCaptureForm
    extends AbstractDeviceForm {

  public SignatureCaptureForm() {
    super("FNSIGNATURE");
  }

}
