//$Id: IdleForm.java 215 2012-08-06 14:49:04Z dtvdomain\aabdul $
package dtv.hardware.hypercom.forms;

import static dtv.hardware.hypercom.HypercomCommands.CMD_CLEAR_ALL_DISPLAY_LINES;
import dtv.hardware.hypercom.HypercomCommands;
import dtv.hardware.hypercom.statelevel.IHypercomStateLevelCallback;
import dtv.hardware.statelevel.StateLevel;
import dtv.i18n.*;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2009 MICROS Retail
 * 
 * @author czhou
 * @created Jul 9, 2009
 * @version $Revision: 215 $
 */
public class IdleForm
    extends AbstractDeviceForm {

  private static final String FS = "\u001C";

  public IdleForm() {
    super("FNIDLEFRM");
  }

  /** {@inheritDoc} */
  @Override
  protected void afterInitForm(StateLevel argPreviousState) {

    IHypercomStateLevelCallback callback = getCallback();
    callback.sendCommand(this, new String(CMD_CLEAR_ALL_DISPLAY_LINES),
        Integer.valueOf(callback.getTimeout()));
    callback.sendCommand(this,
        HypercomCommands.CMD_FORM_REQUEST + FS + getFormId() + FS + HypercomCommands.CMD_GLOBAL_PROMPT_1
            + makeTranslatable("_poleIdleText").toString(OutputContextType.POLE_DISPLAY),
        Integer.valueOf(callback.getTimeout()));
  }

}
