//$Id: TransactionInfoForm.java 215 2012-08-06 14:49:04Z dtvdomain\aabdul $
package dtv.hardware.hypercom.forms;

import java.awt.BorderLayout;
import java.awt.Font;
import java.math.BigDecimal;
import java.util.*;

import javax.swing.*;
import javax.swing.text.JTextComponent;

import org.apache.log4j.Logger;

import jpos.JposConst;
import jpos.JposException;

import dtv.hardware.*;
import dtv.hardware.hypercom.HypercomCommands;
import dtv.hardware.hypercom.statelevel.HypercomStateLevel;
import dtv.hardware.hypercom.statelevel.IHypercomStateLevelCallback;
import dtv.hardware.question.*;
import dtv.hardware.sigcap.state.*;
import dtv.hardware.statelevel.StateLevel;
import dtv.hardware.types.InputType;
import dtv.i18n.*;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.hardware.IHardwareType;
import dtv.ui.UIServices;
import dtv.util.*;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2009 MICROS Retail
 * 
 * @author czhou
 * @created Jul 9, 2009
 * @version $Revision: 215 $
 */
public class TransactionInfoForm
    extends AbstractDeviceForm {

  private static final Logger logger_ = Logger.getLogger(TransactionInfoForm.class);
  private static final String FS = "\u001C";

  //  private final IFormattable creditOrDebitMessage_;
  //  private final IFormattable creditLabelText_;
  //  private final IFormattable debitLabelText_;
  private final IFormattable swipeYourCardMessage_;
  private final IFormattable badSwipeMessage_;
  private final IFormattable processingMessage_;
  private final IFormattable thanksMessage_;

  //  private final IFormattable subtotalText_;
  //  private final IFormattable taxText_;
  // private final IFormattable totalText_;

  private final LabelControl lblMessage_;
  private final ListboxControl lstItems_;

  //  private final LabelControl lblSubtotalLabel_;
  //  private final LabelControl lblTaxLabel_;
  // private final LabelControl lblTotalLabel_;

  //  private final LabelControl lblSubtotalAmount_;
  //  private final LabelControl lblTaxAmount_;
  private final LabelControl lblTotalAmount_;

  //  private final AnimationControl aniSwipe_;
  //  private final LabelControl lblQuestion_;
  //  private final ButtonControl btn1_;
  //  private final ButtonControl btn2_;

  //  public final LedControl led_;

  //  private final IFormattable processAsCredit_;
  //  private final IFormattable yes_;
  //  private final IFormattable no_;

  private ILineItemRenderer lineRenderer_;
  IItemDebug debugWindow_;

  private final IItemDebug getDebugWindow(IHypercomStateLevelCallback argCallback) {
    if (debugWindow_ == null) {
      if (Boolean.getBoolean(getClass().getName() + ".DEBUG")
          || argCallback.getProp("dtvDebugItems", Boolean.FALSE, false).booleanValue()) {
        UIServices.invoke(new Runnable() {
          @Override
          public void run() {
            debugWindow_ = new ItemDebugWindow();
          }
        }, true, true);
      }
      else {
        debugWindow_ = new DoNothingItemDebug();
      }
    }
    return debugWindow_;
  }

  public void setLineItemRenderer(ILineItemRenderer argLineItemRenderer) {
    lineRenderer_ = argLineItemRenderer;
  }

  public TransactionInfoForm() {
    super("FNSWIPEFRM");
    lineRenderer_ = new DefaultLineItemRenderer(0);
    currentDeviceTranState_ = new DeviceTranState(new DefaultLineItemRenderer(0), null);

    lstItems_ = new ListboxControl(this, 1, true, /*OnSelect*/3, /*OnUpArrow*/4, /*OnDownArrow*/5);
    lblMessage_ = new LabelControl(this, 2, false, " ");

    //    lblSubtotalLabel_ = new LabelControl(this, 3, true, "Subtotal");
    //    lblTaxLabel_ = new LabelControl(this, 8, true, "Tax");
    //lblTotalLabel_ = new LabelControl(this, 5, true, "Total");

    //    lblSubtotalAmount_ = new LabelControl(this, 6, true, "0.00");
    //    lblTaxAmount_ = new LabelControl(this, 7, true, "0.00");
    lblTotalAmount_ = new LabelControl(this, 4, true, "0.00", "total");

    //    aniSwipe_ = new AnimationControl(this, 9, false);
    //    lblQuestion_ = new LabelControl(this, 11, false);
    //    btn1_ = new ButtonControl(this, 12, false, "Credit", /*hotspot*/1);
    //    btn2_ = new ButtonControl(this, 13, false, "Debit", /*hotspot*/2);

    //    led_ = new LedControl(this);

    //    creditOrDebitMessage_ = makeTranslatable("_creditOrDebitMessage");
    //    creditLabelText_ = makeTranslatable("_creditLabelText");
    //    debitLabelText_ = makeTranslatable("_debitLabelText");
    swipeYourCardMessage_ = makeTranslatable("_swipeYourCardMessage");
    badSwipeMessage_ = makeTranslatable("_badCardSwipeMessage");
    processingMessage_ = makeTranslatable("_processingMessage");
    thanksMessage_ = makeTranslatable("_thanksMessage");
    //    subtotalText_ = makeTranslatable("_subtotalText");
    //    taxText_ = makeTranslatable("_taxText");
    //totalText_ = makeTranslatable("_totalText");

    //    processAsCredit_ = makeTranslatable("_processAsCredit");
    //    yes_ = makeTranslatable("_yes");
    //    no_ = makeTranslatable("_no");

  }

  private static interface IItemDebug {
    void visible(boolean b);

    void setText(String s);

    void setMessage(IFormattable argMsg);
  }

  static class DoNothingItemDebug
      implements IItemDebug {

    @Override
    public void visible(boolean b) {
      /*do nothing*/
    }

    @Override
    public void setText(String s) {
      /*do nothing*/
    }

    @Override
    public void setMessage(IFormattable m) {
      /*do nothing*/
    }
  }

  private class ItemDebugWindow
      extends JFrame
      implements IItemDebug {

    private static final long serialVersionUID = 1L;

    @Override
    public void visible(boolean b) {
      if (b) {
        setVisible(true);
        validate();
      }
      else {
        setVisible(false);
      }
    }

    final JTextComponent text_;
    final JLabel msg_;

    ItemDebugWindow() {
      setTitle("Item Debug Window");
      this.setSize(400, 300);
      JPanel jContentPane = new JPanel(new BorderLayout());
      this.setContentPane(jContentPane);

      text_ = new JTextArea();
      text_.setFont(Font.decode("Monospaced-PLAIN-11"));
      text_.setEditable(false);
      jContentPane.add(new JScrollPane(text_), java.awt.BorderLayout.CENTER);

      msg_ = new JLabel();
      jContentPane.add(msg_, java.awt.BorderLayout.SOUTH);
    }

    @Override
    public void setText(final String text) {
      UIServices.invoke(new Runnable() {
        @Override
        public void run() {
          text_.setText(text);
        }
      });
    }

    @Override
    public void setMessage(final IFormattable msg) {
      UIServices.invoke(new Runnable() {
        @Override
        public void run() {
          msg_.setText(msg.toString(OutputContextType.DEFAULT));
        }
      });
    }
  }

  @Override
  protected void initForm(StateLevel argPreviousState)
      throws JposException {

    lstItems_.handleRemoveAllItem();
    super.initForm(argPreviousState);
  }

  @Override
  protected void afterInitForm(StateLevel argPreviousState)
      throws JposException {

    lblTotalAmount_.setCaption(FF.getSimpleFormattable(BigDecimal.ZERO, FormatterType.MONEY));
    
    if ((argPreviousState == HypercomStateLevel.CAPTURING_PIN)
        || (argPreviousState == HypercomStateLevel.CAPTURING_SIGNATURE)) {
      refreshForm();
    }
    //    lblSubtotalLabel_.setCaption(subtotalText_);
    //    lblTaxLabel_.setCaption(taxText_);
    //lblTotalLabel_.setCaption(totalText_);
    getDebugWindow(getCallback()).visible(true);
  }

  /** {@inheritDoc} */
  @Override
  public void exiting(IHypercomStateLevelCallback argCallback, StateLevel argNext) {
    getDebugWindow(argCallback).visible(false);
  }

  @Override
  protected void afterProcessFormModifier(StateLevel argPreviousState)
      throws JposException {
    updateSwipeCardPrompt(currentDeviceTranState_);
    //    if (currentDeviceTranState_.getSwipeState() == SwipeState.NO_ITEMS) {
    //      hideMessage();
    //    }
    //    else if (currentDeviceTranState_.getSwipeState() == SwipeState.THANKS) {
    //      showThanks();
    //    }
    //    else {
    //      showSwipeCard();
    //    }
  }

  /**************************************************************************************************************
   * BRIDGED FROM BASE
   * ******************************************************************************
   * ****************************** Because of the difficulty of making the system use a customer
   * overlay of this class 'canHandle' and 'askQuestion' have been changed and the whole class
   * bridged. In the future if there is an easier way to use 'PROCESS_AS_CREDIT' using a customer
   * overlay, do that.
   */

  @Override
  public boolean canHandle(QuestionEnum argQuestion) {
    if (QuestionEnum.forName("PROCESS_AS_CREDIT").equals(argQuestion)) {
      //super.getCallback().flushWorkerQueue();
      return true;
    }
    return super.canHandle(argQuestion);
  }

  /*
  @Override
  public void askQuestion(QuestionEnum argQuestion, final Map<TypeSafeMapKey<?>, ?> argParameters) {
    if (QuestionEnum.DEBIT_OR_CREDIT.equals(argQuestion)) {
      try {
        // hide other controls in the dynamic region
        lblMessage_.setVisible(false);
        aniSwipe_.setVisible(false);

        lblQuestion_.setCaption(creditOrDebitMessage_);
        btn1_.setCaption(creditLabelText_);
        btn2_.setCaption(debitLabelText_);

        lblQuestion_.setVisible(true);
        btn1_.setVisible(true);
        btn2_.setVisible(true);
        refreshForm();
        setHandlingQuestion(argQuestion);
      }
      catch (JposException ex) {
        getCallback().logJposException(ex);
      }
    }
  }*/

  /*********************************************************************************************************
   * END OF BRIDGED FROM BASE
   * ***********************************************************************
   * *********************************
   */

  private boolean updateSwipeCardPrompt(DeviceTranState argDeviceTranState)
      throws JposException {
    SwipeState swipeState = argDeviceTranState.getSwipeState();
    if (swipeState == SwipeState.THANKS) {
      return showThanks();
    }
    else if (swipeState == SwipeState.NO_ITEMS) {
      return hideMessage();
    }
    else {
      return showSwipeCard();
    }
  }

  private boolean showSwipeCard()
      throws JposException {
    boolean changed = false;
    //    changed = lblQuestion_.setVisible(false);
    //    changed |= btn1_.setVisible(false);
    //    changed |= btn2_.setVisible(false);

    changed |= setMessage(swipeYourCardMessage_);
    /*try {
      changed |= aniSwipe_.setVisible(true);
    }
    catch (Exception ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
    }*/
    return changed;
  }

  private boolean showBadSwipe()
      throws JposException {
    boolean changed = false;
    /*changed = lblQuestion_.setVisible(false);
    changed |= btn1_.setVisible(false);
    changed |= btn2_.setVisible(false);*/

    changed |= setMessage(badSwipeMessage_);
    //changed |= aniSwipe_.setVisible(true);
    return changed;
  }

  private boolean setMessage(IFormattable msg)
      throws JposException {
    boolean changed = false;
    getDebugWindow(getCallback()).setMessage(msg);
    changed = lblMessage_.setCaption(msg);
    changed |= lblMessage_.setVisible(true);
    getCallback().sendCommand(this,
        new String(HypercomCommands.CMD_UPDATE_DISPLAY + FS + "C" + lblMessage_.getControlId() + "N" + msg.toString(OutputContextType.POLE_DISPLAY)),
        Integer.valueOf(getCallback().getTimeout()));
    return changed;
  }

  private boolean clearMessage()
      throws JposException {
    boolean changed = false;
    getDebugWindow(getCallback()).setMessage(IFormattable.EMPTY);
    changed |= lblMessage_.setCaptionAndVisibility("");
    getCallback().sendCommand(this,
        new String(HypercomCommands.CMD_UPDATE_DISPLAY + FS + "C" + lblMessage_.getControlId() + "N" + ""),
        Integer.valueOf(getCallback().getTimeout()));
    return changed;
  }

  boolean showProcessing()
      throws JposException {

    boolean changed = false;
    //    changed |= lblQuestion_.setVisible(false);
    //    changed |= btn1_.setVisible(false);
    //    btn1_.resetState();
    //    changed |= btn2_.setVisible(false);
    //    btn2_.resetState();

    changed |= setMessage(processingMessage_);
    //changed |= aniSwipe_.setVisible(false);
    return changed;
  }

  public boolean hideMessage()
      throws JposException {
    boolean changed = false;
    //    changed |= lblQuestion_.setCaptionAndVisibility("");
    //    changed |= btn1_.setVisible(false);
    //    changed |= btn2_.setVisible(false);

    changed |= clearMessage();
    //changed |= aniSwipe_.setVisible(false);
    return changed;
  }

  private boolean showThanks()
      throws JposException {
    boolean changed = false;
    //    changed |= lblQuestion_.setVisible(false);
    //    changed |= btn1_.setVisible(false);
    //    changed |= btn2_.setVisible(false);

    changed |= setMessage(thanksMessage_);
    //   changed |= aniSwipe_.setVisible(false);
    return changed;
  }

  @Override
  public void cancelQuestion() {
    try {
      setHandlingQuestion(null);
      updateSwipeCardPrompt(currentDeviceTranState_);
      refreshForm();
    }
    catch (JposException ex) {
      getCallback().logJposException(ex);
    }
  }

  /*@Override
  public boolean answerOccurred(String[] argResponseParts) {
    // CREDIT --> XEVT,2,12,1
    // DEBIT --> XEVT,2,13,1

    if (logger_.isInfoEnabled()) {
      logger_.info("ANSWERED-->" + ArrayUtils.toString(argResponseParts));
    }

    if (argResponseParts == null) {
      logger_.warn("null response parts!?!?!");
      return false;
    }
    if (argResponseParts.length != 4) {
      return false;
    }
    if (!"XEVT".equals(argResponseParts[0])) {
      return false;
    }

    final int controlId;
    try {
      controlId = Integer.parseInt(argResponseParts[2]);
    }
    catch (Exception ex) {
      logger_.debug("CAUGHT EXCEPTION", ex);
      return false;
    }
    final IAnswer answer;
    if (controlId == btn1_.getControlId()) {
      answer = Answer.makeCredit(getCallback().getHardwareType());
    }
    else if (controlId == btn2_.getControlId()) {
      answer = Answer.makeDebit(getCallback().getHardwareType());
    }
    else {
      answer = null;

    }

    if (answer != null) {
      super.getCallback().queueWorker(new Runnable() {

        public void run() {
          try {
            showProcessing();
            refreshForm();
          }
          catch (JposException ex) {
            getCallback().logJposException(ex);
          }
          setHandlingQuestion(null);
          IXstEvent event = EventDiscriminator.getInstance().translateEvent(answer);
          getCallback().getEventor().post(IDtvInputDevice.EVENT_ROUTED_EVENT, event);
        }
      });
      return true;
    }

    return false;
  }*/

  private DeviceTranState currentDeviceTranState_;

  @Override
  public void handleDisplayStateChange(DeviceTranState argState) {
    IStateChange[] changes = argState.getChanges(currentDeviceTranState_);
    try {
      handleChanges(argState, changes);
      currentDeviceTranState_ = argState;
    }
    catch (JposException ex) {
      getCallback().logJposException(ex);
    }
  }

  private void handleChanges(DeviceTranState argState, IStateChange[] changes)
      throws JposException {
    logChanges(changes);
    LinkedList<ILabelStateChange> labels = new LinkedList<ILabelStateChange>();
    LinkedList<IItemStateChange> removes = new LinkedList<IItemStateChange>();
    LinkedList<IItemStateChange> adds = new LinkedList<IItemStateChange>();
    LinkedList<IItemStateChange> updates = new LinkedList<IItemStateChange>();
    boolean updateSwipeMessage = false;

    boolean changed = false;
    for (int i = 0; i < changes.length; i++ ) {
      IStateChange change = changes[i];
      if (change instanceof ILabelStateChange) {
        if (DeviceTranState.SWIPE_STATE.equals(((ILabelStateChange) change).getLabelId())) {
          updateSwipeMessage = true;
        }
        else {
          labels.add((ILabelStateChange) change);
        }
      }
      else if (change instanceof IItemStateChange) {
        IItemStateChange ic = (IItemStateChange) change;
        switch (ic.getChangeType()) {
          case NEW:
            adds.addLast(ic);
            break;
          case DELETED:
            removes.addFirst(ic);
            break;
          case MODIFIED:
            //            adds.addLast(ic);
            //            removes.addFirst(ic);   
            updates.addLast(ic);
            break;
          default:
            break;
        }
      }
      else if (change instanceof TransactionChange) {
        updateSwipeMessage = true;
      }
      else {
        logger_.warn("unknown change type " + ObjectUtils.getClassNameFromObject(change));
      }
    }
    if (removes.size() > 0) {
      lstItems_.handleRemoveAllItem();
      lstItems_.handleAddItemsAfterRemoveAll(argState.getItems());
      removes.clear();
      changed = true;
    }
    for (IItemStateChange item : updates) {
      lstItems_.handleUpdateItem(item);
      changed = true;
    }
    for (IItemStateChange item : adds) {
      lstItems_.handleAddItem(item);
      changed = true;
    }
    for (ILabelStateChange lbl : labels) {
      changed |= handleLabelStateChange(lbl);
    }
    if (updateSwipeMessage) {
      changed |= updateSwipeCardPrompt(argState);
    }
    if (changed) {
      showChanges();
    }
  }

  private void logChanges(IStateChange[] argChanges) {
    if (logger_.isDebugEnabled()) {
      StringBuilder sb = new StringBuilder();
      sb.append("CHANGES:\n\t");
      for (IStateChange change : argChanges) {
        sb.append(change);
        sb.append("\n\t");
      }
      logger_.debug(sb);
    }
  }

  private void showChanges() {
    try {
      refreshForm();
    }
    catch (JposException ex) {
      getCallback().logJposException(ex);
    }
  }

  /** {@inheritDoc} */
  @Override
  protected void refreshForm()
      throws JposException {

    getDebugWindow(getCallback()).setText(String.valueOf(currentDeviceTranState_));
    super.refreshForm();
  }

  public void refreshPreviousState()
      throws JposException {
    if (ListboxControl.useDeviceLineCaching()) {
      IHypercomStateLevelCallback callback = getCallback();
      String[] parts =
          callback.sendCommand(this, HypercomCommands.CMD_FORM_REQUEST + "/FNSWIPEFRM",
              Integer.valueOf(callback.getTimeout()));
      try {
        validateCount(parts);
      }
      catch (JposException ex) {
        callback.logJposException(ex);
      }
    }
    else {
      IStateChange[] changes = currentDeviceTranState_.getChanges(new DeviceTranState(lineRenderer_, null));
      handleChanges(currentDeviceTranState_, changes);
    }
    updateSwipeCardPrompt(currentDeviceTranState_);
    /*if (aniSwipe_.isVisible()) {
      aniSwipe_.setRunning(true);
    }*/
  }

  private static void validateCount(String[] argResponse)
      throws JposException {
    if (argResponse == null) {
      throw new JposException(JposConst.JPOS_E_FAILURE, "no response");
    }
    if (!HypercomCommands.CMD_CLEAR_ALL_DISPLAY_LINES.equals(argResponse[0])) {
      //     HypercomFormBuilderDevice.logResponse(argResponse);
      throw new JposException(JposConst.JPOS_E_FAILURE, "bad response; command='"
          + HypercomCommands.CMD_CLEAR_ALL_DISPLAY_LINES + "', response command='" + argResponse[0] + "'");
    }
    if ("0".equals(argResponse[1])) {
      //     HypercomFormBuilderDevice.logResponse(argResponse);
      throw new JposException(JposConst.JPOS_E_FAILURE, "nothing restored");
    }

    try {
      int count = Integer.parseInt(argResponse[1]);
      logger_.info(count + " items restored");
    }
    catch (Throwable ex) {
      throw AbstractDtvJposDevice.toJposException(ex);
    }
  }

  private final Set<InputType> expectedMsrEvents_ = makeExpectedMsrEvents();

  private static Set<InputType> makeExpectedMsrEvents() {
    Set<InputType> s = new HashSet<InputType>();
    s.add(InputType.INPUT_VOUCHER);
    s.add(InputType.INPUT_CREDIT_CARD);
    return s;
  }

  @Override
  public void msrSwipeOccurred(IXstEvent argEvent) {
    logger_.warn("event:" + argEvent);
    if (isHandlingQuestion()) {
      return;
    }
    try {
      if (expectedMsrEvents_.contains(argEvent.getType())) {
        showProcessing();
        refreshForm();
      }
      else if (isCustomerControlledDevice(argEvent.getSource())) {
        showBadSwipe();
      }
    }
    catch (JposException ex) {
      getCallback().logJposException(ex);
    }
  }

  private boolean isCustomerControlledDevice(Object argSource) {
    if (!(argSource instanceof IDtvDevice)) {
      return false;
    }
    IHardwareType<?> type = ((IDtvDevice) argSource).getType();
    return HardwareMgr.getCurrentHardwareMgr().isCustomerControlledDevice(type);
  }

  /** {@inheritDoc} */
  @Override
  public void handleTenderLineCanceled() {
    try {
      showSwipeCard();
      refreshForm();
    }
    catch (JposException ex) {
      getCallback().logJposException(ex);
    }
  }

  private boolean handleLabelStateChange(ILabelStateChange argChange)
      throws JposException {
    String labelId = argChange.getLabelId();
    final LabelControl label;

    //    if (DeviceTranState.SUBTOTAL_FIELD.equals(labelId)) {
    //      label = lblSubtotalAmount_;
    //    }
    //    else if (DeviceTranState.TAX_FIELD.equals(labelId)) {
    //      label = lblTaxAmount_;
    //    }
    //    else 

    if (DeviceTranState.TOTAL_FIELD.equals(labelId)) {
      label = lblTotalAmount_;
    }
    else {
      logger_.warn("unknown labelId " + labelId);
      return false;
    }
    String newText = argChange.getNewText();
    return label.setCaption(newText);
  }

  /** {@inheritDoc} */
  @Override
  public void initialize(IHypercomStateLevelCallback argCallback) {
    setLineItemRenderer(argCallback.getLineItemRenderer());
  }
}
