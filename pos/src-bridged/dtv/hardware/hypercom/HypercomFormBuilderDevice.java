//$Id: HypercomFormBuilderDevice.java 434 2012-09-26 22:21:51Z dtvdomain\aabdul $
package dtv.hardware.hypercom;

import java.io.*;
import java.lang.reflect.Constructor;
import java.util.*;
import java.util.concurrent.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import jpos.*;
import jpos.config.JposEntry;
import jpos.events.DirectIOEvent;
import jpos.events.DirectIOListener;
import EDU.oswego.cs.dl.util.concurrent.Mutex;
import dtv.event.*;
import dtv.event.eventor.DefaultEventor;
import dtv.hardware.*;
import dtv.hardware.custdisplay.IDtvCustDisplayDevice;
import dtv.hardware.events.IHardwareInputEvent;
import dtv.hardware.hypercom.forms.*;
import dtv.hardware.hypercom.statelevel.HypercomStateLevel;
import dtv.hardware.hypercom.statelevel.IHypercomStateLevelCallback;
import dtv.hardware.hypercom.storefile.*;
import dtv.hardware.msr.DtvMsr;
import dtv.hardware.msr.IDtvMsr;
import dtv.hardware.ppad.*;
import dtv.hardware.question.IDtvQuestionDevice;
import dtv.hardware.question.QuestionEnum;
import dtv.hardware.service.DtvJposEntry;
import dtv.hardware.sigcap.*;
import dtv.hardware.sigcap.state.*;
import dtv.hardware.statelevel.StateLevel;
import dtv.hardware.types.HardwareFamilyType;
import dtv.hardware.types.HardwareType;
import dtv.i18n.*;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.hardware.IHardwareFamilyType;
import dtv.pos.tender.TenderHelper;
import dtv.util.*;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;
import dtv.xst.dao.ttr.ITenderLineItem;
import dtv.xst.daocommon.ILineItemFilter;
import dtv.xst.daocommon.UnfilteredLineItemFilter;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2009 MICROS Retail
 * 
 * @author czhou
 * @created Jun 25, 2009
 * @version $Revision: 434 $
 */
public class HypercomFormBuilderDevice
    extends AbstractDtvJposDevice<IHypercomStateLevelCallback>
    implements IDtvMultiuseDevice, IDtvCustDisplayDevice, IDirectIOOwner, ISigCapDisplayStateChangeListener,
    IDtvSigCap, IDtvQuestionDevice, IDtvPinPad, IDtvMsr {

  private static final HypercomFormBuilderDevice INSTANCE = new HypercomFormBuilderDevice();
  //  private final Eventor events_ = new DefaultEventor(new EventDescriptor(SIGCAP_EVENTS, this));

  static final FormattableFactory FF = FormattableFactory.getInstance();
  static final HardwareI18nHelper HH = HardwareI18nHelper.getInstance();
  public static final boolean _NO__HARDWARE = Boolean
      .getBoolean("dtv.hardware.hypercom.HypercomFormBuilderDevice._NO__HARDWARE");
  static final Logger logger_ = Logger.getLogger(HypercomFormBuilderDevice.class);
  static final boolean DEBUG_ENABLED = logger_.isDebugEnabled();
  protected jpos.LineDisplay control_;
  private DeviceState deviceState_;
  BlockingQueue<Runnable> workers_ = new LinkedBlockingQueue<Runnable>();
  private QueueProcessorThread workerQueueProcessor_;
  private int workerCallLevel_ = 0;
  private static final int MAX_WORKER_CALL_LEVEL = 20;
  public static final OutputContextType OUTPUT_CONTEXT = OutputContextType.POLE_DISPLAY;
  private Object subDirectIOListenerMutex_ = new Object();
  DirectIOListener subDirectIOListener_ = null;
  private static final int DEFAULT_TIMEOUT = 200;
  private int timeout_ = DEFAULT_TIMEOUT;
  boolean useBusyMutex_ = false;
  static final Level DEFAULT_MUTEX_LOG_LEVEL = Level.DEBUG;
  Level mutexLogLevel_ = DEFAULT_MUTEX_LOG_LEVEL;
  private Mutex busy_ = new Mutex();
  private Thread busyOwner_ = null;
  private final Collection<IHardwareFamilyType<?>> hardwareFamilyTypes_ =
      new HashSet<IHardwareFamilyType<?>>();
  IDeviceForm currentForm_ = null;
  int maxErrorCount_ = 10;
  private boolean disabledDueToError_ = false;
  private final Eventor events_ = new DefaultEventor(new EventDescriptor(SIGCAP_EVENTS, this));
  private SigCapDisplayModel displayModel_;
  DtvDukptPinPad pinpad_;
  DtvSigCap sigcap_;
  DtvMsr msr_;

  public static final String PROP_CLEAR_ON_EXIT = "dtvClearOnExit";
  public static final String PROP_XBATCH_ENABLED = "dtvUseXbatch";
  public static final String PROP_PIN_ENTRY_REQ = "dtvPinEntryFormRequired";
  public static final String PROP_LINE_ITEM_RENDERER = "dtvLineItemRenderer";
  public static final String PROP_LINE_ITEM_FILTER = "dtvLineItemFilterImpl";
  public static final String PROP_LINE_ITEM_WIDTH = "dtvLineItemWidth";
  //  private static final String PROP_CAP_XLED = "dtvCapXLED";
  //  private static final String PROP_CAP_XVER = "dtvCapXVER";
  //  private static final String PROP_CAP_BUTTON_RESET_STATE = "dtvCapBUTTON_RESET_STATE";
  private static final String PROP_MAX_ERROR_COUNT = "dtvMaxErrorCount";
  private static final String PROP_MUTEX_LOG_LEVEL = "dtvMutexLogLevel";
  private static final String PROP_USE_BUSY_MUTEX = "dtvUseBusyMutex";
  private static final String PROP_DEVICE_STATE_FILE = "dtvDeviceStateFile";
  private static final String PROP_TIMEOUT = "dtvTimeout";
  private static final String PROP_RESET_ON_ERROR = "dtvResetOnError";
  private static final String PROP_WAIT_AFTER_RESET_MILLIS = "dtvWaitAfterResetMillis";

  private Map<String, String> fileMap_;
  private Map<String, String> fileAsMap_;
  private String fileBaseLocation_;
  private File fileBaseDir_;
  private int failureCount_ = 0;
  private boolean shuttingDown_ = false;
  static int threadIndex_ = 0;
  private ILineItemRenderer lineItemRenderer_ = null;
  private ILineItemFilter lineItemFilter_ = null;

  static final Logger commLogger_ = Logger.getLogger(HypercomFormBuilderDevice.class.getName() + ".COMM");
  static final boolean COMM_DEBUG_ENABLED = commLogger_.isDebugEnabled();
  //  private static final boolean INFO_ENABLED = logger_.isInfoEnabled();

  //  private final Eventor events_ = new DefaultEventor(new EventDescriptor(SIGCAP_EVENTS, this));
  private int waitAfterResetMillis_ = 500;

  boolean resetOnError_ = false;
  public static final int[] DIRECT_IO_DATA = new int[] {1};
  private static final String FS = "\u001C";
  //  private static final String RS = "\u001E";
  private static final char cFS = '\u001C';
  //  private static final char cRS = '\u001E';

  protected static final String FONT_FILE_NAME = "GUIFONTS.GFF";
  Object currentFormSynch_ = new Object();

  public static HypercomFormBuilderDevice getInstance(HardwareType<?> argHardwareType) {
    String jposEntryKey =
        HardwareMgr.getCurrentHardwareMgr().getHardwareConfig().getJposEntryKey(null, argHardwareType, "");
    INSTANCE.setJposEntryKey(jposEntryKey);
    return INSTANCE;
  }

  public HypercomFormBuilderDevice() {
    super(HardwareMgr.getCurrentHardwareMgr(), HardwareType.forUse(HardwareFamilyType.CUST_DISPLAY,
        "HYPERCOM"), "Hypercom multi-use device");
    control_ = new jpos.LineDisplay();

    //    //Added the following to support the line display.
    //    final String rendererClassName = getProperty("RendererClass", (String) null, false);
    //    IBufferedDisplayCallback renderer = null;
    //    if (!StringUtils.isEmpty(rendererClassName)) {
    //      try {
    //        final Class clazz = Class.forName(rendererClassName);
    //        final Constructor ctor = clazz.getConstructor(new Class[] {DtvCustDisplay.class});
    //        renderer = (IBufferedDisplayCallback) ctor.newInstance(new Object[] {this});
    //      }
    //      catch (final Throwable ex) {
    //        logger_.error("CAUGHT EXCEPTION", ex);
    //      }
    //    }

    //    if (renderer == null) {
    //      renderer = new dtv.hardware.ingenico.jpos.IngenicoCustDisplayRenderer(this);
    //    }
    //    renderer_ = renderer;
    //    eventBufferer_ = new BufferedDisplayEventHandler(renderer_);
  }

  /** {@inheritDoc} */
  @Override
  public void clearDisplay() {
    logger_.info("in clearDisplay");
    //    control_.clearText();
    if (!isPresent()) {
      return;
    }
    queueWorker(new Runnable() {
      @Override
      public void run() {
        getStateStack().setLevel(getStateLevelCallback(), StateLevel.INITIALIZED);
      }
    });
  }

  /** {@inheritDoc} */
  @Override
  public void displayText(final IFormattable argFormattable) {
    queueWorker(new Runnable() {
      @Override
      public void run() {
        String text = argFormattable.toString(OUTPUT_CONTEXT);
        try {
          if (getStateStack().getLevel().compareTo(HypercomStateLevel.IDLE) <= 0) {
            getStateStack().setLevel(getStateLevelCallback(), HypercomStateLevel.IDLE);
          }
        }
        catch (Throwable thr) {
          JposException ex = toJposException(thr);
          getHardwareMgr().logJposException(
              "problem displaying text '" + text + "' on signature capture device",
              HypercomFormBuilderDevice.this, ex);
        }
      }
    });
  }

  /** {@inheritDoc} */
  @Override
  public Eventor getEventor() {
    return events_;
  }

  /** {@inheritDoc} */
  @Override
  protected final jpos.LineDisplay getJposControl() {
    return control_;
  }

  /** {@inheritDoc} */
  @Override
  protected void beforeStartup() {
    control_ = new jpos.LineDisplay();
    readState();
    //    startupLineDisplay();
    //    try{
    //    control_.displayText("Hello world", 1);
    ////    control_.clearText();
    //    }
    //    catch (JposException ex) {
    //      ex.printStackTrace();
    //    }
  }

  /** {@inheritDoc} */
  @Override
  protected HardwareFamilyType<?> getHardwareFamilyType() {
    return HardwareFamilyType.CUST_DISPLAY;
  }

  private void readState() {
    deviceState_ = DeviceState.load(getStateFile(), getHardwareStartupMessages());
  }

  public DeviceState getDeviceState() {
    return deviceState_;
  }

  private File getStateFile() {
    return new File(getProperty(PROP_DEVICE_STATE_FILE, "tmp/device.ste", false));
  }

  @Override
  public final void idleDisplay() {
    if (!isPresent()) {
      return;
    }
    queueWorker(new Runnable() {
      @Override
      public void run() {
        getStateStack().setLevel(getStateLevelCallback(), HypercomStateLevel.IDLE);
      }
    });
  }

  protected void queueWorker(final Runnable argRunnable) {
    if (Thread.currentThread() == workerQueueProcessor_) {
      increaseWorkerCallLevel();
      argRunnable.run();
      decreaseWorkerCallLevel();
    }
    else {
      synchronized (workers_) {
        workers_.add(argRunnable);
      }
      startQueueProcessor();
    }
  }

  private class QueueProcessorThread
      extends Thread {

    private boolean _exiting;

    private CountDownLatch _flushLatch;
    private final Runnable _flushMark = new Runnable() {
      @Override
      public void run() {
        /*do nothing*/
      }
    };

    public QueueProcessorThread() {
      setName("HypercomFormBuilderDevice-QueueProcessorThread");
      setDaemon(true);
    }

    public void flush()
        throws InterruptedException {

      _flushLatch = new CountDownLatch(1);
      workers_.clear();
      //FIXME - verify: no sync?
      workers_.add(_flushMark);
      _flushLatch.await();
    }

    public void requestExit() {
      _exiting = true;
      //      //Add a new thread to the queue to unblock the blocking queue's take method.
      //      workers_.add(new Thread());
      //      workerQueueProcessor_.interrupt();
      synchronized (workers_) {
        workers_.add(_flushMark);
      }
    }

    @Override
    public void run() {
      for (;;) {
        try {
          final Runnable w = workers_.take();
          if (_exiting) {
            return;
          }
          if (w != _flushMark) {
            increaseWorkerCallLevel();
            w.run();
            decreaseWorkerCallLevel();
          }
          else {
            if (_flushLatch != null) {
              _flushLatch.countDown();
            }
          }
        }
        catch (final Exception ex) {
          logger_.error("CAUGHT EXCEPTION", ex);
        }
      }
    }

    public void shutdown() {
      requestExit();
      try {
        join(3000);
      }
      catch (final InterruptedException ex) {
        /*do nothing*/
      }
      if (isAlive()) {
        interrupt();
      }
    }
  }

  void increaseWorkerCallLevel() {
    if (workerCallLevel_++ > MAX_WORKER_CALL_LEVEL) {
      throw new RuntimeException("WORKER STACK OVERFLOW");
    }
  }

  void decreaseWorkerCallLevel() {
    workerCallLevel_-- ;
  }

  private void startQueueProcessor() {
    if ((workerQueueProcessor_ == null) || !workerQueueProcessor_.isAlive()) {
      workerQueueProcessor_ = new QueueProcessorThread();
      workerQueueProcessor_.start();
    }
  }

  /** {@inheritDoc} */
  @Override
  protected IHypercomStateLevelCallback makeStateLevelCallback() {
    return new HypercomStateChangeCallback(null);
  }

  /** {@inheritDoc} */
  @Override
  public void subtotalMode(boolean newValue) {
    if (isCustomerDisplayEnabled()) {
      StateLevel level = getStateStack().getLevel();
      if (!newValue && (level != null) && (level.compareTo(StateLevel.INITIALIZED) >= 0)
          && (level.compareTo(HypercomStateLevel.ITEMS) < 0)) {

        queueWorker(new Runnable() {
          @Override
          public void run() {
            getStateStack().setLevel(getStateLevelCallback(), HypercomStateLevel.ITEMS);
          }
        });
      }
      else if (newValue && (level != null) && (level.compareTo(HypercomStateLevel.ITEMS) == 0)) {
        level.enterState(makeStateLevelCallback(), HypercomStateLevel.ITEMS);
      }
      //
      ////      startupLineDisplay();
      ////    try{
      ////    control_.displayText("Hello world", 1);
      ////    control_.clearText();
      ////    }
      ////    catch (JposException ex) {
      ////      ex.printStackTrace();
      ////    }
    }
  }

  boolean isCustomerDisplayEnabled() {

    return hardwareFamilyTypes_.contains(HardwareFamilyType.CUST_DISPLAY);
  }

  /** {@inheritDoc} */
  @Override
  public void setDirectIOListener(DirectIOListener l) {
    synchronized (subDirectIOListenerMutex_) {
      subDirectIOListener_ = l;
      if (DEBUG_ENABLED) {
        logger_.debug("======subDirectIOListener_ is " + subDirectIOListener_);
      }
    }
  }

  @Override
  public void unsetDirectIOListener(DirectIOListener l) {
    if (subDirectIOListener_ == l) {
      synchronized (subDirectIOListenerMutex_) {
        if (subDirectIOListener_ == l) {
          subDirectIOListener_ = null;
          logger_.debug("======subDirectIOListener_ cleared");
        }
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public void directIO(int argCommand, int[] argData, Object argObject)
      throws JposException {
    if ((Thread.currentThread() != workerQueueProcessor_) && (workerQueueProcessor_ != null)
        && !shuttingDown_) {
      logger_.warn("directIO on unexpected thread!!", new Throwable("STACK TRACE"));
    }
    if (COMM_DEBUG_ENABLED) {
      commLogger_.debug("O:" + toCleanString(argObject));
    }
    waitForNotBusy(3000);
    if (!_NO__HARDWARE()) {
      control_.directIO(0, argData, argObject);
    }
  }

  @Override
  public int getTimeout() {
    return timeout_;
  }

  public void shutdownLineDisplay() {
    try {
      control_.setDeviceEnabled(false);
    }
    catch (JposException ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
    }
    try {
      control_.release();
    }
    catch (JposException ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
    }
    try {
      control_.close();
    }
    catch (JposException ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
    }
    control_ = null;
  }

  public void startupLineDisplay() {
    control_ = new LineDisplay();
    try {
      control_.open(getJposEntryKey());
    }
    catch (JposException ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
    }
    try {
      control_.claim(1000);
    }
    catch (JposException ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
    }
    try {
      control_.setDeviceEnabled(true);
      control_.clearText();
    }
    catch (JposException ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
    }
  }

  /** {@inheritDoc} */
  @Override
  protected void addListeners() {
    if (!_NO__HARDWARE()) {
      control_.addDirectIOListener(directIOListener_);
      control_.addStatusUpdateListener(getJposListener());
    }
  }

  JposListener directIOListener_ = new JposListener("MainDirectIOListener", this) {

    @Override
    public void directIOOccurred(DirectIOEvent argEvent) {
      try {
        busyAttempt();
        logger_.debug("-----------DirectIOListener.directIO");
        //        if (COMM_DEBUG_ENABLED) {
        //          commLogger_.debug("I:" + stringFor(argEvent.getObject()));
        //        }
        if (!filterEvent(argEvent)) {
          DirectIOListener l = HypercomFormBuilderDevice.this.subDirectIOListener_;
          if (l == null) {
            l = HypercomFormBuilderDevice.this.getJposListener();
          }
          else {
            HypercomFormBuilderDevice.this.unsetDirectIOListener(l);
          }
          //          logger_.log(mutexLogLevel_, "notifying " + l + "-->" + stringFor(argEvent.getObject()));
          l.directIOOccurred(argEvent);
        }
      }
      catch (InterruptedException ex) {
        logger_.debug("CAUGHT EXCEPTION", ex);
      }
      finally {
        logger_.log(mutexLogLevel_, "releasing busy mutex"
        //, new Throwable("STACK TRACE")
            );
        busyRelease();
      }
    }
  };

  void busyAttempt()
      throws InterruptedException {

    if (!useBusyMutex_) {
      return;
    }
    logger_.log(mutexLogLevel_, "attempting to acquire busy"
    //, new Throwable("STACK TRACE")
        );
    if (busy_.attempt(0)) {
      logger_.log(mutexLogLevel_, "...busy mutex acquired");
      busyOwner_ = Thread.currentThread();
    }
    else {
      logger_.log(mutexLogLevel_, "...busy mutex not acquired::owned by " + busyOwner_);
    }
  }

  void busyRelease() {
    if (!useBusyMutex_) {
      return;
    }
    if (busyOwner_ == Thread.currentThread()) {
      logger_.log(mutexLogLevel_, "busy released");
      busyOwner_ = null;
    }
    busy_.release();
  }

  // filter any button events that occur after a question is answered
  protected boolean filterEvent(DirectIOEvent argEvent) {
    //    byte[] bytes = (byte[]) argEvent.getObject();
    //    if (bytes.length > 3 //
    //        && bytes[0] == (byte) '8' //
    //        && bytes[1] == (byte) '1' //
    //        && bytes[2] == (byte) '.') {
    //      if (DEBUG_ENABLED) {
    //        logger_.debug("Unexpected card swipe message; suppressing");
    //      }
    //      return true;
    //    }
    //    // if we are currently asking a question, filter no events
    //    if (questionResponseListener_ == subDirectIOListener_) {
    //      return false;
    //    }
    //    if (bytes.length >= 4 
    //        && bytes[0] == (byte)'X' 
    //        && bytes[1] == (byte)'E' 
    //        && bytes[2] == (byte)'V' 
    //        && bytes[3] == (byte)'T') {
    //      if (INFO_ENABLED) {
    //        logger_.info("Unexpected event; not asking a question:" + stringFor(bytes));
    //      }
    //      return true;
    //    }
    return false;
  }

  @Override
  public void addUse(HardwareType<?> argUse) {
    IHardwareFamilyType<?> family = argUse.getFamily();
    if (HardwareFamilyType.PINPAD.equals(family)) {
      if (pinpad_ == null) {
        pinpad_ = new DtvDukptPinPad(hardwareMgr_, argUse);
        // take PINpad events only from the PINpad we own
        EventManager.registerEventHandler(pinpadListener_, new EventDescriptor(IDtvPinPad.PINPAD_EVENTS,
            pinpad_));
      }
    }
    if (HardwareFamilyType.SIG_CAP.equals(family)) {
      if (sigcap_ == null) {
        sigcap_ = new DtvSigCap(hardwareMgr_, argUse);
        // take SigCap events only from the SigCap we own
        EventManager.registerEventHandler(sigcapListener_, new EventDescriptor(IDtvSigCap.SIGCAP_EVENTS,
            sigcap_));
      }
    }
    if (HardwareFamilyType.MSR.equals(family)) {
      if (msr_ == null) {
        msr_ = new DtvMsr(hardwareMgr_, argUse);
        // take MSR events from any MSR device even if it's not the one we own
        EventManager.registerEventHandler(msrListener_, new EventDescriptor(IDtvMsr.MSR_EVENTS));
      }
    }
    hardwareFamilyTypes_.add(family);
  }

  @Override
  public void handleDisplayStateChange(final DeviceTranState state) {
    if (isCustomerDisplayEnabled()) {
      if (currentForm_ != null) {
        queueWorker(new Runnable() {
          @Override
          public void run() {
            final IDeviceForm form = currentForm_;
            if (form != null) {
              // enter ITEM mode if there are any items on the transaction
              if ((getStateStack().getLevel().compareTo(HypercomStateLevel.ITEMS) < 0)
                  && (state.getItems().length > 0)) {
                getStateStack().setLevel(getStateLevelCallback(), HypercomStateLevel.ITEMS);
              }
              form.handleDisplayStateChange(state);
            }
          }
        });
      }
    }
  }

  @Override
  public void handleTenderLineCanceled() {
    if (isCustomerDisplayEnabled()) {
      final IDeviceForm form = currentForm_;
      if (form != null) {
        queueWorker(new Runnable() {
          public void run() {
            form.handleTenderLineCanceled();
          }
        });
      }

    }
  }

  protected class HypercomStateChangeCallback
      extends AbstractDtvJposDevice.DefaultStateChangeCallback
      implements IHypercomStateLevelCallback {

    private final Map<TypeSafeMapKey<?>, ?> suppliedParameters_;
    private Integer maximumTimeout_ = null;

    public HypercomStateChangeCallback(Map<TypeSafeMapKey<?>, ?> argParameters) {
      suppliedParameters_ = argParameters;
    }

    @Override
    public void enableMsr() {
      HypercomFormBuilderDevice.this.enableMsr();
    }

    @Override
    public ILineItemRenderer getLineItemRenderer() {
      return HypercomFormBuilderDevice.this.getLineItemRenderer();
    }

    @Override
    public String[] sendCommand(IDeviceForm argForm, String argCommand, Integer argResponseTimeOut) {
      validateCurrentForm(argForm);
      return HypercomFormBuilderDevice.this.sendCommand(argCommand, argResponseTimeOut);
    }

    @Override
    public String[] sendCommandAndValidate(IDeviceForm argForm, String[] argCommandData, Integer argTimeout)
        throws JposException {

      validateCurrentForm(argForm);
      return HypercomFormBuilderDevice.this.sendCommandAndValidate(argCommandData, argTimeout);
    }

    private void validateCurrentForm(IDeviceForm argForm) {
      if ((HypercomFormBuilderDevice.this.currentForm_ != null)
          && (HypercomFormBuilderDevice.this.currentForm_ != argForm)) {
        throw new IllegalStateException("current form is "
            + HypercomFormBuilderDevice.this.currentForm_.getFormId() + " not " + argForm.getFormId());
      }
    }

    @Override
    public void setCurrentForm(IDeviceForm argForm, Runnable argFormModifier, StateLevel argPreviousState) {
      if (argForm != null) {
        try {

          synchronized (currentFormSynch_) {

            HypercomFormBuilderDevice.this.currentForm_ = argForm;
            argForm.show(argFormModifier, argPreviousState);
          }
        }
        catch (Throwable thr) {
          JposException ex = AbstractDtvJposDevice.toJposException(thr);
          logJposException("problem displaying form '" + argForm.getFormId() + "'", ex);
        }
      }
    }

    @Override
    public Map<TypeSafeMapKey<?>, ?> getSuppliedParameters() {
      return suppliedParameters_;
    }

    @Override
    public OutputContextType getOutputContextType() {
      return OutputContextType.POLE_DISPLAY;
    }

    /** {@inheritDoc} */
    @Override
    public void queueWorker(Runnable argWorker) {
      HypercomFormBuilderDevice.this.queueWorker(argWorker);
    }

    /** {@inheritDoc} */
    @Override
    public void queueWorkerAndWait(Runnable argRunnable)
        throws InterruptedException {

      HypercomFormBuilderDevice.this.queueWorkerAndWait(argRunnable);
    }

    /** {@inheritDoc} */
    @Override
    public Eventor getEventor() {
      return HypercomFormBuilderDevice.this.getEventor();
    }

    /** {@inheritDoc} */
    @Override
    public int getTimeout() {
      return HypercomFormBuilderDevice.this.getTimeout();
    }

    /** {@inheritDoc} */
    @Override
    public void logJposException(JposException argException) {
      super.logJposException(argException);
      resetOnError(argException);
    }

    /** {@inheritDoc} */
    @Override
    public void logJposException(String argMessage, JposException argException) {
      super.logJposException(argMessage, argException);
      resetOnError(argException);
    }

    //private static final int MAX_BATCH_MESSAGE_SIZE = 480;
    //private int sendBatch(List<String> argCommands, int argStart) {
    //  int index = argStart;
    //  StringBuilder sb = new StringBuilder();
    //  sb.append(HypercomCommands.CMD_BATCH);
    //  sb.append(FS);
    //  sb.append(argCommands.get(index++));
    //
    //  if (index < argCommands.size()) {
    //    String s = argCommands.get(index++ );
    //
    //    // test if we have room for the next command
    //    for (; s.length() + sb.length() + 1 < MAX_BATCH_MESSAGE_SIZE;) {
    //      // we do, so add it
    //      if (!HypercomCommands.CMD_SHOW_FORM.equals(s)) {
    //        // but only if it's not an XSFM...we'll add that manually to the end of every packet
    //        sb.append(RS);
    //        sb.append(s);
    //      }
    //      if (index == argCommands.size()) {
    //        break;
    //      }
    //      s = argCommands.get(index++ );
    //    }
    //  }
    //  sb.append(RS).append(VfCommands.CMD_SHOW_FORM);
    //  int maxSent = index - 1;
    //
    //  int timeout = (maximumTimeout_ == null ? getTimeout() : maximumTimeout_.intValue());
    //
    //  // send the batch command
    //  String[] response = HypercomFormBuilderDevice.this.sendCommand(sb.toString(), new Integer(timeout));
    //  logResponse(response);
    //
    //  // validate the response is a batch response
    //  if (response == null || !VfCommands.CMD_BATCH.equals(response[0])) {
    //    String msg = "bad response; command='" + ByteUtils.toString(sb.toString().getBytes()) //
    //                 + "', response='" + cleanResponse(response) + "'";
    //    logger_.error(msg);
    //  }
    //  else {
    //    validateBatchResponse(sb, response);
    //  }
    //  return maxSent;
    //}
    //
    //private void validateBatchResponse(StringBuilder argRequest, String[] response) {
    //  try {
    //    //validate the response parts and log if any failed
    //    String[] responseParts = StringUtils.split(StringUtils.join(response, FS, 1, response.length - 1), cRS);
    //    for (int i = 0; i < responseParts.length; i++ ) {
    //      String[] subresponse = StringUtils.split(responseParts[i], cFS);
    //      if (subresponse.length > 1 && !"1".equals(subresponse[1])) {
    //        logger_.warn(ByteUtils.toString(("bad response; command='" + argRequest + "', response='"
    //                                         + cleanResponse(response) + "'").getBytes()));
    //        break;
    //      }
    //    }
    //  }
    //  catch (Throwable ex) {
    //
    //    logger_.error(ByteUtils.toString(("CAUGHT EXCEPTION; command='" + argRequest + "', response='"
    //        + cleanResponse(response) + "'").getBytes()), ex);
    //  }
    //}  
  }

  void enableMsr() {
    if (!_NO__HARDWARE()) {
      if (msr_ != null) {
        try {
          queueWorkerAndWait(new Runnable() {
            @Override
            public void run() {
              msr_.enable();
            }
          });
        }
        catch (InterruptedException ex) {
          logger_.debug("CAUGHT EXCEPTION", ex);
        }
      }
    }
  }

  void enableMsrNoWait() {
    if (!_NO__HARDWARE()) {
      if (msr_ != null) {
        queueWorker(new Runnable() {
          @Override
          public void run() {
            msr_.enable();
          }
        });
      }
    }
  }

  void disableMsr() {
    if (!_NO__HARDWARE()) {
      if (msr_ != null) {
        try {
          queueWorkerAndWait(new Runnable() {
            @Override
            public void run() {
              msr_.disable();
              resetDevice();
            }
          });
        }
        catch (InterruptedException ex) {
          logger_.debug("CAUGHT EXCEPTION", ex);
        }
      }
    }
  }

  public void resetDevice() {
    // no response expected; so no wait
    // sends a reset device message
    //    sendCommand(HypercomCommands.CMD_RETURN_TO_IDLE, 0);
    sleep(waitAfterResetMillis_);
  }

  public String[] sendCommand(String argCommand, Integer argResponseTimeOut) {
    return sendCommand(argCommand, argResponseTimeOut, null);
  }

  protected String[] sendCommand(String argCommand, Integer argResponseTimeOut,
      DirectIOListener argSublistener) {
    if (argResponseTimeOut == null) {
      argResponseTimeOut = Integer.valueOf(getTimeout());
    }
    try {
      busyAcquire();
      try {
        if (argResponseTimeOut.intValue() > 0) {
          DirectIOCall call = new DirectIOCall(this, argCommand);
          call.setTimeout(argResponseTimeOut.intValue());
          call.setSublistener(argSublistener);
          DirectIOEvent e = call.call();
          failureCount_ = 0;
          //        lastErrorCode_ = 0;
          return getResponseParts(e);
        }
        else {
          if (expectHighLevelResponse(argCommand)) {
            logger_.warn("no timeout for '" + argCommand + "'!?!?");
          }
          directIO(100, DIRECT_IO_DATA, argCommand);
          failureCount_ = 0;
          //        lastErrorCode_ = 0;
          return null;
        }
      }
      finally {
        busyRelease();
      }
    }
    catch (Throwable t) {
      if (shuttingDown_) {
        return null;
      }
      JposException ex = toJposException(t);
      hardwareMgr_.logJposException("problem sending command '" + toCleanString(argCommand) + "' ("
          + getType().getName() + ")", HypercomFormBuilderDevice.this, ex);
      //      lastErrorCode_ = ex.getErrorCode();
      failureCount_++ ;
      if (failureCount_ > maxErrorCount_) {
        disabledDueToError_ = true;
        logger_.warn("too many consecutive errors (" + failureCount_ + ")...disabling the device");
        try {
          shutdown();
        }
        catch (Exception ex2) {
          logger_.error("CAUGHT EXCEPTION", ex);
        }
      }
    }
    return null;
  }

  public ILineItemRenderer getLineItemRenderer() {
    if (lineItemRenderer_ == null) {
      int lineWidth = getProperty(PROP_LINE_ITEM_WIDTH, Integer.valueOf(33), false).intValue();
      try {
        // try creating with ctor(int)
        Class<?> rendererClass = Class.forName(getProperty(PROP_LINE_ITEM_RENDERER, (String) null, false));
        Constructor<?> ctor = rendererClass.getConstructor(new Class[] {int.class});
        lineItemRenderer_ = (ILineItemRenderer) ctor.newInstance(new Object[] {new Integer(lineWidth)});
      }
      catch (Throwable ex) {
        logger_.debug("CAUGHT EXCEPTION", ex);
      }
      if (lineItemRenderer_ == null) {
        try {
          // try no-arg ctor
          Class<?> rendererClass = Class.forName(getProperty(PROP_LINE_ITEM_RENDERER, (String) null, false));
          lineItemRenderer_ = (ILineItemRenderer) rendererClass.newInstance();
        }
        catch (Throwable ex) {
          logger_.debug("CAUGHT EXCEPTION", ex);
        }
        if (lineItemRenderer_ == null) {
          // use default renderer instead
          logger_
              .info("using default line item renderer::customer account lines may not renderer as desired");
          lineItemRenderer_ = new DefaultLineItemRenderer(lineWidth);
        }
      }
    }
    return lineItemRenderer_;
  }

  public String[] sendCommandAndValidate(String[] argCommandData, Integer argTimeout)
      throws JposException {

    String cmd = makeCommand(argCommandData);
    String[] response = sendCommand(cmd, argTimeout);
    if ((response == null) && (failureCount_ == 1)) {
      sleep(1000);
      response = sendCommand(cmd, argTimeout);
    }
    validateResponse(argCommandData[0], response);
    return response;
  }

  public static void sleep(int millis) {
    try {
      Thread.sleep(millis);
    }
    catch (InterruptedException ex) {
      /*do nothing*/
    }
  }

  public static String makeCommand(String... argCommandData) {
    return StringUtils.join(argCommandData, FS);
  }

  public static void validateResponse(String argCommand, String[] argResponse)
      throws JposException {

    if (_NO__HARDWARE) {
      return;
    }
    if (argResponse == null) {
      throw new JposException(JposConst.JPOS_E_FAILURE, "no response");
    }
    if (!argCommand.equals(argResponse[0])) {
      logResponse(argResponse);
      throw new JposException(JposConst.JPOS_E_FAILURE, "bad response; command='" + argCommand
          + "', response='" + cleanResponse(argResponse) + "'");
    }
    if (!"1".equals(argResponse[1])) {
      logResponse(argResponse);
      throw new JposException(JposConst.JPOS_E_FAILURE, "bad response; value='" + argResponse[1] + "'");
    }
  }

  public static void logResponse(String[] argResponse) {
    if (COMM_DEBUG_ENABLED) {
      commLogger_.debug("RESPONSE:" + cleanResponse(argResponse));
    }
  }

  protected static String cleanResponse(String[] response) {
    if (response == null) {
      return "null";
    }
    //    if (response[0].startsWith("81.")) {
    //      return "81.(card data suppressed)";
    //    }
    return ByteUtils.toString((StringUtils.join(response, "<FS>")).getBytes());
  }

  protected void queueWorkerAndWait(Runnable argRunnable)
      throws InterruptedException {

    if (Thread.currentThread() == workerQueueProcessor_) {
      increaseWorkerCallLevel();
      argRunnable.run();
      decreaseWorkerCallLevel();
    }
    else {
      if (isBusyOwner()) {
        logger_.warn("DETECTED AND AVOIDED LOCKUP: TRYING TO WAIT FROM A BAD THREAD", new Throwable(
            "STACK TRACE"));
        queueWorker(argRunnable);
      }
      else {
        WaitableWorker w = new WaitableWorker(argRunnable);
        synchronized (workers_) {
          workers_.put(w);
        }
        startQueueProcessor();
        w.waitForCompletion();
      }
    }
  }

  private boolean isBusyOwner() {
    return busyOwner_ == Thread.currentThread();
  }

  public void resetOnError(JposException argException) {
    if (resetOnError_ && (argException.getErrorCode() != JposConst.JPOS_E_TIMEOUT)) {
      resetDevice();
    }
  }

  private void busyAcquire()
      throws InterruptedException {

    if (!useBusyMutex_) {
      return;
    }
    busy_.acquire();
    logger_.log(mutexLogLevel_, "...busy mutex acquired");
    busyOwner_ = Thread.currentThread();
  }

  public static String[] getResponseParts(DirectIOEvent argResponse) {
    if (argResponse == null) {
      return null;
    }
    final String s = new String(((byte[]) argResponse.getObject()));

    int found = s.indexOf(cFS);

    if (found == -1) {
      return new String[] {s};
    }

    List<String> parts = new ArrayList<String>(4);

    int lastFound = 0;
    for (; found != -1; found = s.indexOf(cFS, lastFound)) {

      parts.add(s.substring(lastFound, found));
      lastFound = found + 1;
    }

    // add the last part
    parts.add(s.substring(lastFound, s.length()));

    return parts.toArray(new String[parts.size()]);
  }

  public boolean expectHighLevelResponse(String argCommand) {
    //    switch (argCommand.charAt(0)) {
    //      // "72" has no high level response
    //      case '7':
    //        return argCommand.charAt(1) != '2';
    //      // "XLED" and "XRST" have no high level response
    //      case 'X':
    //        switch (argCommand.charAt(1)) {
    //          case 'L':
    //            switch(argCommand.charAt(2)) {
    //              case 'C':
    //                // XLCS - clear list - has no high level response
    //                return argCommand.charAt(3) != 'S';
    //              case 'E':
    //                // XLED - LED control - has no high level response
    //                return argCommand.charAt(3) != 'D';
    //            }
    //            break;
    //          case 'R':
    //            return !argCommand.startsWith("XRST");
    //          case 'M':
    //            return !argCommand.startsWith("XMBH");
    //        }
    //        break;
    //
    //      // "S03", "S04", etc. have no high level response
    //      case 'S':
    //        if (argCommand.charAt(1) == '0') {
    //          return false;
    //        }
    //        break;
    //    }
    return true;
  }

  String toCleanString(Object argObject) {
    String s = String.valueOf(argObject);
    try {
      byte[] ba = s.getBytes("UTF-8");
      s = ByteUtils.toString(ba);
    }
    catch (Exception ex) {
      logger_.debug("CAUGHT EXCEPTION", ex);
    }
    return s;
  }

  /** {@inheritDoc} */
  @Override
  public boolean isPresent() {
    return true;
  }

  // implements IDtvSigCap

  private static final String FORM_NAME = "FN";
  private static final String CREDIT_DEBIT = "creditDebit";
  private static final String SIGCAP_FORM = "SIGNATURE";

  /*  private static final String DISPLAY_LINE = "DL";
    private static final String BUFFER_SIZE = "SB";
    private static final String DCC_FORM = "scrCurrency";
    private static final String DCC_CONFIRM = "CONFIRM";
    
    private static final String SWIPE_FORM = "SWIPEFRM";
  */
  /** {@inheritDoc} */
  @Override
  public void beginCapture(final Map<TypeSafeMapKey<?>, ?> argParameters)
      throws SigCapException {
    //nothing for now

    if (disabledDueToError_) {
      throw new SigCapException();
    }

    FutureTask<Boolean> task = new FutureTask<Boolean>(new Callable<Boolean>() {

      @Override
      public Boolean call()
          throws SigCapException {

        // disable the msr
        disable();
        final IHypercomStateLevelCallback callback;
        if (isCustomerDisplayEnabled()) {
          // include the tender as supplied data, so the amount, etc will be included
          callback = new HypercomStateChangeCallback(argParameters);
        }
        else {
          callback = getStateLevelCallback();
        }

        ITenderLineItem line = (ITenderLineItem) argParameters.get(IDtvSigCap.CREDIT_DEBIT_LINE_KEY);

        getStateStack().setLevel(callback, HypercomStateLevel.CAPTURING_SIGNATURE);
        //        }
        if (!_NO__HARDWARE()) {
          if ((line instanceof ICreditDebitTenderLineItem)) {

            ICreditDebitTenderLineItem creditLine = (ICreditDebitTenderLineItem) line;
            sigcap_.beginCapture(argParameters);

            //Customization to display customer name, amount and Acct Number on the signature screen
            sendCommand(
                HypercomCommands.CMD_FORM_REQUEST
                    + FS
                    + FORM_NAME
                    + SIGCAP_FORM
                    + FS
                    + HypercomCommands.CMD_GLOBAL_PROMPT_1
                    + HH.makeTranslatable("_sigCapPleaseSign", new IFormattable[0])
                    + FS
                    + HypercomCommands.CMD_GLOBAL_PROMPT_2
                    + StringUtils.truncate(creditLine.getCustomerName(), 25)
                    + FS
                    + HypercomCommands.CMD_GLOBAL_PROMPT_3
                    + FF.getSimpleFormattable(creditLine.getAmount(), FormatterType.MONEY)
                    + FS
                    + HypercomCommands.CMD_GLOBAL_PROMPT_4
                    + FF.getSimpleFormattable(creditLine.getAccountNumber(),
                        FormatterType.forName("CreditCard")), Integer.valueOf(getTimeout()));

          }
        }
        return Boolean.TRUE;
      }
    });
    queueWorker(task);
    try {
      task.get();
    }
    catch (ExecutionException ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
      throw SigCapException.toSigCapException(ex.getCause());
    }
    catch (InterruptedException ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
    }

  }

  /** {@inheritDoc} */
  @Override
  public void endCapture(final boolean argWait) {
    //nothing for now

    try {
      Runnable w = new Runnable() {
        @Override
        public void run() {

          if (!_NO__HARDWARE()) {
            sigcap_.endCapture(argWait);
          }
          if (argWait) {
            getStateStack().revertLevel(getStateLevelCallback());
          }
          else {
            getStateStack().setLevel(getStateLevelCallback(), HypercomStateLevel.WAIT);
          }
          enableMsr();
        }
      };
      if (argWait) {
        queueWorkerAndWait(w);
      }
      else {
        queueWorker(w);
      }
    }
    catch (InterruptedException ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
    }

  }

  @Override
  protected boolean initializeImpl() {

    displayModel_ =
        new SigCapDisplayModel(HypercomFormBuilderDevice.this, getLineItemRenderer(), getLineItemFilter());
    clearDisplayImpl();
    return true;
  }

  public ILineItemFilter getLineItemFilter() {
    if (lineItemFilter_ == null) {
      try {
        String impl = getProperty(PROP_LINE_ITEM_FILTER, (String) null, true);
        lineItemFilter_ = (ILineItemFilter) Class.forName(impl).newInstance();
      }
      catch (Throwable ex) {
        logger_.error("CAUGHT EXCEPTION", ex);
        lineItemFilter_ = new UnfilteredLineItemFilter();
      }
    }
    return lineItemFilter_;
  }

  protected void clearDisplayImpl() {
    //    sendCommandAndValidate(HypercomCommands.CMD_CLEAR_ALL_DISPLAY_LINES, getTimeout());
  }

  protected void sendCommandAndValidate(String argCommand, Integer timeout) {

    try {
      String[] response = sendCommand(argCommand, timeout);
      validateResponse(argCommand, response);
    }
    catch (JposException ex) {

      hardwareMgr_.logJposException(this, ex);
      resetOnError(ex);
    }
  }

  private boolean temporaryShutdown_ = true;
  boolean clearOnExit_ = false;

  /** disable, release, and close the device */
  @Override
  protected void beforeShutdown() {
    shuttingDown_ = true;
    fileMap_ = null;
    fileAsMap_ = null;
    disabledDueToError_ = false;
    if (displayModel_ != null) {
      displayModel_.stopProcessing();
    }
    displayModel_ = null;

    stopQueueProcessor();
    if (!temporaryShutdown_) {
      if (clearOnExit_) {
        clearDisplayImpl();
      }
      else {
        try {
          if (getStateStack().getLevel() != HypercomStateLevel.IDLE) {
            getStateStack().setLevel(getStateLevelCallback(), HypercomStateLevel.IDLE);
          }
        }
        catch (Exception ex) {
          logger_.error("CAUGHT EXCEPTION", ex);
        }
      }
    }
    shutdownSubdevices();
    if (!temporaryShutdown_) {
      resetDevice();
    }
  }

  protected void stopQueueProcessor() {
    if (workerQueueProcessor_ != null) {
      workerQueueProcessor_.shutdown();
      workerQueueProcessor_ = null;
    }
  }

  public void shutdownSubdevices() {
    if (!_NO__HARDWARE()) {
      if (pinpad_ != null) {
        pinpad_.shutdown();
      }
      if (sigcap_ != null) {
        sigcap_.shutdown();
      }
      if (msr_ != null) {
        msr_.shutdown();
      }
    }
  }

  private EventHandler pinpadListener_ = new EventHandler() {
    @Override
    protected void handle(final Event argEvent) {
      try {
        if (IDtvInputDevice.INPUT_COMPLETE_EVENT == argEvent.getName()) {
          queueWorker(new Runnable() {
            @Override
            public void run() {
              getStateStack().revertLevel(getStateLevelCallback());
              enableMsr();
            }
          });
        }
        else if (IDtvInputDevice.INPUT_CANCELED_EVENT == argEvent.getName()) {
          queueWorker(new Runnable() {
            @Override
            public void run() {
              getStateStack().revertLevel(getStateLevelCallback());
              enableMsr();
            }
          });
        }
      }
      catch (Throwable ex) {
        logger_.error("CAUGHT EXCEPTION", ex);
      }
    }
  };

  private EventHandler sigcapListener_ = new EventHandler() {
    @Override
    protected void handle(Event argEvent) {
      try {
        if (IDtvInputDevice.INPUT_COMPLETE_EVENT == argEvent.getName()) {
          queueWorker(new Runnable() {
            @Override
            public void run() {
              getStateStack().revertLevel(getStateLevelCallback());
              enableMsr();
            }
          });
        }
        else if (IDtvInputDevice.INPUT_CANCELED_EVENT == argEvent.getName()) {
          queueWorker(new Runnable() {
            @Override
            public void run() {
              getStateStack().revertLevel(getStateLevelCallback());
              enableMsr();
            }
          });
        }
      }
      catch (Throwable ex) {
        logger_.error("CAUGHT EXCEPTION", ex);
      }
    }
  };

  private EventHandler msrListener_ = new EventHandler() {
    @Override
    protected void handle(final Event argEvent) {
      try {
        if (IDtvInputDevice.EVENT_ROUTED_EVENT == argEvent.getName()) {
          if (getStateStack() != null) {
            if (HypercomStateLevel.ITEMS.equals(getStateStack().getLevel())) {
              final IXstEvent event = (IXstEvent) argEvent.getPayload();
              queueWorker(new Runnable() {
                @Override
                public void run() {
                  currentForm_.msrSwipeOccurred(event);
                }
              });
            }
          }
        }
      }
      catch (Throwable ex) {
        logger_.error("CAUGHT EXCEPTION", ex);
      }
    }
  };

  @Override
  protected void afterStartup() {
    try {
      storeFiles();
    }
    catch (Exception ex) {
      getHardwareMgr().logJposException("CAUGHT EXCEPTION", this, AbstractDtvJposDevice.toJposException(ex));
    }
    startupSubdevices();
    if (!temporaryShutdown_) {
      try {
        queueWorkerAndWait(new Runnable() {
          @Override
          public void run() {
            clearDisplayImpl();
          }
        });
      }
      catch (InterruptedException ex) {
        getHardwareMgr()
            .logJposException("CAUGHT EXCEPTION", this, AbstractDtvJposDevice.toJposException(ex));
      }
    }
    disabledDueToError_ = false;
    writeState();
    temporaryShutdown_ = false;
  }

  //XXX -verify new throws
  protected void storeFiles()
      throws InterruptedException, IOException {
    while (true) {
      try {
        getDeviceUpdateStrategy().call();
        return;
      }
      catch (RestartStoreFilesException ex) {
        logger_.info("CAUGHT EXCEPTION", ex);
      }
    }
  }

  private IDeviceUpdateStrategy deviceUpdateStrategy_ = null;

  private IDeviceUpdateStrategy getDeviceUpdateStrategy() {
    if (deviceUpdateStrategy_ == null) {
      deviceUpdateStrategy_ = new DefaultDeviceUpdateStrategy();
      deviceUpdateStrategy_.setDevice(this);
      deviceUpdateStrategy_.setFiles(getDeviceFiles());
    }
    return deviceUpdateStrategy_;
  }

  private void writeState() {
    DeviceState.save(deviceState_);
  }

  public File getFile(String argFileName)
      throws IOException {

    return new File(fileBaseDir_, lookupI18n(argFileName)).getCanonicalFile();
  }

  public static String lookupI18n(String argLiteralOrKey) {
    IFormattable f = makeSimpleFormattable(argLiteralOrKey);
    return f.toString(OUTPUT_CONTEXT);
  }

  private static IFormattable makeSimpleFormattable(String argLiteralOrKey) {
    IFormattable f = FF.getSimpleFormattable(argLiteralOrKey);
    if (f instanceof IResourceFormattable) {
      ((IResourceFormattable) f).setResourceBundleName("dtv.pos.i18n.hardware");
    }
    return f;
  }

  /** {@inheritDoc} */
  @Override
  public dtv.hardware.HardwareStartupMessages getHardwareStartupMessages() {
    return super.getHardwareStartupMessages();
  }

  public ProcessBuilder getDownloadCommand(File argFileToLoad)
      throws IOException {
    //    boolean isWindows = System.getProperty("os.name").toLowerCase().startsWith("windows");
    //    String port = getConfig().get("portName", (String) null);
    //    List<String> command = new LinkedList<String>();
    //    if (isWindows) {
    //      command.add(new File("./res/mx870/DDL.exe").getCanonicalPath());
    //      command.add("-p" + port.replace("COM", ""));
    //    }
    //    else {
    //      command.add(new File("./res/mx870/ldl").getCanonicalPath());
    //      command.add("-p" + port);
    //    }
    //    command.add("-b" + getConfig().get("baudRate", (String) null));
    //    command.add("-t5");
    //    command.add("-i" + argFileToLoad.getName());
    //    ProcessBuilder pb = new ProcessBuilder(command);
    //    pb.directory(argFileToLoad.getParentFile());
    //    return pb;
    return null;
  }

  private FileAsPair[] getDeviceFiles() {
    readFiles();

    int idx = 0;
    Map<String, FileAsPair> files = new HashMap<String, FileAsPair>();
    Set<Map.Entry<String, String>> entries = fileMap_.entrySet();
    for (Map.Entry<String, String> e : entries) {
      String name = e.getValue();
      String asName = fileAsMap_.get(e.getKey());
      if ("-f".equals(asName)) {
        int[] idxRef = new int[] {idx};
        readFilesFromList(idxRef, files, name);
        idx = idxRef[0];
      }
      else if ("-".equals(name)) {
        files.remove(asName);
      }
      else {
        files.put(asName, new FileAsPair(idx++ , name, asName));
      }
    }
    FileAsPair[] results = new FileAsPair[files.size()];
    results = files.values().toArray(results);
    Arrays.sort(results);
    return results;
  }

  private void readFiles() {
    if (fileMap_ != null) {
      return;
    }
    fileMap_ = new TreeMap<String, String>();
    fileAsMap_ = new HashMap<String, String>();
    for (JposEntry.Prop item : (DtvJposEntry) super.getConfig()) {
      String name = item.getName();

      if ((name != null) && item.isOfType(String.class)) {
        if (name.startsWith("dtvFile")) {
          String value = item.getValueAsString();
          if ((value != null) && (value.length() > 0)) {
            if ("dtvFileBase".equals(name)) {
              fileBaseLocation_ = value;
              fileBaseDir_ = new File(lookupI18n(fileBaseLocation_));
            }
            else {
              String key = name.substring("dtvFile".length());
              if (name.endsWith("As")) {
                key = key.substring(0, key.length() - 2);
                if (NumberUtils.isInteger(key)) {
                  fileAsMap_.put(key, value);
                }
              }
              else {
                if (NumberUtils.isInteger(key)) {
                  fileMap_.put(key, value);
                }
              }
            }
          }
        }
      }
    }
  }

  protected void readFilesFromList(int[] idxRef, Map<String, FileAsPair> list, String argListFileName) {
    int idx = idxRef[0];
    try {
      File f = getFile(argListFileName);
      BufferedReader r = null;
      r = FileUtils.getFileReader(f);
      String line = null;
      while ((line = r.readLine()) != null) {
        String name = line.trim();
        if (name.startsWith("-i")) {
          name = name.substring(2).trim();
        }
        if (name.length() == 0) {
          continue;
        }
        String nameAs = name;
        if (name.startsWith("-delete ")) {
          name = name.substring("-delete ".length());
          nameAs = "-delete";
        }
        list.put(nameAs, new FileAsPair(idx++ , name, nameAs));
      }
      idxRef[0] = idx;
    }
    catch (Throwable thr) {
      JposException ex = toJposException(thr);
      hardwareMgr_.logJposException("problem reading from list file '" + argListFileName + "' from "
          + fileBaseLocation_ + " (" + getType().getName() + ")", this, ex);
      idxRef[0] = idx;
    }
  }

  public void startupSubdevices() {
    if (!_NO__HARDWARE()) {
      if (pinpad_ != null) {
        if (pinpad_.startup(getHardwareStartupMessages())) {
          logger_.info(pinpad_.getType() + " started");
        }
        else {
          logger_.warn(pinpad_.getType() + " NOT started");
        }
      }
      if (sigcap_ != null) {
        if (sigcap_.startup(getHardwareStartupMessages())) {
          logger_.info(sigcap_.getType() + " started");
        }
        else {
          logger_.warn(sigcap_.getType() + " NOT started");
        }
      }
      if (msr_ != null) {
        if (msr_.startup(getHardwareStartupMessages())) {
          logger_.info(msr_.getType() + " started");
        }
        else {
          logger_.warn(msr_.getType() + " NOT started");
        }
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  protected void onOpen() {
    waitAfterResetMillis_ = getProperty(PROP_WAIT_AFTER_RESET_MILLIS, Integer.valueOf(500), false).intValue();
    clearOnExit_ = getProperty(PROP_CLEAR_ON_EXIT, Boolean.FALSE, false).booleanValue();
    useBusyMutex_ = getProperty(PROP_USE_BUSY_MUTEX, Boolean.TRUE, false).booleanValue();
    maxErrorCount_ = getProperty(PROP_MAX_ERROR_COUNT, Integer.valueOf(10), false).intValue();
    mutexLogLevel_ =
        Level.toLevel(getProperty(PROP_MUTEX_LOG_LEVEL, DEFAULT_MUTEX_LOG_LEVEL.toString(), false));
    timeout_ = getProperty(PROP_TIMEOUT, Integer.valueOf(DEFAULT_TIMEOUT), false).intValue();
    resetOnError_ = getProperty(PROP_RESET_ON_ERROR, Boolean.FALSE, false).booleanValue();
  }

  /** {@inheritDoc} */
  @Override
  protected void onEnable() {
    // query the device for version information
    //    if (capXVER_) {
    //      String[] versions = sendCommand(HypercomCommands.CMD_VERSION, 1000);
    //      HypercomVersionInfo hvi = HypercomVersionInfo.make(versions);
    //      if (hvi != null) {
    //        deviceState_.setVersionInfo(hvi);
    //        if (msr_ != null) {
    //          msr_.hasRfidCreditReader(hvi.hasRfidReader());
    //        }
    //      }
    //      validateVersions(hvi);
  }

  /** {@inheritDoc} */
  @Override
  protected void setJposEntryKey(String newValue) {
    if (StringUtils.isEmpty(getJposEntryKey())) {
      super.setJposEntryKey(newValue);
    }
    else if (shuttingDown_ && (newValue == null)) {
      super.setJposEntryKey(newValue);
    }
    else if (!getJposEntryKey().equals(newValue) && !StringUtils.isEmpty(newValue)) {
      logger_.warn("different jpos keys configured for " + HypercomFormBuilderDevice.class.getName() + " ("
          + getJposEntryKey() + " and " + newValue + ")");
    }
  }

  //start 08/21  
  /** {@inheritDoc} */
  @Override
  public void appendTimeoutMessage(HardwareStartupMessages argMessages) {

    if (getStateStack().lessThan(StateLevel.INITIALIZED)) {
      super.appendTimeoutMessage(argMessages);
      logger_.warn("timed out with level = " + getStateStack().getLevel());
    }
  }

  public void shutdownTemporary() {

    temporaryShutdown_ = true;
    shutdown();
  }

  /** {@inheritDoc} */
  @Override
  protected void afterShutdown() {
    shuttingDown_ = false;
    writeState();
  }

  protected void flushWorkerQueue(final boolean enableMsr) {
    QueueProcessorThread queueProcessorThread = workerQueueProcessor_;
    if (Thread.currentThread() == queueProcessorThread) {
      throw new IllegalStateException("cannot flush the queue from inside a queued worker");
    }
    try {
      if (queueProcessorThread != null) {
        queueProcessorThread.flush();
      }
      else {
        synchronized (workers_) {
          for (; !workers_.isEmpty();) {
            Runnable element = workers_.take();
            if (element instanceof WaitableWorker) {
              ((WaitableWorker) element).abort();
            }
          }
        }
      }
      // now that the queue is empty, send a "72" to make sure the device is OK
      queueWorkerAndWait(new Runnable() {
        @Override
        public void run() {

          disableMsr();
          if (enableMsr) {
            enableMsr();
          }
        }
      });
    }
    catch (InterruptedException ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
    }
  }

  /** {@inheritDoc} */
  @Override
  public void askQuestion(final QuestionEnum argQuestion, final Map<TypeSafeMapKey<?>, ?> argParameters) {

    if (!canHandle(argQuestion)) {
      throw new IllegalArgumentException("currently unable to handle question " + argQuestion);
    }
    flushWorkerQueue(false);
    queueWorker(new Runnable() {
      @Override
      public void run() {
        if (argQuestion.equals(QuestionEnum.DEBIT_OR_CREDIT)) {
          IDeviceForm form = new CreditDebitForm();
          currentForm_ = form;
          try {
            getStateStack().setLevel(getStateLevelCallback(), HypercomStateLevel.CREDIT_DEBIT);
            sendCommandAndValidate(HypercomCommands.CMD_FORM_REQUEST + FS + FORM_NAME + CREDIT_DEBIT,
                Integer.valueOf(getTimeout()));
          }
          catch (Exception e) {
            logger_.error("CAUGHT EXCEPTION", e);
          }

        }

        currentForm_.askQuestion(argQuestion, argParameters);
        setDirectIOListener(questionResponseListener_);
      }
    });

  }

  /** {@inheritDoc} */
  @Override
  public boolean canHandle(QuestionEnum argQuestion) {

    if (!isPresent()) {
      return false;
    }
    if (currentForm_ == null) {
      logger_.warn("no current form so unable to handle questions");
      return false;
    }
    return currentForm_.canHandle(argQuestion);
  }

  JposListener questionResponseListener_ = new JposListener("QuestionResponseListener", this) {

    @Override
    public void directIOOccurred(DirectIOEvent arg0) {

      logger_.debug("-----------QuestionResponseListener.directIO");
      final IDeviceForm form = currentForm_;
      if (form == null) {
        logger_.error("Got an answer, but no active form");
        return;
      }
      if (form.answerOccurred(getResponseParts(arg0))) {
        unsetDirectIOListener(questionResponseListener_);
        enableMsrNoWait();
      }
      else {
        setDirectIOListener(questionResponseListener_);
      }
    }
  };

  /** {@inheritDoc} */
  @Override
  public void cancelQuestion() {
    logger_.info("in cancelQuestion");
    if (isHandlingQuestion()) {
      try {
        queueWorkerAndWait(new Runnable() {
          @Override
          public void run() {

            unsetDirectIOListener(questionResponseListener_);
            currentForm_.cancelQuestion();
            enableMsr();
          }
        });
      }
      catch (InterruptedException ex) {
        logger_.error("CAUGHT EXCEPTION", ex);
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public boolean isHandlingQuestion() {
    return currentForm_.isHandlingQuestion();
  }

  @Override
  public void disable() {
    disableMsr();
  }

  @Override
  public void enable() {
    enableMsr();
  }

  /** {@inheritDoc} */
  @Override
  public void startPinEntry(final ICreditDebitTenderLineItem argSwipeEvent)
      throws PinPadException {
    FutureTask<Boolean> task = new FutureTask<Boolean>(new Callable<Boolean>() {
      @Override
      public Boolean call()
          throws PinPadException {

        //        disableMsr();
        getStateStack().setLevel(getStateLevelCallback(), HypercomStateLevel.CAPTURING_PIN);
        if (!_NO__HARDWARE()) {

          try {
            pinpad_.startPinEntry(argSwipeEvent);

            // Customization to display cancel to process as credit message
            Object inputEvent = argSwipeEvent.getInputEvent();
            if ((inputEvent instanceof IHardwareInputEvent)
                && (TenderHelper.getInstance().isCreditDebitAmbiguous((IHardwareInputEvent<?>) inputEvent))) {

              sendCommand(
                  HypercomCommands.CMD_UPDATE_DISPLAY + FS + "C2" + "N"
                      + HH.makeTranslatable("_processAsCredit", new IFormattable[0]),
                  Integer.valueOf(getTimeout()));
            }
          }
          catch (PinPadException ex) {
            getStateStack().revertLevel(getStateLevelCallback());
            throw ex;
          }
        }
        return Boolean.TRUE;
      }
    });
    queueWorker(task);
    try {
      task.get();
    }
    catch (ExecutionException ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
      throw PinPadException.toPinPadException(ex.getCause());
    }
    catch (InterruptedException ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
    }
  }

  /** {@inheritDoc} */
  @Override
  public void abortPinEntry() {

    try {
      queueWorkerAndWait(new Runnable() {
        @Override
        public void run() {

          if (!_NO__HARDWARE()) {
            pinpad_.abortPinEntry();
            resetDevice();
          }
          getStateStack().revertLevel(getStateLevelCallback());
          enableMsr();
        }
      });
    }
    catch (InterruptedException ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
    }
  }

  /** {@inheritDoc} */
  @Override
  protected int getDefaultStartupTimeoutMillis() {
    return 0;
  }

  /** {@inheritDoc} */
  @Override
  public boolean hasRfidCreditReader() {
    if (msr_ != null) {
      return msr_.hasRfidCreditReader();
    }
    else {
      return false;
    }
  }

  /*public void setStackLevelDccSelection() {
    try {
      getStateStack().setLevel(getStateLevelCallback(), HypercomStateLevel.DCC_SELECTION);
      sendCommandAndValidate(HypercomCommands.CMD_FORM_REQUEST + FS + FORM_NAME + DCC_FORM,
          Integer.valueOf(getTimeout()));
    }
    catch (Exception e) {
      logger_.error("CAUGHT EXCEPTION", e);
    }
  }

  public void setStackLevelDccConfirm() {
    try {
      getStateStack().setLevel(getStateLevelCallback(), HypercomStateLevel.DCC_CONFIRM);
      sendCommandAndValidate(HypercomCommands.CMD_FORM_REQUEST + FS + FORM_NAME + DCC_CONFIRM,
          Integer.valueOf(getTimeout()));
    }
    catch (Exception e) {
      logger_.error("CAUGHT EXCEPTION", e);
    }
  }

  public void revertStackLevelDccSelection(boolean argWait, final IRetailTransaction argTran) {
    try {
      Runnable w = new Runnable() {

        public void run() {

          if (!_NO__HARDWARE()) {
            sendCommandAndValidate(HypercomCommands.CMD_FORM_REQUEST + FS + FORM_NAME + SWIPE_FORM + FS
                + HypercomCommands.CMD_GLOBAL_PROMPT_1 + "$" + argTran.getTotal().toString(),
                Integer.valueOf(getTimeout()));
          }
          getStateStack().revertLevel(getStateLevelCallback());
          enableMsr();
        }
      };
      if (argWait) {
        queueWorkerAndWait(w);
      }
      else {
        queueWorker(w);
      }
    }
    catch (InterruptedException ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
    }

  }*/
}
