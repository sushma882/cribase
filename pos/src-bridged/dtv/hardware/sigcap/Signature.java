//$Id: Signature.java 216 2012-08-06 14:51:10Z dtvdomain\aabdul $
package dtv.hardware.sigcap;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dtv.hardware.types.HardwareFamilyType;
import dtv.hardware.types.HardwareType;
import dtv.pos.iframework.hardware.AbstractInput;
import dtv.pos.iframework.hardware.IHardwareType;
import dtv.util.ArrayUtils;
import dtv.util.ByteArray;
import dtv.util.ByteUtils;
import dtv.util.EncodingHelper;

/**
 * A captured signature.<br>
 * Copyright (c) 2004 Datavantage Corporation
 * 
 * @author dberkland
 * @created January 28, 2004
 * @version $Revision: 216 $
 */
public class Signature
    extends AbstractInput
    implements ISignature {

  private static final long serialVersionUID = 1L;
  private static final char SINGLE_BYTE_FORMAT = 'a';
  private static final char DOUBLE_BYTE_FORMAT = 'b';
  private static final char FOUR_BYTE_FORMAT = 'd';
  private final int maximumX_, maximumY_;
  private final Point[] points_;
  private String stringData_;
  private final boolean isComplete_;
  private static final int max_coordinate_value = 10000;

  public Signature(boolean argIsComplete, IHardwareType argSourceType, Point[] argPoints,
      boolean argFlipHorizontal, boolean argFlipVertical) {

    super(null, argSourceType, argPoints == null || argPoints.length == 0, null);
    isComplete_ = argIsComplete;
    if (isError()) {

      maximumX_ = 0;
      maximumY_ = 0;
      points_ = new Point[0];
    }
    else {

      maximumX_ = shiftX(argPoints);
      maximumY_ = shiftY(argPoints);

      points_ = flip(maximumX_, maximumY_, argPoints, argFlipHorizontal, argFlipVertical);
    }
  }

  public static Signature forError(IHardwareType argSourceType) {
    return new Signature(true, argSourceType, (Point[]) null, false, false);
  }

  private int shiftY(Point[] argPoints) {

    // create int[2] with int[0]=min, int[1]=max; init to outrageous values
    int[] minMaxPair = new int[] {Integer.MAX_VALUE, Integer.MIN_VALUE};
    // check each point
    for (int i = 0; i < argPoints.length; i++ ) {
      evaluate(minMaxPair, argPoints[i].y);
    }
    // if the min is more than o, shift min to 0
    if (minMaxPair[0] > 0) {
      // shift each point
      for (int i = 0; i < argPoints.length; i++ ) {
        // skip pen-ups
        if (argPoints[i].y > 0) {
          // shift the point
          argPoints[i].y -= minMaxPair[0];
        }
      }
      // shift the max
      minMaxPair[1] -= minMaxPair[0];
    }
    // return the max
    return minMaxPair[1];
  }

  private int shiftX(Point[] argPoints) {

    // create int[2] with int[0]=min, int[1]=max; init to outrageous values
    int[] minMaxPair = new int[] {Integer.MAX_VALUE, Integer.MIN_VALUE};
    // check each point
    for (int i = 0; i < argPoints.length; i++ ) {
      evaluate(minMaxPair, argPoints[i].x);
    }
    // if the min is more than o, shift min to 0
    if (minMaxPair[0] > 0) {
      // shift each point
      for (int i = 0; i < argPoints.length; i++ ) {
        // skip pen-ups
        if (argPoints[i].x > 0) {
          // shift the point
          argPoints[i].x -= minMaxPair[0];
        }
      }
      // shift the max
      minMaxPair[1] -= minMaxPair[0];
    }
    // return the max
    return minMaxPair[1];
  }

  private void evaluate(int[] argMinMaxPair, int argValue) {

    // skip pen ups
    if (argValue < 0 || argValue > max_coordinate_value) {
      return;
    }
    // check if this point is a new max
    if (argValue > argMinMaxPair[1]) {
      argMinMaxPair[1] = argValue;
    }
    // check if this point is a new min
    if (argValue < argMinMaxPair[0]) {
      argMinMaxPair[0] = argValue;
    }
  }

  private Point[] flip(int argMaximumX, int argMaximumY, Point[] argPoints, boolean argFlipHorizontal,
      boolean argFlipVertical) {

    Point[] results = new Point[argPoints.length];
    for (int i = 0; i < argPoints.length; i++ ) {
      if (argPoints[i].x < max_coordinate_value && argPoints[i].y < max_coordinate_value) {
        results[i] = argPoints[i];
      }
      else
        results[i] = new Point(-1, -1);
    }
    if (argFlipHorizontal) {
      results = flipHorizontal(results, argMaximumY);
    }
    if (argFlipVertical) {
      results = flipVertical(results, argMaximumX);
    }
    return results;
  }

  private Point[] flipHorizontal(Point[] argPoints, int argMaximumY) {

    Point[] results = new Point[argPoints.length];
    for (int i = 0; i < argPoints.length; i++ ) {
      int x = argPoints[i].x;
      int y = argPoints[i].y;
      if (x >= 0 && y >= 0) {
        y = argMaximumY - y;
      }
      results[i] = new Point(x, y);
    }
    return results;
  }

  private Point[] flipVertical(Point[] argPoints, int argMaximumX) {

    Point[] results = new Point[argPoints.length];
    for (int i = 0; i < argPoints.length; i++ ) {
      int x = argPoints[i].x;
      int y = argPoints[i].y;
      if (x >= 0 && y >= 0) {
        x = argMaximumX - x;
      }
      results[i] = new Point(x, y);
    }
    return results;
  }

  /**
   * String constructor. Takes the results from {@link #getData()} from another <tt>Signature</tt>
   * object.
   * 
   * @see #getData()
   * @param argString String
   */
  public Signature(String argString) {

    super(null, HardwareType.forUse(HardwareFamilyType.SIG_CAP, ""), false, null);

    try {
      isComplete_ = true;
      List<Point> points = decode(argString);
      Point p = points.get(0);
      maximumX_ = p.x;
      maximumY_ = p.y;
      // create storage for the rest of the points
      points_ = ArrayUtils.remove(points.toArray(new Point[points.size()]), 0);
    }
    catch (NumberFormatException ex) {
      throw new IllegalArgumentException('"' + argString + '"');
    }
  }

  private static List<Point> decode(String argString) {

    final char mode = argString.charAt(0);
    switch (mode) {
      case SINGLE_BYTE_FORMAT:
      case DOUBLE_BYTE_FORMAT:
      case FOUR_BYTE_FORMAT:
        break;
      default:
        // this parsing support for the legacy format (about twice as long)
        String[] sa = argString.split(":");
        List<Point> points = new ArrayList<Point>();
        for (String s : sa) {
          points.add(parsePoint(s));
        }
        return points;
    }

    byte[] bytes = EncodingHelper.decodeToBytes(argString.substring(1), true);
    List<Point> results = new ArrayList<Point>();
    switch (mode) {
      case SINGLE_BYTE_FORMAT:
        for (int index = 0; index < bytes.length;) {
          Point p = new Point();
          p.x = getByte(bytes, index);
          index += 1;
          p.y = getByte(bytes, index);
          index += 1;
          results.add(p);
        }
        break;
      case DOUBLE_BYTE_FORMAT:
        for (int index = 0; index < bytes.length;) {
          Point p = new Point();
          p.x = toShortInt(bytes, index);
          index += 2;
          p.y = toShortInt(bytes, index);
          index += 2;
          results.add(p);
        }
        break;
      case FOUR_BYTE_FORMAT:
        for (int index = 0; index < bytes.length;) {
          Point p = new Point();
          p.x = ByteUtils.toInt(bytes, index);
          index += 4;
          p.y = ByteUtils.toInt(bytes, index);
          index += 4;
          results.add(p);
        }
        break;
      default:
        throw new IllegalArgumentException(String.valueOf(mode));
    }
    return results;
  }

  private static final int getByte(byte[] argBytes, int argStart) {

    int i = ByteUtils.ubyteToInt(argBytes[argStart]);
    if (i == 0xFF) {
      return -1;
    }
    return i;
  }

  private static final int toShortInt(byte[] argBytes, int argStart) {

    int i = ByteUtils.ubyteToInt(argBytes[argStart]);
    i |= ByteUtils.ubyteToInt(argBytes[argStart + 1]) << 8;
    if (i == 0xFFFF) {
      return -1;
    }
    return i;
  }

  private static Point parsePoint(String argString) {

    String[] sa = argString.split(",");
    return new Point(Integer.parseInt(sa[0]), Integer.parseInt(sa[1]));
  }

  /**
   * Gets the signature as a string that can be persisted. Example format is "300,500:30,4:32,5"
   * where colons seperate points, and commas seperate x from y for each point. The first point is
   * the maximum bounds for the device. The results of this method can be passed to the string
   * constructor to recreate a signature.
   * 
   * @return string representation of the signature data
   */
  @Override
  public String getData() {

    if (stringData_ == null) {
      stringData_ = toStringData(maximumX_, maximumY_, points_);
    }
    return stringData_;
  }

  public static String toStringData(int argMaximumX, int argMaximumY, Point[] argPoints) {

    ByteArray ba = new ByteArray();
    final char id;
    if (argMaximumX > Short.MAX_VALUE || argMaximumY > Short.MAX_VALUE) {
      id = FOUR_BYTE_FORMAT;
      ba.appendInt(argMaximumX);
      ba.appendInt(argMaximumY);
      for (Point p : argPoints) {
        ba.appendInt(p.x);
        ba.appendInt(p.y);
      }
    }
    else if (argMaximumX > Byte.MAX_VALUE || argMaximumY > Byte.MAX_VALUE) {
      id = DOUBLE_BYTE_FORMAT;
      ba.appendShort(argMaximumX);
      ba.appendShort(argMaximumY);
      for (Point p : argPoints) {
        ba.appendShort(p.x);
        ba.appendShort(p.y);
      }
    }
    else {
      id = SINGLE_BYTE_FORMAT;
      ba.appendByte(argMaximumX);
      ba.appendByte(argMaximumY);
      for (Point p : argPoints) {
        ba.appendByte(p.x);
        ba.appendByte(p.y);
      }
    }
    return id + EncodingHelper.encodeBytes(ba.toArray(), true);
  }

  /** {@inheritDoc} */
  public int getMaximumX() {

    return maximumX_;
  }

  /** {@inheritDoc} */
  public int getMaximumY() {

    return maximumY_;
  }

  /** {@inheritDoc} */
  public Point[] getPoints() {

    if (points_ == null) {
      return new Point[0];
    }
    Point[] rtn = new Point[points_.length];
    System.arraycopy(points_, 0, rtn, 0, points_.length);
    return rtn;
  }

  /** {@inheritDoc} */
  public boolean isComplete() {

    return isComplete_;
  }

  /**
   * Gets a textual description of the object
   * @return a textual description of the object
   */
  @Override
  public String toString() {

    StringBuilder sb = new StringBuilder();
    sb.append(getClass().getName());
    sb.append("[isError=").append(isError());
    sb.append(",isComplete=").append(isComplete_);
    sb.append(",sourceType=").append(getSourceType());
    sb.append(",pointsIsNull=").append(points_ == null);
    if (points_ != null) {
      sb.append(",pointCount=").append(points_.length);
    }
    sb.append(",maximumX=").append(maximumX_);
    sb.append(",maximumY=").append(maximumY_);
    sb.append("]");
    return sb.toString();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object argObj) {

    if (!(argObj instanceof Signature)) {
      return false;
    }
    Signature other = (Signature) argObj;
    if (isComplete_ != other.isComplete_)
      return false;
    if (maximumX_ != other.maximumX_)
      return false;
    if (maximumY_ != other.maximumY_)
      return false;
    if (!equivalentHardwareTypes(this.getSourceType(), other.getSourceType()))
      return false;
    if (!Arrays.equals(points_, other.points_))
      return false;
    return true;
  }

  private static boolean equivalentHardwareTypes(IHardwareType t1, IHardwareType t2) {
    // handles both null 
    if (t1 == null) {
      return (t2 == null);
    }
    if (t2 == null) {
      return false;
    }
    //neither are null at this point
    return t1.getName().equals(t2.getName());
  }
}
