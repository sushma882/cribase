//$Id: EventedPricingTransaction.java 777 2014-03-27 02:15:29Z suz.jliu $
package dtv.xst.pricing;

import static dtv.data2.access.ModelEventor.PRIVILEGED_EVENT_DESCRIPTOR_NAME;
import static dtv.event.EventManager.registerEventHandler;

import java.util.*;

import org.apache.log4j.Logger;

import dtv.data2.access.*;
import dtv.event.*;
import dtv.pricing2.PricingDeal;
import dtv.util.DateUtils;
import dtv.xst.dao.crm.ICustomerAffiliation;
import dtv.xst.dao.crm.IParty;
import dtv.xst.dao.doc.IDocumentLineItem;
import dtv.xst.dao.hrs.IEmployee;
import dtv.xst.dao.prc.DealId;
import dtv.xst.dao.trl.*;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.pricing.coupon.CouponValueConfig;

/**
 * The Xstore way to tie the addition, removal, and changes to the transaction to the shadow
 * transaction in the pricing world.<br>
 * 
 * Copyright (c) 2006 Datavantage Corporation
 * 
 * @author gfischer
 * @version $Revision: 777 $
 * @created Feb 28, 2006
 */
public class EventedPricingTransaction
    extends EventHandler
    implements dtv.pricing2.Transaction<IRetailTransactionLineItem, XSTPricingDescriptor, IPosTransaction> {

  private static final Logger logger_ = Logger.getLogger(EventedPricingTransaction.class);
  private static final long serialVersionUID = -6980832326498329037L;

  protected dtv.pricing2.Transaction<IRetailTransactionLineItem, XSTPricingDescriptor, IPosTransaction> delegate_;

  private final IPosTransaction trans_;

  public EventedPricingTransaction(IPosTransaction trans) {
    delegate_ = XSTPricingAdapter.getInstance().createTransaction(trans, new Date());
    trans_ = trans;

    EventDescriptor privilegedDescriptor = new EventDescriptor(PRIVILEGED_EVENT_DESCRIPTOR_NAME, trans);
    registerEventHandler(this, privilegedDescriptor);
  }

  // Delegate methods.
  @Override
  public void addItem(IRetailTransactionLineItem argItem) {
    delegate_.addItem(argItem);
  }

  /** {@inheritDoc} */
  @Override
  public void addTarget(String argTarget) {
    delegate_.addTarget(argTarget);
  }

  /** {@inheritDoc} */
  @Override
  public void addTransactionDeal(Object argDealKey, PricingDeal<XSTPricingDescriptor> argDeal) {
    delegate_.addTransactionDeal(argDealKey, argDeal);
  }

  @Override
  public void addTrigger(String argTrigger) {
    delegate_.addTrigger(argTrigger);
  }

  /** {@inheritDoc} */
  @Override
  public void clearTargets() {
    delegate_.clearTargets();
  }

  /** {@inheritDoc} */
  @Override
  public void debug(StringBuffer argSb) {
    delegate_.debug(argSb);
  }

  // This method made possible by the letter 'A' and method return narrowing.
  @Override
  public dtv.pricing2.Result<IRetailTransactionLineItem, XSTPricingDescriptor> getBestDeals() {
    //refresh all the item in case any price change was performed
    refreshItems();
    return delegate_.getBestDeals();
  }

  @Override
  public IPosTransaction getNativeTransaction() {
    return delegate_.getNativeTransaction();
  }

  /** {@inheritDoc} */
  @Override
  public List<PricingDeal<XSTPricingDescriptor>> getPossibleDeals() {
    return delegate_.getPossibleDeals();
  }

  @Override
  public dtv.pricing2.Result<IRetailTransactionLineItem, XSTPricingDescriptor> getSubstituteDeals() {
    refreshItems();
    return delegate_.getSubstituteDeals();
  }

  @Override
  public dtv.pricing2.Result<IRetailTransactionLineItem, XSTPricingDescriptor> getThresholdDeals() {
    refreshItems();
    return delegate_.getThresholdDeals();
  }

  /** {@inheritDoc} */
  @Override
  public boolean isDirty() {
    return delegate_.isDirty();
  }

  /** {@inheritDoc} */
  @Override
  public void reestablishPricingAdapter(
      dtv.pricing2.PricingAdapter<IRetailTransactionLineItem, XSTPricingDescriptor, IPosTransaction> argAdapter) {

    // It appears that items do not come back as expected through serialization.
    // this should, instead of
    delegate_.reestablishPricingAdapter(argAdapter);
    EventDescriptor privilegedDescriptor =
        new EventDescriptor(PRIVILEGED_EVENT_DESCRIPTOR_NAME, delegate_.getNativeTransaction());
    registerEventHandler(this, privilegedDescriptor);
  }

  @Override
  public void refreshItem(IRetailTransactionLineItem argItem) {
    if (argItem instanceof ICouponLineItem) {
      if (argItem.getVoid()) {
        handleRemoveCouponLineItem((ICouponLineItem) argItem);
      }
      else {
        handleAddCouponLineItem((ICouponLineItem) argItem);
      }
    }
    else {
      delegate_.refreshItem(argItem);
    }
  }

  @Override
  public void removeItem(IRetailTransactionLineItem argItem) {
    delegate_.removeItem(argItem);
    logger_.info("Removing item from transaction");
  }

  /** {@inheritDoc} */
  @Override
  public void removeTarget(String argTarget) {
    delegate_.removeTarget(argTarget);
  }

  /** {@inheritDoc} */
  @Override
  public void removeTransactionDeal(Object argDealKey) {
    delegate_.removeTransactionDeal(argDealKey);
  }

  @Override
  public void removeTrigger(String argTrigger) {
    delegate_.removeTrigger(argTrigger);
  }
  
  /** {@inheritDoc} */
  @Override
  public PricingDeal<XSTPricingDescriptor>[] getEligibleConditionalDeals() {
    return delegate_.getEligibleConditionalDeals();
  }

  protected final IPosTransaction getTransaction() {
    return trans_;
  }

  /** {@inheritDoc} */
  @Override
  protected void handle(Event argEvent) {
    handle(argEvent, new HashSet<IRetailTransactionLineItem>());
  }

  protected void handle(Event argEvent, Set<IRetailTransactionLineItem> argRefreshed) {
    Object eventName = argEvent.getName();
    if (argEvent.getPayload() instanceof EventList) {
      EventList events = (EventList) argEvent.getPayload();
      List<IEventFinder> elements = events.getElements();
      for (IEventFinder finder : elements) {
        handle(finder.getBaseEvent(), argRefreshed);
      }
    }
    else if (eventName == IPosTransaction.ADD_RETAILTRANSACTIONLINEITEMS) {
      handleAddRetailTranLineItem(argEvent);
    }
    else if (eventName == IPosTransaction.REMOVE_RETAILTRANSACTIONLINEITEMS) {
      handleRemoveRetailTranLineItem(argEvent);
    }
    else if (eventName == IRetailTransaction.SET_CUSTOMERPARTY) {
      handleAddCustomer(argEvent);
    }
    else {
      IRetailTransactionLineItem lineItem = null;
      if ((lineItem = argEvent.getSource(IRetailTransactionLineItem.class)) != null) {
        if (!argRefreshed.contains(lineItem)) {
          refreshItem(lineItem);
          argRefreshed.add(lineItem);
        }
      }
    }
  }

  protected void handleAddCouponLineItem(ICouponLineItem couponLineItem) {
    //Add trigger for every coupon line item added to this transaction.
    addTrigger("COUPON:" + couponLineItem.getCouponTypeCode() + ":" + couponLineItem.getCouponId());
    if (couponLineItem.getValueCode() != null) {
      CouponValueConfig config =
          XSTPricingAdapter.getInstance().getManufacturersCouponValue(couponLineItem.getValueCode());
      if (config != null) {
        PricingDeal<XSTPricingDescriptor> deal = config.makeDeal(couponLineItem);
        if (deal != null) {
          addTransactionDeal(deal.getNativeDeal().getDealId(), deal);
        }
      }
    }
  }

  /**
   * Handles modifying pricing attributes based on the customer that has been added or removed from
   * the retail transaction.
   * 
   * @param argEvent the event posted when a customer was set on the transaction
   */
  protected void handleAddCustomer(Event argEvent) {
    IParty newCustomer = (IParty) argEvent.getPayload();
    if (newCustomer == null) {
      // Remove all the customer group affiliations as triggers
      removeTrigger("CUSTGROUP");
    }
    else {
      // Also add the customer affiliation groups
      for (ICustomerAffiliation aff : newCustomer.getCustomerGroups()) {
        addTrigger("CUSTGROUP:" + aff.getCustomerGroupId());
      }

      // Add employee trigger
      /* @todo This should not be hard-coded, as it can be overridden by a hook method in 
       * EmployeeHelper.  But in the absence of a coherent container mechanism, shared functionality
       * refactoring, or even a mutual dependency between the artificially separated pos and dtx
       * projects, the performance gain is probably worth the hack. */
      if ("EMPLOYEE".equalsIgnoreCase(newCustomer.getPartyTypeCode())) {
        IEmployee emp = searchEmployeeByPartyId(newCustomer.getPartyId());
        if (emp != null) {
          Date terminatedDate = emp.getTerminatedDate();
          if ((terminatedDate == null) || terminatedDate.after(DateUtils.getNewDate())) {
            addTrigger("CUSTGROUP:EMPLOYEE");
          }
        }
      }
    }
    /* Add or remove from the current transactional space any deals that are or aren't specifically
     * targeted for this customer. */
    handleDealTargeting(newCustomer);
  }

  protected void handleAddDocumentLineItem(IDocumentLineItem docLineItem) {
    //Add trigger for every document line item added to this transaction.
    addTrigger("COUPON:" + docLineItem.getDocumentType() + ":" + docLineItem.getDocumentId());
  }

  protected void handleAddRetailTranLineItem(Event argEvent) {
    if (argEvent.getPayload() instanceof ICouponLineItem) {
      handleAddCouponLineItem((ICouponLineItem) argEvent.getPayload());
    }
    else if (argEvent.getPayload() instanceof IDocumentLineItem) {
      handleAddDocumentLineItem((IDocumentLineItem) argEvent.getPayload());
    }
    else if (argEvent.getPayload() instanceof IRetailTransactionLineItem) {
      // The integration item adapter will determine eligibility for addition.
      addItem((IRetailTransactionLineItem) argEvent.getPayload());
    }
  }

  /**
   * Removes from the current deal space any customer-targeted deals for which the specified party
   * is not eligible. Specifically, the following rules will be applied:<br>
   * <ul>
   * <li>If there is no party, then only non-targeted deals in the space will be recognized.</li>
   * <li>If the party does not explicitly identify any targeted deals, then that party's deal
   * eligibility is limited to only non-targeted deals.</li>
   * <li>If the party identifies any targeted deals, then that party's deal eligibility will include
   * all non-targeted deals in addition to all targeted deals identified for that party.</li>
   * </ul>
   * 
   * @param argParty the new party assigned to the current transaction; <code>null</code> if no
   * party is assigned or if the current party is being unassigned
   */
  protected void handleDealTargeting(IParty argParty) {
    if (argParty != null) {
      for (DealId targetedDealId : argParty.getTargetedDeals()) {
        addTarget(targetedDealId.getDealId());
      }
    }
    else {
      clearTargets();
    }
  }

  protected void handleRemoveCouponLineItem(ICouponLineItem couponLineItem) {
    //Add trigger for every coupon line item added to this transaction.
    removeTrigger("COUPON:" + couponLineItem.getCouponTypeCode() + ":" + couponLineItem.getCouponId());
    if (couponLineItem.getValueCode() != null) {
      CouponValueConfig config =
          XSTPricingAdapter.getInstance().getManufacturersCouponValue(couponLineItem.getValueCode());
      if (config != null) {
        removeTransactionDeal(config.getDealId(couponLineItem.getManufacturerId()));
      }
    }
  }

  protected void handleRemoveDocumentLineItem(IDocumentLineItem docLineItem) {
    //Add trigger for every document line item added to this transaction.
    removeTrigger("COUPON:" + docLineItem.getDocumentType() + ":" + docLineItem.getDocumentId());
  }

  protected void handleRemoveRetailTranLineItem(Event argEvent) {
    IRetailTransactionLineItem lineItem;
    if (argEvent.getPayload() instanceof ICouponLineItem) {
      handleRemoveCouponLineItem((ICouponLineItem) argEvent.getPayload());
    }
    else if (argEvent.getPayload() instanceof IDocumentLineItem) {
      handleRemoveDocumentLineItem((IDocumentLineItem) argEvent.getPayload());
    }
    else if (argEvent.getPayload() instanceof IRetailTransactionLineItem) {
      lineItem = (IRetailTransactionLineItem) argEvent.getPayload();
      removeItem(lineItem);
    }
  }

  /**
   * refresh all the item in case any price change was performed
   */
  protected void refreshItems() {
    if (trans_ != null) {
      List<IRetailTransactionLineItem> saleLines = trans_.getRetailTransactionLineItems();

      if ((saleLines != null) && !saleLines.isEmpty()) {
        for (IRetailTransactionLineItem lineItem : trans_.getRetailTransactionLineItems()) {
          refreshItem(lineItem);
        }
      }
    }
  }

  protected IEmployee searchEmployeeByPartyId(long argPartyId) {
    IQueryKey<IEmployee> EMPLOYEE_BY_PARTY_ID =
        new QueryKey<IEmployee>("EMPLOYEE_BY_PARTY_ID", IEmployee.class);

    Map<String, Object> params = new HashMap<String, Object>();
    params.put("argPartyId", Long.valueOf(argPartyId));
    List<IEmployee> found;
    try {
      found = DataFactory.getObjectByQuery(EMPLOYEE_BY_PARTY_ID, params);
    }
    catch (ObjectNotFoundException ex) {
      logger_.info("No employee found", ex);
      return null;
    }
    catch (Exception ex) {
      logger_.error(
          "An unexpected exception occurred while looking up employee with party id: " + argPartyId, ex);
      return null;
    }

    if ((found == null) || found.isEmpty()) {
      return null;
    }
    else {
      IEmployee emp = found.get(0);
      if (found.size() > 1) {
        logger_.warn("more than one employee with partyId " + argPartyId + " using the one with employeeId="
            + emp.getEmployeeId());
      }
      return emp;
    }
  }
}
