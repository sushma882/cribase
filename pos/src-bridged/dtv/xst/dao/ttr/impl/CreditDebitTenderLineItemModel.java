package dtv.xst.dao.ttr.impl;

import cri.pos.common.CriConfigurationMgr;
import dtv.data2.access.IDataModel;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Auto Generated Model<br>
 * Copyright (c) 2014 Datavantage Corporation
 *
 * @author DAOGen
 */
@SuppressWarnings("all")
public class CreditDebitTenderLineItemModel 
  extends TenderLineItemModel
  implements dtv.xst.dao.ttr.ICreditDebitTenderLineItem {

  // Fix serialization compatability based on the name of the DAO
  private static final long serialVersionUID = 1871830094L;

  private transient dtv.event.Eventor events_ = new dtv.data2.access.ModelEventor(this);

  private transient boolean alreadyInStart_ = false;
  private transient boolean alreadyInCommit_ = false;

  private IDataModel myExtension_;

  @Override
  public void initDAO() {
    setDAO(new CreditDebitTenderLineItemDAO());
  }

  /**
   * Return the data access object associated with this data model
   * @return our DAO, properly casted
   */
  private CreditDebitTenderLineItemDAO getDAO_() {
     return (CreditDebitTenderLineItemDAO)daoImpl_;
  }

  /**
   * Gets the value of the create_date field.
   * @return The value of the field.
   */
  public Date getCreateDate() {
    return getDAO_().getCreateDate();
  }

  /**
   * Sets the value of the create_date field.
   * @param argCreateDate The new value for the field.
   */
  public void setCreateDate(Date argCreateDate) {
    if( setCreateDate_noev(argCreateDate) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_CREATEDATE, argCreateDate);
    }
  }
  public boolean setCreateDate_noev(Date argCreateDate) {
    boolean ev_postable = false;
    if( (getDAO_().getCreateDate() == null && argCreateDate != null ) ||
        (getDAO_().getCreateDate() != null && !getDAO_().getCreateDate().equals(argCreateDate)) ) {
      getDAO_().setCreateDate( argCreateDate );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the create_user_id field.
   * @return The value of the field.
   */
  public String getCreateUserId() {
    return getDAO_().getCreateUserId();
  }

  /**
   * Sets the value of the create_user_id field.
   * @param argCreateUserId The new value for the field.
   */
  public void setCreateUserId(String argCreateUserId) {
    if( setCreateUserId_noev(argCreateUserId) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_CREATEUSERID, argCreateUserId);
    }
  }
  public boolean setCreateUserId_noev(String argCreateUserId) {
    boolean ev_postable = false;
    if( (getDAO_().getCreateUserId() == null && argCreateUserId != null ) ||
        (getDAO_().getCreateUserId() != null && !getDAO_().getCreateUserId().equals(argCreateUserId)) ) {
      getDAO_().setCreateUserId( argCreateUserId );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the update_date field.
   * @return The value of the field.
   */
  public Date getUpdateDate() {
    return getDAO_().getUpdateDate();
  }

  /**
   * Sets the value of the update_date field.
   * @param argUpdateDate The new value for the field.
   */
  public void setUpdateDate(Date argUpdateDate) {
    if( setUpdateDate_noev(argUpdateDate) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_UPDATEDATE, argUpdateDate);
    }
  }
  public boolean setUpdateDate_noev(Date argUpdateDate) {
    boolean ev_postable = false;
    if( (getDAO_().getUpdateDate() == null && argUpdateDate != null ) ||
        (getDAO_().getUpdateDate() != null && !getDAO_().getUpdateDate().equals(argUpdateDate)) ) {
      getDAO_().setUpdateDate( argUpdateDate );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the update_user_id field.
   * @return The value of the field.
   */
  public String getUpdateUserId() {
    return getDAO_().getUpdateUserId();
  }

  /**
   * Sets the value of the update_user_id field.
   * @param argUpdateUserId The new value for the field.
   */
  public void setUpdateUserId(String argUpdateUserId) {
    if( setUpdateUserId_noev(argUpdateUserId) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_UPDATEUSERID, argUpdateUserId);
    }
  }
  public boolean setUpdateUserId_noev(String argUpdateUserId) {
    boolean ev_postable = false;
    if( (getDAO_().getUpdateUserId() == null && argUpdateUserId != null ) ||
        (getDAO_().getUpdateUserId() != null && !getDAO_().getUpdateUserId().equals(argUpdateUserId)) ) {
      getDAO_().setUpdateUserId( argUpdateUserId );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the acct_nbr field.
   * @return The value of the field.
   */
  protected String getAccountNumberImpl() {
    return getDAO_().getAccountNumber();
  }

  /**
   * Sets the value of the acct_nbr field.
   * @param argAccountNumber The new value for the field.
   */
  protected void setAccountNumberImpl(String argAccountNumber) {
    if( setAccountNumber_noev(argAccountNumber) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_ACCOUNTNUMBER, argAccountNumber);
    }
  }
  public boolean setAccountNumber_noev(String argAccountNumber) {
    boolean ev_postable = false;
    if( (getDAO_().getAccountNumber() == null && argAccountNumber != null ) ||
        (getDAO_().getAccountNumber() != null && !getDAO_().getAccountNumber().equals(argAccountNumber)) ) {
      getDAO_().setAccountNumber( argAccountNumber );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the adjudication_code field.
   * @return The value of the field.
   */
  public String getAdjudicationCode() {
    return getDAO_().getAdjudicationCode();
  }

  /**
   * Sets the value of the adjudication_code field.
   * @param argAdjudicationCode The new value for the field.
   */
  public void setAdjudicationCode(String argAdjudicationCode) {
    if( setAdjudicationCode_noev(argAdjudicationCode) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_ADJUDICATIONCODE, argAdjudicationCode);
    }
  }
  public boolean setAdjudicationCode_noev(String argAdjudicationCode) {
    boolean ev_postable = false;
    if( (getDAO_().getAdjudicationCode() == null && argAdjudicationCode != null ) ||
        (getDAO_().getAdjudicationCode() != null && !getDAO_().getAdjudicationCode().equals(argAdjudicationCode)) ) {
      getDAO_().setAdjudicationCode( argAdjudicationCode );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the auth_mthd_code field.
   * @return The value of the field.
   */
  public String getAuthorizationMethodCode() {
    return getDAO_().getAuthorizationMethodCode();
  }

  /**
   * Sets the value of the auth_mthd_code field.
   * @param argAuthorizationMethodCode The new value for the field.
   */
  public void setAuthorizationMethodCode(String argAuthorizationMethodCode) {
    if( setAuthorizationMethodCode_noev(argAuthorizationMethodCode) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_AUTHORIZATIONMETHODCODE, argAuthorizationMethodCode);
    }
  }
  public boolean setAuthorizationMethodCode_noev(String argAuthorizationMethodCode) {
    boolean ev_postable = false;
    if( (getDAO_().getAuthorizationMethodCode() == null && argAuthorizationMethodCode != null ) ||
        (getDAO_().getAuthorizationMethodCode() != null && !getDAO_().getAuthorizationMethodCode().equals(argAuthorizationMethodCode)) ) {
      getDAO_().setAuthorizationMethodCode( argAuthorizationMethodCode );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the auth_nbr field.
   * @return The value of the field.
   */
  public String getAuthorizationCode() {
    return getDAO_().getAuthorizationCode();
  }

  /**
   * Sets the value of the auth_nbr field.
   * @param argAuthorizationCode The new value for the field.
   */
  public void setAuthorizationCode(String argAuthorizationCode) {
    if( setAuthorizationCode_noev(argAuthorizationCode) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_AUTHORIZATIONCODE, argAuthorizationCode);
    }
  }
  public boolean setAuthorizationCode_noev(String argAuthorizationCode) {
    boolean ev_postable = false;
    if( (getDAO_().getAuthorizationCode() == null && argAuthorizationCode != null ) ||
        (getDAO_().getAuthorizationCode() != null && !getDAO_().getAuthorizationCode().equals(argAuthorizationCode)) ) {
      getDAO_().setAuthorizationCode( argAuthorizationCode );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the bank_reference_number field.
   * @return The value of the field.
   */
  public String getBankReferenceNumber() {
    return getDAO_().getBankReferenceNumber();
  }

  /**
   * Sets the value of the bank_reference_number field.
   * @param argBankReferenceNumber The new value for the field.
   */
  public void setBankReferenceNumber(String argBankReferenceNumber) {
    if( setBankReferenceNumber_noev(argBankReferenceNumber) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_BANKREFERENCENUMBER, argBankReferenceNumber);
    }
  }
  public boolean setBankReferenceNumber_noev(String argBankReferenceNumber) {
    boolean ev_postable = false;
    if( (getDAO_().getBankReferenceNumber() == null && argBankReferenceNumber != null ) ||
        (getDAO_().getBankReferenceNumber() != null && !getDAO_().getBankReferenceNumber().equals(argBankReferenceNumber)) ) {
      getDAO_().setBankReferenceNumber( argBankReferenceNumber );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the customer_name field.
   * @return The value of the field.
   */
  public String getCustomerName() {
    return getDAO_().getCustomerName();
  }

  /**
   * Sets the value of the customer_name field.
   * @param argCustomerName The new value for the field.
   */
  public void setCustomerName(String argCustomerName) {
    if( setCustomerName_noev(argCustomerName) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_CUSTOMERNAME, argCustomerName);
    }
  }
  public boolean setCustomerName_noev(String argCustomerName) {
    boolean ev_postable = false;
    if( (getDAO_().getCustomerName() == null && argCustomerName != null ) ||
        (getDAO_().getCustomerName() != null && !getDAO_().getCustomerName().equals(argCustomerName)) ) {
      getDAO_().setCustomerName( argCustomerName );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the entry_mthd_code field.
   * @return The value of the field.
   */
  public String getEntryMethodCode() {
    return getDAO_().getEntryMethodCode();
  }

  /**
   * Sets the value of the entry_mthd_code field.
   * @param argEntryMethodCode The new value for the field.
   */
  public void setEntryMethodCode(String argEntryMethodCode) {
    if( setEntryMethodCode_noev(argEntryMethodCode) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_ENTRYMETHODCODE, argEntryMethodCode);
    }
  }
  public boolean setEntryMethodCode_noev(String argEntryMethodCode) {
    boolean ev_postable = false;
    if( (getDAO_().getEntryMethodCode() == null && argEntryMethodCode != null ) ||
        (getDAO_().getEntryMethodCode() != null && !getDAO_().getEntryMethodCode().equals(argEntryMethodCode)) ) {
      getDAO_().setEntryMethodCode( argEntryMethodCode );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the ps2000 field.
   * @return The value of the field.
   */
  public String getPs2000() {
    return getDAO_().getPs2000();
  }

  /**
   * Sets the value of the ps2000 field.
   * @param argPs2000 The new value for the field.
   */
  public void setPs2000(String argPs2000) {
    if( setPs2000_noev(argPs2000) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_PS2000, argPs2000);
    }
  }
  public boolean setPs2000_noev(String argPs2000) {
    boolean ev_postable = false;
    if( (getDAO_().getPs2000() == null && argPs2000 != null ) ||
        (getDAO_().getPs2000() != null && !getDAO_().getPs2000().equals(argPs2000)) ) {
      getDAO_().setPs2000( argPs2000 );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the mediaissuer_id field.
   * @return The value of the field.
   */
  public String getMediaIssuerId() {
    return getDAO_().getMediaIssuerId();
  }

  /**
   * Sets the value of the mediaissuer_id field.
   * @param argMediaIssuerId The new value for the field.
   */
  public void setMediaIssuerId(String argMediaIssuerId) {
    if( setMediaIssuerId_noev(argMediaIssuerId) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_MEDIAISSUERID, argMediaIssuerId);
    }
  }
  public boolean setMediaIssuerId_noev(String argMediaIssuerId) {
    boolean ev_postable = false;
    if( (getDAO_().getMediaIssuerId() == null && argMediaIssuerId != null ) ||
        (getDAO_().getMediaIssuerId() != null && !getDAO_().getMediaIssuerId().equals(argMediaIssuerId)) ) {
      getDAO_().setMediaIssuerId( argMediaIssuerId );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the personal_id_ref_nbr field.
   * @return The value of the field.
   */
  public String getPersonalIdReferenceNumber() {
    return getDAO_().getPersonalIdReferenceNumber();
  }

  /**
   * Sets the value of the personal_id_ref_nbr field.
   * @param argPersonalIdReferenceNumber The new value for the field.
   */
  public void setPersonalIdReferenceNumber(String argPersonalIdReferenceNumber) {
    if( setPersonalIdReferenceNumber_noev(argPersonalIdReferenceNumber) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_PERSONALIDREFERENCENUMBER, argPersonalIdReferenceNumber);
    }
  }
  public boolean setPersonalIdReferenceNumber_noev(String argPersonalIdReferenceNumber) {
    boolean ev_postable = false;
    if( (getDAO_().getPersonalIdReferenceNumber() == null && argPersonalIdReferenceNumber != null ) ||
        (getDAO_().getPersonalIdReferenceNumber() != null && !getDAO_().getPersonalIdReferenceNumber().equals(argPersonalIdReferenceNumber)) ) {
      getDAO_().setPersonalIdReferenceNumber( argPersonalIdReferenceNumber );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the personal_id_req_typcode field.
   * @return The value of the field.
   */
  public String getPersonalIdRequiredTypeCode() {
    return getDAO_().getPersonalIdRequiredTypeCode();
  }

  /**
   * Sets the value of the personal_id_req_typcode field.
   * @param argPersonalIdRequiredTypeCode The new value for the field.
   */
  public void setPersonalIdRequiredTypeCode(String argPersonalIdRequiredTypeCode) {
    if( setPersonalIdRequiredTypeCode_noev(argPersonalIdRequiredTypeCode) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_PERSONALIDREQUIREDTYPECODE, argPersonalIdRequiredTypeCode);
    }
  }
  public boolean setPersonalIdRequiredTypeCode_noev(String argPersonalIdRequiredTypeCode) {
    boolean ev_postable = false;
    if( (getDAO_().getPersonalIdRequiredTypeCode() == null && argPersonalIdRequiredTypeCode != null ) ||
        (getDAO_().getPersonalIdRequiredTypeCode() != null && !getDAO_().getPersonalIdRequiredTypeCode().equals(argPersonalIdRequiredTypeCode)) ) {
      getDAO_().setPersonalIdRequiredTypeCode( argPersonalIdRequiredTypeCode );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the expr_date field.
   * @return The value of the field.
   */
  protected String getExpirationDateStringImpl() {
    return getDAO_().getExpirationDateString();
  }

  /**
   * Sets the value of the expr_date field.
   * @param argExpirationDateString The new value for the field.
   */
  protected void setExpirationDateStringImpl(String argExpirationDateString) {
    if( setExpirationDateString_noev(argExpirationDateString) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_EXPIRATIONDATESTRING, argExpirationDateString);
    }
  }
  public boolean setExpirationDateString_noev(String argExpirationDateString) {
    boolean ev_postable = false;
    if( (getDAO_().getExpirationDateString() == null && argExpirationDateString != null ) ||
        (getDAO_().getExpirationDateString() != null && !getDAO_().getExpirationDateString().equals(argExpirationDateString)) ) {
      getDAO_().setExpirationDateString( argExpirationDateString );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the mask_acct_nbr field.
   * @return The value of the field.
   */
  public String getMaskAccountNumberDao() {
    return getDAO_().getMaskAccountNumberDao();
  }

  /**
   * Sets the value of the mask_acct_nbr field.
   * @param argMaskAccountNumberDao The new value for the field.
   */
  public void setMaskAccountNumberDao(String argMaskAccountNumberDao) {
    if( setMaskAccountNumberDao_noev(argMaskAccountNumberDao) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_MASKACCOUNTNUMBERDAO, argMaskAccountNumberDao);
    }
  }
  public boolean setMaskAccountNumberDao_noev(String argMaskAccountNumberDao) {
    boolean ev_postable = false;
    if( (getDAO_().getMaskAccountNumberDao() == null && argMaskAccountNumberDao != null ) ||
        (getDAO_().getMaskAccountNumberDao() != null && !getDAO_().getMaskAccountNumberDao().equals(argMaskAccountNumberDao)) ) {
      getDAO_().setMaskAccountNumberDao( argMaskAccountNumberDao );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the cashback_amt field.
   * @return The value of the field.
   */
  public BigDecimal getCashbackAmount() {
    return getDAO_().getCashbackAmount();
  }

  /**
   * Sets the value of the cashback_amt field.
   * @param argCashbackAmount The new value for the field.
   */
  public void setCashbackAmount(BigDecimal argCashbackAmount) {
    if( setCashbackAmount_noev(argCashbackAmount) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_CASHBACKAMOUNT, argCashbackAmount);
    }
  }
  public boolean setCashbackAmount_noev(BigDecimal argCashbackAmount) {
    boolean ev_postable = false;
    if( (getDAO_().getCashbackAmount() == null && argCashbackAmount != null ) ||
        (getDAO_().getCashbackAmount() != null && !getDAO_().getCashbackAmount().equals(argCashbackAmount)) ) {
      getDAO_().setCashbackAmount( argCashbackAmount );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the card_level_indicator field.
   * @return The value of the field.
   */
  public String getCardLevelIndicator() {
    return getDAO_().getCardLevelIndicator();
  }

  /**
   * Sets the value of the card_level_indicator field.
   * @param argCardLevelIndicator The new value for the field.
   */
  public void setCardLevelIndicator(String argCardLevelIndicator) {
    if( setCardLevelIndicator_noev(argCardLevelIndicator) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_CARDLEVELINDICATOR, argCardLevelIndicator);
    }
  }
  public boolean setCardLevelIndicator_noev(String argCardLevelIndicator) {
    boolean ev_postable = false;
    if( (getDAO_().getCardLevelIndicator() == null && argCardLevelIndicator != null ) ||
        (getDAO_().getCardLevelIndicator() != null && !getDAO_().getCardLevelIndicator().equals(argCardLevelIndicator)) ) {
      getDAO_().setCardLevelIndicator( argCardLevelIndicator );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the acct_nbr_hash field.
   * @return The value of the field.
   */
  public String getAccountNumberHash() {
    return getDAO_().getAccountNumberHash();
  }

  /**
   * Sets the value of the acct_nbr_hash field.
   * @param argAccountNumberHash The new value for the field.
   */
  public void setAccountNumberHash(String argAccountNumberHash) {
    if( setAccountNumberHash_noev(argAccountNumberHash) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_ACCOUNTNUMBERHASH, argAccountNumberHash);
    }
  }
  public boolean setAccountNumberHash_noev(String argAccountNumberHash) {
    boolean ev_postable = false;
    if( (getDAO_().getAccountNumberHash() == null && argAccountNumberHash != null ) ||
        (getDAO_().getAccountNumberHash() != null && !getDAO_().getAccountNumberHash().equals(argAccountNumberHash)) ) {
      getDAO_().setAccountNumberHash( argAccountNumberHash );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the authorization_token field.
   * @return The value of the field.
   */
  public String getAuthorizationToken() {
    return getDAO_().getAuthorizationToken();
  }

  /**
   * Sets the value of the authorization_token field.
   * @param argAuthorizationToken The new value for the field.
   */
  public void setAuthorizationToken(String argAuthorizationToken) {
    if( setAuthorizationToken_noev(argAuthorizationToken) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_AUTHORIZATIONTOKEN, argAuthorizationToken);
    }
  }
  public boolean setAuthorizationToken_noev(String argAuthorizationToken) {
    boolean ev_postable = false;
    if( (getDAO_().getAuthorizationToken() == null && argAuthorizationToken != null ) ||
        (getDAO_().getAuthorizationToken() != null && !getDAO_().getAuthorizationToken().equals(argAuthorizationToken)) ) {
      getDAO_().setAuthorizationToken( argAuthorizationToken );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the transaction_reference_data field.
   * @return The value of the field.
   */
  public String getTransactionReferenceData() {
    return getDAO_().getTransactionReferenceData();
  }

  /**
   * Sets the value of the transaction_reference_data field.
   * @param argTransactionReferenceData The new value for the field.
   */
  public void setTransactionReferenceData(String argTransactionReferenceData) {
    if( setTransactionReferenceData_noev(argTransactionReferenceData) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_TRANSACTIONREFERENCEDATA, argTransactionReferenceData);
    }
  }
  public boolean setTransactionReferenceData_noev(String argTransactionReferenceData) {
    boolean ev_postable = false;
    if( (getDAO_().getTransactionReferenceData() == null && argTransactionReferenceData != null ) ||
        (getDAO_().getTransactionReferenceData() != null && !getDAO_().getTransactionReferenceData().equals(argTransactionReferenceData)) ) {
      getDAO_().setTransactionReferenceData( argTransactionReferenceData );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the trace_number field.
   * @return The value of the field.
   */
  public String getTraceNumber() {
    return getDAO_().getTraceNumber();
  }

  /**
   * Sets the value of the trace_number field.
   * @param argTraceNumber The new value for the field.
   */
  public void setTraceNumber(String argTraceNumber) {
    if( setTraceNumber_noev(argTraceNumber) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_TRACENUMBER, argTraceNumber);
    }
  }
  public boolean setTraceNumber_noev(String argTraceNumber) {
    boolean ev_postable = false;
    if( (getDAO_().getTraceNumber() == null && argTraceNumber != null ) ||
        (getDAO_().getTraceNumber() != null && !getDAO_().getTraceNumber().equals(argTraceNumber)) ) {
      getDAO_().setTraceNumber( argTraceNumber );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the tax_amt field.
   * @return The value of the field.
   */
  public BigDecimal getTaxAmount() {
    return getDAO_().getTaxAmount();
  }

  /**
   * Sets the value of the tax_amt field.
   * @param argTaxAmount The new value for the field.
   */
  public void setTaxAmount(BigDecimal argTaxAmount) {
    if( setTaxAmount_noev(argTaxAmount) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_TAXAMOUNT, argTaxAmount);
    }
  }
  public boolean setTaxAmount_noev(BigDecimal argTaxAmount) {
    boolean ev_postable = false;
    if( (getDAO_().getTaxAmount() == null && argTaxAmount != null ) ||
        (getDAO_().getTaxAmount() != null && !getDAO_().getTaxAmount().equals(argTaxAmount)) ) {
      getDAO_().setTaxAmount( argTaxAmount );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the discount_amt field.
   * @return The value of the field.
   */
  public BigDecimal getDiscountAmount() {
    return getDAO_().getDiscountAmount();
  }

  /**
   * Sets the value of the discount_amt field.
   * @param argDiscountAmount The new value for the field.
   */
  public void setDiscountAmount(BigDecimal argDiscountAmount) {
    if( setDiscountAmount_noev(argDiscountAmount) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_DISCOUNTAMOUNT, argDiscountAmount);
    }
  }
  public boolean setDiscountAmount_noev(BigDecimal argDiscountAmount) {
    boolean ev_postable = false;
    if( (getDAO_().getDiscountAmount() == null && argDiscountAmount != null ) ||
        (getDAO_().getDiscountAmount() != null && !getDAO_().getDiscountAmount().equals(argDiscountAmount)) ) {
      getDAO_().setDiscountAmount( argDiscountAmount );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the freight_amt field.
   * @return The value of the field.
   */
  public BigDecimal getFreightAmount() {
    return getDAO_().getFreightAmount();
  }

  /**
   * Sets the value of the freight_amt field.
   * @param argFreightAmount The new value for the field.
   */
  public void setFreightAmount(BigDecimal argFreightAmount) {
    if( setFreightAmount_noev(argFreightAmount) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_FREIGHTAMOUNT, argFreightAmount);
    }
  }
  public boolean setFreightAmount_noev(BigDecimal argFreightAmount) {
    boolean ev_postable = false;
    if( (getDAO_().getFreightAmount() == null && argFreightAmount != null ) ||
        (getDAO_().getFreightAmount() != null && !getDAO_().getFreightAmount().equals(argFreightAmount)) ) {
      getDAO_().setFreightAmount( argFreightAmount );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the duty_amt field.
   * @return The value of the field.
   */
  public BigDecimal getDutyAmount() {
    return getDAO_().getDutyAmount();
  }

  /**
   * Sets the value of the duty_amt field.
   * @param argDutyAmount The new value for the field.
   */
  public void setDutyAmount(BigDecimal argDutyAmount) {
    if( setDutyAmount_noev(argDutyAmount) ) {
      events_.post(dtv.xst.dao.ttr.ICreditDebitTenderLineItem.SET_DUTYAMOUNT, argDutyAmount);
    }
  }
  public boolean setDutyAmount_noev(BigDecimal argDutyAmount) {
    boolean ev_postable = false;
    if( (getDAO_().getDutyAmount() == null && argDutyAmount != null ) ||
        (getDAO_().getDutyAmount() != null && !getDAO_().getDutyAmount().equals(argDutyAmount)) ) {
      getDAO_().setDutyAmount( argDutyAmount );
      ev_postable = true;
    }
    return ev_postable;
  }


  public IDataModel getCreditDebitTenderLineItemExt() {
    return myExtension_;
  }

  public void setCreditDebitTenderLineItemExt(IDataModel argExt) {
    myExtension_ = argExt;
  }

  private transient dtv.event.EventHandler eventCascade_ = new dtv.event.handler.CascadingHandler(this);


  @Override
  public Object getValue(String argFieldId) {
    if ("CreditDebitTenderLineItemExtension".equals(argFieldId)) {
      return myExtension_;
    }
    else {
      return super.getValue(argFieldId);
    }
  }

  @Override
  public void setValue(String argFieldId, Object argValue) {
    if ("CreditDebitTenderLineItemExtension".equals(argFieldId)) {
      myExtension_ = (IDataModel)argValue;
    }
    else {
      super.setValue(argFieldId, argValue);
    }
  }

  @Override
  public void startTransaction() {
    if (alreadyInStart_) {
      return;
    }
    else {
      alreadyInStart_ = true;
    }

    super.startTransaction();

    
    alreadyInStart_ = false;
  }

  @Override
  public void rollbackChanges() {
    if (isAlreadyRolledBack())
      return;

    super.rollbackChanges();

  }

  @Override
  public void commitTransaction() {
    if (alreadyInCommit_) {
      return;
    } else {
      alreadyInCommit_ = true;
    }

    super.commitTransaction();

    
    alreadyInCommit_ = false;
  }

  private void readObject(java.io.ObjectInputStream argStream)
                         throws java.io.IOException, ClassNotFoundException {
    argStream.defaultReadObject();

    // reinit our eventors
    events_ = new dtv.data2.access.ModelEventor(this);
    eventCascade_ = new dtv.event.handler.CascadingHandler(this);
  }


  // ------------------------------------------------------------- 
  // ------------------------------------------------------------- 
  // - CUSTOM CODE BELOW THIS POINT. EDIT IN THE APPROPRIATE DTJ - 
  // ------------------------------------------------------------- 
  // ------------------------------------------------------------- 

//DTJ-START $URL: https://node1000.gspann.local:8443/svn/20181115-cri/branches/fipay_cust_name/pos/src-bridged/dtv/xst/dao/ttr/impl/CreditDebitTenderLineItemModel.java $ $Id: CreditDebitTenderLineItemModel.java 877 2015-07-15 18:38:01Z suz.sxie $
  private transient String encryptedPin;
  private transient String cidUnencrypted;
  private transient String track1 = null;
  private transient String track2 = null;
  private transient String track3 = null;
  private transient Date expirationDate;
  private transient String expirationDateStringUnencrypted;
  private transient String additionalPinSecurityInfo;
  private transient String cid;
  private transient boolean imprint;

  public void setTrack1(String newValue) {
    track1 = encryptField(newValue);
  }

  public void setTrack2(String newValue) {
    track2 = encryptField(newValue);
  }

  public void setTrack3(String newValue) {
    track3 = encryptField(newValue);
  }

  public String getTrack3Encrypted() {
    return track3;
  }

  public String getTrack2Encrypted() {
    return track2;
  }

  public String getTrack1Encrypted() {
    return track1;
  }

  public String getTrack3() {
    return decryptField(track3);
  }

  public String getTrack2() {
    return decryptField(track2);
  }

  public String getTrack1() {
    return decryptField(track1);
  }

  private static String encryptField(String value) {
    if (value == null) {
      return null;
    }
    byte[] b = getEncryptionService(dtv.util.service.IEncryptionService.CREDIT_CARD_ENCRYPTOR)
               .encrypt(value);
    return getEncoderService(dtv.util.service.IEncoderService.BASE_64).encode(b);
  }

  private static String decryptField(String value) {
    if (value == null) {
      return null;
    }
    byte[] b = getEncoderService(dtv.util.service.IEncoderService.BASE_64).decode(value);
    return getEncryptionService(dtv.util.service.IEncryptionService.CREDIT_CARD_ENCRYPTOR).decrypt(b);
  }

  public String getExpirationDateStringEncrypted() {
    return getExpirationDateStringImpl();
  }

  /*
   * {OVERRIDE GENERATED METHOD}
   */
  public String getExpirationDateString() {
    decryptExpirationDate(getExpirationDateStringImpl());
    return expirationDateStringUnencrypted;
  }

  public Date getExpirationDate() {
    decryptExpirationDate(getExpirationDateStringImpl());
    return expirationDate;
  }


  private void decryptExpirationDate(String argExpirationDateString) {    
    if (expirationDateStringUnencrypted == null || expirationDate == null) {
      if (argExpirationDateString == null) {
        expirationDateStringUnencrypted = null;
        expirationDate = null;
      }
      else {
        expirationDateStringUnencrypted = decryptField(argExpirationDateString);

        if (CriConfigurationMgr.isAjbEnabled()) {
          expirationDate = dtv.util.DateUtils.parseMsrDate(expirationDateStringUnencrypted);
        }
        else {
          expirationDate = dtv.util.DateUtils.parseDtvExpirationDate(expirationDateStringUnencrypted);
        }
      }
    }
  }

  /*
   * {OVERRIDE GENERATED METHOD}
   */
  public void setExpirationDateString(String newValue) {
    String expirationDateString = null;
    expirationDateStringUnencrypted = newValue;
    
    if (expirationDateStringUnencrypted == null) {
      expirationDate = null;
    }
    else {
      expirationDateString = encryptField(expirationDateStringUnencrypted);

      if (CriConfigurationMgr.isAjbEnabled()) {
        expirationDate = dtv.util.DateUtils.parseMsrDate(expirationDateStringUnencrypted);
      }
      else {
        expirationDate = dtv.util.DateUtils.parseDtvExpirationDate(expirationDateStringUnencrypted);
      }
    }
    
    setExpirationDateStringImpl(expirationDateString);
  }

  /**
   * Non-persistent field that indicates if the tenderline will require the cashier to make an imprint of the card
   *
   * @param argFlag new value
   */
  public void setImprint(boolean argFlag) {
    imprint = argFlag;
  }

  /**
   * Non-persistent field that indicates if the tenderline will require the cashier to make an imprint of the card
   *
   * @return value of non-persistent field that indicates if the tenderline will require the cashier to make an imprint of the card
   */
  public boolean getImprint() {
    return imprint;
  }

  /**
   * Non-persistent field that contains the encrypted PIN that will be used to authorize a debit tender
   *
   * @param newValue new value for field
   */
  public void setEncryptedPin(String newValue) {
    encryptedPin = newValue;
  }

  /**
   * Non-persistent field that contains the encrypted PIN that will be used to authorize a debit tender
   *
   * @return valeu of field
   */
  public String getEncryptedPin() {
    return encryptedPin;
  }

  /**
   * Non-persistent field that contains additional security information (e.g. key serial number) that will be used to authorize a debit tender
   *
   * @param newValue new value for field
   */
  public void setAdditionalPinSecurityInfo(String newValue) {
    additionalPinSecurityInfo = newValue;
  }

  /**
   * Non-persistent field that contains additional security information (e.g. key serial number) that will be used to authorize a debit tender
   *
   * @return valeu of field
   */
  public String getAdditionalPinSecurityInfo() {
    return additionalPinSecurityInfo;
  }
  
  /**
   * Sets a non-persisted field that holds the CID from a credit card.
   * 
   * @param newValue the new value for the <code>cid</code> field
   */
  public void setCid(String newValue) {
    if (newValue == null) {
      cid = null;
      cidUnencrypted = null;
    }
    else {
      cid = encryptField(newValue);
      cidUnencrypted = newValue;
    }
  }

  /**
   * Gets the value of a non-persisted field that holds the CID from a credit card.
   * 
   * @return the value of the <code>cid</code> field
   */
  public String getCid() {
    if (cidUnencrypted == null && cid != null) {
      cidUnencrypted = decryptField(cid);
    }
    return cidUnencrypted;
  }

  /*
   * {OVERRIDE GENERATED METHOD}
   */
  public void setAccountNumber(String newValue) {
    // Store hashed account number
    setAccountNumberHash(dtv.util.hash.Hasher.hash(ACCOUNT_HASHER_ID, newValue));
        
    // Store encrypted account number
    setAccountNumberImpl(encryptField(newValue));
  }

  public String getAccountNumberEncrypted() {
    return getAccountNumberImpl();
  }

  /*
   * {OVERRIDE GENERATED METHOD}
   */
  public String getAccountNumber() {
    String accountNumber = getAccountNumberImpl();
    
    if (accountNumber != null) {
      return decryptField(accountNumber);
    }
    return null;
  }
  
  public void clearTokenInformation() {
    setAccountNumberImpl(null);
    setExpirationDateStringImpl(null);
  }
}
