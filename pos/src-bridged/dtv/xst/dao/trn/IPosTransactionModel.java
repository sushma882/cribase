// $Id: IPosTransactionModel.java 777 2014-03-27 02:15:29Z suz.jliu $
package dtv.xst.dao.trn;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import dtv.event.EventEnum;
import dtv.pricing2.PricingDeal;
import dtv.pricing2.Result;
import dtv.xst.dao.loc.RetailLocationId;
import dtv.xst.dao.trl.IRetailTransactionLineItem;
import dtv.xst.pricing.EventedPricingTransaction;
import dtv.xst.pricing.XSTPricingDescriptor;

/**
 * Interface used when accessing the <tt>trn_trans</tt> table. This interface indicates the methods that are
 * defined in the DTJ instead of being generated.<br>
 * <br>
 * Copyright (c) 2005 Datavantage Corporation
 * 
 * @author cdusseau
 * @version $Revision: 777 $
 * @created Oct 7, 2005
 */
public interface IPosTransactionModel {

  EventEnum TRANSACTION_UPDATED = new EventEnum("TransactionUpdated");

  BigDecimal getAmountDue();

  BigDecimal getAmountTendered();

  Date getBeginDatetimestamp();

  /**
   * Returns a list of the deals that are active and may apply for this transaction, if the necessary criteria
   * are met. It is important to note that this method returns all of the deals that <b>may</b> apply, not the
   * deals that have been applied.
   * 
   * @return a list of the deals that are may apply for this transaction
   */
  List<PricingDeal<XSTPricingDescriptor>> getCurrentPossibleDeals();

  Date getCustBirthDate();

  Result<IRetailTransactionLineItem, XSTPricingDescriptor> getDealResults();

  List<IRetailTransactionLineItem> getDiscountLineItems();

  /**
   * Gets the dollar amount of food stamps that can be applied to the transaction. (Once the transaction is
   * fully tendered, this should be zero.)
   * 
   * @return the amount of food stamps that can still be applied to the transaction
   */
  BigDecimal getFoodStampsAmountDue();

  /**
   * Used to get all of the line items from a transaction of a given type code that implement a given
   * interface. This method frees client code from type checking the type of each line.
   * 
   * @param <T> the type of items in the list
   * @param argInterface the interface to locate (e.g. ISaleReturnLineItem, ITaxLineItem,
   * ICreditCardTenderLineItem, etc.)
   * @return a non-null List of items
   * @see IPosTransaction#getRetailTransactionLineItems()
   */
  <T extends IRetailTransactionLineItem> List<T> getLineItems(Class<T> argInterface);

  List<IRetailTransactionLineItem> getLineItemsByTypeCode(String argLineItemTypeCode);

  /**
   * Used to get all of the line items from a transaction of a given type code that implement a given
   * interface. This method frees client code from type checking the type of each line.
   * 
   * @param <T> the type of items in the list
   * @param argLineItemTypeCode the line item type code as seen in <tt>dtv.pos.register.type.LineItemType</tt>
   * @param argInterface the interface to locate (e.g. ISaleReturnLineItem or ITaxLineItem)
   * @return a non-null List of items
   * @see #getLineItemsByTypeCode(String)
   */
  <T extends IRetailTransactionLineItem> List<T> getLineItemsByTypeCode(String argLineItemTypeCode,
      Class<T> argInterface);

  /**
   * Returns the subset of the line items returned by {@link IPosTransaction#getRetailTransactionLineItems()}
   * which are to be included in the PosLog. Generally speaking, all such lines should be so included, but
   * certain "concessions" have been made for difficult-to-accommodate functional requirements which result in
   * the creation of temporary/transient barnacle line items that will be discarded once this transaction is
   * complete.<br>
   * <br>
   * The list returned by this method is not "live". Changes to its composition will have no impact on the
   * backing persistent relationship between this transaction and its retail lines.
   * 
   * @return all retail line items attached to this transaction and which are minimally eligible for inclusion
   * in the PosLog; an empty list if no such lines exist
   */
  List<? extends IRetailTransactionLineItem> getPosLogRetailLineItems();

  EventedPricingTransaction getPricing();

  RetailLocationId getRetailLocationIdObject();

  BigDecimal getRoundedAmount();

  List<IRetailTransactionLineItem> getSaleLineItems();

  Result<IRetailTransactionLineItem, XSTPricingDescriptor> getSubstituteDealResults();

  List<IRetailTransactionLineItem> getTaxLineItems();

  List<IRetailTransactionLineItem> getTenderLineItems();

  Result<IRetailTransactionLineItem, XSTPricingDescriptor> getThresholdDealResults();

  void setAmountTendered(BigDecimal argAmountTendered);

  void setBeginDatetimestamp(Date newValue);

  void setCustBirthDate(Date custBirthDate);

  /**
   * Sets the dollar amount of food stamps that can be applied to the transaction.
   * 
   * @param argAmount the new amount
   */
  void setFoodStampsAmountDue(BigDecimal argAmount);

  void setRoundedAmount(BigDecimal argRoundedAmount);
  
  PricingDeal<XSTPricingDescriptor>[] getEligibleConditionalDeals();
}
