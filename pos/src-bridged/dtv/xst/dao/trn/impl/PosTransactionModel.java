package dtv.xst.dao.trn.impl;

import java.math.BigDecimal;
import java.util.*;

import dtv.data2.access.IDataModel;
import dtv.util.HistoricalList;
import dtv.data2.access.*;
import dtv.data2.access.impl.*;
import dtv.xst.dao.crm.IParty;
import dtv.xst.dao.trn.PosTransactionId;
import dtv.xst.pricing.XSTPricingDescriptor;

/**
 * Auto Generated Model<br>
 * Copyright (c) 2012 Datavantage Corporation
 *
 * @author DAOGen
 */
@SuppressWarnings("all")
public class PosTransactionModel 
  extends dtv.data2.access.impl.AbstractDataModelWithPropertyImpl<dtv.xst.dao.trn.IPosTransactionProperties>
  implements dtv.xst.dao.trn.IPosTransaction {

  // Fix serialization compatability based on the name of the DAO
  private static final long serialVersionUID = 1475778570L;

  private transient dtv.event.Eventor events_ = new dtv.data2.access.ModelEventor(this);

  private transient boolean alreadyInStart_ = false;
  private transient boolean alreadyInCommit_ = false;

  private IDataModel myExtension_;

  @Override
  public String toString() {
    return super.toString() + " Id: " + this.getObjectId();
  }

  @Override
  public void initDAO() {
    setDAO(new PosTransactionDAO());
  }

  /**
   * Return the data access object associated with this data model
   * @return our DAO, properly casted
   */
  private PosTransactionDAO getDAO_() {
     return (PosTransactionDAO)daoImpl_;
  }

  /**
   * Gets the value of the organization_id field.
   * @return The value of the field.
   */
  public long getOrganizationId() {
    if (getDAO_().getOrganizationId() != null) {
      return getDAO_().getOrganizationId().longValue();
    }
    else {
      return 0; // no default specified in the dtx; we default to 0
    }
  }

  /**
   * Sets the value of the organization_id field.
   * @param argOrganizationId The new value for the field.
   */
  public void setOrganizationId(long argOrganizationId) {
    if( setOrganizationId_noev(argOrganizationId) ) {
      // suppress-event is specified for this field.
    }
  }
  public boolean setOrganizationId_noev(long argOrganizationId) {
    boolean ev_postable = false;
    if( (getDAO_().getOrganizationId() == null && new Long(argOrganizationId) != null ) ||
        (getDAO_().getOrganizationId() != null && !getDAO_().getOrganizationId().equals(new Long(argOrganizationId))) ) {
      getDAO_().setOrganizationId( new Long(argOrganizationId) );
      ev_postable = true;
      if( properties_ != null ) {
        // Propagate changes to related objects in relation Properties.
        java.util.Iterator it = properties_.iterator();
        while(it.hasNext() ) {
          // Use the non-eventing setter directly.
          ((dtv.xst.dao.trn.impl.PosTransactionPropertiesModel)it.next()).setOrganizationId_noev(argOrganizationId);
        }
      }
      if( retailTransactionLineItems_ != null ) {
        // Propagate changes to related objects in relation RetailTransactionLineItems.
        java.util.Iterator it = retailTransactionLineItems_.iterator();
        while(it.hasNext() ) {
          // Use the non-eventing setter directly.
          ((dtv.xst.dao.trl.impl.RetailTransactionLineItemModel)it.next()).setOrganizationId_noev(argOrganizationId);
        }
      }
      if( transactionLinks_ != null ) {
        // Propagate changes to related objects in relation TransactionLinks.
        java.util.Iterator it = transactionLinks_.iterator();
        while(it.hasNext() ) {
          // Use the non-eventing setter directly.
          ((dtv.xst.dao.trn.impl.PosTransactionLinkModel)it.next()).setOrganizationId_noev(argOrganizationId);
        }
      }
      if( transactionNotes_ != null ) {
        // Propagate changes to related objects in relation TransactionNotes.
        java.util.Iterator it = transactionNotes_.iterator();
        while(it.hasNext() ) {
          // Use the non-eventing setter directly.
          ((dtv.xst.dao.trn.impl.TransactionNotesModel)it.next()).setOrganizationId_noev(argOrganizationId);
        }
      }
    }
    return ev_postable;
  }

  /**
   * Gets the value of the rtl_loc_id field.
   * @return The value of the field.
   */
  public long getRetailLocationId() {
    if (getDAO_().getRetailLocationId() != null) {
      return getDAO_().getRetailLocationId().longValue();
    }
    else {
      return 0; // no default specified in the dtx; we default to 0
    }
  }

  /**
   * Sets the value of the rtl_loc_id field.
   * @param argRetailLocationId The new value for the field.
   */
  public void setRetailLocationId(long argRetailLocationId) {
    if( setRetailLocationId_noev(argRetailLocationId) ) {
      // suppress-event is specified for this field.
    }
  }
  public boolean setRetailLocationId_noev(long argRetailLocationId) {
    boolean ev_postable = false;
    if( (getDAO_().getRetailLocationId() == null && new Long(argRetailLocationId) != null ) ||
        (getDAO_().getRetailLocationId() != null && !getDAO_().getRetailLocationId().equals(new Long(argRetailLocationId))) ) {
      getDAO_().setRetailLocationId( new Long(argRetailLocationId) );
      ev_postable = true;
      if( properties_ != null ) {
        // Propagate changes to related objects in relation Properties.
        java.util.Iterator it = properties_.iterator();
        while(it.hasNext() ) {
          // Use the non-eventing setter directly.
          ((dtv.xst.dao.trn.impl.PosTransactionPropertiesModel)it.next()).setRetailLocationId_noev(argRetailLocationId);
        }
      }
      if( retailTransactionLineItems_ != null ) {
        // Propagate changes to related objects in relation RetailTransactionLineItems.
        java.util.Iterator it = retailTransactionLineItems_.iterator();
        while(it.hasNext() ) {
          // Use the non-eventing setter directly.
          ((dtv.xst.dao.trl.impl.RetailTransactionLineItemModel)it.next()).setRetailLocationId_noev(argRetailLocationId);
        }
      }
      if( transactionLinks_ != null ) {
        // Propagate changes to related objects in relation TransactionLinks.
        java.util.Iterator it = transactionLinks_.iterator();
        while(it.hasNext() ) {
          // Use the non-eventing setter directly.
          ((dtv.xst.dao.trn.impl.PosTransactionLinkModel)it.next()).setRetailLocationId_noev(argRetailLocationId);
        }
      }
      if( transactionNotes_ != null ) {
        // Propagate changes to related objects in relation TransactionNotes.
        java.util.Iterator it = transactionNotes_.iterator();
        while(it.hasNext() ) {
          // Use the non-eventing setter directly.
          ((dtv.xst.dao.trn.impl.TransactionNotesModel)it.next()).setRetailLocationId_noev(argRetailLocationId);
        }
      }
    }
    return ev_postable;
  }

  /**
   * Gets the value of the business_date field.
   * @return The value of the field.
   */
  public Date getBusinessDate() {
    return getDAO_().getBusinessDate();
  }

  /**
   * Sets the value of the business_date field.
   * @param argBusinessDate The new value for the field.
   */
  public void setBusinessDate(Date argBusinessDate) {
    if( setBusinessDate_noev(argBusinessDate) ) {
      events_.post(dtv.xst.dao.trn.IPosTransaction.SET_BUSINESSDATE, argBusinessDate);
    }
  }
  public boolean setBusinessDate_noev(Date argBusinessDate) {
    boolean ev_postable = false;
    if( (getDAO_().getBusinessDate() == null && argBusinessDate != null ) ||
        (getDAO_().getBusinessDate() != null && !getDAO_().getBusinessDate().equals(argBusinessDate)) ) {
      getDAO_().setBusinessDate( argBusinessDate );
      ev_postable = true;
      if( properties_ != null ) {
        // Propagate changes to related objects in relation Properties.
        java.util.Iterator it = properties_.iterator();
        while(it.hasNext() ) {
          // Use the non-eventing setter directly.
          ((dtv.xst.dao.trn.impl.PosTransactionPropertiesModel)it.next()).setBusinessDate_noev(argBusinessDate);
        }
      }
      if( retailTransactionLineItems_ != null ) {
        // Propagate changes to related objects in relation RetailTransactionLineItems.
        java.util.Iterator it = retailTransactionLineItems_.iterator();
        while(it.hasNext() ) {
          // Use the non-eventing setter directly.
          ((dtv.xst.dao.trl.impl.RetailTransactionLineItemModel)it.next()).setBusinessDate_noev(argBusinessDate);
        }
      }
      if( transactionLinks_ != null ) {
        // Propagate changes to related objects in relation TransactionLinks.
        java.util.Iterator it = transactionLinks_.iterator();
        while(it.hasNext() ) {
          // Use the non-eventing setter directly.
          ((dtv.xst.dao.trn.impl.PosTransactionLinkModel)it.next()).setBusinessDate_noev(argBusinessDate);
        }
      }
      if( transactionNotes_ != null ) {
        // Propagate changes to related objects in relation TransactionNotes.
        java.util.Iterator it = transactionNotes_.iterator();
        while(it.hasNext() ) {
          // Use the non-eventing setter directly.
          ((dtv.xst.dao.trn.impl.TransactionNotesModel)it.next()).setBusinessDate_noev(argBusinessDate);
        }
      }
    }
    return ev_postable;
  }

  /**
   * Gets the value of the wkstn_id field.
   * @return The value of the field.
   */
  public long getWorkstationId() {
    if (getDAO_().getWorkstationId() != null) {
      return getDAO_().getWorkstationId().longValue();
    }
    else {
      return 0; // no default specified in the dtx; we default to 0
    }
  }

  /**
   * Sets the value of the wkstn_id field.
   * @param argWorkstationId The new value for the field.
   */
  public void setWorkstationId(long argWorkstationId) {
    if( setWorkstationId_noev(argWorkstationId) ) {
      // suppress-event is specified for this field.
    }
  }
  public boolean setWorkstationId_noev(long argWorkstationId) {
    boolean ev_postable = false;
    if( (getDAO_().getWorkstationId() == null && new Long(argWorkstationId) != null ) ||
        (getDAO_().getWorkstationId() != null && !getDAO_().getWorkstationId().equals(new Long(argWorkstationId))) ) {
      getDAO_().setWorkstationId( new Long(argWorkstationId) );
      ev_postable = true;
      if( properties_ != null ) {
        // Propagate changes to related objects in relation Properties.
        java.util.Iterator it = properties_.iterator();
        while(it.hasNext() ) {
          // Use the non-eventing setter directly.
          ((dtv.xst.dao.trn.impl.PosTransactionPropertiesModel)it.next()).setWorkstationId_noev(argWorkstationId);
        }
      }
      if( retailTransactionLineItems_ != null ) {
        // Propagate changes to related objects in relation RetailTransactionLineItems.
        java.util.Iterator it = retailTransactionLineItems_.iterator();
        while(it.hasNext() ) {
          // Use the non-eventing setter directly.
          ((dtv.xst.dao.trl.impl.RetailTransactionLineItemModel)it.next()).setWorkstationId_noev(argWorkstationId);
        }
      }
      if( transactionLinks_ != null ) {
        // Propagate changes to related objects in relation TransactionLinks.
        java.util.Iterator it = transactionLinks_.iterator();
        while(it.hasNext() ) {
          // Use the non-eventing setter directly.
          ((dtv.xst.dao.trn.impl.PosTransactionLinkModel)it.next()).setWorkstationId_noev(argWorkstationId);
        }
      }
      if( transactionNotes_ != null ) {
        // Propagate changes to related objects in relation TransactionNotes.
        java.util.Iterator it = transactionNotes_.iterator();
        while(it.hasNext() ) {
          // Use the non-eventing setter directly.
          ((dtv.xst.dao.trn.impl.TransactionNotesModel)it.next()).setWorkstationId_noev(argWorkstationId);
        }
      }
    }
    return ev_postable;
  }

  /**
   * Gets the value of the trans_seq field.
   * @return The value of the field.
   */
  public long getTransactionSequence() {
    if (getDAO_().getTransactionSequence() != null) {
      return getDAO_().getTransactionSequence().longValue();
    }
    else {
      return 0; // no default specified in the dtx; we default to 0
    }
  }

  /**
   * Sets the value of the trans_seq field.
   * @param argTransactionSequence The new value for the field.
   */
  public void setTransactionSequence(long argTransactionSequence) {
    if( setTransactionSequence_noev(argTransactionSequence) ) {
      events_.post(dtv.xst.dao.trn.IPosTransaction.SET_TRANSACTIONSEQUENCE, new Long(argTransactionSequence));
    }
  }
  public boolean setTransactionSequence_noev(long argTransactionSequence) {
    boolean ev_postable = false;
    if( (getDAO_().getTransactionSequence() == null && new Long(argTransactionSequence) != null ) ||
        (getDAO_().getTransactionSequence() != null && !getDAO_().getTransactionSequence().equals(new Long(argTransactionSequence))) ) {
      getDAO_().setTransactionSequence( new Long(argTransactionSequence) );
      ev_postable = true;
      if( properties_ != null ) {
        // Propagate changes to related objects in relation Properties.
        java.util.Iterator it = properties_.iterator();
        while(it.hasNext() ) {
          // Use the non-eventing setter directly.
          ((dtv.xst.dao.trn.impl.PosTransactionPropertiesModel)it.next()).setTransactionSequence_noev(argTransactionSequence);
        }
      }
      if( retailTransactionLineItems_ != null ) {
        // Propagate changes to related objects in relation RetailTransactionLineItems.
        java.util.Iterator it = retailTransactionLineItems_.iterator();
        while(it.hasNext() ) {
          // Use the non-eventing setter directly.
          ((dtv.xst.dao.trl.impl.RetailTransactionLineItemModel)it.next()).setTransactionSequence_noev(argTransactionSequence);
        }
      }
      if( transactionLinks_ != null ) {
        // Propagate changes to related objects in relation TransactionLinks.
        java.util.Iterator it = transactionLinks_.iterator();
        while(it.hasNext() ) {
          // Use the non-eventing setter directly.
          ((dtv.xst.dao.trn.impl.PosTransactionLinkModel)it.next()).setTransactionSequence_noev(argTransactionSequence);
        }
      }
      if( transactionNotes_ != null ) {
        // Propagate changes to related objects in relation TransactionNotes.
        java.util.Iterator it = transactionNotes_.iterator();
        while(it.hasNext() ) {
          // Use the non-eventing setter directly.
          ((dtv.xst.dao.trn.impl.TransactionNotesModel)it.next()).setTransactionSequence_noev(argTransactionSequence);
        }
      }
    }
    return ev_postable;
  }

  /**
   * Gets the value of the dtv_class_name field.
   * @return The value of the field.
   */
  public String getClassName() {
    return getDAO_().getClassName();
  }

  /**
   * Sets the value of the dtv_class_name field.
   * @param argClassName The new value for the field.
   */
  public void setClassName(String argClassName) {
    if( setClassName_noev(argClassName) ) {
      // suppress-event is specified for this field.
    }
  }
  public boolean setClassName_noev(String argClassName) {
    boolean ev_postable = false;
    if( (getDAO_().getClassName() == null && argClassName != null ) ||
        (getDAO_().getClassName() != null && !getDAO_().getClassName().equals(argClassName)) ) {
      getDAO_().setClassName( argClassName );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the create_date field.
   * @return The value of the field.
   */
  public Date getCreateDate() {
    return getDAO_().getCreateDate();
  }

  /**
   * Sets the value of the create_date field.
   * @param argCreateDate The new value for the field.
   */
  public void setCreateDate(Date argCreateDate) {
    if( setCreateDate_noev(argCreateDate) ) {
      // suppress-event is specified for this field.
    }
  }
  public boolean setCreateDate_noev(Date argCreateDate) {
    boolean ev_postable = false;
    if( (getDAO_().getCreateDate() == null && argCreateDate != null ) ||
        (getDAO_().getCreateDate() != null && !getDAO_().getCreateDate().equals(argCreateDate)) ) {
      getDAO_().setCreateDate( argCreateDate );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the create_user_id field.
   * @return The value of the field.
   */
  public String getCreateUserId() {
    return getDAO_().getCreateUserId();
  }

  /**
   * Sets the value of the create_user_id field.
   * @param argCreateUserId The new value for the field.
   */
  public void setCreateUserId(String argCreateUserId) {
    if( setCreateUserId_noev(argCreateUserId) ) {
      // suppress-event is specified for this field.
    }
  }
  public boolean setCreateUserId_noev(String argCreateUserId) {
    boolean ev_postable = false;
    if( (getDAO_().getCreateUserId() == null && argCreateUserId != null ) ||
        (getDAO_().getCreateUserId() != null && !getDAO_().getCreateUserId().equals(argCreateUserId)) ) {
      getDAO_().setCreateUserId( argCreateUserId );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the update_date field.
   * @return The value of the field.
   */
  public Date getUpdateDate() {
    return getDAO_().getUpdateDate();
  }

  /**
   * Sets the value of the update_date field.
   * @param argUpdateDate The new value for the field.
   */
  public void setUpdateDate(Date argUpdateDate) {
    if( setUpdateDate_noev(argUpdateDate) ) {
      // suppress-event is specified for this field.
    }
  }
  public boolean setUpdateDate_noev(Date argUpdateDate) {
    boolean ev_postable = false;
    if( (getDAO_().getUpdateDate() == null && argUpdateDate != null ) ||
        (getDAO_().getUpdateDate() != null && !getDAO_().getUpdateDate().equals(argUpdateDate)) ) {
      getDAO_().setUpdateDate( argUpdateDate );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the update_user_id field.
   * @return The value of the field.
   */
  public String getUpdateUserId() {
    return getDAO_().getUpdateUserId();
  }

  /**
   * Sets the value of the update_user_id field.
   * @param argUpdateUserId The new value for the field.
   */
  public void setUpdateUserId(String argUpdateUserId) {
    if( setUpdateUserId_noev(argUpdateUserId) ) {
      // suppress-event is specified for this field.
    }
  }
  public boolean setUpdateUserId_noev(String argUpdateUserId) {
    boolean ev_postable = false;
    if( (getDAO_().getUpdateUserId() == null && argUpdateUserId != null ) ||
        (getDAO_().getUpdateUserId() != null && !getDAO_().getUpdateUserId().equals(argUpdateUserId)) ) {
      getDAO_().setUpdateUserId( argUpdateUserId );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the begin_datetime field.
   * @return The value of the field.
   */
  public Date getBeginDatetimestamp() {
    return getDAO_().getBeginDatetimestamp();
  }

  /**
   * Sets the value of the begin_datetime field.
   * @param argBeginDatetimestamp The new value for the field.
   */
  protected void setBeginDatetimestampImpl(Date argBeginDatetimestamp) {
    if( setBeginDatetimestamp_noev(argBeginDatetimestamp) ) {
      events_.post(dtv.xst.dao.trn.IPosTransaction.SET_BEGINDATETIMESTAMP, argBeginDatetimestamp);
    }
  }
  public boolean setBeginDatetimestamp_noev(Date argBeginDatetimestamp) {
    boolean ev_postable = false;
    if( (getDAO_().getBeginDatetimestamp() == null && argBeginDatetimestamp != null ) ||
        (getDAO_().getBeginDatetimestamp() != null && !getDAO_().getBeginDatetimestamp().equals(argBeginDatetimestamp)) ) {
      getDAO_().setBeginDatetimestamp( argBeginDatetimestamp );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the begin_time_int field.
   * @return The value of the field.
   */
  public int getBeginTimeInt() {
    if (getDAO_().getBeginTimeInt() != null) {
      return getDAO_().getBeginTimeInt().intValue();
    }
    else {
      return 0; // no default specified in the dtx; we default to 0
    }
  }

  /**
   * Sets the value of the begin_time_int field.
   * @param argBeginTimeInt The new value for the field.
   */
  public void setBeginTimeInt(int argBeginTimeInt) {
    if( setBeginTimeInt_noev(argBeginTimeInt) ) {
      events_.post(dtv.xst.dao.trn.IPosTransaction.SET_BEGINTIMEINT, new Integer(argBeginTimeInt));
    }
  }
  public boolean setBeginTimeInt_noev(int argBeginTimeInt) {
    boolean ev_postable = false;
    if( (getDAO_().getBeginTimeInt() == null && new Integer(argBeginTimeInt) != null ) ||
        (getDAO_().getBeginTimeInt() != null && !getDAO_().getBeginTimeInt().equals(new Integer(argBeginTimeInt))) ) {
      getDAO_().setBeginTimeInt( new Integer(argBeginTimeInt) );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the end_datetime field.
   * @return The value of the field.
   */
  public Date getEndDatetimestamp() {
    return getDAO_().getEndDatetimestamp();
  }

  /**
   * Sets the value of the end_datetime field.
   * @param argEndDatetimestamp The new value for the field.
   */
  public void setEndDatetimestamp(Date argEndDatetimestamp) {
    if( setEndDatetimestamp_noev(argEndDatetimestamp) ) {
      events_.post(dtv.xst.dao.trn.IPosTransaction.SET_ENDDATETIMESTAMP, argEndDatetimestamp);
    }
  }
  public boolean setEndDatetimestamp_noev(Date argEndDatetimestamp) {
    boolean ev_postable = false;
    if( (getDAO_().getEndDatetimestamp() == null && argEndDatetimestamp != null ) ||
        (getDAO_().getEndDatetimestamp() != null && !getDAO_().getEndDatetimestamp().equals(argEndDatetimestamp)) ) {
      getDAO_().setEndDatetimestamp( argEndDatetimestamp );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the keyed_offline_flag field.
   * @return The value of the field.
   */
  public boolean getKeyedOffline() {
    if (getDAO_().getKeyedOffline() != null) {
      return getDAO_().getKeyedOffline().booleanValue();
    }
    else {
      return false;
    }
  }

  /**
   * Sets the value of the keyed_offline_flag field.
   * @param argKeyedOffline The new value for the field.
   */
  public void setKeyedOffline(boolean argKeyedOffline) {
    if( setKeyedOffline_noev(argKeyedOffline) ) {
      events_.post(dtv.xst.dao.trn.IPosTransaction.SET_KEYEDOFFLINE, new Boolean(argKeyedOffline));
    }
  }
  public boolean setKeyedOffline_noev(boolean argKeyedOffline) {
    boolean ev_postable = false;
    if( (getDAO_().getKeyedOffline() == null && new Boolean(argKeyedOffline) != null ) ||
        (getDAO_().getKeyedOffline() != null && !getDAO_().getKeyedOffline().equals(new Boolean(argKeyedOffline))) ) {
      getDAO_().setKeyedOffline( new Boolean(argKeyedOffline) );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the posted_flag field.
   * @return The value of the field.
   */
  public boolean getPosted() {
    if (getDAO_().getPosted() != null) {
      return getDAO_().getPosted().booleanValue();
    }
    else {
      return false;
    }
  }

  /**
   * Sets the value of the posted_flag field.
   * @param argPosted The new value for the field.
   */
  public void setPosted(boolean argPosted) {
    if( setPosted_noev(argPosted) ) {
      events_.post(dtv.xst.dao.trn.IPosTransaction.SET_POSTED, new Boolean(argPosted));
    }
  }
  public boolean setPosted_noev(boolean argPosted) {
    boolean ev_postable = false;
    if( (getDAO_().getPosted() == null && new Boolean(argPosted) != null ) ||
        (getDAO_().getPosted() != null && !getDAO_().getPosted().equals(new Boolean(argPosted))) ) {
      getDAO_().setPosted( new Boolean(argPosted) );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the session_id field.
   * @return The value of the field.
   */
  public long getSessionId() {
    if (getDAO_().getSessionId() != null) {
      return getDAO_().getSessionId().longValue();
    }
    else {
      return 0; // no default specified in the dtx; we default to 0
    }
  }

  /**
   * Sets the value of the session_id field.
   * @param argSessionId The new value for the field.
   */
  public void setSessionId(long argSessionId) {
    if( setSessionId_noev(argSessionId) ) {
      events_.post(dtv.xst.dao.trn.IPosTransaction.SET_SESSIONID, new Long(argSessionId));
    }
  }
  public boolean setSessionId_noev(long argSessionId) {
    boolean ev_postable = false;
    if( (getDAO_().getSessionId() == null && new Long(argSessionId) != null ) ||
        (getDAO_().getSessionId() != null && !getDAO_().getSessionId().equals(new Long(argSessionId))) ) {
      getDAO_().setSessionId( new Long(argSessionId) );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the subtotal field.
   * @return The value of the field.
   */
  public java.math.BigDecimal getSubtotal() {
    return getDAO_().getSubtotal();
  }

  /**
   * Sets the value of the subtotal field.
   * @param argSubtotal The new value for the field.
   */
  public void setSubtotal(java.math.BigDecimal argSubtotal) {
    if( setSubtotal_noev(argSubtotal) ) {
      events_.post(dtv.xst.dao.trn.IPosTransaction.SET_SUBTOTAL, argSubtotal);
    }
  }
  public boolean setSubtotal_noev(java.math.BigDecimal argSubtotal) {
    boolean ev_postable = false;
    if( (getDAO_().getSubtotal() == null && argSubtotal != null ) ||
        (getDAO_().getSubtotal() != null && !getDAO_().getSubtotal().equals(argSubtotal)) ) {
      getDAO_().setSubtotal( argSubtotal );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the taxtotal field.
   * @return The value of the field.
   */
  public java.math.BigDecimal getTaxAmount() {
    return getDAO_().getTaxAmount();
  }

  /**
   * Sets the value of the taxtotal field.
   * @param argTaxAmount The new value for the field.
   */
  public void setTaxAmount(java.math.BigDecimal argTaxAmount) {
    if( setTaxAmount_noev(argTaxAmount) ) {
      events_.post(dtv.xst.dao.trn.IPosTransaction.SET_TAXAMOUNT, argTaxAmount);
    }
  }
  public boolean setTaxAmount_noev(java.math.BigDecimal argTaxAmount) {
    boolean ev_postable = false;
    if( (getDAO_().getTaxAmount() == null && argTaxAmount != null ) ||
        (getDAO_().getTaxAmount() != null && !getDAO_().getTaxAmount().equals(argTaxAmount)) ) {
      getDAO_().setTaxAmount( argTaxAmount );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the roundtotal field.
   * @return The value of the field.
   */
  protected java.math.BigDecimal getRoundedAmountImpl() {
    return getDAO_().getRoundedAmount();
  }

  /**
   * Sets the value of the roundtotal field.
   * @param argRoundedAmount The new value for the field.
   */
  public void setRoundedAmount(java.math.BigDecimal argRoundedAmount) {
    if( setRoundedAmount_noev(argRoundedAmount) ) {
      events_.post(dtv.xst.dao.trn.IPosTransaction.SET_ROUNDEDAMOUNT, argRoundedAmount);
    }
  }
  public boolean setRoundedAmount_noev(java.math.BigDecimal argRoundedAmount) {
    boolean ev_postable = false;
    if( (getDAO_().getRoundedAmount() == null && argRoundedAmount != null ) ||
        (getDAO_().getRoundedAmount() != null && !getDAO_().getRoundedAmount().equals(argRoundedAmount)) ) {
      getDAO_().setRoundedAmount( argRoundedAmount );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the total field.
   * @return The value of the field.
   */
  public java.math.BigDecimal getTotal() {
    return getDAO_().getTotal();
  }

  /**
   * Sets the value of the total field.
   * @param argTotal The new value for the field.
   */
  public void setTotal(java.math.BigDecimal argTotal) {
    if( setTotal_noev(argTotal) ) {
      events_.post(dtv.xst.dao.trn.IPosTransaction.SET_TOTAL, argTotal);
    }
  }
  public boolean setTotal_noev(java.math.BigDecimal argTotal) {
    boolean ev_postable = false;
    if( (getDAO_().getTotal() == null && argTotal != null ) ||
        (getDAO_().getTotal() != null && !getDAO_().getTotal().equals(argTotal)) ) {
      getDAO_().setTotal( argTotal );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the trans_statcode field.
   * @return The value of the field.
   */
  public String getTransactionStatusCode() {
    return getDAO_().getTransactionStatusCode();
  }

  /**
   * Sets the value of the trans_statcode field.
   * @param argTransactionStatusCode The new value for the field.
   */
  public void setTransactionStatusCode(String argTransactionStatusCode) {
    if( setTransactionStatusCode_noev(argTransactionStatusCode) ) {
      events_.post(dtv.xst.dao.trn.IPosTransaction.SET_TRANSACTIONSTATUSCODE, argTransactionStatusCode);
    }
  }
  public boolean setTransactionStatusCode_noev(String argTransactionStatusCode) {
    boolean ev_postable = false;
    if( (getDAO_().getTransactionStatusCode() == null && argTransactionStatusCode != null ) ||
        (getDAO_().getTransactionStatusCode() != null && !getDAO_().getTransactionStatusCode().equals(argTransactionStatusCode)) ) {
      getDAO_().setTransactionStatusCode( argTransactionStatusCode );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the trans_typcode field.
   * @return The value of the field.
   */
  public String getTransactionTypeCode() {
    return getDAO_().getTransactionTypeCode();
  }

  /**
   * Sets the value of the trans_typcode field.
   * @param argTransactionTypeCode The new value for the field.
   */
  public void setTransactionTypeCode(String argTransactionTypeCode) {
    if( setTransactionTypeCode_noev(argTransactionTypeCode) ) {
      events_.post(dtv.xst.dao.trn.IPosTransaction.SET_TRANSACTIONTYPECODE, argTransactionTypeCode);
    }
  }
  public boolean setTransactionTypeCode_noev(String argTransactionTypeCode) {
    boolean ev_postable = false;
    if( (getDAO_().getTransactionTypeCode() == null && argTransactionTypeCode != null ) ||
        (getDAO_().getTransactionTypeCode() != null && !getDAO_().getTransactionTypeCode().equals(argTransactionTypeCode)) ) {
      getDAO_().setTransactionTypeCode( argTransactionTypeCode );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the trans_cancel_reascode field.
   * @return The value of the field.
   */
  public String getTransactionCancelledReasonCode() {
    return getDAO_().getTransactionCancelledReasonCode();
  }

  /**
   * Sets the value of the trans_cancel_reascode field.
   * @param argTransactionCancelledReasonCode The new value for the field.
   */
  public void setTransactionCancelledReasonCode(String argTransactionCancelledReasonCode) {
    if( setTransactionCancelledReasonCode_noev(argTransactionCancelledReasonCode) ) {
      events_.post(dtv.xst.dao.trn.IPosTransaction.SET_TRANSACTIONCANCELLEDREASONCODE, argTransactionCancelledReasonCode);
    }
  }
  public boolean setTransactionCancelledReasonCode_noev(String argTransactionCancelledReasonCode) {
    boolean ev_postable = false;
    if( (getDAO_().getTransactionCancelledReasonCode() == null && argTransactionCancelledReasonCode != null ) ||
        (getDAO_().getTransactionCancelledReasonCode() != null && !getDAO_().getTransactionCancelledReasonCode().equals(argTransactionCancelledReasonCode)) ) {
      getDAO_().setTransactionCancelledReasonCode( argTransactionCancelledReasonCode );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the generic_storage_flag field.
   * @return The value of the field.
   */
  public boolean getGenericStorage() {
    if (getDAO_().getGenericStorage() != null) {
      return getDAO_().getGenericStorage().booleanValue();
    }
    else {
      return false;
    }
  }

  /**
   * Sets the value of the generic_storage_flag field.
   * @param argGenericStorage The new value for the field.
   */
  public void setGenericStorage(boolean argGenericStorage) {
    if( setGenericStorage_noev(argGenericStorage) ) {
      events_.post(dtv.xst.dao.trn.IPosTransaction.SET_GENERICSTORAGE, new Boolean(argGenericStorage));
    }
  }
  public boolean setGenericStorage_noev(boolean argGenericStorage) {
    boolean ev_postable = false;
    if( (getDAO_().getGenericStorage() == null && new Boolean(argGenericStorage) != null ) ||
        (getDAO_().getGenericStorage() != null && !getDAO_().getGenericStorage().equals(new Boolean(argGenericStorage))) ) {
      getDAO_().setGenericStorage( new Boolean(argGenericStorage) );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the operator_party_id field.
   * @return The value of the field.
   */
  public long getOperatorPartyId() {
    if (getDAO_().getOperatorPartyId() != null) {
      return getDAO_().getOperatorPartyId().longValue();
    }
    else {
      return 0; // no default specified in the dtx; we default to 0
    }
  }

  /**
   * Sets the value of the operator_party_id field.
   * @param argOperatorPartyId The new value for the field.
   */
  public void setOperatorPartyId(long argOperatorPartyId) {
    if( setOperatorPartyId_noev(argOperatorPartyId) ) {
      events_.post(dtv.xst.dao.trn.IPosTransaction.SET_OPERATORPARTYID, new Long(argOperatorPartyId));
    }
  }
  public boolean setOperatorPartyId_noev(long argOperatorPartyId) {
    boolean ev_postable = false;
    if( (getDAO_().getOperatorPartyId() == null && new Long(argOperatorPartyId) != null ) ||
        (getDAO_().getOperatorPartyId() != null && !getDAO_().getOperatorPartyId().equals(new Long(argOperatorPartyId))) ) {
      getDAO_().setOperatorPartyId( new Long(argOperatorPartyId) );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the post_void_flag field.
   * @return The value of the field.
   */
  public boolean getPostVoid() {
    if (getDAO_().getPostVoid() != null) {
      return getDAO_().getPostVoid().booleanValue();
    }
    else {
      return false;
    }
  }

  /**
   * Sets the value of the post_void_flag field.
   * @param argPostVoid The new value for the field.
   */
  public void setPostVoid(boolean argPostVoid) {
    if( setPostVoid_noev(argPostVoid) ) {
      events_.post(dtv.xst.dao.trn.IPosTransaction.SET_POSTVOID, new Boolean(argPostVoid));
    }
  }
  public boolean setPostVoid_noev(boolean argPostVoid) {
    boolean ev_postable = false;
    if( (getDAO_().getPostVoid() == null && new Boolean(argPostVoid) != null ) ||
        (getDAO_().getPostVoid() != null && !getDAO_().getPostVoid().equals(new Boolean(argPostVoid))) ) {
      getDAO_().setPostVoid( new Boolean(argPostVoid) );
      ev_postable = true;
    }
    return ev_postable;
  }

  /**
   * Gets the value of the cash_drawer_id field.
   * @return The value of the field.
   */
  public String getCashDrawerId() {
    return getDAO_().getCashDrawerId();
  }

  /**
   * Sets the value of the cash_drawer_id field.
   * @param argCashDrawerId The new value for the field.
   */
  public void setCashDrawerId(String argCashDrawerId) {
    if( setCashDrawerId_noev(argCashDrawerId) ) {
      events_.post(dtv.xst.dao.trn.IPosTransaction.SET_CASHDRAWERID, argCashDrawerId);
    }
  }
  public boolean setCashDrawerId_noev(String argCashDrawerId) {
    boolean ev_postable = false;
    if( (getDAO_().getCashDrawerId() == null && argCashDrawerId != null ) ||
        (getDAO_().getCashDrawerId() != null && !getDAO_().getCashDrawerId().equals(argCashDrawerId)) ) {
      getDAO_().setCashDrawerId( argCashDrawerId );
      ev_postable = true;
    }
    return ev_postable;
  }

  private HistoricalList<dtv.xst.dao.trn.IPosTransactionProperties> properties_;
  // So that rollback() reverts to proper list items.
  private transient HistoricalList<dtv.xst.dao.trn.IPosTransactionProperties> propertiesSavepoint_;
  private IParty operatorParty_;
  // So that we can revert back to a null if need be.
  private transient IParty operatorPartySavepoint_;
  private HistoricalList<dtv.xst.dao.trl.IRetailTransactionLineItem> retailTransactionLineItems_;
  // So that rollback() reverts to proper list items.
  private transient HistoricalList<dtv.xst.dao.trl.IRetailTransactionLineItem> retailTransactionLineItemsSavepoint_;
  private HistoricalList<dtv.xst.dao.trn.IPosTransactionLink> transactionLinks_;
  // So that rollback() reverts to proper list items.
  private transient HistoricalList<dtv.xst.dao.trn.IPosTransactionLink> transactionLinksSavepoint_;
  private HistoricalList<dtv.xst.dao.trn.ITransactionNotes> transactionNotes_;
  // So that rollback() reverts to proper list items.
  private transient HistoricalList<dtv.xst.dao.trn.ITransactionNotes> transactionNotesSavepoint_;

  public IDataModel getPosTransactionExt() {
    return myExtension_;
  }

  public void setPosTransactionExt(IDataModel argExt) {
    myExtension_ = argExt;
  }

  public java.util.List<dtv.xst.dao.trn.IPosTransactionProperties> getProperties() {
    if (properties_ == null) {
      properties_ = new HistoricalList<dtv.xst.dao.trn.IPosTransactionProperties>(null);
    }
    return properties_;
  }

  public void setProperties(java.util.List<dtv.xst.dao.trn.IPosTransactionProperties> argProperties) {
    if( properties_ == null ) {
    properties_ = new HistoricalList<dtv.xst.dao.trn.IPosTransactionProperties>(argProperties);
    } else {
      properties_.setCurrentList(argProperties);
    }

    // Propagate identification information through the graph.
    for( dtv.xst.dao.trn.IPosTransactionProperties child : properties_ ) {
      dtv.xst.dao.trn.impl.PosTransactionPropertiesModel model = (dtv.xst.dao.trn.impl.PosTransactionPropertiesModel) child;
      model.setOrganizationId_noev(this.getOrganizationId());
      model.setRetailLocationId_noev(this.getRetailLocationId());
      model.setBusinessDate_noev(this.getBusinessDate());
      model.setWorkstationId_noev(this.getWorkstationId());
      model.setTransactionSequence_noev(this.getTransactionSequence());
      if (child instanceof IDataModelImpl) {
        IDataAccessObject childDao = ((IDataModelImpl) child).getDAO();
        if (dtv.util.StringUtils.isEmpty(childDao.getOriginDataSource()) && 
            !dtv.util.StringUtils.isEmpty(this.getDAO().getOriginDataSource())) {
          childDao.setOriginDataSource(this.getDAO().getOriginDataSource());
        }
      }
      dtv.event.EventManager.registerEventHandler(eventCascade_, child);
    }
  }

  public void addPosTransactionProperties(dtv.xst.dao.trn.IPosTransactionProperties argPosTransactionProperties) {
    if (properties_ == null) {
      properties_ = new HistoricalList<dtv.xst.dao.trn.IPosTransactionProperties>(null);
    }
    argPosTransactionProperties.setOrganizationId(this.getOrganizationId());
    argPosTransactionProperties.setRetailLocationId(this.getRetailLocationId());
    argPosTransactionProperties.setBusinessDate(this.getBusinessDate());
    argPosTransactionProperties.setWorkstationId(this.getWorkstationId());
    argPosTransactionProperties.setTransactionSequence(this.getTransactionSequence());
    if (argPosTransactionProperties instanceof IDataModelImpl) {
      IDataAccessObject childDao = ((IDataModelImpl) argPosTransactionProperties).getDAO();
      if (dtv.util.StringUtils.isEmpty(childDao.getOriginDataSource()) && 
          !dtv.util.StringUtils.isEmpty(this.getDAO().getOriginDataSource())) {
        childDao.setOriginDataSource(this.getDAO().getOriginDataSource());
      }
    }
    // Register the _handleChildEvent method to receive events from the new child.
    dtv.event.EventManager.registerEventHandler(eventCascade_, new dtv.event.EventDescriptor( argPosTransactionProperties ));
    properties_.add(argPosTransactionProperties);
    events_.post(dtv.xst.dao.trn.IPosTransaction.ADD_PROPERTIES, argPosTransactionProperties);
  }

  public void removePosTransactionProperties(dtv.xst.dao.trn.IPosTransactionProperties argPosTransactionProperties) {
    if (properties_ != null) {
    // De-Register the _handleChildEvent method from receiving the child events.
    dtv.event.EventManager.deregisterEventHandler(eventCascade_, new dtv.event.EventDescriptor(argPosTransactionProperties));
      properties_.remove(argPosTransactionProperties);
      events_.post(dtv.xst.dao.trn.IPosTransaction.REMOVE_PROPERTIES, argPosTransactionProperties);
    }
  }

  public dtv.xst.dao.crm.IParty getOperatorParty() {
    return operatorParty_;
  }

  public void setOperatorParty( dtv.xst.dao.crm.IParty argOperatorParty) {
    // null out keys that define this relationship
    if( argOperatorParty == null) {
      // null out the keys that define this relationship of two independent objects.
      getDAO_().setOperatorPartyId(null);
      if ( operatorParty_ != null) {
        // De-register from the previous child.
        dtv.event.EventManager.deregisterEventHandler(eventCascade_, new dtv.event.EventDescriptor(operatorParty_));
      }
    }
    else {
      getDAO_().setOperatorPartyId(new Long(argOperatorParty.getPartyId()));
      // Register the cascading event handler to receive events from the new child.
      dtv.event.EventManager.registerEventHandler(eventCascade_, new dtv.event.EventDescriptor(argOperatorParty));

    }
    operatorParty_ = argOperatorParty;
    events_.post(dtv.xst.dao.trn.IPosTransaction.SET_OPERATORPARTY, argOperatorParty);
  }

  public java.util.List<dtv.xst.dao.trl.IRetailTransactionLineItem> getRetailTransactionLineItems() {
    if (retailTransactionLineItems_ == null) {
      retailTransactionLineItems_ = new HistoricalList<dtv.xst.dao.trl.IRetailTransactionLineItem>(null);
    }
    return retailTransactionLineItems_;
  }

  protected void setRetailTransactionLineItemsImpl(java.util.List<dtv.xst.dao.trl.IRetailTransactionLineItem> argRetailTransactionLineItems) {
    if( retailTransactionLineItems_ == null ) {
    retailTransactionLineItems_ = new HistoricalList<dtv.xst.dao.trl.IRetailTransactionLineItem>(argRetailTransactionLineItems);
    } else {
      retailTransactionLineItems_.setCurrentList(argRetailTransactionLineItems);
    }

    // notify children with inverse relationships that they have a parent.
    for( dtv.xst.dao.trl.IRetailTransactionLineItem child : retailTransactionLineItems_ ) {
      child.setParentTransaction(this);
    }
    // Propagate identification information through the graph.
    for( dtv.xst.dao.trl.IRetailTransactionLineItem child : retailTransactionLineItems_ ) {
      dtv.xst.dao.trl.impl.RetailTransactionLineItemModel model = (dtv.xst.dao.trl.impl.RetailTransactionLineItemModel) child;
      model.setOrganizationId_noev(this.getOrganizationId());
      model.setRetailLocationId_noev(this.getRetailLocationId());
      model.setBusinessDate_noev(this.getBusinessDate());
      model.setWorkstationId_noev(this.getWorkstationId());
      model.setTransactionSequence_noev(this.getTransactionSequence());
      if (child instanceof IDataModelImpl) {
        IDataAccessObject childDao = ((IDataModelImpl) child).getDAO();
        if (dtv.util.StringUtils.isEmpty(childDao.getOriginDataSource()) && 
            !dtv.util.StringUtils.isEmpty(this.getDAO().getOriginDataSource())) {
          childDao.setOriginDataSource(this.getDAO().getOriginDataSource());
        }
      }
      dtv.event.EventManager.registerEventHandler(eventCascade_, child);
    }
  }

  protected void addRetailTransactionLineItemImpl(dtv.xst.dao.trl.IRetailTransactionLineItem argRetailTransactionLineItem) {
    // notify children with inverse relationships that they have a parent.
    argRetailTransactionLineItem.setParentTransaction(this);
    if (retailTransactionLineItems_ == null) {
      retailTransactionLineItems_ = new HistoricalList<dtv.xst.dao.trl.IRetailTransactionLineItem>(null);
    }
    argRetailTransactionLineItem.setOrganizationId(this.getOrganizationId());
    argRetailTransactionLineItem.setRetailLocationId(this.getRetailLocationId());
    argRetailTransactionLineItem.setBusinessDate(this.getBusinessDate());
    argRetailTransactionLineItem.setWorkstationId(this.getWorkstationId());
    argRetailTransactionLineItem.setTransactionSequence(this.getTransactionSequence());
    if (argRetailTransactionLineItem instanceof IDataModelImpl) {
      IDataAccessObject childDao = ((IDataModelImpl) argRetailTransactionLineItem).getDAO();
      if (dtv.util.StringUtils.isEmpty(childDao.getOriginDataSource()) && 
          !dtv.util.StringUtils.isEmpty(this.getDAO().getOriginDataSource())) {
        childDao.setOriginDataSource(this.getDAO().getOriginDataSource());
      }
    }
    // Register the _handleChildEvent method to receive events from the new child.
    dtv.event.EventManager.registerEventHandler(eventCascade_, new dtv.event.EventDescriptor( argRetailTransactionLineItem ));
    retailTransactionLineItems_.add(argRetailTransactionLineItem);
    events_.post(dtv.xst.dao.trn.IPosTransaction.ADD_RETAILTRANSACTIONLINEITEMS, argRetailTransactionLineItem);
  }

  public void removeRetailTransactionLineItem(dtv.xst.dao.trl.IRetailTransactionLineItem argRetailTransactionLineItem) {
    if (retailTransactionLineItems_ != null) {
    // De-Register the _handleChildEvent method from receiving the child events.
    dtv.event.EventManager.deregisterEventHandler(eventCascade_, new dtv.event.EventDescriptor(argRetailTransactionLineItem));
      retailTransactionLineItems_.remove(argRetailTransactionLineItem);
    // notify children with inverse relationships that they have a parent.
    argRetailTransactionLineItem.setParentTransaction(null);
      events_.post(dtv.xst.dao.trn.IPosTransaction.REMOVE_RETAILTRANSACTIONLINEITEMS, argRetailTransactionLineItem);
    }
  }

  public java.util.List<dtv.xst.dao.trn.IPosTransactionLink> getTransactionLinks() {
    if (transactionLinks_ == null) {
      transactionLinks_ = new HistoricalList<dtv.xst.dao.trn.IPosTransactionLink>(null);
    }
    return transactionLinks_;
  }

  public void setTransactionLinks(java.util.List<dtv.xst.dao.trn.IPosTransactionLink> argTransactionLinks) {
    if( transactionLinks_ == null ) {
    transactionLinks_ = new HistoricalList<dtv.xst.dao.trn.IPosTransactionLink>(argTransactionLinks);
    } else {
      transactionLinks_.setCurrentList(argTransactionLinks);
    }

    // notify children with inverse relationships that they have a parent.
    for( dtv.xst.dao.trn.IPosTransactionLink child : transactionLinks_ ) {
      child.setParentTransaction(this);
    }
    // Propagate identification information through the graph.
    for( dtv.xst.dao.trn.IPosTransactionLink child : transactionLinks_ ) {
      dtv.xst.dao.trn.impl.PosTransactionLinkModel model = (dtv.xst.dao.trn.impl.PosTransactionLinkModel) child;
      model.setOrganizationId_noev(this.getOrganizationId());
      model.setRetailLocationId_noev(this.getRetailLocationId());
      model.setBusinessDate_noev(this.getBusinessDate());
      model.setWorkstationId_noev(this.getWorkstationId());
      model.setTransactionSequence_noev(this.getTransactionSequence());
      if (child instanceof IDataModelImpl) {
        IDataAccessObject childDao = ((IDataModelImpl) child).getDAO();
        if (dtv.util.StringUtils.isEmpty(childDao.getOriginDataSource()) && 
            !dtv.util.StringUtils.isEmpty(this.getDAO().getOriginDataSource())) {
          childDao.setOriginDataSource(this.getDAO().getOriginDataSource());
        }
      }
      dtv.event.EventManager.registerEventHandler(eventCascade_, child);
    }
  }

  public void addPosTransactionLink(dtv.xst.dao.trn.IPosTransactionLink argPosTransactionLink) {
    // notify children with inverse relationships that they have a parent.
    argPosTransactionLink.setParentTransaction(this);
    if (transactionLinks_ == null) {
      transactionLinks_ = new HistoricalList<dtv.xst.dao.trn.IPosTransactionLink>(null);
    }
    argPosTransactionLink.setOrganizationId(this.getOrganizationId());
    argPosTransactionLink.setRetailLocationId(this.getRetailLocationId());
    argPosTransactionLink.setBusinessDate(this.getBusinessDate());
    argPosTransactionLink.setWorkstationId(this.getWorkstationId());
    argPosTransactionLink.setTransactionSequence(this.getTransactionSequence());
    if (argPosTransactionLink instanceof IDataModelImpl) {
      IDataAccessObject childDao = ((IDataModelImpl) argPosTransactionLink).getDAO();
      if (dtv.util.StringUtils.isEmpty(childDao.getOriginDataSource()) && 
          !dtv.util.StringUtils.isEmpty(this.getDAO().getOriginDataSource())) {
        childDao.setOriginDataSource(this.getDAO().getOriginDataSource());
      }
    }
    // Register the _handleChildEvent method to receive events from the new child.
    dtv.event.EventManager.registerEventHandler(eventCascade_, new dtv.event.EventDescriptor( argPosTransactionLink ));
    transactionLinks_.add(argPosTransactionLink);
    events_.post(dtv.xst.dao.trn.IPosTransaction.ADD_TRANSACTIONLINKS, argPosTransactionLink);
  }

  public void removePosTransactionLink(dtv.xst.dao.trn.IPosTransactionLink argPosTransactionLink) {
    if (transactionLinks_ != null) {
    // De-Register the _handleChildEvent method from receiving the child events.
    dtv.event.EventManager.deregisterEventHandler(eventCascade_, new dtv.event.EventDescriptor(argPosTransactionLink));
      transactionLinks_.remove(argPosTransactionLink);
    // notify children with inverse relationships that they have a parent.
    argPosTransactionLink.setParentTransaction(null);
      events_.post(dtv.xst.dao.trn.IPosTransaction.REMOVE_TRANSACTIONLINKS, argPosTransactionLink);
    }
  }

  public java.util.List<dtv.xst.dao.trn.ITransactionNotes> getTransactionNotes() {
    if (transactionNotes_ == null) {
      transactionNotes_ = new HistoricalList<dtv.xst.dao.trn.ITransactionNotes>(null);
    }
    return transactionNotes_;
  }

  public void setTransactionNotes(java.util.List<dtv.xst.dao.trn.ITransactionNotes> argTransactionNotes) {
    if( transactionNotes_ == null ) {
    transactionNotes_ = new HistoricalList<dtv.xst.dao.trn.ITransactionNotes>(argTransactionNotes);
    } else {
      transactionNotes_.setCurrentList(argTransactionNotes);
    }

    // notify children with inverse relationships that they have a parent.
    for( dtv.xst.dao.trn.ITransactionNotes child : transactionNotes_ ) {
      child.setParentTransaction(this);
    }
    // Propagate identification information through the graph.
    for( dtv.xst.dao.trn.ITransactionNotes child : transactionNotes_ ) {
      dtv.xst.dao.trn.impl.TransactionNotesModel model = (dtv.xst.dao.trn.impl.TransactionNotesModel) child;
      model.setOrganizationId_noev(this.getOrganizationId());
      model.setRetailLocationId_noev(this.getRetailLocationId());
      model.setBusinessDate_noev(this.getBusinessDate());
      model.setWorkstationId_noev(this.getWorkstationId());
      model.setTransactionSequence_noev(this.getTransactionSequence());
      if (child instanceof IDataModelImpl) {
        IDataAccessObject childDao = ((IDataModelImpl) child).getDAO();
        if (dtv.util.StringUtils.isEmpty(childDao.getOriginDataSource()) && 
            !dtv.util.StringUtils.isEmpty(this.getDAO().getOriginDataSource())) {
          childDao.setOriginDataSource(this.getDAO().getOriginDataSource());
        }
      }
      dtv.event.EventManager.registerEventHandler(eventCascade_, child);
    }
  }

  protected void addTransactionNotesImpl(dtv.xst.dao.trn.ITransactionNotes argTransactionNotes) {
    // notify children with inverse relationships that they have a parent.
    argTransactionNotes.setParentTransaction(this);
    if (transactionNotes_ == null) {
      transactionNotes_ = new HistoricalList<dtv.xst.dao.trn.ITransactionNotes>(null);
    }
    argTransactionNotes.setOrganizationId(this.getOrganizationId());
    argTransactionNotes.setRetailLocationId(this.getRetailLocationId());
    argTransactionNotes.setBusinessDate(this.getBusinessDate());
    argTransactionNotes.setWorkstationId(this.getWorkstationId());
    argTransactionNotes.setTransactionSequence(this.getTransactionSequence());
    if (argTransactionNotes instanceof IDataModelImpl) {
      IDataAccessObject childDao = ((IDataModelImpl) argTransactionNotes).getDAO();
      if (dtv.util.StringUtils.isEmpty(childDao.getOriginDataSource()) && 
          !dtv.util.StringUtils.isEmpty(this.getDAO().getOriginDataSource())) {
        childDao.setOriginDataSource(this.getDAO().getOriginDataSource());
      }
    }
    // Register the _handleChildEvent method to receive events from the new child.
    dtv.event.EventManager.registerEventHandler(eventCascade_, new dtv.event.EventDescriptor( argTransactionNotes ));
    transactionNotes_.add(argTransactionNotes);
    events_.post(dtv.xst.dao.trn.IPosTransaction.ADD_TRANSACTIONNOTES, argTransactionNotes);
  }

  public void removeTransactionNotes(dtv.xst.dao.trn.ITransactionNotes argTransactionNotes) {
    if (transactionNotes_ != null) {
    // De-Register the _handleChildEvent method from receiving the child events.
    dtv.event.EventManager.deregisterEventHandler(eventCascade_, new dtv.event.EventDescriptor(argTransactionNotes));
      transactionNotes_.remove(argTransactionNotes);
    // notify children with inverse relationships that they have a parent.
    argTransactionNotes.setParentTransaction(null);
      events_.post(dtv.xst.dao.trn.IPosTransaction.REMOVE_TRANSACTIONNOTES, argTransactionNotes);
    }
  }

  private transient dtv.event.EventHandler eventCascade_ = new dtv.event.handler.CascadingHandler(this);


  @Override
  public Object getValue(String argFieldId) {
    if ("Properties".equals(argFieldId)) {
      return getProperties();
    }
    else if ("OperatorParty".equals(argFieldId)) {
      return getOperatorParty();
    }
    else if ("RetailTransactionLineItems".equals(argFieldId)) {
      return getRetailTransactionLineItems();
    }
    else if ("TransactionLinks".equals(argFieldId)) {
      return getTransactionLinks();
    }
    else if ("TransactionNotes".equals(argFieldId)) {
      return getTransactionNotes();
    }
    else if ("PosTransactionExtension".equals(argFieldId)) {
      return myExtension_;
    }
    else {
      return super.getValue(argFieldId);
    }
  }

  @Override
  public void setValue(String argFieldId, Object argValue) {
    if ("Properties".equals(argFieldId)) {
      setProperties(changeToList(argValue,dtv.xst.dao.trn.IPosTransactionProperties.class));
    }
    else if ("OperatorParty".equals(argFieldId)) {
      setOperatorParty((dtv.xst.dao.crm.IParty)argValue);
    }
    else if ("RetailTransactionLineItems".equals(argFieldId)) {
      setRetailTransactionLineItems(changeToList(argValue,dtv.xst.dao.trl.IRetailTransactionLineItem.class));
    }
    else if ("TransactionLinks".equals(argFieldId)) {
      setTransactionLinks(changeToList(argValue,dtv.xst.dao.trn.IPosTransactionLink.class));
    }
    else if ("TransactionNotes".equals(argFieldId)) {
      setTransactionNotes(changeToList(argValue,dtv.xst.dao.trn.ITransactionNotes.class));
    }
    else if ("PosTransactionExtension".equals(argFieldId)) {
      myExtension_ = (IDataModel)argValue;
    }
    else {
      super.setValue(argFieldId, argValue);
    }
  }

  protected dtv.xst.dao.trn.IPosTransactionProperties newProperty(String argPropertyName)  {
    dtv.xst.dao.trn.PosTransactionPropertiesId id = new dtv.xst.dao.trn.PosTransactionPropertiesId();

    id.setRetailLocationId(new Long(getRetailLocationId()));
    id.setBusinessDate(getBusinessDate());
    id.setWorkstationId(new Long(getWorkstationId()));
    id.setTransactionSequence(new Long(getTransactionSequence()));
    id.setPropertyCode(argPropertyName);

    dtv.xst.dao.trn.IPosTransactionProperties prop = dtv.data2.access.DataFactory.getInstance().createNewObject(id, dtv.xst.dao.trn.IPosTransactionProperties.class);
    return prop;
  }

  @Override
  public void startTransaction() {
    if (alreadyInStart_) {
      return;
    }
    else {
      alreadyInStart_ = true;
    }

    super.startTransaction();

    propertiesSavepoint_ = properties_;
    if (properties_ != null) {
      propertiesSavepoint_ = new HistoricalList<dtv.xst.dao.trn.IPosTransactionProperties>(properties_);
      java.util.Iterator it = properties_.iterator();
      while (it.hasNext()) {
        ((dtv.data2.access.IDataModel)it.next()).startTransaction();
      }
    }

    operatorPartySavepoint_ = operatorParty_;
    if (operatorParty_ != null) {
      operatorParty_.startTransaction();
    }

    retailTransactionLineItemsSavepoint_ = retailTransactionLineItems_;
    if (retailTransactionLineItems_ != null) {
      retailTransactionLineItemsSavepoint_ = new HistoricalList<dtv.xst.dao.trl.IRetailTransactionLineItem>(retailTransactionLineItems_);
      java.util.Iterator it = retailTransactionLineItems_.iterator();
      while (it.hasNext()) {
        ((dtv.data2.access.IDataModel)it.next()).startTransaction();
      }
    }

    transactionLinksSavepoint_ = transactionLinks_;
    if (transactionLinks_ != null) {
      transactionLinksSavepoint_ = new HistoricalList<dtv.xst.dao.trn.IPosTransactionLink>(transactionLinks_);
      java.util.Iterator it = transactionLinks_.iterator();
      while (it.hasNext()) {
        ((dtv.data2.access.IDataModel)it.next()).startTransaction();
      }
    }

    transactionNotesSavepoint_ = transactionNotes_;
    if (transactionNotes_ != null) {
      transactionNotesSavepoint_ = new HistoricalList<dtv.xst.dao.trn.ITransactionNotes>(transactionNotes_);
      java.util.Iterator it = transactionNotes_.iterator();
      while (it.hasNext()) {
        ((dtv.data2.access.IDataModel)it.next()).startTransaction();
      }
    }

    
    alreadyInStart_ = false;
  }

  @Override
  public void rollbackChanges() {
    if (isAlreadyRolledBack())
      return;

    super.rollbackChanges();

    properties_ = propertiesSavepoint_;
    propertiesSavepoint_ = null;
    if (properties_ != null) {
      java.util.Iterator it = properties_.iterator();
      while (it.hasNext()) {
        ((dtv.data2.access.IDataModel)it.next()).rollbackChanges();
      }
    }

    operatorParty_ = operatorPartySavepoint_;
    operatorPartySavepoint_ = null;
    if (operatorParty_ != null) {
      operatorParty_.rollbackChanges();
    }

    retailTransactionLineItems_ = retailTransactionLineItemsSavepoint_;
    retailTransactionLineItemsSavepoint_ = null;
    if (retailTransactionLineItems_ != null) {
      java.util.Iterator it = retailTransactionLineItems_.iterator();
      while (it.hasNext()) {
        ((dtv.data2.access.IDataModel)it.next()).rollbackChanges();
      }
    }

    transactionLinks_ = transactionLinksSavepoint_;
    transactionLinksSavepoint_ = null;
    if (transactionLinks_ != null) {
      java.util.Iterator it = transactionLinks_.iterator();
      while (it.hasNext()) {
        ((dtv.data2.access.IDataModel)it.next()).rollbackChanges();
      }
    }

    transactionNotes_ = transactionNotesSavepoint_;
    transactionNotesSavepoint_ = null;
    if (transactionNotes_ != null) {
      java.util.Iterator it = transactionNotes_.iterator();
      while (it.hasNext()) {
        ((dtv.data2.access.IDataModel)it.next()).rollbackChanges();
      }
    }

  }

  @Override
  public void commitTransaction() {
    if (alreadyInCommit_) {
      return;
    } else {
      alreadyInCommit_ = true;
    }

    super.commitTransaction();

    propertiesSavepoint_ = properties_;
    if (properties_ != null) {
      java.util.Iterator it = properties_.iterator();
      while (it.hasNext()) {
        ((dtv.data2.access.IDataModel)it.next()).commitTransaction();
      }
      properties_ = new HistoricalList<dtv.xst.dao.trn.IPosTransactionProperties>(properties_);
    }

    operatorPartySavepoint_ = operatorParty_;
    if (operatorParty_ != null) {
      operatorParty_.commitTransaction();
    }

    retailTransactionLineItemsSavepoint_ = retailTransactionLineItems_;
    if (retailTransactionLineItems_ != null) {
      java.util.Iterator it = retailTransactionLineItems_.iterator();
      while (it.hasNext()) {
        ((dtv.data2.access.IDataModel)it.next()).commitTransaction();
      }
      retailTransactionLineItems_ = new HistoricalList<dtv.xst.dao.trl.IRetailTransactionLineItem>(retailTransactionLineItems_);
    }

    transactionLinksSavepoint_ = transactionLinks_;
    if (transactionLinks_ != null) {
      java.util.Iterator it = transactionLinks_.iterator();
      while (it.hasNext()) {
        ((dtv.data2.access.IDataModel)it.next()).commitTransaction();
      }
      transactionLinks_ = new HistoricalList<dtv.xst.dao.trn.IPosTransactionLink>(transactionLinks_);
    }

    transactionNotesSavepoint_ = transactionNotes_;
    if (transactionNotes_ != null) {
      java.util.Iterator it = transactionNotes_.iterator();
      while (it.hasNext()) {
        ((dtv.data2.access.IDataModel)it.next()).commitTransaction();
      }
      transactionNotes_ = new HistoricalList<dtv.xst.dao.trn.ITransactionNotes>(transactionNotes_);
    }

    
    alreadyInCommit_ = false;
  }

  private void readObject(java.io.ObjectInputStream argStream)
                         throws java.io.IOException, ClassNotFoundException {
    argStream.defaultReadObject();

    // reinit our eventors
    events_ = new dtv.data2.access.ModelEventor(this);
    eventCascade_ = new dtv.event.handler.CascadingHandler(this);
  }


  // ------------------------------------------------------------- 
  // ------------------------------------------------------------- 
  // - CUSTOM CODE BELOW THIS POINT. EDIT IN THE APPROPRIATE DTJ - 
  // ------------------------------------------------------------- 
  // ------------------------------------------------------------- 

//DTJ-START $URL: https://node1000.gspann.local:8443/svn/20181115-cri/branches/fipay_cust_name/pos/src-bridged/dtv/xst/dao/trn/impl/PosTransactionModel.java $ $Id: PosTransactionModel.java 777 2014-03-27 02:15:29Z suz.jliu $
  
  protected transient dtv.util.ListMap<String, dtv.xst.dao.trl.IRetailTransactionLineItem> lineItemIndexByType_ = null;
  protected transient dtv.xst.daocommon.IRetailTransactionManager _transMgr;
  protected transient dtv.xst.pricing.EventedPricingTransaction _pricingShadow;

  private transient java.util.Date _custBirthDate = null;
  private transient java.math.BigDecimal _amountTendered = java.math.BigDecimal.ZERO;
  private transient java.math.BigDecimal _foodStampsAmountDue = java.math.BigDecimal.ZERO;
  
  /** {@inheritDoc} */
  @Override
  public java.util.List<? extends dtv.xst.dao.trl.IRetailTransactionLineItem> getPosLogRetailLineItems() {
    java.util.List<dtv.xst.dao.trl.IRetailTransactionLineItem> posLogLines =
        new java.util.ArrayList<dtv.xst.dao.trl.IRetailTransactionLineItem>();

    for (dtv.xst.dao.trl.IRetailTransactionLineItem line : getRetailTransactionLineItems()) {
      if (line.getTLogSequence() != dtv.xst.dao.trl.IRetailTransactionLineItemModel.EXCLUDE_FROM_POSLOG) {
        posLogLines.add(line);
      }
    }
    return posLogLines;
  }
  
  /*
   * {OVERRIDE GENERATED METHOD}
   */
  public java.math.BigDecimal getRoundedAmount() {
    return dtv.util.NumberUtils.nonNull(getRoundedAmountImpl());
  }

  /**
   * Gets the dollar amount of food stamps that can be applied to the 
   * transaction.  (Once the transaction is fully tendered, this should be zero.)
   * 
   * @return the amount of food stamps that can still be applied to the transaction 
   */
  public java.math.BigDecimal getFoodStampsAmountDue() {
    return _foodStampsAmountDue;
  }
  
  /**
   * Sets the dollar amount of food stamps that can be applied to the transaction.
   * 
   * @param argAmount  the new amount 
   */
  public void setFoodStampsAmountDue(java.math.BigDecimal argAmount) {
    _foodStampsAmountDue = argAmount;
  }

  public java.util.List<dtv.xst.dao.trl.IRetailTransactionLineItem> getLineItemsByTypeCode(String argLineItemTypeCode) {
    dtv.util.ListMap<String, dtv.xst.dao.trl.IRetailTransactionLineItem> itemsByType = getLineItemIndexByType(); 

    if (itemsByType.isEmpty()) {
      if (retailTransactionLineItems_ != null) {
        for (dtv.xst.dao.trl.IRetailTransactionLineItem line : retailTransactionLineItems_) {
          itemsByType.put(line.getLineItemTypeCode(), line);
        }
      }
    }

    if (!itemsByType.containsKey(argLineItemTypeCode)) {
      return java.util.Collections.emptyList();
    }
    return itemsByType.getList(argLineItemTypeCode);
  }
  
  public <T extends dtv.xst.dao.trl.IRetailTransactionLineItem> java.util.List<T> getLineItemsByTypeCode(String argLineItemTypeCode, Class<T> argInterface) {
    dtv.util.ListMap<String, dtv.xst.dao.trl.IRetailTransactionLineItem> itemsByType = getLineItemIndexByType();
    
    if (itemsByType.isEmpty()) {
      if (retailTransactionLineItems_ != null) {
        for (dtv.xst.dao.trl.IRetailTransactionLineItem line : retailTransactionLineItems_) {
          itemsByType.put(line.getLineItemTypeCode(), line);
        }
      }
    }

    if (!itemsByType.containsKey(argLineItemTypeCode)) {
      return java.util.Collections.emptyList();
    }
    
    java.util.List<dtv.xst.dao.trl.IRetailTransactionLineItem> found = itemsByType.getList(argLineItemTypeCode);
    java.util.List<T> results = java.util.Collections.checkedList(new java.util.ArrayList<T>(found.size()), argInterface);
    
    for (dtv.xst.dao.trl.IRetailTransactionLineItem line : found) {
      try {
        results.add(argInterface.cast(line));
      }
      catch (Exception ignored) {
      }
    }
    return results;
  }

  public java.util.List<? extends dtv.data2.access.IDataModel> getLineItemsByTypeCodeFiltered(String argLineItemTypeCode, String filterClassName) {
    
    java.util.List<dtv.xst.dao.trl.IRetailTransactionLineItem> lineItemsByTypeCode;
    dtv.xst.daocommon.ILineItemFilter lineItemFilter = null;
    try {
      lineItemFilter = (dtv.xst.daocommon.ILineItemFilter)Class.forName(filterClassName).newInstance();
    }
    catch (Exception ex) {//Class does not exist return the line items unfiltered
      org.apache.log4j.Logger.getLogger(getClass())
      .error("CAUGHT EXCEPTION getLineItemsByTypeCodeFiltered", ex);
      return getLineItemsByTypeCode(argLineItemTypeCode);
    }
    return lineItemFilter.filter(getLineItemsByTypeCode(argLineItemTypeCode));
  }

  public <T extends dtv.xst.dao.trl.IRetailTransactionLineItem> java.util.List<T> getLineItems(Class<T> argInterface) {
    java.util.List<dtv.xst.dao.trl.IRetailTransactionLineItem> found = getRetailTransactionLineItems();
    java.util.List<T> results = java.util.Collections.checkedList(new java.util.ArrayList<T>(found.size()), argInterface);
    
    for (dtv.xst.dao.trl.IRetailTransactionLineItem line : found) {
      try {
        results.add(argInterface.cast(line));
      }
      catch (Exception ignored) {
      }
    }
    return results;
  }

  public java.math.BigDecimal getAmountDue() {
    return dtv.util.NumberUtils.nonNull(getTotal()).subtract(getAmountTendered()); 
  }

  public java.math.BigDecimal getAmountTendered() {
    return dtv.util.NumberUtils.nonNull(_amountTendered);
  }

  public void setAmountTendered(java.math.BigDecimal argAmountTendered) {
    _amountTendered = argAmountTendered;
  }

  /*
   * {OVERRIDE GENERATED METHOD}
   */
  public void setBeginDatetimestamp(java.util.Date newValue) {
    setBeginDatetimestampImpl(newValue);
    setBeginTimeInt(dtv.util.DateUtils.date2TimeInt(newValue));
  }
  
  /*
   * {OVERRIDE GENERATED METHOD}
   */
  public void addRetailTransactionLineItem(dtv.xst.dao.trl.IRetailTransactionLineItem newItem) {
    synchronized (this) {
      newItem.setRetailTransactionLineItemSequence(getRetailTransactionLineItems().size() + 1);
      getLineItemIndexByType().put(newItem.getLineItemTypeCode(), newItem);
    }
    this.addRetailTransactionLineItemImpl(newItem);
  }
  
  /*
   * {OVERRIDE GENERATED METHOD}
   */
  public void addTransactionNotes(dtv.xst.dao.trn.ITransactionNotes argNote) {
    argNote.setNoteSequence(this.getTransactionNotes().size() + 1);
    addTransactionNotesImpl(argNote);
  }
  
  /**
   * Returns a retail location id object based on the retail location id and
   * the organization id.
   * @return Instance of the retail location id object associated with this transaction.
   */
  public dtv.xst.dao.loc.RetailLocationId getRetailLocationIdObject() {
    dtv.xst.dao.loc.RetailLocationId id = new dtv.xst.dao.loc.RetailLocationId();
    id.setRetailLocationId(new Long(this.getRetailLocationId()));
    return id;
  }

  /*
   * {OVERRIDE GENERATED METHOD}
   */
  public void setRetailTransactionLineItems(java.util.List<dtv.xst.dao.trl.IRetailTransactionLineItem> argRetailTransactionLineItems) {
    lineItemIndexByType_ = new dtv.util.DtvListHashMap<String, dtv.xst.dao.trl.IRetailTransactionLineItem>(10);
    
    for (dtv.xst.dao.trl.IRetailTransactionLineItem lineItem : argRetailTransactionLineItems) {
      getLineItemIndexByType().put(lineItem.getLineItemTypeCode(), lineItem);
    }
    
    setRetailTransactionLineItemsImpl(argRetailTransactionLineItems);
  }
  
  public dtv.xst.pricing.EventedPricingTransaction getPricing() {
    if (_pricingShadow == null) {
      _pricingShadow = dtv.xst.pricing.XSTPricingAdapter.getInstance().makeShadow( this );
    }
    return _pricingShadow;
  }

  public dtv.pricing2.Result<dtv.xst.dao.trl.IRetailTransactionLineItem, dtv.xst.pricing.XSTPricingDescriptor> getDealResults() {
    return getPricing().getBestDeals();
  }
  
  public dtv.pricing2.Result<dtv.xst.dao.trl.IRetailTransactionLineItem, dtv.xst.pricing.XSTPricingDescriptor> getSubstituteDealResults() {
    return getPricing().getSubstituteDeals();
  }
  
  public dtv.pricing2.Result<dtv.xst.dao.trl.IRetailTransactionLineItem, dtv.xst.pricing.XSTPricingDescriptor> getThresholdDealResults() {
    return getPricing().getThresholdDeals();
  }

  /** {@inheritDoc} */
  public java.util.List<dtv.pricing2.PricingDeal<dtv.xst.pricing.XSTPricingDescriptor>> getCurrentPossibleDeals(){
    return getPricing().getPossibleDeals();
  }
  
  public java.util.Date getCustBirthDate() {
    return _custBirthDate;
  }
  
  public void setCustBirthDate(java.util.Date custBirthDate) {
    _custBirthDate = custBirthDate;
  }
  
  public java.util.List<dtv.xst.dao.trl.IRetailTransactionLineItem> getSaleLineItems(){
    return getLineItemsByTypeCode("ITEM");
  }
  
  public java.util.List<dtv.xst.dao.trl.IRetailTransactionLineItem> getTaxLineItems(){
    return getLineItemsByTypeCode("TAX");
  }
  
  public java.util.List<dtv.xst.dao.trl.IRetailTransactionLineItem> getTenderLineItems(){
    return getLineItemsByTypeCode("TENDER");
  }
  
  public java.util.List<dtv.xst.dao.trl.IRetailTransactionLineItem> getDiscountLineItems(){
    return getLineItemsByTypeCode("DISCOUNT");
  }
  
  public dtv.pricing2.PricingDeal<XSTPricingDescriptor>[] getEligibleConditionalDeals() {
    return getPricing().getEligibleConditionalDeals();
  }
  
  private dtv.util.ListMap<String, dtv.xst.dao.trl.IRetailTransactionLineItem> getLineItemIndexByType() {
    if (lineItemIndexByType_ == null) {
      lineItemIndexByType_ = new dtv.util.DtvListHashMap<String, dtv.xst.dao.trl.IRetailTransactionLineItem>(10);
    }
    return lineItemIndexByType_;
  }
  
//DTJ-END $URL: https://node1000.gspann.local:8443/svn/20181115-cri/branches/fipay_cust_name/pos/src-bridged/dtv/xst/dao/trn/impl/PosTransactionModel.java $ $Id: PosTransactionModel.java 777 2014-03-27 02:15:29Z suz.jliu $

}
