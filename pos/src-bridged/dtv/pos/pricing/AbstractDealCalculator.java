// $Id: AbstractDealCalculator.java 1252 2018-04-26 20:12:37Z johgaug $
package dtv.pos.pricing;

import static dtv.data2.access.DataFactory.createObject;
import static dtv.pos.common.ConfigurationMgr.getDisplayProportionDealAmt;
import static dtv.pos.coupon.CouponHelper.COUPON_ID_PROPERTY;
import static dtv.pos.coupon.CouponHelper.makeCouponDealTrigger;
import static dtv.util.NumberUtils.*;
import static dtv.xst.dao.trl.IRetailTransactionLineItemModel.EXCLUDE_FROM_POSLOG;
import static java.math.RoundingMode.HALF_UP;

import java.math.BigDecimal;

import org.apache.log4j.Logger;

import dtv.data2.access.DataFactory;
import dtv.pos.register.type.LineItemType;
import dtv.pricing2.*;
import dtv.util.NumberUtils;
import dtv.xst.dao.trl.*;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.pricing.XSTPricingDescriptor;

/**
 * An abstract ancestor for calculators which add deal lines and modifiers to transactions.<br>
 * <br>
 * Copyright (c) 2011 Datavantage Corporation
 *
 * @author jweiss
 * @created May 13, 2011
 * @version $Revision: 1252 $
 */
public abstract class AbstractDealCalculator
  extends AbstractCalculator {

  private static final Logger _logger = Logger.getLogger(AbstractDealCalculator.class);
  private static final boolean _debugLogging = _logger.isDebugEnabled();

  /**
   * Adds a deal line item to the specified transaction, or updates an existing line if one has already been
   * attached to reflect this deal.
   *
   * @param argResult the meta-data describing the deal to be applied
   * @param argTrans the transaction to be modified
   * @return the deal line item added to <code>argTrans</code>
   */
  protected IRetailTransactionLineItem addDealLineItem(
    ResultApplication<IRetailTransactionLineItem, XSTPricingDescriptor> argResult, IPosTransaction argTrans) {

    String dealId = argResult.getDeal().getNativeDeal().getDealId();
    IRetailTransactionDealLineItem line = null;
    boolean newLine = true;

    // Try to find an existing, voided line item.
    for (IRetailTransactionDealLineItem item : argTrans.getLineItems(IRetailTransactionDealLineItem.class)) {
      if (item.getDealId().equals(dealId)) {
        item.setVoid(false);
        newLine = false;
        line = item;
      }
    }

    // If there isn't one, create and add it to the transaction.
    if (line == null) {
      line = DataFactory.createObject(IRetailTransactionDealLineItem.class);
      line.setDealId(dealId);
      line.setLineItemTypeCode(LineItemType.DEAL.getName());
      line.setReasonCode(argResult.getDeal().getNativeDeal().getReason().getName());
    }

    line.setAmount(argResult.getTransactionAmount());

    if (newLine) {
      argTrans.addRetailTransactionLineItem(line);
    }
    return line;
  }

  /**
   * Processes a deal applied to a transaction.
   *
   * @param argResult a deal determined to be applicable for <code>argTransaction</code>
   * @param argTransaction the transaction to be modified
   */
  protected void handleDealResult(
    ResultApplication<IRetailTransactionLineItem, XSTPricingDescriptor> argResult,
    IPosTransaction argTransaction) {

    if (!argResult.getDeal().isDeferred()) {
      final boolean transDeal = (isTransactionDeal(argResult));

      IRetailTransactionLineItem dealLine = addDealLineItem(argResult, argTransaction);

      /* Prior to Xstore 5.0, we only persisted transaction-wide deal line items; item-specific deals were
       * confined to representation via modifiers. In order to accommodate all of the 5.0 requirements for
       * serialized coupons, in which we need to maintain an association between all deal lines and the coupon
       * IDs that triggered them, we need to create deal lines without regard to line-wide/transaction-wide
       * distinctions. But to preserve consistency in the data/PosLog model, we'll create transient lines to
       * represent line-based deals; the coupon tagging information we need them for is only relevant within
       * the lifecyle of the current transaction. */
      if (!transDeal) {
        DataFactory.makeTransient(dealLine);
        dealLine.setTLogSequence(EXCLUDE_FROM_POSLOG);
      }

      // Determine if this deal is linked to a coupon and store the coupon id if it is.
      for (ICouponLineItem coupon : argTransaction.getLineItems(ICouponLineItem.class)) {
        if (coupon.getVoid()) {
          continue;
        }

        String couponTrigger = makeCouponDealTrigger(coupon);

        for (String dealTrigger : argResult.getDeal().getTriggers()) {
          if (couponTrigger.equalsIgnoreCase(dealTrigger)) {
            dealLine.setStringProperty(COUPON_ID_PROPERTY, coupon.getCouponId());
            break;
          }
        }
      }

      // Now make the price modifiers and attach the reason for those modifiers.
      for (ResultAppPredication<IRetailTransactionLineItem, XSTPricingDescriptor> p : argResult.getCauses()) {
        modifyLineItem(argResult.getDeal(), p, dealLine, transDeal);
        if (_debugLogging) {
          _logger.debug("Deal added for amount: " + p.getAmountBrokered());
        }
      }
    }
  }

  /**
   * Indicates whether the specified deal meta-data represents a transaction-wide deal.
   *
   * @param argResult the deal meta-data to interrogate
   * @return <code>true</code> if <code>argResult</code> represents a transaction-wide deal;
   * <code>false</code> otherwise
   */
  protected boolean isTransactionDeal(
    ResultApplication<IRetailTransactionLineItem, XSTPricingDescriptor> argResult) {

    return !NumberUtils.isZeroOrNull(argResult.getTransactionAmount());
  }

  /**
   * Applies price modifiers to a sale line to represent a deal applied to it.
   *
   * @param argDeal the deal to be applied
   * @param argPred meta-data describing the merchandise line to be affected by a deal
   * @param argDealLine the line item representing a transaction-wide deal and to be tied to any modifiers
   * created by this method; <code>null</code> if <code>argDeal</code> is not transaction-wide
   * @param argIsTransDeal <code>true</code> if <code>argDeal</code> is transaction-wide; <code>false</code>
   * otherwise
   */
  protected void modifyLineItem(PricingDeal<XSTPricingDescriptor> argDeal,
                                ResultAppPredication<IRetailTransactionLineItem, XSTPricingDescriptor> argPred,
                                IRetailTransactionLineItem argDealLine, boolean argIsTransDeal) {

    XSTPricingDescriptor deal = argDeal.getNativeDeal();
    IRetailPriceModifier mod = null;
    boolean newMod = true;

    /* First, search for a price modifier that we might have used before. If found, unvoid, and set to our
     * worker. */
    ISaleReturnLineItem saleLine = (ISaleReturnLineItem) argPred.getLineItem();

    for (IRetailPriceModifier cMod : saleLine.getRetailPriceModifierByTypeCode(deal.getReason().getName())) {
      if (cMod.getDealId().equals(deal.getDealId())) {
        mod = cMod;
        mod.setVoid(false);
        newMod = false;
      }
    }

    // Fallback to creating the modifier if it doesn't exist.
    if (mod == null) {
      mod = createObject(IRetailPriceModifier.class);
    }

    mod.setDealId(deal.getDealId());
    mod.setTaxabilityCode(deal.getTaxabilityCode());
    mod.setRetailPriceModifierReasonCode(deal.getReason().getName());

    BigDecimal amount =
      (saleLine.getReturn()) ? argPred.getAmountBrokered().negate() : argPred.getAmountBrokered();

    mod.setAmount(amount);
    mod.setExtendedAmount(nonNull(mod.getExtendedAmount()).add(amount));

    BigDecimal dealAmt = (saleLine.getReturn()) ? argPred.getDealAmount().negate() : argPred.getDealAmount();
//    mod.setDealAmount(nonNull(mod.getDealAmount()).add(dealAmt));

    mod.setPriceChangeAmount((deal.getReason().isOverride()) ? saleLine.getUnitPrice() : ZERO);

    mod.setNotes(deal.getDescription());
    mod.setDescription(deal.getDescription());

    if (argIsTransDeal) {
      /* Only associate the deal line with this modifier if this is a transaction-wide deal. Why? Because
       * pre-5.0 we only ever had deal lines for transaction-wide deals, and now we're carrying around
       * transient lines to represent line-level deals so that we can attach the serialized coupon ID to them.
       * Just trying to preserve legacy behavior/assumptions here. */
      mod.setReasonLineItem(argDealLine);
    }

    final String couponId = argDealLine.getStringProperty(COUPON_ID_PROPERTY);
    if (couponId != null) {
      mod.setSerialNumber(couponId);
    }

    if (newMod) {
      saleLine.addRetailPriceModifier(mod);
    }

    BigDecimal baseAmount = (getDisplayProportionDealAmt()) ? dealAmt : amount;
    BigDecimal unitPrice =
      saleLine.getUnitPrice().subtract(baseAmount.divide(saleLine.getQuantity(), 6, HALF_UP));

    mod.setDealAmount(nonNull(mod.getDealAmount()).add(baseAmount));
    saleLine.setUnitPrice((!saleLine.getReturn() && isNegative(unitPrice)) ? ZERO : unitPrice);
  }
}
