// $Id: UnitPriceResetCalculator.java 442 2012-09-28 14:16:03Z suz.sxie $
package dtv.pos.pricing;

import static dtv.pos.pricing.PricingHelper.containsPriceOverride;
import static dtv.util.NumberUtils.nonNull;

import java.math.BigDecimal;

import dtv.util.NumberUtils;
import org.apache.log4j.Logger;

import dtv.pos.common.CommonHelper;
import dtv.pos.register.returns.ReturnType;
import dtv.pos.register.tax.*;
import dtv.xst.dao.trl.*;
import dtv.xst.dao.trn.IPosTransaction;

/**
 * Establish the starting point prices for items. Copyright (c) 2005 Datavantage Corporation
 *
 * @author Greyson Fischer <gfischer@datavantagecorp.com>
 * @version $Revision: 442 $
 */
public class UnitPriceResetCalculator
  extends AbstractCalculator {

  private static final Logger logger_ = Logger.getLogger(UnitPriceResetCalculator.class);

  /** {@inheritDoc} */
  @Override
  public IRetailTransactionLineItem[] handleLineItemEvent(IRetailTransactionLineItem[] lineItems) {
    logger_.info("UnitPriceResetCalculator running now.");

    // Ensure lineItems exist, if not, there is no current transaction
    if ((lineItems == null) || (lineItems.length == 0)) {
      logger_.debug("No line items, bailing out.");
      return lineItems;
    }

    // Get the results of the deal calculation.
    IPosTransaction trans = lineItems[0].getParentTransaction();

    // If there are deal results, apply the line-item parts of the deals which
    // were applicable (this also resets the price of the item to it's given
    // base price)
    for (IRetailTransactionLineItem lineItem : trans.getRetailTransactionLineItems()) {
      if (lineItem instanceof ISaleReturnLineItem) {
        // Void all the line items that we might have had anything to do with,
        // and set the line item back to the pre-deal amount (if it's set)
        ISaleReturnLineItem item = (ISaleReturnLineItem) lineItem;

        // Handle verified return item differently.
        if ((item.getReturn() && ReturnType.valueOf(item.getReturnTypeCode()).isVerified())
          || item.getDisallowDealFlag()) {

          handleKnownPriceLine(item);
          continue;
        }

        // Return the item to the original price without deals.
        // Ensure non-null base price; See 165589 FB #73893
        BigDecimal baseUnitPrice = getBaseUnitPrice(item);
        item.setBaseUnitPrice(baseUnitPrice);
        item.setUnitPrice(baseUnitPrice);
        item.setPreDealAmount(baseUnitPrice);
      }
    }
    return lineItems;
  }

  /**
   * Returns the base unit price of <code>item</code>.
   *
   * @param item the item to check
   * @return the base unit price of the item
   */
  protected BigDecimal getBaseUnitPrice(ISaleReturnLineItem item) {
    BigDecimal baseUnitPrice = nonNull(item.getBaseUnitPrice());

    if (!containsPriceOverride(item) && !item.getVoid()) {
      baseUnitPrice = PricingHelper.getInstance().getCurrentItemPrice(item, item.getReturn());
    }

    // Need to remove VAT if the line is exempted
    return getVatAdjustedPrice(item, baseUnitPrice);
  }

  /**
   * Return the price of the item relative to the tax status of this sale line. This value will differ from
   * the provided price only when the item includes a VAT (tax) and the item has been exempted from tax.
   *
   * @param argLineItem the sale transaction item of concern
   * @param argPrice the current price the item is being sold at
   * @return the real price of the item, taking into account the possible exemption of VAT
   */
  protected BigDecimal getVatAdjustedPrice(ISaleReturnLineItem argLineItem, BigDecimal argPrice) {
    // Only worry about lines that are exempted from tax.
    if (TaxHelper.getInstance().isLineItemTaxExempt(argLineItem)) {
      for (ISaleTaxModifier mod : argLineItem.getTaxModifiers()) {
        if (mod.getVoid()) {
          continue;
        }

        TaxType taxType = TaxType.forName(mod.getSaleTaxGroupRule().getTaxTypeCode());
        // If VAT, then remove the tax amount from the provided price.
        if (TaxType.VAT == taxType) {
          // Calculate what the VAT amount would be
          VatTaxStrategy vat = new VatTaxStrategy();
          if (mod.getTranTaxableAmt() == null) {
            mod.setTranTaxableAmt(argPrice);
          }

          BigDecimal tax =
            vat.calculateRawAmount(argPrice, argLineItem.getQuantity(), mod, argLineItem.getBusinessDate(),
              argPrice);
          argPrice = argPrice.subtract(tax);
          /* The raw tax amount and exempt amount typically would be set in the TaxModifierCalculator, but the
           * problem is, if an exemption is applied when a customer attached to the transaction before any
           * items are rung, then when items are added, the real tax amount is never calculated. Since this
           * calculator modifies the unit price of items with exempt VAT taxing, when the calculateRawAmount
           * method is run in the aforementioned tax calculator, the amount calculated is incorrect as it is
           * based on this already modified price, instead of the base price of the item. Therefore, what we
           * do is set the exempt amount on the modifier here and leave it alone in the TaxModifierCalculator. */
          mod.setExemptTaxAmount(tax);
          mod.setRawTaxAmount(tax);
          // The taxable exempt amount typically would be set in the BaseTaxableAmountCalculator.
          // Reasons why this cannot be done and it is set here are explained there.
          mod.setTaxExemptAmount(argPrice);
        }
      }
    }

    return argPrice;
  }

  /**
   * Process a line item with an already-pre-determined price.
   * @param argLineItem the line item to process
   */
  protected void handleKnownPriceLine(ISaleReturnLineItem argLineItem) {
    BigDecimal qty = argLineItem.getQuantity();

    BigDecimal unitPrice = getVatAdjustedPrice(argLineItem, argLineItem.getBaseUnitPrice());
    if (argLineItem.getReturn()) {
      unitPrice = unitPrice.abs().negate();
    }

    for (IRetailPriceModifier mod : argLineItem.getRetailPriceModifierByTypeCode("DEAL")) {
      if (!mod.getVoid()) {
        BigDecimal amount = mod.getAmount();

        if (argLineItem.getReturn() && !NumberUtils.isPositive(amount)) {
          amount = amount.abs().negate();
        }

        unitPrice = unitPrice.subtract(amount.divide(qty, 6, CommonHelper.getInstance().getRoundingMethod()));
      }
    }
    argLineItem.setUnitPrice(unitPrice);
  }
}
