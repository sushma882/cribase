// $Id: PricingHelper.java 772 2014-03-18 09:33:33Z suz.dhu $
package dtv.pos.pricing;

import static dtv.data2.access.ModelEventor.PRIVILEGED_EVENT_DESCRIPTOR_NAME;
import static dtv.event.EventManager.registerEventHandler;
import static dtv.pos.common.IConfigurationMgr.STORE_TAG;
import static dtv.pos.common.IConfigurationMgr.SYSTEM_CONFIG_TAG;
import static dtv.pos.pricing.PriceProvider.getActualPrice;
import static dtv.pos.register.ItemLocator.getCurrentItemQuantity;
import static dtv.util.CompositeObject.make;
import static dtv.util.NumberUtils.equivalent;
import static dtv.xst.dao.trl.IRetailTransaction.SET_CUSTOMERPARTY;
import static dtv.xst.dao.trl.IRetailTransactionLineItem.SET_VOID;
import static dtv.xst.dao.trl.ISaleReturnLineItem.SET_QUANTITY;
import static dtv.xst.dao.trl.RetailPriceModifierReasonCode.forName;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;
import java.util.*;

import org.apache.log4j.Logger;

import dtv.data2.access.DataFactory;
import dtv.event.*;
import dtv.event.constraint.NameConstraint;
import dtv.hardware.types.HardwareType;
import dtv.pos.common.ConfigurationMgr;
import dtv.pos.framework.op.OpChainEventHelper;
import dtv.pos.register.ItemLocator;
import dtv.util.CompositeObject.TwoPiece;
import dtv.xst.dao.itm.IItem;
import dtv.xst.dao.trl.*;
import dtv.xst.dao.trn.IPosTransaction;

/**
 * Helper class for getting various pricing based attributes for items.<br>
 * <br>
 * Copyright (c) 2007 Datavantage Corporation
 * 
 * @author jculp
 * @version $Revision: 772 $
 * @created Mar 6, 2007
 */
public class PricingHelper
    implements IPricingHelper {

  private static final Logger _logger = Logger.getLogger(PricingHelper.class);
  private static final boolean _debugLogging = _logger.isDebugEnabled();
  private static final String GROSS_SALES_OPTIONS = "GrossSalesOptions";
  private static final IEventConstraint _setCustomerConstraint = new NameConstraint(SET_CUSTOMERPARTY);
  private static final IEventConstraint _setQuantityConstraint = new NameConstraint(SET_QUANTITY);
  private static final IEventConstraint _setVoidConstraint = new NameConstraint(SET_VOID);
  private static final IPricingHelper _instance;

  static {
    // Check the system configured adjuster to use...
    String className = System.getProperty(PricingHelper.class.getName());
    IPricingHelper temp = null;
    try {
      temp = (IPricingHelper) Class.forName(className).newInstance();
    }
    catch (Exception ex) {
      // Custom one was not configured; use the default.
      temp = new PricingHelper();
    }
    _instance = temp;
  }

  /**
   * Returns a flag indicating whether or not <code>argLineItem</code> has had any manual price overrides
   * applied to it.
   * 
   * @param argLineItem the line item to check for a manual price override
   * @return a flag indicating whether or not <code>argLineItem</code> has had any manual price overrides
   * applied to it
   */
  public static boolean containsPriceOverride(ISaleReturnLineItem argLineItem) {
    boolean containsOverride = false;

    for (IRetailPriceModifier priceMod : argLineItem.getRetailPriceModifiers()) {
      RetailPriceModifierReasonCode reason = forName(priceMod.getRetailPriceModifierReasonCode());

      if ((reason != null) && reason.isManualOverride()) {
        containsOverride = true;
      }
    }
    return containsOverride;
  }

  public static boolean getEnforceExcludeFromNetSalesFlag() {
    return ConfigurationMgr.getHelper().getBoolean(
        new String[] {STORE_TAG, SYSTEM_CONFIG_TAG, GROSS_SALES_OPTIONS, "EnforceExcludeFromNetSalesFlag"});
  }

  public static boolean getExcludeReturnsFromGrossSales() {
    return ConfigurationMgr.getHelper().getBoolean(
        new String[] {STORE_TAG, SYSTEM_CONFIG_TAG, GROSS_SALES_OPTIONS, "ExcludeReturns"});
  }

  public static boolean getExcludeVatFromNetSales() {
    return ConfigurationMgr.getHelper().getBoolean(
        new String[] {STORE_TAG, SYSTEM_CONFIG_TAG, "ExcludeVatFromNetSales"});
  }

  public static boolean getIncludeSalesTaxInGrossSales() {
    return ConfigurationMgr.getHelper().getBoolean(
        new String[] {STORE_TAG, SYSTEM_CONFIG_TAG, "IncludeSalesTaxInGrossSales"});
  }

  /**
   * Get the singleton instance of the class implementing IPricingHelper.
   * @return the class implementing the interface IPricingHelper.
   */
  public static IPricingHelper getInstance() {
    return _instance;
  }

  private final Map<TwoPiece<String, Boolean>, BigDecimal> _currentItemPriceMap =
      new HashMap<TwoPiece<String, Boolean>, BigDecimal>();

  private final EventHandler _setCustomerHandler = new EventHandler() {
    @Override
    protected void handle(Event argEvent) {
      if (_debugLogging) {
        _logger.debug("Repricing items on the transaction due to a customer association change.");
      }

      IRetailTransaction transaction = (IRetailTransaction) argEvent.getSource();
      Set<String> pricedItems = new HashSet<String>();

      for (ISaleReturnLineItem saleItem : transaction.getLineItems(ISaleReturnLineItem.class)) {
        if (!containsPriceOverride(saleItem) && !saleItem.getVoid() && !saleItem.getReturn()
            && !pricedItems.contains(saleItem.getItemId())) {

          BigDecimal itemQuantity =
              getCurrentItemQuantity(transaction, saleItem.getItemId(), saleItem.getReturn());

          BigDecimal newPrice = getActualPrice(saleItem.getItemId(), itemQuantity);
          newPrice = saleItem.getReturn() ? newPrice.negate() : newPrice;

          _currentItemPriceMap.put(make(saleItem.getItemId(), saleItem.getReturn()), newPrice);
          pricedItems.add(saleItem.getItemId());
        }
      }
    };
  };

  private final EventHandler _setQuantityHandler = new EventHandler() {
    @Override
    protected void handle(Event argEvent) {
      ISaleReturnLineItem saleItem = (ISaleReturnLineItem) argEvent.getSource();
      BigDecimal itemQuantity = getCurrentItemQuantity(saleItem.getItemId(), saleItem.getReturn());

      if (_debugLogging) {
        _logger.debug("Repricing item [" + saleItem.getItemId() + "] due to a quantity change.");
      }

      /* If this sale item does not belong to a transaction, then it is being newly added, so add its quantity
       * to the current item quantity. Otherwise, it represents a quantity change and its quantity has already
       * been included in the current item quantity on the transaction. */
      if (saleItem.getParentTransaction() == null) {
        itemQuantity = itemQuantity.add(((BigDecimal) argEvent.getPayload()));
      }

      BigDecimal newPrice = getActualPrice(saleItem.getItemId(), itemQuantity);
      newPrice = (saleItem.getReturn() && (newPrice != null)) ? newPrice.negate() : newPrice;
      _currentItemPriceMap.put(make(saleItem.getItemId(), saleItem.getReturn()), newPrice);
    }
  };

  private final EventHandler _setVoidHandler = new EventHandler() {
    @Override
    protected void handle(Event argEvent) {
      if (argEvent.getSource() instanceof ISaleReturnLineItem) {
        ISaleReturnLineItem saleItem = (ISaleReturnLineItem) argEvent.getSource();

        if (_debugLogging) {
          _logger.debug("Repricing item [" + saleItem.getItemId() + "] due to an item void.");
        }
        BigDecimal remainingQuantity = getCurrentItemQuantity(saleItem.getItemId(), saleItem.getReturn());
        // If there are none of this item left on the transaction, reprice the item for quantity of one
        remainingQuantity = (equivalent(remainingQuantity, ZERO)) ? ONE : remainingQuantity;

        BigDecimal newPrice = getActualPrice(saleItem.getItemId(), remainingQuantity);
        newPrice = saleItem.getReturn() ? newPrice.negate() : newPrice;

        _currentItemPriceMap.put(make(saleItem.getItemId(), saleItem.getReturn()), newPrice);
      }
    }
  };

  // An event handler responsible for responding to the completion/cancellation of the current transaction.
  private final EventHandler _transTerminatedHandler = new EventHandler() {
    /** {@inheritDoc} */
    @Override
    protected void handle(Event argEvent) {
      _currentItemPriceMap.clear();
    }
  };

  /**
   * Constructs a <code>PricingHelper</code>.
   */
  protected PricingHelper() {
    super();
    registerEventHandlers();
  }

  /** {@inheritDoc} */
  @Override
  public BigDecimal getCurrentItemPrice(ISaleReturnLineItem saleItem, boolean argReturn) {
    BigDecimal price = _currentItemPriceMap.get(make(saleItem.getItemId(), argReturn));

    /* @todo This hack is only temporary to get Xservices to work. They don't have the event wiring that
     * populates the pricing cache, but this brute force is indifferent to pricing quantities and isn't going
     * to cut it either. */
    if (price == null) {
      price = getActualPrice(saleItem.getItem().getItemId());
      /*403579 - POSLOG missing RegularSalesUnit price and Actual Sales unit price
       * This was due to missing price record. When there is no price xstore uses list price and when that transction was
       * suspended and resumed the price is null. Even though this is data issue CRI insisted on fixing in code.
       */
      if (price == null) {
        _logger.warn( "no current price for " + saleItem.getItem() + ", trying list price");
        price = saleItem.getItem().getListPrice();
      }
      
      if (argReturn && price != null) {
        price = price.negate();
      }
    }
    return price;
  }

  /** {@inheritDoc} */
  @Override
  public BigDecimal getStandAloneDealPrice(IItem argItem) {
    BigDecimal currentPrice = PriceProvider.getActualPrice(argItem.getItemId());
    IPosTransaction dummyTrans = DataFactory.createObject(IPosTransaction.class);

    ISaleReturnLineItem dummyLineItem =
        ItemLocator.getLocator().getSaleLineItem(argItem, SaleItemType.SALE, HardwareType.KEYBOARD);

    dummyLineItem.setBaseUnitPrice(currentPrice);
    dummyTrans.addRetailTransactionLineItem(dummyLineItem);

    AbstractDealCalculator calc = new Pricing2Calculator();
    List<IRetailTransactionLineItem> lineItems = dummyTrans.getRetailTransactionLineItems();

    IRetailTransactionLineItem[] postCalcLines =
        calc.handleLineItemEvent(lineItems.toArray(new IRetailTransactionLineItem[0]));

    ISaleReturnLineItem saleItem = (ISaleReturnLineItem) postCalcLines[0];
    BigDecimal promoPrice = saleItem.getExtendedAmount();

    return promoPrice;
  }

  /**
   * Get current deal price excluding multiple quantity deals (Bogo deals etc…) and including quantity 1 deals for transaction deal. 
   */
  @Override
  public BigDecimal getPriceExcludingMultipleQuantityDeals(IItem argItem) {
    BigDecimal currentPrice = PriceProvider.getActualPrice(argItem.getItemId());
    IPosTransaction dummyTrans = DataFactory.createObject(IPosTransaction.class);

    ISaleReturnLineItem dummyLineItem =
        ItemLocator.getLocator().getSaleLineItem(argItem, SaleItemType.SALE, HardwareType.KEYBOARD);

    dummyLineItem.setBaseUnitPrice(currentPrice);
    dummyTrans.addRetailTransactionLineItem(dummyLineItem);

    AbstractDealCalculator calc = new Pricing2Calculator();
    List<IRetailTransactionLineItem> lineItems = dummyTrans.getRetailTransactionLineItems();

    IRetailTransactionLineItem[] postCalcLines =
        calc.handleLineItemEvent(lineItems.toArray(new IRetailTransactionLineItem[0]));
    

    // Calculate applicable quantity 1 deals.
    ThresholdDealCalculator calcDeal = new ThresholdDealCalculator();
    postCalcLines = calcDeal.handleLineItemEvent(lineItems.toArray(new IRetailTransactionLineItem[0]));

    ISaleReturnLineItem saleItem = (ISaleReturnLineItem) postCalcLines[0];
    BigDecimal promoPrice = saleItem.getExtendedAmount();

    return promoPrice;
  }
  
  /**
   * Registers event handlers.
   */
  protected void registerEventHandlers() {
    OpChainEventHelper.registerTransTerminatedOcpHandler(_transTerminatedHandler, true);

    EventDescriptor privilegedDescriptor = new EventDescriptor(PRIVILEGED_EVENT_DESCRIPTOR_NAME);
    registerEventHandler(_setCustomerHandler, privilegedDescriptor, _setCustomerConstraint, true);
    registerEventHandler(_setQuantityHandler, privilegedDescriptor, _setQuantityConstraint, true);
    registerEventHandler(_setVoidHandler, privilegedDescriptor, _setVoidConstraint, true);
  }
}
