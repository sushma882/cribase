// $Id: DiscountsCalculator.java 155 2012-07-25 18:23:15Z dtvdomain\mschreiber $
package dtv.pos.pricing.discount;

import static dtv.pos.common.ConfigurationMgr.getLineDiscountUseConfiguredScale;
import static dtv.pos.common.ConfigurationMgr.getLineItemDiscountPrecision;
import static dtv.util.NumberUtils.*;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import org.apache.log4j.Logger;

import dtv.pos.pricing.discount.type.DiscountCalculationMethod;
import dtv.xst.dao.trl.*;

/**
 * Discount calculator that rounds each modifier to match the local currency precision.<br>
 * <br>
 * Copyright (c) 2008 Datavantage Corporation
 * 
 * @author dberkland
 * @version $Revision: 155 $
 * @created Nov 3, 2008
 */
public class DiscountsCalculator
    extends AbstractDiscountCalculator {

  private static final RoundingMode DISCOUNT_ROUNDING_MODE = RoundingMode.HALF_UP;
  private static final Logger _logger = Logger.getLogger(DiscountsCalculator.class);
  private static final boolean _debugLogging = _logger.isDebugEnabled();

  /**
   * Apply a discount to a group
   * 
   * @param argMods the modifiers to be updated
   * @param argGroupDiscountAmt the dollar amount of the discount
   * @param argLineAmountCalculator adapter used to determine how much of the discount applies to each line
   */
  protected void applyGroupTranDiscount(final List<IRetailPriceModifier> argMods,
      final BigDecimal argGroupDiscountAmt, final ILineAmountCalculator argLineAmountCalculator) {

    // keep track how much of the discount has been allocated (to help with penny rounding)
    BigDecimal remainingDiscountAmt = argGroupDiscountAmt;

    // keep track of which line had the biggest variance (used for allocating the final penny)
    IRetailPriceModifier maxRoundedModifier = null;
    // keep track of the biggest variance amount (used for allocating the final penny)
    BigDecimal maxRoundingAmt = ZERO;

    for (IRetailPriceModifier mod : argMods) {
      final ISaleReturnLineItem parentLineItem = mod.getParentLine();
      final BigDecimal unitPrice = parentLineItem.getUnitPrice();
      // set the price change amount here so we only do it once per calculator pass
      mod.setPriceChangeAmount(unitPrice);

      final BigDecimal quantity = parentLineItem.getQuantity();
      final BigDecimal lineExtendedPrice = unitPrice.multiply(quantity);

      // unrounded line discount amount
      BigDecimal rawDiscountAmt = argLineAmountCalculator.calc(lineExtendedPrice);

      // rounded line discount amount
      BigDecimal discountAmt =
          rawDiscountAmt.setScale(getCurrency().getDefaultFractionDigits(), DISCOUNT_ROUNDING_MODE);
      // how much was dropped
      BigDecimal roundingAmt = rawDiscountAmt.subtract(discountAmt).abs();
      // don't discount to less than zero
      // (this deals with low price and zero price items)
      if (isGreaterThan(discountAmt.abs(), lineExtendedPrice.abs())) {
        discountAmt = lineExtendedPrice;
        roundingAmt = ZERO;
      }
      // if we run out of discount to apply, give any remaining lines no more than the remaining amount
      // (this deals with extreme edge case of many very cheap items)
      if (isGreaterThan(discountAmt.abs(), remainingDiscountAmt.abs())) {
        discountAmt = remainingDiscountAmt;
        roundingAmt = ZERO;
      }
      // does the current line have the largest rounding variance??
      if ((maxRoundedModifier == null)
          || (isGreaterThan(discountAmt, ZERO) && (isGreaterThanOrEqual(maxRoundingAmt, roundingAmt)))) {

        maxRoundingAmt = roundingAmt;
        maxRoundedModifier = mod;
        if (_debugLogging) {
          _logger.debug(">>> LARGEST ROUNDING " + maxRoundingAmt + " ON "
              + maxRoundedModifier.getObjectIdAsString());
        }
      }
      if (_debugLogging) {
        _logger.debug(">>> DISCOUNTING (" + discountAmt + ") OF THE REMAINING " + remainingDiscountAmt);
      }
      // decrement the remaining discount amount
      remainingDiscountAmt = remainingDiscountAmt.subtract(discountAmt);

      // set the various amounts on the modifier
      setModDiscountAmount(mod, discountAmt);
    }

    // apply any remaining discount to the line with the biggest variance (or the last one if there is a tie)
    if (!isZero(remainingDiscountAmt)) {
      if (maxRoundedModifier == null) {
        _logger.error("NO MODIFER TO RECEIVE DISCOUNT DIFFERENCE OF " + remainingDiscountAmt);
      }
      else {
        if (_debugLogging) {
          _logger.debug(">>> APPLYING EXTRA " + remainingDiscountAmt + " FROM " + argGroupDiscountAmt
              + " TO " + maxRoundedModifier.getObjectIdAsString());
        }
        // add the variance to the existing modifier amount
        BigDecimal updatedAmt = maxRoundedModifier.getExtendedAmount().add(remainingDiscountAmt);
        // update the modifier with the new amount
        setModDiscountAmount(maxRoundedModifier, updatedAmt);
      }
    }
  }

  /**
   * Calculate the subtotal of lines that are parents of <tt>argMods</tt>.
   * 
   * @param argMods the modifiers to consider
   * @return the subtotal for items these modifiers apply to
   */
  protected BigDecimal calculateGroupSubtotal(List<IRetailPriceModifier> argMods) {
    BigDecimal subtotal = ZERO;

    for (IRetailPriceModifier mod : argMods) {
      ISaleReturnLineItem parentLineItem = mod.getParentLine();
      BigDecimal unitPrice = parentLineItem.getUnitPrice();

      if (unitPrice != null) {
        BigDecimal quantity = parentLineItem.getQuantity();
        if (quantity != null) {
          BigDecimal amount = unitPrice.multiply(quantity);
          subtotal = add(subtotal, amount);
        }
      }
    }
    return subtotal;
  }

  /**
   * Calculate all group discounts. Calculate percentage-based discounts before amount-based discount.
   * Amount-based discounts should be sorted by the sort order configured on the discount record
   * (dsc_discount.sort_order).
   */
  @Override
  protected void calculateGroupTransDiscount(Map<DiscountMapKey, List<IRetailPriceModifier>> argGroupTransMap) {
    if ((argGroupTransMap == null) || argGroupTransMap.isEmpty()) {
      return;
    }

    // Sort all group discounts in the right order.
    Set<IDiscountLineItem> discountLineItems =
        new TreeSet<IDiscountLineItem>(GroupTransactionDiscountLineComparator.getInstance());

    for (DiscountMapKey key : argGroupTransMap.keySet()) {
      discountLineItems.add(key.getDiscountLineItem());
    }

    for (IDiscountLineItem discountLine : discountLineItems) {
      DiscountCalculationMethod calcMethod = DiscountHelper.getDiscountCalculationMethod(discountLine);

      BigDecimal maxDiscountAmt = discountLine.getLineItemDiscount().getMaxDiscount();
      if (maxDiscountAmt == null) {
        maxDiscountAmt = discountLine.getAmount();
      }

      if ((discountLine instanceof IVoucherDiscountLineItem //
          )
          && calcMethod.usesPromptIfNeeded() //
          && calcMethod.isDollarAmountBased()) {
        BigDecimal discAmt = ((IVoucherDiscountLineItem) discountLine).getFaceValueAmount();

        if ((discAmt != null) && isGreaterThan(discAmt.abs(), maxDiscountAmt.abs())) {
          maxDiscountAmt = discAmt;
        }
      }

      discountLine.setAmount(maxDiscountAmt);
      if (isZero(maxDiscountAmt)) {
        maxDiscountAmt = null;
      }
      DiscountMapKey key = new DiscountMapKey(discountLine);

      if (calcMethod.isNewPriceBased()) {
        handleNewPriceGroupTransDiscount(argGroupTransMap.get(key), discountLine.getNewPrice(),
            discountLine.getNewPriceQuantity(), maxDiscountAmt);
      }
      else if (calcMethod.isDollarAmountBased()) {
        handleAmountBaseGroupTransDiscount(argGroupTransMap.get(key), discountLine.getAmount(),
            maxDiscountAmt);
      }
      else if (calcMethod.isPercentageBased()) {
        handlePercentBaseGroupTransDiscount(argGroupTransMap.get(key), discountLine.getPercent(),
            maxDiscountAmt);
      }
      else if (calcMethod.isCompetitivePriceBased() || calcMethod.isCompetitivePriceDiscountAmtBased()) {
        handleCompPriceBaseGroupTransDiscount(argGroupTransMap.get(key), discountLine.getNewPrice(),
            maxDiscountAmt);
      }
    }
  }

  /**
   * Apply an amount-based transaction or group discount to the specified set.
   * 
   * @param argMods the set of modifiers for the discount
   * @param argDiscountAmount the amount to discount
   * @param argMaxDiscountAmt an upper $-amount threshold for the discount, or <code>null</code> if no maximum
   * applied
   */
  protected void handleAmountBaseGroupTransDiscount(final List<IRetailPriceModifier> argMods,
      final BigDecimal argDiscountAmount, final BigDecimal argMaxDiscountAmt) {

    if ((argMods == null) || argMods.isEmpty()) {
      return;
    }

    // determine the group subtotal
    final BigDecimal subtotal = calculateGroupSubtotal(argMods);

    // how much is the total group discount
    BigDecimal groupDiscountAmt;
    // don't discount for more than the subtotal
    if (isGreaterThan(argDiscountAmount.abs(), subtotal.abs())) {
      groupDiscountAmt = subtotal;
    }
    else {
      groupDiscountAmt = argDiscountAmount;
    }

    // check if we hit a configured MAX for the discount
    if ((argMaxDiscountAmt != null) && isGreaterThan(groupDiscountAmt.abs(), argMaxDiscountAmt.abs())) {
      // use the max amount as the actual discount amount
      groupDiscountAmt = argMaxDiscountAmt;
    }
    ProratedLineAmountCalculator lineCalc = new ProratedLineAmountCalculator(subtotal, groupDiscountAmt);
    applyGroupTranDiscount(argMods, groupDiscountAmt, lineCalc);
  }

  protected void handleCompPriceBaseGroupTransDiscount(final List<IRetailPriceModifier> argMods,
      final BigDecimal argNewPrice, final BigDecimal argMaxDiscountAmt) {

    if ((argMods == null) || argMods.isEmpty()) {
      return;
    }
    final BigDecimal subtotal = calculateGroupSubtotal(argMods);

    // don't increase the price
    final BigDecimal newPrice;
    if (isGreaterThan(argNewPrice.abs(), subtotal.abs())) {
      newPrice = subtotal;
    }
    else {
      newPrice = argNewPrice;
    }
    // determine an overall discount amount
    BigDecimal totalDiscountAmt = subtotal.subtract(newPrice);

    // apply the discount
    handleAmountBaseGroupTransDiscount(argMods, totalDiscountAmt, argMaxDiscountAmt);
  }

  /**
   * Handle a line item discount
   * 
   * @param lineItem the line item to handle
   * @param modifier the price modifier
   */
  @Override
  protected void handleLineItemDiscount(ISaleReturnLineItem lineItem, IRetailPriceModifier modifier) {
    BigDecimal unitPrice = lineItem.getUnitPrice();
    BigDecimal discountAmount = null;
    DiscountCalculationMethod calcMethod = DiscountHelper.getDiscountCalculationMethod(modifier);
    BigDecimal priceChangeAmt = ZERO;
    BigDecimal maxDiscount = modifier.getDiscount().getMaxDiscount();

    // process the discount according to its type
    if ((calcMethod != null) && calcMethod.isDollarAmountBased()) {
      // negate the discount amount for returns
      discountAmount = modifier.getAmount();

      if ((maxDiscount != null) && isGreaterThan(discountAmount.abs(), maxDiscount.abs())) {
        discountAmount = maxDiscount;
      }

      if ((discountAmount != null) && isGreaterThan(discountAmount.abs(), unitPrice.abs())) {
        discountAmount = unitPrice;
      }
      unitPrice = unitPrice.subtract(discountAmount);

      // don't allow a promo gift card to reduce the price below zero
      if (calcMethod.usesPromptIfNeeded() && !lineItem.getReturn()) {
        if (isNegative(unitPrice)) {
          unitPrice = ZERO;
        }
      }
    }
    else if ((calcMethod != null)
        && (calcMethod.isCompetitivePriceBased() || calcMethod.isCompetitivePriceDiscountAmtBased())) {

      priceChangeAmt = modifier.getPriceChangeAmount();

      if (isGreaterThan(unitPrice.abs(), priceChangeAmt.abs())) {
        discountAmount = unitPrice.subtract(priceChangeAmt);
        unitPrice = priceChangeAmt;
      }
      else {
        discountAmount = ZERO;
      }
    }
    else if ((calcMethod != null) && calcMethod.isPercentageBased()) {
      final int scale = getLineDiscountUseConfiguredScale() //
          ? getLineItemDiscountPrecision() //
          : getCurrency().getDefaultFractionDigits();

      discountAmount = unitPrice.multiply(modifier.getPercent()).setScale(scale, roundingMode_);

      if ((maxDiscount != null) && isGreaterThan(discountAmount.abs(), maxDiscount.abs())) {
        discountAmount = maxDiscount;
      }

      if (isGreaterThan(discountAmount.abs(), unitPrice.abs())) {
        discountAmount = unitPrice;
      }
      unitPrice = unitPrice.subtract(discountAmount);
    }
    else if ((calcMethod != null) && calcMethod.isNewPriceBased()) {
      priceChangeAmt = modifier.getPriceChangeAmount();

      if (isGreaterThan(unitPrice.abs(), unitPrice.subtract(priceChangeAmt).abs())) {
        discountAmount = unitPrice.subtract(priceChangeAmt);
        unitPrice = priceChangeAmt;
      }
      else {
        discountAmount = ZERO;
      }
    }

    // Set the amount back on the discount modifier so that it can be displayed.
    modifier.setPriceChangeAmount(priceChangeAmt);
    modifier.setAmount(discountAmount);
    modifier.setExtendedAmount(discountAmount.multiply(lineItem.getQuantity()).setScale(roundingScale_,
        roundingMode_));

    lineItem.setUnitPrice(unitPrice);
  }

  /**
   * Apply an "X for Y"-based transaction or group discount to the specified set.
   * 
   * @param argMods the set of modifiers for the discount
   * @param argNewPrice the price (X) for the specified quantity
   * @param argNewPriceQty the quantity (Y) for the specified new price amount. If not specified (zero or
   * NULL), the price is assumed to apply to the entire group.
   * @param argMaxDiscountAmt an upper $-amount threshold for the discount, or <code>null</code> if no maximum
   * applied
   */
  protected void handleNewPriceGroupTransDiscount(List<IRetailPriceModifier> argMods,
      final BigDecimal argNewPrice, final BigDecimal argNewPriceQty, final BigDecimal argMaxDiscountAmt) {

    if ((argMods == null) || argMods.isEmpty()) {
      return;
    }

    // determine the X of "X for Y"
    final BigDecimal newPriceQty;
    if (isZeroOrNull(argNewPriceQty)) {
      // RDB--maintaining the existing behaviour, but I don't think this is always valid
      // shouldn't we be adding the quantities from the lines instead?
      newPriceQty = BigDecimal.valueOf(argMods.size());
    }
    else {
      newPriceQty = argNewPriceQty;
    }

    // calculate a unit price for 1st-(X-1)th item (e.g. 1st and 2nd item in "3 for" pricing)
    BigDecimal newUnitPrice =
        argNewPrice.divide(newPriceQty, getCurrency().getDefaultFractionDigits(), DISCOUNT_ROUNDING_MODE);

    // calculate a unit price for the last item (e.g. 3rd item in "3 for" pricing")
    BigDecimal lastItemUnitPrice = argNewPrice.subtract(newUnitPrice.multiply(newPriceQty.subtract(ONE)));
    // integer for X of "X for Y" pricing
    int maxCount = newPriceQty.intValue();

    // accumulate a new group total to pass to group discount calculation
    BigDecimal totalNewPrice = ZERO;
    // accumulate the current group subtotal to aid calculating a discount amount
    BigDecimal subtotal = ZERO;
    // accumulate the item quantities to identify which price to apply for each item
    int overallItemIndex = 0;
    for (IRetailPriceModifier mod : argMods) {
      final ISaleReturnLineItem parentLineItem = mod.getParentLine();
      final BigDecimal unitPrice = parentLineItem.getUnitPrice();
      final BigDecimal quantity = parentLineItem.getQuantity();
      final BigDecimal extendedAmount = unitPrice.multiply(quantity);
      subtotal = add(subtotal, extendedAmount);

      for (int i = 0; i < quantity.intValue(); i++ ) {
        overallItemIndex++ ;

        // determine if we are at an Xth item, if so, use the rounding-correction price
        final BigDecimal newAmount;
        if (overallItemIndex % maxCount == 0) {
          newAmount = lastItemUnitPrice;
        }
        else {
          newAmount = newUnitPrice;
        }
        totalNewPrice = totalNewPrice.add(newAmount);
      }
    }

    // ensure the new price isn't higher (RDB--Should we really be doing this?)
    if (isGreaterThan(totalNewPrice.abs(), subtotal.abs())) {
      totalNewPrice = subtotal;
    }
    // determine the difference
    BigDecimal totalDiscountAmt = subtotal.subtract(totalNewPrice);

    // check the difference against a potentially configured MAX
    if ((argMaxDiscountAmt != null) && isGreaterThan(totalDiscountAmt.abs(), argMaxDiscountAmt.abs())) {
      totalDiscountAmt = argMaxDiscountAmt;
    }

    // apply the discount
    handleAmountBaseGroupTransDiscount(argMods, totalDiscountAmt, null);
  }

  /**
   * Apply a percentage-based transaction or group discount to the specified set.
   * 
   * @param argMods the set of modifiers for the discount
   * @param argPercent the percentage of subtotal to discount
   * @param argMaxDiscountAmt an upper $-amount threshold for the discount, or <code>null</code> if no maximum
   * applied
   */
  protected void handlePercentBaseGroupTransDiscount(final List<IRetailPriceModifier> argMods,
      final BigDecimal argPercent, final BigDecimal argMaxDiscountAmt) {

    if ((argMods == null) || argMods.isEmpty()) {
      return;
    }

    // determine the group subtotal
    final BigDecimal subtotal = calculateGroupSubtotal(argMods);
    // take a percentage of that subtotal and round as the amount for transaction level
    final BigDecimal groupDiscountAmt =
        subtotal.multiply(argPercent).setScale(getCurrency().getDefaultFractionDigits(),
            DISCOUNT_ROUNDING_MODE);

    // check if we hit a configured MAX for the discount
    if ((argMaxDiscountAmt != null) && isGreaterThan(groupDiscountAmt, argMaxDiscountAmt)) {
      // use the max amount as the actual discount amount
      handleAmountBaseGroupTransDiscount(argMods, argMaxDiscountAmt, argMaxDiscountAmt);

    }
    else {
      PercentBasedLineAmountCalculator lineCalc = new PercentBasedLineAmountCalculator(argPercent);
      applyGroupTranDiscount(argMods, groupDiscountAmt, lineCalc);
    }
  }

  /**
   * Void all group and transaction discount price modifiers and starts from scratch. Update the group and
   * transaction discount maps. Reset all line item unit price back to pre-discount amount.
   * 
   * @param argRetailLineItems
   */
  @Override
  protected void initDiscount(IRetailTransactionLineItem[] argRetailLineItems) {
    for (IRetailTransactionLineItem item : argRetailLineItems) {
      if (item.getVoid() || (!(item instanceof ISaleReturnLineItem))) {
        continue;
      }

      ISaleReturnLineItem lineItem = (ISaleReturnLineItem) item;
      lineItem.setPrediscountAmount(lineItem.getUnitPrice());
      List<IRetailPriceModifier> modifiers = lineItem.getRetailPriceModifiers();

      for (IRetailPriceModifier modifier : modifiers) {
        if (!modifier.getVoid()) {
          RetailPriceModifierReasonCode modifierType =
              RetailPriceModifierReasonCode.forName(modifier.getRetailPriceModifierReasonCode());

          // Don't re-calculate verified return line item.
          if (isModifierFromVerifiedReturn(lineItem, modifier)) {
            if (modifierType.equals(RetailPriceModifierReasonCode.LINE_ITEM_DISCOUNT)
                || modifierType.equals(RetailPriceModifierReasonCode.TRANSACTION_DISCOUNT)
                || modifierType.equals(RetailPriceModifierReasonCode.GROUP_DISCOUNT)) {

              BigDecimal newUnitPrice = lineItem.getUnitPrice().abs().subtract(modifier.getAmount().abs());

              if (isPositive(lineItem.getUnitPrice())) {
                lineItem.setUnitPrice(newUnitPrice);
              }
              else {
                lineItem.setUnitPrice(newUnitPrice.negate());
              }
            }
          }
          else {
            // exclude out of the door discount as well as apply after tax discount.
            DiscountCalculationMethod calcMethod = DiscountHelper.getDiscountCalculationMethod(modifier);

            if (((calcMethod != null) && calcMethod.isOutTheDoor())) {
              continue;
            }

            if (modifierType.equals(RetailPriceModifierReasonCode.GROUP_DISCOUNT)
                && (modifier.getDiscountLineItem() != null)) {
              updateGroupTransDiscountMap(groupDiscountMap_, modifier);
            }
            else if (modifierType.equals(RetailPriceModifierReasonCode.TRANSACTION_DISCOUNT)
                && (modifier.getDiscountLineItem() != null)) {
              updateGroupTransDiscountMap(transDiscountMap_, modifier);
            }
            else if (modifierType.equals(RetailPriceModifierReasonCode.LINE_ITEM_DISCOUNT)) {
              updateLineItemDiscountMap(lineItemDiscountMap_, modifier);
            }
          }
        }
      }
    }
  }

  /**
   * Update the supplied modifier and it's parent line item with the specified discount amount.
   * 
   * @param argMod the modifier to work with
   * @param argDiscountAmount the discount amount for the modifier
   */
  protected void setModDiscountAmount(final IRetailPriceModifier argMod, BigDecimal argDiscountAmount) {
    if (_debugLogging) {
      _logger.debug(">>> SETTING (" + argMod.getObjectIdAsString() + ") --> " + argDiscountAmount);
    }
    final ISaleReturnLineItem parentLineItem = argMod.getParentLine();
    final BigDecimal quantity = parentLineItem.getQuantity();
    final BigDecimal lineQty = (isZero(quantity)) ? ONE : quantity;

    argMod.setExtendedAmount(argDiscountAmount);
    BigDecimal unitDiscountAmt = argDiscountAmount.divide(lineQty, roundingScale_, roundingMode_);
    argMod.setAmount(unitDiscountAmt);
    BigDecimal newUnitPrice = argMod.getPriceChangeAmount().subtract(unitDiscountAmt);
    parentLineItem.setUnitPrice(newUnitPrice);
  }

  /**
   * Interface used to abstract calculating how much of a transaction discount applies to a given line.
   */
  protected static interface ILineAmountCalculator {
    /**
     * Calculate the amount of discount for a specified line extended amount
     * 
     * @param argLineExtendedAmount the extended amount for a line
     * @return the amount of discount
     */
    public BigDecimal calc(BigDecimal argLineExtendedAmount);
  }

  /**
   * Used with a percentage-based discount to determine a discount amount for a single line.
   */
  protected static class PercentBasedLineAmountCalculator
      implements ILineAmountCalculator {

    private final BigDecimal percentage_;

    /**
     * Constructs a <code>PercentBasedLineAmountCalculator</code>.
     * @param argPercentage the discount percentage
     */
    protected PercentBasedLineAmountCalculator(BigDecimal argPercentage) {
      percentage_ = argPercentage;
    }

    @Override
    public BigDecimal calc(BigDecimal argLineExtendedAmount) {
      return argLineExtendedAmount.multiply(percentage_);
    }
  }

  /**
   * Used with amount-based discounts to pro-rate an overall discount amount to a single line.
   */
  protected static class ProratedLineAmountCalculator
      implements ILineAmountCalculator {

    private final BigDecimal subtotal_;
    private final BigDecimal groupDiscountAmt_;

    /**
     * Constructs a <code>ProratedLineAmountCalculator</code>.
     * 
     * @param argSubtotal the pre-discount subtotal for a group of items
     * @param argGroupDiscountAmt the total discount amount for a group of items
     */
    protected ProratedLineAmountCalculator(BigDecimal argSubtotal, BigDecimal argGroupDiscountAmt) {
      subtotal_ = argSubtotal;
      groupDiscountAmt_ = argGroupDiscountAmt;
    }

    @Override
    public BigDecimal calc(BigDecimal argLineExtendedAmount) {
      if (isZero(subtotal_) || isZero(argLineExtendedAmount)) {
        return ZERO;
      }
      BigDecimal ratio = argLineExtendedAmount.divide(subtotal_, roundingScale_, roundingMode_);
      return ratio.multiply(groupDiscountAmt_);
    }
  }
}
