//$Id: VoidDealOp.java 55414 2010-12-07 23:00:05Z dtvdomain\jweiss $
package dtv.pos.pricing.deal;

import java.util.List;

import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.register.IVoidCmd;
import dtv.util.StringUtils;
import dtv.xst.dao.prc.IDeal;
import dtv.xst.dao.prc.IDealTrigger;
import dtv.xst.dao.trl.*;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.pricing.XSTPricingAdapter;

/**
 * Operation handles a void selection on a price modifier resulting from a deal. Copyright (c) 2006
 * Datavantage Corporation
 * 
 * @author unattributed
 * @version $Revision: 55414 $
 */
public class VoidDealOp
    extends Operation {

  private static final long serialVersionUID = 1L;

  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    IVoidCmd cmd = (IVoidCmd) argCmd;

    IRetailPriceModifier modifier = (IRetailPriceModifier) cmd.getVoidObject();

    String dealId = modifier.getDealId(); 
   

    if (dealId == null) {
      return HELPER.completeResponse();
    }
    ISaleReturnLineItem lineItem = modifier.getParentLine();
    IPosTransaction parentTrans = lineItem.getParentTransaction();

    if (!voidManufacturerCoupon(dealId, parentTrans)) {
      if (!voidRetailerDealCoupon(dealId, parentTrans)) {
        // FIXME RDB this is probably ignored with the pricing2 deal engine, but I don't have time to check into it
        lineItem.addRemovedDealId(dealId);
      }
    }

    voidModifiers(dealId, parentTrans);
    return HELPER.completeResponse();
  }

  /**
   * Find and void any manufacturer's coupons for the given deal ID.
   * 
   * @param argDealId the deal ID
   * @param argParentTrans the transaction
   * @return <code>true</code> if the given deal ID is a manufacturer's coupon
   */
  private boolean voidManufacturerCoupon(String argDealId, IPosTransaction argParentTrans) {
    if (argDealId.startsWith("MANUF_COUPON-")) {
      return false;
    }
    String manuf = StringUtils.slice(argDealId, 13, 18);
    String valueCode = StringUtils.slice(argDealId, 19, 21);
    for (ICouponLineItem coupon : argParentTrans.getLineItems(ICouponLineItem.class)) {
      if (manuf.equals(coupon.getManufacturerId()) && valueCode.equals(coupon.getValueCode())) {
        coupon.setVoid(true);
      }
    }
    return true;
  }

  /**
   * Void all modifiers for the given deal ID.
   * 
   * @param argDealId the deal ID
   * @param argParentTrans the transaction with modifiers to be voided
   */
  private void voidModifiers(String argDealId, IPosTransaction argParentTrans) {
    for (ISaleReturnLineItem line : argParentTrans.getLineItems(ISaleReturnLineItem.class)) {
   // get the price modifiers
      List<IRetailPriceModifier> priceModifiers = line.getRetailPriceModifiers();
      IRetailPriceModifier mod = null;
      for(int i=0;i < priceModifiers.size();i++){
       
        mod = priceModifiers.get(i);
      
     // for (IRetailPriceModifier mod : line.getRetailPriceModifiers()) {
        if (argDealId.equals(mod.getDealId())) {
          mod.setVoid(true);
        }
      }
      }
      
    
  }

  /**
   * Find and void any deal coupon line items for the given deal ID.
   * 
   * @param argDealId the deal ID
   * @param argParentTrans the transaction
   * @return <code>true</code> if any coupon line items were found for the given deal ID
   */
  private boolean voidRetailerDealCoupon(String argDealId, IPosTransaction argParentTrans) {
    IDeal deal = XSTPricingAdapter.getInstance().getDealMap().get(argDealId);
    if (deal == null) {
      return false;
    }
    List<IDealTrigger> triggers = deal.getTriggers();
    boolean isCoupon = false;
    for (IDealTrigger trigger : triggers) {
      String tn = trigger.getTrigger();
      if (tn.startsWith("COUPON:")) {
        isCoupon = true;
        String[] parts = StringUtils.split(tn, ':');
        for (ICouponLineItem coupon : argParentTrans.getLineItems(ICouponLineItem.class)) {
          if (parts[1].equals(coupon.getCouponTypeCode()) && parts[2].equals(coupon.getCouponId())) {
            coupon.setVoid(true);
          }
        }
      }
    }
    return isCoupon;
  }

}
