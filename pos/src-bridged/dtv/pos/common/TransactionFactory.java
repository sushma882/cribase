// $Id: TransactionFactory.java 832 2015-03-11 21:35:27Z dtvdomain\pgolli $
package dtv.pos.common;

import org.apache.log4j.Logger;

import dtv.data2.access.DataFactory;
import dtv.pos.customer.CustomerHelper;
import dtv.pos.customer.loyalty.LoyaltyMgr;
import dtv.pos.framework.dataaccess.RetailTransactionMgrImpl;
import dtv.pos.iframework.ITransactionFactory;
import dtv.pos.iframework.ITransactionType;
import dtv.pos.storecalendar.StoreCalendar;
import dtv.pos.till.SessionManager;
import dtv.util.DateUtils;
import dtv.util.sequence.SequenceFactory;
import dtv.xst.dao.crm.IParty;
import dtv.xst.dao.trl.IRetailTransaction;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.trn.PosTransactionId;
import dtv.xst.dao.tsn.ISession;
import dtv.xst.daocommon.IRetailTransactionManager;

/**
 * The transaction factory is responsible for managing some of the common lifecycle activities of
 * transactions. This all flows from the base transaction interface (@see dtv.xst.dao.trn.IPosTransaction) <br>
 * <br>
 * Copyright (c) 2003 Datavantage Corporation
 * 
 * @author Tom Tamulewicz
 * @created February 13, 2003
 * @version $Revision: 832 $
 */
public class TransactionFactory
    implements ITransactionFactory {

  public static final String TRANSACTION_FACTORY_PROPERTY = "dtv.pos.transactionfactory";
  protected static final String TRANSACTION_SEQUENCE_TYPE = "TRANSACTION";

  private static ITransactionFactory _instance;

  private static final Logger logger_ = Logger.getLogger(TransactionFactory.class);

  static {
    // Check the syste configured factory to use...
    String className = System.getProperty(TRANSACTION_FACTORY_PROPERTY);
    try {
      _instance = (ITransactionFactory) Class.forName(className).newInstance();
    }
    catch (Exception ex) {
      _instance = new TransactionFactory();
    }
  }

  public static ITransactionFactory getInstance() {
    return _instance;
  }

  /* Log an error indicating that a transaction could not be created. Also indicate if a sequence number was
   * burned. */
  private static <T extends IPosTransaction> void logCreateTransactionError(String argType,
      PosTransactionId argTranId) {

    StringBuffer sb = new StringBuffer();
    sb.append(argType + " transaction " + "could not be created!");
    if ((argTranId != null) && (argTranId.getTransactionSequence().longValue() > 0)) {
      sb.append("  Sequence #");
      sb.append(argTranId.getTransactionSequence());
      sb.append(" was burned!");
    }
    else {
      sb.append("  No sequence # was burned.");
    }
    logger_.error(sb, new Throwable("STACK TRACE"));
  }

  private IRetailTransactionManager retailTransMgr_;

  protected TransactionFactory() {
    initialize();
  }

  /**
   * Create a new transaction for the specified transaction type.
   * 
   * @param argType the type of transaction to create
   * @param argOperator the current system operator
   * @return the created transaction
   * @see dtv.xst.dao.trn.IPosTransaction
   */
  @Override
  public <T extends IPosTransaction> T createTransaction(ITransactionType<T> argType, IParty argOperator) {
    // Get the interface class that maps to the specified type
    Class<T> interfaceClass = argType.getInterfaceClass();

    // Create the transaction id from the interface class
    PosTransactionId tranId = new PosTransactionId();

    // tms using new storecalendars to track the business date
    tranId.setBusinessDate(DateUtils.clearTime(StoreCalendar.getCurrentBusinessDate()));
    tranId.setRetailLocationId(new Long(ConfigurationMgr.getRetailLocationId()));
    tranId.setWorkstationId(new Long(ConfigurationMgr.getWorkstationId()));

    long transSeq = SequenceFactory.getNextLongValue(TRANSACTION_SEQUENCE_TYPE);
    tranId.setTransactionSequence(transSeq);

    T tran = DataFactory.createObject(tranId, interfaceClass);

    updateNewTransaction(tran, argType, argOperator);

    // Initialize all the pricing data and ready to be used.
    tran.getPricing();

    return tran;
  }

  /**
   * Initialize the transaction factory.
   */
  public void initialize() {
    retailTransMgr_ = new RetailTransactionMgrImpl();
    retailTransMgr_.init();
  }

  @Override
  public void resumeTransaction(IPosTransaction argTrans) {
    argTrans.setTransactionSequence(new Long(SequenceFactory.getNextLongValue(SEQ_TYPE_TRANSACTION)));
    argTrans.setWorkstationId(new Long(ConfigurationMgr.getWorkstationId()));
    argTrans.setBusinessDate(DateUtils.clearTime(StoreCalendar.getCurrentBusinessDate()));
    // Initialize all the pricing data and ready to be used.
    argTrans.getPricing();

    /* Perform some voodoo to ensure that party "0" isn't assigned to transactions to which no customer was
     * attached when they were suspended. There also may be some sketchy event registration/propagation magic
     * in the non-null customer case needed to ensure that party- specific deals/discounts are re-applied when
     * a transaction is resumed. */
    if (argTrans instanceof IRetailTransaction) {
      IRetailTransaction rtrans = (IRetailTransaction) argTrans;
      if (rtrans.getCustomerParty() != null) {
        rtrans.setCustomerParty(CustomerHelper.getInstance().searchPartyById(rtrans.getCustomerPartyId()));
      }
      else {
        rtrans.setCustomerParty(rtrans.getCustomerParty());
      }
    }

    updateNewTransaction(argTrans, argTrans.getTransactionTypeCode(), argTrans.getOperatorParty());
  }

  protected <T extends IPosTransaction> void updateNewTransaction(IPosTransaction argTrans,
      ITransactionType<T> argType, IParty argOperator) {

    updateNewTransaction(argTrans, argType.getName(), argOperator);
  }

  private <T extends IPosTransaction> void updateNewTransaction(IPosTransaction argTrans, String argType,
      IParty argOperator) {

    // Add a reference to the retail transaction manager if this is a retail trans.
    if (argTrans instanceof IRetailTransaction) {
      IRetailTransaction retailTrans = (IRetailTransaction) argTrans;
      retailTransMgr_.addTransaction(retailTrans);
      LoyaltyMgr.getInstance().setCurrentTransaction(retailTrans);
    }

    if (argTrans != null) {
      argTrans.setBeginDatetimestamp(DateUtils.getNewDate());
      argTrans.setPosted(false);
      argTrans.setOperatorParty(argOperator);
      argTrans.setTransactionStatusCode(TransactionStatus.NEW.getName());

      // set the transaction type
      argTrans.setTransactionTypeCode(argType);

      // Set the session if have not been set yet
      ISession session = SessionManager.getInstance().getSession();
      if (session != null) {
        argTrans.setSessionId(session.getSessionId());
      }
    }
    else {
      logCreateTransactionError(argType, null);
    }
  }
}
