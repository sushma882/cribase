// $Id: TransactionType.java 56234 2011-07-07 15:19:49Z dtvdomain\mreynolds $
package dtv.pos.common;

import java.util.*;

import org.apache.log4j.Logger;

import dtv.pos.iframework.ITransactionType;
import dtv.pos.iframework.type.AbstractCodeEnum;
import dtv.xst.dao.inv.*;
import dtv.xst.dao.thr.ITimeclockTransaction;
import dtv.xst.dao.trl.IEscrowTransaction;
import dtv.xst.dao.trl.IRetailTransaction;
import dtv.xst.dao.trn.*;
import dtv.xst.dao.tsn.*;

/**
 * The TransactionType class provides the type safe enum for all transaction types needed throughout the
 * application.<br>
 * Copyright (c) 2003 Datavantage Corporation
 * 
 * @author jhsiao
 * @created December 13, 2002
 * @version $Revision: 56234 $
 */
public class TransactionType<T extends IPosTransaction>
    extends AbstractCodeEnum
    implements ITransactionType<T> {

  private static final Logger logger_ = Logger.getLogger(TransactionType.class);

  /** searchable storage for instances of the class */
  private static final Map<String, TransactionType<? extends IPosTransaction>> values_ =
      new HashMap<String, TransactionType<? extends IPosTransaction>>();
  private static TransactionType<IPosTransaction>[] sortedInstances_;
  private static final Object SORT_LOCK = new Object();

  /* * * * * * * * * * START OF INSTANCES * * * * * * * * * */
  public static final TransactionType<ITimeclockTransaction> TIMECLOCK =
      new TransactionType<ITimeclockTransaction>("TIMECLOCK", ITimeclockTransaction.class);
  public static final TransactionType<INoSaleTransaction> NO_SALE = new TransactionType<INoSaleTransaction>(
      "NO_SALE", INoSaleTransaction.class);
  public static final TransactionType<IRetailTransaction> RETAIL_SALE =
      new TransactionType<IRetailTransaction>("RETAIL_SALE", IRetailTransaction.class);
  public static final TransactionType<ISessionControlTransaction> SESSION_CONTROL =
      new TransactionType<ISessionControlTransaction>("SESSION_CONTROL", ISessionControlTransaction.class);
  public static final TransactionType<ITenderControlTransaction> TENDER_CONTROL =
      new TransactionType<ITenderControlTransaction>("TENDER_CONTROL", ITenderControlTransaction.class);
  public static final TransactionType<ITenderControlTransaction> TENDER_EXCHANGE =
      new TransactionType<ITenderControlTransaction>("TENDER_EXCHANGE", ITenderControlTransaction.class);
  public static final TransactionType<IPosTransaction> SYSTEM_OPEN = new TransactionType<IPosTransaction>(
      "SYSTEM_OPEN", IPosTransaction.class);
  public static final TransactionType<IPosTransaction> SYSTEM_CLOSE = new TransactionType<IPosTransaction>(
      "SYSTEM_CLOSE", IPosTransaction.class);
  public static final TransactionType<IPosTransaction> WORKSTATION_OPEN =
      new TransactionType<IPosTransaction>("WORKSTATION_OPEN", IPosTransaction.class);
  public static final TransactionType<IPosTransaction> WORKSTATION_CLOSE =
      new TransactionType<IPosTransaction>("WORKSTATION_CLOSE", IPosTransaction.class);
  public static final TransactionType<IPosTransaction> WORKSTATION_START_REMOTE_CLOSE =
      new TransactionType<IPosTransaction>("WORKSTATION_START_REM_CLOSE", IPosTransaction.class);
  public static final TransactionType<IPosTransaction> WORKSTATION_COMPLETE_REMOTE_CLOSE =
      new TransactionType<IPosTransaction>("WORKSTATION_COMPLETE_REM_CLOSE", IPosTransaction.class);
  public static final TransactionType<IInventoryControlTransaction> INVENTORY_CONTROL =
      new TransactionType<IInventoryControlTransaction>("INVENTORY_CONTROL",
          IInventoryControlTransaction.class);
  public static final TransactionType<IPostVoidTransaction> POST_VOID =
      new TransactionType<IPostVoidTransaction>("POST_VOID", IPostVoidTransaction.class);
  public static final TransactionType<IPosTransaction> TRAINING_MODE_ENTRY =
      new TransactionType<IPosTransaction>("TRAINING_MODE_ENTRY", IPosTransaction.class);
  public static final TransactionType<IPosTransaction> TRAINING_MODE_EXIT =
      new TransactionType<IPosTransaction>("TRAINING_MODE_EXIT", IPosTransaction.class);
  public static final TransactionType<IExchangeRateTransaction> EXCHANGE_RATE =
      new TransactionType<IExchangeRateTransaction>("EXCHANGE_RATE", IExchangeRateTransaction.class);
  public static final TransactionType<IPosTransaction> BALANCE_INQUIRY =
      new TransactionType<IPosTransaction>("BALANCE_INQUIRY", IPosTransaction.class);

  public static final TransactionType<IRetailTransaction> CREDIT_APPLICATION =
      new TransactionType<IRetailTransaction>("CREDIT_APPLICATION", IRetailTransaction.class);
  public static final TransactionType<IPosTransaction> ACCOUNT_LOOKUP = new TransactionType<IPosTransaction>(
      "ACCOUNT_LOOKUP", IPosTransaction.class);

  public static final TransactionType<ITillControlTransaction> TILL_CONTROL =
      new TransactionType<ITillControlTransaction>("TILL_CONTROL", ITillControlTransaction.class);
  public static final TransactionType<IInventorySummaryCountTransaction> INVENTORY_SUMMARY_COUNT =
      new TransactionType<IInventorySummaryCountTransaction>("INVENTORY_SUMMARY_COUNT",
          IInventorySummaryCountTransaction.class);
  public static final TransactionType<IMovementPendingTransaction> MOVEMENT_PENDING =
      new TransactionType<IMovementPendingTransaction>("MOVEMENT_PENDING", IMovementPendingTransaction.class);

  public static final TransactionType<IEscrowTransaction> ESCROW = new TransactionType<IEscrowTransaction>(
      "ESCROW", IEscrowTransaction.class);

  public static final TransactionType<IPosTransaction> BATCH_CLOSE = new TransactionType<IPosTransaction>(
      "BATCH_CLOSE", IPosTransaction.class);
  
  public static final TransactionType<IPosTransaction> CONVEYED_BAGS = new TransactionType<IPosTransaction>(
      "CONVEYED_BAGS", IPosTransaction.class);

  /**
   * A transaction type for a non-monetary affecting transaction that is related to an order. Usually a
   * transaction of type will indicate some status change for an order and it will have properties indicating
   * the order and the status.
   */
  public static final TransactionType<IPosTransaction> ORDER = new TransactionType<IPosTransaction>("ORDER",
      IPosTransaction.class);

  /* * * * * * * * * * END OF INSTANCES * * * * * * * * * */

  /**
   * retrieval method to get the value for the specified name
   * 
   * @param argName the name of the value to get
   * @return null if an invalid name is selected
   */
  public static TransactionType<? extends IPosTransaction> forName(String argName) {
    if (argName == null) {
      return null;
    }
    TransactionType<? extends IPosTransaction> found = values_.get(argName.trim().toUpperCase());
    if (found == null) {
      logger_.warn("There is no instance of [" + TransactionType.class.getName() + "] named [" + argName
          + "].", new Throwable("STACK TRACE"));
    }
    return found;
  }

  /**
   * Get the instances in a sorted array.
   * 
   * @return the instances in a sorted array.
   */
  @SuppressWarnings("unchecked")
  public static TransactionType<IPosTransaction>[] getInstances() {
    synchronized (SORT_LOCK) {
      if (sortedInstances_ == null) {
        sortedInstances_ = values_.values().toArray(new TransactionType[0]);
        Arrays.sort(sortedInstances_);
      }
    }
    return sortedInstances_;
  }

  /** storage for the name of the value */
  private final Class<T> transactionInterface_;

  /**
   * constructor for the TransactionType object
   * 
   * @param argName name of the object
   * @param argTransactionInterface the class used for the type
   */
  protected TransactionType(String argName, Class<T> argTransactionInterface) {
    super(TransactionType.class, argName);
    transactionInterface_ = argTransactionInterface;
    values_.put(getCode(), this);
  }

  /**
   * Return the class for the data access object interface that maps to this transaction type.
   * 
   * @return the class for the data access object interface that maps to this transaction type.
   */
  @Override
  public Class<T> getInterfaceClass() {
    return transactionInterface_;
  }

  /**
   * Indicates whether the specified transaction has a type naming this enumeration instance.
   * 
   * @param argTransaction the reference transaction
   * @return <code>true</code> if <code>argTransaction</code>'s type names this enumeration instance;
   * <code>false</code> otherwise
   */
  public boolean matches(IPosTransaction argTransaction) {
    return (argTransaction != null) && matches(argTransaction.getTransactionTypeCode());
  }
}
