//$Id: PromptForShipNowOp.java 301 2012-08-30 20:04:42Z dtvdomain\mschreiber $
package dtv.pos.inventory.ship.op;

import org.apache.log4j.Logger;

import dtv.pos.common.OpChainKey;
import dtv.pos.common.PromptKey;
import dtv.pos.framework.action.XstDataAction;
import dtv.pos.framework.action.type.XstDataActionKey;
import dtv.pos.framework.ui.op.AbstractPromptOp;
import dtv.pos.iframework.action.IXstActionKey;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.iframework.ui.config.IPromptKey;
import dtv.pos.inventory.ship.ShippingHelper;
import dtv.xst.dao.inv.IInventoryControlDocument;
import dtv.xst.dao.inv.IInventoryStatuses;

/**
 * Ask the employee if they want to ship the document now. Copyright (c) 2004 Datavantage
 * Corporation
 * 
 * @author jdrummond
 * @created January 22, 2004
 * @version $Revision: 301 $
 */
public class PromptForShipNowOp
    extends AbstractPromptOp {

  private static final long serialVersionUID = 1L;
  private static final Logger logger_ = Logger.getLogger(PromptForShipNowOp.class);

  /**
   * return false if the inventory control document that has been selected is closed. If it's closed
   * then there's nothing to process.
   * 
   * @param argCmd current command
   * @return true if the operation is to be process; false otherwise.
   */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    if (!(argCmd instanceof IShippingCmd)) {
      logger_.error("cmd type mismatch. Expected type " + IShippingCmd.class.getName());
      return false;
    }

    IShippingCmd cmd = (IShippingCmd) argCmd;
    IInventoryControlDocument doc = cmd.getInvControlDoc();

    if (!ShippingHelper.getInstance().allowEdit(doc, cmd)) {
      return false;
    }

    if (IInventoryStatuses.CLOSED.equals(doc.getTempStatusCode())) {
      return false;
    }

    return super.isOperationApplicable(argCmd);
  }

  @Override
  protected IPromptKey getPromptKey(IXstCommand argCmd) {
    return PromptKey.valueOf("PROMPT_FOR_SHIP");
  }

  /**
   * handle the data coming back from the user. If they say yes then go through the ops listed in
   * SHIPPING_DO_SHIP_PREP chain.
   * 
   * @param argCmd the current command.
   * @param argEvent the current event (containing the response data).
   * @return IOpResponse
   */
  @Override
  protected IOpResponse handlePromptResponse(IXstCommand argCmd, IXstEvent argEvent) {
    if (argEvent instanceof XstDataAction) {
      final IXstActionKey key = ((XstDataAction) argEvent).getActionKey();
      IShippingCmd cmd = (IShippingCmd) argCmd;

      if (key == XstDataActionKey.YES || key == XstDataActionKey.ACCEPT) {
        cmd.setAllItemsShipped(true);

        return HELPER.getCompleteStackChainResponse(OpChainKey.valueOf("SHIPPING_DO_SHIP_PREP"));
      }
      else if (key == XstDataActionKey.NO) {
        cmd.setAllItemsShipped(false);

        return HELPER.completeResponse();
      }
    }

    return HELPER.waitResponse();
  }

}
