// $Id: ItemResultsEditModel.java 777 2014-03-27 02:15:29Z suz.jliu $
package dtv.pos.inventory.lookup;

import static dtv.hardware.types.HardwareType.KEYBOARD;
import static dtv.i18n.OutputContextType.VIEW;
import static dtv.pos.common.ConfigurationMgr.getItemLookupSalesHistoryWeeks;
import static dtv.pos.common.ConfigurationMgr.getRetailLocationId;
import static dtv.pos.framework.form.EditModelField.makeFieldDef;
import static dtv.pos.iframework.form.ICardinality.OPTIONAL;
import static dtv.pos.iframework.form.IEditModelFieldMetadata.ATTR_NEW;
import static dtv.pos.iframework.form.IEditModelFieldMetadata.ATTR_NO_SETTER;
import static dtv.util.NumberUtils.isGreaterThan;
import static dtv.xst.dao.trl.SaleItemType.SALE;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;
import java.util.*;

import javax.swing.table.TableModel;

import cri.pos.inventory.lookup.CriItemPromotionalData;

import cri.pos.pricing.CriPricingHelper;

import dtv.data2.access.*;
import dtv.i18n.FormattableFactory;
import dtv.i18n.IFormattable;
import dtv.pos.common.ConfigurationMgr;
import dtv.pos.framework.form.DaoEditModel;
import dtv.pos.framework.form.EditModelField;
import dtv.pos.iframework.form.*;
import dtv.pos.iframework.form.config.IDaoEditMappingConfig;
import dtv.pos.iframework.form.config.IFieldDependencyConfig;
import dtv.pos.iframework.security.SecuredObjectID;
import dtv.pos.inventory.lookup.style.DefaultItemDimensionValue;
import dtv.pos.kits.KitComponentDetailResult;
import dtv.pos.pricing.PriceProvider;
import dtv.pos.register.ItemLocator;
import dtv.pos.register.tax.TaxHelper;
import dtv.pos.register.tax.TaxModifierCalculator;
import dtv.pricing2.IHistoricalPrice;
import dtv.ui.UIResourceManager;
import dtv.util.StringUtils;
import dtv.xst.dao.inv.ISerializedStockLedger;
import dtv.xst.dao.inv.IStockLedger;
import dtv.xst.dao.itm.*;
import dtv.xst.dao.tax.ITaxGroupRule;
import dtv.xst.dao.tax.ITaxLocation;
import dtv.xst.dao.trl.*;
import dtv.xst.dao.trl.impl.RetailTransactionModel;

/**
 * An edit model to display the results of an item lookup.<br>
 * <br>
 * 
 * Copyright (c) 2003 Datavantage Corporation
 * 
 * @author Jeff Sheldon
 * @created May 20, 2003
 * @version $Revision: 777 $
 */
public class ItemResultsEditModel
    extends DaoEditModel
    implements IItemResultsEditModel {

  private static final IQueryKey<IStockLedger> ITEM_RESULTS_STOCK_LEDGER = new QueryKey<IStockLedger>(
      "ITEM_RESULTS_STOCK_LEDGER", IStockLedger.class);
  private static final IDimensionSummary DEFAULT_DIMENSION = new DefaultItemDimensionValue();
  private static final String STOCK_LEDGER_FIELD = "stockLedger";
  private static final String GRID_PAGE_VALUE_FIELD = "pageValue";
  private static final String ITEM_GRID_FIELD = "itemGrid";
  private static final String DIMENSION1_NAME_FIELD = "dimension1Name";
  private static final String DIMENSION2_NAME_FIELD = "dimension2Name";
  private static final String DIMENSION3_NAME_FIELD = "dimension3Name";
  private static final String DIMENSION1_FIELD = "dimension1";
  private static final String DIMENSION2_FIELD = "dimension2";
  private static final String DIMENSION3_FIELD = "dimension3";
  private static final String ITEM_IMAGE_FIELD = "itemImage";
  private static final String CURRENT_PRICE_FIELD = "currentPrice";
  private static final String BLIND_RETURN_PRICE_FIELD = "blindReturnPrice";
  private static final String CURRENT_TAX_FIELD = "currentTax";

  private final EditModelField<List> similarItemsDef_;
  private final EditModelField<List> substituteItemsDef_;
  private final EditModelField<List> priceHistoryDef_;
  private final EditModelField<List> upcDef_;
  private final EditModelField<List> itemPromotionalDef_;
  private final EditModelField<Object> dimensionDef_;
  private final EditModelField<List> serialNumberDef_;
  private final EditModelField<List> kitComponentsDef_;
  private final EditModelField<Object> vendorItemsDef_;
  private final EditModelField<List> itemTaxRatesDef_;

  private String styleMessage_;
  private List<?> tempDimensions_;
  private List<IItem> similarItem_;
  private Object vendorItem_;
  private List<IItem> substituteItem_;
  private List<IHistoricalPrice> priceHistory_;
  private List<ISerializedStockLedger> serialNumber_;
  private String pageHeader_;
  private String rowHeader_;
  private String columnHeader_;
  private String pageValue_;
  private List<IItemCrossReference> upc_;
  private List<CriItemPromotionalData> promotions_;
  private Object dimension_;
  private List<KitComponentDetailResult> kitComponents_;
  private String style_;
  private boolean isAddItemOptionVisible_;
  private boolean isSpecialOrderOptionVisible_;
  private boolean isLayawayOptionVisible_;
  private List<?> stockLedger_;
  private List<ItemTaxRateData> itemTaxRates_;

  private IItem _modeledItem;
  private IVendor vendor_;
  private AddItemOptions _addItemOptions;
  private int _currentPage;
  private ItemGridModel _gridModel;
  private String _dimension1Name;
  private String _dimension2Name;
  private String _dimension3Name;
  private String _dimension1;
  private String _dimension2;
  private String _dimension3;
  private BigDecimal _currentPrice;
  private BigDecimal _blindReturnPrice;
  private BigDecimal _currentTax;

  // Replenishment
  private Object thisYearSales_;
  private Object lastYearSales_;
  private Object previousYearSales_;
  private String salesHistoryTitle_;

  private Object itemSalesWeeksHistory_;
  private final EditModelField<Object> itemSalesWeeksHistoryDef_;

  /**
   * Constructor
   * 
   * @param argDaos the data objects to be proxied by this DaoEditModel
   * @param argConfig the field mapping configuration
   * @param argIsNew a value indicating is create or update permissions should be used. If <tt>null</tt>, the
   * isNew flag of each dao will be used instead.
   */
  public ItemResultsEditModel(IDataModel[] argDaos, IDaoEditMappingConfig argConfig, Boolean argIsNew) {
    super(argDaos, argConfig, argIsNew);

    for (IDataModel dataModel : argDaos) {
      if (dataModel instanceof IItem) {
        _modeledItem = (IItem) dataModel;
        _currentPrice = PriceProvider.getActualPrice(_modeledItem.getItemId());
        _blindReturnPrice = CriPricingHelper.getInstance().getLowestPrice(_modeledItem);
        _currentTax = calculateCurrentTax();
        break;
      }
    }

    similarItemsDef_ = EditModelField.makeFieldDef(this, "similarItem", List.class, 0, OPTIONAL);
    substituteItemsDef_ = EditModelField.makeFieldDef(this, "substituteItem", List.class, 0, OPTIONAL);
    priceHistoryDef_ = EditModelField.makeFieldDef(this, "priceHistory", List.class, 0, OPTIONAL);
    upcDef_ = EditModelField.makeFieldDef(this, "upcs", List.class, 0, OPTIONAL);
    itemPromotionalDef_ = EditModelField.makeFieldDef(this, "promotions", List.class, 0, OPTIONAL);
    dimensionDef_ = createDef("itemDimensions", ICardinality.OPTIONAL);
    serialNumberDef_ = EditModelField.makeFieldDef(this, "serialNumber", List.class, 0, OPTIONAL);
    vendorItemsDef_ = createDef("vendorItem", ICardinality.OPTIONAL);
    kitComponentsDef_ = EditModelField.makeFieldDef(this, "kitComponents", List.class, 0, OPTIONAL);
    itemTaxRatesDef_ = EditModelField.makeFieldDef(this, "itemTaxRates", List.class, 0, OPTIONAL);
    itemSalesWeeksHistoryDef_ = makeFieldDef(this, "itemSalesWeeksHistory", Object.class, 0, OPTIONAL);

    addField(createDef("thisYearSales", OPTIONAL));
    addField(createDef("lastYearSales", OPTIONAL));
    addField(createDef("previousYearSales", OPTIONAL));
    addField("salesHistoryTitle", String.class);
    addField(itemSalesWeeksHistoryDef_);
    setThisYearSales(ZERO);
    setLastYearSales(ZERO);
    setPreviousYearSales(ZERO);
    IFormattable arg = FF.getLiteral(Integer.valueOf(getItemLookupSalesHistoryWeeks()));
    IFormattable msg = FF.getTranslatable("_salesHistoryMsg1", new IFormattable[] {arg});
    setSalesHistoryTitle(msg.toString(VIEW));

    addFields(argDaos);
  }

  @Override
  public AddItemOptions getAddItemOptions() {
    return _addItemOptions;
  }

  /**
   * Returns the column header.
   * @return the column header
   */
  @Override
  public String getColumnHeader() {
    return columnHeader_;
  }

  /** {@inheritDoc} */
  @Override
  public int getCurrentPage() {
    return _currentPage;
  }

  public BigDecimal getCurrentPrice() {
    return _currentPrice;
  }
  
  public BigDecimal getBlindReturnPrice() {
    return _blindReturnPrice;
  }

  public BigDecimal getCurrentTax() {
    return _currentTax;
  }

  public String getDimension1() {
    return _dimension1;
  }

  public String getDimension1Name() {
    return _dimension1Name;
  }

  public String getDimension2() {
    return _dimension2;
  }

  public String getDimension2Name() {
    return _dimension2Name;
  }

  public String getDimension3() {
    return _dimension3;
  }

  public String getDimension3Name() {
    return _dimension3Name;
  }

  /** {@inheritDoc} */
  @Override
  public ItemGridModel getGridModel() {
    return _gridModel;
  }

  /**
   * Returns whether the currently modeled item has any inventory.
   * 
   * @return <code>true</code> if the currently modeled item has any inventory; <code>false</code> otherwise
   */
  public Boolean getHasInventory() {
    return new Boolean(isGreaterThan(getInventoryCount(), ZERO));
  }

  /**
   * Returns the inventory count of the currently modeled item.
   * @return the inventory count of the currently modeled item
   */
  @Override
  public BigDecimal getInventoryCount() {
    BigDecimal onHand = BigDecimal.ZERO;
    List ledgers = (List) getValue(STOCK_LEDGER_FIELD);

    if (ledgers != null) {
      for (Iterator it = ledgers.iterator(); it.hasNext();) {
        IStockLedger ledger = (IStockLedger) it.next();

        if (ConfigurationMgr.getOnHandInventoryBucketId().equals(ledger.getBucketId())) {
          onHand = ledger.getUnitcount();
        }
      }
    }

    return onHand;
  }

  /**
   * Returns the dimensions for the currently modeled item.
   * @return the dimensions for the currently modeled item
   */
  public Object getItemDimensions() {
    return dimension_;
  }

  /**
   * Returns the grid table model.
   * @return the grid table model
   */
  @Override
  public TableModel getItemGrid() {
    return _gridModel.getModel();
  }

  public String getItemImage() {
    String itemUrl = (String) getValue("itemUrl");

    if (StringUtils.isEmpty(itemUrl)) {
      itemUrl = UIResourceManager.getInstance().getString("_itemImageNotAvailable");
    }

    return itemUrl;
  }

  public Object getItemSalesWeeksHistory() {
    return this.itemSalesWeeksHistory_;
  }

  public EditModelField<Object> getItemSalesWeeksHistoryList() {
    return itemSalesWeeksHistoryDef_;
  }

  public List<ItemTaxRateData> getItemTaxRates() {
    return itemTaxRates_;
  }

  /**
   * Returns the collection of kit component details for the item
   * @return the collection of kit component details for the item
   */
  public List<KitComponentDetailResult> getKitComponents() {
    return kitComponents_;
  }

  public Object getLastYearSales() {
    return this.lastYearSales_;
  }

  /** {@inheritDoc} */
  @Override
  public IItem getModeledItem() {
    return _modeledItem;
  }

  /**
   * Returns the page header.
   * @return the page header
   */
  @Override
  public String getPageHeader() {
    return pageHeader_;
  }

  /**
   * Returns the page value.
   * @return the page value
   */
  @Override
  public String getPageValue() {
    return pageValue_;
  }

  public Object getPreviousYearSales() {
    return this.previousYearSales_;
  }

  /**
   * Returns the price history for the currently modeled item.
   * @return the price history for the currently modeled item
   */
  public List<IHistoricalPrice> getPriceHistory() {
    return priceHistory_;
  }

  /**
   * Returns the row header.
   * @return the row header
   */
  @Override
  public String getRowHeader() {
    return rowHeader_;
  }

  public String getSalesHistoryTitle() {
    return this.salesHistoryTitle_;
  }

  /**
   * @return Returns the serialNumber.
   */
  public List<ISerializedStockLedger> getSerialNumber() {
    return serialNumber_;
  }

  /**
   * Returns the item similar to the currently modeled item.
   * @return the item similar to the currently modeled item
   */
  public List<IItem> getSimilarItem() {
    return similarItem_;
  }

  /**
   * Returns the items similar to the currently modeled item.
   * @return the items similar to the currently modeled item
   */
  @SuppressWarnings("unchecked")
  public List getSimilarItemList() {
    return similarItemsDef_.getEnumeratedPossibleValues();
  }

  /**
   * Get the stock ledger
   * @return the stock ledger
   */
  @SuppressWarnings("unchecked")
  public List getStockLedger() {
    return stockLedger_;
  }

  /**
   * Returns the style of the currently modeled item.
   * @return the style of the currently modeled item
   */
  @Override
  public String getStyle() {
    return style_;
  }

  /**
   * Returns the message stating the style of the modeled item.
   * @return the message stating the style of the modeled item
   */
  public String getStyleMessage() {
    return styleMessage_;
  }

  /**
   * Returns the item that qualifies as a substitute for the currently modeled item.
   * 
   * @return the item that qualifies as a substitute for the currently modeled item
   */
  public List<IItem> getSubstituteItem() {
    return substituteItem_;
  }

  /**
   * Returns the items that qualify as substitutes for the currently modeled item.
   * 
   * @return the items that qualify as substitutes for the currently modeled item
   */
  @SuppressWarnings("unchecked")
  public List getSubstituteItemsList() {
    return substituteItemsDef_.getEnumeratedPossibleValues();
  }

  /**
   * Returns the temporary dimensions of the currently modeled item.
   * @return the temporary dimensions of the currently modeled item
   */
  @SuppressWarnings("unchecked")
  public List getTempDimensions() {
    return tempDimensions_;
  }

  public Object getThisYearSales() {
    return this.thisYearSales_;
  }

  /**
   * Returns the UPC of the currently modeled item.
   * @return the UPC of the currently modeled item
   */
  public List<IItemCrossReference> getUpcs() {
    return upc_;
  }

  /**
   * Returns the promotions of the currently modeled item.
   * @return the promotions of the currently modeled item
   */
  public List<CriItemPromotionalData> getPromotions() {
    return promotions_;
  }

  /**
   * Returns the vendor associated with the modeled item, or null if not available. Depending on the Operation
   * used with this model, the vendor data may not be available.
   * @return the vendor associated with the modeled item
   */
  public IVendor getVendor() {
    return vendor_;
  }

  /**
   * Returns the selected item from the list of items having the same vendor as the currently modeled item.
   * @return the selected item from the list of items having the same vendor as the currently modeled item
   */
  public Object getVendorItem() {
    /* If there is no selected item on the "same vendors" list, simply return the first item in the list. */
    if ((vendorItem_ == null) && ((getVendorItemList() != null) && (getVendorItemList().size() > 0))) {
      return getVendorItemList().get(0);
    }
    return vendorItem_;
  }

  /**
   * Returns the list of items having the same vendor as the currently modeled item.
   * @return the list of items having the same vendor as the currently modeled item
   */
  @SuppressWarnings("unchecked")
  public List getVendorItemList() {
    return vendorItemsDef_.getEnumeratedPossibleValues();
  }

  @Override
  public boolean isAddItemEnabled() {
    return _addItemOptions != null ? _addItemOptions.isAddItemAllowed() : false;
  }

  /**
   * Returns whether the "add item" function is visible.
   * 
   * @return <code>true</code> if the "add item" functionality is visible; <code>false</code> otherwise
   */
  public boolean isAddItemVisible() {
    return isAddItemOptionVisible_;
  }

  /**
   * Returns whether the current grid page is also the first.
   * 
   * @return <code>true</code> if the current grid page is also the first grid page; <code>false</code>
   * otherwise
   */
  @Override
  public boolean isFirstGridPage() {
    return _currentPage == 0;
  }

  /**
   * Returns whether the current grid page is also the last.
   * 
   * @return <code>true</code> if the current grid page is also the last grid page; <code>false</code>
   * otherwise
   */
  @Override
  public boolean isLastGridPage() {
    return (_currentPage == _gridModel.getPageCount() - 1) || (_gridModel.getPageCount() == 0);
  }

  /**
   * Returns whether the "Layaway" function is visible.
   * 
   * @return <code>true</code> if the "Layaway" functionality is visible; <code>false</code> otherwise
   */
  public boolean isLayawayVisible() {
    return isLayawayOptionVisible_;
  }

  /**
   * Returns whether the specified field has read-only access.
   * 
   * @param argFieldKey the ID of the field of interest
   * @return <tt>true</tt> if the field cannot be changed (e.g. primary key fields), otherwise <tt>false</tt>
   */
  @Override
  public boolean isReadOnly(String argFieldKey) {
    if (argFieldKey.equalsIgnoreCase(ITEM_GRID_FIELD) || argFieldKey.equalsIgnoreCase(GRID_PAGE_VALUE_FIELD)) {
      return false;
    }

    return super.isReadOnly(argFieldKey);
  }

  /**
   * Returns whether the "special order" function is visible.
   * 
   * @return <code>true</code> if the "special order" functionality is visible; <code>false</code> otherwise
   */
  public boolean isSpecialOrderVisible() {
    return isSpecialOrderOptionVisible_;
  }

  @Override
  public void setAddItemOptions(AddItemOptions argOptions) {
    _addItemOptions = argOptions;
  }

  /**
   * Sets whether the "add item" functionality is visible.
   * 
   * @param argAddItemVisible <code>true</code> if the "add item" functionality should be visible;
   * <code>false</code> if it should be invisible
   */
  public void setAddItemVisibility(boolean argAddItemVisible) {
    isAddItemOptionVisible_ = argAddItemVisible;
  }

  /**
   * Sets the column header.
   * @param argColumnHeader the column header
   */
  @Override
  public void setColumnHeader(String argColumnHeader) {
    columnHeader_ = argColumnHeader;
  }

  /** {@inheritDoc} */
  @Override
  public void setCurrentPage(int argPage) {
    _currentPage = argPage;

    IDimensionSummary currentPageValue = _gridModel.getPageValue(argPage);
    _gridModel.setPage(currentPageValue.getDimensionValue());
    pageValue_ = currentPageValue.getDescription();
  }

  /** {@inheritDoc} */
  @Override
  public void setGridModel(ItemGridModel argGridModel) {
    if (argGridModel != null) {
      _gridModel = argGridModel;

      setPageHeader(argGridModel.getPageDescription());
      setRowHeader(argGridModel.getRowDescription());
      setColumnHeader(argGridModel.getColumnDescription());
    }
    else {
      _gridModel = new ItemGridModel();
      IFormattable noDimensions = FormattableFactory.getInstance().getTranslatable("_itemGridNoDimensions");
      setColumnHeader(noDimensions.toString(VIEW));
    }

    initializeDimensions(_gridModel.getDimensionTypes());
  }

  /**
   * Sets whether the currently modeled item has any inventory.
   * @param argHasInventory ignored
   */
  public void setHasInventory(Boolean argHasInventory) {
    // place holder, this is a computed field
  }

  /**
   * Sets the dimensions for the curently modeled item.
   * @param argDimensions the dimensions for the curently modeled item
   */
  @SuppressWarnings("unchecked")
  public void setItemDimensionList(List argDimensions) {
    dimensionDef_.setEnumeratedPossibleValues(argDimensions);
  }

  /**
   * Sets the dimensions for the curently modeled item.
   * @param argDimensions the dimensions for the curently modeled item
   */
  public void setItemDimensions(Object argDimensions) {
    dimension_ = argDimensions;
  }

  public void setItemSalesWeeksHistory(Object argItemSalesWeeksHistory) {
    this.itemSalesWeeksHistory_ = argItemSalesWeeksHistory;
  }

  public void setItemSalesWeeksHistoryList(List<?> argItemSalesWeeksHistoryList) {
    this.itemSalesWeeksHistoryDef_.setEnumeratedPossibleValues((List<Object>) argItemSalesWeeksHistoryList);
  }

  /**
   * sets the Item Tax Rates for the currently modeled item
   * @param argItemTaxRates the item tax rates for the currently modeled item
   */
  public void setItemTaxRates(List<ItemTaxRateData> argItemTaxRates) {
    itemTaxRates_ = argItemTaxRates;
  }

  /**
   * sets the Item Tax Rates for the currently modeled item
   * @param argItemTaxRates the item tax rates for the currently modeled item
   */
  public void setItemTaxRatesList(List argItemTaxRates) {
    itemTaxRatesDef_.setEnumeratedPossibleValues(argItemTaxRates);
  }

  /**
   * Specifies the collection of kit component details for the item
   * @param argKitComponents the collection of kit component details for the item
   */
  public void setKitComponents(List<KitComponentDetailResult> argKitComponents) {
    kitComponents_ = argKitComponents;
  }

  /**
   * Specifies the collection of kit component details for the item
   * @param argKitComponents the collection of kit component details for the item
   */
  public void setKitComponentsList(List argKitComponents) {
    kitComponentsDef_.setEnumeratedPossibleValues(argKitComponents);
  }

  public void setLastYearSales(Object argLastYearSales) {
    this.lastYearSales_ = argLastYearSales;
  }

  /**
   * Sets whether the "Layaway" functionality should be visible.
   * 
   * @param argLayawayVisible <code>true</code> if the "Layaway" functionality should be visible;
   * <code>false</code> if it should be invisible
   */
  public void setLayawayVisibility(boolean argLayawayVisible) {
    isLayawayOptionVisible_ = argLayawayVisible;
  }

  /**
   * Sets the page header.
   * @param argPageHeader the page header
   */
  @Override
  public void setPageHeader(String argPageHeader) {
    pageHeader_ = argPageHeader;
  }

  public void setPreviousYearSales(Object argPreviousYearSales) {
    this.previousYearSales_ = argPreviousYearSales;
  }

  /**
   * Sets the price history for the currently modeled item.
   * @param argHistory the price history for the currently modeled item
   */
  public void setPriceHistory(List<IHistoricalPrice> argHistory) {
    priceHistory_ = argHistory;
  }

  /**
   * Sets the price history for the currently modeled item.
   * @param argHistory the price history for the currently modeled item
   */
  @SuppressWarnings("unchecked")
  public void setPriceHistoryList(List argHistory) {
    priceHistoryDef_.setEnumeratedPossibleValues(argHistory);
  }

  /**
   * Sets the row header.
   * @param argRowHeader the row header
   */
  @Override
  public void setRowHeader(String argRowHeader) {
    rowHeader_ = argRowHeader;
  }

  public void setSalesHistoryTitle(String argSalesHistoryTitle) {
    this.salesHistoryTitle_ = argSalesHistoryTitle;
  }

  /**
   * @param argSerialNumber The serialNumber to set.
   */
  public void setSerialNumber(List<ISerializedStockLedger> argSerialNumber) {
    serialNumber_ = argSerialNumber;
  }

  /**
   * Set the serial number list
   * @param argSerials the serial number list
   */
  @SuppressWarnings("unchecked")
  public void setSerialNumberList(List argSerials) {
    serialNumberDef_.setEnumeratedPossibleValues(argSerials);
  }

  /**
   * Sets the item similar to the currently modeled item.
   * @param argSimilarItem the item similar to the currently modeled item
   */
  public void setSimilarItem(List<IItem> argSimilarItem) {
    similarItem_ = argSimilarItem;
  }

  /**
   * Sets the items similar to the currently modeled item.
   * @param argSimilarItems the items similar to the currently modeled item
   */
  @SuppressWarnings("unchecked")
  public void setSimilarItemList(List argSimilarItems) {
    similarItemsDef_.setEnumeratedPossibleValues(argSimilarItems);
  }

  /**
   * Sets whether the "special order" functionality should be visible.
   * 
   * @param argSpecialOrderVisible <code>true</code> if the "special order" functionality should be visible;
   * <code>false</code> if it should be invisible
   */
  public void setSpecialOrderVisibility(boolean argSpecialOrderVisible) {
    isSpecialOrderOptionVisible_ = argSpecialOrderVisible;
  }

  /**
   * Set the stock ledger
   * @param argStockLedger the stock ledger
   */
  public void setStockLedger(List<?> argStockLedger) {
    stockLedger_ = argStockLedger;
  }

  /**
   * Sets the style of the currently modeled item.
   * @param argStyle the style of the currently modeled item
   */
  @Override
  public void setStyle(String argStyle) {
    style_ = argStyle;
  }

  /**
   * Sets the message stating the style of the modeled item.
   * @param argStyleMessage the message stating the style of the modeled item
   */
  public void setStyleMessage(String argStyleMessage) {
    styleMessage_ = argStyleMessage;
  }

  /**
   * Sets the item that qualifies as a substitute for the currently modeled item.
   * 
   * @param argSubstituteItem the item that qualifies as a substitute for the currently modeled item
   */
  public void setSubstituteItem(List<IItem> argSubstituteItem) {
    substituteItem_ = argSubstituteItem;
  }

  /**
   * Sets the items that qualify as substitutes for the currently modeled item.
   * 
   * @param argSubstituteItems the items that qualify as substitutes for the currently modeled item
   */
  @SuppressWarnings("unchecked")
  public void setSubstituteItemsList(List argSubstituteItems) {
    substituteItemsDef_.setEnumeratedPossibleValues(argSubstituteItems);
  }

  /**
   * Sets the temporary dimensions of the currently modeled item.
   * 
   * @param argTempDimensions the temporary dimensions of the currently modeled item
   */
  @SuppressWarnings("unchecked")
  public void setTempDimensions(List argTempDimensions) {
    tempDimensions_ = argTempDimensions;
  }

  public void setThisYearSales(Object argThisYearSales) {
    this.thisYearSales_ = argThisYearSales;
  }

  /**
   * Sets the UPCs of the currently modeled item.
   * @param argUpcs the UPCs of the currently modeled item
   */
  @SuppressWarnings("unchecked")
  public void setUpcList(List argUpcs) {
    upcDef_.setEnumeratedPossibleValues(argUpcs);
  }

  /**
   * Sets the UPC of the currently modeled item.
   * @param argUpc the UPC of the currently modeled item
   */
  public void setUpcs(List<IItemCrossReference> argUpc) {
    upc_ = argUpc;
  }

  /**
   * Sets the promotions of the currently modeled item.
   * @param argPromotions the promotions of the currently modeled item
   */
  @SuppressWarnings("unchecked")
  public void setPromotionList(List argPromotions) {
    itemPromotionalDef_.setEnumeratedPossibleValues(argPromotions);
  }

  /**
   * Sets the promotions of the currently modeled item.
   * @param argPromotions the promotions of the currently modeled item
   */
  public void setPromotions(List<CriItemPromotionalData> argPromotions) {
    promotions_ = argPromotions;
  }

  /**
   * Sets the vendor for the modeled item, if available. Depending on the Operation used with this model, the
   * vendor data may not be available. (If the DisplayFoundItemOp is used, the vendor data is not available
   * here.) This field allows the vendor object to be a resource available to the form.
   * @param argVendor the vendor whose fields should be available to users of the model
   */
  public void setVendor(IVendor argVendor) {
    vendor_ = argVendor;
  }

  /**
   * Sets the "selected" item in the list of items having the same vendor as the currently modeled item.
   * @param argVendorItem the "selected" item in the list of items having the same vendor as the currently
   * modeled item
   */
  public void setVendorItem(Object argVendorItem) {
    vendorItem_ = argVendorItem;
  }

  /**
   * Sets the list of items having the same vendor as the currently modeled item.
   * @param argVendorItems the list of items having the same vendor as the currently modeled item
   */
  @SuppressWarnings("unchecked")
  public void setVendorItemList(List argVendorItems) {
    vendorItemsDef_.setEnumeratedPossibleValues(argVendorItems);
  }

  protected void initializeDimensions(List<IItemDimensionType> argDimensionTypes) {
    IFormattable dimension1Name = FF.getTranslatable("_itemResultDimension1Colon");
    IFormattable dimension2Name = FF.getTranslatable("_itemResultDimension2Colon");
    IFormattable dimension3Name = FF.getTranslatable("_itemResultDimension3Colon");
    String dimension1Value = DEFAULT_DIMENSION.getDescription();
    String dimension2Value = DEFAULT_DIMENSION.getDescription();
    String dimension3Value = DEFAULT_DIMENSION.getDescription();

    if ((argDimensionTypes != null) && (getModeledItem().getItemDimensions() != null)) {
      IItemDimensions dimensions = getModeledItem().getItemDimensions();

      for (IItemDimensionType type : argDimensionTypes) {
        switch (type.getSequence()) {
          case 1:
            IFormattable name1 = FF.getLiteral(type.getDescription());
            dimension1Name = FF.getTranslatable("_itemResultDimensionName", name1);
            dimension1Value = getDimensionValueDescription(type, dimensions.getDimension1());
            break;

          case 2:
            IFormattable name2 = FF.getLiteral(type.getDescription());
            dimension2Name = FF.getTranslatable("_itemResultDimensionName", name2);
            dimension2Value = getDimensionValueDescription(type, dimensions.getDimension2());
            break;

          case 3:
            IFormattable name3 = FF.getLiteral(type.getDescription());
            dimension3Name = FF.getTranslatable("_itemResultDimensionName", name3);
            dimension3Value = getDimensionValueDescription(type, dimensions.getDimension3());
            break;
        }
      }
    }

    _dimension1Name = dimension1Name.toString(VIEW);
    _dimension2Name = dimension2Name.toString(VIEW);
    _dimension3Name = dimension3Name.toString(VIEW);
    _dimension1 = dimension1Value;
    _dimension2 = dimension2Value;
    _dimension3 = dimension3Value;
  }

  /** Initializes and adds all of the fields for this edit model. */
  @SuppressWarnings("unchecked")
  private void addFields(IDataModel[] argDaos) {
    for (IDataModel dataModel : argDaos) {
      if (dataModel instanceof IItem) {
        // Add the stock ledger field to allow inventory levels for this item to be available
        IItem item = (IItem) dataModel;
        addStockLedgerField(item);
      }
    }

    addField("styleMessage", String.class);
    addField("inventoryCount", BigDecimal.class, ATTR_NEW + ATTR_NO_SETTER);
    addField("hasInventory", Boolean.class);
    addField(similarItemsDef_);
    addField(substituteItemsDef_);
    addField(priceHistoryDef_);
    addField(upcDef_);
    addField(itemPromotionalDef_);
    addField(dimensionDef_);
    addField(serialNumberDef_);
    addField(vendorItemsDef_);
    addField(kitComponentsDef_);
    addField(itemTaxRatesDef_);
    addField("pageHeader", String.class);
    addField("columnHeader", String.class);
    addField("rowHeader", String.class);
    addField("style", String.class);
    addField(GRID_PAGE_VALUE_FIELD, String.class, ATTR_NEW + ATTR_NO_SETTER);
    addField(ITEM_GRID_FIELD, TableModel.class, ATTR_NEW + ATTR_NO_SETTER);
    addField(DIMENSION1_NAME_FIELD, String.class, ATTR_NEW + ATTR_NO_SETTER);
    addField(DIMENSION2_NAME_FIELD, String.class, ATTR_NEW + ATTR_NO_SETTER);
    addField(DIMENSION3_NAME_FIELD, String.class, ATTR_NEW + ATTR_NO_SETTER);
    addField(DIMENSION1_FIELD, String.class, ATTR_NEW + ATTR_NO_SETTER);
    addField(DIMENSION2_FIELD, String.class, ATTR_NEW + ATTR_NO_SETTER);
    addField(DIMENSION3_FIELD, String.class, ATTR_NEW + ATTR_NO_SETTER);
    addField(ITEM_IMAGE_FIELD, String.class, ATTR_NEW + ATTR_NO_SETTER);
    addField(CURRENT_PRICE_FIELD, String.class, ATTR_NEW + ATTR_NO_SETTER);
    addField(BLIND_RETURN_PRICE_FIELD, String.class, ATTR_NEW + ATTR_NO_SETTER);
    addField(CURRENT_TAX_FIELD, String.class, ATTR_NEW + ATTR_NO_SETTER);
    addField("vendor", IVendor.class);
  }

  /**
   * Add a stock ledger field
   * @param argItem the stock ledger field
   */
  @SuppressWarnings("unchecked")
  private void addStockLedgerField(IItem argItem) {
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("argRetailLocationId", new Integer(ConfigurationMgr.getRetailLocationId()));
    params.put("argItemId", argItem.getItemId());

    List<IStockLedger> ledger = null;

    try {
      ledger = DataFactory.getObjectByQuery(ITEM_RESULTS_STOCK_LEDGER, params);
    }
    catch (ObjectNotFoundException ex) {
      ledger = new ArrayList<IStockLedger>();
    }

    IEditModelField stockLedgerDef_ =
        EditModelField.makeFieldDefUnsafe(this, "stockLedger", List.class, IEditModelFieldMetadata.ATTR_NEW,
            (IFieldDependencyConfig) null, ICardinality.OPTIONAL, ledger, (IListFieldElementDescr) null,
            (IValueWrapperFactory) null, (SecuredObjectID) null);

    addField(stockLedgerDef_);
  }

  /**
   * Calculates current tax for the item as if it was added to a standard sales transaction
   * @return tax amount
   */
  private BigDecimal calculateCurrentTax() {
    if (getCurrentPrice() == null) {
      return null;
    }

    TaxHelper TH = TaxHelper.getInstance();
    ITaxLocation location = TH.getLocations().getByRetailLocation(getRetailLocationId());

    List<ITaxGroupRule> taxGroupRules =
        TH.getGroupRules().get(TH.getCurrentSaleItemTaxGroup(getModeledItem(), null), location);

    ISaleReturnLineItem lineItem =
        ItemLocator.getLocator().getSaleLineItem(getModeledItem(), SALE, false, KEYBOARD);

    lineItem.setQuantity(ONE);
    IRetailTransaction trans = new RetailTransactionModel();
    IRetailTransactionLineItem[] retailLines = new IRetailTransactionLineItem[] {lineItem};
    TH.createSaleTaxModifiers(lineItem, taxGroupRules);

    for (ISaleTaxModifier taxMod : lineItem.getTaxModifiers()) {
      taxMod.setTaxableAmount(getCurrentPrice());
      taxMod.setTranTaxableAmt(getCurrentPrice());
    }

    trans.addRetailTransactionLineItem(lineItem);
    TaxModifierCalculator tmc = new TaxModifierCalculator();
    tmc.handleLineItemEvent(retailLines);

    return TH.getTax(lineItem);
  }

  /** Creates and returns a field definition from the specified parameters. */
  private EditModelField<Object> createDef(String argFieldName, ICardinality argCardinality) {
    return EditModelField.makeFieldDef(this, argFieldName, Object.class, 0, argCardinality);
  }

  private String getDimensionValueDescription(IItemDimensionType argType, String argValue) {
    String desc = null;

    for (IItemDimensionValue value : argType.getDimensionValues()) {
      if (value.getDimensionValue().equals(argValue)) {
        desc = value.getDescription();
      }
    }

    if (desc == null) {
      desc = argValue;
    }

    return desc;
  }
}
