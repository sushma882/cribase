// $Id: PromptCommAssocOp.java 172 2012-07-27 19:39:21Z dtvdomain\mschreiber $
package dtv.pos.commission;

import static dtv.pos.common.ConfigurationMgr.*;

import java.util.List;

import dtv.i18n.FormatterType;
import dtv.i18n.IFormattable;
import dtv.pos.common.*;
import dtv.pos.employee.EmployeeHelper;
import dtv.pos.framework.op.OpState;
import dtv.pos.framework.ui.config.DataFieldConfig;
import dtv.pos.framework.ui.config.PromptConfig;
import dtv.pos.iframework.action.IXstDataAction;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.*;
import dtv.pos.iframework.ui.config.IPromptKey;
import dtv.pos.iframework.validation.*;
import dtv.pos.register.ItemLocator;
import dtv.pos.register.returns.ReturnManager;
import dtv.util.StringUtils;
import dtv.xst.dao.hrs.IEmployee;
import dtv.xst.dao.trl.ICommissionModifier;

/**
 * Prompts for commissioned associates.<br>
 * Copyright (c) 2003 Datavantage Corporation
 * 
 * @author rsable
 * @created January 29, 2004
 * @version $Revision: 172 $
 */
public class PromptCommAssocOp
    extends AbstractValidationOp {

  private static final long serialVersionUID = 1L;
  private final IOpState MID_PROMPT = new OpState(this, "MID_PROMPT");

  private final IValidationKey[] VALIDATIONS = {ValidationKey.valueOf("COMMISSIONED_ASSOCIATES"),
      ValidationKey.valueOf("COMMISSIONED_ASSOCIATE_ID")};

  /** {@inheritDoc} */
  @Override
  public IValidationData getValidationData(IXstCommand argCmd, IXstEvent argEvent, IValidationKey argKey) {
    ICommissionedAssocsCmd cmd = (ICommissionedAssocsCmd) argCmd;

    Integer assocs = Integer.valueOf(cmd.getCommissionedAssocs().size() + 1);
    List<IEmployee> currentAssocs = cmd.getCommissionedAssocs();
    Object[] suppliedData = {argEvent.getStringData(), currentAssocs};

    return new ValidationData(ValidationDataType.INT, assocs, suppliedData);
  }

  /** {@inheritDoc} */
  @Override
  public IValidationKey[] getValidationKeys(IXstCommand argCmd) {
    return VALIDATIONS;
  }

  /**
   * Handle the prompting process. This simply requests the display of a prompt and then handles the event
   * that occurs in response to the prompt.
   * 
   * @param argCmd the current command
   * @param argEvent the current event
   * @return the response indicating the operations completion status
   */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    ICommissionedAssocsCmd cmd = (ICommissionedAssocsCmd) argCmd;
    IOpState state = argCmd.getOpState();

    if (((state == POST_PROMPT) || (state == MID_PROMPT)) && (argEvent instanceof IXstDataAction)) {
      IOpResponse response = handleDataAction(argCmd, (IXstDataAction) argEvent);
      if (response != null) {
        return response;
      }
    }
    // Check the current operation state.
    if ((state == null) || (state == ERROR_MESSAGE_PROMPT)) {
      return getPromptResponse(argCmd, argEvent, getPromptKey(argCmd), getPromptArgs(argCmd, argEvent));
    }
    else if (state == MID_PROMPT) {
      if (!isAssocNumValid(cmd.getCommissionedAssocs().size())) {
        argCmd.setOpState(POST_PROMPT);
      }
      return getPromptResponse(argCmd, argEvent, getPromptKey(argCmd), getPromptArgs(argCmd, argEvent));
    }
    else {
      return HELPER.completeResponse();
    }
  }

  /**
   * Only run operation if configured to prompt for commissioned associates
   * 
   * @param argCmd current command
   * @return true if configured to prompt for commisioned associated or command is marked that we are changing
   * the commissioned associates, false otherwise
   */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    ICommissionedAssocsCmd cmd = (ICommissionedAssocsCmd) argCmd;
    return getPromptForCommissionedAssociates() || cmd.isChangeAssoc();
  }

  /** {@inheritDoc} */
  @Override
  protected IFormattable[] getPromptArgs(IXstCommand argCmd, IXstEvent argEvent) {
    ICommissionedAssocsCmd cmd = (ICommissionedAssocsCmd) argCmd;
    IFormattable args[] = new IFormattable[2];

    int max = getMaxCommissionedAssociatesAllowed();
    args[0] = ff_.getSimpleFormattable(max, FormatterType.INTEGER);

    int actual = cmd.getCommissionedAssocs().size();
    args[1] = ff_.getSimpleFormattable(actual, FormatterType.INTEGER);

    return args;
  }

  /** {@inheritDoc} */
  @Override
  protected IPromptKey getPromptKey(IXstCommand argCmd) {
    return ((getMinCommissionedAssociatesAllowed() == 1) && (getMaxCommissionedAssociatesAllowed() == 1)) //
        ? PromptKey.valueOf("PROMPT_COMMASSOC_SINGLE") //
        : PromptKey.valueOf("PROMPT_COMMASSOC");
  }

  /**
   * Return the prompt request to prompt the user to enter layaway deposit along with the default amount of
   * the suggested deposit amount.
   * 
   * @param argCmd the current command
   * @param argEvent the current event
   * @param argKey the prompt key
   * @param argPromptArgs the prompt text arguements
   * @return the prompt request
   */
  @Override
  protected IOpResponse getPromptResponse(IXstCommand argCmd, IXstEvent argEvent, IPromptKey argKey,
      IFormattable[] argPromptArgs) {

    IOpState state = argCmd.getOpState();
    PromptConfig config = new PromptConfig();

    if (state == null) {
      config.setDataFieldConfig(new DataFieldConfig());
      String id =
          argCmd.getStationModel().getAuthenticatedUser().getSystemUser().getOperatorParty().getEmployeeId();
      /* CRI: blow this id away (don't default to current cashier) - force cashier to enter correct id */
      id = null;
      config.getDataFieldConfig().setDefaultValue(id);
    }
    argCmd.setOpState(MID_PROMPT);
    return HELPER.getPromptResponse(argKey, argPromptArgs, config);
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleDataAction(IXstCommand argCmd, IXstDataAction argAction) {
    ICommissionedAssocsCmd cmd = (ICommissionedAssocsCmd) argCmd;
    String data = ((IXstEvent) argAction).getStringData();

    if (StringUtils.isEmpty(data)) {
      boolean assocsValid = isAssocNumValid(cmd.getCommissionedAssocs().size());
      return (assocsValid) ? HELPER.completeResponse() : runValidationCheck(argCmd, argAction);
    }
    else {
      IOpResponse r = runValidationCheck(argCmd, argAction);
      if (r.getOpStatus() != OpStatus.COMPLETE) {
        return r;
      }
      IEmployee edata = EmployeeHelper.getInstance().getEmployeeById(data);
      cmd.addCommissionedAssoc(edata);

      if (ReturnManager.getInstance().getCurrentReturnType() != null) {
        ICommissionModifier commissionMod = ItemLocator.getLocator().createCommissionModifier();
        commissionMod.setEmployeeParty(edata.getParty());
        cmd.addCommissionedModifier(commissionMod);
        ReturnManager.getInstance().setCurrentCommissionModifiers(cmd.getCommissionModifier());
      }

      if (!isAssocNumValid(cmd.getCommissionedAssocs().size() + 1)) {
        return HELPER.completeResponse();
      }
    }
    return null;
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handlePromptResponse(IXstCommand argCmd, IXstEvent argEvent) {
    return HELPER.completeResponse();
  }

  /**
   * Return whether the associate number is valid.
   * 
   * @param num the associate number to check.
   * @return true if valid, false if invalid.
   */
  private boolean isAssocNumValid(int num) {
    return CommAssocsHelper.getInstance().isAssocNumSelectedValid(num);
  }
}
