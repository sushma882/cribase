// $Id: CreateResumeTransOp.java 799 2014-07-04 05:02:02Z suz.sxie $
package dtv.pos.register.suspendresume;

import java.util.*;

import org.apache.log4j.Logger;

import dtv.data2.access.DataFactory;
import dtv.data2x.IDataServices;
import dtv.data2x.model.IDataModelFactory;
import dtv.i18n.FormattableFactory;
import dtv.i18n.IFormattable;
import dtv.pos.common.*;
import dtv.pos.customer.account.AccountManager;
import dtv.pos.customer.account.type.CustomerAccountStateType;
import dtv.pos.customer.account.type.CustomerAccountType;
import dtv.pos.customer.loyalty.LoyaltyMgr;
import dtv.pos.document.DocumentHelper;
import dtv.pos.framework.op.Operation;
import dtv.pos.framework.security.SecurityMgr;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.*;
import dtv.pos.register.tax.TaxHelper;
import dtv.pos.register.type.NonPhysicalItemType;
import dtv.pos.tender.TenderHelper;
import dtv.service.ServiceProvider;
import dtv.util.*;
import dtv.xst.dao.cat.*;
import dtv.xst.dao.doc.IDocument;
import dtv.xst.dao.doc.IDocumentLineItem;
import dtv.xst.dao.itm.INonPhysicalItem;
import dtv.xst.dao.trl.*;
import dtv.xst.dao.trn.*;
import dtv.xst.dao.ttr.IVoucher;

/**
 * This operation handles creating the new resume transaction by cloning the suspend transaction.
 * (This operation is implemented under the assumption that no tender has been applied to current
 * transction.)<br>
 * Copyright (c) 2005 Datavantage Corporation
 * 
 * @author jhsiao
 * @version $Revision: 799 $
 * @created Dec 28, 2005
 */
public class CreateResumeTransOp
    extends Operation {

  private static final long serialVersionUID = 1L;
  private static final Logger logger_ = Logger.getLogger(CreateResumeTransOp.class);

  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    ISuspendResumeCmd cmd = (ISuspendResumeCmd) argCmd;
    IPosTransaction suspendTrans = cmd.getSuspendedTransaction();

    /* We cannot afford to do a second transaction lookup here, when connected to a slow datasource. So, we
     * will do a cheap, but effective deep clone... */
    IPosTransaction resumeTrans = null;
    int count = 1;
    try {
      resumeTrans = (IPosTransaction) EncodingHelper.deserialize(EncodingHelper.serialize(suspendTrans));
    }
    catch (Exception ee) {
      String msg =
          "An error occured while serializing/deserializing" + "suspended transaction to obtain clone.";

      logger_.error(msg, ee);
      throw new OpExecutionException(msg, ee);
    }
    // Sets the transaction line sequence to start from 1, not using the existing one from suspendTrans
    for (IRetailTransactionLineItem rtlLineItem : resumeTrans.getRetailTransactionLineItems()) {
      rtlLineItem.setRetailTransactionLineItemSequence(count++ );
    }
    // Switch the cashier from the old one to the current one.
    resumeTrans.setOperatorParty(SecurityMgr.getCurrentUser().getOperatorParty());

    // Add the new resume transaction on station mode, and retail transaction manager impl
    argCmd.getStationModel().setCurrentTransaction(resumeTrans);
    cmd.setTransaction(resumeTrans);

    if (resumeTrans instanceof IRetailTransaction) {
      LoyaltyMgr.getInstance().setCurrentTransaction((IRetailTransaction) resumeTrans);
    }

    // Initialize the resume transaction.
    TransactionFactory.getInstance().resumeTransaction(resumeTrans);

    // Apply specific modifications to the descendants of the resume transaction.
    IOpResponse response = changeObjectState(resumeTrans, suspendTrans, cmd);
    if (response != null) {
      return response;
    }

    /* Dirty the entire object graph so that all models copied from the original transaction will be persisted
     * appropriately. */
    getDataModelFactory().makeDirty(resumeTrans, true);

    createTransactionLink(suspendTrans, resumeTrans);

    // Restore special order setup account (FB #99034)
    restoreCustAccounts(resumeTrans);

    return HELPER.completeResponse();
  }

  protected final IDataModelFactory getDataModelFactory() {
    IDataModelFactory dataModelFactory =
        (IDataModelFactory) ServiceProvider.getService(IDataServices.class.getName());

    return dataModelFactory;
  }

  protected IOpResponse handleSaleReturnLineItem(ISaleReturnLineItem argSaleLine,
      IPosTransaction argResumeTrans, IPosTransaction argSuspendTrans, ISuspendResumeCmd cmd) {

    // Price modifiers
    for (IRetailPriceModifier priceMod : argSaleLine.getRetailPriceModifiers()) {
      if (priceMod.getDiscBusinessDate() != null) {
        priceMod.setDiscTransactionSequence(argSaleLine.getTransactionSequence());
        priceMod.setDiscBusinessDate(argSaleLine.getBusinessDate());
        priceMod.setDiscWorkstationId(argSaleLine.getWorkstationId());
      }

      if (priceMod.getDiscountLineItem() != null) {
        priceMod.getDiscountLineItem().setBusinessDate(argSaleLine.getBusinessDate());
        priceMod.getDiscountLineItem().setTransactionSequence(argSaleLine.getTransactionSequence());
      }
    }

    // Line association modifiers
    for (ILineItemAssociationModifier asscMod : argSaleLine.getLineItemAssociationModifiers()) {
      asscMod.setChildTransactionSequence(argSaleLine.getTransactionSequence());
      asscMod.setChildBusinessDate(argSaleLine.getBusinessDate());
      asscMod.setChildWorkstationId(argSaleLine.getWorkstationId());
    }

    // Inventory location modifiers
    /* Since this transaction is being resumed, all inventory location modifiers need to be active and not
     * "original" in order to processed. Therefore, copy them, re-add them to the list and remove the original
     * ones. Use copies of lists to avoid ConcurrentModificationException. */
    HistoricalList<IRetailInventoryLocationModifier> locMods =
        (HistoricalList<IRetailInventoryLocationModifier>) argSaleLine.getRetailInventoryLocationModifiers();

    List<IRetailInventoryLocationModifier> locModsCopy =
        new ArrayList<IRetailInventoryLocationModifier>(locMods);

    List<IRetailInventoryLocationModifier> origLocMods =
        new ArrayList<IRetailInventoryLocationModifier>(locMods.getOriginalList());

    copyRetailInventoryLocationModifiers(argSaleLine, locModsCopy);

    for (IRetailInventoryLocationModifier origLocMod : origLocMods) {
      argSaleLine.removeRetailInventoryLocationModifier(origLocMod);
    }

    // Line item generic storage
    LineItemGenericStorageId genericId = argSaleLine.getLineItemGenericStorageId();
    if (genericId != null) {
      genericId.setTransactionSequence(argSuspendTrans.getTransactionSequence());

      ILineItemGenericStorage genericStorage =
          (ILineItemGenericStorage) DataFactory.getObjectByIdNoThrow(genericId);

      if (genericStorage != null) {
        genericStorage.setTransactionSequence(argSaleLine.getTransactionSequence());
        getDataModelFactory().makeDirty(genericStorage, false);
        cmd.addPersistable(genericStorage);
      }
    }

    // Restore group rules to modifiers if necessary
    TaxHelper.getInstance().updateGroupRulesForSaleLine(argSaleLine);

    // Voucher sale line item
    if (argSaleLine instanceof IVoucherSaleLineItem) {
      handleVoucherSaleLineItem((IVoucherSaleLineItem) argSaleLine);
    }

    return null;
  }

  private IOpResponse changeObjectState(IPosTransaction argResumeTrans, IPosTransaction argSuspendTrans,
      ISuspendResumeCmd cmd) {

    IOpResponse response = null;

    for (IRetailTransactionLineItem lineItem : argResumeTrans.getRetailTransactionLineItems()) {
      lineItem.setBeginDateTimestamp(DateUtils.getNewDate());

      if (lineItem instanceof ISaleReturnLineItem) {
        response =
            handleSaleReturnLineItem((ISaleReturnLineItem) lineItem, argResumeTrans, argSuspendTrans, cmd);
      }
      else if (lineItem instanceof IDocumentLineItem) {
        response = handleDocumentLineItem((IDocumentLineItem) lineItem);
      }
      else if (lineItem instanceof ITaxLineItem) {
        response = handleTaxLineItem((ITaxLineItem) lineItem);
      }
      lineItem.setEndDateTimestamp(DateUtils.getNewDate());
    }
    return response;
  }

  private void copyRetailInventoryLocationModifiers(ISaleReturnLineItem argLineItem,
      List<IRetailInventoryLocationModifier> argOrigMods) {

    for (IRetailInventoryLocationModifier origMod : argOrigMods) {
      IRetailInventoryLocationModifier newMod =
          DataFactory.createObject(IRetailInventoryLocationModifier.class);

      newMod.setActionCode(origMod.getActionCode());
      newMod.setDestinationBucketId(origMod.getDestinationBucketId());
      newMod.setDestinationLocationId(origMod.getDestinationLocationId());
      newMod.setItemId(origMod.getItemId());
      newMod.setQuantity(origMod.getQuantity());
      newMod.setSerialNumber(origMod.getSerialNumber());
      newMod.setSourceBucketId(origMod.getSourceBucketId());
      newMod.setSourceLocationId(origMod.getSourceLocationId());
      argLineItem.addRetailInventoryLocationModifier(newMod);
    }
  }

  private void createTransactionLink(IPosTransaction suspendTrans, IPosTransaction resumeTrans) {
    // Create the transaction link id from the interface class
    PosTransactionLinkId transLinkId = new PosTransactionLinkId();

    // Set the values on the transaction link id
    transLinkId.setLinkBusinessDate(suspendTrans.getBusinessDate());
    transLinkId.setLinkRetailLocationId(new Long(suspendTrans.getRetailLocationId()));
    transLinkId.setLinkWorkstationId(new Long(suspendTrans.getWorkstationId()));
    transLinkId.setLinkTransactionSequence(new Long(suspendTrans.getTransactionSequence()));
    transLinkId.setBusinessDate(resumeTrans.getBusinessDate());
    transLinkId.setRetailLocationId(new Long(resumeTrans.getRetailLocationId()));
    transLinkId.setWorkstationId(new Long(resumeTrans.getWorkstationId()));
    transLinkId.setTransactionSequence(new Long(resumeTrans.getTransactionSequence()));
    transLinkId.setOrganizationId(new Long(resumeTrans.getOrganizationId()));

    // Create the transaction link
    IPosTransactionLink transLink = DataFactory.createObject(transLinkId, IPosTransactionLink.class);

    // Set the trans link values
    transLink.setLinkBusinessDate(transLinkId.getLinkBusinessDate());
    transLink.setLinkRetailLocationId(transLinkId.getLinkRetailLocationId().longValue());
    transLink.setLinkTransactionSequence(transLinkId.getLinkTransactionSequence().longValue());
    transLink.setLinkWorkstationId(transLinkId.getLinkWorkstationId().longValue());
    transLink.setBusinessDate(transLinkId.getBusinessDate());
    transLink.setRetailLocationId(transLinkId.getRetailLocationId().longValue());
    transLink.setTransactionSequence(transLinkId.getTransactionSequence().longValue());
    transLink.setWorkstationId(transLinkId.getWorkstationId().longValue());

    transLink.setParentTransaction(resumeTrans);
    transLink.setLinkedTransaction(suspendTrans);
    transLink.setLinkTypeCode(TransactionLinkTypeCode.RESUME_TRANSACTION.toString());

    resumeTrans.addPosTransactionLink(transLink);

  }

  private Map<String, String> getAccountCodes(IPosTransaction argResumeTrans) {
    Map<String, String> map = new HashMap<String, String>();
    List<IRetailTransactionLineItem> lineItems = argResumeTrans.getRetailTransactionLineItems();

    for (IRetailTransactionLineItem lineItem : lineItems) {
      if (!lineItem.getVoid() && lineItem instanceof ISaleReturnLineItem) {
        ISaleReturnLineItem saleItem = (ISaleReturnLineItem) lineItem;
        ICustomerItemAccountModifier mod = saleItem.getCustomerAccountModifier();

        if (mod != null) {
          if (!map.containsKey(mod.getCustAccountCode())) {
            map.put(mod.getCustAccountCode(), mod.getCustAccountId());
          }
        }
      }
    }
    return map;
  }

  private IOpResponse handleDocumentLineItem(IDocumentLineItem argDocLine) {
    IDocument doc =
        DocumentHelper.get().retrieveDocument(argDocLine.getDocumentId(), argDocLine.getSeriesId(),
            argDocLine.getDocumentType());

    if (doc == null) {
      logger_.error("ERROR!!! Cannot retrieve the document: " + " Doc ID: " + argDocLine.getDocumentId()
          + " Series Id: " + argDocLine.getSeriesId() + " Document Type: " + argDocLine.getDocumentType());
    }
    else {
      doc.setDocumentStatus(DocumentHelper.DocumentStatus.REDEEMED.toString());
    }

    argDocLine.setDocument(doc);

    return HELPER.completeResponse();
  }

  private IOpResponse handleTaxLineItem(ITaxLineItem argTaxLine) {
    if (argTaxLine.getSaleTaxGroupRule() == null) {
      // implies that Xstore native taxing was
      // not used on the original trans
      TaxHelper.getInstance().setGroupRuleForTaxLine(argTaxLine);
    }

    // Changed to NULL because we still need to create the TransactionLink. - C Dusseau 3/13/07
    return null;
  }

  private IOpResponse handleVoucherSaleLineItem(IVoucherSaleLineItem argVoucherLine) {
    FormattableFactory ff = FormattableFactory.getInstance();

    // Upon resume the voucher sale line item, need to create or retrieve the
    // voucher and set it to the sale line item.

    IVoucher voucher = null;
    INonPhysicalItem nonPhysItem = (INonPhysicalItem) argVoucherLine.getItem();
    String voucherTypeCode = nonPhysItem.getNonPhysicalItemTypeCode();
    String serialNumber = argVoucherLine.getSerialNumber();

    if ((voucherTypeCode != null) && (serialNumber != null)) {
      voucher = TenderHelper.getInstance().getVoucherFromDatabase(voucherTypeCode, serialNumber);

      if (voucher == null) {
        voucher = TenderHelper.getInstance().createVoucher(voucherTypeCode, serialNumber);
      }
      else if (!argVoucherLine.getReturn() && (nonPhysItem.getNonPhysicalItemTypeCode() != null)
          && nonPhysItem.getNonPhysicalItemTypeCode().equals(NonPhysicalItemType.VOUCHER.getName())) {

        String voucherNumber = voucher.getSerialNumber();
        voucher = null;
        argVoucherLine.setVoid(true);

        return HELPER.getPromptResponse(OpStatus.ERROR_HALT,
            PromptKey.valueOf("VOUCHER_HAS_CHANGED_BEFORE_RESUME"),
            new IFormattable[] {ff.getTranslatable(voucherNumber)});
      }

      if (voucher != null) {
        // Return voucher sale line item

        if (!NumberUtils.isZeroOrNull(voucher.getUnspentBalanceAmount())) {
          //voucher = null;
          argVoucherLine.setVoid(true);

          return HELPER.getPromptResponse(OpStatus.ERROR_HALT,
              PromptKey.valueOf("VOUCHER_HAS_CHANGED_BEFORE_RESUME"),
              new IFormattable[] {ff.getTranslatable(voucher.getSerialNumber())});
        }
        else if (argVoucherLine.getReturn()) {
          voucher.setUnspentBalanceAmount(NumberUtils.ZERO);
          argVoucherLine.setVoucher(voucher);
        }
        else {
          voucher.setUnspentBalanceAmount(argVoucherLine.getUnitPrice());
          argVoucherLine.setVoucher(voucher);
        }
        voucher.setVoucherStatusCode(null);
      }
    }
    argVoucherLine.setVoucher(voucher);

    return null;
  }

  private void restoreCustAccounts(IPosTransaction argResumeTrans) {
    Map<String, String> accounts = getAccountCodes(argResumeTrans);

    for (String accountCode : accounts.keySet()) {
      String accountId = accounts.get(accountCode);

      if (!StringUtils.isEmpty(accountId)) {
        CustomerAccountId id = new CustomerAccountId();
        id.setCustAccountId(accountId);
        id.setCustAccountCode(accountCode);
        id.setOrganizationId(new Long(argResumeTrans.getOrganizationId()));

        ICustomerAccount acct = (ICustomerAccount) DataFactory.getObjectById(id);
        acct.setCustAccountStateCode(CustomerAccountStateType.NEW.getName());

        // Restore item account details and activities.
        if (acct instanceof ICustomerItemAccount) {
          for (IRetailTransactionLineItem lineItem : argResumeTrans.getRetailTransactionLineItems()) {
            for (ICustomerItemAccountDetail detail : ((ICustomerItemAccount) acct)
                .getCustItemAccountDetails()) {

              IRetailTransactionLineItem detailLineItem = detail.getRetailLineItem();

              if ((lineItem.getOrganizationId() == detailLineItem.getOrganizationId())
                  && (lineItem.getRetailLocationId() == detailLineItem.getRetailLocationId())
                  && lineItem.getBusinessDate().equals(detailLineItem.getBusinessDate())
                  && (lineItem.getWorkstationId() == detailLineItem.getWorkstationId())
                  && (lineItem.getRetailTransactionLineItemSequence() == detailLineItem
                      .getRetailTransactionLineItemSequence())) {

                detail.setTransactionSequence(argResumeTrans.getTransactionSequence());
                detail.setRetailLineItem(lineItem);
                break;
              }

              for (ICustomerItemAccountActivity activity : detail.getCustItemAccountActivities()) {
                activity.setRetailLocationId(detail.getRetailLocationId());
                activity.setBusinessDate(detail.getBusinessDate());
                activity.setTransSequence(detail.getTransactionSequence());
                activity.setWorkstationId(detail.getWorkstationId());
                activity.setTransLineItemSeq(detail.getRetailTransactionLineItemSequence());
              }
            }
          }
        }
        // Add the account to the account manager.
        AccountManager.getInstance().addAccount(CustomerAccountType.forName(accountCode), acct);
      }
    }
  }
}
