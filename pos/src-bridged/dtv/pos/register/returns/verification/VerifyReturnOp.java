// $Id: VerifyReturnOp.java 57940 2012-03-01 15:01:16Z dtvdomain\jweiss $
package dtv.pos.register.returns.verification;

import static dtv.pos.common.TransactionStatus.*;
import static dtv.pos.framework.action.type.XstChainActionType.START;
import static dtv.pos.iframework.op.OpStatus.*;
import static dtv.pos.register.returns.ReturnType.UNVERIFIED;
import static dtv.pos.register.returns.ReturnType.VERIFIED;
import static dtv.pos.register.type.LineItemType.ITEM;

import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.Logger;

import dtv.pos.common.*;
import dtv.pos.customer.loyalty.LoyaltyHelper;
import dtv.pos.customer.loyalty.LoyaltyMgr;
import dtv.pos.framework.action.type.XstDataActionKey;
import dtv.pos.framework.op.OpState;
import dtv.pos.iframework.action.IXstDataAction;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.*;
import dtv.pos.iframework.ui.config.IPromptKey;
import dtv.pos.register.ISaleItemCmd;
import dtv.pos.register.returns.*;
import dtv.pos.register.tax.*;
import dtv.xst.dao.cat.ICustomerLoyaltyCard;
import dtv.xst.dao.crm.IParty;
import dtv.xst.dao.trl.*;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.trn.PosTransactionId;

/**
 * Check the configuration to see if return verification functionality has turned on and if so, get the return
 * verification method from the configuration. Search the original sale transaction based on the method type.
 * If no transaction found, prompt the user to select one of the options. If found, trigger the return
 * verification opchain.<br>
 * <br>
 * Copyright (c) 2003 Datavantage Corporation
 * 
 * @author jhsiao
 * @created July 11, 2003
 * @version $Revision: 57940 $
 */
public class VerifyReturnOp
    extends AbstractCreateTransactionAsNeededOp {

  private static final long serialVersionUID = 1L;
  private static final Logger logger_ = Logger.getLogger(VerifyReturnOp.class);

  private static final IPromptKey RETURN_VERIFICATION_NOT_FOUND = PromptKey
      .valueOf("RETURN_VERIFICATION_NOT_FOUND");
  private static final IPromptKey RETURN_POSTVOID_TRANS = PromptKey.valueOf("RETURN_POSTVOID_TRANS");
  private static final IPromptKey RETURN_SUSPENDED_TRANS = PromptKey.valueOf("RETURN_SUSPENDED_TRANS");
  private static final IPromptKey RETURN_CANCELLED_TRANS = PromptKey.valueOf("RETURN_CANCELLED_TRANS");
  private static final IPromptKey RETURN_INVALID_TRANS = PromptKey.valueOf("RETURN_INVALID_TRANS");

  private static final IOpChainKey PRE_RETURN_VERIFICATION = OpChainKey.valueOf("PRE_RETURN_VERIFICATION");
  private static final IOpChainKey RETURN_ITEM_AUTHORIZE = OpChainKey.valueOf("RETURN_ITEM_AUTHORIZE");

  protected final IOpState POST_PROMPT = new OpState(this, "POST_PROMPT");

  public VerifyReturnOp() {
    super(TransactionType.RETAIL_SALE);
  }

  /**
   * Check the configuration to see if return verification functionality has turned on and if so, get the
   * return verification method from the configuration. Search the original sale transaction based on the
   * method type. If no transaction found, prompt the user to select one of the options. If found, trigger the
   * return verification opchain.
   * 
   * @param argCmd the current command
   * @param argEvent the current event
   * @return the operation response
   */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    ReturnManager manager = ReturnManager.getInstance();
    ISaleItemCmd cmd = (ISaleItemCmd) argCmd;
    IRetailTransaction rtlTrans = null;

    // Return verification is required.
    if ((argCmd.getOpState() == null) && ConfigurationMgr.getReturnVerificationRequired()) {
      PosTransactionId id = manager.getCurrentOrigTransactionId();
      rtlTrans = TransactionHelper.searchExistingOrigTrans(cmd, id);

      if (rtlTrans == null) {
        IPosTransaction trans = TransactionHelper.searchTransFromHost(id);
        if (trans instanceof IRetailTransaction) {
          rtlTrans = (IRetailTransaction) trans;
        }
        else if (trans != null) {
          return HELPER.getPromptResponse(ERROR_HALT, RETURN_INVALID_TRANS);
        }
      }

      if (rtlTrans != null) {
        // Should not allow to return post void transaction.
        if (rtlTrans.getPostVoid()) {
          return HELPER.getPromptResponse(ERROR_HALT, RETURN_POSTVOID_TRANS);
        }
        // Should not allow to return suspended transaction.
        else if (SUSPEND.matches(rtlTrans.getTransactionStatusCode())) {
          return HELPER.getPromptResponse(ERROR_HALT, RETURN_SUSPENDED_TRANS);
        }
        // Do not return canceled transaction.
        else if (CANCEL.matches(rtlTrans.getTransactionStatusCode())
            || CANCEL_ORPHANED.matches(rtlTrans.getTransactionStatusCode())) {

          return HELPER.getPromptResponse(ERROR_HALT, RETURN_CANCELLED_TRANS);
        }

        manager.setCurrentReturnType(VERIFIED);
        manager.setCurrentOriginalTransaction(rtlTrans);

        List<ISaleReturnLineItem> originalItems =
            rtlTrans.getLineItemsByTypeCode(ITEM.getName(), ISaleReturnLineItem.class);

        final boolean giftRcpt =
            (cmd.getEditModel() instanceof OriginalReceiptInformationSearchModel)
                ? ((OriginalReceiptInformationSearchModel) cmd.getEditModel()).getGiftRcpt() : false;

        for (ISaleReturnLineItem originalItem : originalItems) {
          originalItem.setReturnedWithGiftReceipt(giftRcpt);
          originalItem.setPreDealAmount(getVatExemptedPrice(originalItem, originalItem.getBaseUnitPrice()));
        }

        final IRetailTransaction newTrans = (IRetailTransaction) getTransaction(argCmd);
        final IParty purchaseCustomer = rtlTrans.getCustomerParty();

        /* If the original purchase had a customer attached but the return transaction does not, then assume
         * the customer doing the returning is the same as the person who did the purchasing and transfer that
         * customer to the current transaction. */
        
        /* CRI 429338 - Don't assign the party record to return transaction if the party id of the original purchase
         * is 0. Party id 0 on the original purchase is coming from ecom transaction lookup and this is only a
         *  transient party record assigned to original purchase for name display purpose on original transaction
         * 
         */
        if ((newTrans.getCustomerParty() == null) && (purchaseCustomer != null) && (purchaseCustomer.getPartyId() != 0)) {
          newTrans.setCustomerParty(purchaseCustomer);
          newTrans.setTaxExemption(rtlTrans.getTaxExemption());
        }

        /* If the original purchaser had a loyalty card but the return transaction does not have one assigned,
         * then assume that the original loyalty card should be debited for the return and transfer it to the
         * current transaction. */
        if ((LoyaltyMgr.getInstance().getCurrentCard() == null) && (purchaseCustomer != null)) {
          ICustomerLoyaltyCard primaryCard = purchaseCustomer.getPrimaryLoyaltyCard();
          if (primaryCard != null) {
            primaryCard = LoyaltyHelper.getInstance().lookupCard(primaryCard);
            LoyaltyMgr.getInstance().setCurrentCard(primaryCard);
          }
        }

        return HELPER.getRunChainResponse(COMPLETE_HALT, PRE_RETURN_VERIFICATION, argCmd, START);
      }

      return handleOriginalTranNotFound(argCmd);
    }
    else if (argCmd.getOpState() == null) {
      // Since return verification is not required, this is a verified return.
      manager.setCurrentReturnType(UNVERIFIED);
      return handleNotVerifiedReturn(argCmd);
    }
    else if (argCmd.getOpState() == POST_PROMPT) {
      if ((argEvent instanceof IXstDataAction)
          && (((IXstDataAction) argEvent).getActionKey() == XstDataActionKey.YES)) {
        // Accept -> continue the return as unverified return.
        manager.setCurrentReturnType(UNVERIFIED);
        return handleNotVerifiedReturn(argCmd);
      }

      // Re-search for the original transaction
      manager.setCurrentOrigTransactionId(null);
      argCmd.setOpState(null);

      return HELPER.getRunChainResponse(COMPLETE_HALT, RETURN_ITEM_AUTHORIZE, argCmd, START);
    }
    else {
      logger_.error("No coding to handle this opstate " + argCmd.getOpState());
      return HELPER.silentErrorResponse();
    }
  }

  /**
   * Return whether or not this operation is applicable based on whether or not this return item is returned
   * with a receipt.
   * 
   * @param argCmd the current command
   * @return whether or not this operation is applicable
   */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    return ReturnManager.getInstance().getReturnWithReceipt().booleanValue();
  }

  /**
   * If there is no original sale transaction found and the user choose to continue as a not verified return,
   * this method will trigger not verify return chain.
   * 
   * @param argCmd the current command
   * @return the run chain request
   */
  protected IOpResponse handleNotVerifiedReturn(IXstCommand argCmd) {
    ISaleItemCmd cmd = (ISaleItemCmd) argCmd;
    String returnItemType = cmd.getReturnItemType();
    IOpChainKey chainKey = ReturnItemType.getChainKeyForName(returnItemType);

    if (chainKey != null) {
      return HELPER.getRunChainResponse(COMPLETE_HALT, chainKey, argCmd, START);
    }

    logger_.error("There is no chain key specified for return item type " + returnItemType);
    return HELPER.errorNotifyResponse();
  }

  protected IOpResponse handleOriginalTranNotFound(IXstCommand argCmd) {
    // IF can't find any transaction, prompt the user to either select again
    argCmd.setOpState(POST_PROMPT);
    return HELPER.getPromptResponse(INCOMPLETE_HALT, RETURN_VERIFICATION_NOT_FOUND);
  }

  /**
   * Returns the base price of <code>argSaleLineItem</code> if it has been subject to VAT tax exemption. If it
   * has not been, returns <code>argPrice</code>.
   * 
   * @param argSaleItem the sale line item to check for VAT tax exemption
   * @param argPrice the price to return if the item has not been subject to VAT tax exempted
   * @return the base unit price of <code>argSaleLineItem</code>
   */
  private BigDecimal getVatExemptedPrice(ISaleReturnLineItem argSaleItem, BigDecimal argPrice) {
    if (TaxHelper.getInstance().isLineItemTaxExempt(argSaleItem)) {
      for (ISaleTaxModifier mod : argSaleItem.getTaxModifiers()) {
        if (mod.getVoid()) {
          continue;
        }

        TaxType taxType = TaxType.forName(mod.getSaleTaxGroupRule().getTaxTypeCode());
        // If VAT, then remove the tax amount from the provided price.
        if (TaxType.VAT == taxType) {
          if (mod.getTranTaxableAmt() == null) {
            mod.setTranTaxableAmt(argPrice);
          }

          // Calculate what the VAT amount would be
          VatTaxStrategy vat = new VatTaxStrategy();
          BigDecimal tax =
              vat.calculateRawAmount(argPrice, argSaleItem.getQuantity(), mod, argSaleItem.getBusinessDate(),
                  argPrice);
          argPrice = argPrice.subtract(tax);
        }
      }
    }

    return argPrice;
  }
}
