// $Id: HandleReceiptScanDuringSaleOp.java 832 2015-03-11 21:35:27Z dtvdomain\pgolli $
package dtv.pos.register.returns;

import static dtv.pos.common.ConfigurationMgr.*;
import static dtv.pos.common.TransactionStatus.SUSPEND;
import static dtv.pos.iframework.op.OpStatus.ERROR_HALT;
import static dtv.pos.register.returns.ReturnType.VERIFIED;

import org.apache.log4j.Logger;

import dtv.hardware.events.TransactionScanEvent;
import dtv.pos.common.*;
import dtv.pos.customer.CustAssociationCmd;
import dtv.pos.customer.ICustAssociationCmd;
import dtv.pos.customer.loyalty.LoyaltyMgr;
import dtv.pos.framework.action.type.XstChainActionType;
import dtv.pos.framework.action.type.XstDataActionKey;
import dtv.pos.framework.op.OpState;
import dtv.pos.iframework.action.IXstDataAction;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.*;
import dtv.pos.register.ISaleItemCmd;
import dtv.pos.register.returns.verification.VerifyReturnOp;
import dtv.xst.dao.cat.ICustomerLoyaltyCard;
import dtv.xst.dao.crm.IParty;
import dtv.xst.dao.trl.IRetailTransaction;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.trn.PosTransactionId;

/**
 * This operation handles a receipt scanning event during Xstore sale mode. When a receipt's barcode is
 * scanned, the operation will do the following:<br>
 * - Look for the transaction: if not found, the unverified return prompt will be show to the store associate.<br>
 * - For a found transaction:<br>
 * - Verify the transaction is not a suspended or post-voided transaction, if configured to do so.<br>
 * - Check the transaction for a customer; if not found the store associate will be forced to associate a
 * customer.<br>
 * In all cases above, the operation ends with kicking off the return process.<br>
 * 
 * Copyright (c) 2006 Datavantage Corporation
 * 
 * @author sandrews
 * @version $Revision: 832 $
 * @created Jan 6, 2006
 */
public class HandleReceiptScanDuringSaleOp
    extends AbstractCreateTransactionAsNeededOp {

  private static final long serialVersionUID = 1L;
  private static final Logger logger_ = Logger.getLogger(VerifyReturnOp.class);
  private final ReturnManager rtnManager_ = ReturnManager.getInstance();

  /* Operation state that tells this operation handler that the Unverified Return Prompt was shown the last
   * time this operation was executed. */
  private final IOpState POST_PROMPT = new OpState(this, "POST_PROMPT");
  /* Operation state that tells this operation handler calss that the CUST_ASSOCATION operation chain was
   * initated that last time this operation was executed. */
  private final IOpState RTNCUST_PROMPT = new OpState(this, "RTNCUST_PROMPT");

  public HandleReceiptScanDuringSaleOp() {
    super(TransactionType.RETAIL_SALE);
  }

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    if (argEvent instanceof TransactionScanEvent) {
      /* Search for the transaction */
      PosTransactionId id = ((TransactionScanEvent) argEvent).getTransactionId();

      if (id != null) {
        if (id.getOrganizationId().longValue() <= 0) {
          id.setOrganizationId(new Long(getOrganizationId()));
        }

        rtnManager_.setCurrentOrigTransactionId(id);
        IRetailTransaction rtlTrans = null;
        rtlTrans = TransactionHelper.searchExistingOrigTrans((ISaleItemCmd) argCmd, id);

        if (rtlTrans == null) {
          IPosTransaction trans = TransactionHelper.searchTransFromHost(id);

          if (trans instanceof IRetailTransaction) {
            rtlTrans = (IRetailTransaction) trans;
          }
          else if (trans != null) {
            return HELPER.getPromptResponse(ERROR_HALT, PromptKey.valueOf("RETURN_INVALID_TRANS"));
          }
        }

        if (rtlTrans != null) {
          /* Verify the transaction if configured to do so */
          if (getReturnVerificationRequired()) {
            if (rtlTrans.getPostVoid()) {
              return HELPER.getPromptResponse(ERROR_HALT, PromptKey.valueOf("RETURN_POSTVOID_TRANS"));
            }
            // Should not allow to return suspended transaction.
            else if (SUSPEND.matches(rtlTrans.getTransactionStatusCode())) {
              return HELPER.getPromptResponse(ERROR_HALT, PromptKey.valueOf("RETURN_SUSPENDED_TRANS"));
            }
          }

          rtnManager_.setCurrentOriginalTransaction(rtlTrans);
          rtnManager_.setCurrentReturnType(VERIFIED);
          rtnManager_.setReturnWithReceipt(true);

          /* If a customer is not on the original transaction, get one by kicking off the CUST_ASSOCIATION
           * OpChain. Otherwise, kick off the PRE_RETURN_VERIFICATION OpChain. */
          IParty currentCustomer = ((IRetailTransaction) getTransaction(argCmd)).getCustomerParty();
          IParty purchaseCustomer = rtlTrans.getCustomerParty();

          if (currentCustomer == null) {
            /* CRI 429338 - Don't assign the party record to return transaction if the party id of the original purchase
             * is 0. Party id 0 on the original purchase is coming from ecom transaction lookup and this is only a
             *  transient party record assigned to original purchase for name display purpose on original transaction
             * 
             */

            if (purchaseCustomer != null && purchaseCustomer.getPartyId() != 0) {
              IRetailTransaction newTran = (IRetailTransaction) getTransaction(argCmd);
              newTran.setCustomerParty(purchaseCustomer);
              newTran.setTaxExemption(rtlTrans.getTaxExemption());
            }
            else if (getPromptForCustomerUponReturn()) {
              argCmd.setOpState(RTNCUST_PROMPT);
              return HELPER.getWaitStackChainResponse(OpChainKey.valueOf("CUST_ASSOCIATION"),
                  getCustAssociationCmd(argCmd));
            }
          }
          return handleVerifiedReturn(argCmd);
        }
        else {
          /* Transaction not found, continue anyway? */
          return promptUnverifiedReturn(argCmd);
        }
      }
      else {
        /* Failsafe prompt */
        logger_.warn("Barcode problem....using failsafe unverification prompt");
        return promptUnverifiedReturn(argCmd);
      }
    }
    else if (argCmd.getOpState() == POST_PROMPT) {
      /* Handle store associate response to the unverified return prompt */
      if ((argEvent instanceof IXstDataAction)
          && (((IXstDataAction) argEvent).getActionKey() == XstDataActionKey.YES)) {
        // Accept -> continue the return as unverified return.
        rtnManager_.setCurrentReturnType(ReturnType.UNVERIFIED);
        rtnManager_.setReturnWithReceipt(true);
        return handleNotVerifiedReturn(argCmd);
      }
    }
    else if (argCmd.getOpState() == RTNCUST_PROMPT) {
      /* Handle customer association chain completion (OpChain CUST_ASSOCIATION) */
      ISaleItemCmd cmd = (ISaleItemCmd) argCmd;
      rtnManager_.getCurrentOrigTransaction().setCustomerParty(cmd.getParty());
      return handleVerifiedReturn(argCmd);
    }

    return HELPER.completeResponse();
  }

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    return argCmd instanceof ISaleItemCmd;
  }

  /**
   * Gets the customer association command to use for the customer association chain. Can be overriden to
   * provide a different command.
   * 
   * @param argCmd the current command
   * @return <tt>argCmd</tt> if it implements the needed interface, otherwise a new
   * {@link dtv.pos.customer.CustAssociationCmd}
   */
  protected ICustAssociationCmd getCustAssociationCmd(IXstCommand argCmd) {
    ICustAssociationCmd cmd = new CustAssociationCmd();

    if (isReturnCustomerRecordRequired()) {
      cmd.setSearchRequired(true);
    }

    return cmd;
  }

  /**
   * Kicks off the Unverified Return operation chain.
   * 
   * @param argCmd the current operation chain object.
   * @return run chain request
   */
  protected IOpResponse handleNotVerifiedReturn(IXstCommand argCmd) {
    return HELPER.getRunChainResponse(OpStatus.COMPLETE_HALT, OpChainKey.valueOf("PRE_UNVERIFIED_RETURN"),
        argCmd, XstChainActionType.START);
  }

  /**
   * Shows the Unverified Return prompt to a store associate.
   * 
   * @param argCmd the current operation chain command object.
   * @return prompt request to show Unverified Return prompt
   */
  protected IOpResponse promptUnverifiedReturn(IXstCommand argCmd) {
    argCmd.setOpState(POST_PROMPT);
    return HELPER.getPromptResponse(OpStatus.INCOMPLETE_HALT,
        PromptKey.valueOf("RETURN_VERIFICATION_NOT_FOUND"));
  }

  private IOpResponse handleVerifiedReturn(IXstCommand argCmd) {
    IRetailTransaction originalTrans = rtnManager_.getCurrentOrigTransaction();
    /* There is no reason that the transaction currently on the command at this point should not be a retail
     * transaction representing the the return. Hence, it is OK to just cast it here */
    IRetailTransaction returnTrans = (IRetailTransaction) getTransaction(argCmd);

    if ((originalTrans.getLoyaltyCard() != null) && (returnTrans.getCustomerParty() != null)) {
      String originalLoyaltyCard = originalTrans.getLoyaltyCard();
      IParty customer = returnTrans.getCustomerParty();

      /* If the customer for the return transaction has the loyalty card used on the original transaction,
       * then assign that card to the return transaction as well. */
      for (ICustomerLoyaltyCard loyaltyCard : customer.getLoyaltyCards()) {
        if (loyaltyCard.getCardNumber().equals(originalLoyaltyCard)) {
          LoyaltyMgr.getInstance().setCurrentTransaction(returnTrans);
          LoyaltyMgr.getInstance().setCurrentCard(loyaltyCard);
          break;
        }
      }
    }

    return HELPER.getRunChainResponse(OpStatus.COMPLETE_HALT, OpChainKey.valueOf("VERIFY_RETURN"), argCmd,
        XstChainActionType.START);
  }
}
