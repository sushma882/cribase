//$Id: ChangeTaxGroupRuleOp.java 561 2012-12-17 21:13:50Z dtvdomain\aabdul $
package dtv.pos.register.tax;

import static dtv.util.StringUtils.equivalentIgnoreCase;
import static dtv.pos.common.ConfigurationMgr.getRetailLocationId;

import java.util.List;
import org.apache.log4j.Logger;
import dtv.pos.PosConstants;
import dtv.pos.customer.account.ICustomerItemAccountMaintModel;
import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.util.config.ConfigUtils;
import dtv.util.config.IConfigObject;
import dtv.xst.dao.cat.ICustomerItemAccount;
import dtv.xst.dao.cat.ICustomerItemAccountDetail;
import dtv.xst.dao.com.IReasonCode;
import dtv.xst.dao.tax.ITaxGroupRule;
import dtv.xst.dao.tax.ITaxLocation;
import dtv.xst.dao.trl.*;

/**
 * <p>
 * Operation that changes the tax group (e.g. tax location) of a selected item, retail transaction,
 * or customer item account.
 * </p>
 * 
 * <p>
 * Copyright (c) 2003 Datavantage Corporation
 * </p>
 * 
 * @author dberkland
 * @created July 21, 2003
 * @version $Revision: 561 $
 */
public class ChangeTaxGroupRuleOp
    extends Operation {

  private static final long serialVersionUID = 1L;
  private static final Logger logger_ = Logger.getLogger(ChangeTaxGroupRuleOp.class);

  private boolean isCustAssociationTaxChangeFlag = false;

  /** {@inheritDoc} */
  @Override
  public void setParameter(String argName, IConfigObject argValue) {
    if ("CUST_ASSOCATION_TAX_CHANGE".equalsIgnoreCase(argName)) {
      isCustAssociationTaxChangeFlag = ConfigUtils.asBool(argValue.toString());
    }
    else {
      super.setParameter(argName, argValue);
    }
  }

  /**
   * Determine the level of tax being changed and iterate any item taxes if needed.
   * 
   * @param argCmd the current command
   * @param argEvent the event that occurred
   * @return response
   */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    IChangeTaxCmd cmd = (IChangeTaxCmd) argCmd;
    Object changeItem = cmd.getChangeItem();

    if (changeItem instanceof IRetailTransaction) {
      return changeTax(cmd, (IRetailTransaction) changeItem);
    }
    else if (changeItem instanceof ISaleReturnLineItem) {
      return changeTax(cmd, (ISaleReturnLineItem) changeItem);
    }
    else if (changeItem instanceof ICustomerItemAccountMaintModel) { // pos object
      return changeTax(cmd, (ICustomerItemAccountMaintModel) changeItem);
    }
    else if (changeItem instanceof ICustomerItemAccount) { // dtx object
      return changeTax(cmd, (ICustomerItemAccount) changeItem);
    }
    else {
      logger_.error("Unexpected tax change item [" + changeItem + "]", new Throwable("STACK TRACE"));
      return HELPER.errorNotifyResponse();
    }
  }

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    return ((argCmd instanceof IChangeTaxCmd) && (((IChangeTaxCmd) argCmd).getChangeItem() != null));
  }

  /**
   * Change the tax on a customer item account.
   * 
   * @param argCmd the current command.
   * @param argItemAccount the item account to change.
   * @return the result of changing the tax.
   */
  protected IOpResponse changeTax(IChangeTaxCmd argCmd, ICustomerItemAccount argItemAccount) {
    Iterable<ICustomerItemAccountDetail> details = argItemAccount.getCustItemAccountDetails();

    if (details != null) {
      for (ICustomerItemAccountDetail detail : details) {
        IRetailTransactionLineItem line = detail.getRetailLineItem();
        if (line instanceof ITaxLineItem) {
          ((ITaxLineItem) line).setVoid(true);
        }
      }
      for (ICustomerItemAccountDetail detail : details) {
        IRetailTransactionLineItem line = detail.getRetailLineItem();
        if (line instanceof ISaleReturnLineItem) {
          changeTax(argCmd, (ISaleReturnLineItem) line);
        }
      }
    }
    return HELPER.completeResponse();
  }

  /**
   * Change the tax on a customer account.
   * 
   * @param argCmd the current command.
   * @param argAccountModel the account model to change.
   * @return the result of changing the tax.
   */
  protected IOpResponse changeTax(IChangeTaxCmd argCmd, ICustomerItemAccountMaintModel argAccountModel) {
    ICustomerItemAccount account = argAccountModel.getAccount();
    return changeTax(argCmd, account);
  }

  /**
   * Change the tax on a transaction.
   * 
   * @param argCmd the current command.
   * @param argTransaction the transaction to change.
   * @return the result of changing the tax.
   */
  protected IOpResponse changeTax(IChangeTaxCmd argCmd, IRetailTransaction argTransaction) {
    // void the existing tax lines
    Iterable<ITaxLineItem> taxLines = argTransaction.getLineItems(ITaxLineItem.class);
    if (taxLines != null) {
      for (ITaxLineItem taxLine : taxLines) {
        taxLine.setVoid(true);
      }
    }

    // change taxes on all lines
    Iterable<ISaleReturnLineItem> itemLines = argTransaction.getLineItems(ISaleReturnLineItem.class);
    if (itemLines != null) {
      for (ISaleReturnLineItem itemLine : itemLines) {
        changeTax(argCmd, itemLine);
      }
    }
    return HELPER.completeResponse();
  }

  /**
   * Change the tax for a line item.
   * 
   * @param argCmd the current command.
   * @param argLineItem the line item to change.
   * @return the result of changing the tax.
   */
  protected IOpResponse changeTax(IChangeTaxCmd argCmd, ISaleReturnLineItem argLineItem) {
    String taxGroupId =
        TaxHelper.getInstance().getCurrentSaleItemTaxGroup(argLineItem.getItem(), argCmd.getCustomer());

    if (!taxRulesChanging(argLineItem, taxGroupId, argCmd.getTaxLocation())) {
      return HELPER.completeResponse();
    }

    //Checking if the tax for any line items where changed from the default current retail location ID. 
    //If so skipping the tax change for that line item
    if (isTaxLocationChanged(argLineItem,
        TaxHelper.getInstance().getLocations().getByRetailLocation(getRetailLocationId()))) {
      return HELPER.completeResponse();
    }

    List<ISaleTaxModifier> modifiers = argLineItem.getTaxModifiers();
    for (ISaleTaxModifier modifier : modifiers) {
      modifier.setVoid(true);
    }

    List<ITaxGroupRule> rules =
        TaxHelper.getInstance().getGroupRules().get(taxGroupId, argCmd.getTaxLocation());

    TaxHelper.getInstance().createSaleTaxModifiers(argLineItem, rules);

    modifiers = argLineItem.getTaxModifiers();
    for (ISaleTaxModifier modifier : modifiers) {
      if (!modifier.getVoid()) {
        modifier.setTaxOverride(true);
        if (getTaxChangeReason(argCmd) != null) {
          modifier.setTaxOverrideReasonCode(getTaxChangeReason(argCmd).getReasonCode());
        }
      }
    }
    return HELPER.completeResponse();
  }

  /**
   * Get the tax change reason code from the command.
   * 
   * @param argCmd the command on which the reason code is stored.
   * @return the reason code.
   */
  protected IReasonCode getTaxChangeReason(IXstCommand argCmd) {
    return (IReasonCode) argCmd.getValue(PosConstants.SELECTED_REASON_CODE);
  }

  /**
   * Examines the tax modifiers on the line item and returns true if the tax rules are going to
   * change based off of the passed tax group id and tax location.
   * 
   * @param argItem the line item who's modifiers are to be inspected.
   * @param argTaxGroupId the new tax group id that the tax modifiers will be based off of.
   * @param argTaxLoc the new tax location that the rules will be based off of.
   * @return <code>true</code> if eighter <code>argTaxGroupId</code> or <code>argTaxLoc</code> is
   * different from at least one current active tax modifier's group and location ids.
   * <code>false</code> otherwise.
   */
  protected boolean taxRulesChanging(ISaleReturnLineItem argItem, String argTaxGroupId, ITaxLocation argTaxLoc) {
    if (argTaxLoc == null) {
      return false;
    }

    boolean hasActiveModifier = false;
    for (ISaleTaxModifier modifier : argItem.getTaxModifiers()) {
      if (!modifier.getVoid()) {
        hasActiveModifier = true;
        if (!equivalentIgnoreCase(modifier.getTaxGroupId(), argTaxGroupId)
            || !equivalentIgnoreCase(modifier.getTaxLocationId(), argTaxLoc.getTaxLocationId())) {
          return true;
        }
      }
    }
    return !hasActiveModifier;
  }

  /**
   * Examines the tax modifiers on the line item and returns true if the tax rules are changed from
   * the current retail tax location
   * 
   * @param argItem the line item who's modifiers are to be inspected.
   * @param argTaxLoc the tax location of the current retail location.
   * @return <code>true</code> if <code>argTaxLoc</code> is different from the current line item tax
   * modifiers <code>false</code> otherwise.
   */
  protected boolean isTaxLocationChanged(ISaleReturnLineItem argItem, ITaxLocation argTaxLoc) {

    String saleItemType = argItem.getSaleReturnLineItemTypeCode();

    if (!(SaleItemType.SALE.equals(SaleItemType.forName(saleItemType)) || SaleItemType.RETURN
        .equals(SaleItemType.forName(saleItemType)))) {
      return false;
    }
    
    if(!isCustAssociationTaxChangeFlag) {
      return false;
    }

    boolean isTaxChanged = false;
    for (ISaleTaxModifier modifier : argItem.getTaxModifiers()) {
      if (!modifier.getVoid()) {
        if (!equivalentIgnoreCase(modifier.getTaxLocationId(), argTaxLoc.getTaxLocationId())) {
          isTaxChanged = true;
          break;
        }
      }
    }
    return isTaxChanged;

  }
}
