// $Id: TaxHelper.java 56627 2011-08-04 16:06:57Z dtvdomain\jweiss $

package dtv.pos.register.tax;

import static dtv.pos.register.returns.ReturnType.VERIFIED;

import static dtv.pos.common.ConfigurationMgr.getRetailLocationId;
import static dtv.util.NumberUtils.isZeroOrNull;
import static dtv.util.StringUtils.equivalentIgnoreCase;
import static java.math.BigDecimal.ZERO;
import static java.math.RoundingMode.HALF_EVEN;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import org.apache.log4j.Logger;

import dtv.data2.access.*;
import dtv.pos.common.ConfigurationMgr;
import dtv.pos.common.TransactionHelper;
import dtv.pos.customer.account.type.CustomerItemAccountDetailStateType;
import dtv.pos.register.type.LineItemType;
import dtv.pos.register.type.NonPhysicalItemType;
import dtv.pos.storecalendar.StoreCalendar;
import dtv.util.*;
import dtv.util.config.ConfigException;
import dtv.util.sequence.SequenceFactory;
import dtv.xst.dao.cat.ICustomerItemAccountDetail;
import dtv.xst.dao.crm.ICustomerAffiliation;
import dtv.xst.dao.crm.IParty;
import dtv.xst.dao.itm.IItem;
import dtv.xst.dao.itm.INonPhysicalItem;
import dtv.xst.dao.tax.*;
import dtv.xst.dao.trl.*;

/**
 * This class contains all tax-related data object operations. The purpose is to separate the data object
 * coding from the regular operation.<br>
 * <br>
 * Copyright (c) 2003 Datavantage Corporation
 * 
 * @author jhsiao
 * @created April 21, 2003
 * @version $Revision: 56627 $
 */
public class TaxHelper {
  public static final String SEQ_TYPE_TAX_EXEMPTION = "TAX_EXEMPTION";
  public static final String ALL_CUSTOMER_GROUPS = "ALL";

  private static final Logger logger_ = Logger.getLogger(TaxHelper.class);
  private static final IQueryKey<ITaxExemption> TAX_EXEMPTIONS = new QueryKey<ITaxExemption>(
      "TAX_EXEMPTIONS", ITaxExemption.class);
  private static final IQueryKey<ITaxTaxGroupMapping> NEW_TAX_TAX_GROUP = new QueryKey<ITaxTaxGroupMapping>(
      "NEW_TAX_TAX_GROUP", ITaxTaxGroupMapping.class);
  private static final String SYSTEM_PROPERTY = TaxHelper.class.getName();
  private static final TaxHelper INSTANCE;

  static {
    TaxHelper instance;
    // Check the system configured factory to use...
    try {
      instance = (TaxHelper) Class.forName(System.getProperty(SYSTEM_PROPERTY)).newInstance();
    }
    catch (Throwable ex) {
      logger_.info("Creating default tax helper");
      instance = new TaxHelper();
    }
    INSTANCE = instance;
  }

  /**
   * Returns a tax helper.
   * @return a tax helper
   */
  public static TaxHelper getInstance() {
    return INSTANCE;
  }

  /**
   * Summarizes the taxes for the items in <code>argAccountDetails</code>, based on tax group rules.
   * @param argAccountDetails the account details for which taxes should be summarized
   * @param argActiveOnly a flag that indicates if only taxes for active items should be included
   * @return the tax summary for this account
   */
  public static Map<TaxGroupRuleId, TaxInfo> getTaxSummary(
      List<ICustomerItemAccountDetail> argAccountDetails, boolean argActiveOnly) {
    Map<TaxGroupRuleId, TaxInfo> taxSummary = new HashMap<TaxGroupRuleId, TaxInfo>();

    for (ICustomerItemAccountDetail detail : argAccountDetails) {
      CustomerItemAccountDetailStateType status =
          CustomerItemAccountDetailStateType.forName(detail.getStateCode());

      if (argActiveOnly && !status.isOpen()) {
        continue;
      }

      if (detail.getRetailLineItem() instanceof ISaleReturnLineItem) {
        ISaleReturnLineItem line = (ISaleReturnLineItem) detail.getRetailLineItem();

        for (ISaleTaxModifier saleTaxModifier : line.getTaxModifiers()) {
          TaxGroupRuleId id = (TaxGroupRuleId) saleTaxModifier.getSaleTaxGroupRule().getObjectId();

          if (!saleTaxModifier.getVoid()) {
            TaxInfo taxInfo = taxSummary.get(id);

            if (taxInfo == null) {
              taxInfo = new TaxInfo(saleTaxModifier);
              taxSummary.put(id, taxInfo);
            }
            else {
              taxInfo.add(saleTaxModifier);
            }
          }
        }
      }
    }

    return taxSummary;
  }

  /**
   * Indicates whether the specified customer has any active tax exemptions.
   * 
   * @param argCustomer the reference customer
   * @return <code>true</code> if <code>argCustomer</code> has any active tax exemptions; <code>false</code>
   * otherwise
   */
  public static boolean hasCurrentTaxExemptions(IParty argCustomer) {
    return (argCustomer != null) //
        && !getInstance().getTaxExemptions(argCustomer, StoreCalendar.getCurrentBusinessDate()).isEmpty();
  }

  protected final TaxLocationCache taxLocations_;

  protected TaxSystemType type_;
  private final TaxGroupRuleCache taxGroupRules_;
  private final TaxRateRuleCache rateRules_;

  private final TaxBracketCache taxBrackets_;

  /**
   * The constructor for the TaxHelper class.
   */
  protected TaxHelper() {
    taxLocations_ = TaxLocationCache.getInstance();
    taxGroupRules_ = TaxGroupRuleCache.getInstance();
    rateRules_ = TaxRateRuleCache.getInstance();
    taxBrackets_ = TaxBracketCache.getInstance();
    type_ = TaxSystemType.XSTORE;
  }

  /**
   * Copy a tax line item. If either argument is null, does nothing.
   * @param argOrigTaxLineItem the source tax line item.
   * @param argNewTaxLineItem the destination tax line item whose data will be overwritten.
   */
  public void copyTaxLineItem(ITaxLineItem argOrigTaxLineItem, ITaxLineItem argNewTaxLineItem) {
    if ((argOrigTaxLineItem == null) || (argNewTaxLineItem == null)) {
      return;
    }

    argNewTaxLineItem.setTaxableAmount(argOrigTaxLineItem.getTaxableAmount());
    argNewTaxLineItem.setTaxAmount(argOrigTaxLineItem.getTaxAmount());
    argNewTaxLineItem.setTaxGroupId(argOrigTaxLineItem.getTaxGroupId());
    argNewTaxLineItem.setTaxLocationId(argOrigTaxLineItem.getTaxLocationId());
    argNewTaxLineItem.setTaxOverride(argOrigTaxLineItem.getTaxOverride());
    argNewTaxLineItem.setTaxOverrideAmount(argOrigTaxLineItem.getTaxOverrideAmount());
    argNewTaxLineItem.setTaxOverridePercentage(argOrigTaxLineItem.getTaxOverridePercentage());
    argNewTaxLineItem.setTaxOverrideReasonCode(argOrigTaxLineItem.getTaxOverrideReasonCode());
    argNewTaxLineItem.setTaxPercentage(argOrigTaxLineItem.getTaxPercentage());
    argNewTaxLineItem.setTaxRuleSequence(argOrigTaxLineItem.getTaxRuleSequence());
  }

  /**
   * Copy exact tax modifier percentages.
   * @param newLine the new sale line item to set.
   * @param origSaleItem the original sale line item.
   */
  public void copyTaxModifiersExactPercent(ISaleReturnLineItem newLine, ISaleReturnLineItem origSaleItem) {
    List<ISaleTaxModifier> modList = origSaleItem.getTaxModifiers();

    if (modList != null) {
      for (ISaleTaxModifier oldMod : modList) {
        ISaleTaxModifier newMod = TaxHelper.getInstance().createEmptySaleTaxModifier(newLine);

        newMod.setTaxableAmount(oldMod.getTaxableAmount());
        newMod.setTaxAmount(oldMod.getTaxAmount());
        newMod.setTaxExemptAmount(oldMod.getTaxExemptAmount());
        newMod.setTaxExemptionId(oldMod.getTaxExemptionId());
        newMod.setSaleTaxGroupRule(oldMod.getSaleTaxGroupRule());
        newMod.setAuthorityId(oldMod.getAuthorityId());
        newMod.setAuthorityName(oldMod.getAuthorityName());
        newMod.setAuthorityTypeCode(oldMod.getAuthorityTypeCode());
        newMod.setTaxOverrideBracket(oldMod.getTaxOverrideBracket());

        newMod.setTaxOverrideReasonCode(oldMod.getTaxOverrideReasonCode());

        newMod.setTaxOverride(oldMod.getTaxOverride());

        if (oldMod.getTaxOverride() && (oldMod.getTaxOverridePercentage() != null)) {
          newMod.setTaxOverridePercentage(oldMod.getTaxOverridePercentage());
        }
        else if (oldMod.getTaxOverride() && (oldMod.getTaxOverrideAmount() != null)) {
          BigDecimal percent = newLine.getQuantity().divide(origSaleItem.getQuantity(), HALF_EVEN);
          newMod.setTaxOverrideAmount(percent.multiply(oldMod.getTaxOverrideAmount()));
        }
        else if (!isZeroOrNull(oldMod.getRawTaxPercentage())) {
          newMod.setTaxOverridePercentage(calculateTaxOverridePercentage(origSaleItem, oldMod));
        }
        else {
          newMod.setTaxOverrideAmount(oldMod.getRawTaxAmount());
        }
        newMod.setVoid(oldMod.getVoid());

        newLine.addSaleTaxModifier(newMod);
      }
    }
  }

  /**
   * Create a new sale tax modifier for the sale line item.
   * 
   * @param argLineItem the line item for which to create a tax modifier
   * @return the created modifier
   * @throws ConfigException if there is an error creating the tax modifier object
   */
  public ISaleTaxModifier createEmptySaleTaxModifier(ISaleReturnLineItem argLineItem)
      throws ConfigException {

    try {
      ISaleTaxModifier taxModifier = DataFactory.createObject(ISaleTaxModifier.class);
      // argLineItem.addTaxModifier(taxModifier);
      return taxModifier;
    }
    catch (Exception ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
      throw new ConfigException(ex);
    }
  }

  /**
   * Create new sale tax modifier for the particular tax group rule
   * 
   * @param argLineItem the line item this new sale tax modifier will be attached to
   * @param argTaxGroupRules the tax group rule obejct for this sale tax modifier
   */
  public void createSaleTaxModifiers(ISaleReturnLineItem argLineItem, List<ITaxGroupRule> argTaxGroupRules) {
    if (argTaxGroupRules != null) {
      for (int i = 0; i < argTaxGroupRules.size(); i++ ) {
        ISaleTaxModifier taxModifier = null;
        taxModifier = DataFactory.createObject(ISaleTaxModifier.class);
        ITaxGroupRule groupRule = argTaxGroupRules.get(i);
        taxModifier.setSaleTaxGroupRule(groupRule);
        /* DAOGen does not permit default values for BigDecimal. Setting the default value via DDL will not
         * work since the field is specified in the insert statement. (0) is required istead of null to permit
         * the SUM() to work properly in the daily sales and cash report. */
        taxModifier.setTaxAmount(BigDecimal.ZERO);
        taxModifier.setAuthorityId(groupRule.getTaxAuthority().getTaxAuthorityId());
        taxModifier.setAuthorityName(groupRule.getTaxAuthority().getName());
        taxModifier.setAuthorityTypeCode(groupRule.getTaxAuthority().getTaxAuthorityId());
        taxModifier.setTaxOverrideBracket(groupRule.getTaxRateRules().get(0).getTaxBracketId());
        argLineItem.addSaleTaxModifier(taxModifier);
      }
    }

  }

  /**
   * Create a tax exemption for a party.
   * @param argParty the party to receive an exemption.
   * @return the tax exemption that is created.
   */
  public ITaxExemption createTaxExemption(IParty argParty) {
    String idKey = SequenceFactory.getNextStringValue(SEQ_TYPE_TAX_EXEMPTION);
    TaxExemptionId id = new TaxExemptionId();
    // Get a new tax exemption id from the sequence manager
    id.setTaxExemptionId(idKey);

    ITaxExemption exemption = DataFactory.createObject(id, ITaxExemption.class);

    exemption.setPartyId(argParty.getPartyId());

    return exemption;
  }

  /**
   * Create and return the new tax line item object for this particular retail transaction and tax group rule.
   * 
   * @param argTaxGroupRule the tax group rule to the tax line item with
   * @return the new tax line item object for this particular retail transaction and tax group rule.
   */
  public ITaxLineItem createTaxLineItem(dtv.xst.dao.trn.IPosTransaction argParentTransaction,
      ITaxGroupRule argTaxGroupRule) {
    return createTaxLineItem(null, argParentTransaction, argTaxGroupRule);
  }

  /**
   * Create and return the new tax line item object for this particular retail transaction and tax group rule.
   * 
   * @param argTaxGroupRule the tax group rule to the tax line item with
   * @return the new tax line item object for this particular retail transaction and tax group rule.
   */
  public ITaxLineItem createTaxLineItem(RetailTransactionLineItemId argId,
      dtv.xst.dao.trn.IPosTransaction argParentTransaction, ITaxGroupRule argTaxGroupRule) {

    ITaxLineItem taxLineItem = null;

    if (argId == null) {
      taxLineItem = DataFactory.createObject(ITaxLineItem.class);
    }
    else {
      taxLineItem = DataFactory.createObject(argId, ITaxLineItem.class);
    }

    taxLineItem.setBeginDateTimestamp(DateUtils.getNewDate());
    taxLineItem.setEndDateTimestamp(DateUtils.getNewDate());
    taxLineItem.setLineItemTypeCode(LineItemType.TAX.getName());
    taxLineItem.setSaleTaxGroupRule(argTaxGroupRule);

    taxLineItem.setAuthorityId(argTaxGroupRule.getTaxAuthority().getTaxAuthorityId());
    taxLineItem.setAuthorityName(argTaxGroupRule.getTaxAuthority().getName());

    /* 7/1/09 jweiss The following has been delegated to a protected method. In the base implementation, we
     * can't transfer values from tax_tax_group_rule.description to trl_tax_lineitm.authority_type_code
     * because the former is 254-characters wide while the latter is only 30. Subsequent accommodations will
     * have to be made for Taxware-specific requirements. */
    // This seems backwards, but the group rule's actual "type code"
    // is just a generic sales vs. VAT tax indicator and becomes
    // meaningless in this context -- the group rule's description,
    // which would be the jurisdiction type for TaxWare, is what we
    // really want here.
    // taxLineItem.setAuthorityTypeCode(argTaxGroupRule.getDescription());
    taxLineItem.setAuthorityTypeCode(getTaxLineAuthorityType(argTaxGroupRule));

    return taxLineItem;
  }

  /**
   * Get the tax brackets
   * @return the tax brackets
   */
  public TaxBracketCache getBrackets() {
    return taxBrackets_;
  }

  /**
   * Return the tax group of the current sale line item based on customer group and item tax group.
   * 
   * @param item item, IParty party
   * @return the tax group of the current sale line item.
   */
  public String getCurrentSaleItemTaxGroup(IItem item, IParty party) {
    if (item == null) {
      logger_.error("There is no item attached to this sale return line item to calculate tax.");
      return null;
    }

    String taxGroupId = null;
    taxGroupId = item.getTaxGroupId();
    if (item.getTaxGroupId() == null) {
      logger_.error("No Tax group specified in the item table for item id " + item.getItemId());

    }

    if ((taxGroupId != null)) {
      String newTaxGroupId = getNewTaxTaxGroup(taxGroupId, party);

      if (newTaxGroupId != null) {
        taxGroupId = newTaxGroupId;
      }
    }

    return taxGroupId;
  }

  /**
   * Get the tax group rules.
   * @return the tax group rules.
   */
  public TaxGroupRuleCache getGroupRules() {
    return taxGroupRules_;
  }

  /**
   * Get the tax locations.
   * @return the tax locations.
   */
  public TaxLocationCache getLocations() {
    return taxLocations_;
  }

  /**
   * Returns the sale tax modifier to which remainder tax should be applied (for example, during an override).
   * 
   * @param argMods the list of sale tax modifiers
   * @return the modifier to be used in applying remainder tax
   */
  public ISaleTaxModifier getModForRemainderTax(List<ISaleTaxModifier> argMods) {
    if ((argMods == null) || argMods.isEmpty()) {
      return null;
    }
    // return last mod
    return argMods.get(argMods.size() - 1);
  }

  /**
   * Get the parent group rule for a tax rate rule, or null if not present.
   * @param rule the rule to look up.
   * @return the parent group rule, or null.
   */
  public dtv.xst.dao.tax.ITaxGroupRule getParentGroupRule(ITaxRateRule rule) {
    List<ITaxGroupRule> rules = null;

    if ((rule != null) && (taxGroupRules_ != null)) {
      // Get the parent group rule based on the rule's group id
      String taxGroupId = rule.getTaxGroupId();
      ITaxLocation location = taxLocations_.getByRetailLocation(ConfigurationMgr.getRetailLocationId());
      rules = taxGroupRules_.get(taxGroupId, location);
    }
    if ((rules != null) && !rules.isEmpty()) {
      return rules.get(0);
    }
    return null;
  }

  /***
   * Get the tax rate rules.
   * @return the tax rate rules.
   */
  public TaxRateRuleCache getRateRules() {
    return rateRules_;
  }

  /**
   * Returns the unrounded tax total for the given sale return line item. VAT taxes are excluded from this
   * total, as they have been accounted for in the item's price.
   * 
   * @param argLineItem the line item to check.
   * @return the estimated tax for the line item.
   */
  public BigDecimal getTax(ISaleReturnLineItem argLineItem) {
    BigDecimal totalLineItemTax = ZERO;

    // if not using TaxWare, this doesn't do anything
    TaxHelper.getInstance().updateGroupRulesForSaleLine(argLineItem);

    List<ISaleTaxModifier> taxModifiers = argLineItem.getTaxModifiers();
    if (taxModifiers != null) {
      for (ISaleTaxModifier mod : taxModifiers) {
        // Tom -> Don't add in tax if it's already included!
        if (TaxType.VAT.equals(TaxType.forName(mod.getSaleTaxGroupRule().getTaxTypeCode()))) {
          continue;
        }
        totalLineItemTax = totalLineItemTax.add(mod.getTaxAmount());
      }
    }
    return totalLineItemTax;
  }

  /**
   * Get the tax exemptions for a party.
   * @param argParty the party to check.
   * @return the tax exemptions.
   */
  public List<ITaxExemption> getTaxExemptions(IParty argParty) {
    return getTaxExemptions(argParty.getPartyId(), null, null);
  }

  /**
   * Get the tax exemptions for a party on a particular date.
   * @param argParty the party to check.
   * @param argDate the date to check.
   * @return the tax exemptions.
   */
  public List<ITaxExemption> getTaxExemptions(IParty argParty, Date argDate) {
    return getTaxExemptions(argParty.getPartyId(), argDate, null);
  }

  /**
   * Get the tax exemptions for a customer on a particular date with a reason.
   * @param argCustomer the customer to check.
   * @param argDate the date to check.
   * @param argReasonCode the reason code to check.
   * @return the tax exemptions.
   */
  public List<ITaxExemption> getTaxExemptions(IParty argCustomer, Date argDate, String argReasonCode) {
    return getTaxExemptions(argCustomer.getPartyId(), argDate, argReasonCode);
  }

  /**
   * Get the tax exemptions for a party.
   * @param argPartyId the party ID to check.
   * @return the tax exemptions.
   */
  public List<ITaxExemption> getTaxExemptions(long argPartyId) {
    return getTaxExemptions(argPartyId, null, null);
  }

  /**
   * Get the tax exemptions for a party on a date with a reason.
   * 
   * @param argPartyId the party ID to check
   * @param argDate the date to check
   * @param argReasonCode the reason code to check
   * @return the tax exemptions
   */
  public List<ITaxExemption> getTaxExemptions(long argPartyId, Date argDate, String argReasonCode) {
    try {
      Map<String, Object> params = new HashMap<String, Object>();
      params.put("argPartyId", Long.valueOf(argPartyId));
      if (argDate != null) {
        params.put("argExpDate", argDate);
      }
      if (argReasonCode != null) {
        params.put("argReasonCode", argReasonCode);
      }
      return DataFactory.getObjectByQuery(TAX_EXEMPTIONS, params);
    }
    catch (ObjectNotFoundException ex) {
      logger_.info("no tax exemptions found");
    }
    return new ArrayList<ITaxExemption>();
  }

  /**
   * Determines the tax group ID that should be used to tax this line item.
   * 
   * @param argLineItem the line item
   * @return the tax group ID to use for taxation
   */
  public String getTaxGroupId(ISaleReturnLineItem argLineItem) {
    if (argLineItem != null) {
      return argLineItem.getTaxGroupId();

    }
    return null;
  }

  /**
   * Return the tax line item object for the particular retail transaction and tax group rule.
   * 
   * @param argTaxGroupRule the tax group rule for the tax line item we are looking for
   * @param argRetailTrans the retail transaction we look for the tax line item from
   * @return the tax line item object for the particular retail transaction and tax group rule.
   */
  public ITaxLineItem getTaxLineItem(ITaxGroupRule argTaxGroupRule, IRetailTransaction argRetailTrans) {
    List<IRetailTransactionLineItem> lineItems =
        argRetailTrans.getLineItemsByTypeCode(LineItemType.TAX.getName());
    for (IRetailTransactionLineItem lineItem : lineItems) {
      if (lineItem instanceof ITaxLineItem) {
        ITaxLineItem taxLineItem = (ITaxLineItem) lineItem;
        if (taxLineItem.getSaleTaxGroupRule() == argTaxGroupRule) {
          return taxLineItem;
        }
      }
      else {
        logger_.warn(new StringBuilder("Transaction contains line item with type of [")
            .append(LineItemType.TAX.getName()).append("] that does not implement [")
            .append(ITaxLineItem.class.getName()).append("]"));
      }
    }

    // If reach this point, means no tax line item contains this tax group rule.
    // So create one.
    return createTaxLineItem(argRetailTrans, argTaxGroupRule);
  }

  /**
   * Returns the type-safe enum object corresponding to this instance. <br>
   * @return the tax system that this helper implements
   */
  public TaxSystemType getTaxSystemType() {
    return type_;
  }

  /**
   * Get the total tax percent for a tax modifier.
   * @param argSaleTaxMod the tax modifier to process.
   * @return the total percentage tax for this modifier.
   */
  public BigDecimal getTotalTaxPercentForTaxModifier(ISaleTaxModifier argSaleTaxMod) {
    ITaxGroupRule taxGroupRule = argSaleTaxMod.getSaleTaxGroupRule();
    List<ITaxRateRule> rateRules = taxGroupRule.getTaxRateRules();

    BigDecimal totalTaxPercent = NumberUtils.ZERO;

    if (rateRules != null) {
      for (ITaxRateRule rateRule : rateRules) {
        BigDecimal rate =
            rateRule.getPercent() != null ? rateRule.getPercent() : rateRule.getAmount() != null ? rateRule
                .getAmount() : NumberUtils.ZERO;

        // If its a zero extended amount sale line item, need to get the extended
        // amount from the customer item account modifier.
        ISaleReturnLineItem saleLine = argSaleTaxMod.getParentLine();
        BigDecimal extAmt = saleLine.getExtendedAmount();
        if (saleLine.getForceZeroExtendedAmt()) {
          ICustomerItemAccountModifier custAcctMod = saleLine.getCustomerAccountModifier();

          if ((custAcctMod != null) && (custAcctMod.getItemAccountExtendedPrice() != null)) {
            extAmt = custAcctMod.getItemAccountExtendedPrice();
          }
          else {
            // Unless there is no modifier, then just calculate it like it normally would be calculated
            extAmt = saleLine.getUnitPrice().multiply(saleLine.getQuantity());
          }
        }

        if (!NumberUtils.isZeroOrNull(extAmt)) {
          totalTaxPercent = totalTaxPercent.add(rate.divide(extAmt, 10, RoundingMode.HALF_EVEN));
        }
      }
    }

    return totalTaxPercent;
  }

  /**
   * Check to see if the given line item is tax exempt. A line item is tax exempt if it has any tax modifiers
   * that are exempt.
   * 
   * @param argLineItem the line item to test
   * @return whether the item is tax exempt
   */
  public boolean isLineItemTaxExempt(ISaleReturnLineItem argLineItem) {
    // first, determine if entire transaction is tax exempt
    IRetailTransaction trans = (IRetailTransaction) argLineItem.getParentTransaction();
    if ((trans != null) && (trans.getTaxExemption() != null)) {
      return true;
    }

    List<ISaleTaxModifier> taxMods = argLineItem.getTaxModifiers();
    if (taxMods != null) {
      for (ISaleTaxModifier modifier : taxMods) {
        if (modifier.getTaxExemptionId() != null) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Check to see if the given line item has a tax override.
   * 
   * @param argLineItem the line item to test
   * @return whether the item has a tax override
   */
  public boolean isLineItemTaxOverriden(ISaleReturnLineItem argLineItem) {
    List<ISaleTaxModifier> taxMods = argLineItem.getTaxModifiers();
    if (taxMods != null) {
      for (ISaleTaxModifier modifier : taxMods) {
        if (modifier.getTaxOverride()) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * determines whether override amounts calculated by a tax strategy are applicable to this modifier
   * @param argModifier the tax modifier of interest
   * @return true if overrides should be calculated via the tax strategy
   */
  public boolean isOverrideCalcApplicable(ISaleTaxModifier argModifier) {
    if (argModifier.getTaxOverride() //
        || TransactionHelper.isVerifiedReturn(argModifier.getParentLine()) //
        || TransactionHelper.isRemoteSendRefund(argModifier.getParentLine()) //
        || TransactionHelper.isRemoteSendPickup(argModifier.getParentLine())) {
      return true;
    }
    return false;
  }

  /**
   * Check to see if a sale tax modifier is tax exempt.
   * 
   * @param argMod the sale tax modifier to check
   * @return true if the modifier is considered tax exempt
   */
  public boolean isSaleTaxModifierTaxExempt(ISaleTaxModifier argMod) {
    /* The modifier is exempt if it is either explicitly identified as such for the line item it modifies or
     * if it belongs to a transaction that is exempt. */
    boolean exempt = !StringUtils.isEmpty(argMod.getTaxExemptionId());
    
    if (!exempt) {
      
      if (!argMod.getParentLine().getReturn()
          && !VERIFIED.matches(argMod.getParentLine().getReturnTypeCode())) {
        exempt =
            (((IRetailTransaction) argMod.getParentLine().getParentTransaction()).getTaxExemption() != null);
      } 
     
    }
    
    
    
    return exempt;
  }

  /**
   * Determines whether <code>argLineItem</code> is a non-monetary item to which tax could potentially apply.
   * 
   * @param argLineItem the line item to check
   * @return true if this is a non-monetary item
   */
  public boolean isTaxApplicable(ISaleReturnLineItem argLineItem) {
    // somehow we're getting null pointers here.
    if (argLineItem == null) {
      return false;
    }

    IItem item = argLineItem.getItem();
    if ((item != null) && (item instanceof INonPhysicalItem)) {
      INonPhysicalItem npi = (INonPhysicalItem) item;
      /* "Monetary" non-physical items such as payments, fees and deposits cannot have tax applied to them */
      if (NonPhysicalItemType.forName(npi.getNonPhysicalItemTypeCode()).isMonetary()) {
        return false;
      }
    }

    return true;
  }

  /**
   * Determine if a list of modifiers represents a tax.
   * @param argMods the mods to check.
   * @return true if the mods specify a tax, false if they have no tax.
   */
  public boolean isThisTaxable(List<? extends ISaleTaxModifier> argMods) {
    boolean result = true;

    if ((argMods != null) && !argMods.isEmpty()) {
      BigDecimal taxAmt = NumberUtils.ZERO;
      BigDecimal taxPercent = NumberUtils.ZERO;

      for (ISaleTaxModifier mod : argMods) {

        List<ITaxRateRule> taxRateRule = mod.getSaleTaxGroupRule().getTaxRateRules();
        if (taxRateRule != null) {
          for (ITaxRateRule rateRule : taxRateRule) {
            taxAmt = taxAmt.add(NumberUtils.nonNull(rateRule.getAmount()));
            taxPercent = taxPercent.add(NumberUtils.nonNull(rateRule.getPercent()));
          }
        }
      }

      if (NumberUtils.isZero(taxAmt) && NumberUtils.isZero(taxPercent)) {
        result = false;
      }
    }

    return result;
  }

  /**
   * Check to see if the given transaction has any tax exempt line items.
   * 
   * @param argTrans the transaction to test
   * @return whether the transaction has any tax exempt items
   */
  public boolean isTransactionTaxExempt(IRetailTransaction argTrans) {
    return (argTrans.getTaxExemption() != null);
  }

  /**
   * Access the various tax caches to get them to load up the most commonly used tax objects.
   */
  public void preload() {}

  /**
   * Remove voided tax lines from a list of tax line items.
   * @param taxLines the list of tax items to check.
   * @return a new list containing only those tax lines that are not voided.
   */
  public List<ITaxLineItem> removeVoidedTaxLines(List<ITaxLineItem> taxLines) {
    List<ITaxLineItem> results = new ArrayList<ITaxLineItem>(taxLines.size());
    for (ITaxLineItem tl : taxLines) {
      if (!tl.getVoid()) {
        results.add(tl);
      }
    }
    return results;
  }

  /**
   * Round a tax amount.
   * @param argMod the tax modifier containing the tax.
   * @param argAmt the amount of tax.
   * @return the amount of tax.
   */
  public BigDecimal roundTaxAmount(ISaleTaxModifier argMod, BigDecimal argAmt) {
    final boolean isTranLevel = argMod.getSaleTaxGroupRule().getTaxedAtTransLevel();
    final RoundingMode roundingMode =
        NumberUtils.getRoundingModeForName(argMod.getSaleTaxGroupRule().getTaxAuthority().getRoundingCode());
    final int roundingScale = argMod.getSaleTaxGroupRule().getTaxAuthority().getRoundingDigitsQuantity();

    if (isTranLevel) {
      argAmt = argAmt.setScale(6, RoundingMode.HALF_EVEN);
    }
    else {
      argAmt = argAmt.setScale(roundingScale, roundingMode);
    }

    return argAmt;
  }

  /**
   * Round a tax amount according to the rules specified by the given tax authority.
   * 
   * @param argTaxAuth the tax authority providing the rules to round the tax amount by
   * @param argAmt the tax amount to round
   * @return the rounded tax amount
   */
  public BigDecimal roundTaxAmount(ITaxAuthority argTaxAuth, BigDecimal argAmt) {
    final RoundingMode roundingMode = NumberUtils.getRoundingModeForName(argTaxAuth.getRoundingCode());
    final int roundingScale = argTaxAuth.getRoundingDigitsQuantity();

    argAmt = argAmt.setScale(roundingScale, roundingMode);
    return argAmt;
  }

  /**
   * This method does nothing for Xstore native taxing and is intended to be overridden.
   * @param argTaxLine the tax line to update
   */
  public void setGroupRuleForTaxLine(ITaxLineItem argTaxLine) {
    // NOTHING - OVERRIDE THIS METHOD FOR ALTERNATE TAX SYSTEMS!
  }

  /**
   * This method does nothing for Xstore native taxing and is intended to be overridden.
   * @param argLineItem the sale line to update
   */
  public void updateGroupRulesForSaleLine(ISaleReturnLineItem argLineItem) {
    // NOTHING - OVERRIDE THIS METHOD FOR ALTERNATE TAX SYSTEMS!
  }

  /**
   * Calculates the tax override percentage to use when copying a tax modifier.
   * 
   * @param argOrigSaleItem the original sale line item
   * @param argOrigTaxModifier the original tax modifier
   * @return the tax override percentage to use when copying a tax modifier
   */
  protected BigDecimal calculateTaxOverridePercentage(ISaleReturnLineItem argOrigSaleItem,
      ISaleTaxModifier argOrigTaxModifier) {

    return argOrigTaxModifier.getRawTaxPercentage();
  }

  /**
   * Get a new tax group for a group and party.
   * @param argTaxGroupId the tax group ID to look up.
   * @param party the party to look up.
   * @return the new tax group ID.
   */
  protected String getNewTaxTaxGroup(String argTaxGroupId, IParty party) {
    Map<String, Object> params = new HashMap<String, Object>();

    params.put("argTaxGroupId", argTaxGroupId);
    params.put("argRtlLocId", getRetailLocationId());

    try {
      List<dtv.xst.dao.crm.ICustomerAffiliation> custAffiliationList =
          new ArrayList<dtv.xst.dao.crm.ICustomerAffiliation>();

      if (party != null) {
        custAffiliationList = party.getCustomerGroups();
      }
      List<ITaxTaxGroupMapping> result = DataFactory.getObjectByQuery(NEW_TAX_TAX_GROUP, params);

      List<String> custGroupsList = new ArrayList<String>();
      for (ICustomerAffiliation cust : custAffiliationList) {
        custGroupsList.add(cust.getCustomerGroupId().toUpperCase());
      }

      for (ITaxTaxGroupMapping grpMap : result) {
        String custGroup = grpMap.getCustomerGroup().toUpperCase();
        if (custGroupsList.contains(custGroup) || equivalentIgnoreCase(custGroup, ALL_CUSTOMER_GROUPS)) {
          return grpMap.getNewTaxGroupId(); /* assuming that the list "result" is already sorted by priority
                                             * in the search query */
        }
      }
    }
    catch (ObjectNotFoundException e) {
      // e.printStackTrace();
      if (logger_.isDebugEnabled()) {
        logger_.debug("No tax group found for the Customer");
      }
    }
    return null;
  }

  /**
   * Returns the "authority type code" reported by the specified tax group rule.
   * 
   * @param argTaxGroupRule the tax group rule to interrogate
   * @return the "authority type code" reported by <code>argTaxGroupRule</code>
   */
  protected String getTaxLineAuthorityType(ITaxGroupRule argTaxGroupRule) {
    return argTaxGroupRule.getTaxTypeCode();
  }
}
