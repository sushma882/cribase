// $Id: CountSummaryItem.java 417 2012-09-25 20:56:35Z suz.sxie $
package dtv.pos.till.count;

import static dtv.pos.common.ConfigurationMgr.getUseTillAccountability;
import static dtv.util.NumberUtils.*;

import java.math.BigDecimal;
import java.util.*;

import cri.pos.till.CriTillHelper;
import org.apache.log4j.Logger;

import dtv.event.*;
import dtv.event.constraint.NameConstraint;
import dtv.event.handler.CascadingHandler;
import dtv.i18n.FormattableFactory;
import dtv.i18n.OutputContextType;
import dtv.mvc.IModel;
import dtv.pos.common.CommonHelper;
import dtv.pos.tender.TenderHelper;
import dtv.pos.till.ITillHelper;
import dtv.pos.till.TillHelper;
import dtv.pos.till.types.TenderCountTypeCode;
import dtv.xst.dao.tnd.*;
import dtv.xst.dao.trl.IRetailTransactionLineItem;
import dtv.xst.dao.tsn.*;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * Each count summary item obejct represents a tender summary on the till count summary view. This object
 * stores till count information in the summary level as well as handling creation of count objects depends on
 * the count method of this count summary item object.<br>
 * <br>
 * Copyright (c) 2003 Datavantage Corporation
 *
 * @author jhsiao
 * @created September 11, 2003
 * @version $Revision: 417 $
 */
public class CountSummaryItem
  implements IModel {

  private static final Logger logger_ = Logger.getLogger(CountSummaryItem.class);;
  private static final FormattableFactory FF = FormattableFactory.getInstance();
  private static final ITillHelper HELPER = TillHelper.getInstance();
  private static final IEventConstraint UPDATE_TOTALS_CONSTRAINT = new NameConstraint(
    ICountable.UPDATE_TOTALS);

  private final TenderCountTypeCode forceCountMethod_;
  private final List<ICountable> countObject_ = new ArrayList<ICountable>();
  private final Object countSummaryObject_; // tender or tenderType
  private final ISession session_;
  private final List<String> tenderIdList_ = new ArrayList<String>();
  private final List<ITender> tenders_ = new ArrayList<ITender>();
  private final ITenderControlTransaction transaction_;
  private final boolean autoPopulateDeclareAmt_;
  private final boolean isLocalCurrency_;
  private final boolean isTenderType_;
  private final EventHandler handler_ = new CascadingHandler(this) {
    /** {@inheritDoc} */
    @Override
    protected void handle(Event argEvent) {
      updateDeclaredValues();
    }
  };

  private String name_;
  private String desc_;
  private BigDecimal totalDeclaredAmt_ = ZERO;
  private BigDecimal totalEndCountAmt_ = ZERO;
  private BigDecimal totalSystemAmt_ = ZERO;
  private int totalDeclaredCount_ = 0;
  private int totalEndCount_ = 0;
  private int totalSystemCount_ = 0;
  private boolean _representsAudit = false;

  /**
   * The constuctor of CountSummaryItem class. Initiate necessary information at the till count tender summary
   * level.
   *
   * @param model the till count model
   * @param session the current session to perform till count on
   * @param tenderObject either the tender or tender type data object
   * @param isTenderType whether or not this is a tender level or tender type level till count for this count
   * summary item
   * @param forceCountMethod can speficy the force count method to override the configured method type
   * @param transaction for reconcile count, the end count tender control transaction will be pass in for
   * reference
   * @param localCurrency whether or not the tender or tender type this count summary item contains is local
   * currency
   * @param argAutoPopulateDeclareAmt boolean
   */
  public CountSummaryItem(TillCountModel model, ISession session, Object tenderObject, boolean isTenderType,
                          TenderCountTypeCode forceCountMethod, ITenderControlTransaction transaction, boolean localCurrency,
                          boolean argAutoPopulateDeclareAmt) {

    session_ = session;
    transaction_ = transaction;
    forceCountMethod_ = forceCountMethod;
    isLocalCurrency_ = localCurrency;

    isTenderType_ = isTenderType;
    countSummaryObject_ = tenderObject;
    autoPopulateDeclareAmt_ = argAutoPopulateDeclareAmt;

    if (isTenderType_) {
      ITenderType tenderType = (ITenderType) tenderObject;
      name_ = tenderType.getTenderTypecode();
      desc_ = FF.getSimpleFormattable(tenderType.getDescription()).toString(OutputContextType.VIEW);
    }
    else {
      // tenders_.add(tenderObject);
      tenderIdList_.add(((ITender) tenderObject).getTenderId());
      ITender tender = (ITender) tenderObject;
      name_ = tender.getTenderId();
      desc_ = FF.getSimpleFormattable(tender.getDescription()).toString(OutputContextType.VIEW);
    }
  }

  /**
   * Add a tender to current tender summary count
   * @param tender the tender data object
   */
  public void addTender(ITender tender) {
    tenders_.add(tender);
    tenderIdList_.add(tender.getTenderId());
  }

  /**
   * Creates a new unit count object meant to be populated by the user.
   * @return UnitCount the new unit count model
   */
  public UnitCount createUnitCount() {
    return createUnitCount(ZERO, 0, null);
  }

  /**
   * Create the unit count object and initialize the values in that object.
   *
   * @param amount the unit count amount
   * @param count the unit count count
   * @param serialNumber the serial number of the unit count
   * @return UnitCount the new unit count model
   */
  public UnitCount createUnitCount(BigDecimal amount, int count, String serialNumber) {
    UnitCount unitCount = new UnitCount(this, countSummaryObject_);
    countObject_.add(unitCount);
    EventManager.registerEventHandler(handler_, unitCount, UPDATE_TOTALS_CONSTRAINT);
    unitCount.setTotalCount(count);
    unitCount.setTotalValue(amount);
    unitCount.setSerialNumber(serialNumber);

    return unitCount;
  }

  /**
   * Return the actually local currency total declared amount.
   * @return the actually local currency total declared amount
   */
  public BigDecimal getActualAmount() {
    BigDecimal exchangeRate = getExchangeRate();
    if (isZero(exchangeRate)) {
      return ZERO;
    }
    BigDecimal amt =
      totalDeclaredAmt_.divide(exchangeRate, CommonHelper.getInstance().getCurrencyScale(), CommonHelper
        .getInstance().getRoundingMethod());
    amt = CommonHelper.getInstance().roundCurrency(amt);
    return amt;
  }

  public BigDecimal getCloseCountThresholdAmt() {
    if (isTenderType()) {
      return nonNull(((ITenderType) countSummaryObject_).getCloseCountDiscrepancyThreshold());
    }
    else {
      return nonNull(((ITender) countSummaryObject_).getCloseCountDiscrepancyThreshold());
    }
  }

  /**
   * Return the tender count method type.
   * @return the tender count method type
   */
  public TenderCountTypeCode getCountMethod() {
    if (countSummaryObject_ == null) {
      return null;
    }

    if (forceCountMethod_ != null) {
      return forceCountMethod_;
    }

    if (isTenderType_) {
      ITenderType tenderType = (ITenderType) countSummaryObject_;
      return TenderCountTypeCode.valueOf(tenderType.getUnitCountCode());
    }
    ITender tender = (ITender) countSummaryObject_;
    return TenderCountTypeCode.valueOf(tender.getUnitCountCode());
  }

  /**
   * Return a list of all count objects for the current count summary item.
   * @return a list of all count objects for the current count summary item
   */
  public List<ICountable> getCountObjectList() {
    return countObject_;
  }

  /**
   * Return the tender or tender type data object for current count summary item.
   * @return the tender or tender type data object for current count summary item
   */
  public Object getCountSummaryObject() {
    return countSummaryObject_;
  }

  /**
   * Return the description for current count summary item.
   * @return the description for current count summary item
   */
  public String getDescription() {
    return desc_;
  }

  /**
   * Return the difference amount between the declared and system total amount.
   * @return the difference amount between the declared and system total amount
   */
  public BigDecimal getDifferenceAmount() {
    BigDecimal difference = (nonNull(getTotalDeclaredAmount()).subtract(nonNull(getTotalSystemAmount())));
    difference = CommonHelper.getInstance().roundCurrency(difference);

    return difference;
  }

  /**
   * Return the difference in count between the declared and system counts.
   * @return the difference in count between the declared and system counts
   */
  public int getDifferenceCount() {
    if (transaction_ == null) {
      return (getTotalDeclaredCount() - getTotalSystemCount());
    }
    // Reconcile count. The difference is the difference between the end count and the reconcile count
    return (getTotalDeclaredCount() - getTotalEndCountCount());
  }

  /**
   * Return the end count tender control transaction data object
   * @return the end count tender control transaction data object
   */
  public ITenderControlTransaction getEndCountTransaction() {
    return transaction_;
  }

  /**
   * Return the exchange rate. If the current tender or tender type is local currency, will return 1.
   * @return the exchange rate
   */
  public BigDecimal getExchangeRate() {
    // Under the assumption that every type of currency has to be counted in
    // separate screen. No mix currency types in one screen. Therefore, if it's
    // foreign currency, this must be tender level count.
    if (isLocalCurrency_) {
      return ONE;
    }
    if (countSummaryObject_ instanceof ITender) {
      ITender tender = (ITender) countSummaryObject_;
      return TenderHelper.getInstance().getExchangeRate(tender.getCurrencyId());
    }
    logger_.error("Should not configure to mix different currencies in one count screen.");
    return ZERO;
  }

  /**
   * Return the tender id or tender type code of the current count summary item
   * @return the tender id or tender type code of the current count summary item
   */
  public String getName() {
    return name_;
  }

  /**
   * Return the till count sort order in the till count summary level
   * @return the till count sort order in the till count summary level
   */
  public int getSortOrder() {
    int order = 9999;

    if (isTenderType_ && !tenders_.isEmpty()) {
      // Loop through all tenders and find the smallest number.
      for (ITender tender : tenders_) {
        if (order > tender.getDisplayOrder()) {
          order = tender.getDisplayOrder();
        }
      }
    }
    else if (!isTenderType_) {
      order = ((ITender) countSummaryObject_).getDisplayOrder();
    }
    return order;
  }

  /**
   * Return a list of tender data objects this tender summary count contains.
   * @return a list of tender data objects this tender summary count contains.
   */
  public List<ITender> getTenders() {
    return tenders_;
  }

  /**
   * Return the total user declared amount for this count summary
   * @return the total user declared amount for this count summary
   */
  public BigDecimal getTotalDeclaredAmount() {
    return CommonHelper.getInstance().roundCurrency(totalDeclaredAmt_);

  }

  /**
   * Return the total user declared counts for this count summary
   * @return the total user declared counts for this count summary
   */
  public int getTotalDeclaredCount() {
    return totalDeclaredCount_;
  }

  /**
   * Return the total end count amount for the current count summary item
   * @return the total end count amount for the current count summary item
   */
  public BigDecimal getTotalEndCountAmount() {
    return CommonHelper.getInstance().roundCurrency(totalEndCountAmt_);
  }

  /**
   * Return the total end count counts for the current count summary item
   * @return the total end count counts for the current count summary item
   */
  public int getTotalEndCountCount() {
    return totalEndCount_;
  }

  /**
   * Return the total user system amount for this count summary
   * @return the total user system amount for this count summary
   */
  public BigDecimal getTotalSystemAmount() {
    return CommonHelper.getInstance().roundCurrency(totalSystemAmt_);
  }

  /**
   * Return the total user system count for this count summary
   * @return the total user system count for this count summary
   */
  public int getTotalSystemCount() {
    return totalSystemCount_;
  }

  /**
   * This method handles creating the count object for current tender summary till count. Base on the count
   * method, create different count object (unit, denomination, and total count objects).
   */
  public void init() {
    if (autoPopulateDeclareAmt_) {
      updateSystemValues();
      TotalsCount totalCount = new TotalsCount(this, countSummaryObject_);
      countObject_.add(totalCount);
      EventManager.registerEventHandler(handler_, totalCount, UPDATE_TOTALS_CONSTRAINT);
      totalCount.setTotalCount(getTotalSystemCount());
      totalCount.setTotalValue(getTotalSystemAmount());
      return;
    }

    if ((getCountMethod() == TenderCountTypeCode.UNIT_NORMAL)
      || (getCountMethod() == TenderCountTypeCode.UNIT_SHORT)) {
      if (transaction_ == null) {
        if (!isSystemTotalAutoGenerated()) {
          // Unit count. Create only one unit count object since don't know how
          // many will have.
          createUnitCount();
        }
        else {
          // System auto generate all unit count from all tender line item
          // objects for the current session.
          createSystemGeneratedUnitCountObjects();
        }
      }
      else {
        // Reconcile count.
        createEndCountUnitCountObjects();
      }
    }
    else if ((getCountMethod() == TenderCountTypeCode.TOTAL_NORMAL)
      || (getCountMethod() == TenderCountTypeCode.TOTAL_SHORT)) {
      // Total count. Create only one total count object.
      TotalsCount totalCount = new TotalsCount(this, countSummaryObject_);
      countObject_.add(totalCount);
      EventManager.registerEventHandler(handler_, totalCount, UPDATE_TOTALS_CONSTRAINT);
      totalCount.setTotalCount(0);
      totalCount.setTotalValue(ZERO);

      if (transaction_ != null) {
        // Reconcile count
        updateEndCountTotalCount(totalCount);
      }
      else if (isSystemTotalAutoGenerated()) {
        // System auto populate the total count count and amount.
        updateSystemGeneratedTotalCount(totalCount);
      }
    }
    else if (getCountMethod() == TenderCountTypeCode.DENOMINATION) {
      // Denomination count. Depends on current tender object type to determine
      // what kind of denomination we are dealing here. If its tenderType,
      // tenders are the denomination. If its tender, the tender denominations
      // are the denomination.
      if (isTenderType_) {
        if ((tenders_ != null) && !tenders_.isEmpty()) {
          for (ITender tender : tenders_) {
            DenominationCount denoCount = new DenominationCount(this, tender);
            countObject_.add(denoCount);
            EventManager.registerEventHandler(handler_, denoCount, UPDATE_TOTALS_CONSTRAINT);
            denoCount.setTotalCount(0);
            denoCount.setTotalValue(ZERO);

            if (transaction_ != null) {
              // update the denomination count amount from tenderCount object
              ITenderCount tenderCount = HELPER.getTenderCountFromTenderControlTrans(transaction_, tender);
              if (tenderCount != null) {
                denoCount.setTotalEndCount(tenderCount.getMediaCount());
                denoCount.setTotalEndCountValue(tenderCount.getAmount());
              }
            }
            else if (isSystemTotalAutoGenerated()) {
              // System auto populate the denominational count counts and amount.
              updateSystemGeneratedDenoCount(denoCount, tender);
            }
          }
        }
      }
      else {
        ITender tender = (ITender) countSummaryObject_;
        List<ITenderDenomination> denominations = tender.getTenderDenominations();

        if ((denominations != null) && !denominations.isEmpty()) {
          Collections.sort(denominations, new TenderSorter());

          for (ITenderDenomination tenderDenomination : denominations) {
            DenominationCount denoCount = new DenominationCount(this, tenderDenomination);
            countObject_.add(denoCount);
            EventManager.registerEventHandler(handler_, denoCount, UPDATE_TOTALS_CONSTRAINT);

            if (transaction_ != null) {
              // Update the denomination count amount from the
              // tenderDenominationCount object.
              ITenderDenominationCount tenderDenominationCount =
                HELPER.getTenderDenominationCountFromTenderControlTrans(transaction_, tenderDenomination);

              if (tenderDenominationCount != null) {
                denoCount.setTotalEndCount(tenderDenominationCount.getMediaCount());
                denoCount.setTotalEndCountValue(tenderDenominationCount.getAmount());
              }
            }
            denoCount.setTotalCount(denoCount.getTotalEndCount());
            denoCount.setTotalValue(denoCount.getTotalEndCountValue());
          }
        }
      }
    }

    if (transaction_ != null) {
      // This is reconcile count. Need to update the end count amount and count.
      updateEndCountValues();
    }
    updateSystemValues();
  }

  /**
   * Returns a flag indicating if this count summary item represents an audit count.
   * @return a flag indicating if this count summary item represents an audit count
   */
  public boolean isAudit() {
    return _representsAudit;
  }

  /**
   * Return whether or not the current tender or tender type is local currency.
   * @return whether or not the current tender or tender type is local currency.
   */
  public boolean isLocalCurrency() {
    return isLocalCurrency_;
  }

  /**
   * Return whether or not the system is configured to auto generate the system total for this count summary
   * item.
   * @return whether or not the system is configured to auto generate the system total for this count summary
   * item.
   */
  public boolean isSystemTotalAutoGenerated() {
    boolean flag = false;

    // Take the SystemAutoGenerateFlag from the first tender
    List<ITender> tenders = getTenders();
    if ((tenders != null) && !tenders.isEmpty()) {
      ITender tender = tenders.get(0);
      flag = tender.getPopulateSystemCount();
    }

    return flag;
  }

  /**
   * Return whether or not this is a tender type level count
   * @return whether or not this is a tender type level count
   */
  public boolean isTenderType() {
    return isTenderType_;
  }

  /**
   * Return whether or not this given tender is counted as part of current count summary.
   * @param tenderId the tender ID
   * @return whether or not this given tender is counted as part of current count summary.
   */
  public boolean isThisTenderContainedInThisCountSummaryItem(String tenderId) {
    if ((tenders_ != null) && !tenders_.isEmpty()) {
      for (ITender tenderObject : tenders_) {
        if (tenderObject.getTenderId().equalsIgnoreCase(tenderId)) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Specifies whether or not this count summary item represents an audit count.
   * @param argRepresentsAudit whether or not this count summary item represents an audit count
   */
  public void setAudit(boolean argRepresentsAudit) {
    _representsAudit = argRepresentsAudit;
    updateSystemValues();
  }

  /**
   * Return the description for current count summary item.
   * @return the description for current count summary item.
   */
  @Override
  public String toString() {
    return desc_;
  }

  /**
   * This method updates the user declared amount and counts for this count summary
   */
  public void updateDeclaredValues() {
    totalDeclaredAmt_ = ZERO;
    totalDeclaredCount_ = 0;

    if ((countObject_ != null) && !countObject_.isEmpty()) {
      for (ICountable count : countObject_) {
        totalDeclaredAmt_ = totalDeclaredAmt_.add(nonNull(count.getTotalValue()));
        totalDeclaredAmt_ = CommonHelper.getInstance().roundCurrency(totalDeclaredAmt_);
        totalDeclaredCount_ = totalDeclaredCount_ + count.getTotalCount();
      }
    }
  }

  /**
   * This method re-calculate and update the end count total amount and counts for the current count summary
   * item.
   */
  public void updateEndCountValues() {
    totalEndCountAmt_ = ZERO;
    totalEndCount_ = 0;

    if ((countObject_ != null) && !countObject_.isEmpty()) {
      for (ICountable count : countObject_) {
        totalEndCountAmt_ = totalEndCountAmt_.add(nonNull(count.getTotalEndCountValue()));
        totalEndCountAmt_ = CommonHelper.getInstance().roundCurrency(totalEndCountAmt_);
        totalEndCount_ = totalEndCount_ + count.getTotalEndCount();
      }
    }
  }

  /**
   * This method update the system total amount and total count for the current count summary item.
   */
  public void updateSystemValues() {
    updateSystemValues(false, null);
  }

  public void updateSystemValues(boolean isStoreSafeAudit, List<ISession> argOpenTillSessions) {
    totalSystemAmt_ = ZERO;
    totalSystemCount_ = 0;

    BigDecimal totalCashRemainInRegisters = HELPER.getTotalRegisterCashRemaining();

    if (transaction_ == null) {
      for (ISessionTender sessionTender : session_.getSessionTenders()) {
        if (isThisTenderContainedInThisCountSummaryItem(sessionTender.getTenderId())) {
          totalSystemAmt_ = totalSystemAmt_.add(nonNull(sessionTender.getMediaAmount()));

          if (!getUseTillAccountability()
            && !isAudit()
            && HELPER.isStoreBankSession(session_)
            && TenderHelper.getInstance().getLocalCurrencyTenderId()
            .equalsIgnoreCase(sessionTender.getTenderId())) {

            totalSystemAmt_ =  totalSystemAmt_.subtract(totalCashRemainInRegisters);
          }

          BigDecimal registerTillAmount = BigDecimal.ZERO;
          if (isStoreSafeAudit && argOpenTillSessions != null) {
            for (ISession tillSession:argOpenTillSessions) {
              for (ISessionTender tillSessionTender : tillSession.getSessionTenders()) {
                  if (tillSessionTender.getTenderId().equals(sessionTender.getTenderId())) {
                    registerTillAmount = registerTillAmount.add(nonNull(tillSessionTender.getMediaAmount()));
                  }
              }
            }
          }
          registerTillAmount = CommonHelper.getInstance().roundCurrency(registerTillAmount);

          if (isStoreSafeAudit) {
            totalSystemAmt_ = totalSystemAmt_.add(registerTillAmount)
              .subtract(CriTillHelper.getInstance().getTotalRegisterFloatAmt());
          }
          totalSystemAmt_ = CommonHelper.getInstance().roundCurrency(totalSystemAmt_);
          totalSystemCount_ = totalSystemCount_ + sessionTender.getMediaCount();
        }
      }
    }
    else {
      /* The only time that you will get here is when the "transaction_" variable is not null. The only time
       * the "transaction_" variable is not null is when reconciling a till when using till accountability. A
       * till reconcile count uses the values from the till end count as its system values. The values from
       * the till end count have already been calculated by the time you reach this point. Therefore, just use
       * the already-calculatd end count values for the system values in this case instead of recalculating
       * them again, in a way that may be incorrect. */
      totalSystemAmt_ = totalEndCountAmt_;
      totalSystemCount_ = totalEndCount_;
    }
  }

  public boolean verifyCurrencyLocaleOption(CurrencyLocaleOptions argCurrencyLocaleOptions) {
    if (argCurrencyLocaleOptions == CurrencyLocaleOptions.LOCAL_ONLY) {
      return isLocalCurrency();
    }

    if (argCurrencyLocaleOptions == CurrencyLocaleOptions.FOREIGN_ONLY) {
      return !isLocalCurrency();
    }

    return true;
  }

  /**
   * Upon reconcile count, need to pre-populate end count objects and values.
   */
  private void createEndCountUnitCountObjects() {
    // Need to retrieve the end count and create all unit count objects.
    ITenderTypeCount tenderTypeCount = null;
    if (isTenderType_) {
      tenderTypeCount =
        HELPER.getTenderTypeCountFromTenderControlTrans(transaction_, (ITenderType) countSummaryObject_);
    }
    else {
      ITenderCount tenderCount =
        HELPER.getTenderCountFromTenderControlTrans(transaction_, (ITender) countSummaryObject_);
      if (tenderCount != null) {
        tenderTypeCount = tenderCount.getParentTenderTypeCount();
      }

      if (tenderTypeCount != null) {
        List<ITenderSerializedCount> tenderSerializedCountList = tenderTypeCount.getTenderSerializedCounts();
        createUnitCountFromSerializedCount(tenderSerializedCountList);
      }
    }
  }

  /**
   * If the system configured to auto generate system total, this method will create the unit count objects
   * for current count summary item.
   */
  private void createSystemGeneratedUnitCountObjects() {
    // Get all tender line item contains the tenders
    int count = 0;

    if ((tenderIdList_ != null) && !tenderIdList_.isEmpty()) {
      for (ITender tender : tenders_) {
        List<IRetailTransactionLineItem> tenderLineItems =
          HELPER.getTenderLineItemsForSession(session_, tender.getTenderId());

        if ((tenderLineItems != null) && !tenderLineItems.isEmpty()) {
          for (IRetailTransactionLineItem retailLine : tenderLineItems) {
            ITenderLineItem tndrLineItem = (ITenderLineItem) retailLine;
            createUnitCount(tndrLineItem.getAmount(), 1, tndrLineItem.getSerialNumber());
            count++ ;
          }
        }
      }
    }

    if (count == 0) {
      createUnitCount();
    }
  }

  /**
   * Create the unit count objects based on end count serialized count data objects.
   * @param tenderSerializedCountList a list of serialized count data objects
   */
  private void createUnitCountFromSerializedCount(List<ITenderSerializedCount> tenderSerializedCountList) {
    if ((tenderSerializedCountList != null) && !tenderSerializedCountList.isEmpty()) {
      for (ITenderSerializedCount tenderSerializedCount : tenderSerializedCountList) {
        UnitCount count =
          createUnitCount(tenderSerializedCount.getAmount(), 1, tenderSerializedCount.getSerialNumber());

        count.setTotalEndCountValue(tenderSerializedCount.getAmount());
        count.setTotalEndCount(1);
      }
    }
  }

  /**
   * This method update the total count amount and count based on end count.
   * @param totalCount the total count object
   */
  private void updateEndCountTotalCount(TotalsCount argTotalCount) {
    // Need to update the total count and total amount
    if (isTenderType()) {
      ITenderType tenderType = (ITenderType) countSummaryObject_;
      ITenderTypeCount tenderTypeCount =
        TillHelper.getInstance().getTenderTypeCountFromTenderControlTrans(transaction_, tenderType);

      if (tenderTypeCount != null) {
        argTotalCount.setTotalEndCountValue(tenderTypeCount.getAmount());
        argTotalCount.setTotalEndCount(tenderTypeCount.getMediaCount());
      }
    }
    else {
      // The total is simply the tenderCount amount
      ITenderCount tenderCount =
        HELPER.getTenderCountFromTenderControlTrans(transaction_, (ITender) countSummaryObject_);
      if (tenderCount != null) {
        argTotalCount.setTotalEndCount(tenderCount.getMediaCount());
        argTotalCount.setTotalEndCountValue(tenderCount.getAmount());
      }
    }

    argTotalCount.setTotalValue(argTotalCount.getTotalEndCountValue());
    argTotalCount.setTotalCount(argTotalCount.getTotalEndCount());
  }

  private void updateSystemGeneratedDenoCount(DenominationCount denoCount, ITender tender) {
    List<IRetailTransactionLineItem> tenderLineItems =
      HELPER.getTenderLineItemsForSession(session_, tender.getTenderId());

    BigDecimal totalAmount = ZERO;
    int count = 0;

    if ((tenderLineItems != null) && !tenderLineItems.isEmpty()) {
      for (IRetailTransactionLineItem retailLine : tenderLineItems) {
        ITenderLineItem tndrLineItem = (ITenderLineItem) retailLine;
        if (tender.getTenderId().equalsIgnoreCase(tndrLineItem.getTender().getTenderId())) {
          totalAmount = totalAmount.add(tndrLineItem.getAmount());
          count++ ;
        }
      }
    }
    denoCount.setTotalValue(totalAmount);
    denoCount.setTotalCount(count);
  }

  private void updateSystemGeneratedTotalCount(TotalsCount totalCount) {
    BigDecimal totalAmount = ZERO;
    int count = 0;

    if ((tenderIdList_ != null) && !tenderIdList_.isEmpty()) {
      for (ITender tender : tenders_) {
        List<IRetailTransactionLineItem> tenderLineItems =
          HELPER.getTenderLineItemsForSession(session_, tender.getTenderId());

        if ((tenderLineItems != null) && !tenderLineItems.isEmpty()) {
          for (IRetailTransactionLineItem retailLine : tenderLineItems) {
            ITenderLineItem tndrLineItem = (ITenderLineItem) retailLine;
            totalAmount = totalAmount.add(tndrLineItem.getAmount());
            count++ ;
          }
        }
      }
    }

    totalCount.setTotalValue(totalAmount);
    totalCount.setTotalCount(count);
  }

  public enum CurrencyLocaleOptions {
    ALL, FOREIGN_ONLY, LOCAL_ONLY
  }

  class TenderSorter
    implements Comparator<ITenderDenomination> {
    @Override
    public int compare(ITenderDenomination object, ITenderDenomination object1) {
      return object.getSortOrder() - object1.getSortOrder();
    }
  }

}
