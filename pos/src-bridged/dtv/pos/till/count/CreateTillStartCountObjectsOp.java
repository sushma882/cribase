//$Id: CreateTillStartCountObjectsOp.java 55414 2010-12-07 23:00:05Z dtvdomain\jweiss $
package dtv.pos.till.count;

import java.math.BigDecimal;

import dtv.pos.common.ConfigurationMgr;
import dtv.util.NumberUtils;
import dtv.xst.dao.tsn.ISessionTender;
import dtv.xst.dao.tsn.ITenderCount;

/**
 * Create tender control transaction and count related data objects based on the count summary
 * objects from current till count model for session end count.<br>
 * Copyright (c) 2003 Datavantage Corporation
 * 
 * @author jhsiao
 * @created September 11, 2003
 * @version $Revision: 55414 $
 */
public class CreateTillStartCountObjectsOp
    extends AbstractProcessIncomingTillCashCountOp {

  private static final long serialVersionUID = 1L;

  @Override
  protected void adjustCashLevels(ISessionTender argStoreBankCash, ISessionTender argTillCash,
      ITenderCount argCashCount) {
    //If there is any difference between original starting cash and the beginning count, the
    //difference needs to be updated against store bank
    BigDecimal differenceAmount =
        NumberUtils.nonNull(argCashCount.getAmount()).subtract(
            NumberUtils.nonNull(argTillCash.getMediaAmount()));

    if (!ConfigurationMgr.getUseTillAccountability()) {
      // In register accountability,  adjust the store bank amount by the counted amount and the 
      //difference b/w the last closing cash amount (system amount) and starting count amount.
      argStoreBankCash.setMediaAmount(NumberUtils.nonNull(argStoreBankCash.getMediaAmount()).subtract(
          argCashCount.getAmount()).add(differenceAmount));
    }
    else {
      // For till accountability, the till has already been issued with a certain amount and the
      // store bank has already been adjusted by that amount.  Therefore, at this point, the store
      // only needs to be adjusted by the difference b/w the issued amount and the starting count amount.
      argStoreBankCash.setMediaAmount(NumberUtils.nonNull(argStoreBankCash.getMediaAmount()).subtract(
          differenceAmount));
    }

    argTillCash.setMediaAmount(argCashCount.getAmount());
    argCashCount.setDifferenceAmount(differenceAmount);
  }
}
