// $Id: PromptVoucherNumberOp.java 783 2014-05-21 03:08:19Z suz.jliu $
package dtv.pos.tender.voucher;

import static dtv.xst.daocommon.ExchangeRateHelper.getLocalizedAmount;

import java.math.BigDecimal;
import java.util.*;

import org.apache.log4j.Logger;

import dtv.hardware.events.IAccountInputEvent;
import dtv.hardware.events.IVoucherEvent;
import dtv.hardware.inputrules.EventDiscriminator;
import dtv.hardware.keyboard.KeyboardInput;
import dtv.hardware.types.HardwareType;
import dtv.hardware.types.InputType;
import dtv.i18n.IFormattable;
import dtv.pos.common.*;
import dtv.pos.framework.op.OpResponse;
import dtv.pos.framework.op.OpResponseHelper;
import dtv.pos.framework.security.SecurityRequest;
import dtv.pos.framework.security.SecurityResponse;
import dtv.pos.iframework.cmd.ITransactionCmd;
import dtv.pos.iframework.event.*;
import dtv.pos.iframework.hardware.IHardwareType;
import dtv.pos.iframework.hardware.IInput;
import dtv.pos.iframework.op.*;
import dtv.pos.iframework.type.AttributeKey;
import dtv.pos.iframework.type.VoucherActivityCodeType;
import dtv.pos.iframework.ui.config.*;
import dtv.pos.iframework.validation.*;
import dtv.pos.register.ISaleItemCmd;
import dtv.pos.register.IScanItemCmd;
import dtv.pos.register.type.LineItemType;
import dtv.pos.storecalendar.StoreCalendar;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.pos.tender.TenderHelper;
import dtv.pos.tender.voucher.config.*;
import dtv.util.*;
import dtv.util.sequence.SequenceFactory;
import dtv.xst.dao.trl.*;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.ttr.IVoucher;
import dtv.xst.dao.ttr.IVoucherModel;

/**
 * This operation handles prompting the user to enter the voucher number and validate it.<br>
 * src-brigded: Not to allow gift card sale to be tendered with another gift card<br>
 * Copyright (c) 2004 Datavantage Corporation
 * 
 * @author jhsiao
 * @created September 8, 2004
 * @version $Revision: 783 $
 */
public class PromptVoucherNumberOp
    extends AbstractValidationOp
    implements IXstEventObserver {

  private static final long serialVersionUID = 1L;
  private static final Logger logger_ = Logger.getLogger(PromptVoucherNumberOp.class);
  private static final IValidationKey[] validationKeys_ = {ValidationKey.valueOf("VOUCHER_NUMBER")};
  private static final Class<?>[] EVENT_TYPES = {IVoucherEvent.class};
  private static final List<IRetailTransactionLineItem> noLines_ = Collections.emptyList();
  private final List<String> IsdGiftCards = Arrays.asList("ISD_GIFT_CARD", "ISD_GIFT_ECARD");

  /** {@inheritDoc} */
  @SuppressWarnings("unchecked")
  @Override
  public Class<?>[] getObservedEventInterfaces() {
    return EVENT_TYPES;
  }

  /** {@inheritDoc} */
  @Override
  public IXstEventType[] getObservedEvents() {
    return new IXstEventType[] {InputType.INPUT_ITEM};
  }

  /** {@inheritDoc} */
  @Override
  public IPromptKey getPromptKey(IXstCommand argCmd) {
    IPromptKey promptKey = super.getPromptKey(argCmd);

    IVoucherCmd cmd = (IVoucherCmd) argCmd;

    if (isSwipeRequired(argCmd)) {
      // manual entry is not allowed
      promptKey = PromptKey.valueOf("VOUCHER_SERIAL_NEEDED_NO_MANUAL_ENTRY");
    }
    else if ("ISD_GIFT_CARD".equalsIgnoreCase(cmd.getVoucherType())
        || "ISD_GIFT_ECARD".equalsIgnoreCase(cmd.getVoucherType())) {
      promptKey = PromptKey.valueOf("ENTER_GIFT_CARD_INFO");
    }

    return (promptKey == null) ? PromptKey.valueOf("VOUCHER_SERIAL_NEEDED") : promptKey;
  }

  /** {@inheritDoc} */
  @Override
  public IValidationData getValidationData(IXstCommand argCmd, IXstEvent argEvent, IValidationKey argKey) {
    IVoucherCmd cmd = (IVoucherCmd) argCmd;

    if ((argEvent != null) && (argEvent.getData() != null)) {
      String serialNumber = argEvent.getStringData().trim();
      cmd.setSerialNumber(serialNumber);
      cmd.setEntryMethodCode(HardwareType.KEYBOARD.getName());
    }

    return new ValidationData(cmd.getSerialNumber());
  }

  /** {@inheritDoc} */
  @Override
  public IValidationKey[] getValidationKeys(IXstCommand argCmd) {
    return validationKeys_;
  }

  /**
   * Handle the execution of the operation. This is the primary vehicle through which an operation
   * does what it was designed to do.
   * 
   * @param argCmd the command to run with.
   * @param argEvent the event to run with.
   * @return the response from executing the operation
   */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    IOpResponse opResponse = null;
    IOpState opState = argCmd.getOpState();
    IVoucherCmd cmd = (IVoucherCmd) argCmd;

    // Init
    if (opState == null) {
      // If the serial number already exist, jump to POST_PROMPT
      opResponse = checkSerialNumberAlreadyExist(cmd, argEvent);

      if (opResponse != null) {
        return opResponse;
      }

      opResponse = checkAutoGenerateIssuedVoucher(cmd);

      if (opResponse != null) {
        return opResponse;
      }
      // either is not an ISSUE, or the voucher serial is never auto-generated
      argCmd.setOpState(POST_PROMPT);
      return getPromptResponse(argCmd, argEvent, getPromptKey(argCmd), getPromptArgs(argCmd, argEvent));
    }
    else if (opState == ERROR_MESSAGE_PROMPT) {
      cmd.setOpState(null);
      cmd.setStoredData(null);
      cmd.setSerialNumber(null);

      // Tom -> The code that checks the existence of the serial number
      // looks at the line item, not the command, so null the serial number
      // out there as well...
      IVoucherLineItem voucherItem = cmd.getVoucherLineItem();

      if (voucherItem != null) {
        voucherItem.setSerialNumber(null);
      }

      return OpResponseHelper.INCOMPLETE_RESPONSE;
    }
    else if (opState == POST_PROMPT) {
      opResponse = runValidationCheck(argCmd, argEvent);

      // Only set the response data when it is valid.
      if (!opResponse.getOpStatus().isComplete()) {
        return opResponse;
      }

      // At this point, a serial number should have been provided (manually by the
      // user or otherwise).
      opResponse = validateVoucherNumber(argCmd, argEvent);

      if (opResponse != null) {
        // At least one validation check did not pass.
        return opResponse;
      }

      return handleVoucher(cmd, argEvent, getVoucherConfig(cmd));
    }

    if (opState == SECURITY_PROCESSING) {
      return handleSecurityProcessing(argCmd, argEvent);
    }

    return opResponse;
  }

  /**
   * Handle the reversing of the operation. This is called by the system when some activity has
   * resulted in the cancelation of the operation chain.
   * @param argCmd - the command to reverse with.
   * @return the response from reversing the operation
   */
  public IOpResponse handleOpReverse(IXstCommand argCmd) {
    ((IVoucherCmd) argCmd).setSerialNumber(null);
    return OpResponseHelper.COMPLETE_RESPONSE;
  }

  /**
   * Handle checking to see if this is a issue voucher and if this voucher's issue number should be
   * automatically generated by the system.
   * 
   * @param argCmd the current command
   * @return a response for this operation
   */
  protected IOpResponse checkAutoGenerateIssuedVoucher(IVoucherCmd argCmd) {
    // First check to see if this is a issue voucher and whether or not the system
    // required to prompt for the serial number.
    if (argCmd.getVoucherActivityCodeType() == VoucherActivityCodeType.ISSUED) {
      VoucherConfig config = getVoucherConfig(argCmd);

      if (config.getAutoGenerateSerial()) {
        String voucherSerial = SequenceFactory.getNextStringValue(getSequenceType(argCmd));
        argCmd.setSerialNumber(voucherSerial);
        argCmd.setEntryMethodCode("AutoGenerated");

        // Set the serial number than create a new voucher
        argCmd.setSerialNumber(voucherSerial);
        return handleNewVoucher(argCmd, config);
      }
    }
    return null;
  }

  protected IOpResponse checkSerialNumberAlreadyExist(IVoucherCmd argCmd, IXstEvent argEvent) {
    // If the serial number already exist, jump to POST_PROMPT
    String serialNumber = argCmd.getSerialNumber();
    IVoucherLineItem voucherLine = argCmd.getVoucherLineItem();

    if ((serialNumber == null) && (voucherLine != null)) {
      serialNumber = voucherLine.getSerialNumber();
    }

    if (serialNumber != null) {
      argCmd.setSerialNumber(serialNumber);
      argCmd.setOpState(POST_PROMPT);
      return OpResponseHelper.INCOMPLETE_RESPONSE;
    }

    return null;
  }

  /**
   * If the voucher exist in the database and the amount for that voucher to be redeemed has been
   * determined, call this method to set the unspent balance of that voucher and set the voucher to
   * the voucher tender line item.
   * 
   * @param argCmd current command
   * @param argVoucher the current voucher
   * @param argUnspentAmt the new unspent amount after this tender
   */
  protected void completeTenderVoucher(IVoucherCmd argCmd, IVoucher argVoucher, BigDecimal argUnspentAmt) {
    if (argUnspentAmt != null) {
      argVoucher.setUnspentBalanceAmount(argUnspentAmt);
    }

    argCmd.setVoucher(argVoucher);
    IVoucherLineItem line = argCmd.getVoucherLineItem();

    if (line != null) {
      line.setVoucher(argVoucher);
    }
  }

  /**
   * Return the prompt key to use for when this voucher is already issued.
   * 
   * @param argCmd current command
   * @return the prompt key to use for when this voucher is already issued.
   */
  protected IPromptKey getAlreadyIssuedPromptKey(IVoucherCmd argCmd) {
    return PromptKey.valueOf("VOUCHER_ALREADY_ISSUED");
  }

  /**
   * Return the prompt key to use for the situation when this voucher is already redeemed.
   * 
   * @param argCmd current command
   * @return the prompt key to use for the situation when this voucher is already redeemed.
   */
  protected IPromptKey getAlreadyRedeemedPromptKey(IVoucherCmd argCmd) {
    getAttributeModel(argCmd).setAttribute(AttributeKey.TENDER_INPUT_EVENT, null);
    return PromptKey.valueOf("VOUCHER_ALREADY_REDEEMED");
  }

  protected IPromptKey getIssueAndRedeemSameTransactionPromptKey() {
    return PromptKey.valueOf("VOUCHER_ISSUEREDEEM_SAMETRANSACTION");
  }

  /**
   * Return the prompt key to use for when a gift card sale is tendered with another gift card.
   * 
   * @param argCmd current command
   * @return the prompt key to use for when a gift card sale is tendered with another gift card.
   */
  protected IPromptKey getGiftCardTenderNotAllowedPromptKey() {
    return PromptKey.valueOf("GIFTCARD_PURCHASE_TENDERED_WITH_GIFTCARD");
  }

  /** {@inheritDoc} */
  @Override
  protected final IFormattable[] getPromptArgs(IXstCommand argCmd, IXstEvent argEvent) {
    IVoucherCmd cmd = (IVoucherCmd) argCmd;
    IFormattable[] args = new IFormattable[2];
    args[0] = cmd.getVoucherTypeFormattable();
    args[1] = ff_.getLiteral(cmd.getSerialNumber());
    return args;
  }

  /**
   * Provides the sequence type to use to generate the sequence for a decendant voucher class. The
   * descendant may use the VOUCHER type if a unique sequence is not required. This would be the
   * case where a customer wants a new voucher but does not require the sequence to be unique across
   * the different types. I would guess this is not a common request.
   * 
   * @param argCmd current command
   * @return sequence type
   */
  protected String getSequenceType(IVoucherCmd argCmd) {
    return argCmd.getVoucherType();
  }

  protected IPromptKey getVoidPromptKey() {
    return PromptKey.valueOf("DEFAULT_VOID_VOUCHER");
  }

  protected VoucherConfig getVoucherConfig(IVoucherCmd argCmd) {
    String voucherType = argCmd.getVoucherType();
    TenderHelper TH = TenderHelper.getInstance();

    return TH.getVoucherRootConfig().getVoucherConfig(voucherType);
  }

  /**
   * Handle the cash out voucher activity for a voucher exists in the database
   * 
   * @param argCmd the current command
   * @param argEvent the event that occurred
   * @param argVoucher the voucher needs to be cashed out
   * @return the complete operation state
   */
  protected IOpResponse handleCashOutVoucher(IVoucherCmd argCmd, IXstEvent argEvent, IVoucher argVoucher) {
    // Cash out should cash out all amount of the voucher
    BigDecimal unspentBalanceAmount =
        getLocalizedAmount(argVoucher.getUnspentBalanceAmount(), argVoucher.getCurrencyId());

    if (unspentBalanceAmount != null) {
      if (NumberUtils.isNonPositive(unspentBalanceAmount)) {
        // No amount left to be cash out/ return
        return handleReRedeemVoucher(argCmd, argEvent);
      }

      ((ISaleItemCmd) argCmd).setNewPrice(unspentBalanceAmount);
    }

    completeTenderVoucher(argCmd, argVoucher, NumberUtils.ZERO);
    return OpResponseHelper.COMPLETE_RESPONSE;
  }

  /**
   * Create a new Voucher since the voucher dosen't exist in the database. Update the unspect amount
   * according to the whether its a issue tender and also the type of the tender (mutiple
   * redemption).
   * 
   * @param argCmd the current command
   * @param argConfig the voucher config
   * @return the response from executing the operation
   */
  protected IOpResponse handleNewVoucher(IVoucherCmd argCmd, VoucherConfig argConfig) {
    BigDecimal unSpentAmt = null;
    VoucherActivityCodeType activityType = argCmd.getVoucherActivityCodeType();
    ActivityConfig activityConfig = argConfig.getActivityConfig(activityType);
    IVoucher voucher = null;

    if ((activityType == VoucherActivityCodeType.REDEEMED)
        || (activityType == VoucherActivityCodeType.CASHOUT)) {
      if (argCmd.getOpState() != SECURITY_PROCESSING) {
        if (argConfig.getActivityConfig(activityType).getRequireOverrideIfNotFound()) {
          // send a security request
          argCmd.setOpState(SECURITY_PROCESSING);

          // build a security request
          String[] privs = new String[] {PrivilegeType.valueOf("NOT_FOUND_VOUCHER").toString()};
          IFormattable formattable = ff_.getTranslatable("_promptmsg363");

          SecurityRequest request = SecurityRequest.getAuthorizeRequest(privs);
          request.setMessage(formattable);

          return new OpResponse(OpStatus.INCOMPLETE_HALT, request);
        }
      }
    }

    if (argCmd.getVoucher() == null) {
      // Create a new voucher record in the local datastore.
      voucher = TenderHelper.getInstance().createVoucher(argCmd.getVoucherType(), argCmd.getSerialNumber());
    }
    else {
      voucher = argCmd.getVoucher();
    }

    /* If its a redeem voucher and does not allow multiple redemption, or the activity type is cash out, set
     * the unspent amount to 0. */
    if (((activityType == VoucherActivityCodeType.REDEEMED) && !activityConfig.getMultipleAllowed())
        || (activityType == VoucherActivityCodeType.CASHOUT)) {
      unSpentAmt = NumberUtils.ZERO;
    }
    else {
      if (activityType == VoucherActivityCodeType.ISSUED) {
        // set the issue date in voucher table
        voucher.setIssueDatetimestamp(StoreCalendar.getCurrentBusinessDate());

        // set the expiry date in voucher table.
        if (argConfig.getNumberOfDaysToExpire() > 0) {
          voucher.setExpirationDate(dtv.util.DateUtils.dateAdd(CalendarField.DAY,
              argConfig.getNumberOfDaysToExpire(), StoreCalendar.getCurrentBusinessDate()));
        }
      }

      if (argCmd.getLineItemType() == LineItemType.ITEM) {
        unSpentAmt = ((ISaleItemCmd) argCmd).getItem().getCurrentShelfPrice();
        voucher.setFaceValueAmount(unSpentAmt);
      }
    }

    completeTenderVoucher(argCmd, voucher, unSpentAmt);
    return OpResponseHelper.COMPLETE_RESPONSE;
  }

  /**
   * Return the operation response that contains a prompt request with the specified prompt key.
   * This is mainly established as a separate method to enable subclasses to provide for their own
   * custom implementation.
   * 
   * @param argCmd current command
   * @param argEvent the event that occurred
   * @return {@inheritDoc}
   */
  @Override
  protected IOpResponse handlePromptResponse(IXstCommand argCmd, IXstEvent argEvent) {
    ((IVoucherCmd) argCmd).setSerialNumber(argEvent.getStringData());
    return OpResponseHelper.COMPLETE_RESPONSE;
  }

  /**
   * Handle the redeem voucher tender. First validate the unspent balance to see if there is any
   * value left to be redeemed. If so, process the redemption and update the voucher unspent amount.
   * 
   * @param argCmd the current command
   * @param argEvent the event that occurred
   * @param argVoucher the voucher request to be redeemed
   * @param voucherLineItem the voucher tender line item
   * @param argConfig the voucher config
   * @return either error message prompt for no value left to be redeemed or complete response.
   */
  protected IOpResponse handleRedeemVoucher(IVoucherCmd argCmd, IXstEvent argEvent, IVoucher argVoucher,
      IVoucherLineItem voucherLineItem, VoucherConfig argConfig) {

    if ((argVoucher.getVoucherStatusCode() != null)
        && IVoucherModel.VOID.equalsIgnoreCase(argVoucher.getVoucherStatusCode())) {

      return getPromptResponse(argCmd, argEvent, getVoidPromptKey(), getPromptArgs(argCmd, argEvent));
    }

    if (voucherLineItem != null) {
      // If this voucher was issued in current transaction, its not allowed.
      String activityCode = voucherLineItem.getActivityCode();

      if (VoucherActivityCodeType.ISSUED.toString().equals(activityCode)
          && !argConfig.getAllowIssueAndRedeemInSameTrans()) {
        argCmd.setOpState(ERROR_MESSAGE_PROMPT);
        return getPromptResponse(argCmd, argEvent, getIssueAndRedeemSameTransactionPromptKey(),
            getPromptArgs(argCmd, argEvent));
      }

      if (VoucherActivityCodeType.REDEEMED.toString().equals(activityCode)
          && !argConfig.getActivityConfig(VoucherActivityCodeType.REDEEMED).getMultipleAllowed()) {
        argCmd.setOpState(ERROR_MESSAGE_PROMPT);
        return getPromptResponse(argCmd, argEvent, getAlreadyRedeemedPromptKey(argCmd),
            getPromptArgs(argCmd, argEvent));
      }
    }

    BigDecimal unspentBalanceAmount = argVoucher.getUnspentBalanceAmount();

    if (NumberUtils.isZeroOrNull(unspentBalanceAmount)) {
      return handleReRedeemVoucher(argCmd, argEvent);
    }

    // If the unspent balance of the voucher is greater than 0, update
    // the amount.
    BigDecimal remainingUnspentAmount = null;
    BigDecimal tenderAmount = null;

    if (!argConfig.getActivityConfig(VoucherActivityCodeType.REDEEMED).getMultipleAllowed()) {
      // This voucher can only be redeemed once and for the full issue amount.
      tenderAmount = unspentBalanceAmount;
      remainingUnspentAmount = NumberUtils.ZERO;
    }
    else {
      // This tender can be redeemed multiple times for variable amounts.
      // Apply as much of the remaining balance of the voucher to the current
      // transaction balance as possible, and then calculate whatever unspent
      // voucher balance still remains.
      BigDecimal transactionBalance = ((ISaleTenderCmd) argCmd).getRetailTransaction().getAmountDue();
      tenderAmount = unspentBalanceAmount.min(transactionBalance);
      remainingUnspentAmount = (unspentBalanceAmount.subtract(transactionBalance)).max(NumberUtils.ZERO);
    }

    IVoucherRedeemedLineItem line = (IVoucherRedeemedLineItem) argCmd.getVoucherLineItem();

    if (line != null) {
      line.setAmount(tenderAmount);
    }

    ((ISaleTenderCmd) argCmd).setTenderAmount(tenderAmount);
    completeTenderVoucher(argCmd, argVoucher, remainingUnspentAmount);
    return OpResponseHelper.COMPLETE_RESPONSE;
  }

  /**
   * Handle the prompt for issued tender already exists in the database.
   * 
   * @param argCmd current command
   * @param argEvent the event that occurred
   * @return the message prompt to the user stating this voucher is already issued.
   */
  protected IOpResponse handleReIssueVoucher(IVoucherCmd argCmd, IXstEvent argEvent) {
    argCmd.setOpState(ERROR_MESSAGE_PROMPT);
    IPromptKey key = getAlreadyIssuedPromptKey(argCmd);
    IFormattable[] args = getPromptArgs(argCmd, argEvent);

    return getPromptResponse(argCmd, argEvent, key, args);
  }

  /**
   * Handle the reload voucher activity for a voucher exists in the database.
   * 
   * @param argCmd the current command
   * @param voucher the current voucher
   * @return complete operation state
   */
  protected IOpResponse handleReLoadVoucher(IVoucherCmd argCmd, IVoucher voucher) {
    if (argCmd.getLineItemType() != LineItemType.TENDER) {
      // its a reload voucher sale
      ISaleItemCmd cmd = (ISaleItemCmd) argCmd;
      BigDecimal itemAmount = cmd.getItem().getCurrentShelfPrice();

      if (NumberUtils.isPositive(itemAmount)) {
        BigDecimal unspentAmount = NumberUtils.add(voucher.getUnspentBalanceAmount(), itemAmount);
        voucher.setUnspentBalanceAmount(unspentAmount);
        voucher.setFaceValueAmount(itemAmount);
      }
    }

    argCmd.setVoucher(voucher);
    return OpResponseHelper.COMPLETE_RESPONSE;
  }

  protected IOpResponse handleReRedeemVoucher(IVoucherCmd argCmd, IXstEvent argEvent) {
    argCmd.setOpState(ERROR_MESSAGE_PROMPT);
    IPromptKey key = getAlreadyRedeemedPromptKey(argCmd);
    IFormattable[] args = getPromptArgs(argCmd, argEvent);

    return getPromptResponse(argCmd, argEvent, key, args);
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleSecurityProcessing(IXstCommand argCmd, IXstEvent argEvent) {
    if (argEvent != null) {
      if (argEvent.getData() instanceof SecurityResponse) {
        SecurityResponse response = (SecurityResponse) argEvent.getData();

        if (response.successful()) {
          // If there is any stored data in the command, set it back to the event.
          if (argCmd.getStoredData() != null) {
            argEvent.setData(argCmd.getStoredData());
          }

          IVoucherCmd cmd = (IVoucherCmd) argCmd;
          VoucherConfig config = getVoucherConfig(cmd);
          handleNewVoucher(cmd, config);
          return OpResponseHelper.COMPLETE_RESPONSE;
        }
        else {
          argCmd.setOpState(ERROR_MESSAGE_PROMPT);
          return getPromptResponse(argCmd, argEvent, PromptKey.valueOf("INVALID_VOUCHER_SERIAL_NUMBER"),
              getPromptArgs(argCmd, argEvent));
        }
      }
    }

    return OpResponseHelper.WAIT_RESPONSE;
  }

  /**
   * Retrieve the datastore record for this voucher (if one exists). Create a new Voucher or update
   * the existing voucher based on if the voucher exists.
   * 
   * @param argCmd the current command
   * @param argEvent the event that occurred
   * @param argConfig the voucher config
   * @return the response from executing the operation
   */
  protected IOpResponse handleVoucher(IVoucherCmd argCmd, IXstEvent argEvent, VoucherConfig argConfig) {
    IPosTransaction tran = ((ITransactionCmd) argCmd).getTransaction();
    List<IRetailTransactionLineItem> lineItems =
        (tran != null) ? tran.getRetailTransactionLineItems() : noLines_;

    // check that this voucher is not already on the transaction
    String serial = argCmd.getSerialNumber();
    if (serial != null) {
      for (IRetailTransactionLineItem temp : lineItems) {
        if (temp.getVoid()) {
          continue;
        }

        String tempSerial = null;
        IPromptKey promptKey = null;
        /*CUSTOMIZATION*/
        if (temp instanceof IVoucherSaleLineItem) {
          tempSerial = ((IVoucherSaleLineItem) temp).getSerialNumber();
          if (tempSerial != null) {
            if (serial.equalsIgnoreCase(tempSerial)) {
              promptKey = getAlreadyIssuedPromptKey(argCmd);
            }
            if (IsdGiftCards.contains(argCmd.getVoucherType())) {
              if (VoucherActivityCodeType.REDEEMED.equals(argCmd.getVoucherActivityCodeType())
                  && serial.equalsIgnoreCase(tempSerial)) {
                promptKey = getIssueAndRedeemSameTransactionPromptKey();
              }
              else if (VoucherActivityCodeType.REDEEMED.equals(argCmd.getVoucherActivityCodeType())) {
                promptKey = getGiftCardTenderNotAllowedPromptKey();
              }
            }
          }
        }//<END CUSTOMIZATION>
        else if (temp instanceof IVoucherLineItem) {
          tempSerial = ((IVoucherLineItem) temp).getSerialNumber();
          if ((tempSerial != null) && serial.equalsIgnoreCase(tempSerial)) {
            promptKey = getAlreadyRedeemedPromptKey(argCmd);
          }
        }

        if (promptKey != null) {
          argCmd.setOpState(ERROR_MESSAGE_PROMPT);
          return getPromptResponse(argCmd, argEvent, promptKey, getPromptArgs(argCmd, argEvent));
        }
      }
    }

    // Retrieve the datastore record for this voucher (if one exists).
    final IVoucher voucher;
    IVoucherLineItem voucherLineItem =
        TenderHelper.getInstance().getVoucherFromCurrentTransaction(tran, argCmd.getVoucherType(),
            argCmd.getSerialNumber());
    VoucherActivityCodeType activityType = argCmd.getVoucherActivityCodeType();
    ActivityConfig activityConfig = argConfig.getActivityConfig(activityType);

    if (voucherLineItem == null) {
      voucher = TenderHelper.getInstance().lookupVoucher(argCmd.getVoucherType(), argCmd.getSerialNumber());
    }
    else {
      voucher = voucherLineItem.getVoucher();
    }

    argCmd.setVoucher(voucher);

    if ((voucher == null) || !activityConfig.getValidateLocalBalance()) {
      // There is either no record in the database for this voucher,
      // or we are configured to ignore any previous activity in the
      // database because the processor is the final answer
      return handleNewVoucher(argCmd, argConfig);
    }
    else if (activityType == VoucherActivityCodeType.ISSUED) {
      if (activityConfig.getMultipleAllowed()) {
        return handleReLoadVoucher(argCmd, voucher);
      }
      else {
        // The user is attempting to re-issue an existing voucher.
        return handleReIssueVoucher(argCmd, argEvent);
      }
    }
    else if (activityType == VoucherActivityCodeType.RELOAD) {
      return handleReLoadVoucher(argCmd, voucher);
    }
    else if (activityType == VoucherActivityCodeType.CASHOUT) {
      return handleCashOutVoucher(argCmd, argEvent, voucher);
    }
    else {
      return handleRedeemVoucher(argCmd, argEvent, voucher, voucherLineItem, argConfig);
    }
  }

  protected boolean isSwipeRequired(IXstCommand argCmd) {
    IVoucherCmd cmd = (IVoucherCmd) argCmd;

    VoucherRootConfig vrc = TenderHelper.getInstance().getVoucherRootConfig();
    String voucherType = cmd.getVoucherType();
    VoucherConfig vc = vrc.getVoucherConfig(voucherType);
    VoucherActivityCodeType act = cmd.getVoucherActivityCodeType();
    ActivityConfig ac = vc.getActivityConfig(act);

    return ac.getRequireInputEvent();
  }

  protected void setHardwareType(IVoucherCmd argCmd, IHardwareType<?> argHardwareType) {
    IVoucherLineItem vline = argCmd.getVoucherLineItem();

    if (vline != null) {
      vline.setEntryMethodCode(argHardwareType.getName());
    }
    argCmd.setEntryMethodCode(argHardwareType.getName());

    if (argCmd instanceof ISaleTenderCmd) {
      ((ISaleTenderCmd) argCmd).setScanEntryMethod(argHardwareType);
    }
    else if (argCmd instanceof IScanItemCmd) {
      ((IScanItemCmd) argCmd).setScanEntryMethod(argHardwareType);
    }
  }

  protected IOpResponse validateVoucherNumber(IXstCommand argCmd, IXstEvent argEvent) {
    IVoucherCmd cmd = (IVoucherCmd) argCmd;
    final String serialNumber;
    final IInput input;
    final IAccountInputEvent<?> inputEvent;

    final Object eventCandidate =
        (argEvent instanceof IAccountInputEvent<?>) ? argEvent : cmd.getStoredData();

    if (eventCandidate instanceof IAccountInputEvent<?>) {
      inputEvent = (IAccountInputEvent<?>) eventCandidate;
      cmd.setStoredData(inputEvent);
      input = inputEvent.getInputData();
      setHardwareType(cmd, input.getSourceType());
      serialNumber = inputEvent.getAccountNumber();

      // Validate the input event type to make sure its current voucher type.
      if ((cmd.getVoucherType() == null)
          || ObjectUtils.equivalent(inputEvent.getAccountType(), cmd.getVoucherType())) {

        cmd.setSerialNumber(serialNumber);
        cmd.setVoucherType(inputEvent.getAccountType());
        // returning null indicates no problems with the voucher
        return null;
      }
    }
    else {
      if ((argEvent != null) && (argEvent.getData() != null)) {
        serialNumber = argEvent.getStringData().trim();
      }
      else {
        serialNumber = cmd.getSerialNumber();
      }

      input = new KeyboardInput(serialNumber);
      setHardwareType(cmd, HardwareType.KEYBOARD);
      IXstEvent event = null;

      try {
        if (cmd.getVoucherType() != null) {
          event = EventDiscriminator.getInstance().translateEvent(input, new Object[] {cmd.getVoucherType()});
        }
        else {
          event = EventDiscriminator.getInstance().translateEvent(input);
        }
      }
      catch (Exception ex) {
        logger_.info("Exception caught", ex);
      }

      if (event instanceof IAccountInputEvent) {
        inputEvent = (IAccountInputEvent) event;
      }
      else {
        inputEvent = null;
      }
    }

    if (inputEvent != null) {
      String type = inputEvent.getAccountType();

      if ((cmd.getVoucherType() == null) || type.equalsIgnoreCase(cmd.getVoucherType())) {
        cmd.setSerialNumber(serialNumber);
        boolean origVoucherTypeNull = false;

        if (cmd.getVoucherType() == null) {
          origVoucherTypeNull = true;
          cmd.setVoucherType(type);
        }

        VoucherConfig config = getVoucherConfig(cmd);
        VoucherActivityCodeType activityType = cmd.getVoucherActivityCodeType();
        ActivityConfig activityConfig = config.getActivityConfig(activityType);

        if (activityConfig == null) {
          logger_.warn("Cannot process activity type " + activityType + " for " + config.getName());
          cmd.setSerialNumber(null);

          if (origVoucherTypeNull) {
            cmd.setVoucherType(null);
          }

          argCmd.setOpState(ERROR_MESSAGE_PROMPT);
          return getPromptResponse(argCmd, argEvent, PromptKey.valueOf("INVALID_VOUCHER_FOR_ACTION"),
              new IFormattable[] {ff_.getSimpleFormattable(type)});
        }

        // returning null indicates no problems with the voucher
        return null;
      }
    }

    argCmd.setOpState(ERROR_MESSAGE_PROMPT);
    return getPromptResponse(argCmd, argEvent, PromptKey.valueOf("INVALID_VOUCHER_SERIAL_NUMBER"),
        getPromptArgs(argCmd, argEvent));
  }
  
}
