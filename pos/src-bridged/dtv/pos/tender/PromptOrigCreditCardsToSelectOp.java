// $Id: PromptOrigCreditCardsToSelectOp.java 748 2013-09-12 19:55:52Z dtvdomain\pgolli $
package dtv.pos.tender;

import static dtv.util.NumberUtils.*;

import java.math.BigDecimal;

import org.apache.log4j.Logger;

import imsformat.credit.CCRequest;

import dtv.hardware.HardwareMgr;
import dtv.hardware.custdisplay.IDtvCustDisplay;
import dtv.i18n.FormatterType;
import dtv.i18n.IFormattable;
import dtv.pos.common.*;
import dtv.pos.framework.StationModelMgr;
import dtv.pos.framework.action.type.XstChainActionType;
import dtv.pos.framework.action.type.XstDataActionKey;
import dtv.pos.framework.op.OpResponseHelper;
import dtv.pos.framework.op.OpState;
import dtv.pos.framework.ui.config.DataFieldConfig;
import dtv.pos.framework.ui.config.PromptConfig;
import dtv.pos.framework.ui.op.AbstractListPromptOp;
import dtv.pos.iframework.action.IXstDataAction;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.*;
import dtv.pos.iframework.type.TenderUsageCodeType;
import dtv.pos.iframework.ui.config.IPromptKey;
import dtv.pos.iframework.ui.model.IStationModel;
import dtv.pos.register.IRetailTransactionCmd;
import dtv.pos.register.ItemLocator;
import dtv.pos.register.returns.ReturnConstants;
import dtv.tenderauth.impl.isd.AbstractIsdRequest;
import dtv.xst.dao.tnd.*;
import dtv.xst.dao.trl.IRetailTransaction;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;
import dtv.xst.dao.ttr.ITenderLineItem;
import dtv.xst.daocommon.ExchangeRateHelper;
import dtv.hardware.types.*;

/**
 * This operation handles retrieving and prompting the original credit card information for the user
 * to select for tender in the return verification transaction.<br>
 * src-brigded: To generate a new JOURNAL_KEY for the verified return<br>
 * Copyright (c) 2003 Datavantage Corporation
 * 
 * @author jhsiao
 * @created August 8, 2003
 * @version $Revision: 748 $
 */
public class PromptOrigCreditCardsToSelectOp
    extends AbstractListPromptOp
    implements IReversibleOp {

  private static final Logger logger_ = Logger.getLogger(PromptOrigCreditCardsToSelectOp.class);
  private static final long serialVersionUID = 1L;
  // Command key used to identify whether to skip this operation.
  private static final String KEY_SKIP_PROMPT = "PromptOrigCreditCardsToSelectOp_Skip_Prompt";
  // Value used to identify whether to skip this operation.
  private static final String VALUE_SKIP_PROMPT = "PromptOrigCreditCardsToSelectOp_Skip_Prompt";

  /**
   * Return whether the flag is set on the command signaling that this operation is done and should
   * not be run again.
   * 
   * @param argCmd the command to check.
   * @return <code>true</code> if the flag is set, <code>false</code> if it is not.
   */
  public static boolean isSkipFlagSet(IXstCommand argCmd) {
    return VALUE_SKIP_PROMPT.equals(argCmd.getValue(KEY_SKIP_PROMPT));
  }

  private final IOpState POST_AMT_PROMPT = new OpState(this, "POST_AMT_PROMPT");

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    if (!checkForAllVerifiedReturn(argCmd)) {
      return OpResponseHelper.COMPLETE_RESPONSE;
    }

    if (argCmd.getOpState() == POST_AMT_PROMPT) {
      ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;

      if ((argEvent instanceof IXstDataAction) && (argEvent.getData() instanceof BigDecimal)
          && isPositive((BigDecimal) argEvent.getData())
          && ((IXstDataAction) argEvent).getActionKey().equals(XstDataActionKey.ACCEPT)) {

        // Tom -> The entered amount can not be more than the amount due on the transaction!
        BigDecimal enteredAmt = (BigDecimal) argEvent.getData();

        /* This is one of the few cases where the currency ID of the original tender line item matters. If the
         * credit card was tendered in a store that uses a different currency than this store, then the amount
         * needs to be localized to this store's currency. Note that getLocalizedAmount() handles all of these
         * checks. */
        ICreditDebitTenderLineItem origTenderLine = cmd.getOriginalCreditCardLineItem();
        BigDecimal localizedOriginalAmount =
            ExchangeRateHelper.getLocalizedAmount(origTenderLine.getAmount(), origTenderLine.getCurrencyId());

        BigDecimal amt = least(cmd.getTransaction().getAmountDue().negate(), localizedOriginalAmount);

        if (isGreaterThan(enteredAmt, amt)) {
          argCmd.setOpState(POST_AMT_PROMPT);
          IFormattable[] args =
              new IFormattable[] {cmd.getTenderDescFormattable(),
                  ff_.getSimpleFormattable(amt, FormatterType.MONEY)};

          return HELPER.getPromptResponse(PromptKey.valueOf("REENTER_TENDER_AMOUNT"), args,
              getConfigWithDefaultAmount(amt));
        }

        BigDecimal roundAmt =
            CommonHelper.getInstance().roundCurrency(((BigDecimal) argEvent.getData()).negate());

        cmd.getTenderLineItem().setAmount(roundAmt);
        cmd.getTransaction().addRetailTransactionLineItem(cmd.getTenderLineItem());
      }
      else {
        ITenderLineItem tli = cmd.getTenderLineItem();

        if (tli != null) {
          tli.setVoid(true);
        }
        cmd.setTenderLineItem(null);

        return handleInitialState(argCmd, argEvent);
      }
      return HELPER.completeResponse();
    }
    return super.handleOpExec(argCmd, argEvent);
  }

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpReverse(IXstCommand argCmd, IXstEvent argEvent) {
    ITenderLineItem tenderLineItem = ((ISaleTenderCmd) argCmd).getTenderLineItem();

    if (tenderLineItem != null) {
      tenderLineItem.setVoid(true);
    }
    return HELPER.completeResponse();
  }

  /**
   * Return whether or not this operation is applicable based on whether or not all return items in
   * the current transaction are all verified return and the original transaction was tendered with
   * credit card.
   * 
   * @param argCmd the command object
   * @return whether or not this operation is applicable
   */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    IRetailTransaction retailTransaction = cmd.getRetailTransaction();

    // If this operation previously ran and configured itself not to run again...
    if (isSkipFlagSet(argCmd)) {
      return false;
    }

    // Amount due must be negative for a return.
    if (!isNegative(retailTransaction.getAmountDue())) {
      return false;
    }

    // Do not offer to apply to a credit card if a gift receipt is used...
    Boolean giftReceipt =
        getAttributeModel(argCmd).getAttribute(ReturnConstants.RETURNING_WITH_GIFT_RCPT_FLAG);

    if ((giftReceipt != null) && giftReceipt.booleanValue()) {
      return false;
    }

    if (0 == getPromptList(argCmd, null).length) {
      return false;
    }
    return true;
  }

  /**
   * Check to see if the current transaction is a verified return.
   * @return whether or not the current transaction is a verified return.
   */
  protected boolean checkForAllVerifiedReturn(IXstCommand argCmd) {
    IStationModel stationModel = StationModelMgr.getInstance().getStationModel();
    IRetailTransaction currentTransaction = (IRetailTransaction) stationModel.getCurrentTransaction();

    return ItemLocator.getLocator().checkForAllVerifiedReturn(currentTransaction);
  }

  protected PromptConfig getConfigWithDefaultAmount(BigDecimal amt) {
    PromptConfig config = new PromptConfig();
    config.setDataFieldConfig(new DataFieldConfig());
    config.getDataFieldConfig().setDefaultValue(amt);
    return config;
  }

  /** {@inheritDoc} */
  @Override
  protected IPromptKey getEmptyListPromptKey(IXstCommand argCmd) {
    /* If there is nothing for the list, this operation should not execute in the first place. */
    return null;
  }

  /** {@inheritDoc} */
  @Override
  protected IPromptKey getPromptKey(IXstCommand argCmd) {
    return PromptKey.valueOf("ORIG_CREDITCARD_SELECTION");
  }

  /**
   * Return an array of all original sales transaction credit card tender line item data objects.
   * 
   * @param argCmd the current command
   * @param argEvent the current event
   * @return an array of all original sales transaction credit card tender line item data objects.
   */
  @Override
  protected Object[] getPromptList(IXstCommand argCmd, IXstEvent argEvent) {
    IRetailTransactionCmd cmd = (IRetailTransactionCmd) argCmd;
    final IRetailTransaction currentTransaction = cmd.getRetailTransaction();

    return TenderHelper.getInstance().getUsableTendersFromOriginalTransactions(currentTransaction).toArray();
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleDataAction(IXstCommand argCmd, IXstDataAction argEvent) {
    // Accept the selected credit card.
    if (argEvent.getActionKey() == XstDataActionKey.ACCEPT) {
      return handlePromptResponse(argCmd, argEvent);
    }
    // Canceled out of selecting a credit card.
    else if (argEvent.getActionKey() == XstDataActionKey.NO) {
      argCmd.setValue(KEY_SKIP_PROMPT, VALUE_SKIP_PROMPT);
      return HELPER.getRunChainResponse(OpStatus.COMPLETE_HALT, OpChainKey.valueOf("CHECK_SALE_COMPLETE"),
          argCmd, XstChainActionType.START);
    }

    return HELPER.completeResponse();
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleInitialState(IXstCommand argCmd, IXstEvent argEvent) {
    // We should display the refund due on the Customer Display.
    try {
      IDtvCustDisplay display = HardwareMgr.getCurrentHardwareMgr().getCustDisplay();

      if (display != null) {
        display.subtotalMode(true);
      }
    }
    catch (Exception e) {
      logger_.error("There was an error when attempting to display the refund "
          + "due on the customer display device.", e);
    }

    return super.handleInitialState(argCmd, argEvent);
  }

  /**
   * After the user select a credit card to use, the system will set the entire transaction amount
   * due as the total tender amount for this credit card tender.
   * 
   * @param argCmd the current command
   * @param argEvent the current event
   * @return complete, or the result of prompting.
   */
  @Override
  protected IOpResponse handlePromptResponse(IXstCommand argCmd, IXstEvent argEvent) {
    final Object data = argEvent.getData();

    if ((argEvent != null) && (data != null)) {
      ICreditDebitTenderLineItem lineItem = (ICreditDebitTenderLineItem) data;
      ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
      cmd.setOriginalCreditCardLineItem(lineItem);

      final ITender tender = lineItem.getTender();
      final ITenderType tenderType = tender.getTenderType();
      final String code = tenderType.getTenderTypecode();
      final TenderCategory tenderCategory = TenderCategory.forName(code);

      ICreditDebitTenderLineItem tenderLineItem =
          (ICreditDebitTenderLineItem) TenderHelper.getInstance().createTenderLineItem(tender,
              tenderCategory, TenderStatus.REFUND, argEvent);
      tenderLineItem.setEntryMethodCode(ItemLocator.getLocator().getListSelectionEntryMethod().getName());

      if (cmd.getReturnWithReceipt()) {
        cmd.setTenderUsageCodeType(TenderUsageCodeType.RETURN_WITHRECEIPT);
      }
      else {
        cmd.setTenderUsageCodeType(TenderUsageCodeType.RETURN_WITHOUTRECEIPT);
      }

      TenderHelper.getInstance().copyCreditDebitTenderLineItem(tenderLineItem, lineItem, false);
      //380885 - 1.2.5 - Verified return of credit card trans-acct # should be logged as "keyed"
      tenderLineItem.setEntryMethodCode(HardwareType.KEYBOARD.getName());
      tenderLineItem.setStringProperty(
          CCRequest.JOURNAL_KEY,
          AbstractIsdRequest.generateJournalKey(String.valueOf(cmd.getRetailLocationId()),
              String.valueOf(cmd.getWorkstationId())));
      cmd.setTenderLineItem(tenderLineItem);
      cmd.setTender(tender);
      // This is one of the few cases where the currency ID of the original tender line item matters.
      // If the credit card was tendered in a store that uses a different currency than this store,
      // then the amount needs to be localized to this store's currency. Note that getLocalizedAmount()
      // handles all of these checks.
      BigDecimal localizedOriginalAmount =
          ExchangeRateHelper.getLocalizedAmount(lineItem.getAmount(), lineItem.getCurrencyId());
      BigDecimal amt = least(cmd.getTransaction().getAmountDue().negate(), localizedOriginalAmount);

      if (ConfigurationMgr.getPromptTndrAmtForOriginalCreditCardTender()) {
        argCmd.setOpState(POST_AMT_PROMPT);
        IFormattable[] args = new IFormattable[] {cmd.getTenderDescFormattable()};
        return HELPER.getPromptResponse(PromptKey.valueOf("ENTER_TENDER_AMOUNT"), args,
            getConfigWithDefaultAmount(amt));
      }
      else {
        BigDecimal roundAmt = CommonHelper.getInstance().roundCurrency(amt.negate());
        tenderLineItem.setAmount(roundAmt);
        cmd.getTransaction().addRetailTransactionLineItem(tenderLineItem);
      }
    }
    return HELPER.completeResponse();
  }

  /** {@inheritDoc} */
  @Override
  protected boolean showListIfOne(IXstCommand argCmd) {
    return true;
  }
}
