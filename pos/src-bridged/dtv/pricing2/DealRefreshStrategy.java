//$Id: DealRefreshStrategy.java 608 2013-03-01 15:03:13Z suz.sxie $
package dtv.pricing2;

import java.math.BigDecimal;
import java.util.*;

/**
 * Strategy for refreshing the deals to the global dealspace. Implementors of this class should
 * assume that the dealspace starts out empty, and proceed to add deals to the dealspace using the
 * {@link #addDeal(PricingDeal)} method while inside the {@link #refresh()} method.<br>
 * Note that the newDeal, newAction, and newItem methods will fail miserably if not called from
 * within the implementor's refresh method. Thus, it is not possible to pre-cache deals during
 * construction. This is a lamentable situation, but allows for more cohesion between objects inside
 * the deal engine, and less error-prone integrations.
 *
 * @author Greyson Fischer <gfischer@datavantagecorp.com>
 * @version $Revision: 608 $
 * @param <D> Type of native deals.
 */
public abstract class DealRefreshStrategy<D> {
  // The list of deals added during a refresh. Before and after refreshing, this is always null.
  List<PricingDeal<D>> _added = null;
  // The pricing adapter from whence this refreshing strategy comes.
  PricingAdapter<?, D, ?> _adapter = null;

  // An item adapter obtained from the pricing adapter.
  private PricingItemAdapter<?> _iAdapt = null;
  private Enum<?> PRICE;

  /**
   * Add a deal to the dealspace during refresh. Implementors of a deal refresh strategy must use
   * this function to add deals to the global dealspace during their execution of 'refresh'.
   *
   * @param deal Completed deal to be added to the global dealspace.
   */
  protected final void addDeal(PricingDeal<D> deal) {
    // Find the insert point, then insert.
    final DealComparator comp = new DealComparator();
    int idx = Math.abs(iteratorBinarySearch(_added, deal, comp) + 1);
    _added.add(idx, deal);
  }

  /**
   * Get a new action description. In order to create deals which take action on either items or the
   * transaction as a whole, the refreshing strategy must ensure that they obtain new action
   * descriptions from this method. The type of action returned is opaque, but will provide the
   * action corresponding to <tt>action</tt> to the deal and may be set as either the full-deal
   * 'transAction' or as an individual item's action.<br>
   *
   * @param action Enumeration of the action to be taken.
   * @param amount Amount of discount, corresponding to the action.
   * @return A new action corresponding to arguments.
   */
  protected final DealAction newAction(PricingAction action, BigDecimal amount) {
    if (action != null) {
      switch (action) {
        case NEW_PRICE:
          return new ActionNewPrice(_iAdapt.translateItemField(PRICE, amount));
        case CURRENCY_OFF:
          return new ActionCurrencyOff(_iAdapt.translateItemField(PRICE, amount));
        case PERCENT_OFF:
          return new ActionPercentOff(amount, _iAdapt);
      }
    }
    return null;
  }

  /**
   * Create a new deal description. In order to create deals, the implementor must use this method
   * during calls to 'refresh'. Without any further alteration, a deal returned from this method
   * will have no trigger, be effective on all days during all times, exclude no other deals, match
   * only a full transaction with no minimum or maximum subtotal, have no restrictions on its
   * generosity, be applied only once, and take no action on either items or the transaction. <br>
   * The native deal will have no effect on the deal from the perspective of the deal engine, and
   * should be used as a payload for an object which is cogniscent of the applicaion of deals to the
   * native transaction type.
   *
   * @param nativeDeal Native deal/payload of this deal.
   * @return A new deal description.
   * @see #refresh()
   */
  protected final PricingDeal<D> newDeal(D nativeDeal) {
    return new PricingDeal<D>(_adapter, nativeDeal);
  }

  /**
   * Get a new item description. In order to create deals which can match items, the refreshing
   * strategy must ensure that they obtain a new item matching description from this method. Without
   * any further alteration, a rule returned from this method will non-consumably match 1 of any
   * item type with no minimum total threshold.
   *
   * @return A new, empty item matching description.
   * @see #refresh()
   */
  protected final ItemMatch newItem() {
    return new ItemMatch(_adapter);
  }

  /**
   * Create a new price substitution action with modification. The action that this method produces
   * substitutes a different field as the price, after modifying it with either a percentage, or
   * currency off. <br>
   * Although there are two arguments for the change to make to the field, only one may be used at a
   * time. PercentMod will take precedence.
   *
   * @param field Enumeration value representing the field which will become the substitute.
   * @param percentMod Percentage by which to alter the field before substitution.
   * @param currencyMod The currency to add or remove from the field before substitution.
   * @return The deal action which substitutes a modified price.
   */
  protected DealAction newSubstitutionAction(Enum<?> field, BigDecimal percentMod, BigDecimal currencyMod) {
    DealAction act = null;

    if (percentMod != null) {
      act = new ActionPercentSubstitute(field.ordinal(), percentMod);

    }
    else if (currencyMod != null) {
      act = new ActionAbsoluteSubstitute(field.ordinal(), _iAdapt.translateItemField(PRICE, currencyMod));

    }
    else {
      act = new ActionSubstitute(field.ordinal());
    }

    return act;
  }

  /**
   * Refresh the dealspace in an implementation-specific way. Integrators of the deal engine with
   * any system will be required to implement this method to refresh the global dealspace. This
   * method exists to provide an engine-agnostic storage hook. The deals may be obtained from a
   * database, flat file, or any other mechanism desired or applicable to the environment. Ideally,
   * this method will need to run no more than nightly and at each restart of the system; thus,
   * strict efficiency is not important.
   *
   * @see #addDeal(PricingDeal)
   * @see #newAction(PricingAction, BigDecimal)
   * @see #newDeal(Object)
   * @see #newItem()
   */
  protected abstract void refresh();

  /**
   * Containing refresh. This sets up the environment for the child class to perform the refresh in
   * a properly stateful way.<br>
   * This method is for deal engine internal use only.
   *
   * @param space Dealspace that this strategy has been requested to fill.
   * @param now The external actor time.
   */
  final void refresh(DealSpaceGlobal<D> space, Date now) {
    // Create the audit list for alteration by the subclass.
    _added = new LinkedList<PricingDeal<D>>();

    // Call on the implementation to get the deals in the appropriate manner.
    refresh();

    // Make the changes to the dealspace.
    space.setDeals(_added, now);
/*    for (PricingDeal<D> dd: _added) {
      String value = null;
      if (dd.getTransAction()!= null) {
        value = dd.getTransAction().getActionEnum().toString();
        System.out.println(dd.toString() + " *********** " + value);
      }
      if(dd.getDealAction() != null) {
        for (DealAction a : dd.getDealAction()) {
          if ( a != null) {
          System.out.println(dd.toString() + " *********** " + a.getActionEnum());
          }
        }
      }

    }*/

    _added = null;
  }

  /**
   * Set the pricing adapter to use for this refresh strategy. Used internally to the deal engine,
   * this sets the item adapter after the refresh strategy is requested from the implemented
   * {@link PricingAdapter}. This is for engine-internal use only.
   *
   * @param argAdapter Pricing adapter.
   */
  final void setPricingAdapter(PricingAdapter<?, D, ?> argAdapter) {
    this._adapter = argAdapter;
    this._iAdapt = this._adapter.getItemAdapter();
    this.PRICE = _adapter.field("PRICE");
  }

  /**
   * Available pricing actions. This enumeration serves two purposes. The first is as identifier for
   * the action desired from {@link DealRefreshStrategy#newAction(PricingAction, BigDecimal)}. The
   * second is as a sorting mechanism for the deals as they are inserted into the refreshing stage.
   *
   * @author Greyson Fischer<gfischer@datavantagecorp.com>
   */
  public enum PricingAction {
    SUBSTITUTE, NEW_PRICE, CURRENCY_OFF, PERCENT_OFF;

    private static TreeMap<String, PricingAction> uppers = new TreeMap<String, PricingAction>();
    static {
      for (PricingAction field : values()) {
        uppers.put(field.name().toUpperCase(), field);
      }
    }

    public static PricingAction valueOfIgnoreCase(String name) {
      return uppers.get(name.toUpperCase());
    }

  }

  private static <T> int iteratorBinarySearch(List<? extends T> l, T key, Comparator<? super T> c) {
    int low = 0;
    int high = l.size()-1;
    ListIterator<? extends T> i = l.listIterator();

    while (low <= high) {
      int mid = (low + high) >>> 1;
      T midVal = get(i, mid);
      int cmp = c.compare(midVal, key);

      if (cmp < 0)
        low = mid + 1;
      else if (cmp > 0)
        high = mid - 1;
      else
        return mid; // key found
    }
    return -(low + 1);  // key not found
  }

  /**
   * Gets the ith element from the given list by repositioning the specified
   * list listIterator.
   */
  private static <T> T get(ListIterator<? extends T> i, int index) {
    T obj = null;
    int pos = i.nextIndex();
    if (pos <= index) {
      do {
        obj = i.next();
      } while (pos++ < index);
    } else {
      do {
        obj = i.previous();
      } while (--pos > index);
    }
    return obj;
  }
}
