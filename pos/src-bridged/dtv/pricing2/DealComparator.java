package dtv.pricing2;

import java.util.Comparator;

/**
 * Best deal ordering comparator. This class should be used to order deals in such a way as to
 * determine in which order two deals should be applied in order to get the best deal, assuming both
 * are compatible.
 *
 * @author Greyson Fischer <gfischer@datavantagecorp.com>
 */
class DealComparator
  implements Comparator<PricingDeal<?>> {

  @Override
  public int compare(PricingDeal<?> left, PricingDeal<?> right) {
    //In the case that there is a difference of prioritization, this is the
    // last step before "randomly" ordering the remaining.
    int dif = left.priorityNudge - right.priorityNudge;
    
    if (dif != 0) {
      return (dif / Math.abs(dif));
    }

    // Deferred deals go last, always.
    if (left.deferred && !right.deferred) {
      return -1;
    }
    else if (!left.deferred && right.deferred) {
      return 1;
    }

    // Transactional deals go before item-based deals.
    if ((left.transAction != null) && (right.transAction == null)) {
      return 1;
    }
    else if ((left.transAction == null) && (right.transAction != null)) {
      return -1;
    }
    else if ((left.transAction != null) && (right.transAction != null)) {
      if (left.transAction.getActionOrdinal() != right.transAction.getActionOrdinal()) {
        return left.transAction.getActionOrdinal() - right.transAction.getActionOrdinal();
      }
      
      if((left.transAction instanceof ActionNewPrice) && (right.transAction instanceof ActionNewPrice)){
        return ((ActionNewPrice) left.transAction).getNewPrice() < ((ActionNewPrice) right.transAction).getNewPrice()?-1:1;
      }else if((left.transAction instanceof ActionCurrencyOff) && (right.transAction instanceof ActionCurrencyOff)){
        return ((ActionCurrencyOff) left.transAction).getAmountOff().abs().longValue() < ((ActionCurrencyOff) right.transAction).getAmountOff().abs().longValue() ?1:-1;
      }else if((left.transAction instanceof ActionPercentOff) && (right.transAction instanceof ActionPercentOff)){
        return ((ActionPercentOff) left.transAction).getPercentageOff().abs().longValue() < ((ActionPercentOff) right.transAction).getPercentageOff().abs().longValue() ?1:-1;
     }
      
      long diff = left.subtotalMin - right.subtotalMin;
      if (diff != 0) {
        return (int) (Math.abs(diff) / diff);
      }

      diff = right.subtotalMax - left.subtotalMax;
      if (diff != 0) {
        return (int) (Math.abs(diff) / diff);
      }
    }
    
    if ((left.itemActions != null) && (left.itemActions.length > 0) && (right.itemActions == null)) {
      return 1;
    }
    else if ((left.itemActions == null) && (right.itemActions != null) && (right.itemActions.length > 0)) {
      return -1;
    }
    else if ((left.itemActions != null) && (left.itemActions.length > 0) && (right.itemActions != null)
      && (right.itemActions.length > 0)) {
//      if (left.collides(right)) {
        int lCount = 0, rCount = 0;
        int lBest = 20, rBest = 20;
        boolean lSwallows = false, rSwallows = false;
        boolean lbogo=false,rbogo=false;
        DealAction lAction = null;
        DealAction rAction = null;
        
        for (int i = 0; i < left.itemActions.length; i++ ) {
          if (left.itemSet[i] != null) {
            //lCount += left.itemSet[i]._consumable ? 1 : 0;
            lSwallows |= left.itemSet[i]._maxQty == Long.MAX_VALUE;
            if (left.itemActions[i] != null) {
              lBest =
                (lBest < left.itemActions[i].getActionOrdinal()) ? left.itemActions[i].getActionOrdinal()
                  : lBest;
              lCount += left.itemActions[i].getActionOrdinal();
              if(lAction == null){
                lAction = left.itemActions[i];
              }
            }
          }
          if(left.itemActions.length >1 ){
            if(left.itemActions[i] == null && !lbogo){
              lbogo = true;
            }
            if(lAction==null){
              lAction = left.itemActions[i];
            }
          }
        }
        for (int i = 0; i < right.itemActions.length; i++ ) {
          if (right.itemSet[i] != null) {
            //rCount += right.itemSet[i]._consumable ? 1 : 0;
            rSwallows |= right.itemSet[i]._maxQty == Long.MAX_VALUE;
            if (right.itemActions[i] != null) {
              rBest =
                (rBest < right.itemActions[i].getActionOrdinal()) ? right.itemActions[i].getActionOrdinal()
                  : rBest;
              rCount += right.itemActions[i].getActionOrdinal();
//              rCount += 1;
              
              if(rAction == null){
                rAction = right.itemActions[i];
              }
            }
          }
          if(right.itemActions.length>1){
            if(right.itemActions[i] == null && !rbogo){
              rbogo = true;
            }
            if(rAction == null)
            {
              rAction = right.itemActions[i];
            }
          }
        }
        
        //BOGO deal will be after new price,currence off or percent off
        if(lbogo && !rbogo){
          return 1;
        }else if (!lbogo && rbogo){
          return -1;
        }else if(lbogo && rbogo){
           if(lAction!=null && rAction!=null){
             if((lAction instanceof ActionNewPrice) && (rAction instanceof ActionNewPrice)){
               return ((ActionNewPrice) lAction).getNewPrice() < ((ActionNewPrice) rAction).getNewPrice()?-1:1;
             }else if((lAction instanceof ActionCurrencyOff) && (rAction instanceof ActionCurrencyOff)){
               return ((ActionCurrencyOff) lAction).getAmountOff().abs().longValue() < ((ActionCurrencyOff) rAction).getAmountOff().abs().longValue() ?1:-1;
             }else if((lAction instanceof ActionPercentOff) && (rAction instanceof ActionPercentOff)){
               return ((ActionPercentOff) lAction).getPercentageOff().abs().longValue() < ((ActionPercentOff) rAction).getPercentageOff().abs().longValue() ?1:-1;
            }
          }
        }
        // Swallowing deals should be put after non-swallowers.
        if (lSwallows && !rSwallows) {
          return 1;
        }
        else if (!lSwallows && rSwallows) {
          return -1;
        }
        if (rBest != lBest) {
          return lBest - rBest;
        }
        float diff =
          (((float) lCount) / ((float) left.itemActions.length))
            - (((float) rCount) / ((float) right.itemActions.length));
        if (diff != 0.0) {
          return (int) (diff / Math.abs(diff));
        }else if(lAction!=null && rAction!=null && diff==0.0){
          if((lAction instanceof ActionNewPrice) && (rAction instanceof ActionNewPrice)){
            return ((ActionNewPrice) lAction).getNewPrice() < ((ActionNewPrice) rAction).getNewPrice()?-1:1;
          }else if((lAction instanceof ActionCurrencyOff) && (rAction instanceof ActionCurrencyOff)){
            return ((ActionCurrencyOff) lAction).getAmountOff().abs().longValue() < ((ActionCurrencyOff) rAction).getAmountOff().abs().longValue() ?1:-1;
          }else if((lAction instanceof ActionPercentOff) && (rAction instanceof ActionPercentOff)){
            return ((ActionPercentOff) lAction).getPercentageOff().abs().longValue() < ((ActionPercentOff) rAction).getPercentageOff().abs().longValue() ?1:-1;
         }
       }
//      }
    }

    // Currently, the way that deal *should* be ordered is unknown... so I'm
    // just making something that will impose an order on refresh so that when
    // the proper deal ordering algorithm is known, all that will be required is
    // to change this method.
    dif = System.identityHashCode(left) - System.identityHashCode(right);
    if (dif != 0) {
      return (dif / Math.abs(dif));
    }
    return 0;
  }
}
