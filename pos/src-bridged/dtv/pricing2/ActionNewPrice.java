//$Id: ActionNewPrice.java 730 2013-06-24 19:45:19Z dtvdomain\bli $
package dtv.pricing2;

import java.math.BigDecimal;

import dtv.pricing2.DealRefreshStrategy.PricingAction;

public final class ActionNewPrice
    implements DealAction {

  protected long newPrice = 0;

  /**
   * Create a price substitution action which will cause the price of an item matched to change.
   * 
   * @param argNewPrice New price (translated by the item field adapter)
   */
  public ActionNewPrice(long argNewPrice) {
    newPrice = argNewPrice;
  }

  /** {@inheritDoc} */
  @Override
  public final long apply(long price, long qty, long qtyOne, Item<?> theLine, long argActionArgQty) {
    /* We need to calculate a correction factor to correct for the error introduced by the integer 
     * division in the return line below.  We use mod math instead of BigDecimal and rounding in an 
     * effort to maintain good performance.  */
    long correction = (((newPrice * qtyOne) % argActionArgQty) << 1 > argActionArgQty) ? 1 : 0;
    return (((newPrice * qtyOne / argActionArgQty) + correction - price) * qty / qtyOne);
  }

  /** {@inheritDoc} */
  @Override
  public PricingAction getActionEnum() {
    return DealRefreshStrategy.PricingAction.NEW_PRICE;
  }

  /** {@inheritDoc} */
  @Override
  public int getActionOrdinal() {
    final int ord = DealRefreshStrategy.PricingAction.NEW_PRICE.ordinal();
    return ord;
  }

  @Override
  public String toString() {
    return ("= $" + new BigDecimal(newPrice).movePointLeft(2));
  }
  
  public long getNewPrice() {
    return newPrice;
  }
  
}