// $Id: PricingDeal.java 1252 2018-04-26 20:12:37Z johgaug $
package dtv.pricing2;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

import dtv.util.CompositeObject;
import dtv.util.CompositeObject.TwoPiece;

/**
 * Deal representation. This is the abstract (logically) representation of a single deal, complete with the
 * information which must be satisfied to bring about its results in the transaction. Subclassing this deal is
 * vital to the operation of the pricing engine in any environment, since the engine, and related class have
 * no knowledge about the native transactions, application of deals, or item descriptions. Also, the values
 * stored at this level should <b>never</b> be changed once the deal has been added to the dealspace, except
 * by the deal engine itself; this may cause the deals to be improperly ordered.<br>
 * <br>
 *
 * Copyright (c) 2007 Datavantage Corporation
 *
 * @author gfischer
 * @created June 1, 2006
 * @version $Revision: 1252 $
 * @param <D> Native deal type
 */
public class PricingDeal<D>
  implements Serializable {

  protected final D nativeDeal;
  protected final PricingItemAdapter<?> iAdapter;

  protected transient final PricingAdapter<?, D, ?> adapter;
  protected int priorityNudge;

  protected final Collection<String> triggers;
  protected final LocalDateTime effectiveDate;
  protected final LocalDateTime endDate;
  protected final Collection<TwoPiece<LocalTime, LocalTime>> effectiveTimes;

  /**
   * Deals excluded by inclusion of this deal. Deal exclusions must be filled with other deals from the global
   * dealspace. This may require a second step in the collection of the deals -- maybe even an identification
   * of some kind for the deals until this is populated.<br>
   * Since the deals are indelible, this must refer to the native deal that the deals contains.
   */
  protected final Object[] exclusions;

  /**
   * Matching of items for which this deal is applicable. Since these will have been properly run on the items
   * in the transaction before the results of the deal are computed, any changes to the actual items can be
   * done on the result items of this set. Which ensures that the correctly matched item is used for
   * computation.
   */
  protected final ItemMatch[] itemSet;

  /**
   * A bitmask created with a number of ones equal to the number of item matches. Any matching algorithm
   * should use a bitmask to ensure that all item matches have been matched, this can then be tested for
   * equality against that mask to verify that all item matches have been satisfied at least once.
   */
  protected final int matchBase;

  /** Minimum threshold for transaction-wide rule. */
  protected final long subtotalMin;
  /** Maximum threshold for transaction-wide test. */
  protected final long subtotalMax;
  /** Actions that this deal takes in regards to the pricing of the deal. */
  protected final DealAction[] itemActions;
  /** An action which corresponds to the whole transaction */
  protected final DealAction transAction;
  /** Putting a cap on the number of times a deal may be applied to a transaction. Default = 1. */
  protected final int iterationCap;

  /**
   * Put a cap on the total generosity of the deal: the amount it may change the transaction before it becomes
   * ineligible. This value should always be negative, since the cummulative differences should be used
   * instead of the absolute value to avoid the extra conditionals.
   */
  protected final long generosityCap;

  /**
   * A key identifying which must be matched by a deal space in order to maintain its membership in it.
   * Non-targeted deals will always retain such membership, but a targeted deal will only do so if the
   * transaction/deal space recognizes the target advertised by the deal.
   */
  protected final String target;
  protected final boolean deferred;
  protected final boolean substitute;
  protected final boolean higherNonactionAmt, excludePriceOverride, excludeDiscounted;
  protected final boolean nonactionPriceOrderDescendent, actionPriceOrderDescendent;
  protected final long qtyOne;

  private final Enum<?> PRICE, QTY, ALLOW_DEAL, SALE_ITEM_TYPE_ONLY, PRICE_OVERRIDE, DISCOUNTED;
  protected final int QTY_IDX, PRICE_IDX, ALLOW_DEAL_IDX, SALE_ITEM_TYPE_ONLY_IDX, PRICE_OVERRIDE_IDX,
    DISCOUNTED_IDX;

  private static final long serialVersionUID = 1;

  @SuppressWarnings("unchecked")
  private static <I> Item<I>[][] newItemMatrix(int x, int y) {
    return new Item[x][y];
  }

  /**
   * It is impossible to create a deal which does not derrive from a native deal type. This is the most basic
   * constructor allowed.
   *
   * @param argNativeDeal Native deal which this proxy is deemed to represent.
   */
  public PricingDeal(PricingAdapter<?, D, ?> argAdapter, D argNativeDeal) {
    this.PRICE = argAdapter.field("PRICE");
    this.QTY = argAdapter.field("QUANTITY");
    this.ALLOW_DEAL = argAdapter.field("ALLOW_DEAL");
    this.SALE_ITEM_TYPE_ONLY = argAdapter.field("SALE_ITEM_TYPE_ONLY");
    this.PRICE_OVERRIDE = argAdapter.field("PRICE_OVERRIDE");
    this.DISCOUNTED = argAdapter.field("DISCOUNTED");

    this.QTY_IDX = QTY.ordinal();
    this.PRICE_IDX = PRICE.ordinal();
    this.ALLOW_DEAL_IDX = ALLOW_DEAL.ordinal();
    this.SALE_ITEM_TYPE_ONLY_IDX = SALE_ITEM_TYPE_ONLY.ordinal();
    this.PRICE_OVERRIDE_IDX = PRICE_OVERRIDE.ordinal();
    this.DISCOUNTED_IDX = DISCOUNTED.ordinal();

    this.adapter = argAdapter;
    this.iAdapter = argAdapter.getItemAdapter();
    this.qtyOne = iAdapter.translateItemField(QTY, BigDecimal.ONE);

    this.effectiveDate = null;
    this.endDate = null;
    this.effectiveTimes = null;
    this.triggers = null;
    this.iterationCap = 1;
    this.generosityCap = Long.MIN_VALUE;
    this.exclusions = null;
    this.itemSet = null;
    this.itemActions = null;
    this.transAction = null;
    this.subtotalMin = Long.MIN_VALUE;
    this.subtotalMax = Long.MAX_VALUE;
    this.matchBase = 0;
    this.deferred = false;
    this.substitute = false;
    this.actionPriceOrderDescendent = false;
    this.nonactionPriceOrderDescendent = false;
    this.higherNonactionAmt = false;
    this.excludePriceOverride = false;
    this.excludeDiscounted = false;
    this.nativeDeal = argNativeDeal;
    this.priorityNudge = 0;
    this.target = null;
  }

  protected PricingDeal(LocalDateTime argEffectiveDate, LocalDateTime argEndDate,
                        Collection<TwoPiece<LocalTime, LocalTime>> argEffectiveTimes, Collection<String> argTriggers,
                        int argIterationCap, long argGenerosityCap, Object[] argExclusions, ItemMatch[] argItemSet,
                        DealAction[] argItemActions, DealAction argTransAction, long argSubtotalMin, long argSubtotalMax,
                        int argMatchBase, D argNativeDeal, PricingAdapter<?, D, ?> argAdapter, boolean defer, int nudge,
                        boolean argSubstitute, boolean argActionPriceOrderDescendent, boolean argNonactionPriceOrderDescendent,
                        boolean argHigherNonactionAmt, boolean argExcludePriceOverride, boolean argExcludeDiscounted,
                        String argTarget) {

    this.PRICE = argAdapter.field("PRICE");
    this.QTY = argAdapter.field("QUANTITY");
    this.ALLOW_DEAL = argAdapter.field("ALLOW_DEAL");
    this.SALE_ITEM_TYPE_ONLY = argAdapter.field("SALE_ITEM_TYPE_ONLY");
    this.PRICE_OVERRIDE = argAdapter.field("PRICE_OVERRIDE");
    this.DISCOUNTED = argAdapter.field("DISCOUNTED");

    this.QTY_IDX = QTY.ordinal();
    this.PRICE_IDX = PRICE.ordinal();
    this.ALLOW_DEAL_IDX = ALLOW_DEAL.ordinal();
    this.SALE_ITEM_TYPE_ONLY_IDX = SALE_ITEM_TYPE_ONLY.ordinal();
    this.PRICE_OVERRIDE_IDX = PRICE_OVERRIDE.ordinal();
    this.DISCOUNTED_IDX = DISCOUNTED.ordinal();

    this.effectiveDate = argEffectiveDate;
    this.endDate = argEndDate;
    this.effectiveTimes = argEffectiveTimes;
    this.exclusions = argExclusions;
    this.generosityCap = argGenerosityCap;
    this.itemActions = argItemActions;
    this.itemSet = argItemSet;
    this.iterationCap = argIterationCap;
    this.subtotalMax = argSubtotalMax;
    this.subtotalMin = argSubtotalMin;
    this.transAction = argTransAction;
    this.triggers = argTriggers;
    this.matchBase = argMatchBase;
    this.nativeDeal = argNativeDeal;

    this.adapter = argAdapter;
    this.iAdapter = argAdapter.getItemAdapter();
    this.deferred = defer;
    this.qtyOne = iAdapter.translateItemField(QTY, BigDecimal.ONE);
    this.priorityNudge = nudge;
    this.substitute = argSubstitute;
    this.actionPriceOrderDescendent = argActionPriceOrderDescendent;
    this.nonactionPriceOrderDescendent = argNonactionPriceOrderDescendent;
    this.higherNonactionAmt = argHigherNonactionAmt;
    this.excludePriceOverride = argExcludePriceOverride;
    this.excludeDiscounted = argExcludeDiscounted;
    this.target = argTarget;

  }

  protected PricingDeal(LocalDateTime argEffectiveDate, LocalDateTime argEndDate, LocalTime argStartTime,
                        LocalTime argEndTime, Collection<String> argTriggers, int argIterationCap, long argGenerosityCap,
                        Object[] argExclusions, ItemMatch[] argItemSet, DealAction[] argItemActions, DealAction argTransAction,
                        long argSubtotalMin, long argSubtotalMax, int argMatchBase, D argNativeDeal,
                        PricingAdapter<?, D, ?> argAdapter, boolean defer, int nudge, boolean argSubstitute,
                        boolean argActionPriceOrderDescendent, boolean argNonactionPriceOrderDescendent,
                        boolean argHigherNonactionAmt, boolean argExcludePriceOverride, boolean argExcludeDiscounted,
                        String argTarget) {

    this(argEffectiveDate, argEndDate, Collections.singleton(CompositeObject.make(argStartTime, argEndTime)),
      argTriggers, argIterationCap, argGenerosityCap, argExclusions, argItemSet, argItemActions,
      argTransAction, argSubtotalMin, argSubtotalMax, argMatchBase, argNativeDeal, argAdapter, defer,
      nudge, argSubstitute, argActionPriceOrderDescendent, argNonactionPriceOrderDescendent,
      argHigherNonactionAmt, argExcludePriceOverride, argExcludeDiscounted, argTarget);
  }

  /**
   * Adds the specified start/end time range to the set of times during which this deal is effective.
   *
   * @param argStartTime the earliest time in this range during which this deal is effective
   * @param argEndTime the latest time in this range during which this deal is effective
   */
  public void addEffectiveTimeRange(LocalTime argStartTime, LocalTime argEndTime) {
    effectiveTimes.add(CompositeObject.make(argStartTime, argEndTime));
  }

  /**
   * Apply the deal to the transaction described. This method should not make changes to the items directly,
   * only to <tt>prices</tt>, <tt>locallyConsumed</tt> and <tt>consumed</tt>. This method may assume that
   * 'isEligible' was called immediately before.
   *
   * @param myIdx Index of this deal in the dealstack (I don't remember why this is useful)
   * @param items Items in the transaction
   * @param s StackFrame (borrowed from the EngineBestDeal) used for application in this context.
   * @return Difference in the overall transaction's subtotal.
   */
  public long apply(int myIdx, Item<?>[] items, StackFrame<?, D> s) {
    int i, tMatch;
    DealAction act;
    long temp = 0, difference = 0, hold = 0;

    System.arraycopy(getLatestItemPrice(s),0, s.prices, 0, s.itemCount);

    // Apply whole-transaction deals.
    if (transAction != null) {
      // Calculate the correct subtotal first
      long tempSubtotal = s.subtotal;

      for (i = 0; i < items.length; i++ ) {
        Item<?> item = items[i];

        /* The legacy logic which directly decremented the subtotal -- on the assumption that whatever was
         * "lost" would be restored on the back end by Engine -- isn't going to fly. If this is a "deferred"
         * deal (i.e. a bounce-back coupon), then the subtotal is not re- incremented by the amount returned
         * by this method; the "deferredTotal" is. Because the workings of this thing are so arcane, it's
         * difficult to make a move here without jeopardizing the whole mystical operation. But here goes... */
        if (excludeCurrentItem(item, false)) {
          tempSubtotal -= item.fields[PRICE.ordinal()] * item.fields[QTY.ordinal()] / qtyOne;
        }
        else if ((itemSet != null) && (itemSet.length > 0)) {
          for (ItemMatch match : itemSet) {
            if (!match.testItem(item)) {
              tempSubtotal -= item.fields[PRICE.ordinal()] * item.fields[QTY.ordinal()] / qtyOne;
              break;
            }
          }
        }
      }

      if ((subtotalMax < 0) && (subtotalMin < 0)) {
        difference = transAction.apply(tempSubtotal, (-1 * qtyOne), qtyOne, null, qtyOne);
      }
      else {
        difference = transAction.apply(tempSubtotal, qtyOne, qtyOne, null, qtyOne);
      }

      s.generosity += difference;

      // If this causes a spill, just go as far as we can.
      if (s.generosity < generosityCap) {
        difference = s.generosity = generosityCap;
      }

      // Now apply this difference throughout the items.
      // (remember that the difference is recorded as negative.
      hold = difference;

      /* Now that we've run that gauntlet, adjust the "live" subtotal if we're not working with a deferred
       * deal. */
      if (!s.deal.deferred) {
        s.subtotal = tempSubtotal;
      }

      for (i = 0; i < items.length; i++ ) {
        Item<?> item = items[i];

        // Filter out the item that doesn't allow discount or deal
        if (excludeCurrentItem(item, true)) {
          continue;
        }
        else if ((itemSet != null) && (itemSet.length > 0)) {
          boolean found = false;
          for (ItemMatch match : itemSet) {
            if (!match.testItem(item)) {
              found = true;
              break;
            }
          }

          if (found) {
            continue;
          }
        }

        // If the deal in question is deferred, do not apply changes to the prices now
        if (!s.deal.deferred) {
          if (isLastMatch(items, i)) {
            s.priceDelta[i] = hold;
            s.prices[i] += s.priceDelta[i];
            hold = 0;
          }
          else {
            temp = (s.subtotal == 0) ? 0 : s.prices[i] * difference / s.subtotal;

            /* Perform a brute-force rounding here so that extra rounding pennies are evenly distributed
             * across all deal lines in a transaction-wide deal rather than being allotted entirely to a
             * single deal line. */
            if (s.subtotal != 0) {
              long tenfoldResult = s.prices[i] * difference * 10 / s.subtotal;
              if (-tenfoldResult % 10 >= 5) {
                temp -= 1;
              }
            }

            s.priceDelta[i] = temp;
            s.prices[i] += s.priceDelta[i];
            hold -= temp;
          }
        }
      }
    }
    // Apply deals.
    Item<?> item;
    long qty;
    int m;

    if ((itemSet != null) && (itemSet.length > 0)) {
      // See if there is any NEW_PRICE action with multiple action argument. This is for rounding issue.
      boolean[] multipleQtyNewPrice = new boolean[itemActions.length];
      long[] newPriceCount = new long[itemActions.length];

      if ((itemActions != null) && (itemActions.length > 0)) {
        for (int k = 0; k < itemSet.length; k++ ) {
          if ((itemActions[k] != null) && (itemSet[k]._actionArgQty > 1)
            && (itemActions[k] instanceof ActionNewPrice) && (itemSet[k]._maxQty > 0)
            && (itemSet[k]._maxQty == itemSet[k]._minQty)) {
            multipleQtyNewPrice[k] = true;
          }
          else {
            multipleQtyNewPrice[k] = false;
          }
        }
      }

      for (i = 0; i < items.length; i++ ) {
        item = items[i];

        // Filter out the item that doesn't allow discount or deal
        if (excludeCurrentItem(item, false)) {
          continue;
        }

        qty = item.fields[QTY_IDX];

        // act=null;

        for (m = 0; m < itemSet.length; m++ ) {

          if ((s.applications[i] < qty) && (s.matched[i][m] > 0)) {
            // Apply the deal's item action to the item.


            if ((itemActions != null) && ((act = itemActions[m]) != null)) {

              // Test for Substitute action eligibility
              if ((act != null) && (act instanceof SubstituteDealAction)) {
                if (!((SubstituteDealAction) act).isEligible(s.prices[i], s.matched[i][m], qtyOne, item)) {
                  continue;
                }
              }

              if (s.generosity <= generosityCap) {
                // No more applications, start erasing our tracks.
                s.matched[i][m] = 0;
              }
              else {
                // System.out.println("Applying to qty of "+ s.matched[i][m]);
                temp =
                  act.apply(s.prices[i], s.matched[i][m], qtyOne, item, itemSet[m]._actionArgQty);
//act.apply(item.fields[PRICE_IDX], s.matched[i][m], qtyOne, item, itemSet[m]._actionArgQty);  -- solve discount from the orginal price to the discounted price

                if (multipleQtyNewPrice[m]) {
                  for (long k = 0; k < s.matched[i][m]; k += qtyOne) {
                    // Changed usage of newPriceCount to always be scaled, instead of sometimes scaled and
                    // sometimes not.
                    newPriceCount[m] += qtyOne;

                    if (Math.IEEEremainder(newPriceCount[m], itemSet[m]._actionArgQty) == 0) {
                      long totalPrice = ((ActionNewPrice) act).newPrice;
                      long remain =
                        Double.valueOf(Math.IEEEremainder(totalPrice, itemSet[m]._actionArgQty / qtyOne))
                          .longValue();
                      temp += remain;
                    }
                  }

                  if (newPriceCount[m] + s.matched[i][m] > itemSet[m]._actionArgQty) {
                    newPriceCount[m] += s.matched[i][m];
                  }
                }

                temp = ((temp * -1) > s.prices[i]) ? s.prices[i] * -1 : temp;
              }
              if (s.generosity + temp < generosityCap) { // (Remember generosity is negative)
                s.priceDelta[i] = temp = generosityCap - s.generosity;
              }
              //award should be always negative number
              if(temp < 0){
                difference += temp;
                s.generosity += temp;
                if (!deferred) {
                  // Don't apply price changes for deferred deals.
                  s.prices[i] += temp;
                }
              }
            }
            // apply and consume.
            long usedQty = s.matched[i][m];
            s.applications[i] += usedQty;
            //if the award is not negative,then the item is not consumed and can be used by other deals
            if(difference < 0){
              s.consumed[i] += (itemSet[m]._consumable) ? usedQty : 0;
            }
          }
        }
      }
    }

    // Change the dealset with impunity, since it comes off a stack.
    tMatch = s.deals.length;
    if (exclusions != null) {
      for (Object ex : exclusions) {
        for (i = myIdx; i < tMatch; i++ ) {
          if ((s.deals[i] != null) && (s.deals[i].nativeDeal == ex)) {
            s.deals[i] = null;
          }
        }
      }
    }

    return difference;
  }

  /**
   * Test for collision of two deals. If the deals will consume any of the same items, or explicitly excludes
   * the other, then the deals are said to collide. It must be true that a <tt>left.collides(left)</tt> will
   * be true if the deal consumes any items; it must also be true that if <tt>left.collides(right)</tt> that
   * <tt>right.collides(left)</tt> must also be true.
   *
   * @param right The right-hand deal against which to test collision.
   * @return true if the deals may be competing for items or application.
   */
  public boolean collides(PricingDeal<?> right) {
    boolean collision = false;

    // Does left exclude right?
    if (this.exclusions != null) {
      for (Object ex : this.exclusions) {
        collision |= (ex == right.nativeDeal);
      }
    }

    // Does right exclude left?
    if (right.exclusions != null) {
      for (Object ex : right.exclusions) {
        collision |= (ex == this.nativeDeal);
      }
    }

    // Do left or right have intersecting item matchings?
    if ((this.itemSet != null) && (right.itemSet != null)) {
      for (ItemMatch iLeft : this.itemSet) {
        for (ItemMatch iRight : right.itemSet) {
          collision |= iLeft.intersects(iRight);
        }
      }
    }

    return collision;
  }

  public boolean excludeCurrentItem(Item<?> argItem, boolean argCheckReturn) {
    // exclude the return item
    if (argCheckReturn && (argItem.fields[PRICE_IDX] < 0)) {
      return true;
    }

    // If this item configured not allowing discount, exclude it.
    if (argItem.fields[ALLOW_DEAL_IDX] == 0) {
      return true;
    }

    // If this deal configured to exclude price override or discounted item, check them.
    if ((this.excludePriceOverride && (argItem.fields[PRICE_OVERRIDE_IDX] == 1))
      || (this.excludeDiscounted && (argItem.fields[DISCOUNTED_IDX] == 1))) {
      return true;
    }

    return false;
  }

  public DealAction[] getDealAction() {
    return itemActions;
  }

  // These are all the way down here because they are one of the least important
  // parts of the deal. These getters are not used in the deal engine itself.
  public LocalDateTime getEffectiveDate() {
    return effectiveDate;
  }

  /**
   * Returns all effective time ranges assigned to this deal.
   * @return all effective time ranges assigned to this deal
   */
  public Collection<? extends TwoPiece<LocalTime, LocalTime>> getEffectiveTimes() {
    if (effectiveTimes == null) {
      return Collections.emptyList();
    }
    return effectiveTimes;
  }

  public LocalDateTime getEndDate() {
    return endDate;
  }

  public int getIterationCap() {
    return iterationCap;
  }

  public D getNativeDeal() {
    return nativeDeal;
  }

  public long getSubtotalMax() {
    return subtotalMax;
  }

  public long getSubtotalMin() {
    return subtotalMin;
  }

  public String getTarget() {
    return target;
  }

  public DealAction getTransAction() {
    return transAction;
  }

  public Collection<String> getTriggers() {
    return triggers;
  }

  public boolean isDeferred() {
    return deferred;
  }

  public <I> boolean isEligible(Item<I>[] items, StackFrame<I, D> s) {
    // Match subtotal. (Innocent until proven guilty)
    boolean passed = ((s.subtotal >= subtotalMin) && (s.subtotal < subtotalMax));
    passed &= s.generosity > generosityCap; // Remember: genCap is negative

    // Match items
    if (passed && (itemSet != null) && (itemSet.length > 0)) {
      int itemCount = items.length, matchers = itemSet.length;

      for (int m = 0; m < matchers; m++ ) {
        s.matchItQty[m] = 0;
        s.matchTotals[m] = 0;
        for (int k = 0; k < itemCount; k++ ) {
          s.matched[k][m] = 0;
        }
      }

      // 1. Separate the match rules by action
      ItemMatch[] actionRule = new ItemMatch[matchers];
      ItemMatch[] nonActionRule = new ItemMatch[matchers];

      for (int m = 0; m < matchers; m++ ) {
        ItemMatch match = itemSet[m];

        if (itemActions[m] != null) {
          actionRule[m] = match;
        }
        else {
          nonActionRule[m] = match;
        }
      }

      long[] lineItemQtys = new long[itemCount];
      for (int i = 0; i < itemCount; i++ ) {
        if (excludeCurrentItem(items[i], true)) {
          continue;
        }

        lineItemQtys[i] =
          items[i].fields[QTY_IDX]
            - (s.consumed[i] > s.applications[i] ? s.consumed[i] : s.applications[i]);
      }

      // 2. First group line items by match rules.
      Item<I>[][] matchGroupItems = newItemMatrix(matchers, itemCount);

      long matchRuleAvailableQty[] = new long[matchers];

      refreshAvailableQty(matchers, itemCount, items, matchGroupItems, matchRuleAvailableQty, s);

      // 2a. Take the action rules and see what the absolute minimum required item quantities are.
      long[] itemAppliedAmounts =
        getFutureMinRequiredQuantity(actionRule, matchRuleAvailableQty, items, matchGroupItems,
          lineItemQtys,s);

      matchGroupItems = newItemMatrix(matchers, itemCount);

      matchRuleAvailableQty = new long[matchers];

      // 2b. Use the minimum quantities to generate a new available quantity to be used for the non-action
      // rules
      refreshAvailableQty(matchers, itemCount, items, matchGroupItems, matchRuleAvailableQty, s,
        itemAppliedAmounts);

      // 3. Loop through non action match rules first and identify total number of qty applicable for this
      // group of line items.
      // 4. Update the match qty on the action group line items by applying to the most expensive first

      matchDealWithinMatchRule(nonActionRule, matchRuleAvailableQty, matchGroupItems, lineItemQtys, items, s,
        true);
      
      
  /*  work for the lowest price
      matchDealWithinMatchRule(actionRule, matchRuleAvailableQty, matchGroupItems, lineItemQtys, items, s,
          false);
      */


      // 5. First group line items by match rules.
      matchGroupItems = newItemMatrix(matchers, itemCount);

      matchRuleAvailableQty = new long[matchers];

      refreshAvailableQty(matchers, itemCount, items, matchGroupItems, matchRuleAvailableQty, s);

      // 6. Loop through action match rules and identify total number of qty applicable for this group of line
      // items.
      // 7. Update the match qty on the NON action group line items by applying to the most expensive first
      if (this.higherNonactionAmt) {
        
        /*
        matchDealWithinMatchRule(actionRule, matchRuleAvailableQty, matchGroupItems, lineItemQtys, items, s,
          true, nonActionRule);
        */


        matchDealWithinMatchRule(actionRule, matchRuleAvailableQty, matchGroupItems, lineItemQtys, items, s,
          true, nonActionRule);


      }
      else {


        matchDealWithinMatchRule(actionRule, matchRuleAvailableQty, matchGroupItems, lineItemQtys, items, s,
          false);
       

        /* work for the lowest
        matchDealWithinMatchRule(nonActionRule, matchRuleAvailableQty, matchGroupItems, lineItemQtys, items, s,
            false);
        */


      }
    }
    else {
      return passed;
    }

    return matchSetTest(items, s) > 0;
  }

  public boolean isExcludeDiscounted() {
    return excludeDiscounted;
  }

  public boolean isExcludePriceOverride() {
    return excludePriceOverride;
  }

  public boolean isSubstitute() {
    return substitute;
  }

  /**
   * Clears all effective time ranges previously assigned to this deal.
   */
  public void removeEffectiveTimes() {
    effectiveTimes.clear();
  }

  public PricingDeal<D> setActionPriceOrderDescendent(boolean argFlag) {
    return new PricingDeal<D>(effectiveDate, endDate, effectiveTimes, triggers, iterationCap, generosityCap,
      exclusions, itemSet, itemActions, transAction, subtotalMin, subtotalMax, matchBase, this.nativeDeal,
      adapter, deferred, priorityNudge, substitute, argFlag, nonactionPriceOrderDescendent,
      higherNonactionAmt, excludePriceOverride, excludeDiscounted, target);
  }

  public PricingDeal<D> setDeferred(boolean defer) {
    return new PricingDeal<D>(effectiveDate, endDate, effectiveTimes, triggers, iterationCap, generosityCap,
      exclusions, itemSet, itemActions, transAction, subtotalMin, subtotalMax, matchBase, this.nativeDeal,
      adapter, defer, priorityNudge, substitute, actionPriceOrderDescendent, nonactionPriceOrderDescendent,
      higherNonactionAmt, excludePriceOverride, excludeDiscounted, target);
  }

  /**
   * Set the effective date of the deal.
   *
   * @param argEffectiveDate Effective date, or null for effective immediately.
   * @see #getEffectiveDate()
   */
  public PricingDeal<D> setEffectiveDate(LocalDateTime argEffectiveDate) {
    return new PricingDeal<D>(argEffectiveDate, endDate, effectiveTimes, triggers, iterationCap,
      generosityCap, exclusions, itemSet, itemActions, transAction, subtotalMin, subtotalMax, matchBase,
      this.nativeDeal, adapter, deferred, priorityNudge, substitute, actionPriceOrderDescendent,
      nonactionPriceOrderDescendent, higherNonactionAmt, excludePriceOverride, excludeDiscounted, target);
  }

  /**
   * Set the effective times of the deal.
   *
   * @param argEffectiveTimes effective times, or null for effective all day long
   * @see {@link #getEffectiveTimes()}
   */
  public PricingDeal<D> setEffectiveTimes(Collection<TwoPiece<LocalTime, LocalTime>> argEffectiveTimes) {
    return new PricingDeal<D>(effectiveDate, endDate, argEffectiveTimes, triggers, iterationCap,
      generosityCap, exclusions, itemSet, itemActions, transAction, subtotalMin, subtotalMax, matchBase,
      this.nativeDeal, adapter, deferred, priorityNudge, substitute, actionPriceOrderDescendent,
      nonactionPriceOrderDescendent, higherNonactionAmt, excludePriceOverride, excludeDiscounted, target);
  }

  /**
   * Set the end date of the deal.
   *
   * @param argEndDate End date, or null for no ending.
   * @see #getEndDate()
   */
  public PricingDeal<D> setEndDate(LocalDateTime argEndDate) {
    return new PricingDeal<D>(effectiveDate, argEndDate, effectiveTimes, triggers, iterationCap,
      generosityCap, exclusions, itemSet, itemActions, transAction, subtotalMin, subtotalMax, matchBase,
      this.nativeDeal, adapter, deferred, priorityNudge, substitute, actionPriceOrderDescendent,
      nonactionPriceOrderDescendent, higherNonactionAmt, excludePriceOverride, excludeDiscounted, target);
  }

  /**
   * Set the end time for the deal. This is relative to the date(s) on which this deal is effective.
   *
   * @param argEndTime The end time in 'HHmm' int format.
   */
  public PricingDeal<D> setEndTime(LocalTime argEndTime) {
    Collection<TwoPiece<LocalTime, LocalTime>> newTimes = new ArrayList<TwoPiece<LocalTime, LocalTime>>();
    LocalTime currentStartTime = null;

    if ((effectiveTimes != null) && !effectiveTimes.isEmpty()) {
      if (effectiveTimes.size() == 1) {
        currentStartTime = effectiveTimes.iterator().next().a();
      }
    }
    newTimes.add(CompositeObject.make(currentStartTime, argEndTime));

    return new PricingDeal<D>(effectiveDate, endDate, newTimes, triggers, iterationCap, generosityCap,
      exclusions, itemSet, itemActions, transAction, subtotalMin, subtotalMax, matchBase, this.nativeDeal,
      adapter, deferred, priorityNudge, substitute, actionPriceOrderDescendent,
      nonactionPriceOrderDescendent, higherNonactionAmt, excludePriceOverride, excludeDiscounted, target);
  }

  public PricingDeal<D> setExcludeDiscounted(boolean argFlag) {
    return new PricingDeal<D>(effectiveDate, endDate, effectiveTimes, triggers, iterationCap, generosityCap,
      exclusions, itemSet, itemActions, transAction, subtotalMin, subtotalMax, matchBase, this.nativeDeal,
      adapter, deferred, priorityNudge, substitute, actionPriceOrderDescendent,
      nonactionPriceOrderDescendent, higherNonactionAmt, excludePriceOverride, argFlag, target);
  }

  public PricingDeal<D> setExcludePriceOverride(boolean argFlag) {
    return new PricingDeal<D>(effectiveDate, endDate, effectiveTimes, triggers, iterationCap, generosityCap,
      exclusions, itemSet, itemActions, transAction, subtotalMin, subtotalMax, matchBase, this.nativeDeal,
      adapter, deferred, priorityNudge, substitute, actionPriceOrderDescendent,
      nonactionPriceOrderDescendent, higherNonactionAmt, argFlag, excludeDiscounted, target);
  }

  /**
   * Set the list of deals excluded by this deal.
   *
   * @param argExclusions deal exclusions.
   */
  public PricingDeal<D> setExclusions(PricingDeal<D>... argExclusions) {
    Object[] l_exclusions = new Object[argExclusions.length];
    for (int i = 0; i < argExclusions.length; i++ ) {
      l_exclusions[i] = argExclusions[i].nativeDeal;
    }
    return new PricingDeal<D>(effectiveDate, endDate, effectiveTimes, triggers, iterationCap, generosityCap,
      l_exclusions, itemSet, itemActions, transAction, subtotalMin, subtotalMax, matchBase,
      this.nativeDeal, adapter, deferred, priorityNudge, substitute, actionPriceOrderDescendent,
      nonactionPriceOrderDescendent, higherNonactionAmt, excludePriceOverride, excludeDiscounted, target);
  }

  public PricingDeal<D> setGenerosityCap(BigDecimal argGenerosityCap) {
    long genCap = 0;
    if (argGenerosityCap != null) {
      genCap = -iAdapter.translateItemField(PRICE, argGenerosityCap);
    }
    return new PricingDeal<D>(effectiveDate, endDate, effectiveTimes, triggers, iterationCap, genCap,
      exclusions, itemSet, itemActions, transAction, subtotalMin, subtotalMax, matchBase, this.nativeDeal,
      adapter, deferred, priorityNudge, substitute, actionPriceOrderDescendent,
      nonactionPriceOrderDescendent, higherNonactionAmt, excludePriceOverride, excludeDiscounted, target);
  }

  public PricingDeal<D> setHigherNonactionAmt(boolean argFlag) {
    return new PricingDeal<D>(effectiveDate, endDate, effectiveTimes, triggers, iterationCap, generosityCap,
      exclusions, itemSet, itemActions, transAction, subtotalMin, subtotalMax, matchBase, this.nativeDeal,
      adapter, deferred, priorityNudge, substitute, actionPriceOrderDescendent,
      nonactionPriceOrderDescendent, argFlag, excludePriceOverride, excludeDiscounted, target);
  }

  /**
   * Set the actions which this deal performs. Each action must have the same index in the array that the item
   * matching constraint given has.
   *
   * @param argItemActions Array of deal actions.
   */
  public PricingDeal<D> setItemActions(DealAction... argItemActions) {
    return new PricingDeal<D>(effectiveDate, endDate, effectiveTimes, triggers, iterationCap, generosityCap,
      exclusions, itemSet, argItemActions, transAction, subtotalMin, subtotalMax, matchBase,
      this.nativeDeal, adapter, deferred, priorityNudge, substitute, actionPriceOrderDescendent,
      nonactionPriceOrderDescendent, higherNonactionAmt, excludePriceOverride, excludeDiscounted, target);
  }

  /**
   * Set the list of item matches.
   *
   * @param argItemSet array of item matches.
   */
  public PricingDeal<D> setItemSet(ItemMatch... argItemSet) {
    // Set up the desired bitmask.
    int l_matchBase = 0;
    if (argItemSet != null) {
      for (int i = 0; i < argItemSet.length; i++ ) {
        l_matchBase |= (1 << i);
      }
    }
    return new PricingDeal<D>(effectiveDate, endDate, effectiveTimes, triggers, iterationCap, generosityCap,
      exclusions, argItemSet, itemActions, transAction, subtotalMin, subtotalMax, l_matchBase,
      this.nativeDeal, adapter, deferred, priorityNudge, substitute, actionPriceOrderDescendent,
      nonactionPriceOrderDescendent, higherNonactionAmt, excludePriceOverride, excludeDiscounted, target);
  }

  /**
   * Set the iteration cap (maximum number of times the deal will apply)
   *
   * @param argIterationCap interation cap, or -1 for no limit
   */
  public PricingDeal<D> setIterationCap(int argIterationCap) {
    if (argIterationCap < 0) {
      return new PricingDeal<D>(effectiveDate, endDate, effectiveTimes, triggers, Integer.MAX_VALUE,
        generosityCap, exclusions, itemSet, itemActions, transAction, subtotalMin, subtotalMax, matchBase,
        this.nativeDeal, adapter, deferred, priorityNudge, substitute, actionPriceOrderDescendent,
        nonactionPriceOrderDescendent, higherNonactionAmt, excludePriceOverride, excludeDiscounted, target);
    }
    return new PricingDeal<D>(effectiveDate, endDate, effectiveTimes, triggers, argIterationCap,
      generosityCap, exclusions, itemSet, itemActions, transAction, subtotalMin, subtotalMax, matchBase,
      this.nativeDeal, adapter, deferred, priorityNudge, substitute, actionPriceOrderDescendent,
      nonactionPriceOrderDescendent, higherNonactionAmt, excludePriceOverride, excludeDiscounted, target);
  }

  public PricingDeal<D> setNonactionPriceOrderDescendent(boolean argFlag) {
    return new PricingDeal<D>(effectiveDate, endDate, effectiveTimes, triggers, iterationCap, generosityCap,
      exclusions, itemSet, itemActions, transAction, subtotalMin, subtotalMax, matchBase, this.nativeDeal,
      adapter, deferred, priorityNudge, substitute, actionPriceOrderDescendent, argFlag,
      higherNonactionAmt, excludePriceOverride, excludeDiscounted, target);
  }

  public PricingDeal<D> setPriorityNudge(int nudge) {
    return new PricingDeal<D>(effectiveDate, endDate, effectiveTimes, triggers, iterationCap, generosityCap,
      exclusions, itemSet, itemActions, transAction, subtotalMin, subtotalMax, matchBase, nativeDeal,
      adapter, deferred, nudge, substitute, actionPriceOrderDescendent, nonactionPriceOrderDescendent,
      higherNonactionAmt, excludePriceOverride, excludeDiscounted, target);
  }

  /**
   * Set the start time for the deal. This is relative to the date(s) on which this deal is effective.
   *
   * @param argStartTime The start time in 'HHmm' int format.
   */
  public PricingDeal<D> setStartTime(LocalTime argStartTime) {
    Collection<TwoPiece<LocalTime, LocalTime>> newTimes = new ArrayList<TwoPiece<LocalTime, LocalTime>>();
    LocalTime currentEndTime = null;

    if ((effectiveTimes != null) && !effectiveTimes.isEmpty()) {
      if (effectiveTimes.size() == 1) {
        currentEndTime = effectiveTimes.iterator().next().b();
      }
    }
    newTimes.add(CompositeObject.make(argStartTime, currentEndTime));

    return new PricingDeal<D>(effectiveDate, endDate, newTimes, triggers, iterationCap, generosityCap,
      exclusions, itemSet, itemActions, transAction, subtotalMin, subtotalMax, matchBase, this.nativeDeal,
      adapter, deferred, priorityNudge, substitute, actionPriceOrderDescendent,
      nonactionPriceOrderDescendent, higherNonactionAmt, excludePriceOverride, excludeDiscounted, target);
  }

  public PricingDeal<D> setSubstitute(boolean argSub) {
    return new PricingDeal<D>(effectiveDate, endDate, effectiveTimes, triggers, iterationCap, generosityCap,
      exclusions, itemSet, itemActions, transAction, subtotalMin, subtotalMax, matchBase, this.nativeDeal,
      adapter, deferred, priorityNudge, argSub, actionPriceOrderDescendent, nonactionPriceOrderDescendent,
      higherNonactionAmt, excludePriceOverride, excludeDiscounted, target);
  }

  /**
   * Set subtotal maximum threshold.
   *
   * @param argSubtotalMax The maximum boundry of the threshold, in true currency.
   */
  public PricingDeal<D> setSubtotalMax(BigDecimal argSubtotalMax) {
    long l_subtotalMax = Long.MAX_VALUE;
    if (argSubtotalMax != null) {
      l_subtotalMax = iAdapter.translateItemField(PRICE, argSubtotalMax);
    }
    return new PricingDeal<D>(effectiveDate, endDate, effectiveTimes, triggers, iterationCap, generosityCap,
      exclusions, itemSet, itemActions, transAction, subtotalMin, l_subtotalMax, matchBase,
      this.nativeDeal, adapter, deferred, priorityNudge, substitute, actionPriceOrderDescendent,
      nonactionPriceOrderDescendent, higherNonactionAmt, excludePriceOverride, excludeDiscounted, target);
  }

  /* ** GETTERS ** */

  /**
   * Set subtotal minimum threshold.
   *
   * @param argSubtotalMin The minimum subtotal amount, in true currency.
   */
  public PricingDeal<D> setSubtotalMin(BigDecimal argSubtotalMin) {
    long l_subtotalMin = 0;
    if (argSubtotalMin != null) {
      l_subtotalMin = iAdapter.translateItemField(PRICE, argSubtotalMin);
    }
    return new PricingDeal<D>(effectiveDate, endDate, effectiveTimes, triggers, iterationCap, generosityCap,
      exclusions, itemSet, itemActions, transAction, l_subtotalMin, subtotalMax, matchBase,
      this.nativeDeal, adapter, deferred, priorityNudge, substitute, actionPriceOrderDescendent,
      nonactionPriceOrderDescendent, higherNonactionAmt, excludePriceOverride, excludeDiscounted, target);
  }

  public PricingDeal<D> setTarget(String argTarget) {
    return new PricingDeal<D>(effectiveDate, endDate, effectiveTimes, triggers, iterationCap, generosityCap,
      exclusions, itemSet, itemActions, transAction, subtotalMin, subtotalMax, matchBase, this.nativeDeal,
      adapter, deferred, priorityNudge, substitute, actionPriceOrderDescendent,
      nonactionPriceOrderDescendent, higherNonactionAmt, excludePriceOverride, excludeDiscounted, argTarget);
  }

  public PricingDeal<D> setTransAction(DealAction argTtransAction) {
    return new PricingDeal<D>(effectiveDate, endDate, effectiveTimes, triggers, iterationCap, generosityCap,
      exclusions, itemSet, itemActions, argTtransAction, subtotalMin, subtotalMax, matchBase,
      this.nativeDeal, adapter, deferred, priorityNudge, substitute, actionPriceOrderDescendent,
      nonactionPriceOrderDescendent, higherNonactionAmt, excludePriceOverride, excludeDiscounted, target);
  }

  /**
   * Set the deal's triggers.
   *
   * @param argTriggers Deal trigger, or null for untriggered deals.
   */
  public PricingDeal<D> setTriggers(Collection<String> argTriggers) {
    return new PricingDeal<D>(effectiveDate, endDate, effectiveTimes, argTriggers, iterationCap,
      generosityCap, exclusions, itemSet, itemActions, transAction, subtotalMin, subtotalMax, matchBase,
      this.nativeDeal, adapter, deferred, priorityNudge, substitute, actionPriceOrderDescendent,
      nonactionPriceOrderDescendent, higherNonactionAmt, excludePriceOverride, excludeDiscounted, target);
  }

  /**
   * Set the deal's triggers. This method is the same as the above, but allows for the use of arrays and/or
   * hand-added triggers.
   *
   * @param argTriggers Triggers which must be added to the pricing transaction to activate this deal.
   * @return The new, immutable deal object.
   */
  public PricingDeal<D> setTriggers(String... argTriggers) {
    return setTriggers(Arrays.asList(argTriggers));
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    return nativeDeal.toString();
  }

  protected boolean isLastMatch(Item<?>[] items, int i) {
    for (int j = i + 1; j < items.length; j++ ) {
      if (!excludeCurrentItem(items[j], true)) {
        return false;
      }
    }
    return true;
  }

  private boolean distributeDealCount(long matchRuleAppliedQty[], StackFrame<?, D> argS, int m, int i,
                                      long[] argLineItemQtys, Item<?>[] items) {

    boolean minItemTotalBasedDeal = (itemSet[m]._minTotal > 0);
    if ((minItemTotalBasedDeal) && (argS.matchTotals[m] >= itemSet[m]._minTotal)) {
      return false;
    }

    final long matchQty =
      (argLineItemQtys[i] >= matchRuleAppliedQty[m]) ? matchRuleAppliedQty[m] : argLineItemQtys[i];
    final long divide = (items[i].fields[QTY_IDX] - argS.applications[i]) / qtyOne;
    final long price = (divide == 0) ? 0 : (argS.prices[i] / divide * matchQty / qtyOne);

    argLineItemQtys[i] -= matchQty;
    matchRuleAppliedQty[m] -= matchQty;

    argS.matchTotals[m] += price;
    argS.matched[i][m] = matchQty;

    return (minItemTotalBasedDeal) ? (argS.matchTotals[m] >= itemSet[m]._minTotal) : (matchQty > 0);
  }

  private void distributeDealLines(long matchRuleAppliedQty[], StackFrame<?, D> s, long[] argLineItemQtys,
                                   Item<?>[] items, Item<?>[][] argMatchGroupItem, ItemMatch[] argRuleMap) {
    for (int m = 0; m < argRuleMap.length; m++ ) {
      if (argRuleMap[m] == null) {
        continue;
      }

      boolean found = false;
      if ((items != null) && (items.length > 0)) {
        for (int i = items.length - 1; i >= 0; i-- ) {
          if (matchRuleAppliedQty[m] == 0) {
            break;
          }

          if (argMatchGroupItem[m][i] == null) {
            continue;
          }

          if (distributeDealCount(matchRuleAppliedQty, s, m, i, argLineItemQtys, items)) {
            found = true;
          }

        }

        if (found) {
          s.matchItQty[m] += qtyOne;
        }
      }
    }
  }

  /**
   * A method to get the minimum required item quanties that are needed by the passed rule map.
   *
   * @param <I>
   * @param argRuleMap the map of item rules that are being investigated.
   * @param argMatchRuleAvailableQty the current available quantity that can be applied to an individual rule.
   * @param argItems the list of items that are currently being considered for deals.
   * @param argMatchGroupItem a matrix of items that can be applied to argRuleMap where the first index maps
   * to argRuleMap and the second index maps to argItems
   * @param argLineItemQtys the current quantities of the line items this array is parallel to argItems
   * @return an array that represents the minimum quantity required by argRuleMap
   */
  private <I> long[] getFutureMinRequiredQuantity(ItemMatch[] argRuleMap, long argMatchRuleAvailableQty[],
                                                  Item<I>[] argItems, Item<I> argMatchGroupItem[][], long argLineItemQtys[],StackFrame<I, D> s) {
    long futureMinAppliedQty[] = new long[itemSet.length];
    long[] itemReservedQuantity = new long[argItems.length];
    long[] temporaryItemQuantities = new long[argLineItemQtys.length];
    //boolean flag =  hasItemsPriceChanged(s);
    int itemIdx=-1;

    // copy the current item quantities out into a temporary array so we don't have any undesired side
    // effects.
    System.arraycopy(argLineItemQtys, 0, temporaryItemQuantities, 0, argLineItemQtys.length);

    for (int m = 0; m < argRuleMap.length; m++ ) {
      ItemMatch match = argRuleMap[m];
      if ((match != null) && (match._minQty <= argMatchRuleAvailableQty[m])) {
        futureMinAppliedQty[m] = match._minQty;
      }
    }
    long itemPrices[] = getLatestItemPrice(s);

    // Go through each method in the list and cycle through the available items that could be applied to this
    // method starting from the first (cheapest). The resulting array should represent the absolute minimum
    // quantity required to be applied for the rules passed.
    for (int method = 0; method < argRuleMap.length; method++ ) {

      //the even position is more expensive than another
      for (int item = 0; item < argItems.length; item++ ) {

      /*  
      for (int item = argItems.length - 1; item >=0; item-- ) {
        */

        if (argMatchGroupItem[method][item] != null) {
          long appAmount =
            (futureMinAppliedQty[method] >= temporaryItemQuantities[item]) ? temporaryItemQuantities[item]
              : futureMinAppliedQty[method];

          if(appAmount >0 && isLineItemDeal() && itemPrices[item] > 0){
            itemIdx = getMostPossibleItemIdx(s,futureMinAppliedQty,temporaryItemQuantities,method,item,argRuleMap,argItems);
            if(itemIdx != -1){
              futureMinAppliedQty[method] -= appAmount;
              temporaryItemQuantities[itemIdx] -= appAmount;
              itemReservedQuantity[itemIdx] += appAmount;
            }
            appAmount = 0;
          }else{
            futureMinAppliedQty[method] -= appAmount;
            temporaryItemQuantities[item] -= appAmount;
            itemReservedQuantity[item] += appAmount;
          }
        }
      }
    }

    return itemReservedQuantity;
  }

  private long getMaxDealEligibleSet(long[] argMatchItQty) {
    long maxMatchSet = 99999999;
    int matchers = itemSet.length;

    for (int i = 0; i < matchers; i++ ) {
      maxMatchSet = Math.min(maxMatchSet, argMatchItQty[i]);
    }

    return maxMatchSet;
  }

  private <I> void matchDealWithinMatchRule(ItemMatch[] argRuleMap, long argMatchRuleAvailableQty[],
                                            Item<I>[][] argMatchGroupItem, long[] argLineItemQtys, Item<I>[] items, StackFrame<I, D> s,
                                            boolean argOrderAscending) {

    matchDealWithinMatchRule(argRuleMap, argMatchRuleAvailableQty, argMatchGroupItem, argLineItemQtys, items,
      s, argOrderAscending, null);
  }

  private <I> void matchDealWithinMatchRule(ItemMatch[] argRuleMap, long argMatchRuleAvailableQty[],
                                            Item<I>[][] argMatchGroupItem, long[] argLineItemQtys, Item<I>[] items, StackFrame<I, D> s,
                                            boolean argOrderDescending, ItemMatch[] argSuppliedRuleMap) {

    int matchers = itemSet.length;

    // Loop through NON-action or action match rules and identify total number of qty applicable for this
    // group of line items.
    long matchRuleAppliedQty[] = new long[matchers];

    List<PriceOrderListElement> priceOrderList = new ArrayList<PriceOrderListElement>(s.prices.length);
    for (int i=0; i<s.prices.length;i ++) {
      priceOrderList.add(new PriceOrderListElement(i, s.prices[i]));
    }

    if (argOrderDescending) {
      //get the items price from high to low
      Collections.sort(priceOrderList, new PriceHighToLower());
    }
    else {
      //get the items price from low to high
      Collections.sort(priceOrderList, new PriceLowToHigh());
    }

    if ((argRuleMap != null) && (argRuleMap.length > 0)) {
      for (int m = 0; m < argRuleMap.length; m++ ) {
        ItemMatch match = argRuleMap[m];

        if (match == null) {
          continue;
        }

        if ((argMatchRuleAvailableQty[m] >= match._minQty) && (argMatchRuleAvailableQty[m] <= match._maxQty)
          && (match._maxQty > match._minQty)) {
          // All available qty is applicable
          matchRuleAppliedQty[m] = argMatchRuleAvailableQty[m];
        }
        else if ((match._minQty == match._maxQty) && (argMatchRuleAvailableQty[m] >= match._minQty)) {
          // Available qty will be either min or max qty
          matchRuleAppliedQty[m] = match._minQty;
        }
        else if ((match._maxQty > match._minQty) && (argMatchRuleAvailableQty[m] >= match._minQty)) {
          // available qty will be the match qty.
          matchRuleAppliedQty[m] = match._maxQty;
        }
      }

      // Update the match qty on the action group line items by applying to the most expensive
      if (argSuppliedRuleMap == null) {
        // Inside this if statement: don't care about if total non action amount needs to be more than total
        // action amount.
        for (int m = 0; m < argRuleMap.length; m++ ) {
          if (argRuleMap[m] == null) {
            continue;
          }

          boolean found = false;

          if ((items != null) && (items.length > 0)) {
            /*if (!argOrderDescending) {
              for (int i = 0; i < items.length; i++ ) {
                if (matchRuleAppliedQty[m] == 0) {
                  break;
                }

                if (argMatchGroupItem[m][i] == null) {
                  continue;
                }

                if (isBestLowestPriceItemToApply(i,m,s,matchRuleAppliedQty,argLineItemQtys,argRuleMap,items,argMatchGroupItem) && distributeDealCount(matchRuleAppliedQty, s, m, i, argLineItemQtys, items)) {
                  found = true;
                }
              }
            }
            else {
              for (int i = items.length - 1; i >= 0; i-- ) {
                if (matchRuleAppliedQty[m] == 0) {
                  break;
                }

                if (argMatchGroupItem[m][i] == null) {
                  continue;
                }

                if (isBestHighestPriceItemToApply(i,m,s,matchRuleAppliedQty,argLineItemQtys,argRuleMap,items,argMatchGroupItem) && distributeDealCount(matchRuleAppliedQty, s, m, i, argLineItemQtys, items)) {
                  found = true;
                }
              }
            }*/

            for (PriceOrderListElement element :  priceOrderList ) {
              if (matchRuleAppliedQty[m] == 0) {
                break;
              }

              if (argMatchGroupItem[m][element.getIndex()] == null) {
                continue;
              }

              if (!argOrderDescending) {
                if (isBestLowestPriceItemToApply(element.getIndex(),m,s,matchRuleAppliedQty,argLineItemQtys,argRuleMap,items,argMatchGroupItem) && distributeDealCount(matchRuleAppliedQty, s, m, element.getIndex(), argLineItemQtys, items)) {
                  found = true;
                }
              }
              else {
                if (isBestHighestPriceItemToApply(element.getIndex(),m,s,matchRuleAppliedQty,argLineItemQtys,argRuleMap,items,argMatchGroupItem) && distributeDealCount(matchRuleAppliedQty, s, m, element.getIndex(), argLineItemQtys, items)) {
                  found = true;
                }
              }

            }

            if (found) {
              s.matchItQty[m] += qtyOne;
            }
          }
        }
      }
      else {
        // Inside this if statement:
        // 1. DO care about if total non action amount needs to be more than total action amount.
        // 2. And now is trying to distribute the non action qty to line item.
        StackFrame<I, D> sCopy = StackFrame.createFirstFrame(adapter, s.deals, items, qtyOne);

        if (!argOrderDescending) {
          // If non action line items are sort by amount in descending order and the first test fail, it
          // fails. No need to try different combination.
          sCopy.shadow(s);

          distributeDealLines(matchRuleAppliedQty, sCopy, argLineItemQtys, items, argMatchGroupItem,
            argRuleMap);

          boolean validation = validateActionNNonactionAmount(sCopy, argRuleMap, argSuppliedRuleMap);
          if (validation) {
            s.shadow(sCopy);
          }
        }
        else {
          // If non action line items are sort by amount in ascending order, once the validation failed, need
          // to
          // start over and loop through all line items except for the first(cheapest) one.

          boolean validation = false;
          int lineItemCount = items.length;
          // int index = 0;
          long[] newLineItemQtys = new long[items.length];
          System.arraycopy(argLineItemQtys, 0, newLineItemQtys, 0, items.length);

          long[] newMatchRuleAppliedQtys = new long[argRuleMap.length];
          System.arraycopy(matchRuleAppliedQty, 0, newMatchRuleAppliedQtys, 0, argRuleMap.length);

          while (!validation && (lineItemCount > 0)) {
            // Create a copy of the current line item quantities so that we can see if our current
            // set of items is valid without blowing away the knowledge of what we've already tried.
            long[] testAppLineItemQtys = new long[items.length];
            System.arraycopy(newLineItemQtys, 0, testAppLineItemQtys, 0, items.length);

            sCopy.clone(s);

            distributeDealLines(newMatchRuleAppliedQtys, sCopy, testAppLineItemQtys, items,
              argMatchGroupItem, argRuleMap);

            validation = validateActionNNonactionAmount(sCopy, argRuleMap, argSuppliedRuleMap);
            if (validation) {
              s.clone(sCopy);
              System.arraycopy(newMatchRuleAppliedQtys, 0, matchRuleAppliedQty, 0, argRuleMap.length);
              System.arraycopy(newLineItemQtys, 0, argLineItemQtys, 0, items.length);

              break;
            }
            else {
              lineItemCount-- ;

              // pop the next most expensive item out of our list so we can try again.
              newLineItemQtys =
                removeFirstActionQty(matchRuleAppliedQty, newLineItemQtys, items, argMatchGroupItem,
                  argRuleMap);
              System.arraycopy(matchRuleAppliedQty, 0, newMatchRuleAppliedQtys, 0, argRuleMap.length);
            }
          }
        }
      }
    }
  }


  protected <I> boolean isBestLowestPriceItemToApply(int i,int m,StackFrame<I, D> s,long matchRuleAppliedQty[],long[] argLineItemQtys,ItemMatch[] argRuleMap,Item<I>[] items,Item<I>[][] argMatchGroupItem){
    boolean flag = true;
    boolean tempFlag = false;
    boolean achieveFlag = false;

    List<Integer> execludeIdx = new ArrayList<Integer>();
    int tempIdx = 0;
    int lowestPriceIdx = tempIdx;

    lowestPriceIdx = getCurrentLowestPriceIdx(s,execludeIdx,argRuleMap,items,argLineItemQtys);
    tempFlag = testMatchQty(i,m,s,matchRuleAppliedQty,argLineItemQtys);

    if(!isLineItemDeal()){
      return flag;
    }

    if(lowestPriceIdx == i && tempFlag){
      return flag;  // no change,continue to use previous logic
    }else{
      while(tempIdx < items.length && lowestPriceIdx>=0){
        tempFlag = testMatchQty(lowestPriceIdx,m,s,matchRuleAppliedQty,argLineItemQtys);
        achieveFlag = isBestItemAchieveable(s,argMatchGroupItem,argRuleMap,items,lowestPriceIdx);
        if(tempFlag && achieveFlag){
          flag = false;
          break;
        }else{
          execludeIdx.add(lowestPriceIdx);
          lowestPriceIdx = getCurrentLowestPriceIdx(s,execludeIdx,argRuleMap,items,argLineItemQtys);
        }
        tempIdx++;
      }
    }

    if(isCurrencyOffCoupon(s) && flag && lowestPriceIdx<0){
      flag = false;
    }
    return flag;
  }


  protected <I> boolean isBestHighestPriceItemToApply(int i,int m,StackFrame<I, D> s,long matchRuleAppliedQty[],long[] argLineItemQtys,ItemMatch[] argRuleMap,Item<I>[] items,Item<I>[][] argMatchGroupItem){
    boolean flag = true;
    boolean tempFlag = false;
    boolean achieveFlag = false;

    List<Integer> execludeIdx = new ArrayList<Integer>();
    long itemPrices[] = getLatestItemPrice(s);
    int tempIdx = itemPrices.length-1;
    int highestPriceIdx = getCurrentHighestPriceIdx(s,execludeIdx,argRuleMap,items,argLineItemQtys);

    tempFlag = testMatchQty(i,m,s,matchRuleAppliedQty,argLineItemQtys);

    if(!isLineItemDeal()){
      return flag;
    }

    if((highestPriceIdx == i) && tempFlag){
      return flag;  // the current item is the lowest item,continue to use the prevous logic
    }else{
      while(tempIdx >=0 && highestPriceIdx >=0){
        tempFlag = testMatchQty(highestPriceIdx,m,s,matchRuleAppliedQty,argLineItemQtys);
        achieveFlag = isBestItemAchieveable(s,argMatchGroupItem,argRuleMap,items,highestPriceIdx);
        if(tempFlag && achieveFlag){
          flag = false;
          break;
        }else{
          execludeIdx.add(highestPriceIdx);
          highestPriceIdx = getCurrentHighestPriceIdx(s,execludeIdx,argRuleMap,items,argLineItemQtys);
        }
        tempIdx--;
      }
    }
    return flag;
  }


  protected <I> boolean testMatchQty(int i,int m,StackFrame<I, D> s,long matchRuleAppliedQty[],long[] argLineItemQtys){
    long matchQty =
      (argLineItemQtys[i] >= matchRuleAppliedQty[m]) ? matchRuleAppliedQty[m] : argLineItemQtys[i];
    return matchQty>0;
  }

  protected <I> int getCurrentLowestPriceIdx(StackFrame<I, D> s,List<Integer> list,ItemMatch[] argRuleMap,Item<I>[] items,long[] argLineItemQtys){
    long[] priceArray = getLatestItemPrice(s);
    long lowestPrice=0;
    int idx=-1,k=0;

    for(int j=0;j< priceArray.length;j++){
      if(list!=null && list.contains(Integer.valueOf(j))) {
        continue;
      }
      if(checkItemsWithRule(argRuleMap,items[j]) && argLineItemQtys[j]>0 && checkDealUsableForCurrentItem(s,j)){
        if(k == 0 && priceArray[j] >0){
          lowestPrice = priceArray[j];
          idx = j;
          k++;
          continue;
        }

        if(priceArray[j]<lowestPrice && priceArray[j] > 0){
          lowestPrice = priceArray[j];
          idx = j;
        }
      }
    }
    return idx;
  }


  protected <I> int getCurrentHighestPriceIdx(StackFrame<I, D> s,List<Integer> list,ItemMatch[] argRuleMap,Item<I>[] items,long[] argLineItemQtys){
    long[] priceArray = getLatestItemPrice(s);
    long highestPrice=0;
    int idx = -1,k=0;

    for(int j = priceArray.length-1 ;j >=0;j--){
      if(list!=null && list.contains(Integer.valueOf(j))) {
        continue;
      }

      if(checkItemsWithRule(argRuleMap,items[j]) && argLineItemQtys[j]>0){

        ///even though the price is zero,but this item is still eligible as a trigger item
     /* if(priceArray[j] == 0){
        idx = j;
        return idx;
      }*/

        if(k == 0 && priceArray[j] > 0 ){
          highestPrice = priceArray[j];
          idx = j;
          k++;
          continue;
        }

        if(priceArray[j] > highestPrice){
          highestPrice = priceArray[j];
          idx = j;
        }
      }
    }
    return idx;
  }


  protected <I> long[] getLatestItemPrice(StackFrame<I, D> s){
    boolean flag = false;
    long[] priceArray = s.prices;
    long[] newPriceArray = s.newPrices;
    boolean isNewPricesZero = true;

    for(int i=0;i<newPriceArray.length;i++){
      if(newPriceArray[i]!=0){
        isNewPricesZero = false;
        break;
      }
    }

    if(isNewPricesZero){
      return priceArray;
    }

    for(int i=0;i<priceArray.length;i++){
      if(newPriceArray[i]>=0 && newPriceArray[i] < priceArray[i]){
        flag = true;
        break;
      }
    }
    return flag ? newPriceArray:priceArray;
  }



  protected <I> int getMostPossibleItemIdx(StackFrame<I, D> s,long futureMinAppliedQty[],long[] temporaryItemQuantities,int m,int n,ItemMatch[] argRuleMap,Item<I>[] argItems){
    int bestItemIdx = -1;
    long qtyAmount = -1;
    long[] latestItemPrice = getLatestItemPrice(s);
    long bestItemValue = -1;
    int itemsLength = argItems.length;
    long tempItemValue= -1;
    boolean flag = isCurrencyOffCoupon(s);


    if(flag){
      bestItemIdx = getMostPossibleItemIdxForCurrencyOffCoupon(s,futureMinAppliedQty,temporaryItemQuantities,m,argRuleMap,argItems);
    }else{
      for (int idx = 0; idx < itemsLength; idx++ ) {

        qtyAmount =
          (futureMinAppliedQty[m] >= temporaryItemQuantities[idx]) ? temporaryItemQuantities[idx]
            : futureMinAppliedQty[m];
        tempItemValue = latestItemPrice[idx];
        if(qtyAmount>0 && checkItemsWithRule(argRuleMap,argItems[idx]) && tempItemValue>0){
          if(bestItemValue == -1){
            bestItemValue = tempItemValue;
            bestItemIdx = idx;
            continue;
          }
          if(tempItemValue < bestItemValue){
            bestItemValue = tempItemValue;
            bestItemIdx = idx;
          }
        }
      }
    }
    return bestItemIdx;
  }


  protected <I> boolean checkItemsWithRule(ItemMatch[] argRuleMap,Item<I> item){
    boolean flag = false;
    for(ItemMatch match:argRuleMap){
      if(match == null ){
        continue;
      }else{
        flag = match.testItem(item);
      }
    }
    return flag;
  }



  protected <I> boolean isItemEligibleForCurrencyOffCoupon(StackFrame<I, D> s,int n,long[] latestItemPrice){
    boolean flag = false;
    for(DealAction act : itemActions){
      if(act instanceof ActionCurrencyOff){
        if(Math.abs(((ActionCurrencyOff) act).amountOff) <= latestItemPrice[n]){
          flag = true;
          break;
        }
      }
    }
    return flag;
  }

  protected <I> boolean isCurrencyOffCoupon(StackFrame<I, D> s){
    boolean flag = false;
    if(s.deal.getTriggers()!=null && s.deal.getTriggers().size()>0){
      for(DealAction act : itemActions){
        if(act instanceof ActionCurrencyOff){
          flag = true;
          break;
        }
      }
    }
    return flag;
  }

  protected <I> boolean checkDealUsableForCurrentItem(StackFrame<I, D> s,int i){
    boolean flag = true;
    if(isCurrencyOffCoupon(s)){
      flag = isItemEligibleForCurrencyOffCoupon(s,i,getLatestItemPrice(s));
    }
    return flag;
  }



  protected <I> int getMostPossibleItemIdxForCurrencyOffCoupon(StackFrame<I, D> s,long futureMinAppliedQty[],long[] temporaryItemQuantities,int m,ItemMatch[] argRuleMap,Item<I>[] argItems){
    int bestItemIdx = -1;
    long qtyAmount = -1;
    long[] latestItemPrice = getLatestItemPrice(s);
    long bestItemValue = -1;
    long tempItemValue= -1;

    for (int idx = 0; idx < argItems.length; idx++ ) {
      qtyAmount =
        (futureMinAppliedQty[m] >= temporaryItemQuantities[idx]) ? temporaryItemQuantities[idx]
          : futureMinAppliedQty[m];

      tempItemValue = latestItemPrice[idx];
      if(qtyAmount>0 && checkItemsWithRule(argRuleMap,argItems[idx]) && isItemEligibleForCurrencyOffCoupon(s,idx,latestItemPrice) && tempItemValue>0){
        if(bestItemValue == -1){
          bestItemValue = tempItemValue;
          bestItemIdx = idx;
          continue;
        }
        if(tempItemValue < bestItemValue){
          bestItemValue = tempItemValue;
          bestItemIdx = idx;
        }
      }
    }

    return bestItemIdx;
  }



  protected <I> boolean isBestItemAchieveable(StackFrame<I, D> s,Item<I>[][] argMatchGroupItem,ItemMatch[] argRuleMap,Item<I>[] items,int itemIdx){
    boolean flag = false;
    // long[] latestItemPrice = getLatestItemPrice(s);
    int idx=-1;

    for(int m=0;m<argRuleMap.length;m++){
      if (argRuleMap[m] == null) {
        continue;
      }
      //reset the idx to avoid ArrayIndexOutOfBoundsException
      if(idx != -1){
        idx = -1;
      }
      for(int i=0;i<items.length;i++){
        if(argMatchGroupItem[m][i] == null){
          continue;
        }
      /* 
       if(latestItemPrice[i] == 0){
          continue;
        }
       */
        idx++;
        if(idx == itemIdx){
          flag = true;
          return flag;
        }
      }
    }
    return flag;
  }


  protected <I> boolean isLineItemDeal(){
    boolean flag = true;
    if(transAction != null){
      flag = false;
    }
    return flag;
  }


  private long matchSetTest(Item<?>[] argItems, StackFrame<?, D> s) {
    long maxMatchSet = getMaxDealEligibleSet(s.matchItQty);

    for (int i = 0; i < itemSet.length; i++ ) {
      if (s.matchItQty[i] > maxMatchSet) {
        long diff = s.matchItQty[i] - maxMatchSet;
        s.matchItQty[i] -= diff;

        for (int j = argItems.length - 1; (j >= 0) && (diff > 0); j-- ) {
          if (s.matched[j][i] < diff) {
            diff -= s.matched[j][i];
            s.matched[j][i] = 0;
          }
          else {
            s.matched[j][i] -= diff;
            diff = 0;
          }
        }
      }
    }
    return maxMatchSet;
  }

  private <I> void refreshAvailableQty(int argMatchers, int argItemCount, Item<I>[] argItems,
                                       Item<I>[][] argMatchGroupItems, long argMatchRuleAvailableQty[], StackFrame<I, D> s) {
    refreshAvailableQty(argMatchers, argItemCount, argItems, argMatchGroupItems, argMatchRuleAvailableQty, s,
      null);
  }

  /**
   * Refreshes the available quantities that can be applied to a rule set.
   *
   * @param <I>
   * @param argMatchers the number of {@link ItemMatch} rules being applied
   * @param argItemCount the number of <code>Item</code> objects that the <code>ItemMatch</code> rules are
   * applying to.
   * @param argItems the list of {@link Item} objects that the <code>ItemMatch</code> rules are being applied
   * to.
   * @param argMatchGroupItems a matrix of items that could be applied to a list of <code>ItemMatch</code>
   * rules defined by the instance variable <code>itemSet</code>.(<i>ALTERED BY METHOD SIDE EFFECT</i>)
   * @param argMatchRuleAvailableQty the available quantities that can be applied to the rules defined in the
   * instance variable <code>itemSet</code>.(<i>ALTERED BY METHOD SIDE EFFECT</i>)
   * @param s the current <code>StackFrame</code>
   * @param argReservedItemQuantities
   */
  private <I> void refreshAvailableQty(int argMatchers, int argItemCount, Item<I>[] argItems,
                                       Item<I>[][] argMatchGroupItems, long argMatchRuleAvailableQty[], StackFrame<I, D> s,
                                       long[] argReservedItemQuantities) {

    long[] totalItemMatch = new long[argItemCount];

    // go through the stack and add up all of the item quantities already matched
    for (int m = 0; m < argMatchers; m++ ) {
      for (int i = 0; i < argItemCount; i++ ) {
        totalItemMatch[i] += s.matched[i][m];
      }
    }

    for (int m = 0; m < argMatchers; m++ ) {
      ItemMatch match = itemSet[m];

      for (int i = 0; i < argItemCount; i++ ) {
        if (excludeCurrentItem(argItems[i], true)) {
          continue;
        }

        // Test for Substitute action eligibility
        DealAction act = itemActions[m];
        if ((itemActions != null) && (act != null)) {
          if ((act != null) && (act instanceof SubstituteDealAction)) {
            if (!((SubstituteDealAction) act).isEligible(s.prices[i], s.matched[i][m], qtyOne, argItems[i])) {
              continue;
            }
          }
        }

        if (match.testItem(argItems[i])) {
          if ((argReservedItemQuantities == null)
            || (argReservedItemQuantities[i] - argItems[i].fields[QTY_IDX] != 0)) {
            argMatchGroupItems[m][i] = argItems[i];

            argMatchRuleAvailableQty[m] +=
              argItems[i].fields[QTY_IDX]
                - (s.consumed[i] > s.applications[i] ? s.consumed[i] : s.applications[i])
                - totalItemMatch[i]
                - ((argReservedItemQuantities != null) ? argReservedItemQuantities[i] : 0);
          }
        }
      }
    }
  }

  private long[] removeFirstActionQty(long matchRuleAppliedQty[], long[] argLineItemQtys, Item<?>[] items,
                                      Item<?>[][] argMatchGroupItem, ItemMatch[] argRuleMap) {
    boolean found = false;

    long[] newLineItemQtys = new long[items.length];
    System.arraycopy(argLineItemQtys, 0, newLineItemQtys, 0, items.length);

    for (int m = 0; (m < argRuleMap.length) && !found; m++ ) {
      if (argRuleMap[m] == null) {
        continue;
      }

      if ((items != null) && (items.length > 0)) {
        for (int i = items.length - 1; i >= 0; i-- ) {
          if (matchRuleAppliedQty[m] == 0) {
            break;
          }

          if (argMatchGroupItem[m][i] == null) {
            continue;
          }

          if (newLineItemQtys[i] == 0) {
            continue;
          }

          newLineItemQtys[i] -= this.qtyOne;
          found = true;
          break;
        }
      }
    }

    return newLineItemQtys;
  }

  private boolean validateActionNNonactionAmount(StackFrame<?, D> argS, ItemMatch[] argRuleMap,
                                                 ItemMatch[] argSuppliedRuleMap) {
    boolean result = true;

    // Need to evaluate whether or not the non action total amount is greater than the action total amount.
    // First sum up the total suppliedRuleMap line item amount.
    if ((argSuppliedRuleMap != null) && (argSuppliedRuleMap.length > 0) && (argRuleMap != null)
      && (argRuleMap.length > 0)) {
      long nonactionTotal = 0;

      for (int actionM = 0; actionM < argSuppliedRuleMap.length; actionM++ ) {
        if (argSuppliedRuleMap[actionM] == null) {
          continue;
        }

        nonactionTotal += argS.matchTotals[actionM];
      }

      long actionTotal = 0;

      for (int nonactionM = 0; nonactionM < argRuleMap.length; nonactionM++ ) {
        if (argRuleMap[nonactionM] == null) {
          continue;
        }

        actionTotal += argS.matchTotals[nonactionM];
      }

      if (nonactionTotal < actionTotal) {
        result = false;
      }
    }

    return result;
  }

  public static class DealConfigException
    extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private final PricingDeal<?> deal_;

    public DealConfigException(PricingDeal<?> deal) {
      super();
      this.deal_ = deal;
    }

    public DealConfigException(PricingDeal<?> deal, String message) {
      super(message);
      this.deal_ = deal;
    }

    public DealConfigException(PricingDeal<?> deal, String message, Throwable cause) {
      super(message, cause);
      this.deal_ = deal;
    }

    public DealConfigException(PricingDeal<?> deal, Throwable cause) {
      super(cause);
      this.deal_ = deal;
    }

    public PricingDeal<?> getDeal() {
      return deal_;
    }
  }


  protected class PriceHighToLower implements Comparator<PriceOrderListElement> {

    /** {@inheritDoc} */
    @Override
    public int compare(PriceOrderListElement leftObj, PriceOrderListElement rightObj) {
      long diff = rightObj.price_ - leftObj.price_;
      return (diff < 0) ? -1 : (diff > 0) ? 1 : 0;
    }
  }

  protected class PriceLowToHigh implements Comparator<PriceOrderListElement> {

    /** {@inheritDoc} */
    @Override
    public int compare(PriceOrderListElement leftObj, PriceOrderListElement rightObj) {
      long diff = leftObj.price_ - rightObj.price_;
      return (diff < 0) ? -1 : (diff > 0) ? 1 : 0;
    }
  }
  /**
   * Internal class to keep the the item price and the the item index.
   */
  protected class PriceOrderListElement {
    private int index_;
    private Long price_;

    public PriceOrderListElement(int argIndex, long argPrice) {
      index_ = argIndex;
      price_ = argPrice;
    }

    public int getIndex() {
      return index_;
    }

    public void setIndex(int index) {
      index_ = index;
    }

    public Long getPrice() {
      return price_;
    }

    public void setPrice(Long price) {
      price_ = price;
    }
  }

}
