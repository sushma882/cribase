//$Id: StackFrame.java 730 2013-06-24 19:45:19Z dtvdomain\bli $
package dtv.pricing2;

import java.util.Arrays;

public class StackFrame<I, D> {
  /**
   * Initialize a stack frame as the first.
   * 
   * @param deals Deals in the dealspace to work with.
   * @param items Items in the transaction.
   */
  public static <II, DD> StackFrame<II, DD> createFirstFrame(PricingAdapter<?, ?, ?> adapt,
      PricingDeal<DD>[] deals, Item<II>[] items, long qtyOne) {

    // Some of the deals may be null, so we must find the first which is non-null to get the pricing adapter.
    Enum<?> PRICE = adapt.field("PRICE");
    Enum<?> QTY = adapt.field("QUANTITY");

    int maxMatches = 0;
    for (PricingDeal<DD> deal : deals) {
      if ((deal != null) && (deal.itemSet != null) && (deal.itemSet.length > maxMatches)) {
        maxMatches = deal.itemSet.length;
      }
    }

    StackFrame<II, DD> frame = new StackFrame<II, DD>(items.length, deals.length, maxMatches, qtyOne);
    long price;

    for (int i = 0; i < frame.dealCount; i++ ) {
      frame.deals[i] = deals[i];
      frame.iterations[i] = 0;
    }
    for (int i = 0; i < frame.itemCount; i++ ) {
      price = items[i].getField(PRICE) * items[i].getField(QTY) / qtyOne;
      frame.consumed[i] = 0;
      frame.prices[i] = price;
      frame.subtotal += price;
    }
    return frame;
  }

  /**
   * Total number of deals.
   */
  protected final int dealCount;

  /**
   * Total number of items.
   */
  protected final int itemCount;

  /**
   * Maximum number of item matching rules in all available deals. Used for pre-allocating space.
   */
  protected final int maxMatches;

  /**
   * Storage of the value representing a quantity of one. Used for scaling.
   */
  protected final long qtyOne;

  /**
   * Deal. The deal to be applied in this recursion stack.
   */
  protected PricingDeal<D> deal;

  /**
   * Cumulative deal generosity. Zero per deal;
   */
  protected long generosity;

  /**
   * Subtotal of the transaction. Downward replication.
   */
  protected long subtotal;

  /**
   * Total amount offered by deferred deals.
   */
  protected long deferredTotal;

  protected final long[] priceDelta;

  /**
   * Cumulative item-rule quantity match. Zeroed per deal application.
   */
  protected final long[] matchItQty; // [maxMatches]

  /**
   * Cumulative item-rule $$ match. Zeroed per deal application.
   */
  protected final long[] matchTotals; // [maxMatches]

  /**
   * Deals. The deals used by this, and lower stack elements.
   */
  protected final PricingDeal<D>[] deals; // [dealCount]

  /**
   * Deal iterations. Zero per deal.
   */
  protected final int[] iterations; // [dealCount]

  /**
   * Global consumption. Notate items which have been consumed by this, or previous deals. Downward
   * replication.
   */
  protected final long[] consumed; // [itemCount]

  /**
   * Local applications. Notate deals which have had this deal applied to them. No replication,
   * reset at each deal level.
   */
  protected final long[] applications; // [itemCount]

  /**
   * Incremental item prices. Record and read the prices of the items as deals are applied. Downward
   * replication.
   */
  protected final long[] prices; // [itemCount]
  
  /**
   * Save the newest price for each item after apply deals
   */
  protected final long[] newPrices; // [itemCount]

  /**
   * Item-rule/item match indices. Zeroed per deal application.
   * <tt>[{@link #itemCount}][{@link #maxMatches}]</tt>
   */
  protected final long[][] matched;

  /**
   * Create a stack frame based on the parameters of an existing stack. This method should be used
   * in preference to the initial creation constructor since it will always be sure to obtain the
   * proper information from the first stack. Typically, this will be used to populate a stack after
   * the first frame has been created, and the first frame should be the argument.
   * 
   * @param simul Similar stack frame to mimic.
   */
  public StackFrame(StackFrame<I, D> simul) {
    this(simul.itemCount, simul.dealCount, simul.maxMatches, simul.qtyOne);
  }

  @SuppressWarnings("unchecked")
  private StackFrame(int argItemCount, int argDealCount, int argMaxMatches, long argQtyOne) {
    this.qtyOne = argQtyOne;
    deals = new PricingDeal[argDealCount];
    iterations = new int[argDealCount];

    consumed = new long[argItemCount];
    applications = new long[argItemCount];
    prices = new long[argItemCount];
    
    newPrices = new long[argItemCount];
    
    matched = new long[argItemCount][argMaxMatches];
    priceDelta = new long[argItemCount];

    matchItQty = new long[argMaxMatches];
    matchTotals = new long[argMaxMatches];

    this.dealCount = argDealCount;
    this.itemCount = argItemCount;
    this.maxMatches = argMaxMatches;
    this.subtotal = 0;
    this.deferredTotal = 0;
    this.generosity = 0;
  }

  public void clone(StackFrame<I, D> s) {
    System.arraycopy(s.deals, 0, deals, 0, dealCount);
    System.arraycopy(s.iterations, 0, iterations, 0, dealCount);
    System.arraycopy(s.consumed, 0, consumed, 0, itemCount);
    System.arraycopy(s.prices, 0, prices, 0, itemCount);
    System.arraycopy(s.newPrices, 0, newPrices, 0, itemCount);
    System.arraycopy(s.applications, 0, applications, 0, itemCount);
    System.arraycopy(s.priceDelta, 0, priceDelta, 0, itemCount);
    System.arraycopy(s.matched, 0, matched, 0, itemCount);
    System.arraycopy(s.matchItQty, 0, matchItQty, 0, maxMatches);
    System.arraycopy(s.matchTotals, 0, matchTotals, 0, maxMatches);

    subtotal = s.subtotal;
    deferredTotal = s.deferredTotal;
    generosity = s.generosity;
  }

  /**
   * Copy the relevent operational information from another stack frame. Note that this method makes
   * heavy use of {@link System#arraycopy(java.lang.Object, int, java.lang.Object, int, int)} which
   * is shown (in {@link TestCase_LogicSpeed}) to be vastly faster than manual copying. By contrast,
   * {@link Arrays#fill(long[], long)} has a negligible advantage over manual filling of the array;
   * since the manual filling of the array can fill more than one in a single loop in this instance,
   * manual filling is the preferred method when there are two arrays of the same size to be
   * shadowed.
   * 
   * @param s Previous stack frame to shadow.
   */
  public void shadow(StackFrame<I, D> s) {
    int i;
    System.arraycopy(s.deals, 0, deals, 0, dealCount);
    System.arraycopy(s.iterations, 0, iterations, 0, dealCount);
    System.arraycopy(s.consumed, 0, consumed, 0, itemCount);
    System.arraycopy(s.prices, 0, prices, 0, itemCount);
    for (i = 0; i < itemCount; i++ ) {
      applications[i] = 0;
      priceDelta[i] = 0;
      Arrays.fill(matched[i], 0);
    }
    for (i = 0; i < maxMatches; i++ ) {
      matchItQty[i] = 0;
      matchTotals[i] = 0;
    }

    subtotal = s.subtotal;
    deferredTotal = s.deferredTotal;
    generosity = 0;
  }
}
