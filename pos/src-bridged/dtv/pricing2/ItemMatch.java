//$Id: ItemMatch.java 777 2014-03-27 02:15:29Z suz.jliu $
package dtv.pricing2;

import java.math.BigDecimal;
import java.util.*;

/**
 * An item matching rule. Used in the deals, ItemMatch represents, and evaluates a rule for matching
 * an item.<br>
 * <br>
 * 
 * Copyright (c) 2007 Datavantage Corporation
 * 
 * @author gfischer
 * @created June 1, 2006
 * @version $Revision: 777 $
 */
public class ItemMatch {
  /**
   * If the deal holding this rule will consume the item (make it ineligible for other deals) then
   * this should be set. <i>Not used internally</i>
   */
  protected final boolean _consumable;

  /**
   * The quantity which this item rule must swallow before it is satisfied. <i>Not used
   * internally</i>
   */
  protected final long _minQty;
  /**
   * The quantity after which this deal will refuse to swallow more. <i>Not used internally</i>
   */
  protected final long _maxQty;

  /**
   * The quantity which this item action argument (dollar amount) apply for <i>Not used
   * internally</i>
   */
  protected final long _actionArgQty;
  /**
   * The minimum additive price of all items matched by this rule. This allows deals to be worded as
   * "buy $X from department Y ..." <i>Not used internally</i>
   */
  protected final long _minTotal;

  private final PricingItemAdapter<?> _iAdapt;
  private final PricingAdapter<?, ?, ?> _adapter;

  /* Field-centered item operations. Each operation in this array has the same
   * index as the field which it is meant to test. */
  private final Map<Integer, Map<Integer, FieldMatch[]>> _fieldTestsMap;
  private FieldMatch[][][] _fieldTests = null;

  private final int _fieldCount_;

  private final Enum<?>[] _values;
  private final Enum<?> _prices;
  private final Enum<?> _quantities;

  public ItemMatch(Map<Integer, Map<Integer, FieldMatch[]>> argFieldTestsMap, boolean argConsumable,
      long argMinQty, long argMaxQty, long argMinTotal, PricingAdapter<?, ?, ?> argAdapter,
      long argActionArgQty) {

    _adapter = argAdapter;
    _values = _adapter._itemFields.getEnumConstants();
    _prices = _adapter.field("PRICE");
    _quantities = _adapter.field("QUANTITY");

    _fieldCount_ = _values.length;
    _iAdapt = _adapter.getItemAdapter();

    _consumable = argConsumable;
    _fieldTests = null;
    _fieldTestsMap = argFieldTestsMap;
    _maxQty = argMaxQty;
    _minTotal = argMinTotal;
    _minQty = argMinQty;
    _actionArgQty = argActionArgQty;
  }

  /**
   * Create a new item matching rule.
   */
  public ItemMatch(PricingAdapter<?, ?, ?> argAdapter) {
    _adapter = argAdapter;
    _values = _adapter._itemFields.getEnumConstants();
    _prices = _adapter.field("PRICE");
    _quantities = _adapter.field("QUANTITY");

    _fieldCount_ = _values.length;
    _iAdapt = _adapter.getItemAdapter();

    _consumable = false;
    _fieldTestsMap = new HashMap<Integer, Map<Integer, FieldMatch[]>>();
    _fieldTests = null;
    _maxQty = _iAdapt.translateItemField(_quantities, BigDecimal.ONE);
    _minQty = 0;
    _actionArgQty = 0;
    _minTotal = 0;
  }

  /**
   * Check for Item matching intersection. If this constraint can match any of the same items that
   * <tt>right</tt> can match, the two constraints intersect.<br>
   * It is always true that the same match will intersect itself if it consumes items, and that if
   * <tt>left.intersects(right)</tt> then <tt>right.intersects(left)</tt>.
   * 
   * @param right Intersection candidate.
   * @return true if the constraints collide, false if the two matchings can coexist without
   * comprimise nicely.
   */
  public boolean intersects(ItemMatch right) {
    if ((_fieldTests != null) && (_fieldTests.length != 0) && (right._fieldTests != null)
        && (right._fieldTests.length != 0) && _consumable && right._consumable) {

      Map<Integer, List<FieldMatch>> thisList = new HashMap<Integer, List<FieldMatch>>();
      Map<Integer, List<FieldMatch>> rightList = new HashMap<Integer, List<FieldMatch>>();

      for (FieldMatch[][] condition : _fieldTests) {
        for (FieldMatch[] fieldMatch : condition) {
          for (int f = 0; f < _fieldCount_; f++ ) {
            if (fieldMatch[f] != null) {
              List<FieldMatch> list = thisList.get(f);
              if (list == null) {
                list = new ArrayList<FieldMatch>();
              }

              list.add(fieldMatch[f]);
              thisList.put(f, list);
            }
          }
        }
      }

      for (FieldMatch[][] condition : right._fieldTests) {
        for (FieldMatch[] fieldMatch : condition) {
          for (int f = 0; f < _fieldCount_; f++ ) {
            if (fieldMatch[f] != null) {
              List<FieldMatch> list = rightList.get(f);
              if (list == null) {
                list = new ArrayList<FieldMatch>();
              }

              list.add(fieldMatch[f]);
              rightList.put(f, list);
            }
          }
        }
      }

      for (Integer thisId : thisList.keySet()) {
        List<FieldMatch> t = thisList.get(thisId);
        List<FieldMatch> r = rightList.get(thisId);

        if ((t != null) && !t.isEmpty() && (r != null) && !r.isEmpty()) {
          for (FieldMatch tMatch : t) {
            for (FieldMatch rMatch : r) {
              if (tMatch.intersects(rMatch)) {
                return true;
              }
            }
          }
        }
      }
    }
    else if (_consumable && right._consumable) {
      return true;
    }
    return false;
  }

  /**
   * Set the consumable indicator. Should this item be consumed when the deal is evaluated (no other
   * consumables may use the item)
   * 
   * @param argConsumable True to consume the item after application, false otherwise.
   */
  public ItemMatch setConsumable(boolean argConsumable) {
    return new ItemMatch(_fieldTestsMap, argConsumable, _minQty, _maxQty, _minTotal, _adapter, _actionArgQty);
  }

  public ItemMatch setFieldTestEqual(Enum<?> field, Object target, int argConditionId, int argConditionSeq) {
    return setFieldTest(field, new MatchEquality(_iAdapt.translateItemField(field, target)), argConditionId,
        argConditionSeq);
  }

  public ItemMatch setFieldTestGreaterthan(Enum<?> field, Object target, int argConditionId,
      int argConditionSeq) {
    return setFieldTest(field, new MatchGreaterthan(_iAdapt.translateItemField(field, target)),
        argConditionId, argConditionSeq);
  }

  public ItemMatch setFieldTestInclusiveBound(Enum<?> field, Object lower, Object upper, int argConditionId,
      int argConditionSeq) {
    return setFieldTest(
        field,
        new MatchInclusiveBound(_iAdapt.translateItemField(field, lower), _iAdapt.translateItemField(field,
            upper)), argConditionId, argConditionSeq);
  }

  public ItemMatch setFieldTestLessthan(Enum<?> field, Object target, int argConditionId, int argConditionSeq) {
    return setFieldTest(field, new MatchLessthan(_iAdapt.translateItemField(field, target)), argConditionId,
        argConditionSeq);
  }

  public ItemMatch setFieldTestNotEqual(Enum<?> field, Object target, int argConditionId, int argConditionSeq) {
    return setFieldTest(field, new MatchInequality(_iAdapt.translateItemField(field, target)),
        argConditionId, argConditionSeq);
  }

  public ItemMatch setMinItemTotal(BigDecimal argMinTotal) {
    long l_minTotal = _iAdapt.translateItemField(_prices, argMinTotal);
    return new ItemMatch(_fieldTestsMap, _consumable, _minQty, _maxQty, l_minTotal, _adapter, _actionArgQty);
  }

  public ItemMatch setQuantity(BigDecimal argExact) {
    final String noMatch = "Cannot match 0 or fewer items.";
    long exact = _iAdapt.translateItemField(_quantities, argExact);
    if (exact <= 0) {
      throw new ItemMatchConfigException(noMatch);
    }
    return new ItemMatch(_fieldTestsMap, _consumable, exact, exact, _minTotal, _adapter, exact);
  }

  public ItemMatch setQuantity(BigDecimal min, BigDecimal max, BigDecimal argActionArgQty) {
    final String badRange = "Maximum quantity cannot be less than minimum.";
    final String noMatch = "Minimum must be greater than or equals to 0";
    long l_minQty = _iAdapt.translateItemField(_quantities, min);
    long l_maxQty = _iAdapt.translateItemField(_quantities, max);
    if (l_minQty < 0) {
      throw new ItemMatchConfigException(noMatch);
    }
    if (l_maxQty <= 0) {
      l_maxQty = Long.MAX_VALUE;
    }
    if (l_maxQty < l_minQty) {
      throw new ItemMatchConfigException(badRange);
    }

    long l_actionArtQty = 1000;
    if (argActionArgQty != null) {
      l_actionArtQty = _iAdapt.translateItemField(_quantities, argActionArgQty);
    }

    return new ItemMatch(_fieldTestsMap, _consumable, l_minQty, l_maxQty, _minTotal, _adapter, l_actionArtQty);

  }

  public ItemMatch setTestForRule(Enum<?> field, MatchRule rule, Object arg0, Object arg1,
      int argConditionId, int argConditionSeq) {
    return setFieldTest(field, forRule(field, rule, arg0, arg1), argConditionId, argConditionSeq);
  }

  /**
   * Test the item as a match for this matching rule. Since the item's price can be changed by the
   * engine during testing, the engine must be able to provide the price. Note that a rule
   * enumerated to test the price is likely intended to apply to prices *before* any changes, as a
   * result, this does not test prices as they have altered during execution of the deal engine.
   * 
   * @param item Item to test.
   * @return action indicator.
   */
  public boolean testItem(Item<?> item) {
    if (_fieldTests == null) {
      buildFieldTestsArray();
    }

    // The overall result is not matched until a condition is met
    boolean result = false;

    if ((_fieldTests != null) && (_fieldTests.length != 0)) {
      for (FieldMatch[][] condition : _fieldTests) {
        /* Different condition groups represent OR logic.  One of the conditions must be met in
         * order for the item to match. */
        // A condition is met until one of its subconditions fails to be met
        boolean conditionMet = true;
        for (FieldMatch[] fieldMatch : condition) {
          /* Within a condition group, different subconditions represent AND logic.  All
           * subconditions must be met in order for a condition to be met. */
          // A subcondition is met until one of its tests fail
          boolean subConditionMet = true;
          FieldMatch test;

          for (int i = 0; i < _fieldCount_; i++ ) {
            test = fieldMatch[i];

            if (test != null) {
              if (!test.test(item.fields[test.ordinal])) {
                subConditionMet = false;
                break;
              }
            }
          }

          if (!subConditionMet) {
            // Once a subcondition is not met, the entire condition is not met
            conditionMet = false;
            break;
          }
        }

        if (conditionMet) {
          // Once a condition is met, the item can be considered matched
          result = true;
          break;
        }
      }
    }
    else {
      // If there are no field tests, then all items match
      result = true;
    }

    return result;
  }
  
  /**
   * Test the item as a match for conditional deals.
   * @param item
   * @return
   */
  public boolean testItemEligibleForConditionalDeals(Item<?> item) {
    if (_fieldTests == null) {
      buildFieldTestsArray();
    }

    // The overall result is not matched until a condition is met
    boolean result = false;

    if ((_fieldTests != null) && (_fieldTests.length != 0)) {
      for (FieldMatch[][] condition : _fieldTests) {
        /* Different condition groups represent OR logic.
         * One of the conditions is met represent the item to match.
         * Within a condition group, different sub-conditions represent AND logic.
         * One of the sub-conditions is met represent the condition is met. 
         */
        boolean conditionMet = false;
        for (FieldMatch[] fieldMatch : condition) {
          boolean subConditionMet = false;
          FieldMatch test;

          for (int i = 0; i < _fieldCount_; i++ ) {
            test = fieldMatch[i];

            if (test != null) {
              if (test.test(item.fields[test.ordinal])) {
                subConditionMet = true;
                break;
              }
            }
          }

          if (subConditionMet) {
            // Once a sub-condition is met, the entire condition is met
            conditionMet = true;
            break;
          }
        }

        if (conditionMet) {
          // Once a condition is met, the item can be considered matched
          result = true;
          break;
        }
      }
    }
    else {
      // If there are no field tests, then all items match
      result = true;
    }

    return result;
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    StringBuffer buf = new StringBuffer();

    if (_fieldTests != null) {
      for (int id = 0; id < _fieldTests.length; id++ ) {
        buf.append("condition id:");
        buf.append(id);
        FieldMatch[][] condition = _fieldTests[id];

        for (int seq = 0; seq < condition.length; seq++ ) {
          buf.append("condition seq:");
          buf.append(seq);

          FieldMatch[] fieldMatch = condition[seq];

          for (Enum<?> f : _values) {
            if (fieldMatch[f.ordinal()] != null) {
              buf.append(f).append(fieldMatch[f.ordinal()].opString()).append(' ');
            }
          }
        }

      }
    }
    return buf.toString();
  }

  protected void buildFieldTestsArray() {
    if (_fieldTestsMap == null) {
      return;
    }

    int level1MaxCount = _fieldTestsMap.keySet().size();
    int level2MaxCount = 0;
    int level3MaxCount = _fieldCount_;

    for (Integer id : _fieldTestsMap.keySet()) {
      Map<Integer, FieldMatch[]> condition = _fieldTestsMap.get(id);

      if (level2MaxCount < condition.keySet().size()) {
        level2MaxCount = condition.keySet().size();
      }
    }

    _fieldTests = new FieldMatch[level1MaxCount][level2MaxCount][level3MaxCount];

    int i = 0;
    for (Integer id : _fieldTestsMap.keySet()) {
      Map<Integer, FieldMatch[]> condition = _fieldTestsMap.get(id);

      int j = 0;
      for (Integer seq : condition.keySet()) {
        FieldMatch[] fieldMatch = condition.get(seq);

        for (int k = 0; k < fieldMatch.length; k++ ) {
          _fieldTests[i][j][k] = fieldMatch[k];
        }
        j++ ;
      }
      i++ ;
    }
  }

  private FieldMatch forRule(Enum<?> fld, MatchRule rule, Object arg0, Object arg1) {
    if (rule != null) {
      long fld0 = _iAdapt.translateItemField(fld, arg0);

      switch (rule) {
        case EQUAL:
          return new MatchEquality(fld0);
        case GREATER:
          return new MatchGreaterthan(fld0);
        case BETWEEN:
          return new MatchInclusiveBound(fld0, _iAdapt.translateItemField(fld, arg1));
        case LESS:
          return new MatchLessthan(fld0);
        case NOT_EQUAL:
          return new MatchInequality(fld0);
      }
    }
    return null;
  }

  /* Sets the test for a field. The tester may be null, in which case the field
   * is ignored during the matching. This method is package-private, since it is
   * used for testing to insert tests which are not appropriate to the
   * implementation. */
  private ItemMatch setFieldTest(Enum<?> field, FieldMatch tester, int argConditionId, int argConditionSeq) {
    Map<Integer, FieldMatch[]> conditionMap = _fieldTestsMap.get(argConditionId);
    if (conditionMap == null) {
      conditionMap = new HashMap<Integer, FieldMatch[]>();
      _fieldTestsMap.put(argConditionId, conditionMap);
    }

    FieldMatch[] map = conditionMap.get(argConditionSeq);
    if (map == null) {
      map = new FieldMatch[_fieldCount_];
      conditionMap.put(argConditionSeq, map);
    }

    FieldMatch[] fieldTests = new FieldMatch[_values.length];

    int i = 0;
    for (Enum<?> f : _values) {
      if (f == field) {
        fieldTests[i] = tester;
        if (fieldTests[i] != null) {
          fieldTests[i].ordinal = f.ordinal();
        }
        i++ ;
      }
      else if (map != null) {
        fieldTests[i] = map[f.ordinal()];
        if (fieldTests[i] != null) {
          fieldTests[i].ordinal = f.ordinal();
        }
        i++ ;
      }
    }
    conditionMap.put(argConditionSeq, fieldTests);

    return new ItemMatch(_fieldTestsMap, _consumable, _minQty, _maxQty, _minTotal, _adapter, _actionArgQty);

  }

  /**
   * Test a single field. Implementors are logical steps in the matching of the value in a single
   * field. the test is unaware as to what field it tests, only how it must test.<br>
   * Combination tests will be better off to implement this directly, rather than try to chain the
   * existing rules together, since one class is more efficient than a chain of them trying to do
   * the same.
   */
  public static abstract class FieldMatch {
    public int ordinal = 0;

    public boolean intersects(FieldMatch test) {
      return genericIntersects(test);
    }

    public abstract String opString();

    public abstract boolean test(long value);

    protected abstract boolean genericIntersects(FieldMatch test);

    protected abstract boolean intersectsImpl(MatchEquality eq);

    protected abstract boolean intersectsImpl(MatchGreaterthan gt);

    protected abstract boolean intersectsImpl(MatchInclusiveBound ib);

    protected abstract boolean intersectsImpl(MatchInequality eq);

    protected abstract boolean intersectsImpl(MatchLessthan lt);
  }

  public enum MatchRule {
    /** Test for equality (1 argument) */
    EQUAL,
    /** Tset for inequality (1 argument) */
    NOT_EQUAL,
    /** Test for greater-than (1 argument) */
    GREATER,
    /** Test for less-than (1 argument) */
    LESS,
    /** Test for inclusively bound (2 arguments) */
    BETWEEN;

    private static TreeMap<String, MatchRule> uppers = new TreeMap<String, MatchRule>();
    static {
      for (MatchRule field : values()) {
        uppers.put(field.name().toUpperCase(), field);
      }
    }

    public static MatchRule valueOfIgnoreCase(String name) {
      return uppers.get(name.toUpperCase().trim());
    }
  }

  private static class ItemMatchConfigException
      extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ItemMatchConfigException(String message) {
      super(message);
    }
  }
}
