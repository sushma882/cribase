// $Id: Engine.java 1252 2018-04-26 20:12:37Z johgaug $
package dtv.pricing2;

import static java.math.RoundingMode.HALF_EVEN;

import java.math.BigDecimal;
import java.util.*;

import org.apache.log4j.Logger;

import dtv.util.ArrayUtils;
import dtv.util.NumberUtils;
import dtv.xst.pricing.XSTPricingDescriptor;

/**
 * A best deal pricing engine. This pricing engine will find the best deal combination by optimized brute
 * force.<br>
 * <br>
 * Copyright (c) 2007 Datavantage Corporation
 * 
 * @author gfischer
 * @created June 1, 2006
 * @version $Revision: 1252 $
 * @param <D> Native deal type
 */
class Engine<I, D> {
  private static final Logger _logger = Logger.getLogger(Engine.class);
  private static final boolean _isDebugEnabled = _logger.isDebugEnabled();

  private final Enum<?> _quantities;
  private final Enum<?> _prices;
  private final Enum<?> _dealAllowances;

  final PricingAdapter<I, D, ?> _adapter;
  /* Stores the amount which makes up a quantity of one based on the item translators. This is used for
   * comparison and scaling. */
  final long _qtyOne;

  // The source dealspace. This is the dealspace from which the engine draws its deals.
  DealSpace<D> _dealSpace = null;
  // Results and best deal combinations.
  ResultStore<I, D> _results = null;

  /* The artificial execution stack of the brute force section. Since the deepest level of recursion is known
   * before major computations are set in motion, this represents all the volatile memory which will be
   * allocated to the process. Using a pre-allocated stack, there is no need to worry about the expensive
   * creation of the arrays of data associated with the process of brute-forcing. This single, unobtrusive
   * line is the biggest reason that this pricing engine runs as quickly as it does. */
  StackFrame<I, D>[] _stack = null;

  /**
   * Constructs an <code>Engine</code>.
   * @param argAdapter an adapter to use for computing prices
   */
  public Engine(PricingAdapter<I, D, ?> argAdapter) {
    this._adapter = argAdapter;
    _quantities = _adapter.field("QUANTITY");
    _prices = _adapter.field("PRICE");
    _dealAllowances = _adapter.field("ALLOW_DEAL");
    this._qtyOne = argAdapter.getItemAdapter().translateItemField(_quantities, BigDecimal.ONE);
  }

  /**
   * Brute force through the dealspace. Using careful recursion (eventually to be unwound, but remaining truly
   * recursive at this point). No passed values may be changed, but the previous frame should be mimiced by
   * the stack element attained with the index <tt>myIdx</tt> before proceeding; this will result in a
   * workable copy of the passable information from the parent invokation.
   * 
   * @param items Items in the transaction [immutable]
   * @param myIdx Stack (and deal) index for this execution
   * @param previousFrame Previous stack frame [immutable]
   * @param dealGroup Deal collision group to test. [immutable]
   * @param argResults Results for which this brute force is working.
   * @param cummulative Used for blind deal application when the deal has been applied through another group.
   */
  public void bruteForce(Item<I>[] items, int myIdx, StackFrame<I, D> previousFrame, boolean[] dealGroup,
      ResultStore<I, D> argResults, ResultStore<I, D> cummulative) {
    int d, i;
    long temp;

    // -
    // - Group skipping and blind application.
    // -

    // Since most brute forcing is on largely empty sets, moving our index to
    // the next non-null deal in this group will cut out some of the function
    // overhead. To maintain the effects, though, there must be some level of
    // blind deal application from the previous deal groups which have been
    // evaluated.
    while ((myIdx < previousFrame.dealCount) && (!dealGroup[myIdx] || (previousFrame.deals[myIdx] == null))) {
      _stack[myIdx].shadow(previousFrame);
      previousFrame = _stack[myIdx];
      if (!dealGroup[myIdx]) {
        // blind-apply
        if ((argResults._dealAmountLevels[myIdx] < 0) || (argResults.deferredAmounts[myIdx] < 0)) {
          if (_isDebugEnabled) {
            _logger.debug("Blind-application: " + myIdx + '(' + argResults._dealAmountLevels[myIdx] + ')'
                + " deal: " + previousFrame.deals[myIdx].nativeDeal.toString());
          }
          previousFrame.subtotal += argResults._dealAmountLevels[myIdx];
          previousFrame.deferredTotal += argResults.deferredAmounts[myIdx];
          for (i = 0; i < previousFrame.itemCount; i++ ) {
            previousFrame.prices[i] += argResults._priceDeltas[myIdx][i];
          }
        }
      }
      myIdx++ ;
    }

    // -
    // - Exit criteria: Reached path's end.
    // -

    if (myIdx == previousFrame.dealCount) {
      // Debugging info for combinations tried.
      if (_isDebugEnabled) {
        _logger.debug("Tried combination: "
            + Arrays.toString(previousFrame.iterations)
            + " = "
            + previousFrame.subtotal
            + "/"
            + previousFrame.deferredTotal
            + " (best: "
            + (argResults._bestDealAmount == Long.MAX_VALUE ? "MAX" : Long
                .toString(argResults._bestDealAmount)) + "/" + argResults.bestDeferredAmount + ")");
      }

      // If this path makes a better discount, record it.
      if ((previousFrame.subtotal < argResults._bestDealAmount)
          || ((previousFrame.subtotal == argResults._bestDealAmount) && (previousFrame.deferredTotal < argResults.bestDeferredAmount))) {
        argResults._bestDealAmount = previousFrame.subtotal;
        argResults.bestDeferredAmount = previousFrame.deferredTotal;
        System.arraycopy(previousFrame.deals, 0, argResults._bestDealSet, 0, previousFrame.dealCount);
        System.arraycopy(previousFrame.iterations, 0, argResults._bestDealIterationCount, 0,
            previousFrame.dealCount);
        for (d = 0; d < previousFrame.dealCount; d++ ) {
          if (dealGroup[d]) { // Allows better interleaving and results reuse
            if ((previousFrame.deals[d] != null) && previousFrame.deals[d].deferred) {
              argResults.deferredAmounts[d] = _stack[d].generosity;
            }
            else {
              argResults._dealAmountLevels[d] = _stack[d].generosity;
            }
            for (i = 0; i < previousFrame.itemCount; i++ ) {
              System.arraycopy(_stack[d].matched[i], 0, argResults._applicationLevels[d][i], 0,
                  previousFrame.maxMatches);
            }
            System.arraycopy(_stack[d].priceDelta, 0, argResults._priceDeltas[d], 0, items.length);
          }
        }
        if (_isDebugEnabled) {
          _logger.debug("Optimal");
        }
      }

      return; // [ESCAPE]
    }

    // -
    // - No-app dealing.
    // -

    // First apply later deals without ours in the mix.
    bruteForce(items, myIdx + 1, previousFrame, dealGroup, argResults, cummulative);

    // Initialize our stack area.
    StackFrame<I, D> s = _stack[myIdx];
    s.deal = previousFrame.deals[myIdx];
    if ((!dealGroup[myIdx]) || ((s.deal = previousFrame.deals[myIdx]) == null)) {
      // Early exit criteria: No deal to apply at this level.
      return; // [ESCAPE]
    }
    
    //Before reset the price,Copy them to the new prices array for future using
     //System.arraycopy(cummulative._latestItemPrice, 0, s.newPrices, 0, s.itemCount);
    
    s.shadow(previousFrame);
    s.iterations[myIdx] = 0;

    // Apply the deal and recurse as many times as we can.
    while ((s.iterations[myIdx] < s.deal.iterationCap) && s.deal.isEligible(items, s)) {
      temp = s.deal.apply(myIdx, items, s);
      s.iterations[myIdx]++ ;
      if (s.deal.deferred) {
        s.deferredTotal += temp;
      }
      else if(temp < 0) {
        s.subtotal += temp;
      }
      if (_isDebugEnabled) {
        _logger.debug("s.iteration " + s.iterations[myIdx] + " iteration cap: " + s.deal.iterationCap
            + " myIdx: " + myIdx);
      }
      //make sure the item price in the item price array of cummulative is the newest price
      //syncItemPrice(cummulative,s);
      bruteForce(items, myIdx + 1, s, dealGroup, argResults, cummulative);
    }
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  public Result<I, D> calculateDeals(Item<I>[] items) {
    PricingDeal[] workingDS =
        filterEligibleDeals(_dealSpace.getDealSpace().toArray(new PricingDeal[0]), items);

    List<boolean[]> collisionMap = getFilteredCollisionMap(workingDS);

    return calculateBestDeals(items, ArrayUtils.removeNulls(workingDS), collisionMap);
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  public Result<I, D> calculateSubstituteDeals(Item<I>[] items) {
    PricingDeal[] workingDS =
        filterEligibleSubstituteDeals(_dealSpace.getDealSpace().toArray(new PricingDeal[0]), items);

    List<boolean[]> collisionMap = getFilteredCollisionMap(workingDS);

    return calculateBestDeals(items, ArrayUtils.removeNulls(workingDS), collisionMap);
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  public Result<I, D> calculateThresholdDeals(Item<I>[] items) {
    PricingDeal[] workingDS =
        filterEligibleThresholdDeals(_dealSpace.getDealSpace().toArray(new PricingDeal[0]), items);

    List<boolean[]> collisionMap = getFilteredCollisionMap(workingDS);

    return calculateBestDeals(items, ArrayUtils.removeNulls(workingDS), collisionMap);
  }

  public void setDealSpace(DealSpace<D> space) {
    _dealSpace = space;
  }
  
  /**
   * Gather the deals that are eligible condition deals for the item.
   * 
   * @param item
   * @return
   */
  @SuppressWarnings("unchecked")
  public PricingDeal<D>[] filterConditionalDealsForItem(Item<?> item) {
    PricingDeal<D>[] allDs = _dealSpace.getDealSpace().toArray(new PricingDeal[0]);
    
    for (int i = 0; i < allDs.length; i++ ) {
      PricingDeal<D> deal = allDs[i];

      if (deal == null) {
        continue;
      }

      boolean isMet = true;
      // Condition1: The deals which have more than one rows in prc_deal_item.
      if (deal.itemSet.length <= 1) {
        isMet = false;
      }

      // exclude the threshold deals
      if ((deal.subtotalMin != 0) && (deal.subtotalMax != 0) && (deal.getTransAction() != null)) {
        allDs[i] = null;
        continue;
      }

      // exclude the substitute deals
      DealAction[] actions = deal.itemActions;
      boolean substitute = false;

      for (DealAction action : actions) {
        if (action instanceof SubstituteDealAction) {
          allDs[i] = null;
          substitute = true;
          break;
        }
      }

      if (substitute) {
        continue;
      }

      boolean isMatch = false;

      if (deal.itemSet != null) {
        for (ItemMatch match : deal.itemSet) {
          /* Condition2: The deals whose qty_min are more than one.
           * The two conditions is logic of OR
           * If one of the conditions is met then the deal is a conditional deal.
           */
          if (isMet || match._minQty > 1000) {
            isMatch = match.testItemEligibleForConditionalDeals(item);
          }

          if (isMatch) {
            break;
          }
        }
      }

      if (!isMatch) {
        allDs[i] = null;
      }
    }

    return ArrayUtils.removeNulls(allDs);
  }
  
  /**
   * Gather the deals based on minimum eligibility requirements. This method is run only once per search.
   * 
   * @param items Items in the transaction (flattened)
   * @return Deals which should be considered during this round, and those which were filtered out by nulling.
   */
  protected PricingDeal<D>[] filterEligibleDeals(PricingDeal<D>[] ds, Item<?>[] items) {
    int dealCount = ds.length, i, d;
    int[] masks = new int[dealCount];
    long[][] matchCounts = new long[dealCount][];

    for (Item<?> item : items) {
      d = 0;
      for (PricingDeal<D> deal : ds) {
        if (deal == null) {
          d++ ;
          continue;
        }

        // exclude the threshold deals
        if ((deal.subtotalMin != 0) && (deal.subtotalMax != 0) && (deal.getTransAction() != null)) {
          ds[d] = null;
          d++ ;
          continue;
        }

        // exclude the substitute deals
        DealAction[] actions = deal.itemActions;
        boolean substitute = false;

        for (DealAction action : actions) {
          if (action instanceof SubstituteDealAction) {
            ds[d] = null;
            substitute = true;
            break;
          }
        }
        if (substitute) {
          d++ ;
          continue;
        }

        if (deal.itemSet != null) {
          if (matchCounts[d] == null) {
            matchCounts[d] = new long[deal.itemSet.length];
          }
          i = 0;
          for (ItemMatch match : deal.itemSet) {
            if (((masks[d] & (1 << i)) <= 0) && match.testItem(item)) {
              matchCounts[d][i] += item.fields[_quantities.ordinal()];
              if (matchCounts[d][i] >= match._minQty) {
                masks[d] |= 1 << i;
              }
            }
            i++ ;
          }
        }
        d++ ;
      }
    }

    // Ensure minimum satisfaction and add eligible.
    boolean pass;
    for (d = 0; d < dealCount; d++ ) {
      PricingDeal<D> deal = ds[d];
      if (deal != null) {
        pass = true;
        pass &= deal.matchBase == masks[d];
      }
      else {
        pass = false;
      }

      if (!pass) {
        // Only set it to null... the index of valid deals must remain
        // the same for collision grouping to work.
        ds[d] = null;
      }
    }

    if (_isDebugEnabled) {
      StringBuffer b = new StringBuffer("Deal filtered [");
      for (d = 0; d < dealCount; d++ ) {
        if (d > 0) {
          b.append(", ");
        }
        b.append((ds[d] == null) ? 'n' : 'y');
      }
      b.append(']');
      _logger.debug(b.toString());
    }

    return ds;
  }

  protected PricingDeal<D>[] filterEligibleSubstituteDeals(PricingDeal<D>[] ds, Item<?>[] items) {
    int dealCount = ds.length, d;
    d = 0;

    for (PricingDeal<D> deal : ds) {
      DealAction[] actions = deal.itemActions;

      if (actions.length < 1) {
        ds[d] = null;
        d++ ;
        continue;
      }

      for (int j = 0; j < actions.length; j++ ) {
        if (!(actions[j] instanceof SubstituteDealAction)) {
          ds[d] = null;
        }
      }
      d++ ;
    }

    if (_isDebugEnabled) {
      StringBuffer b = new StringBuffer("Deal filtered [");
      for (d = 0; d < dealCount; d++ ) {
        if (d > 0) {
          b.append(", ");
        }
        b.append((ds[d] == null) ? 'n' : 'y');
      }
      b.append(']');
      _logger.debug(b.toString());
    }

    return ds;
  }

  protected PricingDeal<D>[] filterEligibleThresholdDeals(PricingDeal<D>[] ds, Item<?>[] items) {
    // Make the subtotal
    long subtotal = 0;
    int dealCount = ds.length, i, d;
    int[] masks = new int[dealCount];
    long[][] matchCounts = new long[dealCount][];

    for (Item<?> item : items) {
      if (item.fields[_dealAllowances.ordinal()] != 0) {
        subtotal += item.fields[_prices.ordinal()] * item.fields[_quantities.ordinal()] / _qtyOne;
      }

      d = 0;
      for (PricingDeal<D> deal : ds) {
        if (deal == null) {
          d++ ;
          continue;
        }

        if ((deal.subtotalMin != 0) && (deal.subtotalMax != 0) && (deal.getTransAction() != null)) {
          if (deal.itemSet != null) {
            if (matchCounts[d] == null) {
              matchCounts[d] = new long[deal.itemSet.length];
            }
            i = 0;
            for (ItemMatch match : deal.itemSet) {
              if (((masks[d] & (1 << i)) <= 0) && match.testItem(item)) {
                matchCounts[d][i] += item.fields[_quantities.ordinal()];
                if (matchCounts[d][i] >= match._minQty) {
                  masks[d] |= 1 << i;
                }
              }
              i++ ;
            }
          }
        }
        else {
          ds[d] = null;
        }
        d++ ;
      }
    }

    // Ensure minimum satisfaction and add eligible.
    boolean pass;
    for (d = 0; d < dealCount; d++ ) {
      PricingDeal<D> deal = ds[d];
      if (deal != null) {
        pass = (subtotal >= deal.subtotalMin) && (subtotal <= deal.subtotalMax);
        pass &= deal.matchBase == masks[d];
      }
      else {
        pass = false;
      }

      if (!pass) {
        // Only set it to null... the index of valid deals must remain
        // the same for collision grouping to work.
        ds[d] = null;
      }
    }

    if (_isDebugEnabled) {
      StringBuffer b = new StringBuffer("Deal filtered [");
      for (d = 0; d < dealCount; d++ ) {
        if (d > 0) {
          b.append(", ");
        }
        b.append((ds[d] == null) ? 'n' : 'y');
      }
      b.append(']');
      _logger.debug(b.toString());
    }

    return ds;
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  private Result<I, D> calculateBestDeals(Item<I>[] items, PricingDeal<D>[] workingDS,
      List<boolean[]> argCollisionMaps) {
    final PricingItemAdapter<I> iAdapt = _adapter.getItemAdapter();

    int i, d, m, dealCount = workingDS.length, itemCount = items.length;

    // Create the stack and result store.
    StackFrame first =
        StackFrame.createFirstFrame(_adapter, workingDS, items,
            iAdapt.translateItemField(_quantities, BigDecimal.ONE));

    _stack = new StackFrame[dealCount];
    _results = new ResultStore<I, D>(dealCount, itemCount, first.maxMatches);
    ResultStore<I, D> tempStore = new ResultStore<I, D>(dealCount, itemCount, first.maxMatches);
    for (i = 0; i < dealCount; i++ ) {
      _stack[i] = new StackFrame(first);
    }
    
    //initialize the price array of _results
    //System.arraycopy(first.prices,0 , _results._latestItemPrice, 0, itemCount);

    // Brute force.
    if (_isDebugEnabled) {
      _logger.debug("Brute forcing " + argCollisionMaps.size() + " groups");
    }
    for (boolean[] group : argCollisionMaps) {
      _results._bestDealAmount = first.subtotal; // Reset the best deal.
      if (_isDebugEnabled) {
        _logger.debug("BEGIN new bruteForce: first.subtotal = " + first.subtotal
            + " stackframe.bestDealAmount = " + _results._bestDealAmount + " result.dealAmountLevel = "
            + ((_results._dealAmountLevels.length == 0) ? "[none]" : _results._dealAmountLevels[0]));
      }
      bruteForce(items, 0, first, group, tempStore, _results);
      _results.fillInfo(group, tempStore);
    }

    Result_Impl<I, D> resultList = new Result_Impl<I, D>();
    ResultApplication_Impl<I, D> res;

    // This is a single-pass application of the optimal dealset, both for
    // validation that the dealset is compatible in itself, and to produce a
    // cross-section of the results suitable to application back to the line
    // items themselves.
    StackFrame prev = first;
    long temp, idif;
    long[] hold = new long[itemCount];
    System.arraycopy(first.prices, 0, hold, 0, itemCount);
    System.arraycopy(first.prices,0 , _results._latestItemPrice, 0, itemCount);
    
    for (d = 0; d < dealCount; d++ ) {
      StackFrame s = _stack[d];
      s.shadow(prev);
      System.arraycopy(_results._latestItemPrice,0 , s.newPrices, 0, itemCount);

      for (i = 0; i < _results._bestDealIterationCount[d]; i++ ) {
        temp = 0;
        if (!workingDS[d].isEligible(items, s)) {
          _logger.debug("Deal " + (d + 1) + " not eligible after election!?");
          continue;//if the current deal is not eligible,then go to next
        }
        temp -= workingDS[d].apply(d, items, s);
        //make sure the item prices are updated
        syncItemPrice(_results,s);

        long totalDealAmt = temp;
        long totalDealAmtReg = temp;

        if (temp > 0) { // Audit-trace result
          long[] actQtyArr = new long[itemCount];
          long[] qtyArr = new long[itemCount];

          long[] subtotalArr = new long[dealCount];

          for (int j = 0; j < itemCount; j++ ) {
            for (m = 0; m < first.maxMatches; m++ ) {
              qtyArr[j] += s.matched[j][m];
              if ((workingDS[d] != null) && (workingDS[d].getDealAction().length > m)
                  && (workingDS[d].getDealAction()[m] != null)) {
                actQtyArr[j] += s.matched[j][m];
              }
            }

            if (qtyArr[j] > 0) {
              subtotalArr[d] = subtotalArr[d] + hold[j];
            }
          }

          if (_isDebugEnabled) {
            _logger.debug("Tallying brokered deal.");
          }
          res = new ResultApplication_Impl<I, D>();
          res.setDeal(workingDS[d]);
          long tWide = 0;

          for (int j = 0; j < itemCount; j++ ) {
            if (j == itemCount - 1) {
              idif = totalDealAmtReg;
            }
            else {
              idif = hold[j] - s.prices[j];
            }
            totalDealAmtReg = totalDealAmtReg - idif;

            long dealAmt = 0;
            if ((qtyArr[j] > 0) && (prev.subtotal != 0) && (totalDealAmt != 0)) {
              if (j == itemCount - 1) {
                dealAmt = totalDealAmt;
              }
              else {
                // dealAmt = temp * hold[j] / subtotalArr[d];
                BigDecimal dividend = BigDecimal.valueOf(temp * hold[j]);
                BigDecimal divisor = BigDecimal.valueOf(subtotalArr[d]);
                dealAmt = dividend.divide(divisor, HALF_EVEN).longValue();
              }
              totalDealAmt = totalDealAmt - dealAmt;
            }
            else if (workingDS[d].getTransAction() != null) {
              dealAmt = idif;
            }

            if (qtyArr[j] > 0) {
              if (_isDebugEnabled) {
                _logger.debug("  - Adding item conglomerate");
              }
              res.addCause(items[j], iAdapt.revertQuantity(qtyArr[j]), iAdapt.revertPrice(idif),
                  iAdapt.revertQuantity(actQtyArr[j]), iAdapt.revertPrice(dealAmt));
            }
            else if (idif > 0) {
              // Transaction-wide amount to process
              tWide += idif;
              res.addCause(items[j], BigDecimal.ZERO, iAdapt.revertPrice(idif),
                  iAdapt.revertQuantity(actQtyArr[j]), iAdapt.revertPrice(dealAmt));
            }
          }
          res.setTransactionAmount(iAdapt.revertPrice(tWide));
          resultList.addResult(res);
        }
        
        //System.arraycopy(s.prices, 0, hold, 0, itemCount);
        //avoid item prices may grater than the original price(eg. new price deal,new price is greater than the original price)
        System.arraycopy(/*getLatestItemPrice(s)*/s.deal.getLatestItemPrice(s) , 0, hold, 0, itemCount);
      }

      // Move to the next by making the current previous.
      prev = s;
    }

    if (_logger.isInfoEnabled() && (!resultList.getDealResults().isEmpty())) {
      _logger.info("Number of deal applications: " + resultList.getDealResults().size());
      for (ResultApplication<I, D> dl : resultList.getDealResults()) {
        _logger.info("  * Deal '" + dl.getDeal().getNativeDeal() + "' applied");
        for (ResultAppPredication<I, D> treb : dl.getCauses()) {
          _logger.info("      - item: [(x" + treb.getQuantityMatched() + ") $" + treb.getAmountBrokered()
              + "] " + treb.getLineItem());
        }
        if (NumberUtils.isPositive(dl.getTransactionAmount())) {
          _logger.info("      @ transaction: " + dl.getTransactionAmount());
        }
      }
    }

    // Allow the garbage collector to collect the stack.
    _stack = null;

    return resultList;
  }

  /**
   * Filters the deal collision groups based on the filtered deal sets to ensure that the collision groups and
   * the working deal set line up later on in the deal engine.
   * 
   * @param argDealSet the filtered deal set with null where a deal has been filtered.
   * @return a deal collision group that has been filtered based on the passed working deal set.
   */
  private List<boolean[]> getFilteredCollisionMap(PricingDeal<D>[] argDealSet) {
    List<boolean[]> globalMap = _dealSpace.getCollisionMap();
    List<boolean[]> filteredCollisionMap = new ArrayList<boolean[]>(globalMap.size());
    ArrayList<Integer> lookupList = new ArrayList<Integer>();

    // figure out which collisions we want to keep and store them in the lookup list.
    for (int dealIdx = 0; dealIdx < argDealSet.length; dealIdx++ ) {
      if (argDealSet[dealIdx] != null) {
        lookupList.add(dealIdx);
      }
    }

    // Initialize the collision group array to the new size
    int finalSize = lookupList.size();
    for (int arrayIdx = 0; arrayIdx < globalMap.size(); arrayIdx++ ) {
      filteredCollisionMap.add(new boolean[finalSize]);
    }

    // Use the lookupList to populate the filtered map with the correct values from the global collision map
    for (int currentPosition = 0; currentPosition < lookupList.size(); currentPosition++ ) {
      Integer dealIdx = lookupList.get(currentPosition);
      for (int currentGroup = 0; currentGroup < filteredCollisionMap.size(); currentGroup++ ) {
        filteredCollisionMap.get(currentGroup)[currentPosition] = globalMap.get(currentGroup)[dealIdx];
      }
    }

    return filteredCollisionMap;
  }
  
 // save the latest item prices and sync them with frame.prices,frame.newPrices
  public void syncItemPrice(ResultStore<I, D> result,StackFrame<I, D> frame){
    long[] resultItemPrice = result._latestItemPrice;
    long[] stackFrameItemPrice = frame.prices;
    int itemCount = result._itemCount;

    for(int i=0;i<itemCount;i++){
      if(stackFrameItemPrice[i] < resultItemPrice[i]){
        resultItemPrice[i] = stackFrameItemPrice[i]; 
      }
     }
    System.arraycopy(_results._latestItemPrice,0 , frame.prices, 0, itemCount);
    System.arraycopy(_results._latestItemPrice,0 , frame.newPrices, 0, itemCount);
  }
}
