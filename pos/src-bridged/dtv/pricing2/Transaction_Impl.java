//$Id: Transaction_Impl.java 777 2014-03-27 02:15:29Z suz.jliu $
package dtv.pricing2;

import java.util.*;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.joda.time.ReadableInstant;

/**
 * A transaction from the perspective of the deal pricing engine. This should be maintained with the
 * native transaction type, since it will contain the properly stateful information regarding that
 * transaction.<br>
 * <br>
 * 
 * Copyright (c) 2007 Datavantage Corporation
 * 
 * @author gfischer
 * @created June 1, 2006
 * @version $Revision: 777 $
 */
class Transaction_Impl<L, D, T>
    implements Transaction<L, D, T> {

  private static final long serialVersionUID = 1;
  private static final Logger _logger = Logger.getLogger(Transaction_Impl.class);
  // this is used to log things like what the items turned out to be, etc.
  private static final Logger _tLogger = Logger.getLogger(Transaction.class);

  // Native transaction linkage
  final T _nativeTransaction;

  // Pricing engine to use with this transaction.
  transient Engine<L, D> _engine;
  // Pricing adapter which created this transaction.
  transient PricingAdapter<L, D, T> _adapter;
  transient Enum<?>[] _itemFields;
  // Item adapter to use with the item addition and removal methods of this transaction.
  transient PricingItemAdapter<L> _itemAdapter;
  // List of translated, sorted items in this transaction.
  ArrayList<Item<L>> _items = null;
  /* Cached results of the last deal search. this is returned in the case that
   * there were no changes in the itemspace between calls to calculate the best deals. */
  transient Result<L, D> _cachedResult = null;
  /* Parent dealspace to the itemspace's dealspace. Used to draw time/location
   * sensitive deals into the transactional dealspace during itemspace changes
   * and regardless of triggers which have been activated. */
  transient DealSpaceTransactionBegin<D> _startingDealSpace;
  /* Collection of the deals available based on triggers added to the
   * transaction. This is the last phase of filtering before the engine must
   * discern the deals based on their minimum requirements. */
  transient DealSpaceTransactionInProgress<D> _mainDealSpace;
  /* Flag to discern any change in the item list (provides cached deals if no
   * changes occured) */
  boolean _dirty;

  /*
   * Create a transaction valid at the time specified by 'now'.
   *
   * @param argAdapter Pricing adapter used to create the transaction.
   * @param now The instant at which the transaction should be viable for deals.
   */
  Transaction_Impl(PricingAdapter<L, D, T> argAdapter, T nativ, ReadableInstant now) {
    _nativeTransaction = nativ;
    _items = new ArrayList<Item<L>>();

    // Although intended for returning from serialization, this method
    // also does everything that we need from the standpoint of setting up
    // the dealspace and assigning members.
    reestablishPricingAdapter(argAdapter);
  }

  /** {@inheritDoc} */
  @Override
  public void addItem(L argItem) {
    if (!_itemAdapter.isPriceable(argItem)) {
      // Not priceable, ignore.
      return;
    }

    Item<L> item = new Item<L>(_adapter, argItem);
    Object temp = null;

    // The test is NOT a case statement for purposes of possible engine
    // extension see PricingAdapter for details (if I remembered to put them
    // there).
    for (Enum<?> field : _itemFields) {
      temp = _itemAdapter.getItemField(field, argItem);
      // Any other field gets normal translation.
      item.fields[field.ordinal()] = _itemAdapter.translateItemField(field, temp);
    }

    if (_logger.isInfoEnabled()) {
      _logger.info("Adding item: " + Arrays.toString(item.fields));
    }
    addItem(item);
  }

  /** {@inheritDoc} */
  @Override
  public void addTarget(String argTarget) {
    _mainDealSpace.addTarget(argTarget);
  }

  /** {@inheritDoc} */
  @Override
  public void addTransactionDeal(Object argDealKey, PricingDeal<D> argDeal) {
    _mainDealSpace.addTransactionDeal(argDealKey, argDeal);
    _dirty = true;
  }

  /** {@inheritDoc} */
  @Override
  public void addTrigger(String trigger) {
    _mainDealSpace.addTrigger(trigger);
  }

  /** {@inheritDoc} */
  @Override
  public void clearTargets() {
    _mainDealSpace.clearTargets();
  }

  /** {@inheritDoc} */
  @Override
  public void debug(StringBuffer sb) {
    sb.append(DebugAide.dealSpaceToString(_mainDealSpace));
    sb.append("\n\n----------------------------------------------------------\n\n");
    sb.append("triggers:");
    for (Entry<String, SortedSet<String>> entry : _mainDealSpace.transactionTriggerGroups_.entrySet()) {
      for (String trigger : entry.getValue()) {
        sb.append("\n  ").append(trigger);
      }
    }
    sb.append("\n\n----------------------------------------------------------\n\n");
    sb.append(DebugAide.printTransactionItems(this));
  }

  /** {@inheritDoc} */
  @Override
  @SuppressWarnings("unchecked")
  public Result<L, D> getBestDeals() {
    // If there have been no changes or deals,
    // there's no reason to recalculate, eh?
    if (_dirty && _mainDealSpace.hasDeals()) {
      Result<L, D> result = null;
      //      EngineDealResult<L, D> result = null;
      if ((_items != null) && (_items.size() > 0)) {
        // Arraylist is sorted during insertion.
        Item<L>[] l_items = _items.toArray(new Item[0]);
        result = _engine.calculateDeals(l_items);
      }

      // For some reason, type safety on generics doesn't recognize the fact
      // that these are compatible...
      _cachedResult = result;

      // Log the itmes (if it's appropriate)
      if (_tLogger.isDebugEnabled()) {
        _tLogger.debug('\n' + DebugAide.printTransactionItems(this).trim() + '\n');
      }
    }

    // Make sure we don't return null, under any circumstances.
    if (_cachedResult == null) {
      _cachedResult = new Result_Impl<L, D>();//EngineDealResult(itemAdapt);
    }
    _dirty = false;

    // Return the now guaranteed-correct collection.
    return _cachedResult;
  }

  /** {@inheritDoc} */
  @Override
  public T getNativeTransaction() {
    return _nativeTransaction;
  }

  /** {@inheritDoc} */
  @Override
  public List<PricingDeal<D>> getPossibleDeals() {
    return _mainDealSpace.getDealSpace();
  }

  @Override
  @SuppressWarnings("unchecked")
  public Result<L, D> getSubstituteDeals() {
    // If there have been no changes or deals,
    // there's no reason to recalculate, eh?
    if (_dirty && _mainDealSpace.hasDeals()) {
      Result<L, D> result = null;

      if ((_items != null) && (_items.size() > 0)) {
        // Arraylist is sorted during insertion.
        Item<L>[] l_items = _items.toArray(new Item[0]);
        result = _engine.calculateSubstituteDeals(l_items);
      }

      // For some reason, type safety on generics doesn't recognize the fact
      // that these are compatible...
      _cachedResult = result;

      // Log the itmes (if it's appropriate)
      if (_tLogger.isDebugEnabled()) {
        _tLogger.debug('\n' + DebugAide.printTransactionItems(this).trim() + '\n');
      }
    }

    // Make sure we don't return null, under any circumstances.
    if (_cachedResult == null) {
      _cachedResult = new Result_Impl<L, D>();//EngineDealResult(itemAdapt);
    }
    _dirty = false;

    // Return the now guaranteed-correct collection.
    return _cachedResult;
  }

  /** {@inheritDoc} */
  @Override
  @SuppressWarnings("unchecked")
  public Result<L, D> getThresholdDeals() {
    // If there have been no changes or deals,
    // there's no reason to recalculate, eh?
    if (_mainDealSpace.hasDeals()) {
      Result<L, D> result = null;

      if ((_items != null) && (_items.size() > 0)) {
        // Arraylist is sorted during insertion.
        Item<L>[] l_items = _items.toArray(new Item[0]);
        result = _engine.calculateThresholdDeals(l_items);
      }

      // For some reason, type safety on generics doesn't recognize the fact
      // that these are compatible...
      _cachedResult = result;

      // Log the itmes (if it's appropriate)
      if (_tLogger.isDebugEnabled()) {
        _tLogger.debug('\n' + DebugAide.printTransactionItems(this).trim() + '\n');
      }
    }

    // Make sure we don't return null, under any circumstances.
    if (_cachedResult == null) {
      _cachedResult = new Result_Impl<L, D>();//EngineDealResult(itemAdapt);
    }
    _dirty = false;

    // Return the now guaranteed-correct collection.
    return _cachedResult;
  }

  /** {@inheritDoc} */
  @Override
  public boolean isDirty() {
    return _dirty;
  }

  /** {@inheritDoc} */
  @Override
  public void reestablishPricingAdapter(PricingAdapter<L, D, T> argAdapter) {
    _adapter = argAdapter;
    _itemAdapter = _adapter.getItemAdapter();
    _itemFields = _adapter._itemFields.getEnumConstants();

    // Rebuild the dealspace (if the check isn't closed before suspension, any
    // deals which expire will be lost (voided)
    _startingDealSpace = new DealSpaceTransactionBegin<D>();
    _startingDealSpace.setParentDealSpace(_adapter.getDealSpace());

    _mainDealSpace = new DealSpaceTransactionInProgress<D>();
    _mainDealSpace.setParentDealSpace(_startingDealSpace);

    // Ensure that the items have received the new item adapter.
    for (Item<L> item : _items) {
      item.adapter = _adapter;
    }

    if (_logger.isDebugEnabled()) {
      _logger.debug("NUMBER OF ITEMS RESURRECTED: " + _items.size());
    }

    _engine = new Engine<L, D>(_adapter);
    _engine.setDealSpace(_mainDealSpace);
    _dirty = true;
  }

  @Override
  public void refreshItem(L argItem) {
    // Do not check for pricability, it may have become outdated before refresh.
    Iterator<Item<L>> iter = _items.iterator();
    Item<L> it;
    while (iter.hasNext()) {
      if (System.identityHashCode((it = iter.next()).getNativeItem()) == System.identityHashCode(argItem)) {
        if (_logger.isInfoEnabled()) {
          _logger.info("Refreshing item: " + Arrays.toString(it.fields));
        }
        iter.remove();
        _dirty = true;
        if (_itemAdapter.isPriceable(argItem)) {
          Item<L> item = new Item<L>(_adapter, argItem);
          Object temp;

          for (Enum<?> field : _itemFields) {
            temp = _itemAdapter.getItemField(field, argItem);
            // Any other field gets normal translation.
            item.fields[field.ordinal()] = _itemAdapter.translateItemField(field, temp);
          }
          if (_logger.isDebugEnabled()) {
            _logger.debug("Re-adding item: " + Arrays.toString(item.fields));
          }
          addItem(item);
        }
        break;
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public void removeItem(L argItem) {
    if (!_itemAdapter.isPriceable(argItem)) {
      // Not priceable, ignore.
      return;
    }

    Iterator<Item<L>> iter = _items.iterator();
    Item<L> it;
    while (iter.hasNext()) {
      if (System.identityHashCode((it = iter.next()).getNativeItem()) == System.identityHashCode(argItem)) {
        iter.remove();
      }

      if (_logger.isInfoEnabled()) {
        _logger.info("Removing item: " + Arrays.toString(it.fields));
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public void removeTarget(String argTarget) {
    _mainDealSpace.removeTarget(argTarget);
  }

  /** {@inheritDoc} */
  @Override
  public void removeTransactionDeal(Object argDealKey) {
    _mainDealSpace.removeTransactionDeal(argDealKey);
    _dirty = true;
  }

  /** {@inheritDoc} */
  @Override
  public void removeTrigger(String trigger) {
    _mainDealSpace.removeTrigger(trigger);
  }
  
  /** {@inheritDoc} */
  @SuppressWarnings("unchecked")
  @Override
  public PricingDeal<D>[] getEligibleConditionalDeals() {
    if ((_items != null) && (_items.size() > 0)) {
      Item<L>[] l_items = _items.toArray(new Item[0]);
      return _engine.filterConditionalDealsForItem(l_items[0]);
    }
    return null;
  }

  // Adds an item to the transaction.
  private void addItem(Item<L> item) {
    if (_items.contains(item)) {
      _items.remove(item);
    }

    int idx = Math.abs(Collections.binarySearch(_items, item) + 1);

    _items.add(idx, item);
    _dirty = true;
  }
}
