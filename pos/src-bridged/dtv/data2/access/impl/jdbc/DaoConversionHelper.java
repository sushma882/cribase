// $Id: DaoConversionHelper.java 26827 2011-02-12 22:14:30Z dtvdomain\jweiss $
package dtv.data2.access.impl.jdbc;

import dtv.data2.access.IDataAccessObject;

/**
 * Contains OPS code for converting DAOs.<br>
 * <br>
 * Copyright (c) 2011 MICROS Retail
 * 
 * @author jgaughan
 * @created Jan 24, 2011
 * @version $Revision: 26827 $
 */
public final class DaoConversionHelper {
  public static boolean isDaoConvertOnPkViolation(IDataAccessObject argDao) {
    if(argDao.getClass().getName().equals("dtv.xst.dao.ttr.impl.VoucherDAO")
        || argDao.getClass().getName().equals("dtv.xst.dao.crm.impl.PartyDAO")
        || argDao.getClass().getName().equals("dtv.xst.dao.crm.impl.PartyLocaleInformationDAO")
        || argDao.getClass().getName().equals("dtv.xst.dao.crm.impl.PartyPropertyDAO")
        || argDao.getClass().getName().equals("dtv.xst.dao.crm.impl.PartyItemCrossReferenceDAO")
        || argDao.getClass().getName().equals("dtv.xst.dao.crm.impl.PartyTelephoneDAO")
        || argDao.getClass().getName().equals("dtv.xst.dao.crm.impl.PartyEmailDAO")
        || argDao.getClass().getName().equals("dtv.xst.dao.inv.impl.StockLedgerDAO")
        || argDao.getClass().getName().equals("dtv.xst.dao.inv.impl.InventoryJournalDAO")
        || argDao.getClass().getName().equals("dtv.xst.dao.ctl.impl.DeviceRegistrationDAO"))
       {
        return true;
        
      }else
        return false;
  }

  private DaoConversionHelper() {
    throw new UnsupportedOperationException();
  }
}
