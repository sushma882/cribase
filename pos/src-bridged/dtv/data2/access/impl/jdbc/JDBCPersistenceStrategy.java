// $Id: JDBCPersistenceStrategy.java 27831 2012-02-20 09:31:59Z dtvdomain\jweiss $
package dtv.data2.access.impl.jdbc;

import static dtv.data2.access.impl.DaoState.INSERT_OR_UPDATE;

import static dtv.data2.access.impl.DaoState.*;

import java.sql.*;
import java.util.*;

import org.apache.log4j.Logger;

import dtv.data2.SQLExceptionScrubber;
import dtv.data2.access.*;
import dtv.data2.access.exception.*;
import dtv.data2.access.impl.*;
import dtv.data2.access.query.*;
import dtv.data2.access.transaction.TransactionToken;
import dtv.util.StringUtils;

/**
 * Persistence strategy for JDBC<br>
 * 
 * Copyright (c) 2005 Datavantage Corporation
 * 
 * @created Feb 8, 2005
 * @author Jeff Sheldon
 * @version $Revision: 27831 $
 */
public class JDBCPersistenceStrategy
    extends AbstractJDBCPersistenceStrategy {

  private static final Logger _logger = Logger.getLogger(JDBCPersistenceStrategy.class);
  private static final boolean _debugLogging = _logger.isDebugEnabled();
  private static final String OLD_PACKAGE_STRUCTURE = "dtv.pos.dao";

  /** {@inheritDoc} */
  @Override
  public boolean checkExistence(IObjectId argId) {
    Object result = getObjectById(argId);
    return (result != null);
  }

  /**
   * Returns models with their full hierarchy loaded, but no relationships loaded.
   * 
   * This method is public for DtxQueryHandler's use, but is not part of the IPersistenceStrategy API
   * 
   * @param argSql the sql query to run.
   * @param argParams parameters for the query to be run
   * @param argTemplate the Class template for the current model loading
   * 
   * @return A list of IDataModel's with their hierarchy loaded, but no relationships
   */
  public List<IDataModel> getModelsByQuery(String argSql, List<?> argParams, Class<?> argTemplate) {
    return getModelsByQuery(argSql, argParams, argTemplate, 0);
  }

  /**
   * Returns models with their full hierarchy loaded, but no relationships loaded.
   * 
   * This method is public for DtxQueryHandler's use, but is not part of the IPersistenceStrategy API
   * 
   * @param argSql the sql query to run.
   * @param argParams parameters for the query to be run
   * @param argTemplate the Class template for the current model loading
   * 
   * @return A list of IDataModel's with their hierarchy loaded, but no relationships
   */
  public List<IDataModel> getModelsByQuery(String argSql, List<?> argParams, Class<?> argTemplate,
      int argMaxRecords) {
    String sqlToRun = SqlQueryBuilder.cleanSqlString(argSql);
    Connection conn = JDBCDataSourceMgr.getInstance().getConnection(getDataSourceName());

    try {
      PreparedStatement statement = conn.prepareStatement(sqlToRun);
      statement.setMaxRows(argMaxRecords);

      if ((argParams != null) && !argParams.isEmpty()) {
        for (int i = 0; i < argParams.size(); i++ ) {
          Object parm = argParams.get(i);

          if (parm == null) {
            throw new DtxException("Null parameter detected for query.  We do not support null "
                + "parameters for queries. Params given: " + argParams);
          }
          if (parm instanceof Timestamp) {
            statement.setTimestamp(i + 1, (Timestamp) parm);
          }
          else {
            statement.setObject(i + 1, parm);
          }
        }
      }
      statement.execute();
      ResultSet resultSet = statement.getResultSet();

      return getModelsByResultSet(resultSet, conn, argTemplate, sqlToRun);
    }
    // Error handling from here on out.
    catch (Exception ex) {
      markConnectionStale(conn);
      if (FailoverException.isFailover(ex)) {
        throw FailoverException.getNewException(ex, getDataSourceName());
      }
      else {
        String msg =
            "getModelsByQuery() exception CAUGHT on datasource " + getDataSourceName() + " Class template: "
                + argTemplate + " executing sql: " + sqlToRun + " params: " + argParams;

        if (ex instanceof SQLException) {
          SQLException sqlEx = SQLExceptionScrubber.scrub((SQLException) ex);
          JDBCHelper.logSqlException(msg, sqlEx);
          throw new JDBCException(sqlEx);
        }
        else {
          _logger.error(msg, ex);

          if (ex instanceof DtxException) {
            throw (DtxException) ex;
          }
          else {
            throw new DtxException(msg, ex);
          }
        }
      }
    }
    finally {
      if (conn != null) {
        try {
          conn.close();
        }
        catch (SQLException e) {
          _logger.debug("Exception while closing connection", e);
        }
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public IDataModel getObjectById(IObjectId argId) {
    IJDBCTableAdapter adapter = getTableAdapter(argId);
    Connection conn = JDBCDataSourceMgr.getInstance().getConnection(getDataSourceName());

    String stmt = adapter.getSelect() + adapter.getSelectWhere();
    stmt = SqlQueryDecorator.decorateSql(stmt, this, argId);

    if (_debugLogging) {
      _logger.debug("Getting object by id with the following statement: " + stmt + " ((" + argId.toString()
          + "))");
    }

    // Perform the lookup.
    try {
      PreparedStatement statement = conn.prepareStatement(stmt);
      statement = adapter.writeObjectId(argId, statement);
      statement.execute();
      ResultSet resultSet = statement.getResultSet();
      List<IDataModel> models = getModelsByResultSet(resultSet, conn, argId.getClass(), stmt);

      return ((models == null) || (models.isEmpty())) ? null : models.get(0);
    }

    // Error handling from here on out.
    catch (Exception ex) {
      markConnectionStale(conn);
      if (FailoverException.isFailover(ex)) {
        throw FailoverException.getNewException(ex, getDataSourceName());
      }
      else {
        String msg =
            "getObjectById() exception CAUGHT on datasource " + getDataSourceName()
                + " while looking up id type: " + argId.getClass().getName() + " value: " + argId;

        if (ex instanceof SQLException) {
          SQLException sqlEx = SQLExceptionScrubber.scrub((SQLException) ex);
          JDBCHelper.logSqlException(msg, sqlEx);
          throw new JDBCException(sqlEx);
        }
        else {
          _logger.error(msg, ex);
          throw new DtxException(msg, ex);
        }
      }
    }
    finally {
      if (conn != null) {
        try {
          conn.close();
        }
        catch (SQLException e) {
          if (_debugLogging) {
            _logger.debug("An exception occurred while closing our connection.", e);
          }
        }
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public Object getObjectByQuery(String argQueryKey, Map<String, Object> argParams) {
    // Let the query handler do the work!
    IQueryHandler handler = DataFactory.getInstance().getQueryHandler(argQueryKey);

    try {
      return handler.execute(this, argParams);
    }

    // Error checking
    catch (Exception ex) {
      if (FailoverException.isFailover(ex)) {
        throw FailoverException.getNewException(ex, getDataSourceName());
      }
      else {
        String msg =
            "getObjectByQuery() exception CAUGHT on datasource " + getDataSourceName()
                + " while executing query key: " + argQueryKey + " with params: (" + argParams + ")";

        if (ex instanceof SQLException) {
          SQLException sqlEx = SQLExceptionScrubber.scrub((SQLException) ex);
          JDBCHelper.logSqlException(msg, sqlEx);
          throw new JDBCException(msg, sqlEx);
        }
        else {
          _logger.error(msg, ex);

          if (ex instanceof DtxException) {
            throw (DtxException) ex;
          }
          else {
            throw new DtxException(msg, ex);
          }
        }
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public Object getObjectByRelationship(IDataModelRelationship argRelationship) {
    IDataModelImpl parent = (IDataModelImpl) argRelationship.getParent();

    // Get the adapter from the map.
    IRelationshipAdapter adapter =
        AdapterMap.getRelationshipAdapter(parent.getDAO().getClass(), argRelationship.getIdentifier());
    IJDBCRelationshipAdapter jdbcAdapter = null;

    if (adapter == null) {
      throw new DtxException("Could not load relationship: " + argRelationship.getIdentifier() + " for DAO: "
          + parent.getDAO().getClass() + " check associated .dtx's.");
    }
    else if (!(adapter instanceof IJDBCRelationshipAdapter)) {
      throw new DtxException("The AdapterMap produced an adapter that was NOT "
          + "compatiable with with JDBC for relationship: [" + argRelationship.getIdentifier()
          + "] and DAO: [" + parent.getDAO().getClass() + "] Adapter: [" + adapter + "]");
    }
    else {
      jdbcAdapter = (IJDBCRelationshipAdapter) adapter;
    }

    jdbcAdapter.setParent(parent.getDAO());
    // Since relationships require full participation by the parent,
    // the parameter list can not support nulls!
    List<?> parms = jdbcAdapter.getParameterList();
    Iterator<?> it = parms.iterator();
    while (it.hasNext()) {
      if (it.next() == null) {
        return null;
      }
    }

    String callString = jdbcAdapter.getSelect();

    if (jdbcAdapter.isOrgHierarchyJoinRequired()) {
      /* The child has orgCode/orgValue fields identifying its rows' membership in an organizational
       * hierarchy. The parent, however, has not in its relationship with the child explicitly identified an
       * association between its orgCode/orgValue fields and the child's (and maybe it can't because it
       * doesn't have them). In this case, we need to decorate any SQL associated with the relationship to
       * ensure that the child's associated rows are appropriately filtered of any rows associated with nodes
       * not in this system's org ancestry. */
      callString = SqlQueryDecorator.decorateSql(jdbcAdapter.getSelect(), this, (IObjectId) null);
    }

    List<IDataModel> models = getModelsByQuery(callString, parms, argRelationship.getChild());

    if ((models != null)
        && IDataModelRelationship.RelationshipType.ONE_TO_ONE.equals(argRelationship.getType())) {
      return models.get(0);
    }
    else {
      return models;
    }
  }

  /** {@inheritDoc} */
  @Override
  public boolean isFullGraphPersisted() {
    return false;
  }

  /** {@inheritDoc} */
  @Override
  public boolean isFullGraphProvided() {
    return false;
  }

  /**
   * Apply any rules to a persistable regarding its object state.
   * 
   * @param argPersistable the persistable to process.
   * @return a persistable to persist based on the object state rules. This may or may not be the passed-in
   * persistable.
   */
  protected IPersistable applyPreObjectStateRules(IPersistable argPersistable) {
    // This method only applies to DAO's... no need to act if this
    // object is something else.
    if (!(argPersistable instanceof IDataAccessObject)) {
      return argPersistable;
    }

    IDataAccessObject dao = (IDataAccessObject) argPersistable;

    // check for "INSERT_ONLY" we will have to query for the data
    // to see if it exists, and persist only if it is not there.
    if (INSERT_ONLY.matches(dao)) {
      IDataModel result = getObjectById(dao.getObjectId());
      dao = DaoUtils.cloneDao(dao);

      if (result == null) {
        dao.setObjectState(NEW.intVal());
        dao.setObjectStateRulesApplied(true);
      }
      else {
        dao.setObjectState(CLEAN.intVal());
        dao.setObjectStateRulesApplied(true);
      }
    }

    return dao;
  }

  /**
   * Returns models with their full hierarchy loaded, but no relationships loaded
   * 
   * @param argResultSet A current, live result set for us to process
   * @param connection current db connection
   * @param argTemplate the Class template for the current model loading
   * @param argSqlQuery the sql query that was used to create this result set.
   * 
   * @return A list of IDataModel's with their hierarchy loaded, but no relationships
   * @exception SqlException
   */
  protected List<IDataModel> getModelsByResultSet(ResultSet argResultSet, Connection argConn,
      Class<?> argTemplate, String argSqlQuery)
      throws SQLException {

    if (argResultSet == null) {
      throw new DtxException("The result set was unexpectedly NULL for a "
          + "successful query - this is unexpected jdbc behavior. Class template: " + "["
          + argTemplate.getName() + "] sql executed: " + argSqlQuery);
    }

    List<IDataModel> results = new ArrayList<IDataModel>();

    while (argResultSet.next()) {
      IJDBCTableAdapter adapter = getTableAdapter(argTemplate);
      adapter.getFiller().fill(argResultSet);
      results.add(loadHierarchy(adapter, argConn));
    }

    // if there were any warning generated, log them.
    // retrieving warnings turns out to be a bit expensive (on Oracle at least)
    if (_debugLogging) {
      boolean warnings = JDBCHelper.handleWarnings(argSqlQuery, argResultSet.getWarnings());
      if (warnings) {
        argResultSet.clearWarnings();
      }
    }
    // Close things up
    argResultSet.close();

    return (results.isEmpty()) ? null : results;
  }

  /**
   * Handle the error condition of attempting to load a DTX object but the system is not able to load the
   * class.
   * 
   * @param argAdapter the table adapter
   * @param argClassNameRoot the class name for the DTX object.
   * @param argClassName the class name for the DBA object. Typically this is a modified version of the root
   * class name.
   * @param argEx the exception that lead to the bad DTX class name.
   */
  protected void handleBadDtxClassName(IJDBCTableAdapter argAdapter, String argClassNameRoot,
      String argClassName, Exception argEx) {
    if (StringUtils.isEmpty(argClassNameRoot)) {
      throw new DtxException("Class name field is not set for a row on table: \"" + argAdapter.getTableName()
          + "\" (or parent table) - see \"dtv_class_name\" field, probably");
    }
    else if (argClassNameRoot.startsWith(OLD_PACKAGE_STRUCTURE)) {
      throw new DtxException("Class name field is set to an OLD package name: \"" + argClassNameRoot
          + "\" for table: " + argAdapter.getTableName() + ". This " + "should be updated to be of "
          + "the form " + "'dtv.xst.dao.itm.impl...', for example.");
    }
    else {
      throw new DtxException("Failed to instantiate: " + argClassName + ".  "
          + "There may be a problem with the " + "class name field on table: " + argAdapter.getTableName(),
          argEx);
    }
  }

  /**
   * Certain insert/update statements we run generate result sets (because of trigger interactions).
   * 
   * It has been shown that with JTDS that resultsets may contain or cover errors that will not be raised
   * until the result sets are read in by someone.
   * 
   * The error that the result set masks can be so serious that transaction data may not be saved, and yet no
   * exception is thrown to us by the driver.
   * 
   * @param argConnection current jdbc connection
   * @param PreparedStatement the current prepared statement in use.
   */
  protected void handleJtdsWorkAround(Connection argConnection, PreparedStatement argStatement)
      throws SQLException {
    // using toString instead of className b/c this is usually our connection wrapper.
    if (argConnection.toString().contains("net.sourceforge.jtds")) {
      ResultSet results = argStatement.getResultSet();
      if (results != null) {
        while (results.next()) {
          ;
        }
      }
    }
  }

  /**
   * Potentially apply special rules to handle the current situation intelligently. This method may:
   * <ol>
   * <li>Call for no further action (return null)</li>
   * <li>Throw an exception</li>
   * <li>Call for an exception to be thrown (Return an exception)</li>
   * <li>Call for the persistence to be retried (Return IDataAccessObject)</li>
   * </ol>
   * 
   * @param argException the exception that occurred while persisting
   * @param argDao the dao we are trying to persist
   * @param argCurrentSql the current sql being executed for logging purposes
   * @param argCurrentParams the current sql parameters being processed, for logging purposes
   * 
   * @return See above. Could be null, an exception to be thrown, or a dao to retry.
   */
  protected Object handlePersistenceException(Exception argException, IPersistable argPersistable,
      String argCurrentSql, List<Object> argCurrentParams) {
    // Retry Exception (likely DB overloaded)
    if (RetryException.isRetryException(argException)) {
      if (_debugLogging) {
        _logger.debug("handlePersistenceException determined this "
            + "to be a retry exception, throwing a retry. Original " + "exception: " + argException);
      }

      throw RetryException.getNewException(argException, getDataSourceName());
    }

    // Failover Exception (network connectivity issues & some DB
    // overload issues)
    else if (FailoverException.isFailover(argException)) {
      if (_debugLogging) {
        _logger.debug("handlePersistenceException determined this "
            + "to be a failover exception, throwing a failover. Original " + "exception: " + argException);
      }

      return FailoverException.getNewException(argException, getDataSourceName());
    }

    // UpdateNotEffectiveException - update statement affected 0
    // rows. Depending on object state, we may convert it to
    // an insert or just log a warning.
    else if (argException instanceof UpdateNotEffectiveException) {
      if ((argPersistable instanceof IDataAccessObject)
          && INSERT_OR_UPDATE.matches(((IDataAccessObject) argPersistable))
          && !((IDataAccessObject) argPersistable).isObjectStateRulesApplied()) {

        IDataAccessObject daoClone = DaoUtils.cloneDao((IDataAccessObject) argPersistable);
        daoClone.setObjectStateRulesApplied(true);

        return daoClone;
      }
      else {
        _logger.warn("An update was run against datasource: [" + getDataSourceName() + "] that had "
            + "no effect: " + argException);

        return argException;
      }
    }

    // PrimaryKeyViolationException - detected here only to throw
    // a more precise exception
    else if ((argPersistable instanceof IDataAccessObject) && (isNew(((IDataAccessObject) argPersistable))
        ||  INSERT_OR_UPDATE.matches(((IDataAccessObject) argPersistable))) 
        && PrimaryKeyViolationException.isPrimaryKeyViolation(argException)) {

      if (DaoConversionHelper.isDaoConvertOnPkViolation((IDataAccessObject) argPersistable)) {
        _logger.warn("Primary key error attempting to insert DAO, updating instead: key=["
            + ((IDataAccessObject) argPersistable).getObjectId() + "]");

        IDataAccessObject daoClone = DaoUtils.cloneDao((IDataAccessObject) argPersistable);
        daoClone.setObjectState(DaoState.UPDATED.intVal());
        daoClone.setObjectStateRulesApplied(true);
        return daoClone;
      }

      PrimaryKeyViolationException ex =
          PrimaryKeyViolationException.getNewException(argException, getDataSourceName());
      ex.setOffendingDao(((IDataAccessObject) argPersistable));
      throw ex;
    }

    // This is an unexpected, unhandled situation.
    else {
      String msg;

      if (argPersistable instanceof IDataAccessObject) {
        IDataAccessObject dao = (IDataAccessObject) argPersistable;
        msg =
            "makePersistent() exception caught trying to persist" + " dao of type ["
                + dao.getClass().getName() + "] with id: " + dao.getObjectId().getClass().getName()
                + " id value: " + dao.getObjectId() + " on datasource: " + getDataSourceName()
                + ". Statement: " + argCurrentSql + " Params: " + argCurrentParams;
      }
      else {
        msg =
            "makePersistent() exception caught trying to persist" + " persistable of type ["
                + argPersistable.getClass().getName() + "] on datasource: " + getDataSourceName()
                + ". Statement: " + argCurrentSql + " Params: " + argCurrentParams;
      }

      if (argException instanceof SQLException) {
        SQLException sqlEx = SQLExceptionScrubber.scrub((SQLException) argException);
        JDBCHelper.logSqlException(msg, sqlEx);
        throw new JDBCException(msg, sqlEx);
      }
      else {
        _logger.error(msg, argException);
        throw new DtxException(msg, argException);
      }
    }
  }

  /**
   * Handle an update not effective. This occurs when a SQL update runs against a database, but zero records
   * are affected. This typically is caused by the fields in the WHERE clause not matching any records in the
   * table.
   * 
   * @param argStatement the prepared statement that ran.
   * @param argSql the raw SQL that executed.
   * @param argPersistable the persistable object the system attempted to persist.
   * @throws SQLException if there is a database error accessing the supplied prepared statement.
   */
  protected void handleUpdateNotEffective(PreparedStatement argStatement, String argSql,
      IPersistable argPersistable)
      throws SQLException {

    if (argPersistable instanceof IDataAccessObject) {
      IDataAccessObject argDAO = (IDataAccessObject) argPersistable;

      if ((isUpdated(argDAO) || INSERT_OR_UPDATE.matches(argDAO)) && (argStatement.getUpdateCount() == 0)) {
        throw new UpdateNotEffectiveException("Update count was zero while persisting dao: ["
            + argDAO.getClass().getName() + "] id: " + argDAO.getObjectId() + " sql statement: " + argSql,
            getDataSourceName());
      }
    }
  }

  /**
   * Loads up all data for the full hierarchy of an object. For example, if the current object is TaskDAO.
   * query: 1. cwo_task 2. itm_non_phys_item and pull all of the data into the data object.
   * 
   * @param argDBAAdapter A loaded DB adapter - loaded with data from the top level parent table only.
   * 
   * @param argConn The current database connection we are working with.
   * 
   * @return A DB adapter loaded with data from all tables in the hierarchy, instead of just the top level
   * parent.
   * 
   * @exception SQLException
   */
  protected IDataModel loadHierarchy(IJDBCTableAdapter argDBAAdapter, Connection argConn)
      throws SQLException {

    IJDBCTableAdapter dbaAdapter = argDBAAdapter;
    IObjectId objectId = dbaAdapter.getObjectId();

    // If this adapter can be subclassed, determine the appropriate subclass to use
    if (dbaAdapter.isExtensible()) {
      String classNameRoot = dbaAdapter.getImplementingClass();
      String className = classNameRoot + "DBA";
      // If the classname defined in dtv_class_name is different
      // from the current DBA we're working with, reflectly load the
      // proper DBA.
      //
      // E.G. argDBAdapter could be "ItemDBA" while the classname is
      // "NonPhysicalItemDBA". In that case, load NonPhysicalItemDBA.
      if (!className.equals(dbaAdapter.getClass().getName())) {
        IJDBCTableAdapter originalAdapter = dbaAdapter;

        if (_debugLogging) {
          _logger.debug("Getting DBA implementation of " + dbaAdapter.getImplementingClass() + " from "
              + dbaAdapter.getClass().getName());
        }
        // create the appropriate adapter class
        try {
          dbaAdapter = (IExtendedJDBCAdapter) Class.forName(className).newInstance();
        }
        catch (Exception ee) {
          handleBadDtxClassName(dbaAdapter, classNameRoot, className, ee);
        }

        IExtendedJDBCAdapter subclass = (IExtendedJDBCAdapter) dbaAdapter;

        // copy over the already loaded data into the new adapter
        try {
          subclass.fill(originalAdapter);
        }
        catch (ClassCastException ee) {
          throw new DtxException("A failure occurred while calling fill on [" + subclass.getClass().getName()
              + "] with [" + dbaAdapter.getClass().getName() + "]  check dtv_class_name field - "
              + "it's probably invalid.", ee);
        }
      }
    }

    if (dbaAdapter instanceof IExtendedJDBCAdapter) {
      IExtendedJDBCAdapter subclass = (IExtendedJDBCAdapter) dbaAdapter;
      // load in each table in the hierarchy
      String[] selects = subclass.getAllSelects();
      IFiller[] fillers = subclass.getAllFillers();

      for (int i = 0; i < selects.length; i++ ) {
        String sql = selects[i] + dbaAdapter.getSelectWhere();
        sql = SqlQueryDecorator.decorateSql(sql, this, (IObjectId) null);

        PreparedStatement statement = argConn.prepareStatement(sql);
        statement = subclass.writeObjectId(objectId, statement);
        statement.execute();
        ResultSet resultSet = statement.getResultSet();

        if (resultSet.next()) {
          fillers[i].fill(resultSet);
        }
        else {
          _logger
              .warn("Unable to load a level in the hierarchy. This object will likely be incompletely loaded. "
                  + "SQL failed: "
                  + sql
                  + " object ID type: "
                  + objectId.getDtxTypeName()
                  + " object ID value: " + objectId);
        }
      }
    }

    if (dbaAdapter != null) {
      IDataAccessObject dao = dbaAdapter.loadDefaultDAO();
      IDataModelImpl model = DataModelFactory.getModelForDAO(dao);
      // Set the originating datasource.
      dao.setOriginDataSource(getDataSourceName());
      model.setDAO(dao);

      return model;
    }
    else {
      return null;
    }
  }

  /** {@inheritDoc} */
  @Override
  protected void makePersistentImpl(TransactionToken argTransToken, IPersistable argPersistable) {
    IPersistable persistable = applyPreObjectStateRules(argPersistable);
    List<JDBCCall> jdbcCalls = getJDBCCallData(persistable);

    if (jdbcCalls == null) { // No need to save if clean!
      return;
    }

    JDBCCall currentCall = null; // save for logging below.
    // Get the database connection
    Connection conn = JDBCDataSourceMgr.getInstance().getConnection(argTransToken, getDataSourceName());

    try {
      // execute for each sql statement provided by the adapter.
      for (JDBCCall jdbcCall : jdbcCalls) {
        currentCall = jdbcCall;
        PreparedStatement statement = conn.prepareStatement(jdbcCall.getSqlString());
        JDBCHelper.assignParameters(statement, jdbcCall.getParams(), jdbcCall.getTypes());
        statement.execute(); // Execute it...
        handleJtdsWorkAround(conn, statement);

        // Handle non-criticals errors/warnings.
        handleUpdateNotEffective(statement, jdbcCall.getSqlString(), persistable);

        // retrieving warnings turns out to be a bit expensive (on Oracle at least)
        // relegate to DEBUG only.
        if (_debugLogging) {
          boolean warnings = JDBCHelper.handleWarnings(jdbcCall.getSqlString(), statement.getWarnings());
          if (warnings) {
            statement.clearWarnings();
          }
        }
      }
    }

    // Error Handling
    catch (Exception ex) {
      markConnectionStale(conn);
      String sql = currentCall != null ? currentCall.getSqlString() : null;
      List<Object> params = currentCall != null ? currentCall.getParams() : null;
      Object result = handlePersistenceException(ex, persistable, sql, params);

      if (result instanceof FailoverException) {
        throw (FailoverException) result;
      }
      else if (result instanceof IPersistable) {
        // recurse. the rules may have changed the state of
        // the object, so we need to try to persist again.
        makePersistent(argTransToken, (IPersistable) result);
      }
      else {
        return;
      }
    }
    finally {
      if (conn != null) {
        try {
          conn.close();
        }
        catch (SQLException e) {
          _logger.error(e, new Throwable());
        }
      }
    }
  }

  /**
   * @param argConn
   */
  protected void markConnectionStale(Connection argConn) {
    if (argConn instanceof DBConnection) {
      ((DBConnection) argConn).setThrowAway();
    }
  }
}
