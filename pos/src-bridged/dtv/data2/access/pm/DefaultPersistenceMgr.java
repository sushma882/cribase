// $Id: DefaultPersistenceMgr.java 2497 2012-06-28 13:36:19Z dtvdomain\sbarrett $
package dtv.data2.access.pm;

import java.util.Map;

import org.apache.log4j.Logger;

import dtv.data2.access.*;
import dtv.data2.access.impl.IDataModelImpl;
import dtv.data2.access.impl.IPersistenceStrategy;
import dtv.data2.access.query.QueryRequest;
import dtv.data2.access.status.StatusMgr;
import dtv.data2.cache.*;

/**
 * Implementation of a persistence manager that utilizes a caching mechanism to look for requested objects
 * before resorting to looking to data sources for them.<br>
 * <br>
 * Copyright (c) 2005 Datavantage Corporation
 *
 * @created Feb 7, 2005
 * @author Jeff Sheldon
 * @version $Revision: 2497 $
 */
public class DefaultPersistenceMgr
    extends PersistenceManager {

  private static final Logger logger_ = Logger.getLogger(DefaultPersistenceMgr.class);
  private static final ICache _cache = CacheManager.getInstance().getCache("dtx-cache");
  private static final ICache _transCache = CacheManager.getInstance().getCache("dtx-cache-trans");

  /** {@inheritDoc} */
  @Override
  public IDataModel getObjectById(IObjectId argId, IPersistenceMgrType argType) {
    // Look for the object in the cache first.
    IDataModel result = (IDataModel) checkCache(_transCache, argId);

    if (result == null) {
      result = (IDataModel) checkCache(_cache, argId);
    }

    if (result == null) {
      // Perform the LIVE lookup & potentially cache
      try {
        result = super.getObjectById(argId, argType);
        cache(argId, result);
      }
      catch (ObjectNotFoundException ee) {
        // We catch ObjectNotFoundException instead of using a finally block because this is the
        // only exception for which cacheing is appropriate.
        cache(argId, result);
        throw ee;
      }
    }
    return result;
  }

  /** {@inheritDoc} */
  @Override
  public Object getObjectByQuery(String argQueryKey, Map<String, Object> argParams,
      IPersistenceMgrType argPmType) {

    QueryRequest request = new QueryRequest(argQueryKey, argParams, argPmType);

    // Look for the object in the cache first.
    Object result = checkCache(_transCache, request);

    if (result == null) {
      result = checkCache(_cache, request);
    }

    if (result == null) {
      // Perform the LIVE lookup & potentially cache
      try {
        result = super.getObjectByQuery(request);
        cache(request, result);
      }
      catch (ObjectNotFoundException ee) {
        // We catch ObjectNotFoundException instead of using a finally block because this is the
        // only exception for which cacheing is appropriate.
        cache(request, result);
        throw ee;
      }
    }
    return result;
  }

  protected void cache(Object argKey, Object argValue) {
    _transCache.put(argKey, argValue);

    // Don't cache NULLS if datasources are offline.
    if ((argValue != null) || (StatusMgr.getInstance().getOfflineCount() <= 0)) {
      _cache.put(argKey, argValue);
    }
  }

  /**
   * Checks the specified cache to determine if the object that is represented by <code>argCacheKey</code> is
   * cached currently.
   *
   * @param argCacheKey the value that represents the object for which the <code>argCache</code> is to be
   * checked
   * @return the cached object that is represented by <code>argCacheKey</code>; null if no object is cached
   */
  protected Object checkCache(ICache argCache, Object argCacheKey) {
    Object result = null;
    try {
      result = argCache.get(argCacheKey);

      if (logger_.isDebugEnabled()) {
        logger_.debug("Result found in cache for key [" + argCacheKey + "]; Object: " + result);
      }
      //CRI chanage.
      //      if (result == null) {
      //        throw new ObjectNotFoundException("Cache contained a null value for key [" + argCacheKey + "]");
      //      }
    }
    catch (NotCachedException ee) {
      if (logger_.isDebugEnabled()) {
        logger_.debug("Result NOT found in cache for key [" + argCacheKey + "]");
      }
    }
    return result;
  }

  /** {@inheritDoc} */
  @Override
  protected void loadRelationship(IDataModelRelationship argRel, IDataModelImpl argModel,
      IPersistenceStrategy argPreferredDataSource) {

    // Check the cache if this is a ONE-ONE relationship (where we can derive the object id).
    IObjectId childObjectId = null;

    if (IDataModelRelationship.RelationshipType.ONE_TO_ONE.equals(argRel.getType())) {
      argRel.setParent(argModel);
      childObjectId = argPreferredDataSource.getChildObjectIdForRelationship(argRel);

      // Look for the object in the cache first.
      if ((childObjectId != null) && childObjectId.validate()) {
        IDataModel peerModel = (IDataModel) checkCache(_transCache, childObjectId);

        if (peerModel == null) {
          peerModel = (IDataModel) checkCache(_cache, childObjectId);
        }

        if (peerModel != null) {
          argModel.setValue(argRel.getIdentifier(), peerModel);
          return;
        }
      }
      else {
        childObjectId = null;
      }
    }

    // Cacheing not applicable so far, perform the LIVE lookup
    super.loadRelationship(argRel, argModel, argPreferredDataSource);

    // Cache the result if this is ONE-ONE
    if (childObjectId != null) {
      cache(childObjectId, argModel.getValue(argRel.getIdentifier()));
    }
  }

  /** {@inheritDoc} */
  @Override
  protected IDataModel loadRelationships(IDataModel argDataModel, IPersistenceStrategy argPreferredDataSource) {
    // : Look for the object in the cache first. It's possible that this object is being loaded in
    // a ONE-MANY scenerio and that this is the first time we know the object id.
    IDataModel result = (IDataModel) checkCache(_transCache, argDataModel.getObjectId());

    if (result == null) {
      result = (IDataModel) checkCache(_cache, argDataModel.getObjectId());
    }

    if (result == null) {
      // Perform the graph load on this object and then cache.
      result = super.loadRelationships(argDataModel, argPreferredDataSource);
      cache(argDataModel.getObjectId(), result);
    }

    return result;
  }
}
