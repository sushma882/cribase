// $Id: AbstractThreadedAuthProcess.java 57479 2011-12-16 20:39:59Z dtvdomain\melghetany $
package dtv.tenderauth.impl;

import static dtv.util.StringUtils.isEmpty;
import static dtv.util.StringUtils.nonNull;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import dtv.data2.access.DataFactory;
import dtv.data2.access.exception.PersistenceException;
import dtv.hardware.auth.UserCancelledException;
import dtv.i18n.*;
import dtv.pos.common.OpExecutionException;
import dtv.pos.common.PersistablesBag;
import dtv.pos.framework.StationModelMgr;
import dtv.pos.iframework.IPersistablesBag;
import dtv.pos.iframework.type.TenderUsageCodeType;
import dtv.pos.iframework.ui.model.IAttributeModel;
import dtv.pos.pricing.discount.type.DiscountApplicationMethod;
import dtv.pos.register.type.LineItemType;
import dtv.pos.ui.text.TextFieldInputType;
import dtv.pos.ui.text.formatter.AbstractNumberFormatter;
import dtv.pos.ui.text.formatter.IDtvTextFormatter;
import dtv.tenderauth.*;
import dtv.tenderauth.config.AuthFailedActionTypesConfig;
import dtv.tenderauth.event.*;
import dtv.tenderauth.impl.event.*;
import dtv.util.*;
import dtv.util.config.*;
import dtv.util.xmlexport.ISafIterator;
import dtv.util.xmlexport.SerializedXmlQueue;
import dtv.xst.dao.dsc.IDiscount;
import dtv.xst.dao.tnd.ITender;
import dtv.xst.dao.tnd.TenderStatus;
import dtv.xst.dao.trl.*;
import dtv.xst.dao.ttr.*;

/**
 * abstract auth process with threading.<br>
 * Copyright (c) 2004 Datavantage Corporation
 * 
 * @author dberkland
 * @created February 19, 2004
 * @version $Revision: 57479 $
 */
public abstract class AbstractThreadedAuthProcess
    extends AbstractAuthProcess
    implements Runnable {

  private static final Logger logger_ = Logger.getLogger(AbstractThreadedAuthProcess.class);
  private static final Logger adminLogger_ = Logger.getLogger("dtv.xstore.comm.paysys");

  private static int threadIndex_ = 0;
  private static final int STAT_CLEAR = 0;
  private static final int STAT_LOCKED = 1;
  private static final int STAT_PROCESSING = 2;
  private static final String PARTIAL_APPROVAL_MESSAGE_ID = "PARTIAL_APPROVAL";

  private final Lock startLock_ = new ReentrantLock();
  private final AtomicBoolean locked_ = new AtomicBoolean();
  private final Properties manualAuthInputFormatterProperties_ = new Properties();
  private final Set<String> blockedAuthNumbers_ = new HashSet<String>();
  private final IAuthStatusListener loggingStatusListener_ = new IAuthStatusListener() {
    @Override
    public void statusChanged(IAuthStatus argStatus) {
      getAuthLog().debug(
          "STATUS(" + argStatus.getPercentComplete() + "):"
              + argStatus.getMessage().toString(OutputContextType.LOG));
    }
  };

  private Pattern manualAuthRegEx_ = null;
  private ITenderAuthRequest request_;
  private String manualAuthInputFormatterType_ = FormatterType.SIMPLE.toString();
  private String clearTrack1With_ = null;
  private String clearTrack2With_ = null;
  private long repollTimeMillis_ = 60000;
  private int status_ = STAT_CLEAR;
  private int manualAuthMinimumLength_ = 1;
  private int manualAuthMaximumLength_ = 30;
  private boolean isManualAuthAmountEditable_;
  private boolean isNegativeAmountHandledLocally_ = false;
  private boolean isVoidHandledLocally_ = false;
  private boolean mustManualAuthAmountMatch_;
  private boolean showAccountNumber_ = getDefaultShowAccountNumber();
  private boolean showExpirationDate_ = getDefaultShowExpirationDate();
  private boolean saveTrack1ForReturns_ = true;
  private boolean saveTrack2ForReturns_ = true;
  private boolean partialApprovals_ = false;
  private boolean trackBalance_ = false;

  /** {@inheritDoc} */
  @Override
  public void cancelRequest(IAuthRequest argRequest) {
    super.getAuthCommunicator().abortCommunications();

    IAuthorizableLineItem line = ((ITenderAuthRequest) argRequest).getLineItem();

    if ((line != null) && (line.getAdjudicationCode() == null)) {
      line.setAdjudicationCode(getAbortedAuthSuccessAdjucationCode());
    }
  }

  /** {@inheritDoc} */
  @Override
  public IAuthRequest getManualAuthInfo(IAuthRequest argRequest) {
    IFormattable message = getMessage(argRequest, getMessageKeyManual(argRequest));

    IAuthInfo manualAuthInfo = getManualAuthInfoObject(argRequest, message);
    argRequest.setMoreAuthInfo(manualAuthInfo);
    return argRequest;
  }

  /**
   * Determines if authorization that are in process are allowed be canceled by the user. This implementation
   * delegates to {@link IAuthRequest#isCancelAllowed()}.
   * 
   * @param argRequest the request to check for cancellability
   * @return <code>true</code> if the UI should allow the user to cancel an authorization that is being
   * processed, <code>false</code> otherwise.
   */
  @Override
  public boolean isCancelAuthAllowed(IAuthRequest argRequest) {
    return ((argRequest == null) || (argRequest.isCancelAllowed()));
  }

  /** {@inheritDoc} */
  @Override
  public final void processRequest(IAuthRequest argRequest) {
    if (argRequest == null) {
      throw new NullPointerException("null request");
    }
    addRequestToAttributeModel(argRequest);

    if (locked_.get()) {
      if (!(argRequest instanceof ISilentForwardRequest) && !(request_ instanceof ISilentForwardRequest)) {
        if (status_ == STAT_PROCESSING) {
          // throw new IllegalStateException("BUSY::" + status_);
          logger_.error("BUSY::" + status_);
        }
      }
    }
    startLock_.lock();
    try {
      if (locked_.get()) {
        synchronized (locked_) {
          try {
            locked_.wait(30000);
          }
          catch (InterruptedException ex) {
            // throw new IllegalStateException("BUSY", ex);
            logger_.error("BUSY::" + status_);
          }
        }
        if (locked_.get()) {
          // throw new IllegalStateException("BUSY");
          logger_.error("BUSY::" + status_);
        }
      }
      locked_.set(true);
      status_ = STAT_LOCKED;
      request_ = (ITenderAuthRequest) argRequest;
      request_.removeStatusListener(loggingStatusListener_);
      request_.addStatusListener(loggingStatusListener_);
      Thread t = new Thread(this, "AuthProcess[" + getAuthMethodCode() + "]-" + ++threadIndex_);
      t.start();
    }
    finally {
      startLock_.unlock();
    }
  }

  /**
   * This method performs the authorization on a seperate thread allowing user interaction to continue.
   */
  @Override
  public final void run() {
    try {
      status_ = STAT_PROCESSING;
      doProcess(request_);
    }
    catch (Throwable ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
      request_.addResponse(handleException(request_));
    }
    finally {
      clearLock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public void setParameter(ParameterConfig argConfig) {
    String name = argConfig.getName();
    IConfigObject value = argConfig.getValue();
    if ("isManualAuthAmountEditable".equalsIgnoreCase(name)) {
      isManualAuthAmountEditable_ = ConfigUtils.toBoolean(value);
    }
    else if ("manualAuthInput.formatter".equalsIgnoreCase(name)) {
      manualAuthInputFormatterType_ = value.toString();
    }
    else if (name.toLowerCase().startsWith("manualAuthInput.formatter".toLowerCase())) {
      String propName = name.substring("manualAuthInput.formatter".length() + 1);
      manualAuthInputFormatterProperties_.put(propName, value.toString());
    }
    else if ("manualAuthValidation.length.minimum".equalsIgnoreCase(name)) {
      manualAuthMinimumLength_ = ConfigUtils.toInt(value);
    }
    else if ("manualAuthValidation.length.maximum".equalsIgnoreCase(name)) {
      manualAuthMaximumLength_ = Math.min(ConfigUtils.toInt(value), 30);
      manualAuthInputFormatterProperties_.put( //
          IDtvTextFormatter.MAX_CHARACTERS, //
          String.valueOf(manualAuthMaximumLength_));
    }
    else if ("manualAuthValidation.regex".equalsIgnoreCase(name)) {
      manualAuthRegEx_ = java.util.regex.Pattern.compile(value.toString());
    }
    else if ("manualAuthValidation.blocked".equalsIgnoreCase(name)) {
      blockedAuthNumbers_.add(value.toString());
    }
    else if ("mustManualAuthAmountMatch".equalsIgnoreCase(name)) {
      mustManualAuthAmountMatch_ = ConfigUtils.toBoolean(value);
    }
    else if ("repollTimeMillis".equalsIgnoreCase(name)) {
      repollTimeMillis_ = ConfigUtils.toInt(value);
    }
    else if ("showAccountNumber".equalsIgnoreCase(name)) {
      showAccountNumber_ = ConfigUtils.toBoolean(value);
    }
    else if ("showExpirationDate".equalsIgnoreCase(name)) {
      showExpirationDate_ = ConfigUtils.toBoolean(value);
    }
    else if ("saveTrack1ForReturns".equalsIgnoreCase(name)) {
      saveTrack1ForReturns_ = ConfigUtils.toBoolean(value);
    }
    else if ("saveTrack2ForReturns".equalsIgnoreCase(name)) {
      saveTrack2ForReturns_ = ConfigUtils.toBoolean(value);
    }
    else if ("clearTrack1With".equalsIgnoreCase(name)) {
      if (value == null) {
        clearTrack1With_ = null;
      }
      else {
        clearTrack1With_ = value.toString();
        if ("null".equals(clearTrack1With_)) {
          clearTrack1With_ = null;
        }
      }
    }
    else if ("clearTrack2With".equalsIgnoreCase(name)) {
      if (value == null) {
        clearTrack2With_ = null;
      }
      else {
        clearTrack2With_ = value.toString();
        if ("null".equals(clearTrack2With_)) {
          clearTrack2With_ = null;
        }
      }
    }
    else if ("partialApprovals".equalsIgnoreCase(name)) {
      partialApprovals_ = ConfigUtils.toBoolean(value);
    }
    else if ("trackBalance".equalsIgnoreCase(name)) {
      trackBalance_ = ConfigUtils.toBoolean(value);
    }
    else if ("isVoidHandledLocally".equalsIgnoreCase(name)) {
      isVoidHandledLocally_ = ConfigUtils.toBoolean(value);
    }
    else if ("isNegativeAmountHandledLocally".equalsIgnoreCase(name)) {
      isNegativeAmountHandledLocally_ = ConfigUtils.toBoolean(value);
    }
    super.setParameter(argConfig);
  }

  /** {@inheritDoc} */
  @Override
  public void updateStoreAndForwardQueue() {
    for (ISafIterator iter = getSafQueue().iterator(); iter.hasNext();) {
      SerializedXmlQueue.Element element = iter.next();
      if ((element != null) && (!element.isFileReadyToProcess())) {
        element.setFileReadyToProcess(true);
      }
    }
  }

  protected void appendAuthInfo(StringBuilder sb, IAuthInfo authInfo) {
    boolean shouldMask = shouldMask(authInfo);
    appendNonNull(sb, "type", authInfo.getType());
    appendNonNull(sb, "msg", authInfo.getMessage());
    IAuthInfoField[] infoFields = authInfo.getInfoFields();

    if ((infoFields != null) && (infoFields.length > 0)) {
      sb.append("infoFields=[");
      for (IAuthInfoField f : infoFields) {
        sb.append(safeToString(f.getLabel()));
        sb.append(":");
        if (shouldMask || shouldMask(f.getFieldKey())) {
          sb.append(mask(f.getValue()));
        }
        else {
          sb.append(safeToString(f.getValue()));
        }
      }
      sb.append("];");
    }
    IAuthInputField[] inputFields = authInfo.getInputFields();
    if ((infoFields != null) && (infoFields.length > 0)) {
      sb.append("infoFields=[");
      for (IAuthInputField f : inputFields) {
        sb.append(safeToString(f.getFieldKey()));
        sb.append("(");
        sb.append(safeToString(f.getLabel()));
        sb.append(")=");
        if (shouldMask || shouldMask(f.getFieldKey())) {
          sb.append(mask(f.getValue()));
        }
        else {
          sb.append(safeToString(f.getValue()));
        }
      }
      sb.append("]");
    }
    sb.append("]");
  }

  protected void appendNonNull(StringBuilder sb, String argTag, Object argValue) {
    if (argValue == null) {
      return;
    }
    sb.append(";");
    sb.append(argTag);
    sb.append("=");
    sb.append(safeToString(argValue));
  }

  protected void appendRequiredInfo(StringBuilder sb, IAuthResponse argResponse) {
    IAuthInfo requiredInfo = argResponse.getRequiredInfo();
    if (requiredInfo == null) {
      return;
    }
    sb.append(";requiredInfo=[");
    appendAuthInfo(sb, requiredInfo);
  }

  protected boolean areCeilingLimitsExceeded(IAuthorizableLineItem argLineItem, String argTenderId,
      TenderUsageCodeType argUsage, boolean argOffline) {

    return getTenderAuthHelper().areCeilingLimitsExceeded(argLineItem, argTenderId, argUsage, argOffline);
  }

  protected boolean areFloorLimitsMet(IAuthorizableLineItem argLineItem, String argTenderId,
      TenderUsageCodeType argUsage, boolean argOffline) {

    return getTenderAuthHelper().areFloorLimitsMet(argLineItem, argTenderId, argUsage, argOffline);
  }

  protected int calcWaitPercent(long argWaitStartTime, long argWaitEndTime) {
    long total = argWaitEndTime - argWaitStartTime;
    long current = System.currentTimeMillis() - argWaitStartTime;
    return (int) ((100 * current) / total);
  }

  protected void clearLock() {
    locked_.set(false);
    status_ = STAT_CLEAR;
    synchronized (locked_) {
      locked_.notify();
    }

  }

  protected void clearTracksForManual(IAuthorizableLineItem argLineItem) {
    // do not save track data on successfully authorized cards (esp. not track2)
    clearTrack1(argLineItem);
    clearTrack2(argLineItem);
  }

  protected void copyBankReferenceNumber(IAuthorizableLineItem argLineItem, IAuthResponse response) {
    String responseBankReferenceNumber = response.getBankReferenceNumber();
    String lineBankReferenceNumber = argLineItem.getBankReferenceNumber();

    if (!isEmpty(responseBankReferenceNumber)) {
      if (!isEmpty(lineBankReferenceNumber)) {
        if (!responseBankReferenceNumber.equals(lineBankReferenceNumber)) {
          logger_.warn("overriding bank reference number from '" + lineBankReferenceNumber + "' to '"
              + responseBankReferenceNumber + "'");
        }
      }
      argLineItem.setBankReferenceNumber(responseBankReferenceNumber);
    }
  }

  protected void copyTokenInformation(IAuthorizableLineItem argLineItem, IAuthResponse argResponse) {
    String authorizationToken =
        !isEmpty(argResponse.getAuthorizationToken()) ? argResponse.getAuthorizationToken() : null;
    if (argLineItem != null) {
      if (!isEmpty(authorizationToken)) {
        if ((isTokenizationEnabled()) && (argLineItem instanceof ICreditDebitTenderLineItem)) {
          argLineItem.setAuthorizationToken(authorizationToken);
          ((ICreditDebitTenderLineItem) argLineItem).clearTokenInformation();
        }
      }
    }
  }

  protected void deleteSAFEntry(IAuthorizableLineItem argLineItem) {
    if (argLineItem == null) {
      return;
    }
    StringBuilder sb = new StringBuilder(argLineItem.getObjectIdAsString());
    StringUtils.replaceAll(sb, "::", "_");
    String lineName = sb.toString();
    for (Iterator<SerializedXmlQueue.Element> iter = getSafQueue().iterator(); iter.hasNext();) {
      if (iter.next().getFileName().contains(lineName)) {
        iter.remove();
      }
    }
  }

  protected void doProcess(final ITenderAuthRequest argRequest) {
    IAuthResponse response = null;

    argRequest.prepRequest();
    if (argRequest instanceof ISilentForwardRequest) {
      response = handleForward(argRequest);
    }
    // move this up ahead of VOID to allow a VOID to be manually authorized
    else if (argRequest.getMoreAuthInfo() != null) {
      StringBuilder sb = new StringBuilder();
      sb.append(getClass().getName()).append(".handleMoreAuthInfoResponse(");
      appendAuthInfo(sb, argRequest.getMoreAuthInfo());
      sb.append(")");
      logger_.info(sb);
      response = handleMoreAuthInfoResponse(argRequest);
    }
    else if (isVoid(argRequest)) {
      logger_.info(getClass().getName() + ".handleVoid");
      response = handleVoid(argRequest);
    }
    else {
      if (NumberUtils.isNegative(argRequest.getAmount())) {
        logger_.info(getClass().getName() + ".handleNegativeAmount");
        response = handleNegativeAmount(argRequest);
      }

      if (response != null) {
        // have a response already
      }
      else if (!isValidRequest(argRequest)) {
        logger_.warn("invalid request [" + argRequest + "] for " + getAuthMethodCode());
        response = handleException(argRequest);
      }
      else if (isOffline()) {
        logger_.info(getClass().getName() + ".handleOffline");
        response = handleOffline(argRequest);
      }
      else {
        logger_.info(getClass().getName() + ".handleOnline");
        response = handleOnline(argRequest);
      }
    }
    logger_.info(getClass().getName() + "->response::" + toLogString(response));
    argRequest.addResponse(response);
  }

  protected String getAccountNumber(IAuthRequest argAuthRequest) {
    IAuthorizableLineItem lineItem = argAuthRequest.getLineItem();
    if (lineItem instanceof ICreditDebitTenderLineItem) {
      return ((ICreditDebitTenderLineItem) lineItem).getAccountNumber();
    }
    else if (lineItem instanceof ICheckTenderLineItem) {
      return ((ICheckTenderLineItem) lineItem).getCheckAccountNumber();
    }
    else {
      return lineItem.getSerialNumber();
    }
  }

  protected BigDecimal getAmount(IAuthRequest argAuthRequest) {
    return argAuthRequest.getLineItem().getAmount();
  }

  protected void getAuthFailedActions(IAuthResponse response) {
    String responseCode = response.getResponseCode();
    AuthFailedActionTypesConfig c = getAuthFailedActionTypesConfig(responseCode);
    AuthFailedActionType[] availableActions = getAuthFailedActionTypes(c, response);
    response.setAvailableActions(availableActions);
    response.setDataActionGroup(c.getDataActionGroupKey());
  }

  protected String getCheckNumber(IAuthRequest argAuthRequest) {
    IAuthorizableLineItem lineItem = argAuthRequest.getLineItem();
    return (lineItem instanceof ICheckTenderLineItem) //
        ? ((ICheckTenderLineItem) lineItem).getCheckSequenceNumber() //
        : null;
  }

  protected IAuthResponse getCidReentryResponse(ITenderAuthRequest argRequest) {
    IAuthInputField[] inputFields =
        new IAuthInputField[] {new AuthInputField(AuthInfoFieldKey.CID,
            FF.getTranslatable("_ajbCreditReentryCIDNumber"))};

    IFormattable message = getMessage(argRequest, AuthInfoType.CID_ENTRY);
    AuthInfo cidRentryInfo = new AuthInfo(AuthInfoType.CID_ENTRY, message, inputFields);

    return new AuthTenderMoreInfoNeededResponse(this, argRequest, cidRentryInfo);
  }

  protected boolean getDefaultShowAccountNumber() {
    return true;
  }

  protected boolean getDefaultShowExpirationDate() {
    return true;
  }

  protected Date getExpirationDate(IAuthRequest argAuthRequest) {
    IAuthorizableLineItem lineItem = argAuthRequest.getLineItem();
    return (lineItem instanceof ICreditDebitTenderLineItem) //
        ? ((ICreditDebitTenderLineItem) lineItem).getExpirationDate() //
        : null;
  }

  protected IAuthResponse getFailedResponse(ITenderAuthRequest argRequest, Object argMessageTarget,
      String... argMessageKey) {

    IFormattable message = getMessage(argMessageTarget, argMessageKey);
    IAuthResponse response = new AuthTenderFailedResponse(this, argRequest, message);
    AuthFailedActionTypesConfig c = getAuthFailedActionTypesConfig(argMessageKey);
    AuthFailedActionType[] availableActions = getAuthFailedActionTypes(c, response);
    response.setAvailableActions(availableActions);
    response.setDataActionGroup(c.getDataActionGroupKey());
    return response;
  }

  protected boolean getIgnoreFailure(ITenderAuthRequest argRequest) {
    return false;
  }

  protected BigDecimal getManualAuthAmount(IAuthInfo argManualAuthInfo) {
    return argManualAuthInfo.getInputFieldValue(AuthInfoFieldKey.AMOUNT, BigDecimal.class);
  }

  protected IAuthInfoField[] getManualAuthInfoFields(IAuthRequest argRequest) {
    List<IAuthInfoField> fields = new LinkedList<IAuthInfoField>();

    IFormattable merchantNumber = getMerchantNumber();
    if ((merchantNumber != IFormattable.EMPTY) && (merchantNumber != null)) {
      fields.add(new AuthInfoField(AuthInfoFieldKey.MERCHANT_NUMBER, //
          FF.getTranslatable("_authManualMerchantNumber"), //
          merchantNumber));
    }

    String transitNumber = getTransitNumber(argRequest);
    if (transitNumber != null) {
      fields.add(new AuthInfoField(AuthInfoFieldKey.TRANSIT_NUMBER, //
          FF.getTranslatable("_authManualTransitNumber"), //
          FF.getLiteral(transitNumber)));
    }

    String accountNumber = getAccountNumber(argRequest);
    if (getShowAccountNumber(accountNumber)) {
      fields.add(new AuthInfoField(AuthInfoFieldKey.ACCT_NUMBER, //
          FF.getTranslatable("_authManualAccountNumber"), //
          FF.getLiteral(accountNumber)));
    }

    String checkNumber = getCheckNumber(argRequest);
    if (checkNumber != null) {
      fields.add(new AuthInfoField(AuthInfoFieldKey.SERIAL_NUMBER, //
          FF.getTranslatable("_authManualCheckNumber"), //
          FF.getLiteral(checkNumber)));
    }

    Date expirationDate = getExpirationDate(argRequest);
    if (getShowExpirationDate(expirationDate)) {
      fields.add(new AuthInfoField(AuthInfoFieldKey.EXP_DATE, //
          FF.getTranslatable("_authManualExpDate"), //
          FF.getSimpleFormattable(expirationDate, FormatterType.DATE_EXP_LONG)));
    }

    if (!isManualAuthAmountEditable()) {
      BigDecimal amount = getAmount(argRequest);
      if (amount != null) {
        fields.add(new AuthInfoField(AuthInfoFieldKey.AMOUNT, //
            FF.getTranslatable("_authManualAmount"), //
            FF.getSimpleFormattable(amount, FormatterType.MONEY)));
      }
    }
    return fields.toArray(new IAuthInfoField[fields.size()]);
  }

  protected IAuthInfo getManualAuthInfoObject(IAuthRequest argRequest, final IFormattable argMessage) {
    final IAuthInfoField[] infoFields = getManualAuthInfoFields(argRequest);
    final IAuthInputField[] inputFields = getManualAuthInputFields(argRequest);

    return AuthInfo.forManual(argMessage, infoFields, inputFields);
  }

  protected IAuthInputField[] getManualAuthInputFields(IAuthRequest argRequest) {
    List<IAuthInputField> fields = new LinkedList<IAuthInputField>();

    if (isManualAuthAmountEditable()) {
      Properties formatterSettings = new Properties();
      formatterSettings.setProperty(AbstractNumberFormatter.ALLOW_NEGATIVE, Boolean.FALSE.toString());
      AuthInputField amountInputField =
          new AuthInputField(AuthInfoFieldKey.AMOUNT, FF.getTranslatable("_authManualAmount"),
              TextFieldInputType.MONEY.toString(), formatterSettings);

      IAuthorizableLineItem lineItem = argRequest.getLineItem();
      BigDecimal amount = NumberUtils.nonNull(lineItem.getAmount()).abs();
      amountInputField.setValue(amount);
      fields.add(amountInputField);
    }
    fields.add(new AuthInputField(AuthInfoFieldKey.AUTH_NUMBER, FF.getTranslatable("_authManualAuthNumber"),
        getManualAuthNumberInputFieldFormatterType(), getManualAuthNumberInputFieldFormatterProperties()));

    return fields.toArray(new IAuthInputField[fields.size()]);
  }

  protected Object getManualAuthNumber(IAuthInfo argManualAuthInfo) {
    return argManualAuthInfo.getInputFieldValue(AuthInfoFieldKey.AUTH_NUMBER, Object.class);
  }

  protected Properties getManualAuthNumberInputFieldFormatterProperties() {
    return new Properties(manualAuthInputFormatterProperties_);
  }

  protected String getManualAuthNumberInputFieldFormatterType() {
    return manualAuthInputFormatterType_;
  }

  protected String getPartialApprovalMessageId() {
    return PARTIAL_APPROVAL_MESSAGE_ID;
  }

  protected String getReportBalancePropertyName() {
    return "REPORTED_BALANCE";
  }

  protected boolean getShowAccountNumber(String argAccountNumber) {
    if (argAccountNumber == null) {
      return false;
    }
    return showAccountNumber_;
  }

  protected boolean getShowExpirationDate(Date argExpirationDate) {
    if (argExpirationDate == null) {
      return false;
    }
    return showExpirationDate_;
  }

  protected ITenderAuthHelper getTenderAuthHelper() {
    return TenderAuthHelper.getInstance();
  }

  protected IFormattable getTenderDescription(ITenderAuthRequest argRequest) {
    return FF.getLiteral(argRequest.getTender().getDescription());
  }

  protected String getTrack1(IAuthorizableLineItem argLine) {
    if (argLine instanceof ICreditDebitTenderLineItem) {
      return ((ICreditDebitTenderLineItem) argLine).getTrack1();
    }
    if (argLine instanceof IVoucherLineItem) {
      return ((IVoucherLineItem) argLine).getTrack1();
    }
    return null;
  }

  protected String getTrack2(IAuthorizableLineItem argLine) {
    if (argLine instanceof ICreditDebitTenderLineItem) {
      return ((ICreditDebitTenderLineItem) argLine).getTrack2();
    }
    if (argLine instanceof IVoucherLineItem) {
      return ((IVoucherLineItem) argLine).getTrack2();
    }
    return null;
  }

  protected boolean getTrackBalance() {
    return trackBalance_;
  }

  protected String getTransitNumber(IAuthRequest argAuthRequest) {
    IAuthorizableLineItem lineItem = argAuthRequest.getLineItem();
    return (lineItem instanceof ICheckTenderLineItem) //
        ? ((ICheckTenderLineItem) lineItem).getBankId() //
        : null;
  }

  protected String getUnknownResponseMessageId() {
    return "RESPONSE_CODE_OTHER";
  }

  /**
   * Nothing implemented at this level, but concrete implementations can return a response that either
   * prevents over the limit transactions from being processed or uses a manual authorization.
   * 
   * @param argRequest the current request
   * @param argOffline
   * @return always <code>null</code> from {@link AbstractThreadedAuthProcess}
   */
  protected IAuthResponse handleCeilingLimitExceeded(ITenderAuthRequest argRequest, boolean argOffline) {
    return null;
  }

  protected IAuthResponse handleException(ITenderAuthRequest argRequest) {
    return getFailedResponse(argRequest, argRequest, getMessageKeyException(argRequest));
  }

  protected IAuthResponse handleFloorLimitMet(ITenderAuthRequest argRequest, boolean argOffline) {
    IAuthorizableLineItem lineItem = argRequest.getLineItem();
    String adjudicationCode = getFloorAuthAdjudicationCode(argRequest, argOffline);
    lineItem.setAdjudicationCode(adjudicationCode);
    lineItem.setAuthorizationCode(getFloorAuthorizationCode(argRequest, argOffline));

    return new AuthSuccessResponse(this, argRequest, getAuthMethodCode(), adjudicationCode);
  }

  protected IAuthResponse handleForward(ITenderAuthRequest argRequest) {
    try {
      IAuthResponse response = getAuthCommunicator().sendRequest(argRequest);
      if (response.isSuccess()) {
        if (isTokenizationEnabled()) {
          copyTokenInformation(argRequest.getLineItem(), response);
        }
        copyBankReferenceNumber(argRequest.getLineItem(), response);
        persistAuthorizableLineItem(argRequest.getLineItem());
      }

      return response;
    }
    catch (Exception ex) {
      // log problem
      getAuthLog().warn("CAUGHT EXCEPTION", ex);
      adminLogger_.error("CAUGHT EXCEPTION" + ex);
      return getFailedResponse(argRequest, argRequest, new String[0]);
    }
  }

  protected IAuthResponse handleInvalidManualAuthNumber(ITenderAuthRequest argRequest, String argAuthNumber) {
    return getFailedResponse(argRequest, argAuthNumber, getMessageKeyInvalidAuthNumber(argRequest));
  }

  protected IAuthResponse handleLocalNegative(ITenderAuthRequest argRequest) {
    IAuthorizableLineItem lineItem = argRequest.getLineItem();
    String adjucationCode = getReturnFloorAuthAdjudicationCode(argRequest, isOffline());
    lineItem.setAdjudicationCode(adjucationCode);

    if (!saveTrack1ForReturns_) {
      clearTrack1(lineItem);
    }
    if (!saveTrack2ForReturns_) {
      clearTrack2(lineItem);
    }
    return new AuthSuccessResponse(this, argRequest, getAuthMethodCode(), adjucationCode);
  }

  /**
   * Handle the processing a manual authorization after the auth number has been keyed and submitted.
   * 
   * @param argRequest the request that will contain the manual auth information.
   * @return success or failure depending on whether the manual authorization information entered was
   * acceptable or not
   */
  protected IAuthResponse handleManual(ITenderAuthRequest argRequest) {
    Object v = getManualAuthNumber(argRequest.getMoreAuthInfo());

    if (validAuthNumber(argRequest, v)) {
      Date startTimestamp = DateUtils.getNewDate();
      // copy the information from the manual auth info on the argRequest
      IAuthorizableLineItem lineItem = argRequest.getLineItem();
      final BigDecimal originalAmount = (lineItem.getAmount() != null) ? lineItem.getAmount().abs() : null;
      BigDecimal amount = originalAmount;
      if (isManualAuthAmountEditable()) {
        amount = getManualAuthAmount(argRequest.getMoreAuthInfo());
        if (amount == null) {
          return getFailedResponse(argRequest, argRequest, "INVALID_AUTH_AMOUNT");
        }

        if (mustManualAuthAmountMatch()) {
          BigDecimal manuallyAuthorizedAmount = getManualAuthAmount(argRequest.getMoreAuthInfo());
          if (!NumberUtils.equivalent(originalAmount, manuallyAuthorizedAmount)) {
            return getFailedResponse(argRequest, argRequest, "MISMATCH_AUTH_AMOUNT");
          }
        }

        // don't allow the amount to be more than the requested amount
        if (originalAmount != null) {
          amount = originalAmount.min(amount);
        }
        setAuthorizedAmount(lineItem, amount);
      }

      updateLineForManual(lineItem, v.toString());

      IAuthResponse response = null;
      try {
        response = storeManualAuthForForwarding(argRequest);
      }
      catch (Exception ex) {
        logger_.error("CAUGHT EXCEPTION", ex);
        return handleException(argRequest);
      }
      clearTracksForManual(lineItem);

      String authorizationCode = lineItem.getAuthorizationCode();
      String adjudicationCode = lineItem.getAdjudicationCode();
      if (response == null) {
        response =
            argRequest.getSuccessfulManualAuthResponse(this, amount, authorizationCode, adjudicationCode);
      }
      else if (!response.isSuccess()) {
        List<String> ids = new ArrayList<String>();
        ids.addAll(Arrays.asList(argRequest.getMessageKeys(response)));
        ids.add(getUnknownResponseMessageId());
        response.setMessage(getMessage(response, ids));
        getAuthFailedActions(response);
      }
      response.setStartTimestamp(startTimestamp);
      response.setEndTimestamp(DateUtils.getNewDate());

      updateLineAmount(argRequest, response, lineItem);

      return response;
    }
    else {
      logger_.info(getClass().getName() + ".handleInvalidManualAuthNumber");
      return handleInvalidManualAuthNumber(argRequest, StringUtils.nonNull(v));
    }
  }

  protected IAuthResponse handleMoreAuthInfoResponse(ITenderAuthRequest argRequest) {
    IAuthInfo moreAuthInfo = argRequest.getMoreAuthInfo();

    if (AuthInfoType.CID_ENTRY.equals(moreAuthInfo.getType())) {
      // we constructed this with only one input field (see the "getCidReentryResponse" method above),
      // so we can just grab directly from that know structure
      String newCid = moreAuthInfo.getInputFields()[0].getValue().toString();
      argRequest.processReenteredCid(newCid);
      return handleOnline(argRequest);
    }
    else if (AuthInfoType.MANUAL_AUTH.equals(moreAuthInfo.getType())) {
      return handleManual(argRequest);
    }
    throw new UnsupportedInfoTypeException(moreAuthInfo);
  }

  protected IAuthResponse handleNegativeAmount(ITenderAuthRequest argRequest) {
    if (!isCreditToAccountAllowed()) {
      IFormattable tenderName = getTenderDescription(argRequest);
      IFormattable message = FF.getTranslatable("_xpayAuthProcessFailedCreditNotAllowed", tenderName);
      AuthTenderFailedResponse response = new AuthTenderFailedResponse(this, request_, message);
      response.allowCancel();
      return response;
    }
    else if (isNegativeAmountHandledLocally()) {
      return handleLocalNegative(argRequest);
    }
    else {
      return null;
    }
  }

  protected IAuthResponse handleNoAuthorizationToken(ITenderAuthRequest argRequest) {
    deleteSAFEntry(argRequest.getLineItem());

    return new AuthSuccessResponse(this, argRequest, getAuthMethodCode(), "NO_TOKEN");
  }

  protected IAuthResponse handleOffline(ITenderAuthRequest argRequest) {
    IAuthorizableLineItem lineItem = argRequest.getLineItem();
    ITender tender = argRequest.getTender();
    final String tenderId = (tender == null) ? null : tender.getTenderId();

    IAuthResponse response = null;
    if (areFloorLimitsMet(lineItem, tenderId, argRequest.getTenderUsageCode(), /* offline= */true)) {
      logger_.info(getClass().getName() + ".handleFloorLimitMet(offline)");
      response = handleFloorLimitMet(argRequest, /* offline= */true);
    }
    else if (areCeilingLimitsExceeded(lineItem, tenderId, argRequest.getTenderUsageCode(), /* offline= */true)) {
      logger_.info(getClass().getName() + ".handleCeilingLimitExceeded(offline)");
      response = handleCeilingLimitExceeded(argRequest, /* offline= */true);
    }
    if (response != null) {
      return response;
    }
    logger_.info(getClass().getName() + ".getFailedResponse(offline)");

    return getFailedResponse(argRequest, argRequest, getMessageKeyOffline(argRequest));
  }

  protected IAuthResponse handleOfflineVoid(ITenderAuthRequest argRequest) {
    if (getIgnoreFailure(argRequest)) {
      /** @todo - RDB put it in a bucket to be processed by hand */
      return new AuthSuccessResponse(this, argRequest, null, "IGNORED_FAILURE");
    }
    else {
      return getFailedResponse(argRequest, argRequest, getMessageKeyOffline(argRequest));
    }
  }

  /**
   * Handle an authorization when the processor is online.
   * 
   * @param argRequest request to process
   * @return response
   */
  protected IAuthResponse handleOnline(ITenderAuthRequest argRequest) {
    IAuthorizableLineItem lineItem = argRequest.getLineItem();
    ITender tender = argRequest.getTender();

    if (tender != null) {
      IAuthResponse response = null;

      if (areFloorLimitsMet(lineItem, tender.getTenderId(), argRequest.getTenderUsageCode(), /* offline= */
      false)) {

        logger_.info(getClass().getName() + ".handleFloorLimitMet(online)");
        response = handleFloorLimitMet(argRequest,/* offline= */false);
      }
      else if (areCeilingLimitsExceeded(lineItem, tender.getTenderId(), argRequest.getTenderUsageCode(),
      /* offline= */false)) {

        logger_.info(getClass().getName() + ".handleCeilingLimitExceeded(online)");
        response = handleCeilingLimitExceeded(argRequest, /* offline= */false);
      }
      if (response != null) {
        return response;
      }
    }
    try {
      logger_.info(getClass().getName() + ".handleOnline->sendRequest");
      IAuthResponse response = getAuthCommunicator().sendRequest(argRequest);

      if (argRequest instanceof ICreditAuthRequest) {
        if (response instanceof ICreditAuthResponse) {
          ICreditAuthResponse resp = (ICreditAuthResponse) response;
          ICreditDebitTenderLineItem tenderLine = (ICreditDebitTenderLineItem) lineItem;
          tenderLine.setPs2000(resp.getPs2000());

          // If there is any PosDataCode, create a line item property to store the value.
          if (resp.getPosDataCode() != null) {
            tenderLine.setStringProperty("POS_DATA_CODE_VALUE", resp.getPosDataCode());
          }

        }
      }
      // check if the response requires us to prompt for the cashier to try typing the CID again
      if (response.isReenteringCID()) {
        logger_.info(getClass().getName() + ".getCidReentryResponse");
        return getCidReentryResponse(argRequest);
      }

      // pass the response to updateLineItemForOnline; if we get a RepollResponse
      // send the request back to the communicator; if not, return the response
      while ((response = updateLineItemForOnline(argRequest, response, lineItem)) instanceof RepollResponse) {
        logger_.info(getClass().getName() + ".handleOnline->sendRequest(repoll)");
        response = getAuthCommunicator().sendRequest(((RepollResponse) response).getRepollRequest());
      }
      if (argRequest instanceof ISuspendableAuthRequest) {
        ((ISuspendableAuthRequest) argRequest).setPending(false);
      }
      return response;
    }
    catch (ReceiveTimeoutException ex) {
      // log problem
      getAuthLog().warn("Timeout waiting for response: " + ex);
      adminLogger_.error("Timeout Waiting For Response: " + ex);
      return handleReceiveTimeout(argRequest);
    }
    catch (OfflineException ex) {
      // log problem
      getAuthLog().warn("Host offline: " + ex);
      adminLogger_.error("Payment Systems Host Offline: " + ex);
      return handleOffline(argRequest);
    }
    catch (UserCancelledException ex) {
      return handleUserCancel(argRequest);
    }
  }

  protected IAuthResponse handleOnlineVoid(ITenderAuthRequest argRequest) {
    IAuthorizableLineItem lineItem = argRequest.getLineItem();
    ITender tender = argRequest.getTender();
    if (tender != null) {
      IAuthResponse response = null;

      if (areFloorLimitsMet(lineItem, tender.getTenderId(), TenderUsageCodeType.VOID, /* offline= */false)) {
        response = handleFloorLimitMet(argRequest,/* offline= */false);
      }
      else if (areCeilingLimitsExceeded(lineItem, tender.getTenderId(), TenderUsageCodeType.VOID,
      /* offline= */false)) {

        response = handleCeilingLimitExceeded(argRequest, /* offline= */false);
      }
      if (response != null) {
        return response;
      }
    }
    try {
      IAuthResponse response = getAuthCommunicator().sendRequest(argRequest);
      return updateLineItemForOnlineVoid(argRequest, response, lineItem);
    }
    catch (ReceiveTimeoutException ex) {
      // log problem
      getAuthLog().warn("CAUGHT EXCEPTION", ex);
      adminLogger_.error("CAUGHT EXCEPTION: " + ex);
      return handleReceiveTimeout(argRequest);
    }
    catch (OfflineException ex) {
      // log problem
      getAuthLog().warn("CAUGHT EXCEPTION", ex);
      adminLogger_.error("CAUGHT EXCEPTION: " + ex);
      return handleOfflineVoid(argRequest);
    }
    catch (UserCancelledException ex) {
      return handleUserCancel(argRequest);
    }
  }

  protected IAuthResponse handlePending(ISuspendableAuthRequest argRequest) {
    argRequest.setPending(true);
    long waitStartTime = System.currentTimeMillis();
    long waitEndTime = waitStartTime + repollTimeMillis_;

    while (!argRequest.isSuspending() && (waitEndTime > System.currentTimeMillis())) {
      try {
        int pct = calcWaitPercent(waitStartTime, waitEndTime);
        AuthStatus authStatus = new AuthStatus("_ajbPrivateAuthPending", pct);
        argRequest.notifyStatusListeners(authStatus);
        Thread.sleep(100);
      }
      catch (InterruptedException ex) {
        logger_.debug("interrupted", ex);
      }
    }
    if (argRequest.isSuspending()) {
      return makeSuspendingResponse(argRequest);
    }
    else {
      return new RepollResponse(this, argRequest);
    }
  }

  protected IAuthResponse handleReceiveTimeout(ITenderAuthRequest argRequest) {
    return handleOffline(argRequest);
  }

  protected IAuthResponse handleUserCancel(ITenderAuthRequest argRequest) {
    return getFailedResponse(argRequest, argRequest, getMessageKeyUserCancel(argRequest));
  }

  protected IAuthResponse handleVoid(ITenderAuthRequest argRequest) {
    if (isVoidHandledLocally()) {
      IAuthorizableLineItem tl = argRequest.getLineItem();
      String adjucationCode = getVoidAuthAdjudicationCode(argRequest, /* offline */false);
      if (tl != null) {
        tl.setAdjudicationCode(adjucationCode);
      }
      return new AuthSuccessResponse(this, argRequest, getAuthMethodCode(), adjucationCode);
    }
    else if (!isValidRequest(argRequest)) {
      logger_.warn("invalid request [" + argRequest + "]");
      return handleException(argRequest);
    }
    else if (isAuthorizationTokenNotAvailable(argRequest)) {
      return handleNoAuthorizationToken(argRequest);
    }
    else if (argRequest.getMoreAuthInfo() != null) {
      return handleManual(argRequest);
    }
    else if (isOffline()) {
      return handleOfflineVoid(argRequest);
    }
    else {
      return handleOnlineVoid(argRequest);
    }
  }

  protected boolean isAuthorizationTokenNotAvailable(ITenderAuthRequest argRequest) {
    if (isTokenizationEnabled()) {
      IAuthorizableLineItem lineItem = argRequest.getLineItem();
      String authToken = lineItem != null ? nonNull(lineItem.getAuthorizationToken()) : "";
      if (isEmpty(authToken)) {
        return true;
      }
    }
    return false;
  }

  protected boolean isManualAuthAmountEditable() {
    return isManualAuthAmountEditable_;
  }

  protected boolean isNegativeAmountHandledLocally() {
    return isNegativeAmountHandledLocally_;
  }

  protected abstract boolean isValidRequest(ITenderAuthRequest argRequest);

  protected boolean isVoid(ITenderAuthRequest argRequest) {
    AuthRequestType type = argRequest.getRequestType();
    if (type == AuthRequestType.VOID_ACTIVATE) {
      return true;
    }
    if (type == AuthRequestType.VOID_RELOAD) {
      return true;
    }
    if (type == AuthRequestType.VOID_TENDER) {
      return true;
    }
    return false;
  }

  protected boolean isVoidHandledLocally() {
    return isVoidHandledLocally_;
  }

  protected IAuthResponse makeSuspendingResponse(ISuspendableAuthRequest argRequest) {
    throw new UnsupportedOperationException("this IAuthProcess implementation (" + getClass().getName()
        + ") does not support suspending");
  }

  protected String mask(Object o) {
    String result;
    if (o instanceof IFormattable) {
      result = ((IFormattable) o).toString(OutputContextType.LOG);
    }
    else if (o == null) {
      return "null";
    }
    else {
      result = String.valueOf(o);
    }
    return StringUtils.fill("*", result.length());
  }

  /**
   * Gets whether or not entered tender amount must equal manual authorization amount.
   * 
   * @return boolean - true, they must be equal, false otherwise
   */
  protected boolean mustManualAuthAmountMatch() {
    return mustManualAuthAmountMatch_;
  }

  protected boolean partialApprovals() {
    return partialApprovals_;
  }

  protected String safeToString(Object o) {
    if (o instanceof IFormattable) {
      return ((IFormattable) o).toString(OutputContextType.LOG);
    }
    return String.valueOf(o);
  }

  protected void setApprovedMessage(IAuthResponse argResponse) {
    if (argResponse.getMessage() == null) {
      List<String> messageKeys = new ArrayList<String>();
      BigDecimal requestAmount=argResponse.getRequest().getAmount();
      if(requestAmount!=null)
      {
        requestAmount=requestAmount.abs();
      }
      messageKeys.addAll(Arrays.asList(argResponse.getRequest().getMessageKeys(argResponse)));
      if (partialApprovals()
          && !NumberUtils.equivalent(argResponse.getAmount(),requestAmount)) {
        messageKeys.add(getPartialApprovalMessageId());
      }

      IFormattable message = buildMessage(false, argResponse, messageKeys.toArray(new String[0]));

      if (message != null) {
        argResponse.setMessage(message);
      }
    }
  }

  protected void setAuthorizedAmount(IAuthorizableLineItem argLineItem, BigDecimal argAmount) {
    if (argLineItem instanceof IAuthorizableTenderLineItem) {
      IAuthorizableTenderLineItem tenderLine = ((IAuthorizableTenderLineItem) argLineItem);
      tenderLine.setAmount(argAmount);
    }
    else if (argLineItem instanceof IVoucherLineItem) {
      IVoucherLineItem tenderLine = ((IVoucherLineItem) argLineItem);
      tenderLine.setFaceValueAmount(argAmount);
      tenderLine.setUnspentBalanceAmount(argAmount);
    }
    else {
      logger_.error("unable to set the amount on line of type "
          + ObjectUtils.getClassNameFromObject(argLineItem));
    }
  }

  protected void setLineAmount(ITenderAuthRequest argRequest, IAuthResponse argResponse,
      IAuthorizableLineItem argLine, BigDecimal argValue) {

    if (argLine instanceof IVoucherLineItem) {
      setVoucherLineAmount(argRequest, argResponse, (IVoucherLineItem) argLine, argValue);
    }
    else if (argLine instanceof ITenderLineItem) {
      setTenderLineAmount(argRequest, argResponse, (ITenderLineItem) argLine, argValue);
    }
    else {
      logger_.warn("unable to set amount on " + ObjectUtils.getClassNameFromObject(argLine));
    }
  }

  protected void setTenderLineAmount(ITenderAuthRequest argRequest, IAuthResponse argResponse,
      ITenderLineItem argLine, BigDecimal argValue) {

    String tenderStatusCode = argLine.getTenderStatusCode();
    if ((TenderStatus.CHANGE.equals(tenderStatusCode)) || (TenderStatus.REFUND.equals(tenderStatusCode))) {
      // We need to record a negative amount if we are issuing a gift card as refund or change tender.
      argValue = argValue.negate();
    }

    argLine.setAmount(argValue);
  }

  protected void setTrack1(IAuthorizableLineItem argLine, String argValue) {
    if (argLine instanceof ICreditDebitTenderLineItem) {
      ((ICreditDebitTenderLineItem) argLine).setTrack1(argValue);
    }
    else if (argLine instanceof IVoucherLineItem) {
      ((IVoucherLineItem) argLine).setTrack1(argValue);
    }
  }

  protected void setTrack2(IAuthorizableLineItem argLine, String argValue) {
    if (argLine instanceof ICreditDebitTenderLineItem) {
      ((ICreditDebitTenderLineItem) argLine).setTrack2(argValue);
    }
    else if (argLine instanceof IVoucherLineItem) {
      ((IVoucherLineItem) argLine).setTrack2(argValue);
    }
  }

  protected void setVoucherDiscountLineAmount(ITenderAuthRequest argRequest, IAuthResponse argResponse,
      IVoucherDiscountLineItem argLine, BigDecimal argValue) {
    argLine.setFaceValueAmount(argValue);

    // if the discount is a line item discount, we need to find the modifier and set its amount
    IDiscount discount = argLine.getLineItemDiscount();
    if (DiscountApplicationMethod.LINE_ITEM.getName().equals(discount.getApplicationMethodCode())) {
      List<ISaleReturnLineItem> items =
          argLine.getParentTransaction().getLineItemsByTypeCode(LineItemType.ITEM.getName(),
              ISaleReturnLineItem.class);
      for (ISaleReturnLineItem item : items) {

        if (item.getVoid()) {
          continue;
        }
        List<IRetailPriceModifier> mods =
            item.getRetailPriceModifierByTypeCode(RetailPriceModifierReasonCode.LINE_ITEM_DISCOUNT.getName());
        for (IRetailPriceModifier mod : mods) {
          if (mod.getVoid()) {
            continue;
          }
          if (mod.getDiscount().equals(discount)) {
            // found it
            mod.setAmount(argValue);
            return;
          }
        }
      }
    }
  }

  protected void setVoucherLineAmount(ITenderAuthRequest argRequest, IAuthResponse argResponse,
      IVoucherLineItem argLine, BigDecimal argValue) {

    if (argLine instanceof IVoucherRedeemedLineItem) {
      setVoucherRedeemedLineAmount(argRequest, argResponse, (IVoucherRedeemedLineItem) argLine, argValue);
    }
    else if (argLine instanceof IVoucherSaleLineItem) {
      setVoucherSoldLineAmount(argRequest, argResponse, (IVoucherSaleLineItem) argLine, argValue);
    }
    else {
      argLine.setFaceValueAmount(argValue);
    }
  }

  protected void setVoucherRedeemedLineAmount(ITenderAuthRequest argRequest, IAuthResponse argResponse,
      IVoucherRedeemedLineItem argLine, BigDecimal argValue) {

    if (argLine instanceof IVoucherDiscountLineItem) {
      setVoucherDiscountLineAmount(argRequest, argResponse, (IVoucherDiscountLineItem) argLine, argValue);
    }
    else if (argLine instanceof IVoucherTenderLineItem) {
      setTenderLineAmount(argRequest, argResponse, (ITenderLineItem) argLine, argValue);
    }
    else {
      argLine.setAmount(argValue);
    }
  }

  protected void setVoucherSoldLineAmount(ITenderAuthRequest argRequest, IAuthResponse argResponse,
      IVoucherSaleLineItem argLine, BigDecimal argValue) {

    // get the new pricemodifier object
    final IRetailPriceModifier modifier;
    try {
      modifier = DataFactory.createObject(IRetailPriceModifier.class);
    }
    catch (Exception ex) {
      throw new ReflectionException(ex);
    }
    modifier.setPriceChangeAmount(argValue);
    modifier.setRetailPriceModifierReasonCode(RetailPriceModifierReasonCode.PRICE_OVERRIDE.getName());
    modifier.setPriceChangeReasonCode("AUTHORIZED_AMOUNT");
    // add the price modifier to the list of modifiers
    argLine.addRetailPriceModifier(modifier);
  }

  protected boolean shouldMask(IAuthInfo argAuthInfo) {
    String type = argAuthInfo.getType();
    if (AuthInfoType.CID_ENTRY.equals(type)) {
      return true;
    }
    return false;
  }

  protected boolean shouldMask(String argField) {
    if (AuthInfoFieldKey.CID.equals(argField)) {
      return true;
    }
    if (AuthInfoFieldKey.ACCT_NUMBER.equals(argField)) {
      return true;
    }
    return false;
  }

  protected boolean supportsSuspending() {
    return false;
  }

  protected String toLogString(IAuthResponse argResponse) {
    StringBuilder sb = new StringBuilder();
    sb.append(argResponse.getClass().getName());
    appendNonNull(sb, "success", argResponse.isSuccess());
    appendNonNull(sb, "authCode", argResponse.getAuthorizationCode());
    appendNonNull(sb, "responseCode", argResponse.getResponseCode());
    appendNonNull(sb, "amount", argResponse.getAmount());
    appendNonNull(sb, "balance", argResponse.getBalance());
    appendNonNull(sb, "bankRefNbr", argResponse.getBankReferenceNumber());
    appendNonNull(sb, "msg", argResponse.getMessage());
    appendAvailableActions(argResponse, sb);
    appendRequiredInfo(sb, argResponse);
    appendNonNull(sb, "name", argResponse.getName());
    appendNonNull(sb, "type", argResponse.getType());
    return sb.toString();
  }

  protected void updateLineAmount(ITenderAuthRequest argRequest, IAuthResponse argResponse,
      IAuthorizableLineItem argLineItem) {

    if (partialApprovals()) {
      BigDecimal requestedAmount = argRequest.getAmount();
      if(requestedAmount!=null)
      {
        requestedAmount=requestedAmount.abs();
      }
      BigDecimal approvedAmount = argResponse.getAmount();
      if (NumberUtils.isGreaterThan(requestedAmount, approvedAmount)) {
        setLineAmount(argRequest, argResponse, argLineItem, approvedAmount);
      }
    }
    if (getTrackBalance()) {
      BigDecimal balance = argResponse.getBalance();
      if (balance != null) {
        if (argLineItem instanceof IVoucherLineItem) {
          ((IVoucherLineItem) argLineItem).setUnspentBalanceAmount(balance);
        }
        else {
          argLineItem.setStringProperty(getReportBalancePropertyName(), balance.toString());
        }
      }
    }
  }

  protected void updateLineForManual(IAuthorizableLineItem argLineItem, String argAuthorizationNumber) {
    argLineItem.setAdjudicationCode(getManualAuthSuccessAdjucationCode());
    argLineItem.setAuthorizationMethodCode(getAuthMethodCode());
    argLineItem.setAuthorizationCode(argAuthorizationNumber);
  }

  protected IAuthResponse updateLineItemForOnline(ITenderAuthRequest argRequest, IAuthResponse argResponse,
      IAuthorizableLineItem argLineItem) {

    if (argLineItem != null) {
      if (argResponse.isSuccess()) {
        argLineItem.setAuthorizationCode(argResponse.getAuthorizationCode());
        updateLineAmount(argRequest, argResponse, argLineItem);
      }
      argLineItem.setAdjudicationCode(argResponse.getResponseCode());
      copyBankReferenceNumber(argLineItem, argResponse);
      copyTokenInformation(argLineItem, argResponse);
    }
    if (argResponse.isSuccess()) {
      setApprovedMessage(argResponse);
    }
    else {
      List<String> ids = new ArrayList<String>();
      ids.addAll(Arrays.asList(argRequest.getMessageKeys(argResponse)));
      ids.add(getUnknownResponseMessageId());
      argResponse.setMessage(getMessage(argResponse, ids));
      getAuthFailedActions(argResponse);

      /* some action codes indicate the somewhere downstream is offline */
      final List<AuthFailedActionType> types = Arrays.asList(argResponse.getAvailableActions());
      if (types.contains(AuthFailedActionType.AUTO_NEXT_HOST)) {
        argRequest.addResponse(argResponse, /* notify= */false);
        argRequest.setRetryCount(argRequest.getRetryCount() + 1);
        return handleOnline(argRequest);
      }
      else if (types.contains(AuthFailedActionType.AUTO_REPROCESS_OFFLINE)) {
        // log failure and retry offline
        argRequest.addResponse(argResponse, /* notify= */false);
        return handleOffline(argRequest);
      }
      else if (types.contains(AuthFailedActionType.AUTO_REPOLL)) {
        if ((argRequest instanceof ISuspendableAuthRequest) && supportsSuspending()) {
          // start timer for auto repoll
          argRequest.addResponse(argResponse, /* notify= */false);
          return handlePending((ISuspendableAuthRequest) argRequest);
        }
        else {
          final String reason;
          if (supportsSuspending()) {
            // just the request class
            reason =
                ObjectUtils.getClassNameFromObject(argRequest) + " does not implement "
                    + ISuspendableAuthRequest.class.getName();
          }
          else if (argRequest instanceof ISuspendableAuthRequest) {
            // just the processor
            reason = getClass().getName() + " does not support suspending.";
          }
          else {
            // the request class and the processor
            reason =
                ObjectUtils.getClassNameFromObject(argRequest) + " does not implement "
                    + ISuspendableAuthRequest.class.getName() + " and " + getClass().getName()
                    + " does not support suspending.";
          }
          logger_.warn(AuthFailedActionType.AUTO_REPOLL + " cannot be used because " + reason);
        }
      }
    }
    return argResponse;
  }

  protected IAuthResponse updateLineItemForOnlineVoid(ITenderAuthRequest argRequest,
      IAuthResponse argResponse, IAuthorizableLineItem argLineItem) {

    if (argLineItem != null) {
      if (argResponse.isSuccess()) {
        argLineItem.setAuthorizationCode(argResponse.getAuthorizationCode());
        argLineItem.setAdjudicationCode(argResponse.getResponseCode());
        argLineItem.setBankReferenceNumber(argResponse.getBankReferenceNumber());
      }
      copyTokenInformation(argLineItem, argResponse);
    }

    if (!argResponse.isSuccess()) {
      List<String> ids = new ArrayList<String>();
      ids.addAll(Arrays.asList(argRequest.getMessageKeys(argResponse)));
      ids.add(getUnknownResponseMessageId());
      argResponse.setMessage(getMessage(argResponse, ids));
      getAuthFailedActions(argResponse);
      /* some action codes indicate the somewhere downstream is offline */
      if (Arrays.asList(argResponse.getAvailableActions()).contains(
          AuthFailedActionType.AUTO_REPROCESS_OFFLINE)) {
        // log failure and retry offline
        argRequest.addResponse(argResponse, /* notify= */false);
        return handleOffline(argRequest);
      }
    }
    return argResponse;
  }

  /**
   * Validate a manual authorization code that was entered.
   * 
   * @param argRequest the request being manually authorized
   * @param argValue the value entered (could be String, Integer, or other depending on the format on the
   * input field)
   * @return <code>true</code> if the manual authorization number is valid, otherwise <code>false</code>
   */
  protected boolean validAuthNumber(ITenderAuthRequest argRequest, Object argValue) {
    if ((argValue == null) && (manualAuthMinimumLength_ > 0)) {
      logger_.debug("manual auth number is null");
      return false;
    }
    String s = argValue.toString().trim();
    if (blockedAuthNumbers_.contains(s)) {
      if (logger_.isDebugEnabled()) {
        logger_.debug("manual auth number '" + s + "' is blocked");
      }
      return false;
    }
    if (s.length() < manualAuthMinimumLength_) {
      if (logger_.isDebugEnabled()) {
        logger_.debug("manual auth number '" + s + "' is shorter than " + manualAuthMinimumLength_
            + " characters");
      }
      return false;
    }
    if (s.length() > manualAuthMaximumLength_) {
      if (logger_.isDebugEnabled()) {
        logger_.debug("manual auth number '" + s + "' is longer than " + manualAuthMaximumLength_
            + " characters");
      }
      return false;
    }
    if (null != manualAuthRegEx_) {
      Matcher matcher = manualAuthRegEx_.matcher(s);
      if (!matcher.find()) {
        return false;
      }
    }
    return true;
  }

  private void addRequestToAttributeModel(IAuthRequest argRequest) {
    IAttributeModel attributeModel = StationModelMgr.getInstance().getStationModel();
    if (attributeModel != null) {
      Set<IAuthRequest> requests = getAuthRequests(attributeModel);
      if (requests == null) {
        requests = new HashSet<IAuthRequest>();
      }
      requests.add(argRequest);
      attributeModel.setAttribute(TenderAuthAttributeKey.AUTH_REQUESTS, requests);
    }
  }

  private void appendAvailableActions(IAuthResponse argResponse, StringBuilder sb) {
    AuthFailedActionType[] acts = argResponse.getAvailableActions();
    if ((acts != null) && (acts.length != 0)) {
      sb.append(";actions=[");
      for (AuthFailedActionType act : acts) {
        sb.append(act).append(",");
      }
      sb.setLength(sb.length() - 1);
      sb.append("]");
    }
  }

  private void clearTrack1(IAuthorizableLineItem argLine) {
    if (getTrack1(argLine) != null) {
      setTrack1(argLine, clearTrack1With_);
    }
  }

  private void clearTrack2(IAuthorizableLineItem argLine) {
    if (getTrack2(argLine) != null) {
      setTrack2(argLine, clearTrack2With_);
    }
  }

  private Set<IAuthRequest> getAuthRequests(IAttributeModel argAttributeModel) {
    return argAttributeModel.getAttribute(TenderAuthAttributeKey.AUTH_REQUESTS);
  }

  private void persistAuthorizableLineItem(IAuthorizableLineItem argLineItem) {
    if (argLineItem == null) {
      return;
    }

    IPersistablesBag bag = new PersistablesBag();
    bag.addObject(argLineItem);

    try {
      bag.persist();
    }
    catch (PersistenceException ex) {
      logger_.error("Exception caught trying to persist authorizable lineitem", ex);
      throw new OpExecutionException(ex);
    }
  }
}
