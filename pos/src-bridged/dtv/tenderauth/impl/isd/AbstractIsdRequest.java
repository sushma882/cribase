// $Id: AbstractIsdRequest.java 429 2012-09-26 20:45:59Z dtvdomain\aabdul $
package dtv.tenderauth.impl.isd;

import static dtv.util.StringUtils.nonNull;
import java.math.BigInteger;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import org.apache.log4j.Logger;
import imsformat.credit.CCRequest;
import dtv.hardware.types.HardwareFamilyType;
import dtv.pos.iframework.type.TenderUsageCodeType;
import dtv.tenderauth.*;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.AbstractAuthRequest;
import dtv.util.DateUtils;
import dtv.util.StringUtils;
import dtv.util.sequence.SequenceFactory;
import dtv.xst.dao.tnd.ITender;
import dtv.xst.dao.trl.IAuthorizableLineItem;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * Abstract response from ISD.<br>
 * Copyright (c) 2007 Datavantage Corporation
 * 
 * @author dberkland
 * @version $Revision: 429 $
 * @created Feb 6, 2007
 */
public abstract class AbstractIsdRequest
    extends AbstractAuthRequest
    implements IIsdRequest, ITenderAuthRequest {

  private static final Logger logger_ = Logger.getLogger(AbstractIsdRequest.class);
  private static final DateFormat dateFormat_ = new java.text.SimpleDateFormat("yyMMdd");
  private static final DateFormat timeFormat_ = new java.text.SimpleDateFormat("HHmmss");
  private final HashMap<String, Object> map_ = new HashMap<String, Object>();

  private ITender tender_;
  private final TenderUsageCodeType tenderUsageCode_;
  private final String terminalId_;
  private final String storeNumber_;

  private int retryIndicator_ = 1;

  /**
   * Constructor
   * 
   * @param argType the type of authorization request
   * @param argLine the line item this request is related to, or <code>null</code> is there is no
   * related transaction line item
   * @param argTenderUsageCode the tender usage
   */
  protected AbstractIsdRequest(AuthRequestType argType, IAuthorizableLineItem argLine,
      TenderUsageCodeType argTenderUsageCode) {

    super(argType, argLine);
    tender_ = getTender(argLine);
    tenderUsageCode_ = argTenderUsageCode;

    terminalId_ = getTerminalIdString(argLine);
    storeNumber_ = getStoreNumberString(argLine);

    setParameter(CCRequest.TERMINAL_NUMBER, terminalId_);
    setParameter(CCRequest.LOCATION, storeNumber_);
    setParameter(CCRequest.SYSTEM_TRACE_AUDIT_NUMBER, SequenceFactory.getNextStringValue("STAN6"));
  }

  /** {@inheritDoc} */
  @Override
  public HashMap<String, Object> getRequestMap() {
    return map_;
  }

  /** {@inheritDoc} */
  @Override
  public final ITender getTender() {
    return tender_;
  }

  /** {@inheritDoc} */
  /**
   * Gets the description of the associated tender.
   * 
   * @return the description of the associated tender.
   */
  @Override
  public String getTenderDescription() {
    return getTender().getDescription();
  }

  /** {@inheritDoc} */
  @Override
  public TenderUsageCodeType getTenderUsageCode() {
    return tenderUsageCode_;
  }

  /** {@inheritDoc} */
  @Override
  public void prepRequest() {
    Date now = DateUtils.getNewDate();
    String seq = String.valueOf(getNextSequence());
    // if length > 4, ISD will truncate all but the four LEFTMOST digits -- bad!
    // just send the four RIGHTMOST digits to prevent problems
    if (seq.length() > 4) {
      seq = seq.substring(seq.length() - 4, seq.length());
    }

    if (isVoidRequest(getRequestType()) && getLineItem() != null) {
      setParameter(CCRequest.MESSAGE_SEQUENCE,
          nonNull(getLineItem().getStringProperty(CCRequest.MESSAGE_SEQUENCE)));
      setParameter(CCRequest.JOURNAL_KEY, nonNull(getLineItem().getStringProperty(CCRequest.JOURNAL_KEY)));
      setParameter(CCRequest.SYSTEM_TRACE_AUDIT_NUMBER,
          nonNull(getLineItem().getStringProperty(CCRequest.SYSTEM_TRACE_AUDIT_NUMBER)));
      setParameter(CCRequest.LOCAL_DATE, nonNull(getLineItem().getStringProperty(CCRequest.LOCAL_DATE)));
      setParameter(CCRequest.LOCAL_TIME, nonNull(getLineItem().getStringProperty(CCRequest.LOCAL_TIME)));
    }
    else {
      setParameter(CCRequest.MESSAGE_SEQUENCE, seq);
      setParameter(CCRequest.RETRY_INDICATOR, String.valueOf(retryIndicator_++ ));
      setParameter(CCRequest.LOCAL_DATE, dateFormat_.format(now));
      setParameter(CCRequest.LOCAL_TIME, timeFormat_.format(now));
      setParameter(CCRequest.JOURNAL_KEY, generateJournalKey(storeNumber_, terminalId_));

      //Saving the properties to line item to be used during void/reversal
      if (getLineItem() != null) {
        getLineItem().setTraceNumber(nonNull(getRequestMap().get(CCRequest.SYSTEM_TRACE_AUDIT_NUMBER)));
        getLineItem().setStringProperty(CCRequest.JOURNAL_KEY,
            nonNull(getRequestMap().get(CCRequest.JOURNAL_KEY)));
        getLineItem().setStringProperty(CCRequest.SYSTEM_TRACE_AUDIT_NUMBER,
            nonNull(getRequestMap().get(CCRequest.SYSTEM_TRACE_AUDIT_NUMBER)));
        getLineItem().setStringProperty(CCRequest.MESSAGE_SEQUENCE,
            nonNull(getRequestMap().get(CCRequest.MESSAGE_SEQUENCE)));
        getLineItem().setStringProperty(CCRequest.LOCAL_DATE,
            nonNull(getRequestMap().get(CCRequest.LOCAL_DATE)));
        getLineItem().setStringProperty(CCRequest.LOCAL_TIME,
            nonNull(getRequestMap().get(CCRequest.LOCAL_TIME)));
      }
    }

    super.prepRequest();
  }

  /** {@inheritDoc} */
  @Override
  public void setParameter(String argName, Object argValue) {
    if ("maximumCIDRetries".equalsIgnoreCase(argName)) {
      super.setParameter(argName, argValue);
    }
    if (argValue == null) {
      map_.remove(argName);
    }
    else {
      map_.put(argName, argValue);
    }
  }

  @Override
  protected void addingResponse(IAuthResponse argResponse) {
    createNPersistLogEntry(argResponse);
  }

  protected ITender getTender(IAuthorizableLineItem argLine) {
    if (argLine instanceof ITenderLineItem) {
      return ((ITenderLineItem) argLine).getTender();
    }
    return null;
  }

  /**
   * Determines if the account data was keyed in manually.
   * @param argEntryMethod the line item's entry method
   * @return <code>true</code> if the data was keyed in manually
   */
  protected boolean isKeyed(String argEntryMethod) {
    if (StringUtils.isEmpty(argEntryMethod)) {
      logger_.warn("empty entry method!?!?!", new Throwable("STACK TRACE"));
      return false;
    }
    if (argEntryMethod.startsWith(HardwareFamilyType.KEYBOARD.getName())) {
      return true;
    }
    else {
      return false;
    }
  }

  protected void setTender(ITender argTender) {
    tender_ = argTender;
  }

  private long getNextSequence() {
    long result = SequenceFactory.getNextLongValue(IAuthFactory.SEQ_TYPE_AUTH);
    // if divisible by 10000, bump the sequence number. ISD cannot accept "0000"
    if (result % 10000 == 0) {
      result = SequenceFactory.getNextLongValue(IAuthFactory.SEQ_TYPE_AUTH);
    }
    return result;
  }

  public static String generateJournalKey(String argStoreNumber, String argTerminal) {
    Date now = DateUtils.getNewDate();
    String tmpKey = argStoreNumber + argTerminal + Long.toString(now.getTime());
    String journalKey = new BigInteger(tmpKey).toString(16);
    journalKey = journalKey.length() > 16 ? journalKey.substring(0, 16) : journalKey;
    return journalKey.toUpperCase();
  }

  /**
   * Updates the authorization request to be a Time out reversal request
   */
  public void prepReversalRequest() {

    setParameter(CCRequest.MESSAGE_TYPE, "4");
    setParameter(CCRequest.STAND_IN, "T");
  }

  /**
   * Clears the Time out reversal request fields for further retries of Debit purchase request
   */
  public void clearReversalRequest() {
    setParameter(CCRequest.MESSAGE_TYPE, "0");
    setParameter(CCRequest.STAND_IN, null);
    setParameter(CCRequest.RETRY_INDICATOR, "1");
    setParameter(CCRequest.SYSTEM_TRACE_AUDIT_NUMBER, SequenceFactory.getNextStringValue("STAN6"));
  }

  public boolean isVoidRequest(AuthRequestType reqType) {

    if (reqType.equals(AuthRequestType.VOID_ACTIVATE)) {
      return true;
    }
    if (reqType.equals(AuthRequestType.VOID_RELOAD)) {
      return true;
    }
    if (reqType.equals(AuthRequestType.VOID_TENDER)) {
      return true;
    }
    if (reqType.equals(AuthRequestType.VOID_CASH_OUT)) {
      return true;
    }

    return false;
  }
}
