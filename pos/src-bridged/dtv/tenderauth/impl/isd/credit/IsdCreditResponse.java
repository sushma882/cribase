//$Id: IsdCreditResponse.java 482 2012-10-15 15:24:03Z dtvdomain\aabdul $
package dtv.tenderauth.impl.isd.credit;

import static dtv.util.StringUtils.isEmpty;

import java.math.BigDecimal;
import java.util.Map;
import imsformat.credit.CCResponse;
import dtv.tenderauth.IAuthRequest;
import dtv.tenderauth.event.ICreditAuthResponse;
import dtv.tenderauth.impl.isd.IsdResponse;

/**
 * Credit auth response from ISD.<br>
 * Copyright (c) 2007 Datavantage Corporation
 * 
 * @author dberkland
 * @version $Revision: 482 $
 * @created Apr 2, 2007
 */
public class IsdCreditResponse
    extends IsdResponse
    implements ICreditAuthResponse {

  private static final long serialVersionUID = 1L;

  /**
   * Constructor
   * 
   * @param argRequest
   * @param argMap
   */
  public IsdCreditResponse(IAuthRequest argRequest, Map<String, Object> argMap) {
    super(argRequest, argMap);
    setAmount(getAuthorizationAmt());
  }

  /**
   * Gets the authorization amount.
   * @return the authorization amount
   */
  public BigDecimal getAuthorizationAmt() {
    String amt = get(CCResponse.TENDER_AMOUNT);
    return toBigDecimal(amt);
  }

  /** {@inheritDoc} */
  @Override
  public String getBankReferenceNumber() {
    return get(CCResponse.RETRIEVAL_REFERENCE_NUMBER);
  }

  /** {@inheritDoc} */
  @Override
  public String getPosDataCode() {
    return get(CCResponse.POS_DATA_CODE);
  }

  /** {@inheritDoc} */
  @Override
  public String getPs2000() {
    return null;
    //I'm not sure what field we would find PS2000 in, if any
    //    return get(CCResponse.THREEDSC_RESP);
  }

  /** {@inheritDoc} */
  @Override
  public boolean isReenteringCID() {
    String cidResultCode = get(CCResponse.CVV2_RESPONSE_CODE);
    if ((!isSuccess()) //
        && ("N".equalsIgnoreCase(cidResultCode)) //
        && (getRequest().isCidRetryAllowed())) {
      return true;
    }
    return false;
  }

  /**
   * Gets the remaining balance of the stored value card.
   * @return the remaining balance of the stored value card.
   */
  @Override
  public BigDecimal getBalance() {
    return getCardBalance();
  }

  /**
   * Gets the card balance; if &lt; 0, this indicates card balance is now zero, and the negative
   * amount is added to the requested authorization amount to obtain the amount actually authorized.
   * 
   * @return the card balance
   */
  public BigDecimal getCardBalance() {
    String amt = checkForNegativeAmount(getCardBalanceString());
    return toBigDecimal(amt);
  }

  /**
   * Gets the card balance as a string.
   * @return the card balance as a string.
   */
  public String getCardBalanceString() {
    return get(CCResponse.NEW_BALANCE);
  }

  private String checkForNegativeAmount(String argAmt) {
    String amt = argAmt;
    String amtSign = get(CCResponse.BALANCE_SIGN1);
    if (isEmpty(amt) || isEmpty(amtSign)) {
      return null;
    }

    if ("-".equals(amtSign.trim())) {
      amt = '-' + argAmt;
    }

    return amt;
  }

}
