//$Id: IsdCreditRequest.java 337 2012-09-04 17:00:32Z dtvdomain\aabdul $
package dtv.tenderauth.impl.isd.credit;

import static dtv.util.StringUtils.nonNull;
import static dtv.util.StringUtils.isEmpty;

import imsformat.credit.CCRequest;
import dtv.pos.iframework.type.TenderUsageCodeType;
import dtv.tenderauth.AuthRequestType;
import dtv.tenderauth.ICreditAuthRequest;
import dtv.tenderauth.impl.AbstractAuthRequest;
import dtv.tenderauth.impl.isd.AbstractIsdRequest;
import dtv.xst.dao.trl.IAuthorizableLineItem;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;

/**
 * Credit card request for ISD.<br>
 * Copyright (c) 2007 Datavantage Corporation<br>
 * @author dberkland
 * @version $Revision: 337 $
 * @created Feb 6, 2007
 */
public class IsdCreditRequest
    extends AbstractIsdRequest
    implements ICreditAuthRequest {

  private static final String AMEX_ID = "AMERICAN_EXPRESS"; // CUSTOMIZATION

  /*
   * The expiration date is stored as MMyy on the credit/debit line item.
   * Fifth Third, the bank used by TSC requires the expiration date in yyMM format.
   */
  private static String reformatExpirationDate(String argDateString) {
    if (argDateString == null) {
      return "9912";
    }
    String result = argDateString.substring(2, 4) + argDateString.substring(0, 2);

    return result;
  }

  /**
   * Constructor
   * 
   * @param argType the type of authorization request
   * @param argLine the line item this request is related to, or <code>null</code> is there is no
   * related transaction line item
   */
  public IsdCreditRequest(AuthRequestType argType, IAuthorizableLineItem argLine) {
    this(argType, argLine, null);
  }

  /**
   * Constructor
   * 
   * @param argType the type of authorization request
   * @param argLine the line item this request is related to, or <code>null</code> is there is no
   * related transaction line item
   * @param argTenderUsageCode tender usage
   */
  public IsdCreditRequest(AuthRequestType argType, IAuthorizableLineItem argLine,
      TenderUsageCodeType argTenderUsageCode) {
    super(argType, argLine, argTenderUsageCode);
    ICreditDebitTenderLineItem ccLine = (ICreditDebitTenderLineItem) argLine;

    setParameter(CCRequest.EXPIRATION_DATE, reformatExpirationDate(ccLine.getExpirationDateString()));
    setVerificationField(ccLine);
    setParameter(CCRequest.ADDITIONAL_AMOUNTS,
        AbstractAuthRequest.getScaledAmountString(ccLine.getCashbackAmount()));
    setParameter(CCRequest.PIN_DATA, ccLine.getEncryptedPin());

    //need to send the KSN in different field. Updated based on the customizations from previous customer
    //setParameter(CCRequest.KSN_20, ccLine.getAdditionalPinSecurityInfo());
    if (!isEmpty(ccLine.getAdditionalPinSecurityInfo())
        && ccLine.getAdditionalPinSecurityInfo().startsWith("FFFF")) {
      setParameter(CCRequest.KSN_FORMAT, ccLine.getAdditionalPinSecurityInfo().substring(4));
    }
    else {
      setParameter(CCRequest.KSN_FORMAT, ccLine.getAdditionalPinSecurityInfo());
    }

    setParameter(CCRequest.TENDER_AMOUNT, AbstractAuthRequest.getAmountString(argLine));
    setParameter(CCRequest.CREDIT_PLAN, ccLine.getStringProperty("PLAN_NUMBER"));
  }

  /** {@inheritDoc} */
  @Override
  public void loadSensitiveData() {
    if (getLineItem() instanceof ICreditDebitTenderLineItem) {
      ICreditDebitTenderLineItem creditLineItem = (ICreditDebitTenderLineItem) getLineItem();

      StringBuilder track1 = new StringBuilder(nonNull(creditLineItem.getTrack1()));
      StringBuilder track2 = new StringBuilder(nonNull(creditLineItem.getTrack2()));

      setParameter(CCRequest.ACCOUNT_NUMBER, creditLineItem.getAccountNumber());

      // -------------------------------------
      // : CUSTOMIZATIONS
      //--------------------------------------
      if (track2.length() > 0) {
        setParameter(CCRequest.TRACK_DATA, track2.toString());
        setParameter(CCRequest.TRACK_NUMBER, "2");
      }
      else if (track1.length() > 0) {
        setParameter(CCRequest.TRACK_DATA, track1.toString());
        setParameter(CCRequest.TRACK_NUMBER, "1");
      }
      else {
        // 243490 -- POS_CONDITION_CODE will be set in AuthConfig to "00"
        //setParameter(CCRequest.POS_CONDITION_CODE, "05");
        setParameter(CCRequest.TRACK_NUMBER, "0");
      }
      // -------------------------------------
      // : END CUSTOMIZATIONS
      //--------------------------------------
      track1.delete(0, track1.length());
      track2.delete(0, track2.length());
    }

  }

  /** {@inheritDoc} */
  @Override
  public void processReenteredCid(String argCid) {
    ICreditDebitTenderLineItem creditLine = (ICreditDebitTenderLineItem) getLineItem();
    creditLine.setCid(argCid);
    incrementCidEntryCount();
    setVerificationField(creditLine);
    getHostList().reset();
  }

  /** {@inheritDoc} */
  @Override
  public void unloadSensitiveData() {

  }

  /** {@inheritDoc} */
  @Override
  protected void setVerificationField(IAuthorizableLineItem argLineItem) {
    ICreditDebitTenderLineItem ccLine = (ICreditDebitTenderLineItem) argLineItem;

    if (!AMEX_ID.equalsIgnoreCase(ccLine.getTenderId())) {
      if (ccLine.getCid() != null) {
        setParameter(CCRequest.CVV2_PRESENCE_INDICATOR, "1");
        setParameter(CCRequest.CVV2_VALUE, ccLine.getCid());
      }
      else {
        setParameter(CCRequest.CVV2_PRESENCE_INDICATOR, "0");
      }
    }
    else {
      setParameter(CCRequest.CVV2_PRESENCE_INDICATOR, " ");
      if (ccLine.getCid() != null) {
        setParameter(CCRequest.SECURITY_CONTROL_INFO, ccLine.getCid());
      }
    }
  }
}
