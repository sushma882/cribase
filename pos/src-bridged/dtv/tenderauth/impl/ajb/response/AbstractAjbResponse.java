//$Id: AbstractAjbResponse.java 960 2015-09-29 16:54:03Z suz.sxie $
package dtv.tenderauth.impl.ajb.response;

import java.math.BigDecimal;
import java.text.*;
import java.util.Date;

import org.apache.log4j.Logger;

import dtv.tenderauth.IAuthRequest;
import dtv.tenderauth.impl.ajb.IAjbResponse;
import dtv.tenderauth.impl.event.AbstractAuthResponse;
import dtv.util.NumberUtils;
import dtv.util.StringUtils;

/**
 * Abstract response object from AJB.<br>
 * Copyright (c) 2005 Datavantage Corporation
 * 
 * @author dberkland
 * @version $Revision: 960 $
 * @created Jun 14, 2005
 */
public abstract class AbstractAjbResponse
    extends AbstractAuthResponse
    implements IAjbResponse {

  private static final long serialVersionUID = 1L;
  private static final Logger logger_ = Logger.getLogger(AbstractAjbResponse.class);

  protected static final DecimalFormat moneyFormatNoCents_ = new DecimalFormat("0");

  protected static final int MESSAGE_ID = 1;

  /** the field number for the action code */
  public static final int ACTION_CODE = 4;

  private static final int REQ_TRAN_TYPE = 6;

  private static final int STORE_NBR = 8;

  private static final int TERMINAL_NBR = 9;

  private static final int OPTIONS = 21;

  private static final int RECEIPT_TEXT = 38;

  private static final int SYSTEM_TRACE_NUMBER = 46;

  private static final int ISO_RESPONSE_CODE = 53;

  protected static final BigDecimal toBigDecimal(String argAmount) {
    return toBigDecimal(argAmount, true);
  }

  protected static final BigDecimal toBigDecimalUnscaled(String argAmount) {
    return toBigDecimal(argAmount, false);
  }

  private static final BigDecimal toBigDecimal(String argAmount, boolean argScaled) {
    if (StringUtils.isEmpty(argAmount)) {
      return null;
    }
    try {
      BigDecimal results = NumberUtils.toBigDecimal(argAmount, moneyFormatNoCents_);
      if (argScaled) {
        results = results.setScale(2).divide(NumberUtils.HUNDRED, BigDecimal.ROUND_HALF_UP);
      }
      return results;
    }
    catch (Exception ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
      return null;
    }
  }

  private final String[] fields_;
  private final DateFormat dateFormat_ = new SimpleDateFormat("yyyyMMdd");

  /**
   * Constructor
   * 
   * @param argRequest the request that triggered this response.
   * @param argFields the raw response fields
   * @param argAuthorizationCode authorization code
   */
  protected AbstractAjbResponse(IAuthRequest argRequest, String[] argFields, String argAuthorizationCode) {
    super(null, argRequest, argAuthorizationCode);
    fields_ = argFields;
    if (getActionCode() == 0) {
      setSuccess(true);
    }
  }

  /** {@inheritDoc} */
  @Override
  public int getActionCode() {
    String s = getActionCodeString();
    try {
      return Integer.parseInt(s);
    }
    catch (Exception ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
      return -1;
    }
  }

  /**
   * Gets the action code as a string.
   * 
   * @return the action code as a string.
   */
  public String getActionCodeString() {
    return getField(ACTION_CODE);
  }

  /**
   * Gets the bank reference number
   * 
   * @return the bank reference number
   */
  @Override
  public String getBankReferenceNumber() {
    return getField(SYSTEM_TRACE_NUMBER);
  }

  /**
   * Gets the ISO response code as a String.
   * @return the ISO response code as a String.
   */
  @Override
  public String getIsoResponse() {
    return getField(ISO_RESPONSE_CODE);
  }

  /**
   * Gets the ISO response code as a number.
   * @return the ISO response code, or -1 if no numeric code was returned.
   */
  @Override
  public int getIsoResponseNumber() {
    String s = getIsoResponse();
    try {
      return Integer.parseInt(s);
    }
    catch (Exception ex) {
      logger_.debug("CAUGHT EXCEPTION", ex);
      return -1;
    }
  }

  /**
   * Gets the message ID.
   * @return the message ID.
   */
  public String getMessageId() {
    return getField(MESSAGE_ID);
  }

  /**
   * Gets the request transaction type. (e.g. "Credit", "Debit", "GiftCard", etc.)
   * 
   * @return the request transaction type.
   */
  public String getRequestTransactionType() {
    return getField(REQ_TRAN_TYPE);
  }

  /**
   * Gets the provider specific response code. This implementation returns the action code.
   * 
   * @return the response code
   */
  @Override
  public String getResponseCode() {
    return getActionCodeString();
  }

  /**
   * Gets the provider specific response code. This implementation returns the action code.
   * 
   * @return the response code
   */
  @Override
  public int getResponseCodeNumber() {
    return getActionCode();
  }

  /**
   * Gets the receipt display text
   * 
   * @return the receipt display text
   */
  public String getResponseText() {
    return getField(RECEIPT_TEXT);
  }

  /**
   * Gets the store number echoed back
   * @return the store number echoed back
   */
  public String getStoreNumber() {
    return getField(STORE_NBR);
  }

  /**
   * Gets the terminal number echoed back
   * @return the terminal number echoed back
   */
  public String getTerminalNumber() {
    return getField(TERMINAL_NBR);
  }

  /**
   * Determines if the response requires an ACK to be sent.
   * 
   * @return <code>true</code> if an ACK should be sent, otherwise <code>false</code>
   */
  @Override
  public boolean isAckReqested() {
    String options = getField(OPTIONS);
    if (options.toUpperCase().indexOf("DEBITACKREQ") > -1) {
      return true;
    }
    else {
      return false;
    }
  }

  protected final String getField(int argFieldNumber) {
    int fieldIndex = argFieldNumber - 1;
    if (fieldIndex >= fields_.length) {
      return "";
    }
    else {
      return fields_[fieldIndex];
    }
  }

  protected void setActionCode(int argValue) {
    setActionCode(String.valueOf(argValue));
    if (argValue == 0) {
      setSuccess(true);
    }
    else {
      setSuccess(false);
    }
  }

  protected void setActionCode(String argValue) {
    setField(ACTION_CODE, argValue);
  }

  protected final void setField(int argFieldNumber, String argValue) {
    int fieldIndex = argFieldNumber - 1;
    fields_[fieldIndex] = argValue;
  }

  protected final Date toDate(String argDate) {
    if (StringUtils.isEmpty(argDate)) {
      return null;
    }
    try {
      return dateFormat_.parse(argDate);
    }
    catch (Exception ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
      return null;
    }
  }

  public String[] getFields() {
    return fields_;
  }

}
