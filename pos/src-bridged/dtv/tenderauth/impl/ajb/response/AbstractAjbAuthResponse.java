//$Id: AbstractAjbAuthResponse.java 875 2015-07-13 22:12:21Z suz.sxie $
package dtv.tenderauth.impl.ajb.response;

import java.math.BigDecimal;

import org.apache.log4j.Logger;

import dtv.tenderauth.IAccountInformation;
import dtv.tenderauth.IAuthRequest;
import dtv.tenderauth.impl.ajb.IAjbAuthResponse;

/**
 * Abstract response object for authorizations from AJB.<br>
 * Copyright (c) 2005 Datavantage Corporation
 *
 * @author dberkland
 * @version $Revision: 875 $
 * @created Jun 14, 2005
 */
public abstract class AbstractAjbAuthResponse
  extends AbstractAjbResponse
  implements IAjbAuthResponse {

  private static final long serialVersionUID = 1L;
  private static final Logger logger_ = Logger.getLogger(AbstractAjbAuthResponse.class);

  private static final int AUTH_REQUEST_TYPE = 10;
  protected static final int AMOUNT = 16;
  private static final int POS_TRAN_NBR = 17;
  private static final int RESPONSE_TEXT = 38;
  private static final int APPROVED_AMOUNT = 52;
  private static final int RESPONSE_CODE = 53;
  private static final int NODE_ID = 54;

  private IAccountInformation accountInformation_;

  /**
   * Constructor
   *
   * @param argRequest the request that triggered this response.
   * @param argFields the raw response fields
   * @param argAuthorizationCode authorization code
   */
  protected AbstractAjbAuthResponse(IAuthRequest argRequest, String[] argFields, String argAuthorizationCode) {
    super(argRequest, argFields, argAuthorizationCode);
  }

  /**
   * @return Returns the AccountInformation.
   */
  @Override
  public IAccountInformation getAccountInformation() {
    return accountInformation_;
  }

  /**
   * Gets the requested amount.
   * @return the requested amount.
   */
  @Override
  public BigDecimal getAmount() {
    String amt = getAmountString();
    return toBigDecimal(amt);
  }

  /**
   * Gets the requested amount as a string.
   * @return the requested amount as a string.
   */
  public String getAmountString() {
    return getField(AMOUNT);
  }

  /**
   * Gets the approved amount.
   * @return the approved amount.
   */
  @Override
  public BigDecimal getApprovedAmount() {
    String amt = getApprovedAmountString();
    return toBigDecimal(amt);
  }

  /**
   * Gets the approved amount as a string.
   * @return the approved amount as a string.
   */
  public final String getApprovedAmountString() {
    return getField(APPROVED_AMOUNT);
  }

  /**
   * Gets the authorization request type.
   * @return the authorization request type.
   */
  public final String getAuthRequestType() {
    return getField(AUTH_REQUEST_TYPE);
  }

  /**
   * Gets the Bank Node ID; the RTS node ID of the authorizer
   * @return the Bank Node ID; the RTS node ID of the authorizer
   */
  public String getNodeId() {
    return getField(NODE_ID);
  }

  /**
   * Gets the transaction sequence number of the POS transaction.
   * @return the transaction sequence number of the POS transaction.
   */
  public final String getPosTransactionNumber() {
    return getField(POS_TRAN_NBR);
  }

  /**
   * Gets the response code for the authorization.
   * @return the response code for the authorization.
   */
  @Override
  public String getResponseCode() {
    return getField(RESPONSE_CODE);
  }

  /**
   * Gets the response code for the authorization.
   * @return the response code for the authorization.
   */
  @Override
  public int getResponseCodeNumber() {
    String s = getResponseCode();
    try {
      return Integer.parseInt(s);
    }
    catch (Exception ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
      return -1;
    }
  }

  /**
   * Gets the response text from the authorizer to be displayed to the user.
   * @return the response text from the authorizer to be displayed to the user.
   */
  @Override
  public String getResponseText() {
    return getField(RESPONSE_TEXT);
  }

  /**
   * @param argAccountInformation The AccountInformation to set.
   */
  public void setAccountInformation(IAccountInformation argAccountInformation) {
    accountInformation_ = argAccountInformation;
  }

}
