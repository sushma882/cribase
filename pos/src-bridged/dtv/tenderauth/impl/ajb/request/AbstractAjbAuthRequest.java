// $Id: AbstractAjbAuthRequest.java 874 2015-07-13 19:24:07Z suz.sxie $
package dtv.tenderauth.impl.ajb.request;

import org.apache.log4j.Logger;

import dtv.hardware.types.HardwareFamilyType;
import dtv.pos.iframework.type.TenderUsageCodeType;
import dtv.tenderauth.AuthRequestType;
import dtv.tenderauth.ITenderAuthRequest;
import dtv.util.StringUtils;
import dtv.xst.dao.trl.IAuthorizableLineItem;

/**
 * Abstract request object for authorization through AJB.<br>
 * Copyright (c) 2005 Datavantage Corporation
 *
 * @author dberkland
 * @version $Revision: 874 $
 * @created Jun 14, 2005
 */
public abstract class AbstractAjbAuthRequest
  extends AbstractAjbRequest
  implements ITenderAuthRequest {

  protected static final int AMOUNT = 16;
  protected static final String AUTH_REQUEST_TYPE_SALE = "Sale";
  protected static final String AUTH_REQUEST_TYPE_REFUND = "Refund";

  private static final Logger logger_ = Logger.getLogger(AbstractAjbAuthRequest.class);
  protected static final int POS_TRAN_NBR = 17;

  private final TenderUsageCodeType tenderUsageCode_;

  protected AbstractAjbAuthRequest(AuthRequestType argType, IAuthorizableLineItem argLine,
                                   TenderUsageCodeType argTenderUsageCode, String argMessageId, String argReqTranType) {

    super(argType, argLine, argMessageId, argReqTranType, getStoreNumberString(argLine),
      getTerminalIdString(argLine));
    tenderUsageCode_ = argTenderUsageCode;
    if (argLine != null) {
      setAmount(argLine.getAmount());
    }
    setField(AMOUNT, getAmountString(argLine));
    setField(POS_TRAN_NBR, getTransactionNumberString(argLine));
    String type = getAuthRequestTypeString(argType, argTenderUsageCode);
    setField(AUTH_REQUEST_TYPE, type);
  }

  /** {@inheritDoc} */
  @Override
  public final TenderUsageCodeType getTenderUsageCode() {
    return tenderUsageCode_;
  }

  /**
   * Gets the transaction sequence portion of the transaction id.
   * @return long
   */
  public final long getTransactionSequence() {
    try {
      return Integer.parseInt(getField(POS_TRAN_NBR));

    }
    catch (Exception ex) {
      logger_.debug("CAUGHT EXCEPTION", ex);
      return -1;
    }
  }

  /**
   * Sets the transaction sequence portion of the transaction id.
   * @param argTransSeq long
   */
  public final void setTransactionSequence(long argTransSeq) {
    setField(POS_TRAN_NBR, String.valueOf(argTransSeq));
  }

  protected final int getAmountField() {
    return AMOUNT;
  }

  protected String getAuthRequestTypeString(AuthRequestType argType, TenderUsageCodeType argTenderUsageCode) {
    if (AuthRequestType.ISSUE == argType) {
      return "Issue";
    }
    else if (AuthRequestType.INQUIRE_BALANCE == argType) {
      return "BalanceInquiry";
    }
    else if ((AuthRequestType.TENDER == argType) || (AuthRequestType.CAPTURE_TENDER == argType)) {
      if (argTenderUsageCode == TenderUsageCodeType.SALE) {
        return AUTH_REQUEST_TYPE_SALE;
      }
      else {
        return AUTH_REQUEST_TYPE_REFUND;
      }
    }
    else if ((AuthRequestType.REFUND_TENDER == argType) || (AuthRequestType.CAPTURE_REFUND_TENDER == argType)) {
      return AUTH_REQUEST_TYPE_REFUND;
    }
    else if (AuthRequestType.CASH_OUT == argType) {
      return "Redeem";
    }
    else if (AuthRequestType.AUTH_STATUS == argType) {
      return "AuthInquiry";
    }
    else if ((AuthRequestType.REVERSE == argType) || (AuthRequestType.VOID_TENDER == argType)
      || (AuthRequestType.VOID_ACTIVATE == argType) || (AuthRequestType.VOID_RELOAD == argType)) {
      return "Cancel";
    }
    else {
      logger_.warn("unexpected request type: " + argType + "; using 'Cancel'");
      return "Cancel";
    }
  }

  protected final boolean isKeyed(String argEntryMethod) {
    if (StringUtils.isEmpty(argEntryMethod)) {
      logger_.warn("empty entry method!?!?!", new Throwable("STACK TRACE"));
      return false;
    }
    if (argEntryMethod.startsWith(HardwareFamilyType.KEYBOARD.getName())) {
      return true;
    }
    else {
      return false;
    }
  }

}
