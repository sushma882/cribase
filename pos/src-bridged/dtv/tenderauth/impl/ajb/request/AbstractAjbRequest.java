// $Id: AbstractAjbRequest.java 960 2015-09-29 16:54:03Z suz.sxie $
package dtv.tenderauth.impl.ajb.request;

import java.lang.reflect.Field;
import java.util.Arrays;

import org.apache.log4j.Logger;

import dtv.data2.access.DataFactory;
import dtv.pos.iframework.IPersistablesBag;
import dtv.pos.iframework.type.TenderUsageCodeType;
import dtv.tenderauth.AuthRequestType;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.AbstractAuthRequest;
import dtv.tenderauth.impl.ajb.IAjbRequest;
import dtv.tenderauth.impl.ajb.IAjbResponse;
import dtv.tenderauth.impl.ajb.status.PendedCreditAuthorizationStatus;
import dtv.tenderauth.impl.ajb.status.PendedCreditAuthorizationType;
import dtv.util.*;
import dtv.util.config.ConfigUtils;
import dtv.util.sequence.SequenceFactory;
import dtv.xst.dao.cat.CustomerAccountAuthorizationId;
import dtv.xst.dao.cat.ICustomerAccountAuthorization;
import dtv.xst.dao.tnd.ITender;
import dtv.xst.dao.trl.IAuthorizableLineItem;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * Abstract data object for a message sent to AJB.<br>
 * Copyright (c) 2005 Datavantage Corporation
 * 
 * @author dberkland
 * @version $Revision: 960 $
 * @created Jun 14, 2005
 */
public abstract class AbstractAjbRequest
    extends AbstractAuthRequest
    implements IAjbRequest {

  protected static final int AJB_OPTION_FIELD = 21; // Used to indicate reversal transaction.

  protected static final int AUTH_REQUEST_TYPE = 10;

  // private static final int RECEIPT_TEXT = 38;
  protected static final String REVERSAL_INDICATOR = "*RevStan";
  private static final Logger logger_ = Logger.getLogger(AbstractAjbRequest.class);
  protected static final int MESSAGE_ID = 1;
  private static final String PROP_CLIENT_ORDER_ID = "CLIENT_ORDER_ID";
  protected static final int REQ_TRAN_TYPE = 6;
  /** sequence type for private credit client order ID */
  private static final String SEQ_TYPE_PRIVATE_CREDIT_CLIENT_ORDER_ID = "PRIVATE_CREDIT_CLIENT_ORDER_ID";
  protected static final int STORE_NBR = 8;
  protected static final int TERMINAL_NBR = 9;

  protected static final int TIMEOUT = 5;

  protected static int getFieldNumberFromName(String argName) {
    if (!argName.startsWith("FIELD_")) {
      return Integer.MIN_VALUE;
    }

    try {
      return Integer.parseInt(argName.substring("FIELD_".length()));
    }
    catch (NumberFormatException ex) {
      logger_.info("CAUGHT EXCEPTION", ex);
      return Integer.MIN_VALUE;
    }
  }

  protected static String makeClientOrderId(IAuthorizableLineItem argLineItem) {
    String clientOrderId = argLineItem.getStringProperty(PROP_CLIENT_ORDER_ID);

    if (StringUtils.isEmpty(clientOrderId)) {
      clientOrderId = SequenceFactory.getNextStringValue(SEQ_TYPE_PRIVATE_CREDIT_CLIENT_ORDER_ID);
      argLineItem.setStringProperty(PROP_CLIENT_ORDER_ID, clientOrderId);
    }

    return clientOrderId;
  }

  private String[] fields_ = new String[120];
  private int maxField_ = 118;
  private String nodeId_;

  private boolean reversalRequired_ = false;

  private boolean sent_ = false;

  private ITender tender_;

  protected AbstractAjbRequest(AuthRequestType argType, IAuthorizableLineItem argLine, String argMessageId,
      String argReqTranType, String argStoreNumber, String argTerminalNumber) {
    super(argType, argLine);
    tender_ = getTender(argLine);
    setField(MESSAGE_ID, argMessageId);
    setField(REQ_TRAN_TYPE, argReqTranType);
    setField(STORE_NBR, argStoreNumber);
    setField(TERMINAL_NBR, argTerminalNumber);
    // setField(STORE_NBR, "999");
    // setField(TERMINAL_NBR, "1");
  }

  /** {@inheritDoc} */
  @Override
  public void clearReversal() {
    setField(AJB_OPTION_FIELD, "");
  }

  /** {@inheritDoc} */
  @Override
  public String[] getMessageKeys(IAuthResponse argResponse) {
    String prefix = getMessageIdPrefix(argResponse);
    int rtsResultCode = ((IAjbResponse) argResponse).getActionCode();
    String reqType = getField(AUTH_REQUEST_TYPE);
    int isoResultCode = ((IAjbResponse) argResponse).getIsoResponseNumber();
    return new String[] {prefix + rtsResultCode + "::" + reqType + "::" + isoResultCode, //
        prefix + rtsResultCode + "::" + isoResultCode,//
        prefix + rtsResultCode + "::" + reqType, //
        prefix + rtsResultCode};
  }

  /**
   * Gets the message string that will be sent to AJB
   * 
   * @return the message string that will be sent to AJB
   */
  @Override
  public String getMessageString() {
    StringBuffer buf = new StringBuffer(100);
    // buf.append("141,");
    for (int i = 0; i <= maxField_; i++ ) {
      if (fields_[i] != null) {
        buf.append(fields_[i]);
      }

      if (i < maxField_) {
        buf.append(",");
      }
    }

    return buf.toString();
  }

  /** {@inheritDoc} */
  @Override
  public String getNodeId() {
    return nodeId_;
  }

  /**
   * @return boolean reversal required
   */
  @Override
  public boolean getReversalRequired() {
    return reversalRequired_;
  }

  /**
   * @return the store number
   */
  public String getStoreNumber() {
    return getField(STORE_NBR);
  }

  /** {@inheritDoc} */
  @Override
  public final ITender getTender() {
    return tender_;
  }

  /**
   * Gets the description of the associated tender.
   * 
   * @return the description of the associated tender.
   */
  @Override
  public String getTenderDescription() {
    return getTender().getDescription();
  }

  /** {@inheritDoc} */
  @Override
  public TenderUsageCodeType getTenderUsageCode() {
    return null;
  }

  /**
   * @return the terminal number
   */
  public String getTerminalNumber() {
    return getField(TERMINAL_NBR);
  }

  /**
   * Gets the sent flag.
   * 
   * @return <code>true</code> if the request was sent.
   */
  @Override
  public boolean isSent() {
    return sent_;
  }

  /**
   * Convert this request object to a reversal request.
   * @param systemTraceNum - system trace number received in the resopnse if a response was
   * received.
   * @return boolean indicating whether the reversal was generated or not.
   */
  @Override
  public boolean makeReversal(String systemTraceNum) {
    // Override as necessary. For now, only debit cards need to override this msthod.
    return false;
  }

  /**
   * Sets the FEPNIS node ID for the request.
   * 
   * @param argNodeId the new node ID
   */
  public void setNodeId(String argNodeId) {
    if (argNodeId.length() > 15) {
      logger_.warn("node ID '" + argNodeId + "' is longer than 15 characters");
      argNodeId = argNodeId.substring(0, 15);
    }
    nodeId_ = argNodeId;
  }

  /** {@inheritDoc} */
  @Override
  public void setParameter(String argName, Object argValue) {
    if ("communicatorReceiveTimeoutMillis".equalsIgnoreCase(argName)) {
      int timeout = ConfigUtils.toInt(argValue);
      timeout = (timeout / 1000) - 5;
      setTimeoutSeconds(timeout);
    }
    else if ("nodeId".equalsIgnoreCase(argName)) {
      setNodeId(argValue.toString());
    }
    else if ("ReversalRequired".equalsIgnoreCase(argName)) {
      reversalRequired_ = ConfigUtils.toBoolean(argValue.toString());
    }
    else if ("CancelAllowed".equalsIgnoreCase(argName)) {
      setCancelAllowed(ConfigUtils.toBoolean(argValue));
    }
    else {
      int fieldNumber = getFieldNumberFromName(argName);

      if (fieldNumber > 0) {
        setField(fieldNumber, StringUtils.nonNull(argValue));
      }
      else {
        Field f = ObjectUtils.getDeclaredFieldIgnoreCase(this.getClass(), argName);

        if (f == null) {
          logger_.warn("unexpected parameter " + argName + "=" + argValue);
          return;
        }
        try {
          f.set(this, argValue);
        }
        catch (Exception ex) {
          logger_.error("CAUGHT EXCEPTION setting " + argName + "=" + argValue, ex);
        }
      }
    }
  }

  /**
   * Updates a pending application to a status of complete based on the suspended transaction
   * sequence number. The application is assigned a new transaction sequence number upon resuming
   * the transaction.
   * 
   * @param argLine the suspended line item id corresponding to the pending authorization
   * @param argBag used to save to the database
   */
  public void setPendedAuthorizationToComplete(ICreditDebitTenderLineItem argLine, IPersistablesBag argBag) {
    CustomerAccountAuthorizationId id = new CustomerAccountAuthorizationId();
    // set id values with lineItem values
    id.setBusinessDate(argLine.getBusinessDate());
    id.setOrganizationId(new Long(argLine.getOrganizationId()));
    id.setRetailLocationId(new Long(argLine.getRetailLocationId()));
    id.setTransactionSequence(new Long(argLine.getTransactionSequence()));
    id.setWorkstationId(new Long(argLine.getWorkstationId()));
    id.setRetailTransactionLineItemSequence(argLine.getRetailTransactionLineItemSequence());

    // Transfer the key fields to a full object then add it to the bag
    ICustomerAccountAuthorization pa = (ICustomerAccountAuthorization) DataFactory.getObjectById(id);
    pa.setStatusCode(PendedCreditAuthorizationStatus.COMPLETE.getName());
    pa.setAuthorizationType(getPendedCreditAuthorizationType().getName());
    pa.setStatusDatetime(DateUtils.getNewDate());
    argBag.addObject(pa);
  }

  /**
   * Sets the sent flag.
   * 
   * @param argSent new value for the sent flag.
   */
  @Override
  public void setSent(boolean argSent) {
    sent_ = argSent;
  }

  public void setTimeoutSeconds(int argSeconds) {
    setField(TIMEOUT, String.valueOf(argSeconds));
  }

  @Override
  protected void addingResponse(IAuthResponse argResponse) {
    createNPersistLogEntry(argResponse);
  }

  protected void addPendingAuthorization(IPersistablesBag argBag, IAuthorizableLineItem lineItem) {
    CustomerAccountAuthorizationId id = new CustomerAccountAuthorizationId();
    // set id values with lineItem values
    id.setBusinessDate(lineItem.getBusinessDate());
    id.setOrganizationId(new Long(lineItem.getOrganizationId()));
    id.setRetailLocationId(new Long(lineItem.getRetailLocationId()));
    id.setRetailTransactionLineItemSequence(new Integer(lineItem.getRetailTransactionLineItemSequence()));
    id.setTransactionSequence(new Long(lineItem.getTransactionSequence()));
    id.setWorkstationId(new Long(lineItem.getWorkstationId()));

    // Transfer the key fields to a full object then add it to the bag
    ICustomerAccountAuthorization pa = DataFactory.createObject(id, ICustomerAccountAuthorization.class);
    pa.setCreateUserId(lineItem.getCreateUserId());
    pa.setCreateDate(lineItem.getCreateDate());
    pa.setStatusCode(PendedCreditAuthorizationStatus.PENDING.getName());
    pa.setAuthorizationType(getPendedCreditAuthorizationType().getName());
    pa.setStatusDatetime(DateUtils.getNewDate());
    argBag.addObject(pa);
  }

  // //////////////////////////////////////////////////////////////////////////
  // field storage management
  // //////////////////////////////////////////////////////////////////////////
  protected final String getField(int argFieldNumber) {
    int fieldIndex = argFieldNumber - 1;

    if (fieldIndex > maxField_) {
      return "";
    }
    else {
      return fields_[fieldIndex];
    }
  }

  protected final int getMaxField() {
    return maxField_;
  }

  @Override
  protected String getMessageIdPrefix(IAuthResponse argResponse) {
    return "ACTION_CODE_";
  }

  protected PendedCreditAuthorizationType getPendedCreditAuthorizationType() {
    throw new UnsupportedOperationException("not implemented");
  }

  protected ITender getTender(IAuthorizableLineItem argLine) {
    if (argLine instanceof ITenderLineItem) {
      return ((ITenderLineItem) argLine).getTender();
    }
    return null;
  }

  protected void setAjbMessage(String argMessage) {
    String[] fields = argMessage.split(",");
    for (int i = 0; i < fields.length; i++ ) {
      setField(i + 1, fields[i]);
    }
  }

  protected void setField(int argFieldNumber, String argValue) {
    int fieldIndex = argFieldNumber - 1;
    ensureSize(fieldIndex + 1);

    if (StringUtils.isEmpty(argValue) && !StringUtils.isEmpty(fields_[fieldIndex])) {
      logger_.warn("clearing field #" + argFieldNumber);
    }

    fields_[fieldIndex] = argValue;
    maxField_ = Math.max(maxField_, fieldIndex);
  }

  public void setTender(ITender argTender) {
    tender_ = argTender;
  }

  private void ensureSize(int argSize) {
    if ((fields_ == null) || (fields_.length < argSize)) {
      String[] newFields = new String[argSize * 2];
      Arrays.fill(newFields, "");

      if (fields_ != null) {
        System.arraycopy(fields_, 0, newFields, 0, maxField_ + 1);
      }

      fields_ = newFields;
    }
  }

  protected void setFields(String[] argFields) {
    if (argFields != null) {
      System.arraycopy(argFields, 0, fields_, 0, argFields.length);
    }
  }

}
