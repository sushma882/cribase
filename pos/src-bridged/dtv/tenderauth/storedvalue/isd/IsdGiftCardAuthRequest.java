//$Id: IsdGiftCardAuthRequest.java 317 2012-08-31 15:05:05Z dtvdomain\aabdul $
package dtv.tenderauth.storedvalue.isd;

import static dtv.util.StringUtils.nonNull;

import java.text.DateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import imsformat.sv.SVRequest;
import dtv.hardware.events.IAccountInputEvent;
import dtv.pos.iframework.type.TenderUsageCodeType;
import dtv.pos.iframework.type.VoucherActivityCodeType;
import dtv.pos.tender.TenderHelper;
import dtv.pos.tender.voucher.config.ActivityConfig;
import dtv.pos.tender.voucher.config.VoucherConfig;
import dtv.tenderauth.AuthRequestType;
import dtv.tenderauth.impl.AbstractAuthRequest;
import dtv.tenderauth.impl.isd.AbstractIsdRequest;
import dtv.tenderauth.storedvalue.IStoredValueAuthRequest;
import dtv.util.DateUtils;
import dtv.util.StringUtils;
import dtv.xst.dao.tnd.ITender;
import dtv.xst.dao.trl.IAuthorizableLineItem;
import dtv.xst.dao.trl.IVoucherLineItem;

/**
 * Authorization request for gift cards via ISD.<br>
 * Copyright (c) 2007 Datavantage Corporation
 * 
 * @author dberkland
 * @version $Revision: 317 $
 * @created Feb 26, 2007
 */
public class IsdGiftCardAuthRequest
    extends AbstractIsdRequest
    implements IStoredValueAuthRequest {

  private static final Logger logger_ = Logger.getLogger(IsdGiftCardAuthRequest.class);

  // used for expiration dates
  private static final DateFormat dateFormat_ = new java.text.SimpleDateFormat("yyMM");
  private static final DateFormat dateFormat2_ = new java.text.SimpleDateFormat("yyyyMMdd");
  private static final DateFormat timeFormat_ = new java.text.SimpleDateFormat("HHmmss");

  private boolean ignoreFailure_ = false;
  private IAccountInputEvent<?> inputEvent_;
  private String currencyId_;
  private String pin_;
  private String previousTransactionId_;

  /**
   * Constructor
   * 
   * @param argType the type of authorization request
   * @param argLine the line item this request is related to, or <code>null</code> is there is no
   * related transaction line item
   */
  public IsdGiftCardAuthRequest(AuthRequestType argType, IAuthorizableLineItem argLine) {
    this(argType, argLine, null);
  }

  /**
   * Constructor
   * 
   * @param argType the type of authorization request
   * @param argLine the line item this request is related to, or <code>null</code> is there is no
   * related transaction line item
   * @param argTenderUsageCode tender usage
   */
  public IsdGiftCardAuthRequest(AuthRequestType argType, IAuthorizableLineItem argLine,
      TenderUsageCodeType argTenderUsageCode) {

    super(argType, argLine, argTenderUsageCode);

    if (argLine != null) {
      IVoucherLineItem line = (IVoucherLineItem) argLine;

      if (isKeyed(argLine.getEntryMethodCode())) {
        setParameter(SVRequest.DATA_ENTRY_METHOD1, "0");
      }
      else {
        setParameter(SVRequest.DATA_ENTRY_METHOD1, "1");
      }

      setParameter(SVRequest.TENDER_AMOUNT, AbstractAuthRequest.getAmountString(argLine));
      setTransactionSequence(line.getTransactionSequence());
    }
    else {
      // balance inquiry
    }
  }

  /**
   * get the account number from the request. This will be from the MSR swipe reader event or from
   * an account that was set.
   * @return String
   */
  @Override
  public String getAccountId() {
    return (String) getRequestMap().get(SVRequest.ACCOUNT_NUMBER);
  }

  /**
   * the three character currency id
   * @return String
   */
  @Override
  public String getCurrencyId() {
    return currencyId_;
  }

  /** {@inheritDoc} */
  @Override
  public boolean getIgnoreFailure() {
    return ignoreFailure_;
  }

  /**
   * set the mag stripe readers card track data event on the request.
   * @return MsrSwipeEvent
   */
  @Override
  public IAccountInputEvent<?> getInputEvent() {
    return inputEvent_;
  }

  /**
   * Gets the cardPIN
   * @return String
   */
  @Override
  public String getPIN() {
    return pin_;
  }

  /**
   * Gets the previous transaction id
   * @return String
   */
  @Override
  public String getPreviousTransaction() {
    return previousTransactionId_;
  }

  /**
   * Gets the transaction sequence portion of the transaction id.
   * @return long
   */
  @Override
  public final long getTransactionSequence() {
    try {
      return Integer.parseInt((String) getRequestMap().get(SVRequest.TRANSACTION_NUMBER));

    }
    catch (Exception ex) {
      logger_.debug("CAUGHT EXCEPTION", ex);
      return -1;
    }
  }

  /** {@inheritDoc} */
  @Override
  public void loadSensitiveData() {
    if (getLineItem() != null) {
      IVoucherLineItem line = (IVoucherLineItem) getLineItem();

      if (!StringUtils.isEmpty(line.getTrack2())) {
        setParameter(SVRequest.TRACK_DATA, line.getTrack2());
        setParameter(SVRequest.TRACK_NUMBER, "2");
      }
      else if (!StringUtils.isEmpty(line.getTrack1())) {
        setParameter(SVRequest.TRACK_DATA, line.getTrack1());
        setParameter(SVRequest.TRACK_NUMBER, "1");
      }
      else {
        setParameter(SVRequest.TRACK_NUMBER, "0");
      }
      // indirect call to setParameter
      setAccountId(line.getSerialNumber());
    }
  }

  /** {@inheritDoc} */
  @Override
  public void prepRequest() {
    super.prepRequest();

    if (isVoidRequest(getRequestType()) && getLineItem() != null) {
      setParameter(SVRequest.DATE1_8, nonNull(getLineItem().getStringProperty(SVRequest.DATE1_8)));
      setParameter(SVRequest.TIME1_8, nonNull(getLineItem().getStringProperty(SVRequest.TIME1_8)));
    }
    else {
      Date now = DateUtils.getNewDate();
      setParameter(SVRequest.DATE1_8, dateFormat2_.format(now));
      setParameter(SVRequest.TIME1_8, timeFormat_.format(now));

      // Saving the properties to line item to be used during void/reversal
      if (getLineItem() != null) {
        getLineItem().setStringProperty(SVRequest.TIME1_8, nonNull(getRequestMap().get(SVRequest.TIME1_8)));
        getLineItem().setStringProperty(SVRequest.DATE1_8, nonNull(getRequestMap().get(SVRequest.DATE1_8)));
      }
    }

  }

  /**
   * set the account. Typically set if account was hand entered.
   * @param argAccountId String
   */
  @Override
  public void setAccountId(String argAccountId) {
    setParameter(SVRequest.ACCOUNT_NUMBER, argAccountId);
  }

  /**
   * the three character currency id
   * @param argCurrencyId String
   */
  @Override
  public void setCurrencyId(String argCurrencyId) {
    currencyId_ = argCurrencyId;
  }

  /** {@inheritDoc} */
  @Override
  public void setIgnoreFailure(boolean argValue) {
    ignoreFailure_ = argValue;
  }

  /**
   * get the mag stripe readers card track data event on the request.
   * @param argValue MsrSwipeEvent
   */
  @Override
  public void setInputEvent(IAccountInputEvent<?> argValue) {
    inputEvent_ = argValue;
  }

  /**
   * Sets the card PIN
   * @param argPIN String
   */
  @Override
  public void setPIN(String argPIN) {
    pin_ = argPIN;
    setParameter(SVRequest.SECURITY_CODE, argPIN);
  }

  /**
   * Sets the previous transaction id
   * @param argId String
   */
  @Override
  public void setPreviousTransaction(String argId) {
    previousTransactionId_ = argId;
  }

  /**
   * Sets the transaction sequence portion of the transaction id.
   * @param argTransSeq long
   */
  @Override
  public final void setTransactionSequence(long argTransSeq) {
    String transSeq = String.valueOf(argTransSeq);
    // if greater than 9999, just take the rightmost 4 digits to ensure that
    // ISD doesn't truncate it for us...
    if (transSeq.length() > 4) {
      transSeq = transSeq.substring(transSeq.length() - 4, transSeq.length());
    }
    setParameter(SVRequest.TRANSACTION_NUMBER, transSeq);
  }

  /** {@inheritDoc} */
  @Override
  public void unloadSensitiveData() {

  }

  @Override
  protected ITender getTender(IAuthorizableLineItem argLineItem) {
    if (argLineItem == null) {
      return null;
    }

    IVoucherLineItem lineItem = (IVoucherLineItem) argLineItem;
    String voucherType = lineItem.getVoucherTypeCode();
    TenderHelper TH = TenderHelper.getInstance();
    VoucherConfig vc = TH.getVoucherRootConfig().getVoucherConfig(voucherType);
    VoucherActivityCodeType activity = VoucherActivityCodeType.valueOf(lineItem.getActivityCode());
    ActivityConfig ac = vc.getActivityConfig(activity);

    return TH.getTender(ac.getTenderId(), ac.getSourceDescription());
  }
}
