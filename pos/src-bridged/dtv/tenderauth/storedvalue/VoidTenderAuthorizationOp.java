// $Id: VoidTenderAuthorizationOp.java 888 2015-07-28 00:03:42Z suz.sxie $
package dtv.tenderauth.storedvalue;

import static dtv.pos.iframework.type.VoucherActivityCodeType.*;

import org.apache.log4j.Logger;

import dtv.pos.common.ConfigurationMgr;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.tenderauth.*;
import dtv.tenderauth.impl.op.AbstractAuthorizeOp;
import dtv.tenderauth.op.IAuthorizationCmd;
import dtv.util.StringUtils;
import dtv.util.config.ConfigUtils;
import dtv.util.config.IConfigObject;
import dtv.xst.dao.tnd.ITender;
import dtv.xst.dao.tnd.TenderStatus;
import dtv.xst.dao.ttr.*;

/**
 * Operation to void a previous authorization.<br>
 * <br>
 * Copyright (c) 2004 Datavantage Corporation
 *
 * @created November 17, 2004
 * @author dberkland
 * @version $Revision: 888 $
 */
public class VoidTenderAuthorizationOp
  extends AbstractAuthorizeOp {

  private static final long serialVersionUID = 1L;
  private static final Logger logger_ = Logger.getLogger(VoidTenderAuthorizationOp.class);
  private static final String CONTINUE_ON_FAILURE_PARAM = "ContinueOnFailure";

  private Boolean continueOnFailure_ = null;

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    if (!(argCmd instanceof ISaleTenderCmd)) {
      return false;
    }

    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    ITenderLineItem tLineItem = cmd.getTenderLineItem();
    if (!(tLineItem instanceof IAuthorizableTenderLineItem)) {
      return false;
    }

    if (tLineItem.getVoid()) {
      // already voided
      return false;
    }

    IAuthorizableTenderLineItem line = (IAuthorizableTenderLineItem) tLineItem;
    // if ((line.getAuthorizationCode() == null) && (line.getBankReferenceNumber() == null)) {
    // return false;
    // }

    final ITender tender = line.getTender();

    // if not able to resolve to a tender, no auth needed
    if (tender == null) {
      return false;
    }
    // if authRequired flag is false, no auth needed
    if (!tender.getAuthRequired()) {
      return false;
    }
    // if no auth method code, no auth needed
    if (StringUtils.isEmpty(tender.getAuthMethodCode())) {
      return false;
    }
    // if a ROOM_CHARGE tender, no auth needed
    if ("ROOM_CHARGE".equalsIgnoreCase(tender.getTenderId())) {
      return false;
    }
    return true;
  }

  /** {@inheritDoc} */
  @Override
  public void setParameter(String argName, IConfigObject argValue) {
    if (CONTINUE_ON_FAILURE_PARAM.equalsIgnoreCase(argName)) {
      continueOnFailure_ = new Boolean(ConfigUtils.toBoolean(argValue));
    }
    else {
      super.setParameter(argName, argValue);
    }
  }

  /** {@inheritDoc} */
  @Override
  protected IAuthProcess getAuthProcessor(IAuthorizationCmd argCmd) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    String authMethodCode = getAuthMethodCode(cmd.getTenderLineItem());
    return AuthFactory.getInstance().getAuthProcess(authMethodCode);
  }

  /**
   * getAuthReqType
   *
   * @param argTndrLine
   * @return AuthRequestType
   */
  protected AuthRequestType getAuthReqType(ITenderLineItem argTndrLine) {
    if (argTndrLine instanceof IVoucherTenderLineItem) {
      IVoucherTenderLineItem voucherTndrLine = (IVoucherTenderLineItem) argTndrLine;
      String activityCode = voucherTndrLine.getActivityCode();

      if (ISSUED.matches(activityCode)) {
        return AuthRequestType.VOID_ACTIVATE;
      }
      else if (RELOAD.matches(activityCode)) {
        return AuthRequestType.VOID_RELOAD;
      }
      else if (CASHOUT.matches(activityCode)) {
        return AuthRequestType.VOID_TENDER;
      }
    }

    if (TenderStatus.TENDER.matches(argTndrLine.getTenderStatusCode())) {
      return AuthRequestType.VOID_TENDER;
    }
    return AuthRequestType.VOID_REFUND_TENDER;
  }

  /** {@inheritDoc} */
  @Override
  protected IAuthRequest getAuthRequest(IAuthorizationCmd argCmd) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    IAuthorizableTenderLineItem tli = (IAuthorizableTenderLineItem) cmd.getTenderLineItem();
    String authMethodCode = getAuthMethodCode(tli);
    AuthRequestType authReqType = getAuthReqType(tli);

    boolean isVoucher = (tli instanceof IVoucherTenderLineItem);
    IAuthRequest request =
      AuthFactory.getInstance().makeAuthRequest(authMethodCode, authReqType, tli, isVoucher);

    if (request instanceof IStoredValueAuthRequest) {
      IStoredValueAuthRequest srRequest = (IStoredValueAuthRequest) request;
      srRequest.setAccountId(tli.getSerialNumber());
      // srRequest.setPreviousTransaction(line.getAuthorizationCode());
      srRequest.setAmount(tli.getAmount());
      srRequest.setTransactionSequence(tli.getTransactionSequence());
      srRequest.setCurrencyId(ConfigurationMgr.getCurrency().getCurrencyCode());
      srRequest.setPIN(cmd.getPINNumber());
    }

    return request;
  }

  /** {@inheritDoc} */
  @Override
  protected Object getPreReceiptTarget(IAuthorizationCmd argCmd) {
    return ((ISaleTenderCmd) argCmd).getTenderLineItem();
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleInitialState(IAuthorizationCmd argCmd) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    IAuthorizableTenderLineItem tli = (IAuthorizableTenderLineItem) cmd.getTenderLineItem();
    tli.setStringProperty("VOIDING", Boolean.TRUE.toString());
    return super.handleInitialState(argCmd);
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleTrainingMode(IAuthorizationCmd argCmd) {
    return HELPER.completeResponse();
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleVoid(IAuthorizationCmd argCmd) {
    argCmd.getAuthProcess().cancelRequest(argCmd.getAuthRequest());
    return (getContinueOnFailure()) ? HELPER.completeResponse() : HELPER.silentErrorResponse();
  }

  protected String getAuthMethodCode(ITenderLineItem argLineItem) {
    ITender tender = ((IAuthorizableTenderLineItem) argLineItem).getTender();
    return (tender == null) ? null : tender.getAuthMethodCode();
  }

  private boolean getContinueOnFailure() {
    if (continueOnFailure_ == null) {
      logger_.warn(CONTINUE_ON_FAILURE_PARAM + " not set on " //
        + getClass().getName() + " loaded from " //
        + getSourceDescription());
      return true;
    }
    else {
      return continueOnFailure_.booleanValue();
    }
  }
}
