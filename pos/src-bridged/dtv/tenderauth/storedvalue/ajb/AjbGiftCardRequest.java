//$Id: AjbGiftCardRequest.java 1025 2016-02-25 19:46:03Z dtvdomain\aguddissa $
package dtv.tenderauth.storedvalue.ajb;

import static dtv.tenderauth.AuthRequestType.*;
import static dtv.util.NumberUtils.isZero;
import static dtv.util.StringUtils.isEmpty;

import java.math.BigDecimal;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import dtv.hardware.events.IAccountInputEvent;
import dtv.hardware.types.HardwareType;
import dtv.pos.iframework.type.TenderUsageCodeType;
import dtv.pos.iframework.type.VoucherActivityCodeType;
import dtv.pos.tender.TenderHelper;
import dtv.pos.tender.voucher.config.ActivityConfig;
import dtv.pos.tender.voucher.config.VoucherConfig;
import dtv.tenderauth.AuthRequestType;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.ajb.request.AbstractAjbAuthRequest;
import dtv.tenderauth.storedvalue.IStoredValueAuthRequest;
import dtv.xst.dao.tnd.ITender;
import dtv.xst.dao.trl.*;

/**
 * Request object for SVS gift card authorization through AJB.<br>
 * <br>
 * Copyright (c) 2005 Datavantage Corporation
 * 
 * @author dberkland
 * @version $Revision: 1025 $
 * @created Jun 14, 2005
 */
public class AjbGiftCardRequest
    extends AbstractAjbAuthRequest
    implements IStoredValueAuthRequest {

  private static final Logger logger_ = Logger.getLogger(AjbGiftCardRequest.class);
  protected static final int CARD_NUMBER = 13;
  private static final int EXP_DATE = 14;
  private static final int TRACK2 = 15;
  private static final int STAN = 21;

  /**
   * Gets the System Trace Number (STAN) for the given line. If the line already has a calculated
   * STAN (stored as bank reference number,) that value is returned. Otherwise the STAN is
   * calculated.
   * 
   * @param argLine the line for which to get the STAN
   * @return the STAN
   */
  protected static String getSystemTraceNumber(IVoucherLineItem argLine) {
    return null;

  }

  /**
   * Retrieves the amount that should be sent in the request for the voucher activity being
   * performed. This value will typically not be configured, but it is used in some cashout
   * situations. If the value is not configured, <code>null</code> will be returned.
   * @param argLineItem the line item containing the voucher to be authorized
   * @return the amount to send in the request, if configured; <code>null</code> otherwise.<br>
   */
  private static BigDecimal getRequestAmount(IAuthorizableLineItem argLineItem) {
    IVoucherLineItem lineItem = (IVoucherLineItem) argLineItem;
    String voucherType = lineItem.getVoucherTypeCode();

    TenderHelper TH = TenderHelper.getInstance();
    VoucherConfig vc = TH.getVoucherRootConfig().getVoucherConfig(voucherType);
    VoucherActivityCodeType activity = VoucherActivityCodeType.valueOf(lineItem.getActivityCode());
    ActivityConfig ac = vc.getActivityConfig(activity);

    BigDecimal[] amounts = ac.getAvailableAmounts();
    BigDecimal amount = null;

    if ((amounts != null) && (amounts.length > 0)) {
      if (amounts[0].compareTo(BigDecimal.ZERO) > 0) {
        amount = amounts[0];
      }
    }
    return amount;
  }

  private IAccountInputEvent<?> inputEvent_;
  private String currencyId_;
  private String pin_;

  private String previousTransactionId_;

  private boolean ignoreFailure_ = false;

  /**
   * Constructor
   * 
   * @param argType the type of authorization request
   * @param argLine the line item this request is related to, or <code>null</code> is there is no
   * related transaction line item
   */
  public AjbGiftCardRequest(AuthRequestType argType, IAuthorizableLineItem argLine) {
    this(argType, argLine, null);
  }

  /**
   * Constructor
   * 
   * @param argType the type of authorization request
   * @param argLine the line item this request is related to, or <code>null</code> is there is no
   * related transaction line item
   * @param argTenderUsageCode tender usage
   */
  public AjbGiftCardRequest(AuthRequestType argType, IAuthorizableLineItem argLine,
      TenderUsageCodeType argTenderUsageCode) {

    super(argType, argLine, argTenderUsageCode, "100", "GiftCard");
    
    /* This amount is really only for cashing out a gift card.  It is needed
     * b/c when cashing out, we are actually sending a "Redeem" request with
     * the maximum possible amount and then accepting the approved amount
     * that is sent back, which turns out to be the card's balance. */
    if (CASH_OUT.equals(argType)) {
      BigDecimal requestAmount = getRequestAmount(argLine);

      if (requestAmount != null) {
        setAmount(requestAmount);
        setField(getAmountField(), getScaledAmountString(requestAmount));
      }
    }

    IVoucherLineItem line = (IVoucherLineItem) argLine;
    if (line != null) {
      if (isKeyed(argLine.getEntryMethodCode())) {
        setField(EXP_DATE, "0000");
      }
      setSystemTraceNumber(getSystemTraceNumber(line));
    }
    if (argType == ISSUE) {
      if (line instanceof IVoucherSaleLineItem) {
        IVoucherSaleLineItem vsl = (IVoucherSaleLineItem) line;
        BigDecimal faceValueAmount = vsl.getFaceValueAmount();

        if (isZero(getAmount()) && !isZero(faceValueAmount)) {
          setField(AMOUNT, getScaledAmountString(faceValueAmount));
          setAmount(faceValueAmount);
        }
      }
    }
  }

  /**
   * get the account number from the request. This will be from the MSR swipe reader event or from
   * an account that was set.
   * @return String
   */
  @Override
  public String getAccountId() {
    //modified for CRI ACT 457634/FB 367813
    String accountId = getField(CARD_NUMBER);
    if (accountId == null) {
      accountId = ((AjbGiftCardResponse) this.getResponses()[0]).getCardNumber();
    }
    return accountId;
    //return getField(CARD_NUMBER);
  }

  /**
   * the three character currency id
   * @return String
   */
  @Override
  public String getCurrencyId() {
    return currencyId_;
  }

  /** {@inheritDoc} */
  @Override
  public boolean getIgnoreFailure() {
    return ignoreFailure_;
  }

  /**
   * set the mag stripe readers card track data event on the request.
   * @return MsrSwipeEvent
   */
  @Override
  public IAccountInputEvent<?> getInputEvent() {
    return inputEvent_;
  }

  /**
   * Gets the cardPIN
   * @return String
   */
  @Override
  public String getPIN() {
    return pin_;
  }

  /**
   * Gets the previous transaction id
   * @return String
   */
  @Override
  public String getPreviousTransaction() {
    return previousTransactionId_;
  }

  /**
   * Returns an isntance of the appropriate response class for this request
   * 
   * @param argFields
   * @param argAuthCode
   * @return The appropriate response object
   */
  @Override
  public IAuthResponse getResponse(String[] argFields, String argAuthCode) {
    return new AjbGiftCardResponse(this, argFields, argAuthCode);
  }

  /** {@inheritDoc} */
  @Override
  public void loadSensitiveData() {
    IVoucherLineItem line = (IVoucherLineItem) getLineItem();
    if (line != null) {
      setField(CARD_NUMBER, line.getSerialNumber());
      setField(TRACK2, line.getTrack2());
    }
  }

  /**
   * set the account. Typically set if account was hand entered.
   * @param argAccountId String
   */
  @Override
  public void setAccountId(String argAccountId) {
    setField(CARD_NUMBER, argAccountId);
  }

  /**
   * the three character currency id
   * @param argCurrencyId String
   */
  @Override
  public void setCurrencyId(String argCurrencyId) {
    currencyId_ = argCurrencyId;
  }

  /** {@inheritDoc} */
  @Override
  public void setIgnoreFailure(boolean argValue) {
    ignoreFailure_ = argValue;
  }

  /**
   * get the mag stripe readers card track data event on the request.
   * @param argValue MsrSwipeEvent
   */
  @Override
  public void setInputEvent(IAccountInputEvent<?> argValue) {
    inputEvent_ = argValue;
  }

  /**
   * Sets the card PIN
   * @param argPIN String
   */
  @Override
  public void setPIN(String argPIN) {
    pin_ = argPIN;
  }

  /**
   * Sets the previous transaction id
   * @param argId String
   */
  @Override
  public void setPreviousTransaction(String argId) {
    previousTransactionId_ = argId;
  }

  /** {@inheritDoc} */
  @Override
  public void unloadSensitiveData() {
    setField(CARD_NUMBER, null);
    setField(TRACK2, null);
  }

  @Override
  protected String getAuthRequestTypeString(AuthRequestType argType, TenderUsageCodeType argTenderUsageCode) {
    if ((ISSUE == argType) //
        || (RELOAD == argType) //
        || (ACTIVATE == argType)) {
      return "Issue";
    }
    else if (INQUIRE_BALANCE == argType) {
      return "BalanceInquiry";
    }
    else if (TENDER == argType) {
      return "Sale";
    }
    else if (CASH_OUT == argType) {
      return "Redeem";
    }
    else if (AUTH_STATUS == argType) {
      return "AuthInquiry";
    }
    else if ((VOID_ACTIVATE == argType) || (VOID_RELOAD == argType)) {
      return "Redeem";
    }
    else if (VOID_TENDER == argType) {
      return "Issue";
    }
    else if (VOID_CASH_OUT == argType) {
      return "Issue";
    }
    else if (REVERSE == argType) {
      return "Cancel";
    }
    else {
      logger_.warn("unexpected request type: " + argType.getName() + "; using 'Cancel'");
      return "Cancel";
    }
  }

  @Override
  protected ITender getTender(IAuthorizableLineItem argLineItem) {
    if (argLineItem instanceof IVoucherLineItem) {
      IVoucherLineItem lineItem = (IVoucherLineItem) argLineItem;
      String voucherType = lineItem.getVoucherTypeCode();

      TenderHelper TH = TenderHelper.getInstance();
      VoucherConfig vc = TH.getVoucherRootConfig().getVoucherConfig(voucherType);
      VoucherActivityCodeType activity = VoucherActivityCodeType.valueOf(lineItem.getActivityCode());
      ActivityConfig ac = vc.getActivityConfig(activity);

      return TH.getTender(ac.getTenderId(), ac.getSourceDescription());
    }
    return super.getTender(argLineItem);
  }

  protected void setSystemTraceNumber(String stan) {
    if (!isEmpty(stan)) {
      setField(STAN, "*stan=" + stan);
    }
  }

}
