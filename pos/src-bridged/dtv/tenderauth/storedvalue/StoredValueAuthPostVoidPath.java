// $Id: StoredValueAuthPostVoidPath.java 478 2012-10-12 15:34:13Z dtvdomain\aabdul $
package dtv.tenderauth.storedvalue;

import java.util.ArrayList;
import java.util.List;

import dtv.pos.assistance.AssistanceHelper;
import dtv.pos.common.OpChainKey;
import dtv.pos.ejournal.postvoid.*;
import dtv.pos.iframework.op.IOpChainKey;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.register.SaleItemCmd;
import dtv.util.StringUtils;
import dtv.xst.dao.trl.IRetailTransactionLineItem;
import dtv.xst.dao.trl.IVoucherSaleLineItem;
import dtv.xst.dao.trn.IPosTransaction;

/**
 * Post void path to handle all the stored value auth's on a transaction.<br>
 * Copyright (c) 2004 Datavantage Corporation
 * 
 * @created 03/04/04
 * @author Jeff Sheldon
 * @version $Revision: 478 $
 */
public class StoredValueAuthPostVoidPath
    extends AbstractPostVoidPath
    implements IIteratorPostVoidPath {

  private List<IVoucherSaleLineItem> voucherLines_;
  private int index_;

  /** {@inheritDoc} */
  @Override
  public IXstCommand getCommand(IPostVoidCmd argCmd) {
    IVoucherSaleLineItem line = voucherLines_.get(index_);
    IXstCommand returnCmd = null;

    SaleItemCmd cmd = new SaleItemCmd();
    cmd.setTransaction(argCmd.getPostVoidTransaction());
    cmd.setVoucherLineItem(line);
    cmd.setLineItem(line);
    cmd.setNewPrice(line.getBaseUnitPrice());
    returnCmd = cmd;

    /* Do not continue on error if this is the first auth attempt.
     * Activity 172863  - C Dusseau 7/13/06 */
    cmd.setContinueOnError(index_ != 0);

    index_++ ;
    if (index_ >= voucherLines_.size()) {
      voucherLines_ = null;
    }
    return returnCmd;
  }

  /** {@inheritDoc} */
  @Override
  public IOpChainKey getOpChainKey() {
    return OpChainKey.valueOf("POST_VOID_STORED_VALUE");
  }

  /** {@inheritDoc} */
  @Override
  public boolean isApplicable(IPostVoidCmd cmd) {
    if (AssistanceHelper.getInstance().isTrainingMode()) {
      return false;
    }
    if (voucherLines_ != null) {
      return !voucherLines_.isEmpty();
    }
    return buildList(cmd);
  }

  /** {@inheritDoc} */
  @Override
  public boolean isComplete(IPostVoidCmd cmd) {
    if (index_ >= voucherLines_.size() - 1) {
      return true;
    }
    return false;
  }

  private boolean buildList(IPostVoidCmd cmd) {
    IPosTransaction trans = cmd.getTransaction();

    List<IRetailTransactionLineItem> lineItems = trans.getRetailTransactionLineItems();

    if ((lineItems == null) || lineItems.isEmpty()) {
      return false;
    }

    voucherLines_ = new ArrayList<IVoucherSaleLineItem>();
    index_ = 0;

    for (int i = 0; i < lineItems.size(); i++ ) {
      Object temp = lineItems.get(i);

      if (!(temp instanceof IVoucherSaleLineItem)) {
        continue;
      }
      IVoucherSaleLineItem line = (IVoucherSaleLineItem) temp;
      if (line.getVoid()) {
        continue;
      }
      //SVS does not send authorization code for approved GC's
      /*if (StringUtils.isEmpty(line.getAuthorizationCode())) {
        continue;
      }*/
      voucherLines_.add(line);
    }
    return !voucherLines_.isEmpty();
  }
}
