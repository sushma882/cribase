// $Id: VoidAuthorizationOp.java 886 2015-07-23 14:53:30Z suz.sxie $
package dtv.tenderauth.storedvalue;

import org.apache.log4j.Logger;

import dtv.data2.access.DataFactory;
import dtv.pos.common.ConfigurationMgr;
import dtv.pos.iframework.action.DataActionGroupKey;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.iframework.type.VoucherActivityCodeType;
import dtv.pos.register.ISaleItemCmd;
import dtv.pos.tender.TenderHelper;
import dtv.pos.tender.voucher.IVoucherCmd;
import dtv.pos.tender.voucher.config.ActivityConfig;
import dtv.pos.tender.voucher.config.VoucherConfig;
import dtv.tenderauth.*;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.op.AbstractAuthorizeOp;
import dtv.tenderauth.op.IAuthorizationCmd;
import dtv.util.StringUtils;
import dtv.util.config.ConfigUtils;
import dtv.util.config.IConfigObject;
import dtv.util.sequence.SequenceFactory;
import dtv.xst.dao.itm.IItem;
import dtv.xst.dao.tnd.ITender;
import dtv.xst.dao.trl.*;
import dtv.xst.dao.ttr.ITenderAuthLog;
import dtv.xst.dao.ttr.TenderAuthLogId;

/**
 * Operation to void a previous authorization.<br>
 * Copyright (c) 2004 Datavantage Corporation
 * 
 * @created 03/02/04
 * @author Jeff Sheldon
 * @version $Revision: 886 $
 */
public class VoidAuthorizationOp
    extends AbstractAuthorizeOp {

  private static final long serialVersionUID = 1L;
  private static final String manualAuthSuccessAdjucationCode_ = "AUTHORIZED_MANUAL";

  private static final DataActionGroupKey POST_VOID_CONTINUE_ON_ERROR_ACTION_GROUP = DataActionGroupKey
      .createForName("CONTINUE_ON_CANCEL");

  private static final Logger logger_ = Logger.getLogger(VoidAuthorizationOp.class);

  private static final String CONTINUE_ON_FAILURE_PARAM = "ContinueOnFailure";

  private static String getAuthMethodCode(IVoucherLineItem argLineItem) {
    IVoucherSaleLineItem saleLine = (IVoucherSaleLineItem) argLineItem;
    IItem item = saleLine.getItem();
    TenderHelper TH = TenderHelper.getInstance();
    ActivityConfig ac = TH.getVoucherRootConfig().getActivityConfigForItemId(item.getItemId());
    String tenderId = ac.getTenderId();
    ITender tender = TH.getTender(tenderId, ac.getSourceDescription());
    if (tender == null) {
      return null;
    }
    else {
      return tender.getAuthMethodCode();
    }
  }

  protected static VoucherConfig getVoucherConfig(IVoucherSaleLineItem argLineItem) {
    IItem item = argLineItem.getItem();
    TenderHelper TH = TenderHelper.getInstance();
    ActivityConfig ac = TH.getVoucherRootConfig().getActivityConfigForItemId(item.getItemId());
    return ac.getParent();
  }

  private Boolean continueOnFailure_ = null;

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    if (!(argCmd instanceof IVoucherCmd)) {
      return false;
    }

    IVoucherCmd vcmd = (IVoucherCmd) argCmd;
    IVoucherLineItem vLineItem = vcmd.getVoucherLineItem();
    if (!(vLineItem instanceof IVoucherSaleLineItem)) {
      return false;
    }
    //SVS does not return authorization code for Gift cards. Applicable only for manual authorizations
    if (manualAuthSuccessAdjucationCode_.equals(vLineItem.getAdjudicationCode())
        && StringUtils.isEmpty(vLineItem.getAuthorizationCode())) {
      return false;
    }

    IVoucherSaleLineItem saleLine = (IVoucherSaleLineItem) vLineItem;
    IItem item = saleLine.getItem();
    TenderHelper TH = TenderHelper.getInstance();
    ActivityConfig ac = TH.getVoucherRootConfig().getActivityConfigForItemId(item.getItemId());
    String tenderId = ac.getTenderId();
    ITender tender = TH.getTender(tenderId, ac.getSourceDescription());
    // if not able to resolve to a tender, no auth needed
    if (tender == null) {
      return false;
    }
    // if authRequired flag is false, no auth needed
    if (!tender.getAuthRequired()) {
      return false;
    }
    // if no auth method code, no auth needed
    if (StringUtils.isEmpty(tender.getAuthMethodCode())) {
      return false;
    }
    return true;
  }

  /** {@inheritDoc} */
  @Override
  public void setParameter(String argName, IConfigObject argValue) {
    if (CONTINUE_ON_FAILURE_PARAM.equalsIgnoreCase(argName)) {
      continueOnFailure_ = ConfigUtils.toBoolean(argValue);
    }
    else {
      super.setParameter(argName, argValue);
    }
  }

  @Override
  protected IAuthProcess getAuthProcessor(IAuthorizationCmd argCmd) {
    IVoucherCmd cmd = (IVoucherCmd) argCmd;
    String authMethodCode = getAuthMethodCode(cmd.getVoucherLineItem());
    return AuthFactory.getInstance().getAuthProcess(authMethodCode);
  }

  @Override
  protected IAuthRequest getAuthRequest(IAuthorizationCmd argCmd) {
    IVoucherCmd cmd = (IVoucherCmd) argCmd;

    IVoucherLineItem vli = cmd.getVoucherLineItem();
    IVoucherSaleLineItem line = (IVoucherSaleLineItem) vli;
    VoucherConfig voucherConfig = getVoucherConfig(line);

    final AuthRequestType reqType;
    VoucherActivityCodeType origType = VoucherActivityCodeType.valueOf(line.getActivityCode());
    if (origType == VoucherActivityCodeType.ISSUED) {
      reqType = AuthRequestType.VOID_ACTIVATE;
    }
    else if (origType == VoucherActivityCodeType.RELOAD) {
      reqType = AuthRequestType.VOID_RELOAD;
    }
    else if (origType == VoucherActivityCodeType.REDEEMED) {
      reqType = AuthRequestType.VOID_TENDER;
    }
    else if (origType == VoucherActivityCodeType.CASHOUT) {
      reqType = getVoidCashoutAuthRequestType();
    }
    else {
      logger_.warn("unexpected type " + origType);
      reqType = AuthRequestType.REVERSE;
    }

    //TODO -- make this optional
    IStoredValueAuthRequest request =
        (IStoredValueAuthRequest) AuthFactory.getInstance().makeAuthRequest(
            voucherConfig.getAuthMethodCode(), reqType, line, true);
    request.setAccountId(line.getSerialNumber());
    request.setAmount(line.getBaseUnitPrice());

    request.setTransactionSequence(vli.getTransactionSequence());
    request.setCurrencyId(ConfigurationMgr.getCurrency().getCurrencyCode());
    request.setPIN(cmd.getPINNumber());

    request.setIgnoreFailure(getContinueOnFailure(cmd));
    return request;
  }

  @Override
  protected DataActionGroupKey getFailedDataActionGroup(IAuthorizationCmd argCmd, IAuthResponse argResponse) {
    if (getContinueOnFailure(argCmd)) {
      return POST_VOID_CONTINUE_ON_ERROR_ACTION_GROUP;
    }

    return super.getFailedDataActionGroup(argCmd, argResponse);
  }

  protected AuthRequestType getVoidCashoutAuthRequestType() {
    return AuthRequestType.VOID_CASH_OUT;
  }

  @Override
  protected IOpResponse handleTrainingMode(IAuthorizationCmd argCmd) {
    return HELPER.completeResponse();
  }

  @Override
  protected IOpResponse handleVoid(IAuthorizationCmd argCmd) {
    if (getContinueOnFailure(argCmd)) {
      // do nothing
      saveLogEntry(((IVoucherCmd) argCmd).getVoucherLineItem());

      return HELPER.completeResponse();
    }
    else {
      // abort the chain
      return HELPER.silentErrorResponse();
    }
  }

  protected ITenderAuthLog makeLogEntry(IAuthorizableLineItem tenderLine) {
    // make an id object
    TenderAuthLogId id = makeLogEntryId(tenderLine);

    // make a log entry object
    ITenderAuthLog logEntry = DataFactory.createObject(id, ITenderAuthLog.class);

    logEntry.setResponseCode("IGNORED_FAILURE");
    logEntry.setErrorText("Cancel was selected and the Post Void proceeded.");

    return logEntry;
  }

  protected TenderAuthLogId makeLogEntryId(IAuthorizableLineItem tenderLine) {
    TenderAuthLogId id = new TenderAuthLogId();
    id.setOrganizationId(new Long(tenderLine.getOrganizationId()));
    id.setRetailLocationId(new Long(tenderLine.getRetailLocationId()));
    id.setWorkstationId(new Long(tenderLine.getWorkstationId()));
    id.setBusinessDate(tenderLine.getBusinessDate());
    id.setTransactionSequence(new Long(tenderLine.getTransactionSequence()));
    id.setRetailTransactionLineItemSequence(new Integer(tenderLine.getRetailTransactionLineItemSequence()));
    // AUTH sequence must be configured to not run outside of a valid int value (Integer.MAX_VALUE)
    id.setAttemptSequence(new Integer((int) SequenceFactory.getNextLongValue(IAuthFactory.SEQ_TYPE_AUTH)));
    return id;
  }

  protected void saveLogEntry(IAuthorizableLineItem tenderLine) {
    ITenderAuthLog logEntry = makeLogEntry(tenderLine);

    DataFactory.makePersistent(logEntry);
  }

  protected boolean getContinueOnFailure(IAuthorizationCmd argCmd) {
    if (continueOnFailure_ == null) {
      logger_.warn(CONTINUE_ON_FAILURE_PARAM + " not set on " + getClass().getName() + " loaded from "
          + getSourceDescription());
      return true;
    }
    else {
      if (continueOnFailure_.booleanValue()) {
        if (argCmd instanceof ISaleItemCmd) {
          return ((ISaleItemCmd) argCmd).isContinueOnError();
        }
        return true;
      }
      else {
        return false;
      }
    }
  }

}
