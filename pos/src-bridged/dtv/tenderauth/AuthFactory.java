//$Id: AuthFactory.java 895 2015-07-29 05:37:58Z suz.sxie $
package dtv.tenderauth;

import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.util.*;

import org.apache.log4j.Logger;

import dtv.data2.access.*;
import dtv.hardware.keyboard.PosKeyEventDispatcherFactory;
import dtv.pos.iframework.type.TenderUsageCodeType;
import dtv.tenderauth.config.*;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.event.IAuthResponseListener;
import dtv.util.*;
import dtv.util.config.*;
import dtv.xst.dao.trl.IAuthorizableLineItem;

/**
 * Factory for authorization processes..<br>
 * Copyright (c) 2004 Datavantage Corporation
 *
 * @author dberkland
 * @created February 12, 2004
 * @version $Revision: 895 $
 */
public class AuthFactory
  implements IAuthFactory {

  /** The property in <code>System.getProperties()</code> to check for an override to this class. */
  public static final String SYSTEM_PROPERTY = AuthFactory.class.getName();

  private static final Logger logger_ = Logger.getLogger(AuthFactory.class);
  private static final Class<?>[] ALTERNATE_CTOR_CLASSES = new Class<?>[] {AuthRequestType.class,
    Object[].class};
  private static final Class<?>[] SIMPLE_CTOR_CLASSES = new Class<?>[] {AuthRequestType.class,
    IAuthorizableLineItem.class};
  private static final Class<?>[] TENDER_CTOR_CLASSES = new Class<?>[] {AuthRequestType.class,
    IAuthorizableLineItem.class, TenderUsageCodeType.class};

  private static IAuthFactory INSTANCE;

  /**
   * Gets the single instance of a class implementing {@link IAuthFactory}. {@link #SYSTEM_PROPERTY}
   * is checked for an override to this class.
   *
   * @return the single instance of an {@link IAuthFactory}
   */
  public static IAuthFactory getInstance() {
    if (INSTANCE == null) {
      // Check the system configured factory to use...
      String className = System.getProperty(SYSTEM_PROPERTY);
      try {
        INSTANCE = (IAuthFactory) Class.forName(className).newInstance();
      }
      catch (Exception ex) {
        INSTANCE = new AuthFactory();
      }
    }
    return INSTANCE;
  }

  private final IAuthResponseListener routingListener_ = new IAuthResponseListener() {
    @Override
    public void responseOccurred(IAuthResponse argResponse) {
      PosKeyEventDispatcherFactory.getMainKeyEventDispatcher().routeEvent(argResponse);
    }
  };

  private String[] authMthdCodes_ = null;
  private AuthConfigHelper configHelper_;
  private Map<String, IAuthProcess> processes_;
  private List<Object> recentCommunications_;
  private WeakReference<ResetListener> resetListener_ = null;

  protected AuthFactory() {
    // singleton -- prevent public instantiation
    reset();
  }

  /** {@inheritDoc} */
  @Override
  public void addRecentCommunication(Object argMessage) {
    recentCommunications_.add(argMessage);
  }

  /** {@inheritDoc} */
  @Override
  public void addResetListener(ResetListener l) {
    resetListener_ = new WeakReference<ResetListener>(l);
  }

  /** {@inheritDoc} */
  @Override
  public void addRoutingListener(IAuthRequest argRequest) {
    // add a listener to the reponse that will come back through the event router
    argRequest.removeResponseListener(routingListener_);
    argRequest.addResponseListener(routingListener_);
  }

  /** {@inheritDoc} */
  @Override
  public String[] getAuthMethodCodes() {
    if (authMthdCodes_ == null) {
      synchronized (this) {
        if (authMthdCodes_ == null) {
          Collection<String> authMthdCodes = new TreeSet<String>();
          try {
            IQueryResultList<IGenericQueryResult> results =
              DataFactory.getObjectByQuery(AuthConstants.QK_AUTH_MTHD_CODES, new HashMap<String, Object>());

            for (IGenericQueryResult result : results) {
              authMthdCodes.add(String.valueOf(result.get(AuthConstants.FIELD_AUTH_MTHD_CODE)));
            }
          }
          catch (Exception ex) {
            logger_.info("CAUGHT EXCEPTION", ex);
          }
          authMthdCodes_ = authMthdCodes.toArray(new String[authMthdCodes.size()]);
        }
      }
    }
    return authMthdCodes_;
  }

  /** {@inheritDoc} */
  @Override
  public IAuthProcess getAuthProcess(String argAuthMethodCode) {
    IAuthProcess p = processes_.get(argAuthMethodCode);
    if (p == null) {
      p = getConfigHelper().getRootConfig().makeAuthProcess(argAuthMethodCode);
      processes_.put(argAuthMethodCode, p);
    }
    return p;
  }

  /**
   * Gets an HTML description of the authorization configuration.
   *
   * @return an HTML description of the authorization configuration.
   */
  @Override
  public String getConfigurationInformation() {
    return new AuthConfigReporter(getConfigHelper().getRootConfig()).getReport();
  }

  /**
   * Gets a property for docbuilding for the given key. The key should be three parts, seperated by
   * decimals. The first part should always be "AuthMthdCode". The next part should be the
   * AuthMthdCode. (This is the same value that would be configured in tnd_tndr.auth_mthd_code. The
   * third and final part should be the name of a parameter configured in the AuthProcess for the
   * given AuthMthdCode.
   *
   * @param argKey the property to get
   * @return the value for the property
   */
  @Override
  public Object getProperty(String argKey) {
    String[] parts = StringUtils.split(argKey, '.');
    if (!"AuthMthdCode".equals(parts[0])) {
      throw new IllegalArgumentException();
    }
    IAuthProcess authProcess = getAuthProcess(parts[1]);
    Map<String, IConfigObject> parameterMap = authProcess.getParameterMap();
    IConfigObject configObject = parameterMap.get(parts[2]);

    if (configObject instanceof IReflectionParameterCapable<?>) {
      return ((IReflectionParameterCapable<?>) configObject).getParamValue();
    }
    // hopefully we never get here unless configObject is null
    return null;
  }

  /** {@inheritDoc} */
  @Override
  public Object[] getRecentCommunications() {
    return recentCommunications_.toArray();
  }

  /** {@inheritDoc} */
  @Override
  public boolean isAuthProcessEnabled(String argCode) {
    AuthProcessConfig config = getConfigHelper().getRootConfig().getAuthProcessConfig(argCode);
    if (config == null) {
      return false;
    }
    if (!config.isEnabled()) {
      return false;
    }
    if (config.isAbstract()) {
      return false;
    }
    return true;
  }

  /** {@inheritDoc} */
  @Override
  public IAuthRequest makeAuthRequest(AuthRequestType argType, IAuthorizableLineItem argTenderLine,
                                      TenderUsageCodeType argTenderUsageCodeType, boolean argRequired) {

    String authMethodCode = argTenderLine.getAuthorizationMethodCode();
    Class<? extends IAuthRequest> clazz = getAuthRequestClass(authMethodCode, argType, argRequired);
    if ((clazz == null) && !argRequired) {
      return null;
    }
    IAuthRequest r = null;
    Constructor<? extends IAuthRequest> ctor = null;
    try {
      ctor = clazz.getConstructor(TENDER_CTOR_CLASSES);
    }
    catch (SecurityException ex) {
      throw new ReflectionException(ex);
    }
    catch (NoSuchMethodException ex) {
      // this ctor is not required, but let's give a way to get the logging if we need it
      logger_.debug("no ctor(AuthRequestType, IAuthorizableTenderLineItem, TenderUsageCodeType)", ex);
    }
    if (ctor != null) {
      try {
        r = ctor.newInstance(new Object[] {argType, argTenderLine, argTenderUsageCodeType});
      }
      catch (Exception ex) {
        throw new ReflectionException(ex);
      }
    }
    else {
      r = makeAuthRequest(authMethodCode, argType, argTenderLine, argRequired);
    }
    return initRequest(authMethodCode, argType, r);
  }

  /** {@inheritDoc} */
  @Override
  public IAuthRequest makeAuthRequest(String argAuthMethodCode, AuthRequestType argType,
                                      IAuthorizableLineItem argLineItem, boolean argRequired) {

    Class<? extends IAuthRequest> clazz = getAuthRequestClass(argAuthMethodCode, argType, argRequired);
    if ((clazz == null) && !argRequired) {
      return null;
    }
    IAuthRequest r = null;
    Constructor<? extends IAuthRequest> ctor = null;
    try {
      ctor = clazz.getConstructor(SIMPLE_CTOR_CLASSES);
    }
    catch (Exception ex) {
      logger_.error("required ctor not found", ex);
      throw new ReflectionException(ex);
    }
    try {
      r = ctor.newInstance(new Object[] {argType, argLineItem});
    }
    catch (Exception ex) {
      throw new ReflectionException(ex);
    }
    return initRequest(argAuthMethodCode, argType, r);
  }

  /** {@inheritDoc} */
  @Override
  public IAuthRequest makeAuthRequest(String argAuthMethodCode, AuthRequestType argType, Object[] argObjects) {
    Class<? extends IAuthRequest> clazz = getAuthRequestClass(argAuthMethodCode, argType, true);
    IAuthRequest r = null;
    Constructor<? extends IAuthRequest> ctor = null;
    try {
      ctor = clazz.getConstructor(ALTERNATE_CTOR_CLASSES);
    }
    catch (Exception ex) {
      logger_.error("required ctor not found", ex);
      throw new ReflectionException(ex);
    }
    try {
      r = ctor.newInstance(new Object[] {argType, argObjects});
    }
    catch (Exception ex) {
      throw new ReflectionException(ex);
    }
    return initRequest(argAuthMethodCode, argType, r);
  }

  /** {@inheritDoc} */
  @Override
  public void removeResetListener(ResetListener l) {
    if (resetListener_ != null) {
      ResetListener o = resetListener_.get();
      if (o != null) {
        if (o.equals(l)) {
          resetListener_ = null;
        }
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public void reset() {
    configHelper_ = null;
    processes_ = new HashMap<String, IAuthProcess>();
    recentCommunications_ = Collections.synchronizedList(new FixedList<Object>(100));
    ResetListener[] listeners = getResetListeners();
    for (ResetListener listener : listeners) {
      listener.handleReset();
    }
  }

  @SuppressWarnings("unchecked")
  protected Class<? extends IAuthRequest> getAuthRequestClass(String argAuthMethodCode,
                                                            AuthRequestType argType, boolean argRequired) {

    Class<?> clazz = getConfigHelper().getRootConfig().getAuthRequestClass(argAuthMethodCode, argType);
    if ((clazz != null) && (ObjectUtils.isImplementing(clazz, IAuthRequest.class))) {
      return (Class<IAuthRequest>) clazz;
    }

    if (!argRequired) {
      return null;
    }

    String message =
      clazz + " is not a class implementing " + IAuthRequest.class.getName()
        + " (configured for AuthMethodCode:" + argAuthMethodCode + ",AuthRequestType:"
        + argType.getName() + ")";
    throw new ConfigException(message);
  }

  private AuthConfigHelper getConfigHelper() {
    if (configHelper_ == null) {
      configHelper_ = new AuthConfigHelper();
      configHelper_.initialize();
    }
    return configHelper_;
  }

  private ResetListener[] getResetListeners() {
    if (resetListener_ != null) {
      ResetListener o = resetListener_.get();
      if (o != null) {
        return new ResetListener[] {o};
      }
    }
    return new ResetListener[0];
  }

  protected IAuthRequest initRequest(String argAuthMethodCode, AuthRequestType argType, IAuthRequest r) {
    setParams(argAuthMethodCode, argType, r);
    addRoutingListener(r);
    return r;
  }

  private void setParams(String argAuthMethodCode, AuthRequestType argType, IAuthRequest r) {
    ParameterConfig[] params =
      getConfigHelper().getRootConfig().getAuthRequestParameters(argAuthMethodCode, argType);
    if (r instanceof IHasParameters) {
      IHasParameters hp = (IHasParameters) r;
      for (ParameterConfig param : params) {
        param.setParameter(hp);
      }
    }
  }

}
