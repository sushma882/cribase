PRINT '$Id: CRITestStoreData.sql 451 2012-10-02 21:34:27Z dtvdomain\mschreiber $';
PRINT '$URL: https://node1000.gspann.local:8443/svn/20181115-cri/branches/fipay_cust_name/pos/database/CRITestStoreData.sql $';

-- ************************************************************************************************
--
-- This script should contain dummy store specific data used only in a test environment such as a
-- tax location, employees, registers and till sessions.
--
-- ************************************************************************************************

DECLARE @intOrganization_ID INT;
SET @intOrganization_ID = $(OrgID);
DECLARE @strCountry_ID VARCHAR(2);
SET @strCountry_ID = $(CountryID);
DECLARE @intStore_ID INT;
SET @intStore_ID = $(StoreID);
DECLARE @strCurrency_ID VARCHAR(3);
SET @strCurrency_ID = $(CurrencyID);

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- STORE SPECIFIC CONFIGURATIONS MUST GO BELOW THIS LINE
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--
-- Test Store
--
--DELETE FROM loc_rtl_loc WHERE rtl_loc_id = @intStore_ID;
--INSERT INTO loc_rtl_loc (organization_id, rtl_loc_id, store_nbr, city, state, postal_code, description, country, store_name, address1, address2, telephone1, telephone2, telephone3, telephone4, latitude, longitude)
--VALUES (@intOrganization_ID, @intStore_ID, @intStore_ID, 'San Diego', 'CA', '92121', 'Corporate Lab', 'US', 'Corporate Lab', '5910 Pacific Center Blvd.', NULL, '8585871500', NULL, NULL, NULL, 32.905154, -117.190433);

-- Set up dummy employees / customer
--DELETE FROM crm_customer_affiliation WHERE party_id IN (2, 3, 4, 5, 6, 7, 8, 9);
--INSERT INTO crm_customer_affiliation (organization_id, cust_group_id, party_id, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'EMPLOYEE', 2, getDate (), 'TestData');
--INSERT INTO crm_customer_affiliation (organization_id, cust_group_id, party_id, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'EMPLOYEE', 3, getDate (), 'TestData');
--INSERT INTO crm_customer_affiliation (organization_id, cust_group_id, party_id, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'EMPLOYEE', 4, getDate (), 'TestData');
--INSERT INTO crm_customer_affiliation (organization_id, cust_group_id, party_id, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'EMPLOYEE', 5, getDate (), 'TestData');
--INSERT INTO crm_customer_affiliation (organization_id, cust_group_id, party_id, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'EMPLOYEE', 6, getDate (), 'TestData');
--INSERT INTO crm_customer_affiliation (organization_id, cust_group_id, party_id, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'EMPLOYEE', 7, getDate (), 'TestData');
--INSERT INTO crm_customer_affiliation (organization_id, cust_group_id, party_id, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'EMPLOYEE', 8, getDate (), 'TestData');
--INSERT INTO crm_customer_affiliation (organization_id, cust_group_id, party_id, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'EMPLOYEE', 9, getDate (), 'TestData');

--DELETE FROM crm_party WHERE party_id IN (2, 3, 4, 5, 6, 7, 8, 9);
--INSERT INTO crm_party (organization_id, employee_id, party_id, cust_id, first_name, last_name,  party_typcode, preferred_locale, sign_up_rtl_loc_id, allegiance_rtl_loc_id, create_date, create_user_id)
--VALUES (@intOrganization_ID, '2', 2, '2', 'Suzie', 'Sales Associate', 'EMPLOYEE', 'en_US', 0, 0, getDate (), 'TestData');
--INSERT INTO crm_party (organization_id, employee_id, party_id, cust_id, first_name, last_name,  party_typcode, preferred_locale, sign_up_rtl_loc_id, allegiance_rtl_loc_id, create_date, create_user_id)
--VALUES (@intOrganization_ID, '3', 3, '3', 'Kelly', 'Key Holder',  'EMPLOYEE', 'en_US', 0, 0, getDate (), 'TestData');
--INSERT INTO crm_party (organization_id, employee_id, party_id, cust_id, first_name, last_name,  party_typcode, preferred_locale, sign_up_rtl_loc_id, allegiance_rtl_loc_id, create_date, create_user_id)
--VALUES (@intOrganization_ID, '4', 4, '4', 'Cathy', 'Counter Super',  'EMPLOYEE', 'en_US', 0, 0, getDate (), 'TestData');
--INSERT INTO crm_party (organization_id, employee_id, party_id, cust_id, first_name, last_name,  party_typcode, preferred_locale, sign_up_rtl_loc_id, allegiance_rtl_loc_id, create_date, create_user_id)
--VALUES (@intOrganization_ID, '5', 5, '5', 'Ashley', 'Asst. Store Manager',  'EMPLOYEE', 'en_US', 0, 0, getDate (), 'TestData');
--INSERT INTO crm_party (organization_id, employee_id, party_id, cust_id, first_name, last_name,  party_typcode, preferred_locale, sign_up_rtl_loc_id, allegiance_rtl_loc_id, create_date, create_user_id)
--VALUES (@intOrganization_ID, '6', 6, '6', 'Samantha', 'Store Manager',  'EMPLOYEE', 'en_US', 0, 0, getDate (), 'TestData');
--INSERT INTO crm_party (organization_id, employee_id, party_id, cust_id, first_name, last_name,  party_typcode, preferred_locale, sign_up_rtl_loc_id, allegiance_rtl_loc_id, create_date, create_user_id)
--VALUES (@intOrganization_ID, '7', 7, '7', 'Danny', 'District Manager',  'EMPLOYEE', 'en_US', 0, 0, getDate (), 'TestData');
--INSERT INTO crm_party (organization_id, employee_id, party_id, cust_id, first_name, last_name,  party_typcode, preferred_locale, sign_up_rtl_loc_id, allegiance_rtl_loc_id, create_date, create_user_id)
--VALUES (@intOrganization_ID, '8', 8, '8', 'Regina', 'Regional Manager',  'EMPLOYEE', 'en_US', 0, 0, getDate (), 'TestData');
--INSERT INTO crm_party (organization_id, employee_id, party_id, cust_id, first_name, last_name,  party_typcode, preferred_locale, sign_up_rtl_loc_id, allegiance_rtl_loc_id, create_date, create_user_id)
--VALUES (@intOrganization_ID, '9', 9, '9', 'Henry', 'Help Desk',  'EMPLOYEE', 'en_US', 0, 0, getDate (), 'TestData');

--DELETE FROM crm_party_locale_information WHERE party_id IN (2, 3, 4, 5, 6, 7, 8, 9);
--INSERT INTO crm_party_locale_information (organization_id, party_id, party_locale_seq, address1, address2, apartment, city, state, postal_code, country, contact_flag, primary_flag, create_date, create_user_id, update_date, update_user_id, record_state, address_type, address3, address4)
--VALUES (@intOrganization_ID, 2, 1, '159 Crocker Park Boulevard', '', NULL, 'Westlake', 'OH', '44145', 'US', 0, 1, getDate (), 'TestData', NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO crm_party_locale_information (organization_id, party_id, party_locale_seq, address1, address2, apartment, city, state, postal_code, country, contact_flag, primary_flag, create_date, create_user_id, update_date, update_user_id, record_state, address_type, address3, address4)
--VALUES (@intOrganization_ID, 3, 1, '159 Crocker Park Boulevard', '', NULL, 'Westlake', 'OH', '44145', 'US', 0, 1, getDate (), 'TestData', NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO crm_party_locale_information (organization_id, party_id, party_locale_seq, address1, address2, apartment, city, state, postal_code, country, contact_flag, primary_flag, create_date, create_user_id, update_date, update_user_id, record_state, address_type, address3, address4)
--VALUES (@intOrganization_ID, 4, 1, '159 Crocker Park Boulevard', '', NULL, 'Westlake', 'OH', '44145', 'US', 0, 1, getDate (), 'TestData', NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO crm_party_locale_information (organization_id, party_id, party_locale_seq, address1, address2, apartment, city, state, postal_code, country, contact_flag, primary_flag, create_date, create_user_id, update_date, update_user_id, record_state, address_type, address3, address4)
--VALUES (@intOrganization_ID, 5, 1, '159 Crocker Park Boulevard', '', NULL, 'Westlake', 'OH', '44145', 'US', 0, 1, getDate (), 'TestData', NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO crm_party_locale_information (organization_id, party_id, party_locale_seq, address1, address2, apartment, city, state, postal_code, country, contact_flag, primary_flag, create_date, create_user_id, update_date, update_user_id, record_state, address_type, address3, address4)
--VALUES (@intOrganization_ID, 6, 1, '159 Crocker Park Boulevard', '', NULL, 'Westlake', 'OH', '44145', 'US', 0, 1, getDate (), 'TestData', NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO crm_party_locale_information (organization_id, party_id, party_locale_seq, address1, address2, apartment, city, state, postal_code, country, contact_flag, primary_flag, create_date, create_user_id, update_date, update_user_id, record_state, address_type, address3, address4)
--VALUES (@intOrganization_ID, 7, 1, '159 Crocker Park Boulevard', '', NULL, 'Westlake', 'OH', '44145', 'US', 0, 1, getDate (), 'TestData', NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO crm_party_locale_information (organization_id, party_id, party_locale_seq, address1, address2, apartment, city, state, postal_code, country, contact_flag, primary_flag, create_date, create_user_id, update_date, update_user_id, record_state, address_type, address3, address4)
--VALUES (@intOrganization_ID, 8, 1, '159 Crocker Park Boulevard', '', NULL, 'Westlake', 'OH', '44145', 'US', 0, 1, getDate (), 'TestData', NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO crm_party_locale_information (organization_id, party_id, party_locale_seq, address1, address2, apartment, city, state, postal_code, country, contact_flag, primary_flag, create_date, create_user_id, update_date, update_user_id, record_state, address_type, address3, address4)
--VALUES (@intOrganization_ID, 9, 1, '159 Crocker Park Boulevard', '', NULL, 'Westlake', 'OH', '44145', 'US', 0, 1, getDate (), 'TestData', NULL, NULL, NULL, NULL, NULL, NULL)

--DELETE FROM hrs_employee WHERE party_id IN (2, 3, 4, 5, 6, 7, 8, 9);
--INSERT INTO hrs_employee (organization_id, employee_id, party_id, login_id, job_title, marital_status, clock_in_not_req_flag, employee_role_code, group_membership, primary_group, employee_typcode, training_status_enum, locked_out_flag, locked_out_timestamp, overtime_eligible_flag, employee_group_id, employee_statcode, create_date, create_user_id)
--VALUES (@intOrganization_ID, '2', 2, 2, 'TEST USER 2', 'S', 0, 'ER01', 'AwA=', 'EVERYONE', 'EVERYONE', 'EXEMPT', 0, NULL, 0, NULL, 'A', getDate (), 'TestData');
--INSERT INTO hrs_employee (organization_id, employee_id, party_id, login_id, job_title, marital_status, clock_in_not_req_flag, employee_role_code, group_membership, primary_group, employee_typcode, training_status_enum, locked_out_flag, locked_out_timestamp, overtime_eligible_flag, employee_group_id, employee_statcode, create_date, create_user_id)
--VALUES (@intOrganization_ID, '3', 3, 3, 'TEST USER 3', 'S', 0, 'ER02', 'BQA=', 'EVERYONE', 'EVERYONE', 'EXEMPT', 0, NULL, 0, NULL, 'A', getDate (), 'TestData');
--INSERT INTO hrs_employee (organization_id, employee_id, party_id, login_id, job_title, marital_status, clock_in_not_req_flag, employee_role_code, group_membership, primary_group, employee_typcode, training_status_enum, locked_out_flag, locked_out_timestamp, overtime_eligible_flag, employee_group_id, employee_statcode, create_date, create_user_id)
--VALUES (@intOrganization_ID, '4', 4, 4, 'TEST USER 4', 'S', 0, 'ER03', 'CQA=', 'EVERYONE', 'EVERYONE', 'EXEMPT', 0, NULL, 0, NULL, 'A', getDate (), 'TestData');
--INSERT INTO hrs_employee (organization_id, employee_id, party_id, login_id, job_title, marital_status, clock_in_not_req_flag, employee_role_code, group_membership, primary_group, employee_typcode, training_status_enum, locked_out_flag, locked_out_timestamp, overtime_eligible_flag, employee_group_id, employee_statcode, create_date, create_user_id)
--VALUES (@intOrganization_ID, '5', 5, 5, 'TEST USER 5', 'S', 0, 'ER04', 'EQA=', 'EVERYONE', 'EVERYONE', 'EXEMPT', 0, NULL, 0, NULL, 'A', getDate (), 'TestData');
--INSERT INTO hrs_employee (organization_id, employee_id, party_id, login_id, job_title, marital_status, clock_in_not_req_flag, employee_role_code, group_membership, primary_group, employee_typcode, training_status_enum, locked_out_flag, locked_out_timestamp, overtime_eligible_flag, employee_group_id, employee_statcode, create_date, create_user_id)
--VALUES (@intOrganization_ID, '6', 6, 6, 'TEST USER 6', 'S', 0, 'ER05', 'IQA=', 'EVERYONE', 'EVERYONE', 'EXEMPT', 0, NULL, 0, NULL, 'A', getDate (), 'TestData');
--INSERT INTO hrs_employee (organization_id, employee_id, party_id, login_id, job_title, marital_status, clock_in_not_req_flag, employee_role_code, group_membership, primary_group, employee_typcode, training_status_enum, locked_out_flag, locked_out_timestamp, overtime_eligible_flag, employee_group_id, employee_statcode, create_date, create_user_id)
--VALUES (@intOrganization_ID, '7', 7, 7, 'TEST USER 7', 'S', 0, 'ER06', 'QQA=', 'EVERYONE', 'EVERYONE', 'EXEMPT', 0, NULL, 0, NULL, 'A', getDate (), 'TestData');
--INSERT INTO hrs_employee (organization_id, employee_id, party_id, login_id, job_title, marital_status, clock_in_not_req_flag, employee_role_code, group_membership, primary_group, employee_typcode, training_status_enum, locked_out_flag, locked_out_timestamp, overtime_eligible_flag, employee_group_id, employee_statcode, create_date, create_user_id)
--VALUES (@intOrganization_ID, '8', 8, 8, 'TEST USER 8', 'S', 0, 'ER07', 'gQA=', 'EVERYONE', 'EVERYONE', 'EXEMPT', 0, NULL, 0, NULL, 'A', getDate (), 'TestData');
--INSERT INTO hrs_employee (organization_id, employee_id, party_id, login_id, job_title, marital_status, clock_in_not_req_flag, employee_role_code, group_membership, primary_group, employee_typcode, training_status_enum, locked_out_flag, locked_out_timestamp, overtime_eligible_flag, employee_group_id, employee_statcode, create_date, create_user_id)
--VALUES (@intOrganization_ID, '9', 9, 9, 'TEST USER 9', 'S', 0, 'ER08', 'AQE=', 'EVERYONE', 'EVERYONE', 'EXEMPT', 0, NULL, 0, NULL, 'A', getDate (), 'TestData');
--
--UPDATE hrs_employee SET clock_in_not_req_flag = 1;
--
-- Test Employee Passwords ("1")
--
--DELETE FROM hrs_employee_password WHERE employee_id IN ('2', '3', '4', '5', '6', '7', '8', '9');
--INSERT INTO hrs_employee_password (organization_id, employee_id, password_seq, password, current_password_flag, create_date, create_user_id)
--VALUES (@intOrganization_ID, '2', 0, 'GkpWMwd1W1w', 1, getDate (), 'TestData');
--INSERT INTO hrs_employee_password (organization_id, employee_id, password_seq, password, current_password_flag, create_date, create_user_id)
--VALUES (@intOrganization_ID, '3', 0, 'GkpWMwd1W1w', 1, getDate (), 'TestData');
--INSERT INTO hrs_employee_password (organization_id, employee_id, password_seq, password, current_password_flag, create_date, create_user_id)
--VALUES (@intOrganization_ID, '4', 0, 'GkpWMwd1W1w', 1, getDate (), 'TestData');
--INSERT INTO hrs_employee_password (organization_id, employee_id, password_seq, password, current_password_flag, create_date, create_user_id)
--VALUES (@intOrganization_ID, '5', 0, 'GkpWMwd1W1w', 1, getDate (), 'TestData');
--INSERT INTO hrs_employee_password (organization_id, employee_id, password_seq, password, current_password_flag, create_date, create_user_id)
--VALUES (@intOrganization_ID, '6', 0, 'GkpWMwd1W1w', 1, getDate (), 'TestData');
--INSERT INTO hrs_employee_password (organization_id, employee_id, password_seq, password, current_password_flag, create_date, create_user_id)
--VALUES (@intOrganization_ID, '7', 0, 'GkpWMwd1W1w', 1, getDate (), 'TestData');
--INSERT INTO hrs_employee_password (organization_id, employee_id, password_seq, password, current_password_flag, create_date, create_user_id)
--VALUES (@intOrganization_ID, '8', 0, 'GkpWMwd1W1w', 1, getDate (), 'TestData');
--INSERT INTO hrs_employee_password (organization_id, employee_id, password_seq, password, current_password_flag, create_date, create_user_id)
--VALUES (@intOrganization_ID, '9', 0, 'GkpWMwd1W1w', 1, getDate (), 'TestData');

--DELETE FROM hrs_employee_store WHERE employee_id IN (2, 3, 4, 5, 6, 7, 8, 9);
--Assign Employees to store # @intStore_ID
--INSERT INTO hrs_employee_store (organization_id, rtl_loc_id, employee_id, employee_store_seq, temp_assignment_flag)
--VALUES (@intOrganization_ID, @intStore_ID, 2, 0, 0);
--INSERT INTO hrs_employee_store (organization_id, rtl_loc_id, employee_id, employee_store_seq, temp_assignment_flag)
--VALUES (@intOrganization_ID, @intStore_ID, 3, 0, 0);
--INSERT INTO hrs_employee_store (organization_id, rtl_loc_id, employee_id, employee_store_seq, temp_assignment_flag)
--VALUES (@intOrganization_ID, @intStore_ID, 4, 0, 0);
--INSERT INTO hrs_employee_store (organization_id, rtl_loc_id, employee_id, employee_store_seq, temp_assignment_flag)
--VALUES (@intOrganization_ID, @intStore_ID, 5, 0, 0);
--INSERT INTO hrs_employee_store (organization_id, rtl_loc_id, employee_id, employee_store_seq, temp_assignment_flag)
--VALUES (@intOrganization_ID, @intStore_ID, 6, 0, 0);
--INSERT INTO hrs_employee_store (organization_id, rtl_loc_id, employee_id, employee_store_seq, temp_assignment_flag)
--VALUES (@intOrganization_ID, @intStore_ID, 7, 0, 0);
--INSERT INTO hrs_employee_store (organization_id, rtl_loc_id, employee_id, employee_store_seq, temp_assignment_flag)
--VALUES (@intOrganization_ID, @intStore_ID, 8, 0, 0);
--INSERT INTO hrs_employee_store (organization_id, rtl_loc_id, employee_id, employee_store_seq, temp_assignment_flag)
--VALUES (@intOrganization_ID, @intStore_ID, 9, 0, 0);

------------------------------
-- US Tax Setup - Start
------------------------------
--DELETE FROM tax_rtl_loc_tax_mapping WHERE organization_id = @intOrganization_ID;
--INSERT INTO tax_rtl_loc_tax_mapping (organization_id, rtl_loc_id, tax_loc_id, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, @intStore_ID, 'CLE', getDate (), 'TestData', NULL, NULL, NULL)

--DELETE FROM tax_tax_authority WHERE organization_id = @intOrganization_ID;
--INSERT INTO tax_tax_authority (organization_id, tax_authority_id, name, rounding_code, rounding_digits_quantity, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'EXEMPT', 'Exempt', 'HALF_UP', 4, getDate (), 'TestData', NULL, NULL, NULL)
--INSERT INTO tax_tax_authority (organization_id, tax_authority_id, name, rounding_code, rounding_digits_quantity, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'STATE', 'State Taxes', 'HALF_UP', 4, getDate (), 'TestData', NULL, NULL, NULL)
--INSERT INTO tax_tax_authority (organization_id, tax_authority_id, name, rounding_code, rounding_digits_quantity, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'NOTAX', 'No tax', 'HALF_UP', 4, getDate (), 'TestData', NULL, NULL, NULL)
--INSERT INTO tax_tax_authority (organization_id, tax_authority_id, name, rounding_code, rounding_digits_quantity, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'LOCAL', 'Local Taxes', 'HALF_UP', 4, getDate (), 'TestData', NULL, NULL, NULL)

--DELETE FROM tax_tax_group WHERE organization_id = @intOrganization_ID;
--INSERT INTO tax_tax_group (organization_id, tax_group_id, name, description, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'N', 'Non-taxable', 'Non-taxable', getDate (), 'TestData', NULL, NULL, NULL)
--INSERT INTO tax_tax_group (organization_id, tax_group_id, name, description, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'Y', 'Taxable', 'Taxable', getDate (), 'TestData', NULL, NULL, NULL)

--DELETE FROM tax_tax_group_rule WHERE organization_id = @intOrganization_ID;
--INSERT INTO tax_tax_group_rule (organization_id, tax_group_id, tax_loc_id, tax_rule_seq_nbr, tax_authority_id, name, description, compound_seq_nbr, compound_flag, taxed_at_trans_level_flag, tax_typcode, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'Y', 'CLE', 1, 'STATE', 'State Taxes 8.25%', NULL, 1, 0, 0, 'SALES', getDate (), 'TestData', NULL, NULL, NULL)
--INSERT INTO tax_tax_group_rule (organization_id, tax_group_id, tax_loc_id, tax_rule_seq_nbr, tax_authority_id, name, description, compound_seq_nbr, compound_flag, taxed_at_trans_level_flag, tax_typcode, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'Y', 'CLE', 2, 'LOCAL', 'Local Taxes 0.25%', NULL, 1, 0, 0, 'SALES', getDate (), 'TestData', NULL, NULL, NULL)
--
--INSERT INTO tax_tax_group_rule (organization_id, tax_group_id, tax_loc_id, tax_rule_seq_nbr, tax_authority_id, name, description, compound_seq_nbr, compound_flag, taxed_at_trans_level_flag, tax_typcode, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'Y', 'OTH', 1, 'STATE', 'State Taxes 4.75%', NULL, 1, 0, 0, 'SALES', getDate (), 'TestData', NULL, NULL, NULL)
--INSERT INTO tax_tax_group_rule (organization_id, tax_group_id, tax_loc_id, tax_rule_seq_nbr, tax_authority_id, name, description, compound_seq_nbr, compound_flag, taxed_at_trans_level_flag, tax_typcode, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'Y', 'OTH', 2, 'LOCAL', 'Local Taxes 1.85%', NULL, 1, 0, 0, 'SALES', getDate (), 'TestData', NULL, NULL, NULL)

--DELETE FROM tax_tax_loc WHERE organization_id = @intOrganization_ID;
--INSERT INTO tax_tax_loc (organization_id, tax_loc_id, name, description, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'CLE', 'Tax OTH', NULL, getDate (), 'TestData', NULL, NULL, NULL)
--INSERT INTO tax_tax_loc (organization_id, tax_loc_id, name, description, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'OTH', 'Tax OTH', NULL, getDate (), 'TestData', NULL, NULL, NULL)

--DELETE FROM tax_tax_rate_rule WHERE organization_id = @intOrganization_ID;
--INSERT INTO tax_tax_rate_rule (organization_id, tax_group_id, tax_loc_id, tax_rule_seq_nbr, tax_rate_rule_seq, tax_rate_min_taxable_amt, effective_datetime, expr_datetime, percentage, amt, daily_start_time, daily_end_time, tax_rate_max_taxable_amt, breakpoint_typcode, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'N', 'CLE', 1, 1, 0.000000, '20080101', '20991231 23:59:59', 0.000000, NULL, NULL, NULL, 1000000000.000000, NULL, getDate (), 'TestData', NULL, NULL, NULL)
--INSERT INTO tax_tax_rate_rule (organization_id, tax_group_id, tax_loc_id, tax_rule_seq_nbr, tax_rate_rule_seq, tax_rate_min_taxable_amt, effective_datetime, expr_datetime, percentage, amt, daily_start_time, daily_end_time, tax_rate_max_taxable_amt, breakpoint_typcode, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'Y', 'CLE', 1, 1, 0.000000, '20080101', '20991231 23:59:59', 0.055000, NULL, NULL, NULL, 1000000000.000000, NULL, getDate (), 'TestData', NULL, NULL, NULL)
--INSERT INTO tax_tax_rate_rule (organization_id, tax_group_id, tax_loc_id, tax_rule_seq_nbr, tax_rate_rule_seq, tax_rate_min_taxable_amt, effective_datetime, expr_datetime, percentage, amt, daily_start_time, daily_end_time, tax_rate_max_taxable_amt, breakpoint_typcode, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'Y', 'CLE', 2, 1, 0.000000, '20080101', '20991231 23:59:59', 0.022500, NULL, NULL, NULL, 1000000000.000000, NULL, getDate (), 'TestData', NULL, NULL, NULL)
--
--INSERT INTO tax_tax_rate_rule (organization_id, tax_group_id, tax_loc_id, tax_rule_seq_nbr, tax_rate_rule_seq, tax_rate_min_taxable_amt, effective_datetime, expr_datetime, percentage, amt, daily_start_time, daily_end_time, tax_rate_max_taxable_amt, breakpoint_typcode, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'N', 'OTH', 1, 1, 0.000000, '20080101', '20991231 23:59:59', 0.000000, NULL, NULL, NULL, 1000000000.000000, NULL, getDate (), 'TestData', NULL, NULL, NULL)
--INSERT INTO tax_tax_rate_rule (organization_id, tax_group_id, tax_loc_id, tax_rule_seq_nbr, tax_rate_rule_seq, tax_rate_min_taxable_amt, effective_datetime, expr_datetime, percentage, amt, daily_start_time, daily_end_time, tax_rate_max_taxable_amt, breakpoint_typcode, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'Y', 'OTH', 1, 1, 0.000000, '20080101', '20991231 23:59:59', 0.100000, NULL, NULL, NULL, 1000000000.000000, NULL, getDate (), 'TestData', NULL, NULL, NULL)
--INSERT INTO tax_tax_rate_rule (organization_id, tax_group_id, tax_loc_id, tax_rule_seq_nbr, tax_rate_rule_seq, tax_rate_min_taxable_amt, effective_datetime, expr_datetime, percentage, amt, daily_start_time, daily_end_time, tax_rate_max_taxable_amt, breakpoint_typcode, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'Y', 'OTH', 2, 1, 0.000000, '20080101', '20991231 23:59:59', 0.022500, NULL, NULL, NULL, 1000000000.000000, NULL, getDate (), 'TestData', NULL, NULL, NULL)

--DELETE FROM tax_postal_code_mapping WHERE organization_id = @intOrganization_ID;
--INSERT INTO tax_postal_code_mapping (organization_id, postal_code, city, tax_loc_id, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, '44140', 'Bay Village', 'OTH', getDate (), 'TestData', NULL, NULL, NULL)
------------------------------
-- US Tax Setup - End
------------------------------

-----------------------------------------------------------------------------------
-- STORE SPECIFIC CONFIGURATIONS MUST GO ABOVE THIS LINE
-----------------------------------------------------------------------------------
-------------------------------------------
-- Keep this at the end of the file.
-------------------------------------------
GO