PRINT '$Id: CRIStoreSpecificData.sql 545 2012-11-20 18:38:16Z dtvdomain\mschreiber $';
PRINT '$URL: https://node1000.gspann.local:8443/svn/20181115-cri/branches/fipay_cust_name/pos/database/CRIStoreSpecificData.sql $';

-- ************************************************************************************************
--
-- This script should contain the customer's initial store specific data load that will be run
-- during installation including store level settings such as a tax location, employees, registers
-- and till sessions.
--
-- ************************************************************************************************

DECLARE @intOrganization_ID INT;
SET @intOrganization_ID = $(OrgID);
DECLARE @strCountry_ID VARCHAR(2);
SET @strCountry_ID = $(CountryID);
DECLARE @intStore_ID INT;
SET @intStore_ID = $(StoreID);
DECLARE @strCurrency_ID VARCHAR(3);
SET @strCurrency_ID = $(CurrencyID);

DECLARE @Till_Float_Amount MONEY;
DECLARE @Store_Bank_Amount MONEY;
DECLARE @Number_Of_Registers INT;
DECLARE @RegisterCounter INT;
--------------------------------------------------------------------------------------------------------------------
-- Be sure to set the correct Organization ID and Retail Location(s) in the statements below.
-- Set the variable @Till_Float_Amount to the float amount to be configured.
-- Set the variable @Number_Of_Registers to the total number of registers to be configured.
--------------------------------------------------------------------------------------------------------------------
SET @Till_Float_Amount = 150.00;
SET @Number_Of_Registers = 10;

SET @Store_Bank_Amount = 500.00;
---------------------------------------------------------------------------------------------------------------------------------------------------------
-- STORE SPECIFIC CONFIGURATIONS MUST GO BELOW THIS LINE
-- BE SURE TO USE VARIABLE @intOrganization_ID FOR ORGANIZATION ID AND @intStore_ID FOR RETAIL LOCATION ID
---------------------------------------------------------------------------------------------------------------------------------------------------------

--Assign Employees 0 & 1 to store # @intStore_ID
DELETE FROM hrs_employee_store WHERE organization_id = @intOrganization_ID AND rtl_loc_id = @intStore_ID AND employee_id IN (0, 1);
INSERT INTO hrs_employee_store (organization_id, rtl_loc_id, employee_id, employee_store_seq, temp_assignment_flag, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 0, 0, 0, getDate (), 'BaseData');
INSERT INTO hrs_employee_store (organization_id, rtl_loc_id, employee_id, employee_store_seq, temp_assignment_flag, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 1, 0, 0, getDate (), 'BaseData');

DELETE FROM loc_state_journal WHERE organization_id = @intOrganization_ID AND rtl_loc_id = @intStore_ID;
INSERT INTO loc_state_journal (organization_id, rtl_loc_id, wkstn_id, status_typcode, state_journal_id, time_stamp, string_value, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 1, 'RTL_LOC_STATE', 1, getDate ()-1, 'CLOSED', getDate (), 'BaseData');

DELETE FROM loc_closing_message WHERE organization_id = @intOrganization_ID AND rtl_loc_id = @intStore_ID;
INSERT INTO loc_closing_message (organization_id, rtl_loc_id, closing_message, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'Store Closed', getDate (), 'BaseData');

DELETE FROM loc_org_hierarchy WHERE organization_id = @intOrganization_ID AND org_value = CONVERT(char(5), @intStore_ID);
INSERT INTO loc_org_hierarchy (organization_id, org_code, org_value, parent_code, parent_value, description, level_mgr, level_order, sort_order, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'STORE', @intStore_ID, '*', '*', 'Store ' + CONVERT(char(5), @intStore_ID), NULL, 1, @intStore_ID, getDate (), 'BaseData', NULL, NULL, NULL)

--DELETE FROM loc_pricing_hierarchy WHERE organization_id = @intOrganization_ID AND level_value = CONVERT(char(5), @intStore_ID);
--INSERT INTO loc_pricing_hierarchy (organization_id, level_code, level_value, parent_code, parent_value, description, level_order, sort_order, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'STORE', @intStore_ID, '*', '*', 'Store ' + CONVERT(char(5), @intStore_ID), 2, @intStore_ID, getDate (), 'BaseData', NULL, NULL, NULL)

DELETE FROM loc_wkstn WHERE organization_id = @intOrganization_ID AND rtl_loc_id = @intStore_ID;
DELETE FROM tsn_tndr_repository WHERE organization_id = @intOrganization_ID AND rtl_loc_id = @intStore_ID;
SET @RegisterCounter = 1;
WHILE @RegisterCounter <= @Number_Of_Registers
BEGIN
	INSERT INTO loc_state_journal (organization_id, rtl_loc_id, wkstn_id, status_typcode, state_journal_id, time_stamp, string_value, create_date, create_user_id)
	VALUES (@intOrganization_ID, @intStore_ID, @RegisterCounter, 'WKSTN_STATE', @RegisterCounter+1, getDate ()-1, 'CLOSED', getDate (), 'BaseData');
	INSERT INTO loc_wkstn (organization_id, rtl_loc_id, wkstn_id, create_date, create_user_id)
	VALUES (@intOrganization_ID, @intStore_ID, @RegisterCounter, getDate (), 'BaseData');
	INSERT INTO tsn_tndr_repository (organization_id, tndr_repository_id, rtl_loc_id, typcode, dflt_opening_cash_balance_amt, name, dflt_wkstn_id, dflt_closing_cash_balance_amt, last_closing_cash_balance_amt, not_issuable_flag, create_date, create_user_id)
	VALUES (@intOrganization_ID, 'Register ' + CONVERT(char(5), @RegisterCounter), @intStore_ID, 'TILL', @Till_Float_Amount, RTRIM('Register ' + CONVERT(char(3), @RegisterCounter)), @RegisterCounter, @Till_Float_Amount, @Till_Float_Amount, 0, getDate (), 'BaseData');
	SET @RegisterCounter = @RegisterCounter + 1;
END

INSERT INTO tsn_tndr_repository (organization_id, tndr_repository_id, rtl_loc_id, typcode, name, dflt_opening_cash_balance_amt, dflt_closing_cash_balance_amt, last_closing_cash_balance_amt, description, not_issuable_flag, create_date, create_user_id)
VALUES (@intOrganization_ID, 'STOREBANK', @intStore_ID, 'STOREBANK', 'Store Safe', @Store_Bank_Amount + (@Till_Float_Amount * @Number_Of_Registers), @Store_Bank_Amount + (@Till_Float_Amount * @Number_Of_Registers), @Store_Bank_Amount + (@Till_Float_Amount * @Number_Of_Registers), 'Store Safe', 1, getDate (), 'BaseData');
INSERT INTO tsn_tndr_repository (organization_id, tndr_repository_id, rtl_loc_id, typcode, name, dflt_opening_cash_balance_amt, dflt_closing_cash_balance_amt, description, not_issuable_flag, create_date, create_user_id)
VALUES (@intOrganization_ID, 'DEPOSIT_BANK', @intStore_ID, 'BANK', 'Deposit Bank', 0.00, 0.00, 'Deposit Bank', 1, getDate (), 'BaseData');

DELETE FROM tsn_session WHERE organization_id = @intOrganization_ID AND rtl_loc_id = @intStore_ID;
INSERT INTO tsn_session (organization_id, rtl_loc_id, tndr_repository_id, session_id, employee_party_id, statcode, create_date, create_user_id)
--VALUES (@intOrganization_ID, @intStore_ID, 'STOREBANK', 0, NULL, 'BGCOUNT', getDate (), 'BaseData');
VALUES (@intOrganization_ID, @intStore_ID, 'STOREBANK', 0, NULL, 'ENDCOUNT', getDate (), 'BaseData');

-- SESSION TENDERS
DELETE FROM tsn_session_tndr WHERE organization_id = @intOrganization_ID AND rtl_loc_id = @intStore_ID;
INSERT INTO tsn_session_tndr (organization_id, rtl_loc_id, tndr_id, session_id, actual_media_count, actual_media_amt, create_date, create_user_id, update_date, update_user_id, record_state)
SELECT @intOrganization_ID, @intStore_ID, tndr_id, 0, 0, 0.000000, getDate (), 'BaseData', NULL, NULL, NULL FROM tnd_tndr;
--
-- Update actual_media_amt for base currency (i.e. 'USD_CURRENCY') is set to Till Float Amount * Number Of Tills
--
UPDATE tsn_session_tndr SET actual_media_amt = @Store_Bank_Amount + (@Till_Float_Amount * @Number_Of_Registers) WHERE organization_id = @intOrganization_ID AND rtl_loc_id = @intStore_ID AND tndr_id = 'USD_CURRENCY';
--UPDATE tsn_session_tndr SET actual_media_amt = @Till_Float_Amount * @Number_Of_Registers WHERE organization_id = @intOrganization_ID AND rtl_loc_id = @intStore_ID AND tndr_id = 'CAD_CURRENCY';

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
-- Inventory Control Location & Buckets
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
DELETE FROM inv_location WHERE organization_id = @intOrganization_ID AND rtl_loc_id = @intStore_ID;
INSERT INTO inv_location (organization_id, rtl_loc_id, inv_location_id, name, active_flag, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'DEFAULT', 'Default Location', 1, getDate (), 'BaseData');

DELETE FROM inv_location_availability WHERE organization_id = @intOrganization_ID AND rtl_loc_id = @intStore_ID;
INSERT INTO inv_location_availability (organization_id, rtl_loc_id, location_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'DEFAULT', 'TRANSFER_FROM', getDate (), 'BaseData');
INSERT INTO inv_location_availability (organization_id, rtl_loc_id, location_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'DEFAULT', 'TRANSFER_TO', getDate (), 'BaseData');

DELETE FROM inv_bucket WHERE organization_id = @intOrganization_ID AND rtl_loc_id = @intStore_ID;
INSERT INTO inv_bucket (organization_id, rtl_loc_id, bucket_id, name, default_location_id, system_bucket_flag, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'DEFAULT', 'Default', NULL, 0, getDate (), 'BaseData');
INSERT INTO inv_bucket (organization_id, rtl_loc_id, bucket_id, name, default_location_id, system_bucket_flag, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'AT_VENDOR', 'At Vendor', NULL, 0, getDate (), 'BaseData');
INSERT INTO inv_bucket (organization_id, rtl_loc_id, bucket_id, name, default_location_id, system_bucket_flag, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'DAMAGED', 'Damaged', NULL, 0, getDate (), 'BaseData');
INSERT INTO inv_bucket (organization_id, rtl_loc_id, bucket_id, name, default_location_id, system_bucket_flag, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'LAYAWAY', 'Layaway', NULL, 0, getDate (), 'BaseData');
INSERT INTO inv_bucket (organization_id, rtl_loc_id, bucket_id, name, default_location_id, system_bucket_flag, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'ON_HAND', 'On Hand', NULL, 0, getDate (), 'BaseData');
INSERT INTO inv_bucket (organization_id, rtl_loc_id, bucket_id, name, default_location_id, system_bucket_flag, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'ON_ORDER', 'On Order', NULL, 0, getDate (), 'BaseData');
INSERT INTO inv_bucket (organization_id, rtl_loc_id, bucket_id, name, default_location_id, system_bucket_flag, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'SHIPPED', 'Shipped', NULL, 0, getDate (), 'BaseData');
INSERT INTO inv_bucket (organization_id, rtl_loc_id, bucket_id, name, default_location_id, system_bucket_flag, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'SOLD', 'Sold', NULL, 0, getDate (), 'BaseData');
INSERT INTO inv_bucket (organization_id, rtl_loc_id, bucket_id, name, default_location_id, system_bucket_flag, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'SPECIAL_ORDER', 'Special Order', NULL, 0, getDate (), 'BaseData');
INSERT INTO inv_bucket (organization_id, rtl_loc_id, bucket_id, name, default_location_id, system_bucket_flag, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'WORK_ORDER', 'Work Order', NULL, 0, getDate (), 'BaseData');

DELETE FROM inv_location_bucket WHERE organization_id = @intOrganization_ID AND rtl_loc_id = @intStore_ID;
--INSERT INTO inv_location_bucket (organization_id, rtl_loc_id, location_id, bucket_id, tracking_method, create_date, create_user_id)
--VALUES (@intOrganization_ID, @intStore_ID, 'DEFAULT', 'DEFAULT', 'ALL', getDate (), 'BaseData');
INSERT INTO inv_location_bucket (organization_id, rtl_loc_id, location_id, bucket_id, tracking_method, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'DEFAULT', 'AT_VENDOR', 'ALL', getDate (), 'BaseData');
INSERT INTO inv_location_bucket (organization_id, rtl_loc_id, location_id, bucket_id, tracking_method, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'DEFAULT', 'DAMAGED', 'ALL', getDate (), 'BaseData');
INSERT INTO inv_location_bucket (organization_id, rtl_loc_id, location_id, bucket_id, tracking_method, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'DEFAULT', 'LAYAWAY', 'ALL', getDate (), 'BaseData');
INSERT INTO inv_location_bucket (organization_id, rtl_loc_id, location_id, bucket_id, tracking_method, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'DEFAULT', 'ON_HAND', 'ALL', getDate (), 'BaseData');
INSERT INTO inv_location_bucket (organization_id, rtl_loc_id, location_id, bucket_id, tracking_method, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'DEFAULT', 'ON_ORDER', 'ALL', getDate (), 'BaseData');
INSERT INTO inv_location_bucket (organization_id, rtl_loc_id, location_id, bucket_id, tracking_method, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'DEFAULT', 'SHIPPED', 'ALL', getDate (), 'BaseData');
INSERT INTO inv_location_bucket (organization_id, rtl_loc_id, location_id, bucket_id, tracking_method, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'DEFAULT', 'SOLD', 'ALL', getDate (), 'BaseData');
INSERT INTO inv_location_bucket (organization_id, rtl_loc_id, location_id, bucket_id, tracking_method, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'DEFAULT', 'SPECIAL_ORDER', 'ALL', getDate (), 'BaseData');
INSERT INTO inv_location_bucket (organization_id, rtl_loc_id, location_id, bucket_id, tracking_method, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, 'DEFAULT', 'WORK_ORDER', 'ALL', getDate (), 'BaseData');
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
-- Inventory Control Location & Buckets
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -


-----------------------------------------------------------------------------------
-- STORE SPECIFIC CONFIGURATIONS MUST GO ABOVE THIS LINE
-----------------------------------------------------------------------------------
-------------------------------------------
-- Keep this at the end of the file.
-------------------------------------------
GO