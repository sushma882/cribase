PRINT '$Id: CRIDataUpdate.sql 1252 2018-04-26 20:12:37Z johgaug $';
PRINT '$URL: https://node1000.gspann.local:8443/svn/20181115-cri/branches/fipay_cust_name/pos/database/CRIDataUpdate.sql $';

-- ************************************************************************************************
--
-- This script contains updates to the initial data load. It should apply data changes only without
-- changing the database schema.
--
-- ************************************************************************************************
DECLARE @intOrganization_ID INT;
SET @intOrganization_ID = $(OrgID);
DECLARE @strCountry_ID VARCHAR(2);
SET @strCountry_ID = $(CountryID);
DECLARE @intStore_ID INT;
SET @intStore_ID = $(StoreID);
DECLARE @strCurrency_ID VARCHAR(3);
SET @strCurrency_ID = $(CurrencyID);

DECLARE @str_CUSTOMER_ID VARCHAR(3);
SET @str_CUSTOMER_ID = 'CRI';


--Setting min accept amount for Gift cards to be 0.01$
UPDATE tnd_tndr_user_settings SET min_accept_amt = '0.000000' WHERE organization_id = @intOrganization_ID AND tndr_id IN ('ISD_GIFT_CARD', 'ISD_GIFT_ECARD', 'ISSUE_ISD_GIFT_CARD', 'ISSUE_MERCHANDISE_CREDIT_CARD', 'MERCHANDISE_CREDIT_CARD', 'RELOAD_ISD_GIFT_CARD')
UPDATE tnd_tndr_user_settings SET online_floor_approval_amt = '0.000000' WHERE organization_id = @intOrganization_ID AND tndr_id IN ('ISD_GIFT_CARD', 'ISD_GIFT_ECARD', 'ISSUE_ISD_GIFT_CARD', 'ISSUE_MERCHANDISE_CREDIT_CARD', 'MERCHANDISE_CREDIT_CARD', 'RELOAD_ISD_GIFT_CARD')
UPDATE tnd_tndr_user_settings SET offline_floor_approval_amt = '0.000000' WHERE organization_id = @intOrganization_ID AND tndr_id IN ('ISD_GIFT_CARD', 'ISD_GIFT_ECARD', 'ISSUE_ISD_GIFT_CARD', 'ISSUE_MERCHANDISE_CREDIT_CARD', 'MERCHANDISE_CREDIT_CARD', 'RELOAD_ISD_GIFT_CARD')

--Require customer association on an issue gift card
UPDATE tnd_tndr SET cust_association_flag = 1 WHERE organization_id = @intOrganization_ID AND tndr_id = 'ISSUE_ISD_GIFT_CARD';

----------------------------------
-- Receipt Text
----------------------------------
DELETE FROM com_receipt_text WHERE organization_id = @intOrganization_ID;
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'ACCOUNT_RECEIVABLE_TERMS', 'DEFAULT', 0, '*', '*', 'Signer acknowledges receipt of goods and/or services in the amount tendered shown hereon.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'BOUNCE_BACK_2', 'DEFAULT', 0, '*', '*', 'Do not detach coupon from original sales receipt. Coupon must be attached to original receipt for redemption.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'BOUNCE_BACK_3', 'DEFAULT', 0, '*', '*', 'Not redeemable for cash, credit or a gift card. No change will be given, whether in the form of cash, credit, or gift card.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'BOUNCE_BACK_4', 'DEFAULT', 0, '*', '*', 'Coupon cannot be replaced, reissued or combined with any other coupon or promotional offer.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'BOUNCE_BACK_5', 'DEFAULT', 0, '*', '*', 'One coupon per transaction.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'BOUNCE_BACK_6', 'DEFAULT', 0, '*', '*', 'Coupon cannot be applied to employee purchases or for the purchase of gift cards.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'CUSTOMER_COPY_FOOTER', 'DEFAULT', 0, '*', '*', 'Love Us? Can we do better? Let us know at\nwww.charlotterusse.com/feedback', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'CUSTOMER_COPY_HEADER', 'DEFAULT', 0, '*', '*', 'Welcome to our store!', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'GIFT_CERTIFICATE_FOOTER', 'DEFAULT', 0, '*', '*', '*Conditions may vary. See your local store for details.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'GIFT_CERTIFICATE_HEADER', 'DEFAULT', 0, '*', '*', 'Redeemdable towards the purchase of any in-store merchandise.*', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'LAYAWAY_FOOTER', 'DEFAULT', 0, '*', '*', 'All layaways are subject to termination if regular payments are not made.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'LAYAWAY_HEADER', 'DEFAULT', 0, '*', '*', 'All layaways may be paid in full and picked up any time before the due date.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'LAYAWAY_MERCH_TICKET_FOOTER', 'DEFAULT', 0, '*', '*', 'Place all items on layaway in the appropriate reserved section.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'LAYAWAY_MERCH_TICKET_HEADER', 'DEFAULT', 0, '*', '*', 'Place this ticket with the corresponding merchandise that has been put on layaway.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'LAYAWAY_TERMS', 'DEFAULT', 0, '*', '*', 'I agree to pay the balance owed as outlined in the payment schedule. I have received a copy of the Layaway Agreement policy and terms and I agree to comply with them.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'ORDER_FOOTER', 'DEFAULT', 0, '*', '*', 'Want that new TV, but you do not want to drive all over town looking for it? We can locate it for you in any of our stores and have it ready for pickup in the one that you choose.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'ORDER_HEADER', 'DEFAULT', 0, '*', '*', 'Thank you for using us for all of your ordering needs.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'ORDER_TERMS', 'DEFAULT', 0, '*', '*', 'The fulfillment of all orders is subject to merchandise availability. We do our best to ensure the accuracy of the information that is communicated to our customers, but availability of merchandise in stores is an estimate and is not guaranteed.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'PAYROLL_DEDUCTION_TERMS', 'DEFAULT', 0, '*', '*', 'Associate acknowledges receipt of goods and/or services in the amount tendered shown hereon and authorizes payroll deduction of this amount.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'REBATE_FOOTER', 'DEFAULT', 0, '*', '*', 'Not valid for merchandise return. Rebate may be subject to additional terms and regulations in some states.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'REBATE_HEADER', 'DEFAULT', 0, '*', '*', 'Rebates must be postmarked within 10 days of the original date of purchase.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'SALE_DISCLAIMER', 'DEFAULT', 0, '*', '*', '\nRefunds and Exchanges\nWithin 30 days with original purchase receipt and tags attached.  Price adjustments will be honored within 14 days of original purchase date.  Refunds without a receipt require valid ID and will be refunded to a non-transferable merchandise credit card.\n\nGift Cards and Merchandise Credit\nmust be present for all transactions and are not replaceable if lost or stolen.  Merchandise Credits are non-transferrable and a valid ID is required to redeem.  Other terms and conditions can be found on the back of the card.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'SALE_DISCLAIMER_1_HEADER_DB', 'DEFAULT', 0, '*', '*', 'Refunds and Exchanges', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'SALE_DISCLAIMER_1_BODY_DB', 'DEFAULT', 0, '*', '*', 'Within 30 days with original purchase receipt and tags attached.  Price adjustments will be honored within 14 days of original purchase date.  Refunds without a receipt require valid ID and will be refunded to a non-transferable merchandise credit card.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'SALE_DISCLAIMER_2_HEADER_DB', 'DEFAULT', 0, '*', '*', 'Gift Cards and Merchandise Credit', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'SALE_DISCLAIMER_2_BODY_DB', 'DEFAULT', 0, '*', '*', 'must be present for all transactions and are not replaceable if lost or stolen.  Merchandise Credits are non-transferrable and a valid ID is required to redeem.  Other terms and conditions can be found on the back of the card.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'SEND_SALE_FOOTER', 'DEFAULT', 0, '*', '*', 'If any changes are required regarding the delivery of your merchandise, contact your local store.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'SEND_SALE_HEADER', 'DEFAULT', 0, '*', '*', 'Thank for you scheduling your delivery with us.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'SEND_SALE_MERCH_TICKET_FOOTER', 'DEFAULT', 0, '*', '*', 'Place the merchandise in the staging area to be loaded onto the truck for delivery.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'SEND_SALE_MERCH_TICKET_HEADER', 'DEFAULT', 0, '*', '*', 'Verify that the merchandise is being sent to the correct address.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'SEND_SALE_TERMS', 'DEFAULT', 0, '*', '*', 'All merchandise will be delivered during the allotted time period. Drivers will contact you approximately twenty minutes before arriving to let you know when within that period your merchandise will be delivered.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'SPECIAL_ORDER_FOOTER', 'DEFAULT', 0, '*', '*', 'Item availability date is provided solely as an estimate. The actual availability date may vary.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'SPECIAL_ORDER_HEADER', 'DEFAULT', 0, '*', '*', 'Items placed on special order may be picked up or sent directly to you.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'SPECIAL_ORDER_TERMS', 'DEFAULT', 0, '*', '*', 'When the customer receives the special order items, the customer must immediately and carefully inspect the merchandise for correctness, completeness and for any possible damage. Any problems must be reported within five days of receiving the merchandise. Any problems not reported within five days will not be the responsibility of the store to correct. I have read these terms and I agree to comply with them.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'STORE_COPY_FOOTER', 'DEFAULT', 0, '*', '*', 'Keep receipts in transaction order.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'STORE_COPY_HEADER', 'DEFAULT', 0, '*', '*', 'Place receipt in drawer.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'TAX_EXEMPTION_DISCLAIMER', 'DEFAULT', 0, '*', '*', 'It is unlawful to use the tax exemption status of another person or organization for your own personal benefit. Violators will be prosecuted.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'TENDER_CREDIT_TERMS', 'DEFAULT', 0, '*', '*', 'I agree to pay the above amount according to my card holder agreement.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'WORK_ORDER_FOOTER', 'DEFAULT', 0, '*', '*', 'In the event of a delay in completing your work order, you will be notified immediately.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'WORK_ORDER_HEADER', 'DEFAULT', 0, '*', '*', 'Items not picked up after 90 days will be forfeited.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'WORK_ORDER_MERCH_TICKET_FOOTER', 'DEFAULT', 0, '*', '*', 'Keep items that are going to the same vendor together to ensure efficient processing.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'WORK_ORDER_MERCH_TICKET_HEADER', 'DEFAULT', 0, '*', '*', 'Place this ticket with the corresponding merchandise that is to be repaired or altered.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'WORK_ORDER_TERMS', 'DEFAULT', 0, '*', '*', 'This store is not liable for loss, theft or damage to these items while they are in our possession.\n \nCustomer agrees to pick up merchandise at the prescribed time or within 14 days of being notified the merchandise is available.\n \nCustomer agrees to pay in full at the time of pickup.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
----------------------------------
-- Receipt Text
----------------------------------

---------------------------------
-- Handheld Employee Setup Start
---------------------------------
DELETE FROM hrs_employee WHERE organization_id = @intOrganization_ID AND employee_id = '999999';
INSERT INTO hrs_employee (organization_id, employee_id, party_id, login_id, job_title, marital_status, clock_in_not_req_flag, employee_role_code, group_membership, primary_group, employee_typcode, training_status_enum, locked_out_flag, locked_out_timestamp, overtime_eligible_flag, employee_group_id, employee_statcode, create_date, create_user_id)
VALUES (@intOrganization_ID, '999999', 999999, '999999', 'HANDHELD EMPLOYEE', NULL, 1, NULL, 'AAI=', 'EVERYONE', NULL, 'EXEMPT', 0, NULL, 0, NULL, 'A', getDate (), 'BaseData');

DELETE FROM hrs_employee_password WHERE organization_id = @intOrganization_ID AND employee_id = '999999';
INSERT INTO hrs_employee_password (organization_id, employee_id, password_seq, password, current_password_flag, create_date, create_user_id)
VALUES (@intOrganization_ID, '999999', 0, '4/IrgxtHCws', 1, getDate (), 'BaseData');

DELETE FROM crm_customer_affiliation WHERE organization_id = @intOrganization_ID AND party_id = 999999;
INSERT INTO crm_customer_affiliation (organization_id, cust_group_id, party_id, create_date, create_user_id)
VALUES (@intOrganization_ID, 'EMPLOYEE', 999999, getDate (), 'BaseData');

DELETE FROM crm_party WHERE organization_id = @intOrganization_ID AND employee_id = '999999';
INSERT INTO crm_party (organization_id, employee_id, party_id, cust_id, first_name, last_name, party_typcode, preferred_locale, sign_up_rtl_loc_id, allegiance_rtl_loc_id, create_date, create_user_id)
VALUES (@intOrganization_ID, '999999', 999999, '999999', 'Handheld', 'Unit',  'EMPLOYEE', 'en_US', 0, 0, getDate (), 'BaseData');

--Assign Employee 999999 to store # @intStore_ID
DELETE FROM hrs_employee_store WHERE organization_id = @intOrganization_ID AND rtl_loc_id = @intStore_ID AND employee_id = '999999';
INSERT INTO hrs_employee_store (organization_id, rtl_loc_id, employee_id, employee_store_seq, temp_assignment_flag, create_date, create_user_id)
VALUES (@intOrganization_ID, @intStore_ID, '999999', 0, 0, getDate (), 'BaseData');

DELETE FROM sec_groups WHERE organization_id = @intOrganization_ID AND group_id = 'HANDHELD';
INSERT INTO sec_groups (organization_id, group_id, org_code, org_value, description, bitmap_position, group_rank, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'HANDHELD', '*', '*', 'Handheld Unit', 10, 3, getDate (), 'BaseData', NULL, NULL, NULL)
--
-- Reload all privileges to account for changes from the handheld unit
--
DELETE FROM sec_privilege WHERE organization_id = @intOrganization_ID;
------------------------------------------
--Xstore Custom Privilege Settings
------------------------------------------
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'VIEW_CUSTOMER_INFO', 0, 'VIEW_CUSTOMER_INFO', 1, '8AE=', 'NO_PROMPT', 0, '8AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TRAINING_MODE', 0, 'TRAINING_MODE', 1, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'DEPOSIT_BAG_CONVEYANCE', 0, 'DEPOSIT_BAG_CONVEYANCE', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'DEPOSIT_BAG_CORRECTION', 0, 'DEPOSIT_BAG_CORRECTION', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'POST_VOID', 0, 'POST_VOID', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'PAID_IN_OUT', 0, 'PAID_IN_OUT', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'EMPLOYEE_MAINTENANCE', 0, 'EMPLOYEE_MAINTENANCE', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ELECTRONIC_JOURNAL', 0, 'ELECTRONIC_JOURNAL', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'MANAGER_DISCOUNT', 0, 'MANAGER_DISCOUNT', 1, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REFUND_CASH', 0, 'REFUND_CASH', 1, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ASSIGN_CUSTOMER', 0, 'ASSIGN_CUSTOMER', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TILL_AUDIT', 0, 'TILL_AUDIT', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'MID_DAY_DEPOSIT', 0, 'MID_DAY_DEPOSIT', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RESTRICT_HANDHELD', 0, 'RESTRICT_HANDHELD', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
------------------------------------------
--Xstore Custom Privilege Settings
------------------------------------------
------------------------------------------
--Xstore Base Privilege Settings
------------------------------------------
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ACCEPT_WARRANTY_NOT_ON_FILE', 0, 'ACCEPT_WARRANTY_NOT_ON_FILE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ACCESS_OTHER_TILLS', 0, 'ACCESS_OTHER_TILLS', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ADD_COUPON', 0, 'ADD_COUPON', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ADD_DISCOUNT', 0, 'ADD_DISCOUNT', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ALLOW_EMPLOYEE_SALE', 0, 'ALLOW_EMPLOYEE_SALE', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ALLOW_OVERSELL', 0, 'ALLOW_OVERSELL', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ASSOCIATE_ADVANCE', 0, 'ASSOCIATE_ADVANCE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ASSOCIATE_SELF_ADVANCE', 0, 'ASSOCIATE_SELF_ADVANCE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ATTACH_OTHER_TILL', 0, 'ATTACH_OTHER_TILL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ATTACH_TILL', 0, 'ATTACH_TILL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CANCEL_INVENTORY_COUNT', 0, 'CANCEL_INVENTORY_COUNT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CASH_PICKUP', 0, 'CASH_PICKUP', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CASH_REFUND_EXCEEDS_AVAILABLE_CASH', 0, 'CASH_REFUND_EXCEEDS_AVAILABLE_CASH', 1, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CASH_TRANSFER', 0, 'CASH_TRANSFER', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_OTHERS_PASSWORD', 0, 'CHANGE_OTHERS_PASSWORD', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_PRICE', 0, 'CHANGE_PRICE', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_QUANTITY', 0, 'CHANGE_QUANTITY', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_SPECIAL_ORDER_EXPECTED_DATE', 0, 'CHANGE_SPECIAL_ORDER_EXPECTED_DATE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_TAX', 0, 'CHANGE_TAX', 1, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_TAX_BY_AMOUNT', 0, 'CHANGE_TAX_BY_AMOUNT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_TAX_BY_PERCENT', 0, 'CHANGE_TAX_BY_PERCENT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_TAX_LOCATION', 0, 'CHANGE_TAX_LOCATION', 1, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_TAX_TO_EXEMPT', 0, 'CHANGE_TAX_TO_EXEMPT', 1, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_TILL_FLOAT', 0, 'CHANGE_TILL_FLOAT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_VERIFIED_RETURN_ITEM_PRICE', 0, 'CHANGE_VERIFIED_RETURN_ITEM_PRICE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CLOSE_WITH_SUSPENDED_TRANS', 0, 'CLOSE_WITH_SUSPENDED_TRANS', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'COMPLETE_INVENTORY_COUNT', 0, 'COMPLETE_INVENTORY_COUNT', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'COUPON_REDEEM_FORCE', 0, 'COUPON_REDEEM_FORCE', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'COUPON_REDEEM_OFFLINE', 0, 'COUPON_REDEEM_OFFLINE', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CREATE_CUSTOMER', 0, 'CREATE_CUSTOMER', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CREATE_SPEC_ORDER', 0, 'CREATE_SPEC_ORDER', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CUST_ASSOC', 0, 'CUST_ASSOC', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'DELETE_CUSTOMER', 0, 'DELETE_CUSTOMER', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'DISABLE_CUST_MSR', 0, 'DISABLE_CUST_MSR', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'DISABLE_MICR', 0, 'DISABLE_MICR', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'DISABLE_VALIDATION_PRINTER', 0, 'DISABLE_VALIDATION_PRINTER', 0, 'AAE=', 'NO_PROMPT', 0, 'AAE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'DISCOUNT_EXCEED_MAX_THRESHOLD', 0, 'DISCOUNT_EXCEED_MAX_THRESHOLD', 1, '8AE=', 'NO_PROMPT', 0, '8AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'EDIT_CUSTOMER', 0, 'EDIT_CUSTOMER', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'EDIT_LAYAWAY_ITEM', 0, 'EDIT_LAYAWAY_ITEM', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'EJOURNAL_ADD_COMMENT', 0, 'EJOURNAL_ADD_COMMENT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'EJOURNAL_GIFT_RECEIPT', 0, 'EJOURNAL_GIFT_RECEIPT', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'EJOURNAL_POST_VOID', 0, 'EJOURNAL_POST_VOID', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'EJOURNAL_REBATE_RECEIPT', 0, 'EJOURNAL_REBATE_RECEIPT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'EJOURNAL_REPRINT_RECEIPT', 0, 'EJOURNAL_REPRINT_RECEIPT', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'EJOURNAL_UPDATE_COMMENT', 0, 'EJOURNAL_UPDATE_COMMENT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'END_COUNT_OTHER_TILL', 0, 'END_COUNT_OTHER_TILL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'END_SALE', 0, 'END_SALE', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'FOREIGN_CURRENCY_MAINT', 0, 'FOREIGN_CURRENCY_MAINT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'INVALID_AR_MANUAL_AUTH_CODE', 0, 'INVALID_AR_MANUAL_AUTH_CODE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ISSUE_TILL', 0, 'ISSUE_TILL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'KEEP_CASH_DRAWER_OPEN', 0, 'KEEP_CASH_DRAWER_OPEN', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'LAYAWAY_CANCEL', 0, 'LAYAWAY_CANCEL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'LOG_IN', 0, 'LOG_IN', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'MAINTAIN_EMPLOYEE_MESSAGES', 0, 'MAINTAIN_EMPLOYEE_MESSAGES', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'MANUAL_CUSTOMER_BIRTH_DATE_ENTRY', 0, 'MANUAL_CUSTOMER_BIRTH_DATE_ENTRY', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'MODIFY_DISCOUNT', 0, 'MODIFY_DISCOUNT', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'NO_SALE', 0, 'NO_SALE', 1, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'NOT_FOUND_VOUCHER', 0, 'NOT_FOUND_VOUCHER', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OPEN_STORE_BANK', 0, 'OPEN_STORE_BANK', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ORDER_REJECT', 0, 'ORDER_REJECT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OVER_MAX_PAYDEDUCT_THRESHOLD', 0, 'OVER_MAX_PAYDEDUCT_THRESHOLD', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OVER_TENDER_ABOVE_CONFIGURED_AMOUNT', 0, 'OVER_TENDER_ABOVE_CONFIGURED_AMOUNT', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OVERRIDE_COUPON_EXPIRED', 0, 'OVERRIDE_COUPON_EXPIRED', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OVERRIDE_COUPON_NOT_EFFECTIVE', 0, 'OVERRIDE_COUPON_NOT_EFFECTIVE', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OVERRIDE_LAYAWAY_CANCEL_TO_ESCROW', 0, 'OVERRIDE_LAYAWAY_CANCEL_TO_ESCROW', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OVERRIDE_LAYAWAY_DEPOSIT_AMOUNT', 0, 'OVERRIDE_LAYAWAY_DEPOSIT_AMOUNT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OVERRIDE_SPEC_ORDER_CANCEL_TO_ESCROW', 0, 'OVERRIDE_SPEC_ORDER_CANCEL_TO_ESCROW', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OVERRIDE_SPECIAL_ORDER_DEPOSIT_AMOUNT', 0, 'OVERRIDE_SPECIAL_ORDER_DEPOSIT_AMOUNT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OVERRIDE_WORK_ORDER_DEPOSIT_AMOUNT', 0, 'OVERRIDE_WORK_ORDER_DEPOSIT_AMOUNT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'PAYROLL_REPOST', 0, 'PAYROLL_REPOST', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'PRINT_CUSTOMER', 0, 'PRINT_CUSTOMER', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'PRINT_GIFT_RCPTS_BEYOND_LIMIT', 0, 'PRINT_GIFT_RCPTS_BEYOND_LIMIT', 1, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RECEIVE_ENTIRE_ASN', 0, 'RECEIVE_ENTIRE_ASN', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RECEIVING', 0, 'RECEIVING', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RECEIVING_EDIT_CLOSED_DATA', 0, 'RECEIVING_EDIT_CLOSED_DATA', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RECONCILE_STORE_BANK', 0, 'RECONCILE_STORE_BANK', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RECONCILE_TILL', 0, 'RECONCILE_TILL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REFUND_TENDER_ABOVE_CONFIGURED_AMOUNT', 0, 'REFUND_TENDER_ABOVE_CONFIGURED_AMOUNT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REFUND_TENDER_BALANCE', 0, 'REFUND_TENDER_BALANCE', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REMOVE_OTHER_TILL', 0, 'REMOVE_OTHER_TILL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REMOVE_TILL', 0, 'REMOVE_TILL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REPAIR_INVOICING', 0, 'REPAIR_INVOICING', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REPLENISHMENT_ORDER_FORCE_CLOSE', 0, 'REPLENISHMENT_ORDER_FORCE_CLOSE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REPORT_DELETE_CORPORATE', 0, 'REPORT_DELETE_CORPORATE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REPORT_DELETE_PERSONAL', 0, 'REPORT_DELETE_PERSONAL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REPORT_DELETE_STORE', 0, 'REPORT_DELETE_STORE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REPRINT_PREVIOUS_DAY', 0, 'REPRINT_PREVIOUS_DAY', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_EXCEED_MAX_THRESHOLD', 0, 'RETURN_EXCEED_MAX_THRESHOLD', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_GIFT_CARD', 0, 'RETURN_GIFT_CARD', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_GIFT_CERTIFICATE', 0, 'RETURN_GIFT_CERTIFICATE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_ITEM', 0, 'RETURN_ITEM', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_ITEM_NOT_IN_HISTORY', 0, 'RETURN_ITEM_NOT_IN_HISTORY', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_ITEM_NOT_IN_ORIG_TRANS', 0, 'RETURN_ITEM_NOT_IN_ORIG_TRANS', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_KIT', 0, 'RETURN_KIT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_MAX_ITEM_PRICE_VALIDATION', 0, 'RETURN_MAX_ITEM_PRICE_VALIDATION', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_MIN_ITEM_PRICE_VALIDATION', 0, 'RETURN_MIN_ITEM_PRICE_VALIDATION', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_NOT_RETURNABLE_ITEM_OVER_MAXDAYS', 0, 'RETURN_NOT_RETURNABLE_ITEM_OVER_MAXDAYS', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_QUANTITY_EXCEED_ORIG_QUANTITY', 0, 'RETURN_QUANTITY_EXCEED_ORIG_QUANTITY', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_TILL', 0, 'RETURN_TILL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_WITH_NON_RETURN_ITEM', 0, 'RETURN_WITH_NON_RETURN_ITEM', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'SELL_GIFT_CARD', 0, 'SELL_GIFT_CARD', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'SELL_GIFT_CERTIFICATE', 0, 'SELL_GIFT_CERTIFICATE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'SELL_ITEM', 0, 'SELL_ITEM', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'SELL_ITEM_NOT_ON_FILE', 0, 'SELL_ITEM_NOT_ON_FILE', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'SELL_NON_PHYSICAL', 0, 'SELL_NON_PHYSICAL', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'SELL_POINTS_CARD', 0, 'SELL_POINTS_CARD', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'SHIPPER_METHOD_OVERRIDE', 0, 'SHIPPER_METHOD_OVERRIDE', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'SHIPPING_EDIT_CLOSED_DATA', 0, 'SHIPPING_EDIT_CLOSED_DATA', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'SPEC_ORDER_CANCEL', 0, 'SPEC_ORDER_CANCEL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'STORE_BANK_CASH_DEPOSIT', 0, 'STORE_BANK_CASH_DEPOSIT', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TENDER_ABOVE_CONFIGURED_AMOUNT', 0, 'TENDER_ABOVE_CONFIGURED_AMOUNT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TENDER_ACCOUNT_RECEIVABLE_OVER_LIMIT', 0, 'TENDER_ACCOUNT_RECEIVABLE_OVER_LIMIT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TENDER_BELOW_CONFIGURED_AMOUNT', 0, 'TENDER_BELOW_CONFIGURED_AMOUNT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TENDER_BEYOND_CHANGE_THRESHOLD', 0, 'TENDER_BEYOND_CHANGE_THRESHOLD', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TENDER_EXCHANGE', 0, 'TENDER_EXCHANGE', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TILL_COUNT_OVER_THRESHOLD_OVERRIDE', 0, 'TILL_COUNT_OVER_THRESHOLD_OVERRIDE', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TIME_CLOCK', 0, 'TIME_CLOCK', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TIME_CLOCK_DURATION', 0, 'TIME_CLOCK_DURATION', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TOTAL_LAYAWAY_ITEM_PRICE_BELOW_MIN', 0, 'TOTAL_LAYAWAY_ITEM_PRICE_BELOW_MIN', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TOTAL_SP_ORDER_ITEM_PRICE_BELOW_MIN', 0, 'TOTAL_SP_ORDER_ITEM_PRICE_BELOW_MIN', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'UNLOCK_OTHERS_REGISTER', 0, 'UNLOCK_OTHERS_REGISTER', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'UNLOCKABLE', 0, 'UNLOCKABLE', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'VIEW_ALL_TASKS', 0, 'VIEW_ALL_TASKS', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'VIEW_AND_EDIT_CUSTOMER_GROUPS', 0, 'VIEW_AND_EDIT_CUSTOMER_GROUPS', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'VIEW_CUSTOMER', 0, 'VIEW_CUSTOMER', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'VIEW_TIMECARD', 0, 'VIEW_TIMECARD', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'VOID_LINE', 0, 'VOID_LINE', 0, '/gE=', 'NO_PROMPT', 0, '/gE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ADMINISTRATIVE_WORK_CODE', 0, 'ADMINISTRATIVE_WORK_CODE', 0, '/wM=', 'NO_PROMPT', 0, '/wM=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ALL_ACCESS', 0, 'ALL_ACCESS', 0, '/wM=', 'NO_PROMPT', 0, '/wM=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_TENDER_PRICE', 0, 'CHANGE_TENDER_PRICE', 0, '/wM=', 'NO_PROMPT', 0, '/wM=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'DELETE_TENDER', 0, 'DELETE_TENDER', 0, '/wM=', 'NO_PROMPT', 0, '/wM=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'JMX_ACCESS', 0, 'JMX_ACCESS', 0, '/wM=', 'NO_PROMPT', 0, '/wM=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'NEGATIVE_PRICE_CHANGE', 0, 'NEGATIVE_PRICE_CHANGE', 0, '/wM=', 'NO_PROMPT', 0, '/wM=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OVERRIDE_6040', 0, 'OVERRIDE_6040', 0, '/wM=', 'NO_PROMPT', 0, '/wM=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'PAYROLL_EDIT_AFTER_POST', 0, 'PAYROLL_EDIT_AFTER_POST', 0, '/wM=', 'NO_PROMPT', 0, '/wM=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'PAYROLL_MAINTENANCE', 0, 'PAYROLL_MAINTENANCE', 0, '/wM=', 'NO_PROMPT', 0, '/wM=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'POSITIVE_PRICE_CHANGE', 0, 'POSITIVE_PRICE_CHANGE', 0, '/wM=', 'NO_PROMPT', 0, '/wM=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REGENERATE_PAYROLL_FILE', 0, 'REGENERATE_PAYROLL_FILE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RELOAD_GIFT_CARD', 0, 'RELOAD_GIFT_CARD', 0, '/wM=', 'NO_PROMPT', 0, '/wM=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_NOT_RETURNABLE_ITEM', 0, 'RETURN_NOT_RETURNABLE_ITEM', 0, '/wM=', 'NO_PROMPT', 0, '/wM=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_REASONCODE_MANAGER', 0, 'RETURN_REASONCODE_MANAGER', 0, '/wM=', 'NO_PROMPT', 0, '/wM=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_REASONCODE_NORMAL', 0, 'RETURN_REASONCODE_NORMAL', 0, '/wM=', 'NO_PROMPT', 0, '/wM=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TILL_MANAGEMENT', 0, 'TILL_MANAGEMENT', 0, '/wM=', 'NO_PROMPT', 0, '/wM=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TIMECARD_MAINTENANCE', 0, 'TIMECARD_MAINTENANCE', 0, '/wM=', 'NO_PROMPT', 0, '/wM=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'VOID_DISCOUNT', 0, 'VOID_DISCOUNT', 0, '/wM=', 'NO_PROMPT', 0, '/wM=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'VOID_ITEM', 0, 'VOID_ITEM', 0, '/wM=', 'NO_PROMPT', 0, '/wM=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'VOID_TENDER', 0, 'VOID_TENDER', 0, '/wM=', 'NO_PROMPT', 0, '/wM=', getDate (), 'BaseData');
------------------------------------------
--Xstore Base Privilege Settings
------------------------------------------
---------------------------------
-- Handheld Employee Setup End
---------------------------------
--
-- 11/19/12 - MDS - Fix the displayed text on the STOREBANK
--
UPDATE tsn_tndr_repository SET name = 'Store Safe', description = 'Store Safe' WHERE organization_id = @intOrganization_ID AND tndr_repository_id = 'STOREBANK';
--
-- 11/26/12 - MDS - Activity # 387250 - Add new Paid In/Out reasons
--
DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'PAID_IN' AND reason_code = 'PI06';
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'PAID_IN', 'PI06', 'Register Fund Decrease', NULL, NULL, 0.010000, 250.000000, 'NONE', NULL, NULL, NULL, NULL, 60, getDate (), 'BaseData', NULL, NULL, NULL, 0)
DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'PAID_OUT' AND reason_code = 'PO06';
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'PAID_OUT', 'PO06', 'Register Fund Increase', NULL, NULL, 0.010000, 250.000000, 'NONE', NULL, NULL, NULL, NULL, 60, getDate (), 'BaseData', NULL, NULL, NULL, 0)


--12/16/2012  388440/- Deleting the ISD records with values in code field
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category IN ('CURR_KEY', 'KEY_ID');

-- 388558 - Alter to clear out old messages to reduce the size of the ctl_event_log table
IF EXISTS(SELECT * FROM sys.databases WHERE name = 'xstorereplication')
BEGIN
 DELETE FROM xstorereplication..ctl_replication_queue WHERE service_name = 'EventLogReplicationCritical';
END

-- 12/18/2012 - Update employee deals so that they have the highest priority and fire first
UPDATE prc_deal SET priority_nudge = -1 WHERE organization_id = @intOrganization_ID AND deal_id IN ('EMP_CLR_20', 'EMP_REG_40', 'EMP_CLR_SPECIAL', 'EMP_REG_SPECIAL');

--
-- 2/20/2013 - MDS - Change discounts to allow them to be combined
--
UPDATE dsc_discount SET exclusive_discount_flag = 0 WHERE organization_id = @intOrganization_ID;


-- 391035/-- Daily Sales Report needs modification.


DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'CUSTOMER_GROUPS' AND code in ('MILITARY', 'STUDENT');

DELETE FROM dsc_discount_group_mapping WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-T1';
INSERT INTO dsc_discount_group_mapping (organization_id, cust_group_id, discount_code, org_code, org_value, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DEFAULT', 'DISC-T1', '*', '*', getDate (), 'BaseData', NULL, NULL, NULL);
--

DELETE FROM dsc_discount_group_mapping WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-T2';
INSERT INTO dsc_discount_group_mapping (organization_id, cust_group_id, discount_code, org_code, org_value, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DEFAULT', 'DISC-T2', '*', '*', getDate (), 'BaseData', NULL, NULL, NULL);
--

DELETE FROM dsc_discount_group_mapping WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-L1';
INSERT INTO dsc_discount_group_mapping (organization_id, cust_group_id, discount_code, org_code, org_value, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DEFAULT', 'DISC-L1', '*', '*', getDate (), 'BaseData', NULL, NULL, NULL);
--

DELETE FROM dsc_discount_group_mapping WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-L2';
INSERT INTO dsc_discount_group_mapping (organization_id, cust_group_id, discount_code, org_code, org_value, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DEFAULT', 'DISC-L2', '*', '*', getDate (), 'BaseData', NULL, NULL, NULL);
--

DELETE FROM dsc_discount_group_mapping WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-L3';
INSERT INTO dsc_discount_group_mapping (organization_id, cust_group_id, discount_code, org_code, org_value, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DEFAULT', 'DISC-L3', '*', '*', getDate (), 'BaseData', NULL, NULL, NULL);
--

DELETE FROM dsc_discount_group_mapping WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-L4';
INSERT INTO dsc_discount_group_mapping (organization_id, cust_group_id, discount_code, org_code, org_value, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DEFAULT', 'DISC-L4', '*', '*', getDate (), 'BaseData', NULL, NULL, NULL);
--

DELETE FROM dsc_discount_group_mapping WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-L5';
INSERT INTO dsc_discount_group_mapping (organization_id, cust_group_id, discount_code, org_code, org_value, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DEFAULT', 'DISC-L5', '*', '*', getDate (), 'BaseData', NULL, NULL, NULL);


DELETE FROM cri_dsc_discount_type WHERE organization_id = @intOrganization_ID;
INSERT INTO cri_dsc_discount_type (organization_id, discount_code, discount_type, org_code, org_value, create_date)
VALUES (@intOrganization_ID, 'DISC-L4', 'MILITARY', '*', '*', getDate ());
INSERT INTO cri_dsc_discount_type (organization_id, discount_code, discount_type, org_code, org_value, create_date)
VALUES (@intOrganization_ID, 'DISC-L5', 'STUDENT', '*', '*', getDate ());
INSERT INTO cri_dsc_discount_type (organization_id, discount_code, discount_type, org_code, org_value, create_date)
VALUES (@intOrganization_ID, 'DISC-T1', 'MILITARY', '*', '*', getDate ());
INSERT INTO cri_dsc_discount_type (organization_id, discount_code, discount_type, org_code, org_value, create_date)
VALUES (@intOrganization_ID, 'DISC-T2', 'STUDENT', '*', '*', getDate ());

DELETE FROM rpt_organizer where organization_id = @intOrganization_ID AND report_name = 'flash_sales' AND report_group = 'Sales' AND report_element = 'Promotions';
INSERT INTO rpt_organizer(organization_id,report_name,report_group,report_element,report_order,create_date,create_user_id)
VALUES(@intOrganization_ID, 'flash_sales', 'Sales', 'Promotions', 45, getDate(), 'BaseData');

DELETE FROM rpt_organizer where organization_id = @intOrganization_ID AND report_name = 'flash_sales' AND report_group = 'Sales' AND report_element = 'Overrides';
INSERT INTO rpt_organizer(organization_id,report_name,report_group,report_element,report_order,create_date,create_user_id)
VALUES(@intOrganization_ID, 'flash_sales', 'Sales', 'Overrides', 46, getDate(), 'BaseData');
--Activity #391035 end


--Activity 404777

IF NOT EXISTS (select * from tnd_tndr where organization_id = @intOrganization_ID and tndr_id='ATM' and tndr_typcode ='MISCELLANEOUS')
INSERT INTO [tnd_tndr]
           ([organization_id]  ,[tndr_id] ,[org_code] ,[org_value] ,[tndr_typcode] ,[currency_id]
           ,[description] ,[auth_mthd_code] ,[serial_id_nbr_req_flag] ,[auth_req_flag] ,[auth_expr_date_req_flag]
           ,[pin_req_flag] ,[cust_sig_req_flag] ,[endorsement_req_flag] ,[open_cash_drawer_req_flag]
           ,[unit_count_req_code]  ,[mag_swipe_reader_req_flag] ,[dflt_to_amt_due_flag]
           ,[min_denomination_amt] ,[display_order] ,[reporting_group]  ,[effective_date]
           ,[expr_date] ,[min_days_for_return] ,[max_days_for_return] ,[cust_id_req_code] ,[cust_association_flag]
           ,[populate_system_count_flag]  ,[include_in_type_count_flag] ,[suggested_deposit_threshold]  ,[suggest_deposit_flag]
           ,[change_tndr_id]  ,[cash_change_limit] ,[over_tender_overridable_flag]  ,[non_voidable_flag]
           ,[disallow_split_tndr_flag] ,[close_count_disc_threshold]  ,[cid_msr_req_flag] ,[cid_keyed_req_flag]
           ,[postal_code_req_flag] ,[post_void_open_drawer_flag]  ,[create_date]  ,[create_user_id]
           ,[update_date]  ,[update_user_id]  ,[record_state])
     VALUES
           (@intOrganization_ID,'ATM','*','*','MISCELLANEOUS','USD','ATM Transaction','MANUAL',1,1,1,0,0,0,0,'TOTAL_NORMAL',1,0,NULL,NULL,'TENDER SUMMARY',NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,'LOCAL_CURRENCY',NULL,0,0,0,NULL,0,0,0,0,NULL,NULL,NULL,'',NULL)



IF NOT EXISTS (select * from tnd_tndr_availability where organization_id = @intOrganization_ID and tndr_id='ATM' and availability_code ='CASH_DRAWER_REPORT')
INSERT INTO [tnd_tndr_availability]
           ([organization_id]  ,[tndr_id] ,[availability_code] ,[org_code] ,[org_value]
           ,[create_date] ,[create_user_id] ,[update_date] ,[update_user_id] ,[record_state])
     VALUES
           (@intOrganization_ID,	'ATM',	'CASH_DRAWER_REPORT',	'*',	'*',	NULL,	NULL,	NULL,	NULL,	NULL)




IF NOT EXISTS (select * from tnd_tndr_availability where organization_id = @intOrganization_ID and tndr_id='ATM' and availability_code ='RETURN_WITH_RECEIPT')
INSERT INTO [tnd_tndr_availability]
           ([organization_id] ,[tndr_id] ,[availability_code] ,[org_code]
           ,[org_value] ,[create_date] ,[create_user_id] ,[update_date]
           ,[update_user_id] ,[record_state])
     VALUES
           (@intOrganization_ID,	'ATM',	'RETURN_WITH_RECEIPT',	'*',	'*',	NULL,	NULL,	NULL,	NULL,	NULL)



IF NOT EXISTS (select * from tnd_tndr_availability where organization_id = @intOrganization_ID and tndr_id='ATM' and availability_code ='SALE')
INSERT INTO [tnd_tndr_availability]
           ([organization_id] ,[tndr_id] ,[availability_code] ,[org_code] ,[org_value]
           ,[create_date] ,[create_user_id] ,[update_date] ,[update_user_id] ,[record_state])
     VALUES
           (@intOrganization_ID,	'ATM',	'SALE',	'*',	'*',	NULL,	NULL,	NULL,	NULL,	NULL)





IF NOT EXISTS (select * from tnd_tndr_typcode where organization_id = @intOrganization_ID and tndr_typcode='MISCELLANEOUS')
INSERT INTO tnd_tndr_typcode (organization_id, tndr_typcode, description, sort_order, unit_count_req_code) 
VALUES (@intOrganization_ID, 'MISCELLANEOUS', 'ATM', 40, 'TOTAL_NORMAL');

IF NOT EXISTS (select * from tnd_tndr_user_settings where organization_id = @intOrganization_ID and tndr_id='ATM')
INSERT INTO [tnd_tndr_user_settings]
           ([organization_id] ,[tndr_id],[group_id],[usage_code] ,[entry_mthd_code],[org_code] ,[org_value] ,[online_floor_approval_amt] ,[online_ceiling_approval_amt] ,[over_tndr_limit]
           ,[offline_floor_approval_amt] ,[offline_ceiling_approval_amt] ,[min_accept_amt] ,[max_accept_amt] ,[max_refund_with_receipt] ,[max_refund_wo_receipt] ,[create_date]
           ,[create_user_id] ,[update_date] ,[update_user_id] ,[record_state])
     VALUES
           (@intOrganization_ID,	'ATM', 'EVERYONE',	'DEFAULT', 		'DEFAULT',	'*',  	'*',	0.010000,	1000.000000,	0.000000,	1111.000000,	1111.000000,	0.010000,	1111.000000,	0.000000,	0.000000,	NULL,	NULL,	NULL,	NULL,	NULL)


IF NOT EXISTS (SELECT * FROM tsn_session_tndr WHERE organization_id = @intOrganization_ID AND tndr_id = 'ATM' and rtl_loc_id = @intStore_ID)
INSERT INTO tsn_session_tndr (organization_id, rtl_loc_id, tndr_id, session_id, actual_media_count, actual_media_amt, create_date, create_user_id, update_date, update_user_id, record_state)
values(@intOrganization_ID, @intStore_ID, 'ATM', 0, 0, 0.000000, getDate (), 'BaseData', NULL, NULL, NULL)

---end Activity 404777

--401324 - Dual Tax Rates for Native America Tribe vs. non-Tribe members--

DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'TAX_CHANGE' and reason_code = '703tribal';
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'TAX_CHANGE', '703tribal', '703 Tribal', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, NULL, NULL, 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)

DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'TAX_CHANGE' and reason_code = '711tribal';
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'TAX_CHANGE', '711tribal', '711 Tribal', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, NULL, NULL, 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)

--CRI 427124 Clothing Voucher Tender
DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'TAX_EXEMPT' and reason_code = 'TE13';
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'TAX_EXEMPT', 'TE13', 'West Virginia Tax Exempt', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, NULL, NULL, 130, getDate (), 'BaseData', NULL, NULL, NULL, 0)

DELETE FROM tnd_tndr WHERE organization_id = @intOrganization_ID AND tndr_id = 'CLOTHING_VOUCHER';
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'CLOTHING_VOUCHER', '*', '*', 'VOUCHER', 'USD', 'Clothing Voucher', NULL, 1, 0, 0, 0, 0, 1, 1, 'UNIT_SHORT', 0, 1, NULL, 80, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)

DELETE FROM tnd_tndr_availability WHERE organization_id = @intOrganization_ID AND tndr_id = 'CLOTHING_VOUCHER' and availability_code in ('CASH_DRAWER_REPORT', 'TILL_COUNT', 'DEPOSIT');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'CLOTHING_VOUCHER', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'CLOTHING_VOUCHER', 'TILL_COUNT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'CLOTHING_VOUCHER', 'DEPOSIT', getDate (), 'BaseData');


DELETE FROM tnd_tndr_user_settings WHERE organization_id = @intOrganization_ID AND tndr_id = 'CLOTHING_VOUCHER';
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'CLOTHING_VOUCHER', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', NULL, NULL, 4999.990000, NULL, NULL, 0.010000, 5000.000000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)

DELETE FROM rpt_organizer WHERE organization_id = @intOrganization_ID AND report_element = 'CLOTHING_VOUCHER';
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'CLOTHING_VOUCHER', 660, getDate (), 'BaseData');

IF NOT EXISTS (SELECT * FROM tsn_session_tndr WHERE organization_id = @intOrganization_ID AND tndr_id = 'CLOTHING_VOUCHER' and rtl_loc_id = @intStore_ID)
INSERT INTO tsn_session_tndr (organization_id, rtl_loc_id, tndr_id, session_id, actual_media_count, actual_media_amt, create_date, create_user_id, update_date, update_user_id, record_state)
values(@intOrganization_ID, @intStore_ID, 'CLOTHING_VOUCHER', 0, 0, 0.000000, getDate (), 'BaseData', NULL, NULL, NULL)


-- Activity 427121- SS-68:  Store 745 Casino rewards tender
IF NOT EXISTS (select * from tnd_tndr where organization_id = @intOrganization_ID and tndr_id='CASINO_REWARDS' and tndr_typcode ='CHECK')
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'CASINO_REWARDS', '*', '*', 'CHECK', 'USD', 'Casino Rewards',NULL, 1, 0, 0, 0, 0, 1, 0, 'UNIT_SHORT', 0, 0, NULL, NULL, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'LOCAL_CURRENCY', NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)

IF NOT EXISTS (select * from tnd_tndr_availability where organization_id = @intOrganization_ID and tndr_id='CASINO_REWARDS' and availability_code ='CASH_DRAWER_REPORT')
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'CASINO_REWARDS', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
IF NOT EXISTS (select * from tnd_tndr_availability where organization_id = @intOrganization_ID and tndr_id='CASINO_REWARDS' and availability_code ='DEPOSIT')
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'CASINO_REWARDS', 'DEPOSIT', getDate (), 'BaseData');
IF NOT EXISTS (select * from tnd_tndr_availability where organization_id = @intOrganization_ID and tndr_id='CASINO_REWARDS' and availability_code ='ORDER')
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'CASINO_REWARDS', 'ORDER', getDate (), 'BaseData');
IF NOT EXISTS (select * from tnd_tndr_availability where organization_id = @intOrganization_ID and tndr_id='CASINO_REWARDS' and availability_code ='SALE')
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'CASINO_REWARDS', 'SALE', getDate (), 'BaseData');
IF NOT EXISTS (select * from tnd_tndr_availability where organization_id = @intOrganization_ID and tndr_id='CASINO_REWARDS' and availability_code ='TILL_COUNT')
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'CASINO_REWARDS', 'TILL_COUNT', getDate (), 'BaseData');

IF NOT EXISTS (select * from tnd_tndr_user_settings where organization_id = @intOrganization_ID and tndr_id='CASINO_REWARDS')
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'CASINO_REWARDS', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', NULL, NULL, 4999.990000, NULL, NULL, 0.010000, 5000.000000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)

IF NOT EXISTS (SELECT * FROM tsn_session_tndr WHERE organization_id = @intOrganization_ID AND tndr_id = 'CLOTHING_VOUCHER' and rtl_loc_id = @intStore_ID)
INSERT INTO tsn_session_tndr (organization_id, rtl_loc_id, tndr_id, session_id, actual_media_count, actual_media_amt, create_date, create_user_id, update_date, update_user_id, record_state)
values(@intOrganization_ID, @intStore_ID, 'CASINO_REWARDS', 0, 0, 0.000000, getDate (), 'BaseData', NULL, NULL, NULL)

--CRI457634 AJB tender
DELETE FROM tnd_tndr WHERE organization_id = @intOrganization_ID and tndr_id = 'TEMP_CREDIT_DEBIT_CARD'
IF NOT EXISTS (SELECT * FROM tnd_tndr WHERE organization_id = @intOrganization_ID AND tndr_id = 'JCB')
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'JCB', '*', '*', 'CREDIT_CARD', 'USD', 'JCB', 'AJB_CREDIT_DEBIT', 0, 1, 1, 0, 1, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'LOCAL_CURRENCY', NULL, 0, 0, 0, NULL, 0, 1, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)

UPDATE tnd_tndr SET auth_mthd_code = 'AJB_CREDIT_DEBIT' WHERE tndr_id = 'AMERICAN_EXPRESS' AND organization_id = @intOrganization_ID
UPDATE tnd_tndr SET auth_mthd_code = 'AJB_CREDIT_DEBIT' WHERE tndr_id = 'MASTERCARD' AND organization_id = @intOrganization_ID
UPDATE tnd_tndr SET auth_mthd_code = 'AJB_CREDIT_DEBIT' WHERE tndr_id = 'DEBIT_CARD' AND organization_id = @intOrganization_ID
UPDATE tnd_tndr SET auth_mthd_code = 'AJB_CREDIT_DEBIT' WHERE tndr_id = 'DINERS_CLUB' AND organization_id = @intOrganization_ID
UPDATE tnd_tndr SET auth_mthd_code = 'AJB_CREDIT_DEBIT' WHERE tndr_id = 'DISCOVER' AND organization_id = @intOrganization_ID
UPDATE tnd_tndr SET auth_mthd_code = 'AJB_CREDIT_DEBIT' WHERE tndr_id = 'VISA' AND organization_id = @intOrganization_ID

UPDATE tnd_tndr SET auth_mthd_code = 'AJB_GIFT_CARD' WHERE tndr_id = 'ISSUE_MERCHANDISE_CREDIT_CARD' AND organization_id = @intOrganization_ID
UPDATE tnd_tndr SET auth_mthd_code = 'AJB_GIFT_CARD' WHERE tndr_id = 'MERCHANDISE_CREDIT_CARD' AND organization_id = @intOrganization_ID

UPDATE tnd_tndr SET non_voidable_flag = 0 WHERE tndr_id = 'AMERICAN_EXPRESS' AND organization_id = @intOrganization_ID
UPDATE tnd_tndr SET non_voidable_flag = 0 WHERE tndr_id = 'MASTERCARD' AND organization_id = @intOrganization_ID
UPDATE tnd_tndr SET non_voidable_flag = 0 WHERE tndr_id = 'DEBIT_CARD' AND organization_id = @intOrganization_ID
UPDATE tnd_tndr SET non_voidable_flag = 0 WHERE tndr_id = 'DINERS_CLUB' AND organization_id = @intOrganization_ID
UPDATE tnd_tndr SET non_voidable_flag = 0 WHERE tndr_id = 'DISCOVER' AND organization_id = @intOrganization_ID
UPDATE tnd_tndr SET non_voidable_flag = 0 WHERE tndr_id = 'VISA' AND organization_id = @intOrganization_ID



DELETE FROM tnd_tndr WHERE organization_id = @intOrganization_ID and tndr_id = 'TEMP_CREDIT_DEBIT_CARD'
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'TEMP_CREDIT_DEBIT_CARD', '*', '*', 'CREDIT_CARD', 'USD', 'Temp Card', 'AJB_CREDIT_DEBIT', 0, 1, 1, 0, 1, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'LOCAL_CURRENCY', NULL, 0, 1, 0, NULL, 0, 1, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)



DELETE FROM tnd_tndr_user_settings WHERE organization_id = @intOrganization_ID and tndr_id in ('TEMP_CREDIT_DEBIT_CARD')
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'TEMP_CREDIT_DEBIT_CARD', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 50.000000, 500.000000, 0.010000, 999999.990000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)

DELETE FROM tnd_tndr WHERE organization_id = @intOrganization_ID and tndr_id IN('GIFT_CARD', 'GIFT_ECARD', 'ISSUE_GIFT_CARD', 'RELOAD_GIFT_CARD','RELOAD_GIFT_ECARD');

INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'GIFT_CARD', '*', '*', 'VOUCHER', 'USD', 'Gift Card', 'AJB_GIFT_CARD', 0, 1, 0, 0, 0, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'ISSUE_GIFT_CARD', NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'ISSUE_GIFT_CARD', '*', '*', 'VOUCHER', 'USD', 'Issue Gift Card', 'AJB_GIFT_CARD', 0, 1, 0, 0, 0, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'LOCAL_CURRENCY', NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'RELOAD_GIFT_CARD', '*', '*', 'VOUCHER', 'USD', 'Reload Gift Card', 'AJB_GIFT_CARD', 0, 1, 0, 0, 0, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'LOCAL_CURRENCY', NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'GIFT_ECARD', '*', '*', 'VOUCHER', 'USD', 'EGift Card', 'AJB_GIFT_CARD', 0, 1, 0, 1, 0, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'ISSUE_GIFT_CARD', NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'RELOAD_GIFT_ECARD', '*', '*', 'VOUCHER', 'USD', 'Reload EGift Card', 'AJB_GIFT_CARD', 0, 1, 0, 1, 0, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'ISSUE_GIFT_CARD', NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)


DELETE FROM tnd_tndr_availability WHERE organization_id = @intOrganization_ID and tndr_id = 'TEMP_CREDIT_DEBIT_CARD'
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'TEMP_CREDIT_DEBIT_CARD', 'ORDER', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'TEMP_CREDIT_DEBIT_CARD', 'SALE', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'TEMP_CREDIT_DEBIT_CARD', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'TEMP_CREDIT_DEBIT_CARD', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');

DELETE FROM tnd_tndr_availability WHERE organization_id = @intOrganization_ID and tndr_id IN('GIFT_CARD', 'GIFT_ECARD', 'ISSUE_GIFT_CARD', 'RELOAD_GIFT_CARD')
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISSUE_GIFT_CARD', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISSUE_GIFT_CARD', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISSUE_GIFT_CARD', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISSUE_GIFT_CARD', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
-- INC0084282 CRU: Return to Gift Card is not an available tender in a Verified Ecomm return - START
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ISSUE_GIFT_CARD', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'RELOAD_GIFT_CARD', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
-- INC0084282 CRU: Return to Gift Card is not an available tender in a Verified Ecomm return - END
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'RELOAD_GIFT_CARD', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'RELOAD_GIFT_CARD', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'RELOAD_GIFT_CARD', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'GIFT_CARD', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'GIFT_CARD', 'SALE', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'GIFT_CARD', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'GIFT_CARD', 'ORDER', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'GIFT_ECARD', 'ORDER', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'GIFT_ECARD', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'GIFT_ECARD', 'SALE', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'GIFT_ECARD', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');

DELETE FROM tnd_tndr_user_settings WHERE organization_id = @intOrganization_ID and tndr_id IN('GIFT_CARD', 'GIFT_ECARD', 'ISSUE_GIFT_CARD', 'RELOAD_GIFT_CARD');
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'GIFT_CARD', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 0.010000, 999999.990000, 0.010000, 500.000000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'GIFT_ECARD', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 0.010000, 999999.990000, 0.010000, 500.000000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'ISSUE_GIFT_CARD', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 0.010000, 999999.990000, 0.010000, 500.000000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'RELOAD_GIFT_CARD', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 0.010000, 999999.990000, 0.010000, 500.000000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)


DELETE FROM tnd_tndr_availability WHERE organization_id = @intOrganization_ID and tndr_id = 'JCB'
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id) VALUES (@intOrganization_ID, 'JCB', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id) VALUES(@intOrganization_ID, 'JCB', 'ORDER', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id) VALUES (@intOrganization_ID, 'JCB', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id) VALUES (@intOrganization_ID, 'JCB', 'SALE', getDate (), 'BaseData')

DELETE FROM itm_item WHERE organization_id = @intOrganization_ID AND item_id IN ('700001', '700002','700003', '700004');
DELETE FROM itm_non_phys_item WHERE organization_id = @intOrganization_ID AND item_id IN ('700001', '700002','700003', '700004');

INSERT INTO itm_item (ORGANIZATION_ID, ITEM_ID, ORG_CODE, ORG_VALUE, NAME, DESCRIPTION, DEPARTMENT_ID, SUBDEPARTMENT_ID, CLASS_ID, SUBCLASS_ID, ITEM_URL, UNIT_COST, CURR_SALE_PRICE, LIST_PRICE, UNIT_OF_MEASURE_CODE, COMPARE_AT_PRICE, MIN_SALE_UNIT_COUNT, MAX_SALE_UNIT_COUNT, ITEM_AVAILABILITY_CODE, DISALLOW_DISCOUNTS_FLAG, PROMPT_FOR_QUANTITY_FLAG, PROMPT_FOR_PRICE_FLAG, PROMPT_FOR_WEIGHT_FLAG, PROMPT_FOR_DESCRIPTION_FLAG, FORCE_QUANTITY_OF_ONE_FLAG, NOT_RETURNABLE_FLAG, NO_GIVEAWAYS_FLAG, ITEM_LVLCODE, PARENT_ITEM_ID, ATTACHED_ITEMS_FLAG, NOT_INVENTORIED_FLAG, SERIALIZED_ITEM_FLAG, ITEM_TYPCODE, SUBSTITUTE_AVAILABLE_FLAG, TAX_GROUP_ID, DTV_CLASS_NAME, MESSAGES_FLAG, VENDOR, SEASON_CODE, PART_NUMBER, QTY_SCALE, RESTOCKING_FEE, SPECIAL_ORDER_LEAD_DAYS, APPLY_RESTOCKING_FEE_FLAG, DISALLOW_SEND_SALE_FLAG, DISALLOW_PRICE_CHANGE_FLAG, DISALLOW_LAYAWAY_FLAG, DISALLOW_SPECIAL_ORDER_FLAG, DISALLOW_WORK_ORDER_FLAG, DISALLOW_REMOTE_SEND_FLAG, DISALLOW_COMMISSION_FLAG, WARRANTY_FLAG, GENERIC_ITEM_FLAG, MIN_AGE_REQUIRED, RESTRICTION_CATEGORY, INITIAL_SALE_QTY, DISPOSITION_CODE, FOODSTAMP_ELIGIBLE_FLAG, STOCK_STATUS, PROMPT_FOR_CUSTOMER, SHIPPING_WEIGHT, DISALLOW_ORDER_FLAG, DISALLOW_DEALS_FLAG, DIMENSION_SYSTEM, pack_size, default_source_type, default_source_id, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, RECORD_STATE)
VALUES (@intOrganization_ID, '700003', '*', '*', 'Issue Gift Card', 'Issue Gift Card', 'NP', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1.0000, 9999.0000, 'AVAILABLE', 1, 0, 1, 0, 0, 1, 1, 0, 'ITEM', NULL, 0, 1, 0, 'NON_PHYSICAL', 0, 'N', 'dtv.xst.dao.itm.impl.NonPhysicalItem', 0, NULL, NULL, NULL, 1, 0.000000, NULL, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO itm_non_phys_item (organization_id, item_id, org_code, org_value, display_order, non_phys_item_typcode, non_phys_item_subtype, exclude_from_net_sales_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, '700003', '*', '*', 10, 'VOUCHER', 'AJB_GIFT_CARD', 1, getDate (), 'BaseData', NULL, NULL, NULL)

INSERT INTO itm_item (ORGANIZATION_ID, ITEM_ID, ORG_CODE, ORG_VALUE, NAME, DESCRIPTION, DEPARTMENT_ID, SUBDEPARTMENT_ID, CLASS_ID, SUBCLASS_ID, ITEM_URL, UNIT_COST, CURR_SALE_PRICE, LIST_PRICE, UNIT_OF_MEASURE_CODE, COMPARE_AT_PRICE, MIN_SALE_UNIT_COUNT, MAX_SALE_UNIT_COUNT, ITEM_AVAILABILITY_CODE, DISALLOW_DISCOUNTS_FLAG, PROMPT_FOR_QUANTITY_FLAG, PROMPT_FOR_PRICE_FLAG, PROMPT_FOR_WEIGHT_FLAG, PROMPT_FOR_DESCRIPTION_FLAG, FORCE_QUANTITY_OF_ONE_FLAG, NOT_RETURNABLE_FLAG, NO_GIVEAWAYS_FLAG, ITEM_LVLCODE, PARENT_ITEM_ID, ATTACHED_ITEMS_FLAG, NOT_INVENTORIED_FLAG, SERIALIZED_ITEM_FLAG, ITEM_TYPCODE, SUBSTITUTE_AVAILABLE_FLAG, TAX_GROUP_ID, DTV_CLASS_NAME, MESSAGES_FLAG, VENDOR, SEASON_CODE, PART_NUMBER, QTY_SCALE, RESTOCKING_FEE, SPECIAL_ORDER_LEAD_DAYS, APPLY_RESTOCKING_FEE_FLAG, DISALLOW_SEND_SALE_FLAG, DISALLOW_PRICE_CHANGE_FLAG, DISALLOW_LAYAWAY_FLAG, DISALLOW_SPECIAL_ORDER_FLAG, DISALLOW_WORK_ORDER_FLAG, DISALLOW_REMOTE_SEND_FLAG, DISALLOW_COMMISSION_FLAG, WARRANTY_FLAG, GENERIC_ITEM_FLAG, MIN_AGE_REQUIRED, RESTRICTION_CATEGORY, INITIAL_SALE_QTY, DISPOSITION_CODE, FOODSTAMP_ELIGIBLE_FLAG, STOCK_STATUS, PROMPT_FOR_CUSTOMER, SHIPPING_WEIGHT, DISALLOW_ORDER_FLAG, DISALLOW_DEALS_FLAG, DIMENSION_SYSTEM, pack_size, default_source_type, default_source_id, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, RECORD_STATE)
VALUES (@intOrganization_ID, '700004', '*', '*', 'Reload Gift Card', 'Reload Gift Card', 'NP', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1.0000, 9999.0000, 'AVAILABLE', 1, 0, 1, 0, 0, 1, 1, 0, 'ITEM', NULL, 0, 1, 0, 'NON_PHYSICAL', 0, 'N', 'dtv.xst.dao.itm.impl.NonPhysicalItem', 0, NULL, NULL, NULL, 1, 0.000000, NULL, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO itm_non_phys_item (organization_id, item_id, org_code, org_value, display_order, non_phys_item_typcode, non_phys_item_subtype, exclude_from_net_sales_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, '700004', '*', '*', 20, 'VOUCHER', 'AJB_GIFT_CARD', 1, getDate (), 'BaseData', NULL, NULL, NULL)


DELETE FROM rpt_organizer WHERE organization_id = @intOrganization_ID AND report_name = 'flash_sales' AND report_group = 'Tenders' AND report_element IN ('GIFT_CARD', 'GIFT_ECARD', 'ISSUE_GIFT_CARD', 'RELOAD_GIFT_CARD');
INSERT INTO rpt_organizer(organization_id,report_name,report_group,report_element,report_order,create_date,create_user_id)
VALUES(@intOrganization_ID, 'flash_sales', 'Tenders', 'GIFT_CARD', 820, getDate(), 'BaseData');
INSERT INTO rpt_organizer(organization_id,report_name,report_group,report_element,report_order,create_date,create_user_id)
VALUES(@intOrganization_ID, 'flash_sales', 'Tenders', 'GIFT_ECARD', 830, getDate(), 'BaseData');
INSERT INTO rpt_organizer(organization_id,report_name,report_group,report_element,report_order,create_date,create_user_id)
VALUES(@intOrganization_ID, 'flash_sales', 'Tenders', 'ISSUE_GIFT_CARD', 840, getDate(), 'BaseData');
INSERT INTO rpt_organizer(organization_id,report_name,report_group,report_element,report_order,create_date,create_user_id)
VALUES(@intOrganization_ID, 'flash_sales', 'Tenders', 'RELOAD_GIFT_CARD', 850, getDate(), 'BaseData');

--INC0062720, INC0062719 CRU - For items 700003 and 700004 the tax_group_id needs to be updated to 89999 from N - START
update itm_item set tax_group_id='89999' where organization_id = @intOrganization_ID AND item_id='700003';
update itm_item set tax_group_id='89999' where organization_id = @intOrganization_ID AND item_id='700004';
--INC0062720, INC0062719 CRU - For items 700003 and 700004 the tax_group_id needs to be updated to 89999 from N - END

-- Keep this at the end of the file.
GO