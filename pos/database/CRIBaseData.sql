PRINT '$Id: CRIBaseData.sql 997 2015-12-17 20:23:04Z dtvdomain\rkasasbeh $';
PRINT '$URL: https://node1000.gspann.local:8443/svn/20181115-cri/branches/fipay_cust_name/pos/database/CRIBaseData.sql $';

-- ************************************************************************************************
--
-- This script should contain the customer's initial data load that will become part of the
-- production database image. This file should not contain any store specific data or test data.
--
-- ************************************************************************************************

DECLARE @intOrganization_ID INT;
SET @intOrganization_ID = $(OrgID);
DECLARE @strCountry_ID VARCHAR(2);
SET @strCountry_ID = $(CountryID);
DECLARE @intStore_ID INT;
SET @intStore_ID = $(StoreID);
DECLARE @strCurrency_ID VARCHAR(3);
SET @strCurrency_ID = $(CurrencyID);

---------------------------------
-- Address types
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'ADDRESS_TYPE';
INSERT INTO com_code_value (organization_id, category, code, org_code, org_value, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'ADDRESS_TYPE', 'HOME', '*', '*', 'Home', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, org_code, org_value, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'ADDRESS_TYPE', 'WORK', '*', '*', 'Work', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, org_code, org_value, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'ADDRESS_TYPE', 'VACATION', '*', '*', 'Vacation', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, org_code, org_value, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'ADDRESS_TYPE', 'OTHER', '*', '*', 'Other', 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Address types
---------------------------------

---------------------------------
-- customer account state
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'CUST_ACCOUNT_STATE';
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'CUST_ACCOUNT_STATE', 'NEW', 'New', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'CUST_ACCOUNT_STATE', 'OPEN', 'Open', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'CUST_ACCOUNT_STATE', 'PENDING', 'Pending', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'CUST_ACCOUNT_STATE', 'IN_PROGRESS', 'In Progress', 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'CUST_ACCOUNT_STATE', 'READY_TO_PICKUP', 'Ready For Pickup', 50, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'CUST_ACCOUNT_STATE', 'CLOSED', 'Closed', 60, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'CUST_ACCOUNT_STATE', 'REFUNDABLE', 'Refundable', 70, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'CUST_ACCOUNT_STATE', 'OVERDUE', 'Overdue', 80, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'CUST_ACCOUNT_STATE', 'DELINQUENT', 'Delinquent', 90, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'CUST_ACCOUNT_STATE', 'ABANDONED', 'Abandoned', 100, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'CUST_ACCOUNT_STATE', 'INACTIVE', 'Inactive', 110, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- customer account state
---------------------------------
---------------------------------
-- customer contact pref
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'CUSTOMER_CONTACT_PREF';
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'CUSTOMER_CONTACT_PREF', 'HOME', 'Home', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'CUSTOMER_CONTACT_PREF', 'WORK', 'Work', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'CUSTOMER_CONTACT_PREF', 'MOBILE', 'Mobile', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'CUSTOMER_CONTACT_PREF', 'FAX', 'Fax', 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)

INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'CUSTOMER_CONTACT_PREF', 'EMAIL', 'Email', 50, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- customer contact prefs
---------------------------------
---------------------------------
-- customer groups
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'CUSTOMER_GROUPS';
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'CUSTOMER_GROUPS', 'DEFAULT', 'Customer', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'CUSTOMER_GROUPS', 'EMPLOYEE', 'Employee', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'CUSTOMER_GROUPS', 'SILVER', 'Silver Level', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'CUSTOMER_GROUPS', 'GOLD', 'Gold Level', 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'CUSTOMER_GROUPS', 'COMP1', 'Company 1', 50, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'CUSTOMER_GROUPS', 'COMP2', 'Company 2', 60, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- customer groups
---------------------------------
---------------------------------------
-- Employee Department Codes
---------------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'EMPLOYEE_DEPARTMENT';
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_DEPARTMENT', 'ED01', 'Sales', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_DEPARTMENT', 'ED02', 'Support', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_DEPARTMENT', 'ED03', 'Management', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------------
-- Employee Department Codes
---------------------------------------
---------------------------------
-- Employee Group Codes
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'EMPLOYEE_GROUP';
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'EMPLOYEE_GROUP', 'DEFAULT', 'Default', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--
-- OR
--
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_GROUP', 'FRONT_ROOM', 'Front Room', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_GROUP', 'BACK_ROOM', 'Back Room', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_GROUP', 'TOP_EMPLOYEES', 'Top Employees', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_GROUP', 'OPENING_STAFF', 'Opening Staff', 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_GROUP', 'CLOSING_STAFF', 'Closing Staff', 50, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Employee Group Codes
---------------------------------
-------------------------------------------
-- Employee Role (Position) Codes
-------------------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'EMPLOYEE_ROLE';
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_ROLE', 'ER01', 'Sale Associate', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_ROLE', 'ER02', 'Key Holder', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_ROLE', 'ER03', 'Counter Supervisor', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_ROLE', 'ER04', 'Assistant Store Manager', 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_ROLE', 'ER05', 'Store Manager', 50, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_ROLE', 'ER06', 'District Manager', 60, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_ROLE', 'ER07', 'Regional Director', 70, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_ROLE', 'ER08', 'Help Desk', 80, getDate (), 'BaseData', NULL, NULL, NULL, 0)
-------------------------------------------
-- Employee Role (Position) Codes
-------------------------------------------
---------------------------------
-- Employee Status Codes
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'EMPLOYEE_STATUS';
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_STATUS', 'A', 'Active', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_STATUS', 'I', 'Inactive', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_STATUS', 'T', 'Terminated', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Employee Status Codes
---------------------------------
---------------------------------
-- Employee Task Codes
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'EMPLOYEE_TASK_TYPE';
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_TASK_TYPE', 'GENERAL', 'General', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_TASK_TYPE', 'MAILING', 'Mailing', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_TASK_TYPE', 'DISPLAY', 'Display', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_TASK_TYPE', 'HOUSEKEEPING', 'Housekeeping', 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_TASK_TYPE', 'RECEIVING', 'Receiving', 50, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_TASK_TYPE', 'SHIPPING', 'Shipping', 60, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_TASK_TYPE', 'COUNT', 'Count', 70, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Employee Task Codes
---------------------------------
---------------------------------
-- Employee Type Codes
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'EMPLOYEE_TYPE';
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_TYPE', 'EVERYONE', 'Default', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'EMPLOYEE_TYPE', 'SALES', 'Sales', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Employee Type Codes
---------------------------------
---------------------------------
-- Gender Codes
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'GENDER';
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'GENDER', 'M', 'Male', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'GENDER', 'F', 'Female', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Gender Codes
---------------------------------
---------------------------------------------------
-- Insurance Plan Codes (Private Credit)
---------------------------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'INSURANCE_PLAN';
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'INSURANCE_PLAN', 'P', 'Platinum', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'INSURANCE_PLAN', 'G', 'Gold', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'INSURANCE_PLAN', 'S', 'Silver', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------------------------
-- Insurance Plan Codes (Private Credit)
---------------------------------------------------
--------------------------------------
-- Inventory Bucket Tracking
--------------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'INV_BUCKET_TRACKING_METHOD';
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INV_BUCKET_TRACKING_METHOD', 'ALL', 'All', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INV_BUCKET_TRACKING_METHOD', 'SERIALIZED', 'Serialized', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INV_BUCKET_TRACKING_METHOD', 'NONE', 'None', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--------------------------------------
-- Inventory Bucket Tracking
--------------------------------------
---------------------------------
-- Inventory Count Type
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'INV_CTL_DOC_COUNT_SUBTYPE';
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INV_CTL_DOC_COUNT_SUBTYPE', 'LOCATION_BASED', 'Location Based', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INV_CTL_DOC_COUNT_SUBTYPE', 'ITEM_BASED', 'Item Based', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INV_CTL_DOC_COUNT_SUBTYPE', 'INIT_LOC', 'Initialize Location', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INV_CTL_DOC_COUNT_SUBTYPE', 'CYCLE_COUNT', 'Cycle Count', 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INV_CTL_DOC_COUNT_SUBTYPE', 'PHYSICAL_COUNT', 'Physical Count', 50, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INV_CTL_DOC_COUNT_SUBTYPE', 'SUPPLY_COUNT', 'Supply Count', 60, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Inventory Count Type
---------------------------------
---------------------------------
-- Inventory Action Codes
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'INVENTORY_ACTION_CODES';
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_ACTION_CODES', 'ITEM_SALE', 'Sale Item', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_ACTION_CODES', 'ITEM_RETURN', 'Return Item', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_ACTION_CODES', 'ITEM_TRANSFER', 'Transfer Item', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_ACTION_CODES', 'ITEM_LAYAWAY', 'Layaway Item', 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_ACTION_CODES', 'RECEIVING', 'Receiving', 50, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_ACTION_CODES', 'SHIPPING', 'Shipping', 60, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_ACTION_CODES', 'INVENTORY_ADJUSTMENT', 'Inventory Adjustment', 70, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_ACTION_CODES', 'WORK_ORDER_ITEMS_ADJUSTMENT', 'Work order adjustment', 80, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_ACTION_CODES', 'WORK_ORDER_PICKUP', 'Work order pickup', 90, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_ACTION_CODES', 'LAYAWAY_CANCEL', 'Cancelled Layaway', 100, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_ACTION_CODES', 'LAYAWAY_PICKUP', 'Layaway pickup', 110, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_ACTION_CODES', 'SPECIAL_ORDER_PICKUP', 'Special order pickup', 120, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_ACTION_CODES', 'RECONCILE_ITEM_SALE', 'Reconcile item sale', 130, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_ACTION_CODES', 'RECONCILE_SHIPPING', 'Reconcile shipping', 140, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_ACTION_CODES', 'CYCLE_COUNT_ADJUSTMENT', 'Cycle Count Adjustment', 150, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_ACTION_CODES', 'PHYSICAL_COUNT_ADJUSTMENT', 'Physical Count Adjustment', 160, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_ACTION_CODES', 'SUPPLY_COUNT_ADJUSTMENT', 'Supply Count Adjustment', 170, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Inventory Action Codes
---------------------------------
---------------------------------
-- item locator distances
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'INVENTORY_LOCATOR_DISTANCES';
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_LOCATOR_DISTANCES', '5', '5 MILES/KILOMETERS', 5, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_LOCATOR_DISTANCES', '10', '10 MILES/KILOMETERS', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_LOCATOR_DISTANCES', '15', '15 MILES/KILOMETERS', 15, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_LOCATOR_DISTANCES', '20', '20 MILES/KILOMETERS', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_LOCATOR_DISTANCES', '25', '25 MILES/KILOMETERS', 25, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_LOCATOR_DISTANCES', '30', '30 MILES/KILOMETERS', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_LOCATOR_DISTANCES', '40', '40 MILES/KILOMETERS', 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_LOCATOR_DISTANCES', '50', '50 MILES/KILOMETERS', 50, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_LOCATOR_DISTANCES', '60', '60 MILES/KILOMETERS', 60, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_LOCATOR_DISTANCES', '70', '70 MILES/KILOMETERS', 70, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_LOCATOR_DISTANCES', '80', '80 MILES/KILOMETERS', 80, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_LOCATOR_DISTANCES', '90', '90 MILES/KILOMETERS', 90, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_LOCATOR_DISTANCES', '100', '100 MILES/KILOMETERS', 100, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_LOCATOR_DISTANCES', '150', '150 MILES/KILOMETERS', 150, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_LOCATOR_DISTANCES', '200', '200 MILES/KILOMETERS', 200, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_LOCATOR_DISTANCES', '300', '300 MILES/KILOMETERS', 300, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_LOCATOR_DISTANCES', '500', '500 MILES/KILOMETERS', 500, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- item locator distances
---------------------------------
---------------------------------
-- Item Group Codes
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'ITEM_GROUP';
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'ITEM_GROUP', 'FL', 'Fluids', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'ITEM_GROUP', 'FR', 'Flares', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'ITEM_GROUP', 'GP', 'Gasket Repair', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Item Group Codes
---------------------------------
---------------------------------
-- Marital Status Codes
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'MARITAL_STATUS';
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'MARITAL_STATUS', 'M', 'Married', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'MARITAL_STATUS', 'S', 'Single', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Marital Status Codes
---------------------------------
-----------------------------------------------------------
-- Misc Invoice (Work Orders) Account Codes
-----------------------------------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'MISC_INVOICE_ACCOUNTS';
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'MISC_INVOICE_ACCOUNTS', 'MIA01 - Shipping', 'Shipping', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'MISC_INVOICE_ACCOUNTS', 'MIA02 - Expediting', 'Expediting', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0);
-----------------------------------------------------------
-- Misc Invoice (Work Orders) Account Codes
-----------------------------------------------------------
---------------------------------
-- Module Name Codes
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'MODULE_NAME';
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'MODULE_NAME', 'MN01', 'Item Entry', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'MODULE_NAME', 'MN02', 'Non Nerchandise', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'MODULE_NAME', 'MN03', 'Tendering', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'MODULE_NAME', 'MN04', 'Shipping', 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'MODULE_NAME', 'MN05', 'Receiving', 50, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'MODULE_NAME', 'MN06', 'Store Open / Close', 60, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'MODULE_NAME', 'MN07', 'Register Open / Close', 70, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Module Name Codes
---------------------------------
------------
-- Opera
------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'OPERA';
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'OPERA', 'inUser', 'Pj4+MAAAAAAe8/7IE9uiPuVMPEfCj7eD', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'OPERA', 'inPsw', 'Pj4+MAAAAAAe8/7IE9uiPuVMPEfCj7eD', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
------------
-- Opera
------------
---------------------------------
-- organization types
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'ORGANIZATION_TYPE';
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'ORGANIZATION_TYPE', 'OT01', 'HOW - House of Worship', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'ORGANIZATION_TYPE', 'OT02', 'School', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'ORGANIZATION_TYPE', 'OT03', 'Band', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'ORGANIZATION_TYPE', 'OT04', 'Club/Restaurant', 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'ORGANIZATION_TYPE', 'OT05', 'Studio', 50, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'ORGANIZATION_TYPE', 'OT06', 'Business', 60, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'ORGANIZATION_TYPE', 'OT07', 'Other', 70, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- organization types
---------------------------------
---------------------------------
-- Pay Status Codes
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'PAY_STATUS';
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'PAY_STATUS', 'H', 'Hourly', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'PAY_STATUS', 'S', 'Salaried', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Pay Status Codes
---------------------------------
------------------------------------------------
-- Private Credit Account Type Codes
------------------------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'PRIVATE_CREDIT_ACCOUNT_TYPE';
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'PRIVATE_CREDIT_ACCOUNT_TYPE', 'STD', 'Standard Account', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'PRIVATE_CREDIT_ACCOUNT_TYPE', 'EXT', 'Extended Payment Plan', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'PRIVATE_CREDIT_ACCOUNT_TYPE', 'DEF', 'Deferred Billing Plan', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
------------------------------------------------
-- Private Credit Account Type Codes
------------------------------------------------
------------------------------------------------
-- Private Credit Primary ID Codes
------------------------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'PRIVATE_CREDIT_PRIMARY_ID_TYPE';
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'PRIVATE_CREDIT_PRIMARY_ID_TYPE', 'DL', 'Drivers License', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'PRIVATE_CREDIT_PRIMARY_ID_TYPE', 'PS', 'Passport', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'PRIVATE_CREDIT_PRIMARY_ID_TYPE', 'SS', 'Social Security Card', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
------------------------------------------------
-- Private Credit Primary ID Codes
------------------------------------------------
------------------------------------------------
-- Private Credit Secondary ID Codes
------------------------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'PRIVATE_CREDIT_SECOND_ID_TYPE';
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'PRIVATE_CREDIT_SECOND_ID_TYPE', 'AX', 'American Express', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'PRIVATE_CREDIT_SECOND_ID_TYPE', 'DS', 'Discover', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'PRIVATE_CREDIT_SECOND_ID_TYPE', 'MC', 'Mastercard', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'PRIVATE_CREDIT_SECOND_ID_TYPE', 'VS', 'VISA', 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
------------------------------------------------
-- Private Credit Secondary ID Codes
------------------------------------------------
---------------------------------
-- Salutation Codes
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'SALUTATION';
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'SALUTATION', 'MR', 'Mr.', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'SALUTATION', 'MRS', 'Mrs.', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'SALUTATION', 'MS', 'Ms.', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'SALUTATION', 'DR', 'Dr.', 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Salutation Codes
---------------------------------
---------------------------------
-- Shopper Status Codes
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'SHOPPER_STATUS';
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'SHOPPER_STATUS', 'MALE', 'Male', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'SHOPPER_STATUS', 'FEMALE', 'Female', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'SHOPPER_STATUS', 'FAMILY', 'Family Group', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Shopper Status Codes
---------------------------------
---------------------------------
-- Telephone Types
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'TELEPHONE_TYPE';
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'TELEPHONE_TYPE', 'TEL1', 'Telephone 1', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'TELEPHONE_TYPE', 'TEL2', 'Telephone 2', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'TELEPHONE_TYPE', 'TEL3', 'Telephone 3', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'TELEPHONE_TYPE', 'TEL4', 'Telephone 4', 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Telephone Types
---------------------------------
-----------------------------------------
-- Work Order Contact Methods
-----------------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'WORK_ORDER_CONTACT_METHODS';
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'WORK_ORDER_CONTACT_METHODS', 'WOCM1', 'Cell Phone', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'WORK_ORDER_CONTACT_METHODS', 'WOCM2', 'Home Phone', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'WORK_ORDER_CONTACT_METHODS', 'WOCM3', 'Work Phone', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'WORK_ORDER_CONTACT_METHODS', 'WOCM4', 'Email', 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
-----------------------------------------
-- Work Order Contact Methods
-----------------------------------------
---------------------------------
-- Work Order Priorities
---------------------------------
DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'WORK_ORDER_PRIORITIES';
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'WORK_ORDER_PRIORITIES', 'WOP1', 'Normal', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'WORK_ORDER_PRIORITIES', 'WOP2', 'Priority', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'WORK_ORDER_PRIORITIES', 'WOP3', 'Urgent', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Work Order Priorities
---------------------------------

---------------------------------
--  Lost Bag Reasons
---------------------------------
DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'BAG_LOST';
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'BAG_LOST', 'LOST1', 'Missing - Contact RLPM and District Manager immediately', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0);
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'BAG_LOST', 'LOST2', 'Conveyance Issue - Contact District Manager immediately', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, getDate (), 'BaseData', NULL, NULL, NULL, 0);
---------------------------------
--  Lost Bag Reasons
---------------------------------

---------------------------------
--  Change Float Reasons
---------------------------------
DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'CHANGE_FLOAT';
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'CHANGE_FLOAT', 'CF01', 'Corporate Approved Seasonal Increase', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'CHANGE_FLOAT', 'CF02', 'Corporate Approved Seasonal Decrease', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
--  Change Float Reasons
---------------------------------
---------------------------------
-- Discount Reasons
---------------------------------
DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'DISCOUNT';
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'DISCOUNT', 'DC1', 'Incorrect Label', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'DISCOUNT', 'DC2', 'Manager Discretion', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'DISCOUNT', 'DC3', 'Price Guarantee', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'DISCOUNT', 'DC4', 'Damage Adjustment', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Discount Reasons
---------------------------------
---------------------------------
--  Inventory Adjustment Reasons
---------------------------------
--
-- set inv_action_code to either 'SUBTRACT' or 'ADD'
--
DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'INVENTORY_ADJUSTMENT';
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_ADJUSTMENT', 'IA01', 'Donation', NULL, NULL, NULL, NULL, NULL, NULL, 'SUBTRACT', NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_ADJUSTMENT', 'IA02', 'Theft', NULL, NULL, NULL, NULL, NULL, NULL, 'SUBTRACT', NULL, NULL, 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'INVENTORY_ADJUSTMENT', 'IA03', 'Found Merchandise', NULL, NULL, NULL, NULL, NULL, NULL, 'ADD', NULL, NULL, 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
--  Inventory Adjustment Reasons
---------------------------------
---------------------------------
--  Item Transfer Reasons
---------------------------------
DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'ITEM_TRANSFER';
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'ITEM_TRANSFER', 'IT01', 'Item Transfer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'ITEM_TRANSFER', 'IT02', 'Item Moved', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'ITEM_TRANSFER', 'IT03', 'Item Shipped', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
--  Item Transfer Reasons
---------------------------------
---------------------------------
--  No Sale Reasons
---------------------------------
DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'NO_SALE';
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'NO_SALE', 'DEFAULT', 'Default', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--
-- OR
--
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'NO_SALE', 'NS01', 'Change From Another Register', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'NO_SALE', 'NS02', 'Wrong Change For Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'NO_SALE', 'NS03', 'Drawer Audit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
--  No Sale Reasons
---------------------------------
---------------------------------
-- Paid In/Paid Out Reasons
---------------------------------
DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'PAID_IN';
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'PAID_IN', 'PI01', 'Replace change from Contests', NULL, NULL, 0.010000, 250.000000, 'NONE', NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'PAID_IN', 'PI02', 'Replace change from Supplies', NULL, NULL, 0.010000, 250.000000, 'NONE', NULL, NULL, NULL, NULL, 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'PAID_IN', 'PI03', 'Other', NULL, NULL, 0.010000, 250.000000, 'NONE', NULL, NULL, NULL, NULL, 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'PAID_IN', 'PI04', 'Add funds to balance register float', NULL, NULL, 0.010000, 250.000000, 'NONE', NULL, NULL, NULL, NULL, 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'PAID_IN', 'PI05', 'Found Money', NULL, NULL, 0.010000, 250.000000, 'NONE', NULL, NULL, NULL, NULL, 50, getDate (), 'BaseData', NULL, NULL, NULL, 0)

DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'PAID_OUT';
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'PAID_OUT', 'PO01', 'Contests', NULL, NULL, 0.010000, 250.000000, 'NONE', NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'PAID_OUT', 'PO02', 'Supplies', NULL, NULL, 0.010000, 250.000000, 'NONE', NULL, NULL, NULL, NULL, 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'PAID_OUT', 'PO03', 'Other', NULL, NULL, 0.010000, 250.000000, 'REQUIRED', NULL, NULL, NULL, NULL, 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'PAID_OUT', 'PO04', 'Remove funds to balance register float', NULL, NULL, 0.010000, 250.000000, 'NONE', NULL, NULL, NULL, NULL, 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'PAID_OUT', 'PO05', 'Claimed Money', NULL, NULL, 0.010000, 250.000000, 'NONE', NULL, NULL, NULL, NULL, 50, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Paid In/Paid Out Reasons
---------------------------------
---------------------------------
--  Post Void Reasons
---------------------------------
DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'POST_VOID';
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'POST_VOID', 'DEFAULT', 'Default Post Void', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--
-- OR
--
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'POST_VOID', 'PV01', 'Same Day Check Return', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'POST_VOID', 'PV02', 'Change Tender', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
--  Post Void Reasons
---------------------------------
---------------------------------
--  Price Change Reasons
---------------------------------
DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'PRICE_CHANGE';
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'PRICE_CHANGE', 'DEFAULT', 'Default Price Change', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--
-- OR
--
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'PRICE_CHANGE', 'PC01', 'Promo Override', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'PRICE_CHANGE', 'PC02', 'Markdown', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
--  Price Change Reasons
---------------------------------

---------------------------------
--  Replenishment
---------------------------------
DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'REPLENISHMENT_HEADER';
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, org_code, org_value, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'REPLENISHMENT_HEADER', 'NEXT_DAY_DELIVERY', '*', '*', 'Next Day Delivery', NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, org_code, org_value, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'REPLENISHMENT_HEADER', 'SECOND_DAY_DELIVERY', '*', '*', 'Second Day Delivery', NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, org_code, org_value, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'REPLENISHMENT_HEADER', 'COMMENT', '*', '*', 'Free Form-Enter Comments', NULL, NULL, NULL, NULL, 'REQUIRED', NULL, NULL, NULL, NULL, 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)

DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'REPLENISHMENT_ITEM';
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, org_code, org_value, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'REPLENISHMENT_ITEM', 'COMMENT', '*', '*', 'Free Form - Enter Comments', NULL, NULL, NULL, NULL, 'REQUIRED', NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
--  Replenishment
---------------------------------

---------------------------------
--  Return Reasons
---------------------------------
DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'RETURN';
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'RETURN', 'RT01', 'Changed Mind', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, 'DEFAULT', 'ON_HAND', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'RETURN', 'RT02', 'Damaged', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, 'DEFAULT', 'ON_HAND', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'RETURN', 'RT03', 'Wrong Size', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, 'DEFAULT', 'ON_HAND', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'RETURN', 'RT04', 'Wrong Color', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, 'DEFAULT', 'ON_HAND', 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'RETURN', 'RT05', 'Gift', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, 'DEFAULT', 'ON_HAND', 50, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'RETURN', 'RT06', 'Price Adjustment', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, 'DEFAULT', 'ON_HAND', 60, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'RETURN', 'RT07', 'Online Return', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, 'DEFAULT', 'ON_HAND', 70, getDate (), 'BaseData', NULL, NULL, NULL, 0)


--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'RETURN', 'DEFAULT', 'Default Return Reason', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, 'DEFAULT', 'ON_HAND', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--VALUES (@intOrganization_ID, 'RETURN', 'RT01', 'Online Purchase', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, 'DEFAULT', 'ON_HAND', 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'RETURN', 'RT11', 'Quality Issue', NULL, NULL, NULL, NULL, 'REQUIRED', NULL, NULL, 'DEFAULT', 'ON_HAND', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--VALUES (@intOrganization_ID, 'RETURN', 'RT11', 'Quality Issue', NULL, NULL, NULL, NULL, 'REQUIRED', NULL, NULL, 'DEFAULT', 'DAMAGED', 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'RETURN', 'RT12', 'Damaged', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, 'DEFAULT', 'ON_HAND', 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'RETURN', 'RT13', 'Wrong Size', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, 'DEFAULT', 'ON_HAND', 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'RETURN', 'RT02', 'Size too Small / Short', 'RT01', NULL, NULL, NULL, 'NONE', NULL, NULL, 'DEFAULT', 'ON_HAND', 100, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'RETURN', 'RT03', 'Size too Large / Long', 'RT01', NULL, NULL, NULL, 'NONE', NULL, NULL, 'DEFAULT', 'ON_HAND', 110, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'RETURN', 'RT04', 'Style', 'RT01', NULL, NULL, NULL, 'NONE', NULL, NULL, 'DEFAULT', 'ON_HAND', 120, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'RETURN', 'RT05', 'Not as Shown', 'RT01', NULL, NULL, NULL, 'REQUIRED', NULL, NULL, 'DEFAULT', 'ON_HAND', 130, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'RETURN', 'RT06', 'Gift Refund', 'RT01', NULL, NULL, NULL, 'NONE', NULL, NULL, 'DEFAULT', 'ON_HAND', 140, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'RETURN', 'RT07', 'Arrived Late', 'RT01', NULL, NULL, NULL, 'NONE', NULL, NULL, 'DEFAULT', 'ON_HAND', 150, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'RETURN', 'RT08', 'Wrong Item Sent', 'RT01', NULL, NULL, NULL, 'NONE', NULL, NULL, 'DEFAULT', 'ON_HAND', 160, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'RETURN', 'RT09', 'Extra Item Sent', 'RT01', NULL, NULL, NULL, 'NONE', NULL, NULL, 'DEFAULT', 'ON_HAND', 170, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'RETURN', 'RT10', 'Defective', 'RT01', NULL, NULL, NULL, 'REQUIRED', NULL, NULL, 'DEFAULT', 'ON_HAND', 180, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--VALUES (@intOrganization_ID, 'RETURN', 'RT10', 'Defective', 'RT01', NULL, NULL, NULL, 'REQUIRED', NULL, NULL, 'DEFAULT', 'DAMAGED', 180, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
--  Return Reasons
---------------------------------
---------------------------------
-- Tax Change Reasons
---------------------------------
DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'TAX_CHANGE';
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'TAX_CHANGE', 'INTX', 'Incorrect Tax', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TAX_CHANGE', 'T2', 'Sales Tax Holiday', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, NULL, NULL, 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TAX_CHANGE', 'T3', 'Tax Location Not on File', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, NULL, NULL, 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TAX_CHANGE', 'T4', 'Other Unlisted Reason', NULL, NULL, NULL, NULL, 'REQUIRED', NULL, NULL, NULL, NULL, 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Tax Change Reasons
---------------------------------
---------------------------------
-- Tax Exempt Reasons
---------------------------------
DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'TAX_EXEMPT';
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'TAX_EXEMPT', 'TE1', 'Texas Tax Exempt', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
VALUES (@intOrganization_ID, 'TAX_EXEMPT', 'TE2', 'Washington Tax Exempt', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, NULL, NULL, 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TAX_EXEMPT', 'TE3', 'Resale', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TAX_EXEMPT', 'TE4', 'School', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TAX_EXEMPT', 'TE5', 'Church', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TAX_EXEMPT', 'TE6', 'Charity', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TAX_EXEMPT', 'TE7', 'Government', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 70, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TAX_EXEMPT', 'TE8', 'Post Production', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 80, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TAX_EXEMPT', 'TE9', 'Diplomat', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 90, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TAX_EXEMPT', 'TE10', 'Neighbor State', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 100, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TAX_EXEMPT', 'TE11', 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 110, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Tax Exempt Reasons
---------------------------------
---------------------------------
-- Till Count Discrepancy Reasons
---------------------------------
DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'TILL_COUNT_DISCREPANCY';
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TILL_COUNT_DISCREPANCY', 'DEFAULT', 'Default', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--
-- OR
--
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TILL_COUNT_DISCREPANCY', 'TCD1', 'Till Count Discrepancy Reason 1', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TILL_COUNT_DISCREPANCY', 'TCD2', 'Till Count Discrepancy Reason 2', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, NULL, NULL, 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TILL_COUNT_DISCREPANCY', 'TCD3', 'Till Count Discrepancy Reason 3', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, NULL, NULL, 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TILL_COUNT_DISCREPANCY', 'TCD4', 'Till Count Discrepancy Reason 4', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, NULL, NULL, 40, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Till Count Discrepancy Reasons
---------------------------------
---------------------------------
-- Time Off Reasons
---------------------------------
DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'TIME_OFF_REAS_CODE';
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TIME_OFF_REAS_CODE', 'TO1', 'Medical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TIME_OFF_REAS_CODE', 'TO2', 'Jury Duty', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TIME_OFF_REAS_CODE', 'TO3', 'Personal Time Off', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Time Off Reasons
---------------------------------
---------------------------------
-- Transaction Cancel Reasons
---------------------------------
DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'TRANSACTION_CANCEL';
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TRANSACTION_CANCEL', 'TC01', 'Changed Mind', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'TRANSACTION_CANCEL', 'TC02', 'Error Correction', NULL, NULL, NULL, NULL, 'NONE', NULL, NULL, NULL, NULL, 20, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Transaction Cancel Reasons
---------------------------------
---------------------------------
-- Void Line Item Reasons
---------------------------------
DELETE FROM com_reason_code WHERE organization_id = @intOrganization_ID AND reason_typcode = 'VOID_LINE_ITEM';
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'VOID_LINE_ITEM', 'VLI01', 'Changed Mind', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
--INSERT INTO com_reason_code (organization_id, reason_typcode, reason_code, description, parent_code, gl_acct_nbr, minimum_amt, maximum_amt, comment_req, cust_msg, inv_action_code, location_id, bucket_id, sort_order, create_date, create_user_id, update_date, update_user_id, record_state, hidden_flag)
--VALUES (@intOrganization_ID, 'VOID_LINE_ITEM', 'VLI02', 'Error Correction', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL, 0)
---------------------------------
-- Void Line Item Reasons
---------------------------------



----------------------
-- Shipper Codes
----------------------
DELETE FROM inv_shipper WHERE organization_id = @intOrganization_ID;
--INSERT INTO inv_shipper (organization_id, shipper_id, org_code, org_value, shipper_desc, display_order, tracking_number_flag, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'CRT', '*', '*', 'Charlotte Russe Transport', 10, 1, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO inv_shipper (organization_id, shipper_id, org_code, org_value, shipper_desc, display_order, tracking_number_flag, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'FEDEX', '*', '*', 'FedEx', 20, 1, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO inv_shipper (organization_id, shipper_id, org_code, org_value, shipper_desc, display_order, tracking_number_flag, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'CITY', '*', '*', 'City', 30, 1, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO inv_shipper (organization_id, shipper_id, org_code, org_value, shipper_desc, display_order, tracking_number_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'UPS1', '*', '*', 'UPS1', 40, 1, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO inv_shipper (organization_id, shipper_id, org_code, org_value, shipper_desc, display_order, tracking_number_flag, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'STVN', '*', '*', 'Stevens', 50, 1, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO inv_shipper (organization_id, shipper_id, org_code, org_value, shipper_desc, display_order, tracking_number_flag, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'STORETRUCK', '*', '*', 'Store Truck', 60, 1, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO inv_shipper (organization_id, shipper_id, org_code, org_value, shipper_desc, display_order, tracking_number_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DISCARD', '*', '*', 'Discard At Store', 70, 1, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO inv_shipper (organization_id, shipper_id, org_code, org_value, shipper_desc, display_order, tracking_number_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'HAND', '*', '*', 'Hand Carry', 80, 1, getDate (), 'BaseData', NULL, NULL, NULL)
----------------------
-- Shipper Codes
----------------------
---------------------------------
-- Shipping Codes
---------------------------------
DELETE FROM inv_shipper_method WHERE organization_id = @intOrganization_ID;
--INSERT INTO inv_shipper_method (organization_id, shipper_method_id, org_code, org_value, shipper_method_desc, shipper_id, domestic_service_code, intl_service_code, display_order, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'USPS_PARCL', '*', '*', 'USPS Parcel Post', 'USPS', NULL, NULL, 10, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO inv_shipper_method (organization_id, shipper_method_id, org_code, org_value, shipper_method_desc, shipper_id, domestic_service_code, intl_service_code, display_order, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'USPS_PRIOR', '*', '*', 'USPS Priority Mail', 'USPS', NULL, NULL, 20, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO inv_shipper_method (organization_id, shipper_method_id, org_code, org_value, shipper_method_desc, shipper_id, domestic_service_code, intl_service_code, display_order, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'USPS_XPRES', '*', '*', 'USPS Express Mail', 'USPS', NULL, NULL, 30, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO inv_shipper_method (organization_id, shipper_method_id, org_code, org_value, shipper_method_desc, shipper_id, domestic_service_code, intl_service_code, display_order, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'UPS_1DAY', '*', '*', 'UPS Next Day Air', 'UPS', '01', '08', 40, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO inv_shipper_method (organization_id, shipper_method_id, org_code, org_value, shipper_method_desc, shipper_id, domestic_service_code, intl_service_code, display_order, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'UPS_2DAY', '*', '*', 'UPS Second Day Air', 'UPS', '02', '07', 50, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO inv_shipper_method (organization_id, shipper_method_id, org_code, org_value, shipper_method_desc, shipper_id, domestic_service_code, intl_service_code, display_order, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'UPS_GROUND', '*', '*', 'UPS Ground', 'UPS', '03', '11', 60, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO inv_shipper_method (organization_id, shipper_method_id, org_code, org_value, shipper_method_desc, shipper_id, domestic_service_code, intl_service_code, display_order, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'STORETRUCK', '*', '*', 'Store Truck', 'STORETRUCK', NULL, NULL, 70, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO inv_shipper_method (organization_id, shipper_method_id, org_code, org_value, shipper_method_desc, shipper_id, domestic_service_code, intl_service_code, display_order, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'FDX_1DAY', '*', '*', 'FedEx Next Day Air', 'FEDEX', 'FIRST_OVERNIGHT', 'INTERNATIONAL_PRIORITY', 80, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO inv_shipper_method (organization_id, shipper_method_id, org_code, org_value, shipper_method_desc, shipper_id, domestic_service_code, intl_service_code, display_order, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'FDX_2DAY', '*', '*', 'FedEx Second Day Air', 'FEDEX', 'FEDEX_2_DAY', 'INTERNATIONAL_ECONOMY', 90, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO inv_shipper_method (organization_id, shipper_method_id, org_code, org_value, shipper_method_desc, shipper_id, domestic_service_code, intl_service_code, display_order, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'FDX_GROUND', '*', '*', 'FedEx Ground', 'FEDEX', 'GROUND_HOME_DELIVERY', 'FEDEX_GROUND', 100, getDate (), 'BaseData', NULL, NULL, NULL)
---------------------------------
-- Shipping Codes
---------------------------------

-----------------------------
-- Transaction Prompts
-----------------------------
DELETE FROM com_trans_prompt_properties WHERE organization_id = @intOrganization_ID;
--INSERT INTO com_trans_prompt_properties (organization_id, property_code, effective_date, org_code, org_value, expiration_date, code_category, prompt_title_key, prompt_msg_key, required_flag, sort_order, prompt_mthd_code, prompt_edit_pattern, validation_rule_key, transaction_state, prompt_key, chain_key, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'PHONE_NUMBER', getDate () - 365, '*', '*', getDate () + 365, NULL, NULL, NULL, 0, 10, 'MASK', '###-###-####', 'PHONE_NUMBER', 'PRE_TENDER', NULL, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO com_trans_prompt_properties (organization_id, property_code, effective_date, org_code, org_value, expiration_date, code_category, prompt_title_key, prompt_msg_key, required_flag, sort_order, prompt_mthd_code, prompt_edit_pattern, validation_rule_key, transaction_state, prompt_key, chain_key, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'POSTAL_CODE1', getDate () - 365, '*', '*', getDate () + 365, NULL, '_EnterPostalCodeTitle', '_EnterPostalCodeMessage', 0, 10, 'SIMPLE', NULL, NULL, 'PRE_SALE', NULL, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO com_trans_prompt_properties (organization_id, property_code, effective_date, org_code, org_value, expiration_date, code_category, prompt_title_key, prompt_msg_key, required_flag, sort_order, prompt_mthd_code, prompt_edit_pattern, validation_rule_key, transaction_state, prompt_key, chain_key, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'POSTAL_CODE2', getDate () - 365, '*', '*', getDate () + 365, NULL, '_EnterPostalCodeTitle', '_EnterPostalCodeMessage', 0, 10, 'SIMPLE', NULL, NULL, 'PRE_TENDER', NULL, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO com_trans_prompt_properties (organization_id, property_code, effective_date, org_code, org_value, expiration_date, code_category, prompt_title_key, prompt_msg_key, required_flag, sort_order, prompt_mthd_code, prompt_edit_pattern, validation_rule_key, transaction_state, prompt_key, chain_key, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'POSTAL_CODE3', getDate () - 365, '*', '*', getDate () + 365, NULL, '_EnterPostalCodeTitle', '_EnterPostalCodeMessage', 0, 10, 'SIMPLE', NULL, NULL, 'POST_TENDER', NULL, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO com_trans_prompt_properties (organization_id, property_code, effective_date, org_code, org_value, expiration_date, code_category, prompt_title_key, prompt_msg_key, required_flag, sort_order, prompt_mthd_code, prompt_edit_pattern, validation_rule_key, transaction_state, prompt_key, chain_key, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'SHOPPER_STATUS', getDate () - 365, '*', '*', getDate () + 365, 'SHOPPER_STATUS', NULL, NULL, 0, 20, 'LIST', NULL, NULL, 'PRE_SALE', 'TRANS_PROP_LIST_OPTIONAL', NULL, getDate (), 'BaseData', NULL, NULL, NULL)
--
-- Add to translations.properties:
-- _EnterPostalCodeTitle=\u00A0
-- _EnterPostalCodeMessage=Enter the customer's zip code.
--

DELETE FROM com_code_value WHERE organization_id = @intOrganization_ID AND category = 'SHOPPER_STATUS';
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'SHOPPER_STATUS', 'MALE', 'Male', 10, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'SHOPPER_STATUS', 'FEMALE', 'Female', 20, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO com_code_value (organization_id, category, code, description, sort_order, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'SHOPPER_STATUS', 'FAMILY', 'Family Group', 30, getDate (), 'BaseData', NULL, NULL, NULL)

-----------------------------
-- Transaction Prompts
-----------------------------
---------------------------------
-- Base Employees
---------------------------------
DELETE FROM hrs_employee WHERE organization_id = @intOrganization_ID AND employee_id IN (0, 1);
INSERT INTO hrs_employee (organization_id, employee_id, party_id, login_id, job_title, marital_status, clock_in_not_req_flag, employee_role_code, group_membership, primary_group, employee_typcode, training_status_enum, locked_out_flag, locked_out_timestamp, overtime_eligible_flag, employee_group_id, employee_statcode, create_date, create_user_id)
VALUES (@intOrganization_ID, 0, 0, 0, 'DEFAULT EMPLOYEE', 'S', 1, 'EVERYONE', '/wE=', 'EVERYONE', 'EVERYONE', 'EXEMPT', 0, NULL, 0, NULL, 'A', getDate (), 'BaseData');
INSERT INTO hrs_employee (organization_id, employee_id, party_id, login_id, job_title, marital_status, clock_in_not_req_flag, employee_role_code, group_membership, primary_group, employee_typcode, training_status_enum, locked_out_flag, locked_out_timestamp, overtime_eligible_flag, employee_group_id, employee_statcode, create_date, create_user_id)
VALUES (@intOrganization_ID, 1, 1, 1, 'DEFAULT EMPLOYEE', 'S', 1, 'EVERYONE', '/wE=', 'EVERYONE', 'EVERYONE', 'EXEMPT', 0, NULL, 0, NULL, 'A', getDate (), 'BaseData');

DELETE FROM hrs_employee_password WHERE organization_id = @intOrganization_ID AND employee_id IN (0, 1);
INSERT INTO hrs_employee_password (organization_id, employee_id, password_seq, password, current_password_flag, create_date, create_user_id)
VALUES (@intOrganization_ID, 0, 0, '/SA4mbH/7Uk', 1, getDate (), 'BaseData');
INSERT INTO hrs_employee_password (organization_id, employee_id, password_seq, password, current_password_flag, create_date, create_user_id)
VALUES (@intOrganization_ID, 1, 0, '/SA4mbH/7Uk', 1, getDate (), 'BaseData');

DELETE FROM crm_customer_affiliation WHERE organization_id = @intOrganization_ID AND party_id IN (0, 1);
INSERT INTO crm_customer_affiliation (organization_id, cust_group_id, party_id, create_date, create_user_id)
VALUES (@intOrganization_ID, 'EMPLOYEE', 0, getDate (), 'BaseData');
INSERT INTO crm_customer_affiliation (organization_id, cust_group_id, party_id, create_date, create_user_id)
VALUES (@intOrganization_ID, 'EMPLOYEE', 1, getDate (), 'BaseData');

DELETE FROM crm_party WHERE organization_id = @intOrganization_ID AND employee_id IN (0, 1);
INSERT INTO crm_party (organization_id, employee_id, party_id, cust_id, first_name, last_name, party_typcode, preferred_locale, sign_up_rtl_loc_id, allegiance_rtl_loc_id, create_date, create_user_id)
VALUES (@intOrganization_ID, 0, 0, 0, 'House', 'Account',  'EMPLOYEE', 'en_US', 0, 0, getDate (), 'BaseData');
INSERT INTO crm_party (organization_id, employee_id, party_id, cust_id, first_name, last_name,  party_typcode, preferred_locale, sign_up_rtl_loc_id, allegiance_rtl_loc_id, create_date, create_user_id)
VALUES (@intOrganization_ID, 1, 1, 1, 'Support', 'Employee', 'EMPLOYEE', 'en_US', 0, 0, getDate (), 'BaseData');

---------------------------------
-- Base Employees
---------------------------------
------------------
-- Work Codes
------------------
DELETE FROM hrs_work_codes WHERE organization_id = @intOrganization_ID;
--INSERT INTO hrs_work_codes (organization_id, work_code, description, sort_order, privilege, selling_flag , payroll_category, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'DEFAULT', 'Default Work Code', 10, 'ALL_ACCESS', NULL, 'REGULAR', getDate (), 'BaseData');
--
-- OR
--
--INSERT INTO hrs_work_codes (organization_id, work_code, description, sort_order, privilege, selling_flag , payroll_category, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'SELLING', 'Selling', 10, 'ALL_ACCESS', NULL, 'REGULAR', getDate (), 'BaseData');
--INSERT INTO hrs_work_codes (organization_id, work_code, description, sort_order, privilege, selling_flag , payroll_category, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'MEETING', 'In Store Meeting', 20, 'ALL_ACCESS', NULL, 'REGULAR', getDate (), 'BaseData');
--INSERT INTO hrs_work_codes (organization_id, work_code, description, sort_order, privilege, selling_flag , payroll_category, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'INVENTORY', 'Physical Inventory', 30, 'ALL_ACCESS', NULL, 'REGULAR', getDate (), 'BaseData');
--INSERT INTO hrs_work_codes (organization_id, work_code, description, sort_order, privilege, selling_flag , payroll_category, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'TRAINING', 'Training', 40, 'ALL_ACCESS', NULL, 'REGULAR', getDate (), 'BaseData');
--INSERT INTO hrs_work_codes (organization_id, work_code, description, sort_order, privilege, selling_flag , payroll_category, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'MANAGEMENT', 'Store Management', 50, 'ALL_ACCESS', NULL, 'REGULAR', getDate (), 'BaseData');
------------------
-- Work Codes
------------------
-----------
-- Shifts
-----------
DELETE FROM sch_shift WHERE organization_id = @intOrganization_ID;
--INSERT INTO sch_shift (organization_id, shift_id, name, description, work_code, start_time, end_time, create_date, create_user_id)
--VALUES (@intOrganization_ID, 301010000000005, 'WeekdayFT', 'Fulltime Weekday shift', 'SELLING', '1900-01-01 09:00:00', '1900-01-01 13:00:00', getDate (), 'BaseData');
--INSERT INTO sch_shift (organization_id, shift_id, name, description, work_code, start_time, end_time, create_date, create_user_id)
--VALUES (@intOrganization_ID, 301010000000018, 'Cashier eve', 'Cashier eve', 'SELLING', '1900-01-01 17:00:00', '1900-01-01 21:00:00', getDate (), 'BaseData');
--INSERT INTO sch_shift (organization_id, shift_id, name, description, work_code, start_time, end_time, create_date, create_user_id)
--VALUES (@intOrganization_ID, 301010000000019, 'Mgr Day', 'Mgr Day', 'MANAGEMENT', '1900-01-01 09:00:00', '1900-01-01 17:00:00', getDate (), 'BaseData');
--INSERT INTO sch_shift (organization_id, shift_id, name, description, work_code, start_time, end_time, create_date, create_user_id)
--VALUES (@intOrganization_ID, 301010000000020, 'Mgr eve', 'Mgr eve', 'MANAGEMENT', '1900-01-01 17:00:00', '1900-01-01 21:00:00', getDate (), 'BaseData');
--INSERT INTO sch_shift (organization_id, shift_id, name, description, work_code, start_time, end_time, create_date, create_user_id)
--VALUES (@intOrganization_ID, 301010000000021, 'Sales day', 'Sales day', 'SELLING', '1900-01-01 09:00:00', '1900-01-01 17:00:00', getDate (), 'BaseData');
--INSERT INTO sch_shift (organization_id, shift_id, name, description, work_code, start_time, end_time, create_date, create_user_id)
--VALUES (@intOrganization_ID, 301010000000022, 'Sales eve', 'Sales eve', 'SELLING', '1900-01-01 17:00:00', '1900-01-01 21:00:00', getDate (), 'BaseData');
--INSERT INTO sch_shift (organization_id, shift_id, name, description, work_code, start_time, end_time, break_duration, create_date, create_user_id)
--VALUES (@intOrganization_ID, 301010000000044, 'Weekend Morning', 'Weekend Morning', 'SELLING', '1900-01-01 08:00:00', '1900-01-01 13:30:00', 900000, getDate (), 'BaseData');
-----------
-- Shifts
-----------
--------------------------------
-- Store Cycle Questions
--------------------------------
DELETE FROM loc_cycle_question_choices WHERE organization_id = @intOrganization_ID;
--INSERT INTO loc_cycle_question_choices (organization_id, question_id, answer_id, answer_text_key, sort_order, CREATE_DATE, CREATE_USER_ID)
--VALUES (@intOrganization_ID, 1, 'LCQ01', 'Sunny and Warm', 10, getDate (), 'BaseData');
--INSERT INTO loc_cycle_question_choices (organization_id, question_id, answer_id, answer_text_key, sort_order, CREATE_DATE, CREATE_USER_ID)
--VALUES (@intOrganization_ID, 1, 'LCQ02', 'Cloudy and Cool', 20, getDate (), 'BaseData');
--INSERT INTO loc_cycle_question_choices (organization_id, question_id, answer_id, answer_text_key, sort_order, CREATE_DATE, CREATE_USER_ID)
--VALUES (@intOrganization_ID, 1, 'LCQ03', 'Raining', 30, getDate (), 'BaseData');
--INSERT INTO loc_cycle_question_choices (organization_id, question_id, answer_id, answer_text_key, sort_order, CREATE_DATE, CREATE_USER_ID)
--VALUES (@intOrganization_ID, 1, 'LCQ04', 'Snowing', 40, getDate (), 'BaseData');

DELETE FROM loc_cycle_questions WHERE organization_id = @intOrganization_ID;
--INSERT INTO loc_cycle_questions (organization_id, question_id, question_text_key, sort_order, effective_datetime, expiration_datetime, question_typcode, CREATE_DATE, CREATE_USER_ID)
--VALUES (@intOrganization_ID, 1, 'Weather Code', 1, '2000-01-01', '2100-01-01', 'SYSTEM_CLOSE', getDate (), 'BaseData');
--INSERT INTO loc_cycle_questions (organization_id, question_id, question_text_key, sort_order, effective_datetime, expiration_datetime, question_typcode, CREATE_DATE, CREATE_USER_ID)
--VALUES (@intOrganization_ID, 1, 'Weather Code', 1, '2000-01-01', '2100-01-01', 'SYSTEM_OPEN', getDate (), 'BaseData');
--------------------------------
-- Store Cycle Questions
--------------------------------


DELETE FROM rpt_organizer;
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Sales', 'GrossSales', 10, getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Sales', 'NetSales', 20, getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Sales', 'Returns', 30, getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Sales', 'Discounts', 40, getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Sales', 'TotalTax', 50, getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'ACCOUNT_CREDIT', 500, getDate (), 'BaseData');
--INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'ACCOUNT_RECEIVABLE', 510, getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'AMERICAN_EXPRESS', 520, getDate (), 'BaseData');
--INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'CAD_CURRENCY', 530, getDate (), 'BaseData');
--INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'CAD_TRAVELERS_CHECK', 540, getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'CHECK', 550, getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'DEBIT_CARD', 560 , getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'DINERS_CLUB', 570, getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'DISCOVER', 580 , getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'GIFT_CERTIFICATE', 600, getDate (), 'BaseData');
--INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'HOME_OFFICE_CHECK', 610, getDate (), 'BaseData');
--INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'ISSUE_GIFT_CERTIFICATE', 620, getDate (), 'BaseData');
--INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'ISSUE_STORE_CREDIT', 630, getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'ISSUE_ISD_GIFT_CARD', 640, getDate (), 'BaseData');
--INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'JCB', 650, getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'MALL_CERTIFICATE', 660, getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'MASTERCARD', 680, getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'MERCHANDISE_CREDIT_CARD', 690, getDate (), 'BaseData');
--INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'PAYROLL_DEDUCTION', 710, getDate (), 'BaseData');
--INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'PRIVATE_LABEL', 720, getDate (), 'BaseData');
--INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'RELOAD_MERCHANDISE_CREDIT_CARD', 730, getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'RELOAD_ISD_GIFT_CARD', 740, getDate (), 'BaseData');
--INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'ROOM_CHARGE', 750, getDate (), 'BaseData');
--INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'STORE_CREDIT', 760, getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'USD_CURRENCY', 770, getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'USD_TRAVELERS_CHECK', 780, getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'VISA', 790, getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'ISD_GIFT_CARD', 800, getDate (), 'BaseData');
INSERT INTO rpt_organizer (organization_id, report_name, report_group, report_element, report_order, create_date, create_user_id)
VALUES (@intOrganization_ID, 'flash_sales', 'Tenders', 'ISD_GIFT_ECARD', 810, getDate (), 'BaseData');
INSERT INTO rpt_organizer(organization_id,report_name,report_group,report_element,report_order,create_date,create_user_id)
VALUES(@intOrganization_ID, 'flash_sales', 'Tenders', 'GIFT_CARD', 820, getDate(), 'BaseData');
INSERT INTO rpt_organizer(organization_id,report_name,report_group,report_element,report_order,create_date,create_user_id)
VALUES(@intOrganization_ID, 'flash_sales', 'Tenders', 'GIFT_ECARD', 830, getDate(), 'BaseData');
INSERT INTO rpt_organizer(organization_id,report_name,report_group,report_element,report_order,create_date,create_user_id)
VALUES(@intOrganization_ID, 'flash_sales', 'Tenders', 'ISSUE_GIFT_CARD', 840, getDate(), 'BaseData');
INSERT INTO rpt_organizer(organization_id,report_name,report_group,report_element,report_order,create_date,create_user_id)
VALUES(@intOrganization_ID, 'flash_sales', 'Tenders', 'RELOAD_GIFT_CARD', 850, getDate(), 'BaseData');


DELETE FROM sec_access_types WHERE organization_id = @intOrganization_ID;
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'CUSTOMER_ATTRIBUTES', 'CREATE', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'CUSTOMER_ATTRIBUTES', 'READ', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'CUSTOMER_ATTRIBUTES', 'UPDATE', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'CUSTOMER_CONTACT_INFO', 'CREATE', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'CUSTOMER_CONTACT_INFO', 'READ', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'CUSTOMER_CONTACT_INFO', 'UPDATE', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'CUSTOMER_EMPLOYEE_DATA', 'CREATE', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'CUSTOMER_EMPLOYEE_DATA', 'READ', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'CUSTOMER_EMPLOYEE_DATA', 'UPDATE', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'CUSTOMER_FINANCIAL_INFO', 'CREATE', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'CUSTOMER_FINANCIAL_INFO', 'READ', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'CUSTOMER_FINANCIAL_INFO', 'UPDATE', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'CUSTOMER_GROUPS', 'CREATE', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'CUSTOMER_GROUPS', 'READ', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'CUSTOMER_GROUPS', 'UPDATE', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'CUSTOMER_ID', 'CREATE', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'CUSTOMER_ID', 'READ', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'CUSTOMER_ID', 'UPDATE', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'DEFAULT', 'CREATE', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'DEFAULT', 'READ', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'DEFAULT', 'UPDATE', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'EMPLOYEE_DATA', 'CREATE', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'EMPLOYEE_DATA', 'READ', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'EMPLOYEE_DATA', 'UPDATE', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'EMPLOYEE_LOCKOUT', 'CREATE', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'EMPLOYEE_LOCKOUT', 'READ', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'EMPLOYEE_LOCKOUT', 'UPDATE', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'EMPLOYEE_NOTE', 'CREATE', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'EMPLOYEE_NOTE', 'READ', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);
INSERT INTO sec_access_types (ORGANIZATION_ID, SECURED_OBJECT_ID, ACCESS_TYPCODE, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, GROUP_MEMBERSHIP, NO_ACCESS_SETTINGS, RECORD_STATE)
VALUES (@intOrganization_ID, 'EMPLOYEE_NOTE', 'UPDATE', getDate (), 'BaseData', NULL, NULL, 'AQ==', 'HIDDEN', NULL);

DELETE FROM sec_acl WHERE organization_id = @intOrganization_ID;
INSERT INTO sec_acl (organization_id, secured_object_id, org_code, org_value, authentication_req_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'CUSTOMER_ATTRIBUTES', '*', '*', 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO sec_acl (organization_id, secured_object_id, org_code, org_value, authentication_req_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'CUSTOMER_CONTACT_INFO', '*', '*', 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO sec_acl (organization_id, secured_object_id, org_code, org_value, authentication_req_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'CUSTOMER_EMPLOYEE_DATA', '*', '*', 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO sec_acl (organization_id, secured_object_id, org_code, org_value, authentication_req_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'CUSTOMER_FINANCIAL_INFO', '*', '*', 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO sec_acl (organization_id, secured_object_id, org_code, org_value, authentication_req_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'CUSTOMER_GROUPS', '*', '*', 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO sec_acl (organization_id, secured_object_id, org_code, org_value, authentication_req_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'CUSTOMER_ID', '*', '*', 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO sec_acl (organization_id, secured_object_id, org_code, org_value, authentication_req_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DEFAULT', '*', '*', 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO sec_acl (organization_id, secured_object_id, org_code, org_value, authentication_req_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'EMPLOYEE_DATA', '*', '*', 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO sec_acl (organization_id, secured_object_id, org_code, org_value, authentication_req_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'EMPLOYEE_LOCKOUT', '*', '*', 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO sec_acl (organization_id, secured_object_id, org_code, org_value, authentication_req_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'EMPLOYEE_NOTE', '*', '*', 0, getDate (), 'BaseData', NULL, NULL, NULL)

DELETE FROM sec_groups WHERE organization_id = @intOrganization_ID;
INSERT INTO sec_groups (organization_id, group_id, org_code, org_value, description, bitmap_position, group_rank, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'EVERYONE', '*', '*', 'Everyone', 1, 9, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO sec_groups (organization_id, group_id, org_code, org_value, description, bitmap_position, group_rank, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'SALESASSOC', '*', '*', 'Sale Associate', 2, 1, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO sec_groups (organization_id, group_id, org_code, org_value, description, bitmap_position, group_rank, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'KEY', '*', '*', 'Key Holder', 3, 1, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO sec_groups (organization_id, group_id, org_code, org_value, description, bitmap_position, group_rank, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'COUNTERSUPVR', '*', '*', 'Counter Supervisor', 4, 2, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO sec_groups (organization_id, group_id, org_code, org_value, description, bitmap_position, group_rank, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'ASSTSTOREMANAGER', '*', '*', 'Assistant Store Manager', 5, 3, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO sec_groups (organization_id, group_id, org_code, org_value, description, bitmap_position, group_rank, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'STOREMANAGER', '*', '*', 'Store Manager', 6, 3, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO sec_groups (organization_id, group_id, org_code, org_value, description, bitmap_position, group_rank, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DISTRICTMANAGER', '*', '*', 'District Manager', 7, 3, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO sec_groups (organization_id, group_id, org_code, org_value, description, bitmap_position, group_rank, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'REGIONALDIRECTOR', '*', '*', 'Regional Director', 8, 3, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO sec_groups (organization_id, group_id, org_code, org_value, description, bitmap_position, group_rank, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'HELPDESK', '*', '*', 'Help Desk', 9, 3, getDate (), 'BaseData', NULL, NULL, NULL)

DELETE FROM sec_privilege WHERE organization_id = @intOrganization_ID;
------------------------------------------
--Xstore Custom Privilege Settings
------------------------------------------
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'VIEW_CUSTOMER_INFO', 0, 'VIEW_CUSTOMER_INFO', 1, '8AE=', 'NO_PROMPT', 0, '8AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TRAINING_MODE', 0, 'TRAINING_MODE', 1, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'DEPOSIT_BAG_CONVEYANCE', 0, 'DEPOSIT_BAG_CONVEYANCE', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'DEPOSIT_BAG_CORRECTION', 0, 'DEPOSIT_BAG_CORRECTION', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'POST_VOID', 0, 'POST_VOID', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'PAID_IN_OUT', 0, 'PAID_IN_OUT', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'EMPLOYEE_MAINTENANCE', 0, 'EMPLOYEE_MAINTENANCE', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ELECTRONIC_JOURNAL', 0, 'ELECTRONIC_JOURNAL', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'MANAGER_DISCOUNT', 0, 'MANAGER_DISCOUNT', 1, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REFUND_CASH', 0, 'REFUND_CASH', 1, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ASSIGN_CUSTOMER', 0, 'ASSIGN_CUSTOMER', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TILL_AUDIT', 0, 'TILL_AUDIT', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'MID_DAY_DEPOSIT', 0, 'MID_DAY_DEPOSIT', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
------------------------------------------
--Xstore Custom Privilege Settings
------------------------------------------
------------------------------------------
--Xstore Base Privilege Settings
------------------------------------------
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ACCEPT_WARRANTY_NOT_ON_FILE', 0, 'ACCEPT_WARRANTY_NOT_ON_FILE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ACCESS_OTHER_TILLS', 0, 'ACCESS_OTHER_TILLS', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ADD_COUPON', 0, 'ADD_COUPON', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ADD_DISCOUNT', 0, 'ADD_DISCOUNT', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ALLOW_EMPLOYEE_SALE', 0, 'ALLOW_EMPLOYEE_SALE', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ALLOW_OVERSELL', 0, 'ALLOW_OVERSELL', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ASSOCIATE_ADVANCE', 0, 'ASSOCIATE_ADVANCE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ASSOCIATE_SELF_ADVANCE', 0, 'ASSOCIATE_SELF_ADVANCE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ATTACH_OTHER_TILL', 0, 'ATTACH_OTHER_TILL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ATTACH_TILL', 0, 'ATTACH_TILL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CANCEL_INVENTORY_COUNT', 0, 'CANCEL_INVENTORY_COUNT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CASH_PICKUP', 0, 'CASH_PICKUP', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CASH_REFUND_EXCEEDS_AVAILABLE_CASH', 0, 'CASH_REFUND_EXCEEDS_AVAILABLE_CASH', 1, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CASH_TRANSFER', 0, 'CASH_TRANSFER', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_OTHERS_PASSWORD', 0, 'CHANGE_OTHERS_PASSWORD', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_PRICE', 0, 'CHANGE_PRICE', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_QUANTITY', 0, 'CHANGE_QUANTITY', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_SPECIAL_ORDER_EXPECTED_DATE', 0, 'CHANGE_SPECIAL_ORDER_EXPECTED_DATE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_TAX', 0, 'CHANGE_TAX', 1, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_TAX_BY_AMOUNT', 0, 'CHANGE_TAX_BY_AMOUNT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_TAX_BY_PERCENT', 0, 'CHANGE_TAX_BY_PERCENT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_TAX_LOCATION', 0, 'CHANGE_TAX_LOCATION', 1, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_TAX_TO_EXEMPT', 0, 'CHANGE_TAX_TO_EXEMPT', 1, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_TILL_FLOAT', 0, 'CHANGE_TILL_FLOAT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_VERIFIED_RETURN_ITEM_PRICE', 0, 'CHANGE_VERIFIED_RETURN_ITEM_PRICE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CLOSE_WITH_SUSPENDED_TRANS', 0, 'CLOSE_WITH_SUSPENDED_TRANS', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'COMPLETE_INVENTORY_COUNT', 0, 'COMPLETE_INVENTORY_COUNT', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'COUPON_REDEEM_FORCE', 0, 'COUPON_REDEEM_FORCE', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'COUPON_REDEEM_OFFLINE', 0, 'COUPON_REDEEM_OFFLINE', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CREATE_CUSTOMER', 0, 'CREATE_CUSTOMER', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CREATE_SPEC_ORDER', 0, 'CREATE_SPEC_ORDER', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CUST_ASSOC', 0, 'CUST_ASSOC', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'DELETE_CUSTOMER', 0, 'DELETE_CUSTOMER', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'DISABLE_CUST_MSR', 0, 'DISABLE_CUST_MSR', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'DISABLE_MICR', 0, 'DISABLE_MICR', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'DISABLE_VALIDATION_PRINTER', 0, 'DISABLE_VALIDATION_PRINTER', 0, 'AAE=', 'NO_PROMPT', 0, 'AAE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'DISCOUNT_EXCEED_MAX_THRESHOLD', 0, 'DISCOUNT_EXCEED_MAX_THRESHOLD', 1, '8AE=', 'NO_PROMPT', 0, '8AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'EDIT_CUSTOMER', 0, 'EDIT_CUSTOMER', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'EDIT_LAYAWAY_ITEM', 0, 'EDIT_LAYAWAY_ITEM', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'EJOURNAL_ADD_COMMENT', 0, 'EJOURNAL_ADD_COMMENT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'EJOURNAL_GIFT_RECEIPT', 0, 'EJOURNAL_GIFT_RECEIPT', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'EJOURNAL_POST_VOID', 0, 'EJOURNAL_POST_VOID', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'EJOURNAL_REBATE_RECEIPT', 0, 'EJOURNAL_REBATE_RECEIPT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'EJOURNAL_REPRINT_RECEIPT', 0, 'EJOURNAL_REPRINT_RECEIPT', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'EJOURNAL_UPDATE_COMMENT', 0, 'EJOURNAL_UPDATE_COMMENT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'END_COUNT_OTHER_TILL', 0, 'END_COUNT_OTHER_TILL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'END_SALE', 0, 'END_SALE', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'FOREIGN_CURRENCY_MAINT', 0, 'FOREIGN_CURRENCY_MAINT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'INVALID_AR_MANUAL_AUTH_CODE', 0, 'INVALID_AR_MANUAL_AUTH_CODE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ISSUE_TILL', 0, 'ISSUE_TILL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'KEEP_CASH_DRAWER_OPEN', 0, 'KEEP_CASH_DRAWER_OPEN', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'LAYAWAY_CANCEL', 0, 'LAYAWAY_CANCEL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'LOG_IN', 0, 'LOG_IN', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'MAINTAIN_EMPLOYEE_MESSAGES', 0, 'MAINTAIN_EMPLOYEE_MESSAGES', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'MANUAL_CUSTOMER_BIRTH_DATE_ENTRY', 0, 'MANUAL_CUSTOMER_BIRTH_DATE_ENTRY', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'MODIFY_DISCOUNT', 0, 'MODIFY_DISCOUNT', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'NO_SALE', 0, 'NO_SALE', 1, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'NOT_FOUND_VOUCHER', 0, 'NOT_FOUND_VOUCHER', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OPEN_STORE_BANK', 0, 'OPEN_STORE_BANK', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ORDER_REJECT', 0, 'ORDER_REJECT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OVER_MAX_PAYDEDUCT_THRESHOLD', 0, 'OVER_MAX_PAYDEDUCT_THRESHOLD', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OVER_TENDER_ABOVE_CONFIGURED_AMOUNT', 0, 'OVER_TENDER_ABOVE_CONFIGURED_AMOUNT', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OVERRIDE_COUPON_EXPIRED', 0, 'OVERRIDE_COUPON_EXPIRED', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OVERRIDE_COUPON_NOT_EFFECTIVE', 0, 'OVERRIDE_COUPON_NOT_EFFECTIVE', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OVERRIDE_LAYAWAY_CANCEL_TO_ESCROW', 0, 'OVERRIDE_LAYAWAY_CANCEL_TO_ESCROW', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OVERRIDE_LAYAWAY_DEPOSIT_AMOUNT', 0, 'OVERRIDE_LAYAWAY_DEPOSIT_AMOUNT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OVERRIDE_SPEC_ORDER_CANCEL_TO_ESCROW', 0, 'OVERRIDE_SPEC_ORDER_CANCEL_TO_ESCROW', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OVERRIDE_SPECIAL_ORDER_DEPOSIT_AMOUNT', 0, 'OVERRIDE_SPECIAL_ORDER_DEPOSIT_AMOUNT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OVERRIDE_WORK_ORDER_DEPOSIT_AMOUNT', 0, 'OVERRIDE_WORK_ORDER_DEPOSIT_AMOUNT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'PAYROLL_REPOST', 0, 'PAYROLL_REPOST', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'PRINT_CUSTOMER', 0, 'PRINT_CUSTOMER', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'PRINT_GIFT_RCPTS_BEYOND_LIMIT', 0, 'PRINT_GIFT_RCPTS_BEYOND_LIMIT', 1, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RECEIVE_ENTIRE_ASN', 0, 'RECEIVE_ENTIRE_ASN', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RECEIVING', 0, 'RECEIVING', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RECEIVING_EDIT_CLOSED_DATA', 0, 'RECEIVING_EDIT_CLOSED_DATA', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RECONCILE_STORE_BANK', 0, 'RECONCILE_STORE_BANK', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RECONCILE_TILL', 0, 'RECONCILE_TILL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REFUND_TENDER_ABOVE_CONFIGURED_AMOUNT', 0, 'REFUND_TENDER_ABOVE_CONFIGURED_AMOUNT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REFUND_TENDER_BALANCE', 0, 'REFUND_TENDER_BALANCE', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REMOVE_OTHER_TILL', 0, 'REMOVE_OTHER_TILL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REMOVE_TILL', 0, 'REMOVE_TILL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REPAIR_INVOICING', 0, 'REPAIR_INVOICING', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REPLENISHMENT_ORDER_FORCE_CLOSE', 0, 'REPLENISHMENT_ORDER_FORCE_CLOSE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REPORT_DELETE_CORPORATE', 0, 'REPORT_DELETE_CORPORATE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REPORT_DELETE_PERSONAL', 0, 'REPORT_DELETE_PERSONAL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REPORT_DELETE_STORE', 0, 'REPORT_DELETE_STORE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REPRINT_PREVIOUS_DAY', 0, 'REPRINT_PREVIOUS_DAY', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_EXCEED_MAX_THRESHOLD', 0, 'RETURN_EXCEED_MAX_THRESHOLD', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_GIFT_CARD', 0, 'RETURN_GIFT_CARD', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_GIFT_CERTIFICATE', 0, 'RETURN_GIFT_CERTIFICATE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_ITEM', 0, 'RETURN_ITEM', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_ITEM_NOT_IN_HISTORY', 0, 'RETURN_ITEM_NOT_IN_HISTORY', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_ITEM_NOT_IN_ORIG_TRANS', 0, 'RETURN_ITEM_NOT_IN_ORIG_TRANS', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_KIT', 0, 'RETURN_KIT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_MAX_ITEM_PRICE_VALIDATION', 0, 'RETURN_MAX_ITEM_PRICE_VALIDATION', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_MIN_ITEM_PRICE_VALIDATION', 0, 'RETURN_MIN_ITEM_PRICE_VALIDATION', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_NOT_RETURNABLE_ITEM_OVER_MAXDAYS', 0, 'RETURN_NOT_RETURNABLE_ITEM_OVER_MAXDAYS', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_QUANTITY_EXCEED_ORIG_QUANTITY', 0, 'RETURN_QUANTITY_EXCEED_ORIG_QUANTITY', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_TILL', 0, 'RETURN_TILL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_WITH_NON_RETURN_ITEM', 0, 'RETURN_WITH_NON_RETURN_ITEM', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'SELL_GIFT_CARD', 0, 'SELL_GIFT_CARD', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'SELL_GIFT_CERTIFICATE', 0, 'SELL_GIFT_CERTIFICATE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'SELL_ITEM', 0, 'SELL_ITEM', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'SELL_ITEM_NOT_ON_FILE', 0, 'SELL_ITEM_NOT_ON_FILE', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'SELL_NON_PHYSICAL', 0, 'SELL_NON_PHYSICAL', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'SELL_POINTS_CARD', 0, 'SELL_POINTS_CARD', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'SHIPPER_METHOD_OVERRIDE', 0, 'SHIPPER_METHOD_OVERRIDE', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'SHIPPING_EDIT_CLOSED_DATA', 0, 'SHIPPING_EDIT_CLOSED_DATA', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'SPEC_ORDER_CANCEL', 0, 'SPEC_ORDER_CANCEL', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'STORE_BANK_CASH_DEPOSIT', 0, 'STORE_BANK_CASH_DEPOSIT', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TENDER_ABOVE_CONFIGURED_AMOUNT', 0, 'TENDER_ABOVE_CONFIGURED_AMOUNT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TENDER_ACCOUNT_RECEIVABLE_OVER_LIMIT', 0, 'TENDER_ACCOUNT_RECEIVABLE_OVER_LIMIT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TENDER_BELOW_CONFIGURED_AMOUNT', 0, 'TENDER_BELOW_CONFIGURED_AMOUNT', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TENDER_BEYOND_CHANGE_THRESHOLD', 0, 'TENDER_BEYOND_CHANGE_THRESHOLD', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TENDER_EXCHANGE', 0, 'TENDER_EXCHANGE', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TILL_COUNT_OVER_THRESHOLD_OVERRIDE', 0, 'TILL_COUNT_OVER_THRESHOLD_OVERRIDE', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TIME_CLOCK', 0, 'TIME_CLOCK', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TIME_CLOCK_DURATION', 0, 'TIME_CLOCK_DURATION', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TOTAL_LAYAWAY_ITEM_PRICE_BELOW_MIN', 0, 'TOTAL_LAYAWAY_ITEM_PRICE_BELOW_MIN', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TOTAL_SP_ORDER_ITEM_PRICE_BELOW_MIN', 0, 'TOTAL_SP_ORDER_ITEM_PRICE_BELOW_MIN', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'UNLOCK_OTHERS_REGISTER', 0, 'UNLOCK_OTHERS_REGISTER', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'UNLOCKABLE', 0, 'UNLOCKABLE', 0, '/AE=', 'NO_PROMPT', 0, '/AE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'VIEW_ALL_TASKS', 0, 'VIEW_ALL_TASKS', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'VIEW_AND_EDIT_CUSTOMER_GROUPS', 0, 'VIEW_AND_EDIT_CUSTOMER_GROUPS', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'VIEW_CUSTOMER', 0, 'VIEW_CUSTOMER', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'VIEW_TIMECARD', 0, 'VIEW_TIMECARD', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'VOID_LINE', 0, 'VOID_LINE', 0, 'AQA=', 'NO_PROMPT', 0, 'AQA=', getDate (), 'BaseData');






INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ADMINISTRATIVE_WORK_CODE', 0, 'ADMINISTRATIVE_WORK_CODE', 0, '/wE=', 'NO_PROMPT', 0, '/wE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'ALL_ACCESS', 0, 'ALL_ACCESS', 0, '/wE=', 'NO_PROMPT', 0, '/wE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'CHANGE_TENDER_PRICE', 0, 'CHANGE_TENDER_PRICE', 0, '/wE=', 'NO_PROMPT', 0, '/wE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'DELETE_TENDER', 0, 'DELETE_TENDER', 0, '/wE=', 'NO_PROMPT', 0, '/wE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'JMX_ACCESS', 0, 'JMX_ACCESS', 0, '/wE=', 'NO_PROMPT', 0, '/wE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'NEGATIVE_PRICE_CHANGE', 0, 'NEGATIVE_PRICE_CHANGE', 0, '/wE=', 'NO_PROMPT', 0, '/wE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'OVERRIDE_6040', 0, 'OVERRIDE_6040', 0, '/wE=', 'NO_PROMPT', 0, '/wE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'PAYROLL_EDIT_AFTER_POST', 0, 'PAYROLL_EDIT_AFTER_POST', 0, '/wE=', 'NO_PROMPT', 0, '/wE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'PAYROLL_MAINTENANCE', 0, 'PAYROLL_MAINTENANCE', 0, '/wE=', 'NO_PROMPT', 0, '/wE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'POSITIVE_PRICE_CHANGE', 0, 'POSITIVE_PRICE_CHANGE', 0, '/wE=', 'NO_PROMPT', 0, '/wE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'REGENERATE_PAYROLL_FILE', 0, 'REGENERATE_PAYROLL_FILE', 0, 'AAA=', 'NO_PROMPT', 0, 'AAA=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RELOAD_GIFT_CARD', 0, 'RELOAD_GIFT_CARD', 0, '/wE=', 'NO_PROMPT', 0, '/wE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_NOT_RETURNABLE_ITEM', 0, 'RETURN_NOT_RETURNABLE_ITEM', 0, '/wE=', 'NO_PROMPT', 0, '/wE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_REASONCODE_MANAGER', 0, 'RETURN_REASONCODE_MANAGER', 0, '/wE=', 'NO_PROMPT', 0, '/wE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'RETURN_REASONCODE_NORMAL', 0, 'RETURN_REASONCODE_NORMAL', 0, '/wE=', 'NO_PROMPT', 0, '/wE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TILL_MANAGEMENT', 0, 'TILL_MANAGEMENT', 0, '/wE=', 'NO_PROMPT', 0, '/wE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'TIMECARD_MAINTENANCE', 0, 'TIMECARD_MAINTENANCE', 0, '/wE=', 'NO_PROMPT', 0, '/wE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'VOID_DISCOUNT', 0, 'VOID_DISCOUNT', 0, '/wE=', 'NO_PROMPT', 0, '/wE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'VOID_ITEM', 0, 'VOID_ITEM', 0, '/wE=', 'NO_PROMPT', 0, '/wE=', getDate (), 'BaseData');
INSERT INTO sec_privilege (organization_id, privilege_type, authentication_req, description, overridable_flag, group_membership, second_prompt_settings, second_prompt_req_diff_emp, second_prompt_group_membership, create_date, create_user_id) VALUES (@intOrganization_ID, 'VOID_TENDER', 0, 'VOID_TENDER', 0, '/wE=', 'NO_PROMPT', 0, '/wE=', getDate (), 'BaseData');
------------------------------------------
--Xstore Base Privilege Settings
------------------------------------------


DELETE FROM thr_payroll_category WHERE organization_id = @intOrganization_ID;
--INSERT INTO thr_payroll_category (organization_id, payroll_category, description, sort_order, include_in_overtime_flag, working_category_flag, pay_code, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'REGULAR', '_payrollCategory.REGULAR.description', 10, 1, 1, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO thr_payroll_category (organization_id, payroll_category, description, sort_order, include_in_overtime_flag, working_category_flag, pay_code, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'SICK', '_payrollCategory.SICK.description', 20, 0, 0, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO thr_payroll_category (organization_id, payroll_category, description, sort_order, include_in_overtime_flag, working_category_flag, pay_code, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'HOLIDAY', '_payrollCategory.HOLIDAY.description', 30, 0, 0, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO thr_payroll_category (organization_id, payroll_category, description, sort_order, include_in_overtime_flag, working_category_flag, pay_code, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'PERSONAL', '_payrollCategory.PERSONAL.description', 40, 0, 0, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO thr_payroll_category (organization_id, payroll_category, description, sort_order, include_in_overtime_flag, working_category_flag, pay_code, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'VACATION', '_payrollCategory.VACATION.description', 50, 0, 0, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO thr_payroll_category (organization_id, payroll_category, description, sort_order, include_in_overtime_flag, working_category_flag, pay_code, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'BEREAVEMENT', '_payrollCategory.BEREAVEMENT.description', 60, 0, 0, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO thr_payroll_category (organization_id, payroll_category, description, sort_order, include_in_overtime_flag, working_category_flag, pay_code, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'FUNERAL', '_payrollCategory.FUNERAL.description', 70, 0, 0, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO thr_payroll_category (organization_id, payroll_category, description, sort_order, include_in_overtime_flag, working_category_flag, pay_code, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'JURY', '_payrollCategory.JURY.description', 80, 0, 0, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO thr_payroll_category (organization_id, payroll_category, description, sort_order, include_in_overtime_flag, working_category_flag, pay_code, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'OTHER', '_payrollCategory.OTHER.description', 90, 0, 0, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO thr_payroll_category (organization_id, payroll_category, description, sort_order, include_in_overtime_flag, working_category_flag, pay_code, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'OT', '_payrollCategory.OT.description', 100, 0, 1, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO thr_payroll_category (organization_id, payroll_category, description, sort_order, include_in_overtime_flag, working_category_flag, pay_code, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'DT', '_payrollCategory.DT.description', 110, 0, 1, NULL, getDate (), 'BaseData', NULL, NULL, NULL)

DELETE FROM tnd_tndr WHERE organization_id = @intOrganization_ID;
--INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'ACCOUNT_CREDIT', '*', '*', 'ACCOUNT_CREDIT', 'USD', 'Previous Payment Credit', NULL, 0, 0, 0, 0, 0, 0, 0, 'TOTAL_SHORT', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'LOCAL_CURRENCY', NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'AMERICAN_EXPRESS', '*', '*', 'CREDIT_CARD', 'USD', 'American Express', 'ISD_AMEX', 0, 1, 1, 0, 1, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'LOCAL_CURRENCY', NULL, 0, 1, 0, NULL, 0, 1, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'CHECK', '*', '*', 'CHECK', 'USD', 'Check', 'ISD_CHECK', 1, 1, 0, 0, 0, 1, 0, 'UNIT_SHORT', 0, 1, NULL, 30, 'TENDER SUMMARY', NULL, NULL, 0, 999999, 'DRIVER_LICENSE', 0, 0, 0, NULL, 0, 'LOCAL_CURRENCY', NULL, 0, 1, 0, NULL, 0, 0, 0, 1, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DEBIT_CARD', '*', '*', 'CREDIT_CARD', 'USD', 'Debit Card', 'ISD_DEBIT_CARD', 0, 1, 0, 1, 0, 0, 0, 'DENOMINATION', 1, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'LOCAL_CURRENCY', NULL, 0, 1, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DINERS_CLUB', '*', '*', 'CREDIT_CARD', 'USD', 'Diners (Discover Network)', 'ISD_DINERS', 0, 1, 1, 0, 1, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'LOCAL_CURRENCY', NULL, 0, 1, 0, NULL, 0, 1, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DISCOVER', '*', '*', 'CREDIT_CARD', 'USD', 'Discover', 'ISD_DISCOVER', 0, 1, 1, 0, 1, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'LOCAL_CURRENCY', NULL, 0, 1, 0, NULL, 0, 1, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'GIFT_CERTIFICATE', '*', '*', 'VOUCHER', 'USD', 'Gift Certificate', NULL, 1, 0, 0, 0, 0, 1, 0, 'DENOMINATION', 0, 0, NULL, 80, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'ISSUE_ISD_GIFT_CARD', 4999.990000, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'ISSUE_MERCHANDISE_CREDIT_CARD', '*', '*', 'VOUCHER', 'USD', 'Issue Merchandise Credit Card', 'ISD_GIFT_CARD', 0, 1, 0, 0, 0, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 1, 0, 0, NULL, 0, 'LOCAL_CURRENCY', NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'ISSUE_ISD_GIFT_CARD', '*', '*', 'VOUCHER', 'USD', 'Issue Gift Card', 'ISD_GIFT_CARD', 0, 1, 0, 0, 0, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'LOCAL_CURRENCY', NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'MALL_CERTIFICATE', '*', '*', 'CHECK', 'USD', 'Mall Certificate Check', NULL, 1, 0, 0, 0, 0, 1, 0, 'UNIT_SHORT', 0, 0, NULL, 40, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'ISSUE_ISD_GIFT_CARD', 4999.990000, 0, 0, 0, NULL, 0, 0, 0, 1, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'MASTERCARD', '*', '*', 'CREDIT_CARD', 'USD', 'MasterCard', 'ISD_MASTERCARD', 0, 1, 1, 0, 1, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'LOCAL_CURRENCY', NULL, 0, 1, 0, NULL, 0, 1, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'MERCHANDISE_CREDIT_CARD', '*', '*', 'VOUCHER', 'USD', 'Merchandise Credit', 'ISD_GIFT_CARD', 0, 1, 0, 0, 0, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'ISSUE_ISD_GIFT_CARD', NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'RELOAD_MERCHANDISE_CREDIT_CARD', '*', '*', 'VOUCHER', 'USD', 'Reload Merchandise Credit', 'ISD_GIFT_CARD', 0, 1, 0, 0, 0, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'LOCAL_CURRENCY', NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'RELOAD_ISD_GIFT_CARD', '*', '*', 'VOUCHER', 'USD', 'Reload Gift Card', 'ISD_GIFT_CARD', 0, 1, 0, 0, 0, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'LOCAL_CURRENCY', NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'USD_CURRENCY', '*', '*', 'CURRENCY', 'USD', 'USD Cash', NULL, 0, 0, 0, 0, 0, 0, 1, 'DENOMINATION', 0, 0, NULL, 10, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 1, 0, 0, 1500.000000, 1, 'LOCAL_CURRENCY', 99.990000, 0, 0, 0, NULL, 0, 0, 0, 1, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'USD_TRAVELERS_CHECK', '*', '*', 'TRAVELERS_CHECK', 'USD', 'USD Travelers Check', NULL, 0, 0, 0, 0, 0, 1, 0, 'DENOMINATION', 0, 0, NULL, 20, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'LOCAL_CURRENCY', 99.990000, 0, 0, 0, NULL, 0, 0, 0, 1, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'VISA', '*', '*', 'CREDIT_CARD', 'USD', 'Visa', 'ISD_VISA', 0, 1, 1, 0, 1, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'LOCAL_CURRENCY', NULL, 0, 1, 0, NULL, 0, 1, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'ISD_GIFT_CARD', '*', '*', 'VOUCHER', 'USD', 'Gift Card', 'ISD_GIFT_CARD', 0, 1, 0, 0, 0, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'ISSUE_ISD_GIFT_CARD', NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'ISD_GIFT_ECARD', '*', '*', 'VOUCHER', 'USD', 'EGift Card', 'ISD_GIFT_ECARD', 0, 1, 0, 1, 0, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'ISSUE_ISD_GIFT_ECARD', NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'RELOAD_ISD_GIFT_ECARD', '*', '*', 'VOUCHER', 'USD', 'Reload EGift Card', 'ISD_GIFT_ECARD', 0, 1, 0, 1, 0, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'ISSUE_ISD_GIFT_CARD', NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'GIFT_CARD', '*', '*', 'VOUCHER', 'USD', 'Gift Card', 'AJB_GIFT_CARD', 0, 1, 0, 0, 0, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'ISSUE_GIFT_CARD', NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'ISSUE_GIFT_CARD', '*', '*', 'VOUCHER', 'USD', 'Issue Gift Card', 'AJB_GIFT_CARD', 0, 1, 0, 0, 0, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'LOCAL_CURRENCY', NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'RELOAD_GIFT_CARD', '*', '*', 'VOUCHER', 'USD', 'Reload Gift Card', 'AJB_GIFT_CARD', 0, 1, 0, 0, 0, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'LOCAL_CURRENCY', NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'GIFT_ECARD', '*', '*', 'VOUCHER', 'USD', 'EGift Card', 'AJB_GIFT_CARD', 0, 1, 0, 1, 0, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'ISSUE_GIFT_CARD', NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr (organization_id, tndr_id, org_code, org_value, tndr_typcode, currency_id, description, auth_mthd_code, serial_id_nbr_req_flag, auth_req_flag, auth_expr_date_req_flag, pin_req_flag, cust_sig_req_flag, endorsement_req_flag, open_cash_drawer_req_flag, unit_count_req_code, mag_swipe_reader_req_flag, dflt_to_amt_due_flag, min_denomination_amt, display_order, reporting_group, effective_date, expr_date, min_days_for_return, max_days_for_return, cust_id_req_code, cust_association_flag, populate_system_count_flag, include_in_type_count_flag, suggested_deposit_threshold, suggest_deposit_flag, change_tndr_id, cash_change_limit, over_tender_overridable_flag, non_voidable_flag, disallow_split_tndr_flag, close_count_disc_threshold, cid_msr_req_flag, cid_keyed_req_flag, postal_code_req_flag, post_void_open_drawer_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'RELOAD_GIFT_ECARD', '*', '*', 'VOUCHER', 'USD', 'Reload EGift Card', 'AJB_GIFT_CARD', 0, 1, 0, 1, 0, 0, 0, 'DENOMINATION', 0, 1, NULL, 1000, 'TENDER SUMMARY', NULL, NULL, 0, 999999, NULL, 0, 0, 0, NULL, 0, 'ISSUE_GIFT_CARD', NULL, 0, 0, 0, NULL, 0, 0, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)

-- Act 362517 email dt. 9-19-2012
UPDATE tnd_tndr
SET cid_keyed_req_flag = 0
WHERE organization_id = @intOrganization_ID
AND tndr_id in ('AMERICAN_EXPRESS','DINERS_CLUB', 'DISCOVER', 'MASTERCARD', 'VISA') ;

DELETE FROM tnd_tndr_availability WHERE organization_id = @intOrganization_ID;
-- ********************************
-- CASH_DRAWER_REPORT
-- ********************************
-- Currency
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'USD_TRAVELERS_CHECK', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'CAD_CURRENCY', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'CAD_TRAVELERS_CHECK', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
-- Checks / Vouchers
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'CHECK', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'GIFT_CERTIFICATE', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'HOME_OFFICE_CHECK', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ISSUE_GIFT_CERTIFICATE', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ISSUE_STORE_CREDIT', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'MALL_CERTIFICATE', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'STORE_CREDIT', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
-- Credit / Debit Cards
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'AMERICAN_EXPRESS', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'DINERS_CLUB', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'DISCOVER', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'JCB', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'MASTERCARD', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'VISA', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'DEBIT_CARD', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
-- Gift / Merchandise Cards
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISSUE_MERCHANDISE_CREDIT_CARD', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'MERCHANDISE_CREDIT_CARD', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'RELOAD_MERCHANDISE_CREDIT_CARD', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISSUE_ISD_GIFT_CARD', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'RELOAD_ISD_GIFT_CARD', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISD_GIFT_CARD', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISD_GIFT_ECARD', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISSUE_GIFT_CARD', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'RELOAD_GIFT_CARD', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'GIFT_CARD', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'GIFT_ECARD', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');

-- Miscellaneous
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ACCOUNT_RECEIVABLE', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'COUPON', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'PAYROLL_DEDUCTION', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'PRIVATE_LABEL', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ROOM_CHARGE', 'CASH_DRAWER_REPORT', getDate (), 'BaseData');
-- ********************************
-- DEPOSIT
-- ********************************
-- Currency
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'CAD_CURRENCY', 'DEPOSIT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'DEPOSIT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'CAD_TRAVELERS_CHECK', 'DEPOSIT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'USD_TRAVELERS_CHECK', 'DEPOSIT', getDate (), 'BaseData');
-- Checks / Vouchers
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'CHECK', 'DEPOSIT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'MALL_CERTIFICATE', 'DEPOSIT', getDate (), 'BaseData');
-- ********************************
-- LAYAWAY
-- ********************************
-- Currency
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'CAD_CURRENCY', 'LAYAWAY', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'USD_CURRENCY', 'LAYAWAY', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'CAD_TRAVELERS_CHECK', 'LAYAWAY', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'USD_TRAVELERS_CHECK', 'LAYAWAY', getDate (), 'BaseData');
-- Checks / Vouchers
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'CHECK', 'LAYAWAY', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'GIFT_CERTIFICATE', 'LAYAWAY', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'MALL_CERTIFICATE', 'LAYAWAY', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'STORE_CREDIT', 'LAYAWAY', getDate (), 'BaseData');
-- Credit / Debit Cards
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'AMERICAN_EXPRESS', 'LAYAWAY', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'DINERS_CLUB', 'LAYAWAY', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'DISCOVER', 'LAYAWAY', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'JCB', 'LAYAWAY', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'MASTERCARD', 'LAYAWAY', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'VISA', 'LAYAWAY', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'DEBIT_CARD', 'LAYAWAY', getDate (), 'BaseData');
-- Gift / Merchandise Cards
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'MERCHANDISE_CREDIT_CARD', 'LAYAWAY', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'ISD_GIFT_CARD', 'LAYAWAY', getDate (), 'BaseData');


-- Miscellaneous
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'ACCOUNT_RECEIVABLE', 'LAYAWAY', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'COUPON', 'LAYAWAY', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'PAYROLL_DEDUCTION', 'LAYAWAY', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'PRIVATE_LABEL', 'LAYAWAY', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ROOM_CHARGE', 'LAYAWAY', getDate (), 'BaseData');
-- ********************************
-- ORDERS
-- ********************************
-- Currency
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'CAD_CURRENCY', 'ORDER', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'CAD_TRAVELERS_CHECK', 'ORDER', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'USD_TRAVELERS_CHECK', 'ORDER', getDate (), 'BaseData');
-- Checks / Vouchers
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'CHECK', 'ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'GIFT_CERTIFICATE', 'ORDER', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'MALL_CERTIFICATE', 'ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'STORE_CREDIT', 'ORDER', getDate (), 'BaseData');
-- Credit / Debit Cards
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'AMERICAN_EXPRESS', 'ORDER', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'DINERS_CLUB', 'ORDER', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'DISCOVER', 'ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'JCB', 'ORDER', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'MASTERCARD', 'ORDER', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'VISA', 'ORDER', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'DEBIT_CARD', 'ORDER', getDate (), 'BaseData');
-- Gift / Merchandise Cards
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'MERCHANDISE_CREDIT_CARD', 'ORDER', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISD_GIFT_CARD', 'ORDER', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISD_GIFT_ECARD', 'ORDER', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'GIFT_CARD', 'ORDER', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'GIFT_ECARD', 'ORDER', getDate (), 'BaseData');

-- Miscellaneous
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ACCOUNT_RECEIVABLE', 'ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'COUPON', 'ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'PAYROLL_DEDUCTION', 'ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'PRIVATE_LABEL', 'ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ROOM_CHARGE', 'ORDER', getDate (), 'BaseData');
-- ********************************
-- RETURN_WITH_GIFT_RECEIPT
-- ********************************
-- Currency
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'CAD_CURRENCY', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'USD_CURRENCY', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
-- Checks / Vouchers
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'HOME_OFFICE_CHECK', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ISSUE_STORE_CREDIT', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
-- Credit / Debit Cards
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'AMERICAN_EXPRESS', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'DINERS_CLUB', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'DISCOVER', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'JCB', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'MASTERCARD', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'VISA', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'DEBIT_CARD', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
-- Gift / Merchandise Cards
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ISSUE_MERCHANDISE_CREDIT_CARD', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'RELOAD_MERCHANDISE_CREDIT_CARD', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISSUE_ISD_GIFT_CARD', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'RELOAD_ISD_GIFT_CARD', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISSUE_GIFT_CARD', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'RELOAD_GIFT_CARD', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');

-- Miscellaneous
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ACCOUNT_RECEIVABLE', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'PRIVATE_LABEL', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ROOM_CHARGE', 'RETURN_WITH_GIFT_RECEIPT', getDate (), 'BaseData');
-- ********************************
-- RETURN_WITH_RECEIPT
-- ********************************
-- Currency
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'CAD_CURRENCY', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
-- Checks / Vouchers
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'HOME_OFFICE_CHECK', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ISSUE_STORE_CREDIT', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
-- Credit / Debit Cards
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'AMERICAN_EXPRESS', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'DINERS_CLUB', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'DISCOVER', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'JCB', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'MASTERCARD', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'VISA', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'DEBIT_CARD', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
-- Gift / Merchandise Cards
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISSUE_MERCHANDISE_CREDIT_CARD', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'RELOAD_MERCHANDISE_CREDIT_CARD', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISSUE_ISD_GIFT_CARD', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISSUE_GIFT_CARD', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');

--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'RELOAD_ISD_GIFT_CARD', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
-- Miscellaneous
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ACCOUNT_RECEIVABLE', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'PRIVATE_LABEL', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ROOM_CHARGE', 'RETURN_WITH_RECEIPT', getDate (), 'BaseData');
-- ********************************
-- RETURN_WITHOUT_RECEIPT
-- ********************************
-- Currency
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'CAD_CURRENCY', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'USD_CURRENCY', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');
-- Checks / Vouchers
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'HOME_OFFICE_CHECK', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ISSUE_STORE_CREDIT', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');
-- Credit / Debit Cards
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'AMERICAN_EXPRESS', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'DINERS_CLUB', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'DISCOVER', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'JCB', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'MASTERCARD', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'VISA', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'DEBIT_CARD', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');
-- Gift / Merchandise Cards
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISSUE_MERCHANDISE_CREDIT_CARD', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISSUE_GIFT_CARD', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');

--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'RELOAD_MERCHANDISE_CREDIT_CARD', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ISSUE_ISD_GIFT_CARD', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'RELOAD_ISD_GIFT_CARD', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');
-- Miscellaneous
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ACCOUNT_RECEIVABLE', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'PRIVATE_LABEL', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ROOM_CHARGE', 'RETURN_WITHOUT_RECEIPT', getDate (), 'BaseData');
-- ********************************
-- SALE
-- ********************************
-- Currency
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'CAD_CURRENCY', 'SALE', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'CAD_TRAVELERS_CHECK', 'SALE', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'USD_TRAVELERS_CHECK', 'SALE', getDate (), 'BaseData');
-- Checks / Vouchers
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'CHECK', 'SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'GIFT_CERTIFICATE', 'SALE', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'MALL_CERTIFICATE', 'SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'STORE_CREDIT', 'SALE', getDate (), 'BaseData');
-- Credit / Debit Cards
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'AMERICAN_EXPRESS', 'SALE', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'DINERS_CLUB', 'SALE', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'DISCOVER', 'SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'JCB', 'SALE', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'MASTERCARD', 'SALE', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'VISA', 'SALE', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'DEBIT_CARD', 'SALE', getDate (), 'BaseData');
-- Gift / Merchandise Cards
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'MERCHANDISE_CREDIT_CARD', 'SALE', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISD_GIFT_CARD', 'SALE', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISD_GIFT_ECARD', 'SALE', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'GIFT_CARD', 'SALE', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'GIFT_ECARD', 'SALE', getDate (), 'BaseData');

-- Miscellaneous
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ACCOUNT_RECEIVABLE', 'SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'COUPON', 'SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'PAYROLL_DEDUCTION', 'SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'PRIVATE_LABEL', 'SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ROOM_CHARGE', 'SALE', getDate (), 'BaseData');
-- ********************************
-- SHIP_SALE
-- ********************************
-- Currency
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'CAD_CURRENCY', 'SHIP_SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'USD_CURRENCY', 'SHIP_SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'CAD_TRAVELERS_CHECK', 'SHIP_SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'USD_TRAVELERS_CHECK', 'SHIP_SALE', getDate (), 'BaseData');
-- Checks / Vouchers
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'CHECK', 'SHIP_SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'GIFT_CERTIFICATE', 'SHIP_SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'MALL_CERTIFICATE', 'SHIP_SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'STORE_CREDIT', 'SHIP_SALE', getDate (), 'BaseData');
-- Credit / Debit Cards
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'AMERICAN_EXPRESS', 'SHIP_SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'DINERS_CLUB', 'SHIP_SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'DISCOVER', 'SHIP_SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'JCB', 'SHIP_SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'MASTERCARD', 'SHIP_SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'VISA', 'SHIP_SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'DEBIT_CARD', 'SHIP_SALE', getDate (), 'BaseData');
-- Gift / Merchandise Cards
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'MERCHANDISE_CREDIT_CARD', 'SHIP_SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'ISD_GIFT_CARD', 'SHIP_SALE', getDate (), 'BaseData');
-- Miscellaneous
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'ACCOUNT_RECEIVABLE', 'SHIP_SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'COUPON', 'SHIP_SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'PAYROLL_DEDUCTION', 'SHIP_SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'PRIVATE_LABEL', 'SHIP_SALE', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ROOM_CHARGE', 'SHIP_SALE', getDate (), 'BaseData');
-- ********************************
-- SPECIAL_ORDER
-- ********************************
-- Currency
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'CAD_CURRENCY', 'SPECIAL_ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'USD_CURRENCY', 'SPECIAL_ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'CAD_TRAVELERS_CHECK', 'SPECIAL_ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'USD_TRAVELERS_CHECK', 'SPECIAL_ORDER', getDate (), 'BaseData');
-- Checks / Vouchers
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'CHECK', 'SPECIAL_ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'GIFT_CERTIFICATE', 'SPECIAL_ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'MALL_CERTIFICATE', 'SPECIAL_ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'STORE_CREDIT', 'SPECIAL_ORDER', getDate (), 'BaseData');
-- Credit / Debit Cards
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'AMERICAN_EXPRESS', 'SPECIAL_ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'DINERS_CLUB', 'SPECIAL_ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'DISCOVER', 'SPECIAL_ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'JCB', 'SPECIAL_ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'MASTERCARD', 'SPECIAL_ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'VISA', 'SPECIAL_ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'DEBIT_CARD', 'SPECIAL_ORDER', getDate (), 'BaseData');
-- Gift / Merchandise Cards
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'MERCHANDISE_CREDIT_CARD', 'SPECIAL_ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'ISD_GIFT_CARD', 'SPECIAL_ORDER', getDate (), 'BaseData');
-- Miscellaneous
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ACCOUNT_RECEIVABLE', 'SPECIAL_ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'COUPON', 'SPECIAL_ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'PAYROLL_DEDUCTION', 'SPECIAL_ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'PRIVATE_LABEL', 'SPECIAL_ORDER', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ROOM_CHARGE', 'SPECIAL_ORDER', getDate (), 'BaseData');
-- ********************************
-- TENDER_EXCHANGE_IN
-- ********************************
-- Currency
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'CAD_CURRENCY', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'USD_CURRENCY', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'CAD_TRAVELERS_CHECK', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'USD_TRAVELERS_CHECK', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
-- Checks / Vouchers
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'CHECK', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'GIFT_CERTIFICATE', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'MALL_CERTIFICATE', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'STORE_CREDIT', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
-- Credit / Debit Cards
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'AMERICAN_EXPRESS', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'DINERS_CLUB', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'DISCOVER', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'JCB', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'MASTERCARD', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'VISA', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'DEBIT_CARD', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
-- Gift / Merchandise Cards
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'MERCHANDISE_CREDIT_CARD', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISD_GIFT_CARD', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISD_GIFT_ECARD', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'GIFT_CARD', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'GIFT_ECARD', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');

-- Miscellaneous
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ACCOUNT_RECEIVABLE', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'COUPON', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'PRIVATE_LABEL', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ROOM_CHARGE', 'TENDER_EXCHANGE_IN', getDate (), 'BaseData');
-- ********************************
-- TENDER_EXCHANGE_OUT
-- ********************************
-- Currency
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'CAD_CURRENCY', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
-- Checks / Vouchers
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'HOME_OFFICE_CHECK', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ISSUE_GIFT_CERTIFICATE', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ISSUE_STORE_CREDIT', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
-- Credit / Debit Cards
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'AMERICAN_EXPRESS', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'DINERS_CLUB', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'DISCOVER', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'JCB', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'MASTERCARD', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'VISA', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'DEBIT_CARD', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
-- Gift / Merchandise Cards
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ISSUE_MERCHANDISE_CREDIT_CARD', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'RELOAD_MERCHANDISE_CREDIT_CARD', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISSUE_ISD_GIFT_CARD', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'RELOAD_ISD_GIFT_CARD', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'ISSUE_GIFT_CARD', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'RELOAD_GIFT_CARD', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');

-- Miscellaneous
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ACCOUNT_RECEIVABLE', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'PRIVATE_LABEL', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ROOM_CHARGE', 'TENDER_EXCHANGE_OUT', getDate (), 'BaseData');
-- ********************************
-- TILL_COUNT
-- ********************************
-- Currency
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'CAD_CURRENCY', 'TILL_COUNT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'TILL_COUNT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'CAD_TRAVELERS_CHECK', 'TILL_COUNT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'USD_TRAVELERS_CHECK', 'TILL_COUNT', getDate (), 'BaseData');
-- Checks / Vouchers
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'CHECK', 'TILL_COUNT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'GIFT_CERTIFICATE', 'TILL_COUNT', getDate (), 'BaseData');
INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
VALUES (@intOrganization_ID, 'MALL_CERTIFICATE', 'TILL_COUNT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'STORE_CREDIT', 'TILL_COUNT', getDate (), 'BaseData');
-- Credit / Debit Cards
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'AMERICAN_EXPRESS', 'TILL_COUNT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'DINERS_CLUB', 'TILL_COUNT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'DISCOVER', 'TILL_COUNT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'JCB', 'TILL_COUNT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'MASTERCARD', 'TILL_COUNT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'VISA', 'TILL_COUNT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'DEBIT_CARD', 'TILL_COUNT', getDate (), 'BaseData');
-- Gift / Merchandise Cards
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'MERCHANDISE_CREDIT_CARD', 'TILL_COUNT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ISD_GIFT_CARD', 'TILL_COUNT', getDate (), 'BaseData');
-- Miscellaneous
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ACCOUNT_RECEIVABLE', 'TILL_COUNT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'COUPON', 'TILL_COUNT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'PRIVATE_LABEL', 'TILL_COUNT', getDate (), 'BaseData');
--INSERT INTO tnd_tndr_availability (organization_id, tndr_id, availability_code, create_date, create_user_id)
--VALUES (@intOrganization_ID, 'ROOM_CHARGE', 'TILL_COUNT', getDate (), 'BaseData');

DELETE FROM tnd_tndr_denomination WHERE organization_id = @intOrganization_ID;
--
-- US Currency
--
INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'PENNY', NULL, NULL, NULL, NULL, 'Penny', 0.010000, 1, NULL)
INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'NICKEL', NULL, NULL, NULL, NULL, 'Nickel', 0.050000, 2, NULL)
INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'DIME', NULL, NULL, NULL, NULL, 'Dime', 0.100000, 3, NULL)
INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'QUARTER', NULL, NULL, NULL, NULL, 'Quarter', 0.250000, 4, NULL)
INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'HALF_DOLLAR', NULL, NULL, NULL, NULL, 'Half Dollar Coins', 0.500000, 5, NULL)
INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'DOLLAR_COIN', NULL, NULL, NULL, NULL, 'Dollar Coins', 1.000000, 6, NULL)
INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'ONE_DOLLAR', NULL, NULL, NULL, NULL, 'One Dollar', 1.000000, 7, NULL)
INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'FIVE_DOLLAR', NULL, NULL, NULL, NULL, 'Five Dollar', 5.000000, 8, NULL)
INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'TEN_DOLLAR', NULL, NULL, NULL, NULL, 'Ten Dollar', 10.000000, 9, NULL)
INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'TWENTY_DOLLAR', NULL, NULL, NULL, NULL, 'Twenty Dollar', 20.000000, 10, NULL)
INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'FIFTY_DOLLAR', NULL, NULL, NULL, NULL, 'Fifty Dollar', 50.000000, 11, NULL)
INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'HUNDRED_DOLLAR', NULL, NULL, NULL, NULL, 'Hundred Dollar', 100.000000, 12, NULL)
INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'TWO_DOLLAR', NULL, NULL, NULL, NULL, 'Two Dollar', 2.000000, 13, NULL)
INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'PENNY_ROLLS', NULL, NULL, NULL, NULL, 'Penny Rolls', 0.500000, 14, NULL)
INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'NICKEL_ROLLS', NULL, NULL, NULL, NULL, 'Nickel Rolls', 2.000000, 15, NULL)
INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'DIME_ROLLS', NULL, NULL, NULL, NULL, 'Dime Rolls', 5.000000, 16, NULL)
INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'QUARTER_ROLLS', NULL, NULL, NULL, NULL, 'Quarter Rolls', 10.000000, 17, NULL)

--INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
--VALUES (@intOrganization_ID, 'USD_TRAVELERS_CHECK', 'TEN', NULL, NULL, NULL, NULL, 'Ten', 10.000000, 18, NULL)
INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
VALUES (@intOrganization_ID, 'USD_TRAVELERS_CHECK', 'TWENTY', NULL, NULL, NULL, NULL, 'Twenty', 20.000000, 19, NULL)
INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
VALUES (@intOrganization_ID, 'USD_TRAVELERS_CHECK', 'FIFTY', NULL, NULL, NULL, NULL, 'Fifty', 50.000000, 20, NULL)
INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
VALUES (@intOrganization_ID, 'USD_TRAVELERS_CHECK', 'HUNDRED', NULL, NULL, NULL, NULL, 'One Hundred', 100.000000, 21, NULL)
--INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
--VALUES (@intOrganization_ID, 'USD_TRAVELERS_CHECK', 'FIVEHUNDRED', getDate (), 'BaseData', NULL, NULL, 'Five Hundred', 500.000000, 22, NULL)
--INSERT INTO tnd_tndr_denomination (organization_id, tndr_id, denomination_id, create_date, create_user_id, update_date, update_user_id, description, value, sort_order, record_state)
--VALUES (@intOrganization_ID, 'USD_TRAVELERS_CHECK', 'THOUSAND', getDate (), 'BaseData', NULL, NULL, 'One Thousand', 1000.000000, 23, NULL)
--
-- tnd_tndr_typcode
--
DELETE FROM tnd_tndr_typcode WHERE organization_id = @intOrganization_ID;
INSERT INTO tnd_tndr_typcode (organization_id, tndr_typcode, description, sort_order, unit_count_req_code, create_date, create_user_id, update_date, update_user_id, close_count_disc_threshold, record_state)
VALUES (@intOrganization_ID, 'CHECK', 'Check', 1, 'TOTAL_NORMAL', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO tnd_tndr_typcode (organization_id, tndr_typcode, description, sort_order, unit_count_req_code, create_date, create_user_id, update_date, update_user_id, close_count_disc_threshold, record_state)
VALUES (@intOrganization_ID, 'CREDIT_CARD', 'Credit Card', 2, 'TOTAL_NORMAL', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO tnd_tndr_typcode (organization_id, tndr_typcode, description, sort_order, unit_count_req_code, create_date, create_user_id, update_date, update_user_id, close_count_disc_threshold, record_state)
VALUES (@intOrganization_ID, 'CURRENCY', 'Currency', 0, 'DENOMINATION', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO tnd_tndr_typcode (organization_id, tndr_typcode, description, sort_order, unit_count_req_code, create_date, create_user_id, update_date, update_user_id, close_count_disc_threshold, record_state)
VALUES (@intOrganization_ID, 'TRAVELERS_CHECK', 'Travelers Check', 11, 'TOTAL_NORMAL', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO tnd_tndr_typcode (organization_id, tndr_typcode, description, sort_order, unit_count_req_code, create_date, create_user_id, update_date, update_user_id, close_count_disc_threshold, record_state)
VALUES (@intOrganization_ID, 'VOUCHER', 'Voucher', 8, 'TOTAL_SHORT', NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM tnd_tndr_user_settings WHERE organization_id = @intOrganization_ID;
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'AMERICAN_EXPRESS', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 50.000000, 500.000000, 0.010000, 999999.990000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'CHECK', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 50.000000, 500.000000, 0.010000, 999999.990000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DEBIT_CARD', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 0.010000, 999999.990000, 0.010000, 999999.990000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DINERS_CLUB', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 50.000000, 500.000000, 0.010000, 999999.990000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DISCOVER', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 50.000000, 500.000000, 0.010000, 999999.990000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'GIFT_CERTIFICATE', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', NULL, NULL, 4999.990000, NULL, NULL, 0.010000, 5000.000000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'ISD_GIFT_CARD', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 0.010000, 999999.990000, 0.010000, 500.000000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'ISD_GIFT_ECARD', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 0.010000, 999999.990000, 0.010000, 500.000000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'ISSUE_ISD_GIFT_CARD', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 0.010000, 999999.990000, 0.010000, 500.000000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'ISSUE_MERCHANDISE_CREDIT_CARD', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 0.010000, 999999.990000, 0.010000, 500.000000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'GIFT_CARD', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 0.010000, 999999.990000, 0.010000, 500.000000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'GIFT_ECARD', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 0.010000, 999999.990000, 0.010000, 500.000000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'ISSUE_GIFT_CARD', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 0.010000, 999999.990000, 0.010000, 500.000000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'MALL_CERTIFICATE', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', NULL, NULL, 4999.990000, NULL, NULL, 0.010000, 5000.000000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'MASTERCARD', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 50.000000, 500.000000, 0.010000, 999999.990000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'MERCHANDISE_CREDIT_CARD', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 0.010000, 999999.990000, 0.010000, 500.000000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'RELOAD_ISD_GIFT_CARD', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 0.010000, 999999.990000, 0.010000, 500.000000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'USD_CURRENCY', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', NULL, NULL, 99.990000, NULL, NULL, 0.010000, 999999.990000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'USD_TRAVELERS_CHECK', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', NULL, NULL, 99.990000, NULL, NULL, 0.010000, 999999.990000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'VISA', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 50.000000, 500.000000, 0.010000, 999999.990000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO tnd_tndr_user_settings (organization_id, tndr_id, group_id, usage_code, entry_mthd_code, org_code, org_value, online_floor_approval_amt, online_ceiling_approval_amt, over_tndr_limit, offline_floor_approval_amt, offline_ceiling_approval_amt, min_accept_amt, max_accept_amt, max_refund_with_receipt, max_refund_wo_receipt, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'RELOAD_GIFT_CARD', 'EVERYONE', 'DEFAULT', 'DEFAULT', '*', '*', 0.010000, 999999.990000, 0.000000, 0.010000, 999999.990000, 0.010000, 500.000000, 999999.990000, 999999.990000, getDate (), 'BaseData', NULL, NULL, NULL)

DELETE FROM loc_org_hierarchy WHERE organization_id = @intOrganization_ID AND org_code = '*' AND org_value = '*';
INSERT INTO loc_org_hierarchy (organization_id, org_code, org_value, parent_code, parent_value, description, level_mgr, level_order, sort_order, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, '*', '*', NULL, NULL, 'Charlotte Russe', NULL, 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO loc_org_hierarchy (organization_id, org_code, org_value, parent_code, parent_value, description, level_mgr, level_order, sort_order, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'CHAIN', 'DEFAULT', '*', '*', 'Chain', 'John Doe', 1, 1, getDate (), 'BaseData', NULL, NULL, NULL)

DELETE FROM loc_pricing_hierarchy WHERE organization_id = @intOrganization_ID AND level_code = '*' AND level_value = '*';
--INSERT INTO loc_pricing_hierarchy (organization_id, level_code, level_value, parent_code, parent_value, description, level_order, sort_order, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, '*', '*', NULL, NULL, 'MASTER', 0, 0, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO loc_pricing_hierarchy (organization_id, level_code, level_value, parent_code, parent_value, description, level_order, sort_order, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'CHAIN', 'DEFAULT', '*', '*', 'Chain', 1, 1, getDate (), 'BaseData', NULL, NULL, NULL)

----------------------------------
-- Work Order Script - Start
----------------------------------
DELETE FROM cwo_work_order_category WHERE organization_id = @intOrganization_ID;
--INSERT INTO cwo_work_order_category (organization_id, category_id, sort_order, description, prompt_for_price_code_flag, max_item_count, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'CWO_CAT_1', 10, 'Cleaning', 1, 10, getDate (), 'BaseData')
--INSERT INTO cwo_work_order_category (organization_id, category_id, sort_order, description, prompt_for_price_code_flag, max_item_count, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'CWO_CAT_2', 20, 'Alteration', 1, 10, getDate (), 'BaseData')
--INSERT INTO cwo_work_order_category (organization_id, category_id, sort_order, description, prompt_for_price_code_flag, max_item_count, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'CWO_CAT_3', 30, 'Monogram', 1, 10, getDate (), 'BaseData')

DELETE FROM cwo_price_code WHERE organization_id = @intOrganization_ID;
--  must be there
--INSERT INTO cwo_price_code (organization_id, price_code, description, sort_order, prompt_for_warranty_nbr_flag, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'CHARGEABLE', 'Chargeable', 10, 0, getDate (), 'BaseData');
--  must be there
--INSERT INTO cwo_price_code (organization_id, price_code, description, sort_order, prompt_for_warranty_nbr_flag, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'NONCHARGEABLE', 'No Charge', 20, 0, getDate (), 'BaseData');
--  optional
--INSERT INTO cwo_price_code (organization_id, price_code, description, sort_order, prompt_for_warranty_nbr_flag, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'FULL_WARRANTY', 'Full Warranty', 30, 1, getDate (), 'BaseData');
--  optional
--INSERT INTO cwo_price_code (organization_id, price_code, description, sort_order, prompt_for_warranty_nbr_flag, create_date, create_user_id)
--VALUES(@intOrganization_ID, 'LIMITED_WARRANTY', 'Limited Warranty', 40, 1, getDate (), 'BaseData');
----------------------------------
-- Work Order Script - End
----------------------------------

----------------------------------
-- Receipt Text
----------------------------------
DELETE FROM com_receipt_text WHERE organization_id = @intOrganization_ID;
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'ACCOUNT_RECEIVABLE_TERMS', 'DEFAULT', 0, '*', '*', 'Signer acknowledges receipt of goods and/or services in the amount tendered shown hereon.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'BOUNCE_BACK_2', 'DEFAULT', 0, '*', '*', 'Do not detach coupon from original sales receipt. Coupon must be attached to original receipt for redemption.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'BOUNCE_BACK_3', 'DEFAULT', 0, '*', '*', 'Not redeemable for cash, credit or a gift card. No change will be given, whether in the form of cash, credit, or gift card.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'BOUNCE_BACK_4', 'DEFAULT', 0, '*', '*', 'Coupon cannot be replaced, reissued or combined with any other coupon or promotional offer.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'BOUNCE_BACK_5', 'DEFAULT', 0, '*', '*', 'One coupon per transaction.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'BOUNCE_BACK_6', 'DEFAULT', 0, '*', '*', 'Coupon cannot be applied to employee purchases or for the purchase of gift cards.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'CUSTOMER_COPY_FOOTER', 'DEFAULT', 0, '*', '*', 'Thank you for shopping with us. Come back soon.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'CUSTOMER_COPY_HEADER', 'DEFAULT', 0, '*', '*', 'Welcome to our store!', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'GIFT_CERTIFICATE_FOOTER', 'DEFAULT', 0, '*', '*', '*Conditions may vary. See your local store for details.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'GIFT_CERTIFICATE_HEADER', 'DEFAULT', 0, '*', '*', 'Redeemdable towards the purchase of any in-store merchandise.*', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'LAYAWAY_FOOTER', 'DEFAULT', 0, '*', '*', 'All layaways are subject to termination if regular payments are not made.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'LAYAWAY_HEADER', 'DEFAULT', 0, '*', '*', 'All layaways may be paid in full and picked up any time before the due date.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'LAYAWAY_MERCH_TICKET_FOOTER', 'DEFAULT', 0, '*', '*', 'Place all items on layaway in the appropriate reserved section.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'LAYAWAY_MERCH_TICKET_HEADER', 'DEFAULT', 0, '*', '*', 'Place this ticket with the corresponding merchandise that has been put on layaway.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'LAYAWAY_TERMS', 'DEFAULT', 0, '*', '*', 'I agree to pay the balance owed as outlined in the payment schedule. I have received a copy of the Layaway Agreement policy and terms and I agree to comply with them.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'ORDER_FOOTER', 'DEFAULT', 0, '*', '*', 'Want that new TV, but you do not want to drive all over town looking for it? We can locate it for you in any of our stores and have it ready for pickup in the one that you choose.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'ORDER_HEADER', 'DEFAULT', 0, '*', '*', 'Thank you for using us for all of your ordering needs.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'ORDER_TERMS', 'DEFAULT', 0, '*', '*', 'The fulfillment of all orders is subject to merchandise availability. We do our best to ensure the accuracy of the information that is communicated to our customers, but availability of merchandise in stores is an estimate and is not guaranteed.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'PAYROLL_DEDUCTION_TERMS', 'DEFAULT', 0, '*', '*', 'Associate acknowledges receipt of goods and/or services in the amount tendered shown hereon and authorizes payroll deduction of this amount.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'REBATE_FOOTER', 'DEFAULT', 0, '*', '*', 'Not valid for merchandise return. Rebate may be subject to additional terms and regulations in some states.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'REBATE_HEADER', 'DEFAULT', 0, '*', '*', 'Rebates must be postmarked within 10 days of the original date of purchase.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'SALE_DISCLAIMER', 'DEFAULT', 0, '*', '*', 'Merchandise cannot be returned if it has been opened or used.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'SEND_SALE_FOOTER', 'DEFAULT', 0, '*', '*', 'If any changes are required regarding the delivery of your merchandise, contact your local store.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'SEND_SALE_HEADER', 'DEFAULT', 0, '*', '*', 'Thank for you scheduling your delivery with us.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'SEND_SALE_MERCH_TICKET_FOOTER', 'DEFAULT', 0, '*', '*', 'Place the merchandise in the staging area to be loaded onto the truck for delivery.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'SEND_SALE_MERCH_TICKET_HEADER', 'DEFAULT', 0, '*', '*', 'Verify that the merchandise is being sent to the correct address.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'SEND_SALE_TERMS', 'DEFAULT', 0, '*', '*', 'All merchandise will be delivered during the allotted time period. Drivers will contact you approximately twenty minutes before arriving to let you know when within that period your merchandise will be delivered.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'SPECIAL_ORDER_FOOTER', 'DEFAULT', 0, '*', '*', 'Item availability date is provided solely as an estimate. The actual availability date may vary.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'SPECIAL_ORDER_HEADER', 'DEFAULT', 0, '*', '*', 'Items placed on special order may be picked up or sent directly to you.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'SPECIAL_ORDER_TERMS', 'DEFAULT', 0, '*', '*', 'When the customer receives the special order items, the customer must immediately and carefully inspect the merchandise for correctness, completeness and for any possible damage. Any problems must be reported within five days of receiving the merchandise. Any problems not reported within five days will not be the responsibility of the store to correct. I have read these terms and I agree to comply with them.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'STORE_COPY_FOOTER', 'DEFAULT', 0, '*', '*', 'Keep receipts in transaction order.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'STORE_COPY_HEADER', 'DEFAULT', 0, '*', '*', 'Place receipt in drawer.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'TAX_EXEMPTION_DISCLAIMER', 'DEFAULT', 0, '*', '*', 'It is unlawful to use the tax exemption status of another person or organization for your own personal benefit. Violators will be prosecuted.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'TENDER_CREDIT_TERMS', 'DEFAULT', 0, '*', '*', 'I agree to pay the above amount according to my card holder agreement.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'WORK_ORDER_FOOTER', 'DEFAULT', 0, '*', '*', 'In the event of a delay in completing your work order, you will be notified immediately.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'WORK_ORDER_HEADER', 'DEFAULT', 0, '*', '*', 'Items not picked up after 90 days will be forfeited.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'WORK_ORDER_MERCH_TICKET_FOOTER', 'DEFAULT', 0, '*', '*', 'Keep items that are going to the same vendor together to ensure efficient processing.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'WORK_ORDER_MERCH_TICKET_HEADER', 'DEFAULT', 0, '*', '*', 'Place this ticket with the corresponding merchandise that is to be repaired or altered.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
--INSERT INTO com_receipt_text (organization_id, text_code, text_subcode, text_seq, org_code, org_value, receipt_text, effective_date, expiration_date, reformat_flag, line_format, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES(@intOrganization_ID, 'WORK_ORDER_TERMS', 'DEFAULT', 0, '*', '*', 'This store is not liable for loss, theft or damage to these items while they are in our possession.\n \nCustomer agrees to pick up merchandise at the prescribed time or within 14 days of being notified the merchandise is available.\n \nCustomer agrees to pay in full at the time of pickup.', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL)
----------------------------------
-- Receipt Text
----------------------------------

-------------------
-- Gift cards
-------------------
DELETE FROM itm_item WHERE organization_id = @intOrganization_ID AND item_id IN ('700001', '700002','700003', '700004');
DELETE FROM itm_non_phys_item WHERE organization_id = @intOrganization_ID AND item_id IN ('700001', '700002','700003', '700004');

INSERT INTO itm_item (ORGANIZATION_ID, ITEM_ID, ORG_CODE, ORG_VALUE, NAME, DESCRIPTION, DEPARTMENT_ID, SUBDEPARTMENT_ID, CLASS_ID, SUBCLASS_ID, ITEM_URL, UNIT_COST, CURR_SALE_PRICE, LIST_PRICE, UNIT_OF_MEASURE_CODE, COMPARE_AT_PRICE, MIN_SALE_UNIT_COUNT, MAX_SALE_UNIT_COUNT, ITEM_AVAILABILITY_CODE, DISALLOW_DISCOUNTS_FLAG, PROMPT_FOR_QUANTITY_FLAG, PROMPT_FOR_PRICE_FLAG, PROMPT_FOR_WEIGHT_FLAG, PROMPT_FOR_DESCRIPTION_FLAG, FORCE_QUANTITY_OF_ONE_FLAG, NOT_RETURNABLE_FLAG, NO_GIVEAWAYS_FLAG, ITEM_LVLCODE, PARENT_ITEM_ID, ATTACHED_ITEMS_FLAG, NOT_INVENTORIED_FLAG, SERIALIZED_ITEM_FLAG, ITEM_TYPCODE, SUBSTITUTE_AVAILABLE_FLAG, TAX_GROUP_ID, DTV_CLASS_NAME, MESSAGES_FLAG, VENDOR, SEASON_CODE, PART_NUMBER, QTY_SCALE, RESTOCKING_FEE, SPECIAL_ORDER_LEAD_DAYS, APPLY_RESTOCKING_FEE_FLAG, DISALLOW_SEND_SALE_FLAG, DISALLOW_PRICE_CHANGE_FLAG, DISALLOW_LAYAWAY_FLAG, DISALLOW_SPECIAL_ORDER_FLAG, DISALLOW_WORK_ORDER_FLAG, DISALLOW_REMOTE_SEND_FLAG, DISALLOW_COMMISSION_FLAG, WARRANTY_FLAG, GENERIC_ITEM_FLAG, MIN_AGE_REQUIRED, RESTRICTION_CATEGORY, INITIAL_SALE_QTY, DISPOSITION_CODE, FOODSTAMP_ELIGIBLE_FLAG, STOCK_STATUS, PROMPT_FOR_CUSTOMER, SHIPPING_WEIGHT, DISALLOW_ORDER_FLAG, DISALLOW_DEALS_FLAG, DIMENSION_SYSTEM, pack_size, default_source_type, default_source_id, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, RECORD_STATE)
VALUES (@intOrganization_ID, '700001', '*', '*', 'Issue Gift Card', 'Issue Gift Card', 'NP', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1.0000, 9999.0000, 'AVAILABLE', 1, 0, 1, 0, 0, 1, 1, 0, 'ITEM', NULL, 0, 1, 0, 'NON_PHYSICAL', 0, 'N', 'dtv.xst.dao.itm.impl.NonPhysicalItem', 0, NULL, NULL, NULL, 1, 0.000000, NULL, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO itm_non_phys_item (organization_id, item_id, org_code, org_value, display_order, non_phys_item_typcode, non_phys_item_subtype, exclude_from_net_sales_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, '700001', '*', '*', 10, 'VOUCHER', 'ISD_GIFT_CARD', 1, getDate (), 'BaseData', NULL, NULL, NULL)

INSERT INTO itm_item (ORGANIZATION_ID, ITEM_ID, ORG_CODE, ORG_VALUE, NAME, DESCRIPTION, DEPARTMENT_ID, SUBDEPARTMENT_ID, CLASS_ID, SUBCLASS_ID, ITEM_URL, UNIT_COST, CURR_SALE_PRICE, LIST_PRICE, UNIT_OF_MEASURE_CODE, COMPARE_AT_PRICE, MIN_SALE_UNIT_COUNT, MAX_SALE_UNIT_COUNT, ITEM_AVAILABILITY_CODE, DISALLOW_DISCOUNTS_FLAG, PROMPT_FOR_QUANTITY_FLAG, PROMPT_FOR_PRICE_FLAG, PROMPT_FOR_WEIGHT_FLAG, PROMPT_FOR_DESCRIPTION_FLAG, FORCE_QUANTITY_OF_ONE_FLAG, NOT_RETURNABLE_FLAG, NO_GIVEAWAYS_FLAG, ITEM_LVLCODE, PARENT_ITEM_ID, ATTACHED_ITEMS_FLAG, NOT_INVENTORIED_FLAG, SERIALIZED_ITEM_FLAG, ITEM_TYPCODE, SUBSTITUTE_AVAILABLE_FLAG, TAX_GROUP_ID, DTV_CLASS_NAME, MESSAGES_FLAG, VENDOR, SEASON_CODE, PART_NUMBER, QTY_SCALE, RESTOCKING_FEE, SPECIAL_ORDER_LEAD_DAYS, APPLY_RESTOCKING_FEE_FLAG, DISALLOW_SEND_SALE_FLAG, DISALLOW_PRICE_CHANGE_FLAG, DISALLOW_LAYAWAY_FLAG, DISALLOW_SPECIAL_ORDER_FLAG, DISALLOW_WORK_ORDER_FLAG, DISALLOW_REMOTE_SEND_FLAG, DISALLOW_COMMISSION_FLAG, WARRANTY_FLAG, GENERIC_ITEM_FLAG, MIN_AGE_REQUIRED, RESTRICTION_CATEGORY, INITIAL_SALE_QTY, DISPOSITION_CODE, FOODSTAMP_ELIGIBLE_FLAG, STOCK_STATUS, PROMPT_FOR_CUSTOMER, SHIPPING_WEIGHT, DISALLOW_ORDER_FLAG, DISALLOW_DEALS_FLAG, DIMENSION_SYSTEM, pack_size, default_source_type, default_source_id, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, RECORD_STATE)
VALUES (@intOrganization_ID, '700002', '*', '*', 'Reload Gift Card', 'Reload Gift Card', 'NP', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1.0000, 9999.0000, 'AVAILABLE', 1, 0, 1, 0, 0, 1, 1, 0, 'ITEM', NULL, 0, 1, 0, 'NON_PHYSICAL', 0, 'N', 'dtv.xst.dao.itm.impl.NonPhysicalItem', 0, NULL, NULL, NULL, 1, 0.000000, NULL, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO itm_non_phys_item (organization_id, item_id, org_code, org_value, display_order, non_phys_item_typcode, non_phys_item_subtype, exclude_from_net_sales_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, '700002', '*', '*', 20, 'VOUCHER', 'ISD_GIFT_CARD', 1, getDate (), 'BaseData', NULL, NULL, NULL)

INSERT INTO itm_item (ORGANIZATION_ID, ITEM_ID, ORG_CODE, ORG_VALUE, NAME, DESCRIPTION, DEPARTMENT_ID, SUBDEPARTMENT_ID, CLASS_ID, SUBCLASS_ID, ITEM_URL, UNIT_COST, CURR_SALE_PRICE, LIST_PRICE, UNIT_OF_MEASURE_CODE, COMPARE_AT_PRICE, MIN_SALE_UNIT_COUNT, MAX_SALE_UNIT_COUNT, ITEM_AVAILABILITY_CODE, DISALLOW_DISCOUNTS_FLAG, PROMPT_FOR_QUANTITY_FLAG, PROMPT_FOR_PRICE_FLAG, PROMPT_FOR_WEIGHT_FLAG, PROMPT_FOR_DESCRIPTION_FLAG, FORCE_QUANTITY_OF_ONE_FLAG, NOT_RETURNABLE_FLAG, NO_GIVEAWAYS_FLAG, ITEM_LVLCODE, PARENT_ITEM_ID, ATTACHED_ITEMS_FLAG, NOT_INVENTORIED_FLAG, SERIALIZED_ITEM_FLAG, ITEM_TYPCODE, SUBSTITUTE_AVAILABLE_FLAG, TAX_GROUP_ID, DTV_CLASS_NAME, MESSAGES_FLAG, VENDOR, SEASON_CODE, PART_NUMBER, QTY_SCALE, RESTOCKING_FEE, SPECIAL_ORDER_LEAD_DAYS, APPLY_RESTOCKING_FEE_FLAG, DISALLOW_SEND_SALE_FLAG, DISALLOW_PRICE_CHANGE_FLAG, DISALLOW_LAYAWAY_FLAG, DISALLOW_SPECIAL_ORDER_FLAG, DISALLOW_WORK_ORDER_FLAG, DISALLOW_REMOTE_SEND_FLAG, DISALLOW_COMMISSION_FLAG, WARRANTY_FLAG, GENERIC_ITEM_FLAG, MIN_AGE_REQUIRED, RESTRICTION_CATEGORY, INITIAL_SALE_QTY, DISPOSITION_CODE, FOODSTAMP_ELIGIBLE_FLAG, STOCK_STATUS, PROMPT_FOR_CUSTOMER, SHIPPING_WEIGHT, DISALLOW_ORDER_FLAG, DISALLOW_DEALS_FLAG, DIMENSION_SYSTEM, pack_size, default_source_type, default_source_id, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, RECORD_STATE)
VALUES (@intOrganization_ID, '700003', '*', '*', 'Issue AJB Gift Card', 'Issue AJB Gift Card', 'NP', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1.0000, 9999.0000, 'AVAILABLE', 1, 0, 1, 0, 0, 1, 1, 0, 'ITEM', NULL, 0, 1, 0, 'NON_PHYSICAL', 0, 'N', 'dtv.xst.dao.itm.impl.NonPhysicalItem', 0, NULL, NULL, NULL, 1, 0.000000, NULL, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO itm_non_phys_item (organization_id, item_id, org_code, org_value, display_order, non_phys_item_typcode, non_phys_item_subtype, exclude_from_net_sales_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, '700003', '*', '*', 30, 'VOUCHER', 'AJB_GIFT_CARD', 1, getDate (), 'BaseData', NULL, NULL, NULL)

INSERT INTO itm_item (ORGANIZATION_ID, ITEM_ID, ORG_CODE, ORG_VALUE, NAME, DESCRIPTION, DEPARTMENT_ID, SUBDEPARTMENT_ID, CLASS_ID, SUBCLASS_ID, ITEM_URL, UNIT_COST, CURR_SALE_PRICE, LIST_PRICE, UNIT_OF_MEASURE_CODE, COMPARE_AT_PRICE, MIN_SALE_UNIT_COUNT, MAX_SALE_UNIT_COUNT, ITEM_AVAILABILITY_CODE, DISALLOW_DISCOUNTS_FLAG, PROMPT_FOR_QUANTITY_FLAG, PROMPT_FOR_PRICE_FLAG, PROMPT_FOR_WEIGHT_FLAG, PROMPT_FOR_DESCRIPTION_FLAG, FORCE_QUANTITY_OF_ONE_FLAG, NOT_RETURNABLE_FLAG, NO_GIVEAWAYS_FLAG, ITEM_LVLCODE, PARENT_ITEM_ID, ATTACHED_ITEMS_FLAG, NOT_INVENTORIED_FLAG, SERIALIZED_ITEM_FLAG, ITEM_TYPCODE, SUBSTITUTE_AVAILABLE_FLAG, TAX_GROUP_ID, DTV_CLASS_NAME, MESSAGES_FLAG, VENDOR, SEASON_CODE, PART_NUMBER, QTY_SCALE, RESTOCKING_FEE, SPECIAL_ORDER_LEAD_DAYS, APPLY_RESTOCKING_FEE_FLAG, DISALLOW_SEND_SALE_FLAG, DISALLOW_PRICE_CHANGE_FLAG, DISALLOW_LAYAWAY_FLAG, DISALLOW_SPECIAL_ORDER_FLAG, DISALLOW_WORK_ORDER_FLAG, DISALLOW_REMOTE_SEND_FLAG, DISALLOW_COMMISSION_FLAG, WARRANTY_FLAG, GENERIC_ITEM_FLAG, MIN_AGE_REQUIRED, RESTRICTION_CATEGORY, INITIAL_SALE_QTY, DISPOSITION_CODE, FOODSTAMP_ELIGIBLE_FLAG, STOCK_STATUS, PROMPT_FOR_CUSTOMER, SHIPPING_WEIGHT, DISALLOW_ORDER_FLAG, DISALLOW_DEALS_FLAG, DIMENSION_SYSTEM, pack_size, default_source_type, default_source_id, CREATE_DATE, CREATE_USER_ID, UPDATE_DATE, UPDATE_USER_ID, RECORD_STATE)
VALUES (@intOrganization_ID, '700004', '*', '*', 'Reload AJB Gift Card', 'Reload AJB Gift Card', 'NP', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1.0000, 9999.0000, 'AVAILABLE', 1, 0, 1, 0, 0, 1, 1, 0, 'ITEM', NULL, 0, 1, 0, 'NON_PHYSICAL', 0, 'N', 'dtv.xst.dao.itm.impl.NonPhysicalItem', 0, NULL, NULL, NULL, 1, 0.000000, NULL, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO itm_non_phys_item (organization_id, item_id, org_code, org_value, display_order, non_phys_item_typcode, non_phys_item_subtype, exclude_from_net_sales_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, '700004', '*', '*', 40, 'VOUCHER', 'AJB_GIFT_CARD', 1, getDate (), 'BaseData', NULL, NULL, NULL)


-------------------
-- Gift cards
-------------------

------------------------------
-- DISCOUNTS
------------------------------
DELETE FROM dsc_discount WHERE organization_id = @intOrganization_ID AND discount_code = 'AWARD_DISCOUNT';
INSERT INTO dsc_discount (organization_id, discount_code, org_code, org_value, effective_datetime, expr_datetime, typcode, app_mthd_code, percentage, description, calculation_mthd_code, prompt, sound, max_trans_count, exclusive_discount_flag, privilege_type, discount, dtv_class_name, min_eligible_price, serialized_discount_flag, taxability_code, max_discount, sort_order, disallow_change_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'AWARD_DISCOUNT', '*', '*', '20120101', NULL, 'AWARD', 'TRANSACTION', NULL, 'Redeemed Awards', 'AMOUNT', NULL, NULL, NULL, 0, NULL, NULL, 'dtv.xst.dao.dsc.impl.Discount', NULL, 0, NULL, NULL, NULL, 1, getDate (), 'BaseData', NULL, NULL, NULL)
--
-- "DEFAULT" Customer
--
DELETE FROM dsc_discount WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-T1';
DELETE FROM dsc_discount_group_mapping WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-T1';
INSERT INTO dsc_discount (organization_id, discount_code, org_code, org_value, effective_datetime, expr_datetime, typcode, app_mthd_code, percentage, description, calculation_mthd_code, prompt, sound, max_trans_count, exclusive_discount_flag, privilege_type, discount, dtv_class_name, min_eligible_price, serialized_discount_flag, taxability_code, max_discount, sort_order, disallow_change_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DISC-T1', '*', '*', '20120101', NULL, 'DISCOUNT', 'TRANSACTION', 0.1000, 'Military 10% Off', 'PERCENT', NULL, NULL, NULL, 1, NULL, NULL, 'dtv.xst.dao.dsc.impl.Discount', NULL, 0, NULL, NULL, NULL, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO dsc_discount_group_mapping (organization_id, cust_group_id, discount_code, org_code, org_value, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DEFAULT', 'DISC-T1', '*', '*', getDate (), 'BaseData', NULL, NULL, NULL)
--
DELETE FROM dsc_discount WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-T2';
DELETE FROM dsc_discount_group_mapping WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-T2';
INSERT INTO dsc_discount (organization_id, discount_code, org_code, org_value, effective_datetime, expr_datetime, typcode, app_mthd_code, percentage, description, calculation_mthd_code, prompt, sound, max_trans_count, exclusive_discount_flag, privilege_type, discount, dtv_class_name, min_eligible_price, serialized_discount_flag, taxability_code, max_discount, sort_order, disallow_change_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DISC-T2', '*', '*', '20120101', NULL, 'DISCOUNT', 'TRANSACTION', 0.1000, 'Student 10% Off', 'PERCENT', NULL, NULL, NULL, 1, NULL, NULL, 'dtv.xst.dao.dsc.impl.Discount', NULL, 0, NULL, NULL, NULL, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO dsc_discount_group_mapping (organization_id, cust_group_id, discount_code, org_code, org_value, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DEFAULT', 'DISC-T2', '*', '*', getDate (), 'BaseData', NULL, NULL, NULL)
--
DELETE FROM dsc_discount WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-L1';
DELETE FROM dsc_discount_group_mapping WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-L1';
INSERT INTO dsc_discount (organization_id, discount_code, org_code, org_value, effective_datetime, expr_datetime, typcode, app_mthd_code, percentage, description, calculation_mthd_code, prompt, sound, max_trans_count, exclusive_discount_flag, privilege_type, discount, dtv_class_name, min_eligible_price, serialized_discount_flag, taxability_code, max_discount, sort_order, disallow_change_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DISC-L1', '*', '*', '20120101', NULL, 'DISCOUNT', 'LINE_ITEM', 0.5000, 'Last Pair 50% Off', 'PERCENT', NULL, NULL, NULL, 1, NULL, NULL, 'dtv.xst.dao.dsc.impl.Discount', NULL, 0, NULL, NULL, NULL, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO dsc_discount_group_mapping (organization_id, cust_group_id, discount_code, org_code, org_value, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DEFAULT', 'DISC-L1', '*', '*', getDate (), 'BaseData', NULL, NULL, NULL)
--
DELETE FROM dsc_discount WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-L2';
DELETE FROM dsc_discount_group_mapping WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-L2';
INSERT INTO dsc_discount (organization_id, discount_code, org_code, org_value, effective_datetime, expr_datetime, typcode, app_mthd_code, percentage, description, calculation_mthd_code, prompt, sound, max_trans_count, exclusive_discount_flag, privilege_type, discount, dtv_class_name, min_eligible_price, serialized_discount_flag, taxability_code, max_discount, sort_order, disallow_change_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DISC-L2', '*', '*', '20120101', NULL, 'DISCOUNT', 'LINE_ITEM', NULL, 'Accommodation $ Off', 'PROMPT_AMOUNT', NULL, NULL, NULL, 1, 'MANAGER_DISCOUNT', NULL, 'dtv.xst.dao.dsc.impl.Discount', NULL, 0, NULL, NULL, NULL, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO dsc_discount_group_mapping (organization_id, cust_group_id, discount_code, org_code, org_value, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DEFAULT', 'DISC-L2', '*', '*', getDate (), 'BaseData', NULL, NULL, NULL)
--
DELETE FROM dsc_discount WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-L3';
DELETE FROM dsc_discount_group_mapping WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-L3';
INSERT INTO dsc_discount (organization_id, discount_code, org_code, org_value, effective_datetime, expr_datetime, typcode, app_mthd_code, percentage, description, calculation_mthd_code, prompt, sound, max_trans_count, exclusive_discount_flag, privilege_type, discount, dtv_class_name, min_eligible_price, serialized_discount_flag, taxability_code, max_discount, sort_order, disallow_change_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DISC-L3', '*', '*', '20120101', NULL, 'DISCOUNT', 'LINE_ITEM', NULL, 'Accommodation - Prompt Price', 'PROMPT_NEW_PRICE', NULL, NULL, NULL, 1, 'MANAGER_DISCOUNT', NULL, 'dtv.xst.dao.dsc.impl.Discount', NULL, 0, NULL, NULL, NULL, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO dsc_discount_group_mapping (organization_id, cust_group_id, discount_code, org_code, org_value, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DEFAULT', 'DISC-L3', '*', '*', getDate (), 'BaseData', NULL, NULL, NULL)
--
DELETE FROM dsc_discount WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-L4';
DELETE FROM dsc_discount_group_mapping WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-L4';
INSERT INTO dsc_discount (organization_id, discount_code, org_code, org_value, effective_datetime, expr_datetime, typcode, app_mthd_code, percentage, description, calculation_mthd_code, prompt, sound, max_trans_count, exclusive_discount_flag, privilege_type, discount, dtv_class_name, min_eligible_price, serialized_discount_flag, taxability_code, max_discount, sort_order, disallow_change_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DISC-L4', '*', '*', '20120101', NULL, 'DISCOUNT', 'LINE_ITEM', 0.1000, 'Military 10% Off', 'PERCENT', NULL, NULL, NULL, 1, NULL, NULL, 'dtv.xst.dao.dsc.impl.Discount', NULL, 0, NULL, NULL, NULL, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO dsc_discount_group_mapping (organization_id, cust_group_id, discount_code, org_code, org_value, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DEFAULT', 'DISC-L4', '*', '*', getDate (), 'BaseData', NULL, NULL, NULL)
--
DELETE FROM dsc_discount WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-L5';
DELETE FROM dsc_discount_group_mapping WHERE organization_id = @intOrganization_ID AND discount_code = 'DISC-L5';
INSERT INTO dsc_discount (organization_id, discount_code, org_code, org_value, effective_datetime, expr_datetime, typcode, app_mthd_code, percentage, description, calculation_mthd_code, prompt, sound, max_trans_count, exclusive_discount_flag, privilege_type, discount, dtv_class_name, min_eligible_price, serialized_discount_flag, taxability_code, max_discount, sort_order, disallow_change_flag, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DISC-L5', '*', '*', '20120101', NULL, 'DISCOUNT', 'LINE_ITEM', 0.1000, 'Student 10% Off', 'PERCENT', NULL, NULL, NULL, 1, NULL, NULL, 'dtv.xst.dao.dsc.impl.Discount', NULL, 0, NULL, NULL, NULL, 0, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO dsc_discount_group_mapping (organization_id, cust_group_id, discount_code, org_code, org_value, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'DEFAULT', 'DISC-L5', '*', '*', getDate (), 'BaseData', NULL, NULL, NULL)
--
-- Employee
--
DELETE FROM dsc_discount WHERE organization_id = @intOrganization_ID AND discount_code = 'EMP-T1';
DELETE FROM dsc_discount_group_mapping WHERE organization_id = @intOrganization_ID AND discount_code = 'EMP-T1';
--INSERT INTO dsc_discount (organization_id, discount_code, org_code, org_value, effective_datetime, expr_datetime, typcode, app_mthd_code, percentage, description, calculation_mthd_code, prompt, sound, max_trans_count, exclusive_discount_flag, privilege_type, discount, dtv_class_name, min_eligible_price, serialized_discount_flag, taxability_code, max_discount, sort_order, disallow_change_flag, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'EMP-T1', '*', '*', '20120101', NULL, 'DISCOUNT', 'TRANSACTION', 0.4000, 'Employee 40% Off', 'PERCENT', NULL, NULL, NULL, 1, NULL, NULL, 'dtv.xst.dao.dsc.impl.Discount', NULL, 0, NULL, NULL, NULL, 0, getDate (), 'BaseData', NULL, NULL, NULL)
--INSERT INTO dsc_discount_group_mapping (organization_id, cust_group_id, discount_code, org_code, org_value, create_date, create_user_id, update_date, update_user_id, record_state)
--VALUES (@intOrganization_ID, 'EMPLOYEE', 'EMP-T1', '*', '*', getDate (), 'BaseData', NULL, NULL, NULL)
------------------------------
-- DISCOUNTS
------------------------------
----------------------------------
-- Employee deals
----------------------------------
DELETE FROM prc_deal WHERE organization_id = @intOrganization_ID AND deal_id IN ('EMP_CLR_20', 'EMP_REG_40', 'EMP_CLR_SPECIAL', 'EMP_REG_SPECIAL');
INSERT INTO prc_deal (organization_id, deal_id, description, act_deferred, effective_date, end_date, start_time, end_time, generosity_cap, iteration_cap, priority_nudge, subtotal_min, subtotal_max, trwide_action, trwide_amount, taxability_code, org_code, org_value, promotion_id, higher_nonaction_amt_flag, exclude_price_override_flag, exclude_discounted_flag, create_date, create_user_id, update_date, update_user_id, record_state, targeted_flag)
VALUES (@intOrganization_ID, 'EMP_CLR_20', '20% Emp. Discount (Clearance)', 0, NULL, NULL, NULL, NULL, -1.000000, 99999, 20, NULL, NULL, NULL, NULL, NULL, '*', '*', NULL, 0, 1, 0, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO prc_deal (organization_id, deal_id, description, act_deferred, effective_date, end_date, start_time, end_time, generosity_cap, iteration_cap, priority_nudge, subtotal_min, subtotal_max, trwide_action, trwide_amount, taxability_code, org_code, org_value, promotion_id, higher_nonaction_amt_flag, exclude_price_override_flag, exclude_discounted_flag, create_date, create_user_id, update_date, update_user_id, record_state, targeted_flag)
VALUES (@intOrganization_ID, 'EMP_REG_40', '40% Emp. Discount', 0, NULL, NULL, NULL, NULL, -1.000000, 99999, 20, NULL, NULL, NULL, NULL, NULL, '*', '*', NULL, 0, 1, 0, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO prc_deal (organization_id, deal_id, description, act_deferred, effective_date, end_date, start_time, end_time, generosity_cap, iteration_cap, priority_nudge, subtotal_min, subtotal_max, trwide_action, trwide_amount, taxability_code, org_code, org_value, promotion_id, higher_nonaction_amt_flag, exclude_price_override_flag, exclude_discounted_flag, create_date, create_user_id, update_date, update_user_id, record_state, targeted_flag)
VALUES (@intOrganization_ID, 'EMP_CLR_SPECIAL', '60% Emp. Discount (Clearance)', 0, '2012-01-01', '2012-01-01', NULL, NULL, -1.000000, 99999, 20, NULL, NULL, NULL, NULL, NULL, '*', '*', NULL, 0, 1, 0, getDate (), 'BaseData', NULL, NULL, NULL, 0)
INSERT INTO prc_deal (organization_id, deal_id, description, act_deferred, effective_date, end_date, start_time, end_time, generosity_cap, iteration_cap, priority_nudge, subtotal_min, subtotal_max, trwide_action, trwide_amount, taxability_code, org_code, org_value, promotion_id, higher_nonaction_amt_flag, exclude_price_override_flag, exclude_discounted_flag, create_date, create_user_id, update_date, update_user_id, record_state, targeted_flag)
VALUES (@intOrganization_ID, 'EMP_REG_SPECIAL', '60% Emp. Discount', 0, '2012-01-01', '2012-01-01', NULL, NULL, -1.000000, 99999, 20, NULL, NULL, NULL, NULL, NULL, '*', '*', NULL, 0, 1, 0, getDate (), 'BaseData', NULL, NULL, NULL, 0)

DELETE FROM prc_deal_trig WHERE organization_id = @intOrganization_ID AND deal_id IN ('EMP_CLR_20', 'EMP_REG_40', 'EMP_CLR_SPECIAL', 'EMP_REG_SPECIAL');
INSERT INTO prc_deal_trig (organization_id, deal_id, deal_trigger, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'EMP_CLR_20', 'CUSTGROUP:EMPLOYEE', getDate (), 'BaseData', NULL, NULL, NULL);
INSERT INTO prc_deal_trig (organization_id, deal_id, deal_trigger, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'EMP_REG_40', 'CUSTGROUP:EMPLOYEE', getDate (), 'BaseData', NULL, NULL, NULL);
INSERT INTO prc_deal_trig (organization_id, deal_id, deal_trigger, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'EMP_CLR_SPECIAL', 'CUSTGROUP:EMPLOYEE', getDate (), 'BaseData', NULL, NULL, NULL);
INSERT INTO prc_deal_trig (organization_id, deal_id, deal_trigger, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'EMP_REG_SPECIAL', 'CUSTGROUP:EMPLOYEE', getDate (), 'BaseData', NULL, NULL, NULL);

DELETE FROM prc_deal_field_test WHERE organization_id = @intOrganization_ID AND deal_id IN ('EMP_CLR_20', 'EMP_REG_40',  'EMP_CLR_SPECIAL', 'EMP_REG_SPECIAL');
INSERT INTO prc_deal_field_test (organization_id, deal_id, item_ordinal, item_condition_group, item_condition_seq, item_field, match_rule, value1, value2, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'EMP_CLR_20', 1, 1, 1, 'ITEM_STOCK_STATUS', 'EQUAL', 'CLEARANCE', NULL, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO prc_deal_field_test (organization_id, deal_id, item_ordinal, item_condition_group, item_condition_seq, item_field, match_rule, value1, value2, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'EMP_REG_40', 1, 1, 1, 'ITEM_STOCK_STATUS', 'NOT_EQUAL', 'CLEARANCE', NULL, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO prc_deal_field_test (organization_id, deal_id, item_ordinal, item_condition_group, item_condition_seq, item_field, match_rule, value1, value2, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'EMP_CLR_SPECIAL', 1, 1, 1, 'ITEM_STOCK_STATUS', 'EQUAL', 'CLEARANCE', NULL, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO prc_deal_field_test (organization_id, deal_id, item_ordinal, item_condition_group, item_condition_seq, item_field, match_rule, value1, value2, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'EMP_REG_SPECIAL', 1, 1, 1, 'ITEM_STOCK_STATUS', 'NOT_EQUAL', 'CLEARANCE', NULL, getDate (), 'BaseData', NULL, NULL, NULL)

DELETE FROM prc_deal_item WHERE organization_id = @intOrganization_ID AND deal_id IN ('EMP_CLR_20', 'EMP_REG_40',  'EMP_CLR_SPECIAL', 'EMP_REG_SPECIAL');
INSERT INTO prc_deal_item (organization_id, deal_id, item_ordinal, consumable, field_sub, sub_percent_change, sub_currency_change, qty_min, qty_max, min_item_total, deal_action, action_arg, action_arg_qty, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'EMP_CLR_20', 1, 1, NULL, NULL, NULL, 1.0000, 1.0000, NULL, 'PERCENT_OFF', 20.000000, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO prc_deal_item (organization_id, deal_id, item_ordinal, consumable, field_sub, sub_percent_change, sub_currency_change, qty_min, qty_max, min_item_total, deal_action, action_arg, action_arg_qty, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'EMP_REG_40', 1, 1, NULL, NULL, NULL, 1.0000, 1.0000, NULL, 'PERCENT_OFF', 40.000000, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO prc_deal_item (organization_id, deal_id, item_ordinal, consumable, field_sub, sub_percent_change, sub_currency_change, qty_min, qty_max, min_item_total, deal_action, action_arg, action_arg_qty, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'EMP_CLR_SPECIAL', 1, 1, NULL, NULL, NULL, 1.0000, 1.0000, NULL, 'PERCENT_OFF', 60.000000, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
INSERT INTO prc_deal_item (organization_id, deal_id, item_ordinal, consumable, field_sub, sub_percent_change, sub_currency_change, qty_min, qty_max, min_item_total, deal_action, action_arg, action_arg_qty, create_date, create_user_id, update_date, update_user_id, record_state)
VALUES (@intOrganization_ID, 'EMP_REG_SPECIAL', 1, 1, NULL, NULL, NULL, 1.0000, 1.0000, NULL, 'PERCENT_OFF', 60.000000, NULL, getDate (), 'BaseData', NULL, NULL, NULL)
----------------------------------
-- Employee deals
----------------------------------

-------------------------------------------
-- Keep this at the end of the file.
-------------------------------------------
GO