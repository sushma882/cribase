PRINT '$Id: CRITestData.sql 606 2013-03-01 15:00:14Z dtvdomain\anayar $';
PRINT '$URL: https://node1000.gspann.local:8443/svn/20181115-cri/branches/fipay_cust_name/pos/database/CRITestData.sql $';

-- ************************************************************************************************
--
-- This script should contain dummy data used only in a test environment such as items, deals and
-- customers.
--
-- ************************************************************************************************

DECLARE @intOrganization_ID INT;
SET @intOrganization_ID = $(OrgID);
DECLARE @strCountry_ID VARCHAR(2);
SET @strCountry_ID = $(CountryID);
DECLARE @intStore_ID INT;
SET @intStore_ID = $(StoreID);
DECLARE @strCurrency_ID VARCHAR(3);
SET @strCurrency_ID = $(CurrencyID);

DECLARE @Price MONEY;

GO

