//$Id: IsdCheckRequest.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.tenderauth.impl.isd.check;

import static dtv.util.StringUtils.isEmpty;
import static dtv.util.StringUtils.nonNull;

import java.util.List;

import imsformat.check.CKRequest;

import dtv.pos.common.LocationFactory;
import dtv.pos.iframework.type.TenderUsageCodeType;
import dtv.tenderauth.AuthRequestType;
import dtv.xst.dao.loc.IRetailLocation;
import dtv.xst.dao.trl.IAuthorizableLineItem;
import dtv.xst.dao.ttr.ICheckTenderLineItem;
import dtv.xst.dao.ttr.IIdentityVerification;

/**
 * Customizations for ISD Check Request<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Jun 15, 2012
 * @version $Revision: 1105 $
 */
public class IsdCheckRequest
    extends dtv.tenderauth.impl.isd.check.IsdCheckRequest {

  /**
   * Constructs a <code>IsdCheckRequest</code>.
   * @param argType
   * @param argLine
   * @param argTenderUsageCode
   */
  public IsdCheckRequest(AuthRequestType argType, IAuthorizableLineItem argLine,
      TenderUsageCodeType argTenderUsageCode) {
    super(argType, argLine, argTenderUsageCode);
    ICheckTenderLineItem line = (ICheckTenderLineItem) argLine;

    setParameter(CKRequest.DATA_ENTRY_METHOD1, "0");
    setParameter(CKRequest.MICR_FORMAT_CODE, "05"); // MICR read in TAC format
    String micrData = line.getBankId() + "T" + line.getCheckAccountNumber() + "A";
    setParameter(CKRequest.CHECK_MICR_DATA, micrData);
    setParameter(CKRequest.TRACE_ID, null); //Telecheck does not want the Trace ID field

    //Adding account Identification ID details
    updateAccountIdentificationDetails(line);
    updateFrankingDetails(line);
  }

  private void updateAccountIdentificationDetails(ICheckTenderLineItem argLineItem) {

    List<IIdentityVerification> idList = argLineItem.getIdentityVerifications();

    if ((idList != null) && !idList.isEmpty()) {
      IIdentityVerification idVerification = idList.get(0);

      if (idVerification != null) {
        setParameter(CKRequest.ACCOUNT_ID_DATA,
            nonNull(idVerification.getIssuingAuthority()) + nonNull(idVerification.getIdNumber()));
        setParameter(CKRequest.DATA_ENTRY_METHOD2, "0");
      }
    }
  }

  private void updateFrankingDetails(ICheckTenderLineItem argLineItem) {

    IRetailLocation rtlLoc = LocationFactory.getInstance().getMyLocation();

    if (!isEmpty(rtlLoc.getStoreName())) {
      argLineItem.setStringProperty("STORE_NAME", rtlLoc.getStoreName());
    }
    if (!isEmpty(rtlLoc.getTelephone1())) {
      argLineItem.setStringProperty("STORE_PH_NUMBER", rtlLoc.getTelephone1());
    }
    if (!isEmpty(rtlLoc.getStoreNbr())) {
      argLineItem.setStringProperty("STORE_NUMBER", rtlLoc.getStoreNbr());
    }

  }
}
