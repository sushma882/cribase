//$Id: IsdKMAuthProcess.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.tenderauth.impl.isd.km;

import org.apache.log4j.Logger;

import dtv.hardware.auth.UserCancelledException;
import dtv.tenderauth.*;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.isd.AbstractIsdAuthProcess;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Sep 20, 2012
 * @version $Revision: 1105 $
 */
public class IsdKMAuthProcess
    extends AbstractIsdAuthProcess {

  private static final Logger logger_ = Logger.getLogger(IsdKMAuthProcess.class);

  /** {@inheritDoc} */
  @Override
  protected boolean isValidRequest(ITenderAuthRequest argRequest) {
    if (!(argRequest instanceof IsdKMRequest)) {
      return false;
    }
    else {
      return true;
    }
  }

  @Override
  protected void doProcess(final ITenderAuthRequest argRequest) {
    IAuthResponse response = null;

    argRequest.prepRequest();

    if (!isValidRequest(argRequest)) {
      logger_.warn("invalid request [" + argRequest + "] for " + getAuthMethodCode());
    }
    else {
      logger_.info(getClass().getName() + ".handleOnline");
      response = handleOnline(argRequest);
    }

    if ((response instanceof IsdKMResponse)) {
      logger_.info(getClass().getName() + "->response::" + toLogString(response));
    }
    else {
      logger_.warn("Failed to get the KM Message response");
    }
    argRequest.addResponse(response);
  }

  /**
   * Handle an authorization when the processor is online.
   * 
   * @param argRequest request to process
   * @return response
   */
  @Override
  protected IAuthResponse handleOnline(ITenderAuthRequest argRequest) {

    IAuthResponse response = null;
    try {
      logger_.info(getClass().getName() + ".handleOnline->sendRequest");
      response = getAuthCommunicator().sendRequest(argRequest);
    }
    catch (ReceiveTimeoutException ex) {
      // log problem
      getAuthLog().warn("Timeout waiting for response: " + ex);
      logger_.warn("Timeout waiting for response: " + ex);
    }
    catch (OfflineException ex) {
      // log problem
      getAuthLog().warn("Host offline: " + ex);
      logger_.warn("Host offline: " + ex);
    }
    catch (UserCancelledException ex) {}
    return response;
  }
}
