//$Id: IsdApiKMResponseConverter.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.tenderauth.impl.isd.km;

import java.util.Map;

import dtv.tenderauth.IAuthRequest;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.isd.IsdApiResponseConverter;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Sep 20, 2012
 * @version $Revision: 1105 $
 */
public class IsdApiKMResponseConverter
    extends IsdApiResponseConverter {

  @SuppressWarnings("unchecked")
  @Override
  public IAuthResponse convertResponse(Object argResponseObject, IAuthRequest argOriginalRequest) {
    return new IsdKMResponse(argOriginalRequest, (Map<String, Object>) argResponseObject);
  }
}
