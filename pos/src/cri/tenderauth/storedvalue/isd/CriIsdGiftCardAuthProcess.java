//$Id: CriIsdGiftCardAuthProcess.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.tenderauth.storedvalue.isd;

import java.math.BigDecimal;
import java.util.*;

import org.apache.log4j.Logger;

import cri.tenderauth.storedvalue.svs.AccessCodeCalculator;

import dtv.i18n.FormatterType;
import dtv.i18n.IFormattable;
import dtv.pos.common.ConfigurationMgr;
import dtv.tenderauth.*;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.storedvalue.IStoredValueAuthRequest;
import dtv.tenderauth.storedvalue.isd.IsdGiftCardAuthProcess;
import dtv.tenderauth.storedvalue.manualauth.IStoredValueManualAuthResult;
import dtv.tenderauth.storedvalue.svs.SvsManualAuthValidator;
import dtv.util.*;
import dtv.xst.dao.trl.IAuthorizableLineItem;
import dtv.xst.dao.ttr.IVoucherTenderLineItem;

/**
 * Customization to check for amount as it is null in case of CASH OUT request<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Jul 10, 2012
 * @version $Revision: 1105 $
 */
public class CriIsdGiftCardAuthProcess
    extends IsdGiftCardAuthProcess {

  private static final Logger logger_ = Logger.getLogger(CriIsdGiftCardAuthProcess.class);

  /** {@inheritDoc} */
  @Override
  protected IAuthResponse handleManual(ITenderAuthRequest argRequest) {
    Object v = getManualAuthNumber(argRequest.getMoreAuthInfo());

    if (validAuthNumber(argRequest, v)) {
      Date startTimestamp = DateUtils.getNewDate();
      // copy the information from the manual auth info on the argRequest

      IAuthorizableLineItem lineItem = argRequest.getLineItem();
      final BigDecimal originalAmount = lineItem.getAmount();
      BigDecimal amount = originalAmount;
      /* get the approved amount */
      BigDecimal manuallyAuthorizedAmount;

      if (isManualAuthAmountEditable()) {
        manuallyAuthorizedAmount = getManualAuthAmount(argRequest.getMoreAuthInfo());
      }
      else {
        manuallyAuthorizedAmount = originalAmount;
      }

      if (mustManualAuthAmountMatch()) {
        if (!NumberUtils.equivalent(originalAmount, manuallyAuthorizedAmount)) {
          return getFailedResponse(argRequest, argRequest, "MISMATCH_AUTH_AMOUNT");
        }
      }

      if ((lineItem instanceof IVoucherTenderLineItem)
          && (((IVoucherTenderLineItem) lineItem).getActivityCode().equalsIgnoreCase("ISSUED")
              || ((IVoucherTenderLineItem) lineItem).getActivityCode().equalsIgnoreCase("RELOAD"))) {

        amount = amount.abs();
        manuallyAuthorizedAmount = manuallyAuthorizedAmount.abs();
      }

      // don't allow the amount to be more than the requested amount
      if ((manuallyAuthorizedAmount != null) && (amount != null)) {
        amount = amount.min(manuallyAuthorizedAmount);
      }
      else if (manuallyAuthorizedAmount != null) {
        amount = manuallyAuthorizedAmount;
      }
      setAuthorizedAmount(lineItem, amount);

      updateLineForManual(lineItem, v.toString());
      IAuthResponse response = null;
      try {
        response = storeManualAuthForForwarding(argRequest);
      }
      catch (Exception ex) {
        logger_.error("CAUGHT EXCEPTION", ex);
        return handleException(argRequest);
      }
      clearTracksForManual(lineItem);

      String authorizationCode = lineItem.getAuthorizationCode();
      String adjudicationCode = lineItem.getAdjudicationCode();
      if (response == null) {
        response =
            argRequest.getSuccessfulManualAuthResponse(this, amount, authorizationCode, adjudicationCode);
      }
      response.setStartTimestamp(startTimestamp);
      response.setEndTimestamp(DateUtils.getNewDate());

      return response;
    }
    else {
      logger_.info(getClass().getName() + ".handleInvalidManualAuthNumber");
      return handleInvalidManualAuthNumber(argRequest, StringUtils.nonNull(v));
    }
  }

  @Override
  protected IAuthInfoField[] getManualAuthInfoFields(IAuthRequest argRequest) {
    List<IAuthInfoField> fields = new LinkedList<IAuthInfoField>();

    IFormattable merchantNumber = getMerchantNumber();
    if ((merchantNumber != IFormattable.EMPTY) && (merchantNumber != null)) {
      fields.add(new AuthInfoField(AuthInfoFieldKey.MERCHANT_NUMBER, //
          FF.getTranslatable("_authManualMerchantNumber"), //
          merchantNumber));
    }

    String transitNumber = getTransitNumber(argRequest);
    if (transitNumber != null) {
      fields.add(new AuthInfoField(AuthInfoFieldKey.TRANSIT_NUMBER, //
          FF.getTranslatable("_authManualTransitNumber"), //
          FF.getLiteral(transitNumber)));
    }

    String accountNumber = getAccountNumber(argRequest);
    if (getShowAccountNumber(accountNumber)) {
      fields.add(new AuthInfoField(AuthInfoFieldKey.ACCT_NUMBER, //
          FF.getTranslatable("_authManualAccountNumber"), //
          FF.getLiteral(accountNumber)));
    }

    String checkNumber = getCheckNumber(argRequest);
    if (checkNumber != null) {
      fields.add(new AuthInfoField(AuthInfoFieldKey.SERIAL_NUMBER, //
          FF.getTranslatable("_authManualCheckNumber"), //
          FF.getLiteral(checkNumber)));
    }

    Date expirationDate = getExpirationDate(argRequest);
    if (getShowExpirationDate(expirationDate)) {
      fields.add(new AuthInfoField(AuthInfoFieldKey.EXP_DATE, //
          FF.getTranslatable("_authManualExpDate"), //
          FF.getSimpleFormattable(expirationDate, FormatterType.DATE_EXP_LONG)));
    }

    IFormattable accessCode =
        getAccessCode(accountNumber, argRequest.getAmount(), ConfigurationMgr.getRetailLocationId());
    if ((accessCode != IFormattable.EMPTY) && (accessCode != null)) {
      fields.add(new AuthInfoField("accessCode", FF.getTranslatable("_authOfflineAccessCode"), accessCode));
    }

    if ((accessCode != IFormattable.EMPTY) && (accessCode != null)) {
      fields.add(new AuthInfoField("password", FF.getTranslatable("_authOfflinePasswordLbl"),
          FF.getTranslatable("_authOfflinePassword")));
    }

    if (!isManualAuthAmountEditable()) {
      BigDecimal amount = getAmount(argRequest);
      if (amount != null) {
        fields.add(new AuthInfoField(AuthInfoFieldKey.AMOUNT, //
            FF.getTranslatable("_authManualAmount"), //
            FF.getSimpleFormattable(amount, FormatterType.MONEY)));
      }
    }
    return fields.toArray(new IAuthInfoField[fields.size()]);

  }

  protected final IFormattable getAccessCode(String argCardNumber, BigDecimal argTranAmount,
      long argRtlLocId) {
    AccessCodeCalculator calculator = AccessCodeCalculator.getInstance();
    String accessCode = calculator.calculate(argCardNumber, argTranAmount, argRtlLocId);
    return FF.getLiteral(accessCode);
  }

  @Override
  protected boolean validAuthNumber(ITenderAuthRequest argRequest, Object argValue) {

    if (argValue == null) {
      return false;
    }
    IStoredValueManualAuthResult results =
        (new SvsManualAuthValidator()).validate((IStoredValueAuthRequest) argRequest, argValue.toString());

    return results.isValid();
  }

}
