//$Id: CriIsdGiftCardCashOutRequest.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.tenderauth.storedvalue.isd;

import imsformat.credit.CCRequest;
import imsformat.sv.SVRequest;

import dtv.pos.iframework.type.TenderUsageCodeType;
import dtv.tenderauth.AuthRequestType;
import dtv.tenderauth.storedvalue.isd.IsdGiftCardCashOutRequest;
import dtv.xst.dao.trl.IAuthorizableLineItem;

/**
 * Customization to include the Pin data in the CASH_OUT request<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Aug 14, 2012
 * @version $Revision: 1105 $
 */
public class CriIsdGiftCardCashOutRequest
    extends IsdGiftCardCashOutRequest {

  /**
   * Constructor
   * 
   * @param argType the type of authorization request
   * @param argLine the line item this request is related to, or <code>null</code> is there is no
   * related transaction line item
   */
  public CriIsdGiftCardCashOutRequest(AuthRequestType argType, IAuthorizableLineItem argLine) {
    super(argType, argLine, null);
  }

  /**
   * Constructor
   * 
   * @param argType the type of authorization request
   * @param argLine the line item this request is related to, or <code>null</code> is there is no
   * related transaction line item
   * @param argTenderUsageCode tender usage
   */
  public CriIsdGiftCardCashOutRequest(AuthRequestType argType, IAuthorizableLineItem argLine,
      TenderUsageCodeType argTenderUsageCode) {
    super(argType, argLine, argTenderUsageCode);
  }

  /** {@inheritDoc} */
  @Override
  public void prepRequest() {
    super.prepRequest();

    // for VOID_CASH_OUT, pull props from line item
    if (AuthRequestType.VOID_CASH_OUT.equals(getRequestType())) {
      setParameter(CCRequest.LOCAL_DATE, getLineItem().getStringProperty(CCRequest.LOCAL_DATE));
      setParameter(CCRequest.LOCAL_TIME, getLineItem().getStringProperty(CCRequest.LOCAL_TIME));
      setParameter(SVRequest.DATE1_8, getLineItem().getStringProperty(SVRequest.DATE1_8));
      setParameter(SVRequest.TIME1_8, getLineItem().getStringProperty(SVRequest.TIME1_8));
      // setParameter(SVRequest.TENDER_AMOUNT, getLineItem().getStringProperty(SVRequest.TENDER_AMOUNT));
      setParameter(SVRequest.SYSTEM_TRACE_AUDIT_NUMBER,
          getLineItem().getStringProperty(SVRequest.SYSTEM_TRACE_AUDIT_NUMBER));
    }
    else {
      // for other request types, store props on line item
      getLineItem().setStringProperty(CCRequest.LOCAL_DATE,
          (String) getRequestMap().get(CCRequest.LOCAL_DATE));
      getLineItem().setStringProperty(CCRequest.LOCAL_TIME,
          (String) getRequestMap().get(CCRequest.LOCAL_TIME));
      getLineItem().setStringProperty(SVRequest.DATE1_8, (String) getRequestMap().get(SVRequest.DATE1_8));
      getLineItem().setStringProperty(SVRequest.TIME1_8, (String) getRequestMap().get(SVRequest.TIME1_8));
      // getLineItem().setStringProperty(SVRequest.TENDER_AMOUNT,
      // (String) getRequestMap().get(SVRequest.TENDER_AMOUNT));
      getLineItem().setStringProperty(SVRequest.SYSTEM_TRACE_AUDIT_NUMBER,
          (String) getRequestMap().get(SVRequest.SYSTEM_TRACE_AUDIT_NUMBER));
    }
  }
}
