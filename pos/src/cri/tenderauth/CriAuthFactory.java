//$Id: CriAuthFactory.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.tenderauth;

import java.lang.reflect.Constructor;

import org.apache.log4j.Logger;

import dtv.tenderauth.*;
import dtv.tenderauth.event.IAuthResponse;
import dtv.util.ReflectionException;

/**
 * Factory for authorization processes.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class CriAuthFactory
    extends AuthFactory {
  private static final Logger logger_ = Logger.getLogger(CriAuthFactory.class);
  private static final Class<?>[] SAF_CTOR_CLASSES = new Class<?>[] {IAuthResponse.class, String.class};

  /**
   * 
   * 
   * @param argAuthMethodCode
   * @param argType
   * @param argResponse
   * @param argAuthCode
   * @return
   */
  public IAuthRequest makeAuthRequest(String argAuthMethodCode, AuthRequestType argType,
      IAuthResponse argResponse, String argAuthCode) {
    Class<? extends IAuthRequest> clazz = getAuthRequestClass(argAuthMethodCode, argType, true);
    IAuthRequest r = null;
    Constructor<? extends IAuthRequest> ctor = null;
    try {
      ctor = clazz.getConstructor(SAF_CTOR_CLASSES);
    }
    catch (Exception ex) {
      logger_.error("required ctor not found", ex);
      throw new ReflectionException(ex);
    }
    try {
      r = ctor.newInstance(argResponse, argAuthCode);
    }
    catch (Exception ex) {
      throw new ReflectionException(ex);
    }
    return initRequest(argAuthMethodCode, argType, r);
  }
}
