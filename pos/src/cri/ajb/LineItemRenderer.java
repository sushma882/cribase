//$Id: LineItemRenderer.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb;

import static dtv.docbuilding.types.DocBuilderAlignmentType.LEFT;
import static dtv.docbuilding.types.DocBuilderAlignmentType.RIGHT;

import java.math.BigDecimal;

import org.apache.log4j.Logger;

import cri.pos.common.CriConfigurationMgr;

import dtv.docbuilding.FormattedLine;
import dtv.hardware.sigcap.state.DefaultLineItemRenderer;
import dtv.pos.customer.account.CustomerAccountHelper;
import dtv.pos.customer.account.ICustomerAccountType;
import dtv.pos.customer.account.type.CustomerAccountType;
import dtv.util.ICodeInterface;
import dtv.util.StringUtils;
import dtv.xst.dao.cat.ICustomerItemAccountDetail;
import dtv.xst.dao.dsc.IDiscount;
import dtv.xst.dao.trl.*;

/**
 * Determines the format of lines show up on the line display of a signature capture device.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class LineItemRenderer
    extends DefaultLineItemRenderer {

  private static final Logger logger_ = Logger.getLogger(LineItemRenderer.class);

  private int itemWidth_ = 0;

  /**
   * Constructor method.
   * 
   * @param argItemWidth
   */
  public LineItemRenderer(int argItemWidth) {
    super(argItemWidth);
    itemWidth_ = argItemWidth;
  }

  /** {@inheritDoc} */
  @Override
  protected String getCustAccountTypeDescription(ICustomerItemAccountDetail argDetail) {
    ICustomerAccountType type = CustomerAccountType.forName(argDetail.getCustAccountCode());
    if ((type instanceof ICodeInterface) && (((ICodeInterface) type).getDescription() != null)) {
      return ((ICodeInterface) type).getDescription();
    }
    return argDetail.getCustAccountCode();
  }

  /** {@inheritDoc} */
  @Override
  protected ICustomerItemAccountDetail searchCustItemAcctDetailForSaleLineItem(ISaleReturnLineItem argLine) {
    return CustomerAccountHelper.getCorrespondingAccountDetail(argLine);
  }

  /** {@inheritDoc} */
  @Override
  protected String render(String argQuantity, int argQtyWidth, String argDescription, String argAmt) {
    int descLength = Math.min(35, argDescription.length());
    FormattedLine.Field[] fields = new FormattedLine.Field[] {
        // add the quantity as the left-most field, LEFT aligned
        newField(0, centeredQuantity(argQuantity, argQtyWidth), LEFT, 1),
        // add the description field after the quantity
        // the previous field is fixed-width, so this field will also be lined up
        newField(null, "    " + argDescription.substring(0, descLength), LEFT, 0),
        // add the amount field, at the far right, right aligned (minus the last character of the amount; see next field)
        newField(-1, StringUtils.slice(argAmt, 0, -1), RIGHT, 1),
        // add the last character of the amount; this is split off to get all the way to the right margin
        newField(null, StringUtils.slice(argAmt, -1, argAmt.length()), LEFT, 1),
        // add some spaces to type over anything from the description that slips through
        newField(null, "            ", LEFT, 0)};
    // combine the fields into a single line with field over-type
    FormattedLine line = new FormattedLine(fields, LEFT, "", /*overtype=*/true);
    // render the line to the configured width
    return line.toString(itemWidth_);
  }

  /** {@inheritDoc} */
  @Override
  public String renderAmount(BigDecimal argAmount) {
    String amountStr = super.renderAmount(argAmount);
    return convertFormat(amountStr);
  }

  /**
   * Convert display format for the money on device.
   *
   * @param argValue
   * @return
   */
  protected String convertFormat(String argValue) {
    if (!StringUtils.isEmpty(argValue)) {
      return argValue.replace(',', getTokenToReplacement());
    }

    return argValue;
  }

  /**
   * Get the replacement token char.
   *
   * @return
   */
  protected char getTokenToReplacement() {
    return CriConfigurationMgr.getTokenToReplacementComma().charAt(0);
  }

  /**
   * Get the replacement token char.
   *
   * @return
   */
  @Override
  protected String[] renderRetailPriceModifier(IRetailPriceModifier argMod) {
    RetailPriceModifierReasonCode code =
        RetailPriceModifierReasonCode.forName(argMod.getRetailPriceModifierReasonCode());

    final int indent;
    final String descr;
    BigDecimal amt;
    if (code.equals(RetailPriceModifierReasonCode.NEW_PRICE_RULE)) {
      //<Element name="PRICING_MODIFIER">
      indent = 4;
      descr = argMod.getPromotionId();
      amt = argMod.getExtendedAmount().negate();
    }
    else if (code.equals(RetailPriceModifierReasonCode.DEAL)) {
      //<Element name="DEAL_PRICE_MODIFIER">
      indent = 4;
      descr = translateIf(argMod.getDescription());
      amt = argMod.getAmount().negate();
    }
    else if (isRegularDiscount(code)) {
      //<Element name="APPLIED_DISCOUNT_LIST">
      //    <CellDataHandlerFactoryClass dtype="Class">dtv.pos.pricing.discount.AppliedDiscountCellDataHandler</CellDataHandlerFactoryClass>
      final IDiscount discount = argMod.getDiscount();
      indent = 2;
      descr = translateIf(discount.getDescription());
      amt = argMod.getExtendedAmount().negate();
    }
    else {
      // unknown type; should not have been passed from the line item filter
      logger_.warn("unexpected discount reason code " + code);
      return new String[0];
    }
    if (argMod.getVoid() || argMod.getParentLine().getVoid()) {
      amt = BigDecimal.ZERO;
    }
    return new String[] {render("", indent, descr, amt)};
  }
}
