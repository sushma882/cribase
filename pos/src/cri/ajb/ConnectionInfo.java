//$Id: ConnectionInfo.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb;

import java.io.Serializable;
import java.util.Map;

import org.apache.log4j.Logger;

import dtv.tenderauth.IAuthProcess;
import dtv.tenderauth.IAuthRequest;
import dtv.tenderauth.impl.ajb.request.AbstractAjbRequest;
import dtv.util.StringUtils;
import dtv.util.config.ConfigUtils;
import dtv.util.config.IConfigObject;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2017 MICROS Retail
 *
 * @author johgaug
 * @created Jan 3, 2017
 * @version $Revision: 1105 $
 */
public class ConnectionInfo
    implements Serializable {

  private static final long serialVersionUID = -5378153868347138328L;

  private static final Logger LOG = Logger.getLogger(ConnectionInfo.class);

  private static final String DEFAULT_HOST = "localhost";
  private static final int DEFAULT_PORT = 24900;

  private static final String PARAM_AJB_HOST = "hostname";
  private static final String PARAM_AJB_PORT = "port";

  private static final String PARAM_AJB_TIMEOUT = "ajbTimeout";
  private static final String PARAM_XST_TIMEOUT = "xstoreTimeout";

  private String host;
  private int port;
  private long xstoreTimeout;

  private transient Integer hashCode;
  private transient String toString;

  /** Constructs a <code>ConnectionInfo</code>. */
  public ConnectionInfo(IAuthProcess argProcess) {
    this(null, argProcess);
  }

  /** Constructs a <code>ConnectionInfo</code>. */
  public ConnectionInfo(IAuthRequest argRequest, IAuthProcess argProcess) {
    Map<String, IConfigObject> parameters = argProcess.getParameterMap();

    if (parameters.containsKey(PARAM_AJB_HOST)) {
      host = parameters.get(PARAM_AJB_HOST).toString();
      if (StringUtils.isEmpty(host)) {
        LOG.warn("No AJB host specified, using default value: " + DEFAULT_HOST);
        host = DEFAULT_HOST;
      }
    }
    else {
      LOG.warn("AJB host parameter missing, using default value: " + DEFAULT_HOST);
      host = DEFAULT_HOST;
    }

    if (parameters.containsKey(PARAM_AJB_PORT)) {
      try {
        port = Integer.parseInt(parameters.get(PARAM_AJB_PORT).toString());
      }
      catch (NumberFormatException ex) {
        LOG.warn("Invalid AJB port specified, using default value: " + DEFAULT_PORT, ex);
        port = DEFAULT_PORT;
      }
    }
    else {
      LOG.warn("AJB port parameter missing, using default value: " + DEFAULT_PORT);
      port = DEFAULT_PORT;
    }

    xstoreTimeout = ConfigUtils.toInt(argProcess.getParameterMap().get(PARAM_XST_TIMEOUT));
    int ajbTimeout = ConfigUtils.toInt(argProcess.getParameterMap().get(PARAM_AJB_TIMEOUT));
    if (argRequest instanceof AbstractAjbRequest) {
      ((AbstractAjbRequest) argRequest).setTimeoutSeconds(ajbTimeout);
    }
    if (xstoreTimeout < ajbTimeout) {
      LOG.warn("Xstore timeout [" + xstoreTimeout + "] is less than AJB timeout [" + ajbTimeout + "]");
      xstoreTimeout = ajbTimeout + 15;
    }
  }

  public String getHost() {
    return host;
  }

  public int getPort() {
    return port;
  }

  public long getXstoreTimeout() {
    return xstoreTimeout;
  }

  /** {@inheritDoc} */
  @Override
  public boolean equals(Object argOther) {
    final boolean equal;
    if (this == argOther) {
      equal = true;
    }
    else if (!(argOther instanceof ConnectionInfo)) {
      equal = false;
    }
    else {
      ConnectionInfo o = (ConnectionInfo) argOther;
      equal = (getPort() == o.getPort()) && (getXstoreTimeout() == o.getXstoreTimeout())
          && Objects.equals(getHost(), o.getHost());
    }
    return equal;
  }

  /** {@inheritDoc} */
  @Override
  public int hashCode() {
    if (hashCode == null) {
      hashCode = Integer
          .valueOf(Objects.hash(getHost(), Integer.valueOf(getPort()), Long.valueOf(getXstoreTimeout())));
    }
    return hashCode.intValue();
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    if (toString == null) {
      toString = getClass().getSimpleName() + "[Host=" + getHost() + ",Port=" + getPort() + ",XstoreTimeout="
          + getXstoreTimeout() + "]";
    }
    return toString;
  }
}
