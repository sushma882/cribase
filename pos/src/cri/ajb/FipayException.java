//$Id: FipayException.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2017 MICROS Retail
 *
 * @author johgaug
 * @created Jan 3, 2017
 * @version $Revision: 1105 $
 */
public class FipayException
    extends RuntimeException {

  /**
   * 
   */
  private static final long serialVersionUID = 6547074293417279969L;

  /** Constructs a <code>FipayException</code>. */
  public FipayException() {}

  /** Constructs a <code>FipayException</code>. */
  public FipayException(String argMessage) {
    super(argMessage);
  }

  /** Constructs a <code>FipayException</code>. */
  public FipayException(Throwable argCause) {
    super(argCause);
  }

  /** Constructs a <code>FipayException</code>. */
  public FipayException(String argMessage, Throwable argCause) {
    super(argMessage, argCause);
  }

}
