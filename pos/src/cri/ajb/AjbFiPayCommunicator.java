//$Id: AjbFiPayCommunicator.java 1245 2018-04-26 18:42:45Z johgaug $
package cri.ajb;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import AJBComm.*;

import dtv.hardware.auth.UserCancelledException;
import dtv.pos.common.EnvironmentHelper;
import dtv.tenderauth.*;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.AbstractCommunicator;
import dtv.util.StringUtils;

/**
 * An auth communicator that sends comma-delimited messages to AJB via AjbFipay.<br>
 * Datavantage Corporation <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author shyxie
 * @created June 20, 2015
 * @version $Revision: 1245 $
 */
public class AjbFiPayCommunicator
    extends AbstractCommunicator {

  static final class CommLogger {

    private static final Logger COM_LOG = Logger.getLogger(CommLogger.class);

    private final boolean allowLogging;

    /** Constructs a <code>CommLogger</code>. */
    public CommLogger() {
      boolean envEnabled = true;
      try {
        envEnabled = EnvironmentHelper.getInstance().isEnvironmentEnabled();
      }
      catch (RuntimeException ex) {
        /*
         * The environment helper may fail to initialize in a dev environment due to missing property
         * files. In production, this will cause many other problems (e.g. unable to open the store),
         * preventing this class from logging anyway.
         */
        envEnabled = false;
      }
      allowLogging = !envEnabled;
    }

    /** Log a communication message if the current environment is NOT production. */
    public void log(Object argMessage) {
      if (allowLogging && COM_LOG.isDebugEnabled()) {
        COM_LOG.debug(argMessage);
      }
    }

  }

  private static final Logger LOG = Logger.getLogger(AjbFiPayCommunicator.class);
  /** Logging object that can safely output FIPay messages to a log. */
  private static final CommLogger COMM_LOG = new CommLogger();
  /** Field delimiter in a raw FIPay message. */
  private static final char DELIMITER = ',';
  /** Mapping of request message IDs to expected response message IDs. Field 1 in AJB. */
  private static final Map<Integer, Integer> _responseCodes;
  /** Lock that guarantees only one thread can access FIPay at a time. */
  private static final ReentrantLock _lock = new ReentrantLock();
  private static final String MSG_LOCK_TIMEOUT = "Failed to acquire communications lock";
  /** The number of seconds to wait when another thread is communicating with FIPay. */
  private static final long LOCK_WAIT_SECONDS = 8;

  static {
    Map<Integer, Integer> responseCodes = new HashMap<Integer, Integer>();
    responseCodes.put(Integer.valueOf(100), Integer.valueOf(101));
    responseCodes.put(Integer.valueOf(101), Integer.valueOf(101));
    responseCodes.put(Integer.valueOf(102), Integer.valueOf(102));
    responseCodes.put(Integer.valueOf(103), Integer.valueOf(103));
    responseCodes.put(Integer.valueOf(106), Integer.valueOf(106));
    responseCodes.put(Integer.valueOf(107), Integer.valueOf(107));
    responseCodes.put(Integer.valueOf(111), Integer.valueOf(111));
    responseCodes.put(Integer.valueOf(115), Integer.valueOf(115));
    responseCodes.put(Integer.valueOf(125), Integer.valueOf(125));
    responseCodes.put(Integer.valueOf(150), Integer.valueOf(151));
    responseCodes.put(Integer.valueOf(151), Integer.valueOf(151));
    responseCodes.put(Integer.valueOf(210), Integer.valueOf(211));
    responseCodes.put(Integer.valueOf(211), Integer.valueOf(211));
    responseCodes.put(Integer.valueOf(901), Integer.valueOf(901));
    _responseCodes = Collections.unmodifiableMap(responseCodes);
  }

  /** {@inheritDoc} */
  @Override
  protected final IAuthResponse sendRequestImpl()
      throws UserCancelledException, ReceiveTimeoutException {
    /* Allow only one thread to communicate with FIPay at a time. If a thread is communicating, try
     * to get past the lock for a few seconds. This should allow non-interactive processes to finish
     * (SAF, POSITEMS). */
    boolean locked = false;
    try {
      locked = _lock.tryLock(LOCK_WAIT_SECONDS, TimeUnit.SECONDS);
      if (locked) {
        return sendRequestImpl2();
      }
      throw new UserCancelledException(MSG_LOCK_TIMEOUT);
    }
    catch (InterruptedException ex) {
      throw new UserCancelledException(MSG_LOCK_TIMEOUT, ex);
    }
    finally {
      if (_lock.isHeldByCurrentThread()) {
        _lock.unlock();
      }
    }
  }

  protected IAuthResponse sendRequestImpl2()
      throws UserCancelledException, ReceiveTimeoutException {
    IAuthProcess process = getAuthProcess();
    IAuthRequest request = getRequest();

    ConnectionInfo connect = new ConnectionInfo(request, process);

    CAFipay comm = new CAFipay(connect.getHost(), connect.getPort());
    try {
      int requestType = -1;
      try {
        String payload = getPayload(request);
        requestType = getTransactionType(payload);
        COMM_LOG.log("Sending: " + payload);
        comm.SEND_MSGAPI(payload);
      }
      catch (CAFipayNetworkException ex) {
        LOG.error("Error sending request to FIPay. Error code [" + ex.getErrorCode() + "] and description ["
            + ex.getErrorDescription() + "]");
        throw new FipayException(ex);
      }
      catch (CAFipayTimeoutException ex) {
        // Wrap FiPay's timeout exception in an auth framework timeout exception.
        throw new ReceiveTimeoutException(ex);
      }

      String primaryResponse = null;

      /* AJB can return multiple responses for a single request. Normally this will include a single 101 response and
       * optionally multiple others (e.g. for receipt data). The 101 response needs to be the main response returned with
       * others chained off the 101. Xstore currently does not care about the other responses, but this code should still
       * parse and return them to ensure no needed data is lost.
       * 
       * This code needs to gather up all of the responses, then convert them one at a time. The 101 must be handled
       * first, and others chained off of that response.
       */
      final int expectedResponseType = getResponseCodeType(requestType);
      long xstoreTimeout = connect.getXstoreTimeout();
      long begin = System.currentTimeMillis();
      while ((primaryResponse == null) && (System.currentTimeMillis() < (begin + (1000L * xstoreTimeout)))) {
        try {
          String rawResponse = comm.RECV_MSGAPI();
          if (StringUtils.isEmpty(rawResponse)) {
            // For some reason FiPay keeps sending empty messages - keepalive? Ignore them.
            continue;
          }
          COMM_LOG.log("Received: " + rawResponse);
          int transactionType = getTransactionType(rawResponse);
          LOG.info("Received AJB message with transaction type [" + transactionType
              + "] and total message length [" + ((rawResponse == null) ? 0 : rawResponse.length()) + "]");
          if (transactionType == expectedResponseType) {
            primaryResponse = rawResponse;
          }
          Thread.sleep(100);
        }
        catch (InterruptedException ex) {
          // Something told the thread to stop, so translate it into an exception that the auth framework understands.
          LOG.warn("Communication canceled", ex);
          throw new UserCancelledException(ex);
        }
        catch (CAFipayException ex) {
          LOG.error("Error receiving response from FIPay. Error code [" + ex.getErrorCode()
              + "] and description [" + ex.getErrorDescription() + "]");
          // Unknown FiPay exception: wrap it into an unchecked exception.
          throw new FipayException(ex);
        }
      }

      if (primaryResponse == null) {
        LOG.error("AJB did not return a " + expectedResponseType
            + " response message. Returning a null response object.");
        throw new ReceiveTimeoutException();
      }
      else {
        IAuthResponse response = getResponseConverter().convertResponse(primaryResponse, request);
        response.setAuthProcess(process);
        return response;
      }
    }
    finally {
      comm.Disconnect();
    }
  }

  /** Get Response Code */
  protected int getResponseCodeType(int argRequestType) {
    final Integer key = Integer.valueOf(argRequestType);
    int response = 0;
    if (_responseCodes.containsKey(key)) {
      response = _responseCodes.get(key).intValue();
    }
    return response;
  }

  /** Get the transaction type (command), e.g. 101. */
  protected int getTransactionType(String argRawResponse) {
    int transactionType = Integer.MAX_VALUE;
    if (argRawResponse != null) {
      int index = argRawResponse.indexOf(DELIMITER);
      if (index > 0) {
        final String val = argRawResponse.substring(0, index);
        try {
          transactionType = Integer.parseInt(val);
        }
        catch (NumberFormatException ex) {
          // Do not log message details, including the exception which might
          // contain more information than can be safely logged. Imagine a
          // malformed message without delimiters that contains sensitive data.
          // This code scrubs the data by truncating everything past three
          // characters (AJBs transaction types are all three-digit integers).
          String toLog = val;
          if (index > 3) {
            toLog = val.substring(0, 3) + "...";
          }
          LOG.warn("Invalid AJB transaction type [" + toLog + "] caused a NumberFormatException");
        }
      }
    }
    return transactionType;
  }

}
