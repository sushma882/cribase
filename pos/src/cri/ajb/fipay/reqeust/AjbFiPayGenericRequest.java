//$Id: AjbFiPayGenericRequest.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.reqeust;

import cri.ajb.fipay.response.AjbFiPayTransactionDisplayResponse;

import dtv.tenderauth.AuthRequestType;
import dtv.tenderauth.ISilentForwardRequest;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.ajb.request.AbstractAjbRequest;

/**
 * AJB generic request.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayGenericRequest
    extends AbstractAjbRequest
    implements ISilentForwardRequest {

  /**
   * Constructor method
   * 
   * @param argType
   * @param argData
   */
  public AjbFiPayGenericRequest(AuthRequestType argType, Object[] argData) {
    super(argType, null, null, null, null, null);
  }

  /** {@inheritDoc} */
  @Override
  public IAuthResponse getResponse(String[] argFields, String argResponseMessageId) {
    return new AjbFiPayTransactionDisplayResponse(this, argFields);
  }

  /** {@inheritDoc} */
  @Override
  protected void addingResponse(IAuthResponse argResponse) {
    //do noting when add the response
  }
}
