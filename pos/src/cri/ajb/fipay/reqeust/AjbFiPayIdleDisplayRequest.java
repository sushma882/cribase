//$Id: AjbFiPayIdleDisplayRequest.java 875 2015-07-13 22:12:21Z suz.sxie $
package cri.ajb.fipay.reqeust;

import dtv.tenderauth.AuthRequestType;

/**
 * AJB FiPay IDLE screen display request.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 875 $
 */
public class AjbFiPayIdleDisplayRequest
    extends AjbFiPayTransactionDisplayRequest {
  /**
   * Constructor method
   * 
   * @param argType
   * @param argData
   */
  public AjbFiPayIdleDisplayRequest(AuthRequestType argType, Object[] argData) {
    super(argType, argData);

    //clean fields.
    setField(HEADER_NBR, null);
    setField(SUMMARY_NBR, null);
  }
}
