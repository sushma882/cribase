//$Id: AjbFiPayCreditDebitRequest.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.reqeust;

import cri.ajb.fipay.response.AjbFiPayCreditDebitResponse;
import cri.pos.tender.CriTenderHelper;

import dtv.i18n.FormattableFactory;
import dtv.i18n.IFormattable;
import dtv.pos.iframework.type.TenderUsageCodeType;
import dtv.pos.tender.TenderHelper;
import dtv.tenderauth.AuthRequestType;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.ajb.request.AjbCreditRequest;
import dtv.util.StringUtils;
import dtv.xst.dao.trl.IAuthorizableLineItem;

/**
 * AJB FiPay credit/debit auth request.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayCreditDebitRequest
    extends AjbCreditRequest
    implements IAjbFiPayTenderAuthRequest {

  protected static final FormattableFactory FF = FormattableFactory.getInstance();
  protected static final CriTenderHelper TENDER_HELPER = (CriTenderHelper) TenderHelper.getInstance();
  protected static final AuthRequestType TENDER_MANUAL_ENTRY = AuthRequestType.forName("TENDER_MANUAL_ENTRY");

  protected static final int TOKEN_NBR = 34;
  protected static final int OPERATOR_NBR = 24;

  /**
   * 
   * @param argType
   * @param argLine
   * @param argTenderUsageCode
   */
  public AjbFiPayCreditDebitRequest(AuthRequestType argType, IAuthorizableLineItem argLine,
      TenderUsageCodeType argTenderUsageCode) {
    this(argType, argLine, argTenderUsageCode, true);
  }

  protected AjbFiPayCreditDebitRequest(AuthRequestType argType, IAuthorizableLineItem argLine,
      TenderUsageCodeType argTenderUsageCode, boolean argNewInvoice) {
    super(argType, argLine, argTenderUsageCode);
    setTokenField(argLine);
    setOperatorId(argLine);

    if (argNewInvoice) {
      setInvoiceNbr(argLine, TENDER_HELPER.buildInvoiceNbr(argLine));
    }
  }

  /**
   * Set the AJB FiPay request invoice number. This is unique for each transaction.
   * 
   * @param argLine
   * @param argInvoiceNbr
   */
  protected void setInvoiceNbr(IAuthorizableLineItem argLine, String argInvoiceNbr) {
    setField(POS_TRAN_NBR, argInvoiceNbr);
    TENDER_HELPER.setOriginalInvoiceNbr(argLine, argInvoiceNbr);

  }

  /** {@inheritDoc} */
  @Override
  public IAuthResponse getResponse(String[] argFields, String argAuthCode) {
    return new AjbFiPayCreditDebitResponse(this, argFields, argAuthCode);
  }

  /** {@inheritDoc} */
  @Override
  public IFormattable getMessage(String argMessageId) {
    if ("SWIPE".equals(argMessageId)) {
      return FF.getTranslatable("_ajbCommunicatorCardSwip");
    }
    else if ("MANUAL_ENTRY".equals(argMessageId)) {
      return FF.getTranslatable("_ajbCommunicatorManualEntry");
    }
    else {
      return super.getMessage(argMessageId);
    }
  }

  /** {@inheritDoc} */
  @Override
  public String getCommand() {
    return getField(MESSAGE_ID);
  }

  protected void setTokenField(IAuthorizableLineItem argLine) {
    if (argLine != null) {
      setField(TOKEN_NBR, argLine.getAuthorizationToken());
    }
  }

  public boolean isSignRequest() {
    String option = getField(AJB_OPTION_FIELD);
    return (!StringUtils.isEmpty(option) && option.contains("*Sign"));
  }

  public boolean isManualEntry() {
    String option = getField(AJB_OPTION_FIELD);
    return (!StringUtils.isEmpty(option) && option.contains("*CEM_Manual"));
  }

  protected void setOperatorId(IAuthorizableLineItem argLine) {
    if (argLine != null) {
      setField(OPERATOR_NBR, argLine.getCreateUserId());
    }
  }

  @Override
  protected String getAuthRequestTypeString(AuthRequestType argType, TenderUsageCodeType argTenderUsageCode) {
    if (TENDER_MANUAL_ENTRY == argType) {
      return AUTH_REQUEST_TYPE_SALE;
    }

    return super.getAuthRequestTypeString(argType, argTenderUsageCode);
  }
}
