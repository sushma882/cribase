//$Id: AjbFiPayGiftCardRequest.java 877 2015-07-15 18:38:01Z suz.sxie $
package cri.ajb.fipay.reqeust;

import dtv.tenderauth.AuthRequestType;
import dtv.xst.dao.trl.IAuthorizableLineItem;

/**
 * AJB gift card void request.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 877 $
 */
public class AjbFiPayGiftCardVoidRequest
    extends AjbFiPayGiftCardRequest {

  public AjbFiPayGiftCardVoidRequest(AuthRequestType argType, IAuthorizableLineItem argLine) {
    super(argType, argLine);

    setOriginalInvoiceNumber(TENDER_HELPER.getOriginalInvoiceNbr(argLine));
    setInvoiceNbr(argLine, TENDER_HELPER.buildInvoiceNbr(argLine, 1));
  }

  /**
   * Set the original AJB FiPay invoice number.
   * 
   * @param argInvoiceNbr
   */
  protected void setOriginalInvoiceNumber(String argInvoiceNbr) {
    setField(CARD_NUMBER, argInvoiceNbr);
  }

  /** {@inheritDoc} */
  @Override
  public void loadSensitiveData() {

  }
}
