//$Id: AjbFiPayCreditDebitVoidRequest.java 902 2015-07-30 19:26:41Z suz.sxie $
package cri.ajb.fipay.reqeust;

import dtv.tenderauth.AuthRequestType;
import dtv.xst.dao.tnd.TenderStatus;
import dtv.xst.dao.trl.IAuthorizableLineItem;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * AJB FiPay credit/debit void auth request.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 902 $
 */
public class AjbFiPayCreditDebitVoidRequest
    extends AjbFiPayCreditDebitRequest {

  /**
   * Constructor method.
   * 
   * @param argType
   * @param argLine
   */
  public AjbFiPayCreditDebitVoidRequest(AuthRequestType argType, IAuthorizableLineItem argLine) {
    super(argType, argLine, null);
    String type = getTenderUsageCode(((ITenderLineItem) argLine).getTenderStatusCode());
    setAuthRequestType(type);
    setOriginalInvoiceNumber(TENDER_HELPER.getOriginalInvoiceNbr(argLine));

    setInvoiceNbr(argLine, TENDER_HELPER.buildInvoiceNbr(argLine, 1));
    //clean this field we are not using these fields in void request
    setField(TOKEN_NBR, null);
  }

  /**
   * Get the tender usage code.
   * 
   * @param argStatus
   * @return
   */
  protected String getTenderUsageCode(String argStatus) {
    if (TenderStatus.TENDER == TenderStatus.forName(argStatus)) {
      return "Sale";
    }
    else if (TenderStatus.REFUND == TenderStatus.forName(argStatus)) {
      return "Refund";
    }
    else {
      return null;
    }
  }

  /**
   * Set the Auth request type.
   * 
   * @param argAuthRequestType
   */
  protected void setAuthRequestType(String argAuthRequestType) {
    setField(AUTH_REQUEST_TYPE, argAuthRequestType);
  }

  /**
   * Set the original AJB FiPay invoice number.
   * 
   * @param argInvoiceNbr
   */
  protected void setOriginalInvoiceNumber(String argInvoiceNbr) {
    setField(ACCOUNT_NBR, argInvoiceNbr);
  }

  /** {@inheritDoc} */
  @Override
  public String getMessageString() {
    StringBuffer buf = new StringBuffer(100);

    for (int i = 1; i <= getMaxField(); i++ ) {
      if (getField(i) != null) {

        buf.append(getField(i));

      }

      if (i < getMaxField()) {
        buf.append(",");
      }
    }

    return buf.toString();
  }

  /** {@inheritDoc} */
  @Override
  public void loadSensitiveData() {

  }
}
