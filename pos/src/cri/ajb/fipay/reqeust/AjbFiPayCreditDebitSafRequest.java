//$Id: AjbFiPayCreditDebitSafRequest.java 960 2015-09-29 16:54:03Z suz.sxie $
package cri.ajb.fipay.reqeust;

import dtv.tenderauth.ITenderAuthRequest;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.ajb.response.AbstractAjbResponse;
import dtv.xst.dao.trl.IAuthorizableLineItem;

/**
 * AJB FiPay credit SAF reqeust.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 960 $
 */
public class AjbFiPayCreditDebitSafRequest
    extends AjbFiPayCreditDebitRequest {
  protected static final int FOCUS_AUTH_CODE_NBR = 19;
  protected static final int ACTION_CODE = 4;
  protected static final int SPDH_CODE = 36;

  /**
   * Constructor method.
   * 
   * @param argResponse
   */
  protected AjbFiPayCreditDebitSafRequest(IAuthResponse argResponse) {
    super(argResponse.getRequest().getRequestType(), argResponse.getRequest().getLineItem(),
        ((ITenderAuthRequest) argResponse.getRequest()).getTenderUsageCode(), false);

    setFields(((AbstractAjbResponse) argResponse).getFields());

    //remove the action code
    setActionCode(null);

    //remove the spdh code
    setSpdhCode(null);
  }

  /**
   * Constructor method.
   * 
   * @param argResponse
   * @param argAuthCode
   */
  public AjbFiPayCreditDebitSafRequest(IAuthResponse argResponse, String argAuthCode) {
    this(argResponse);
    setFocusAuthCode(argAuthCode);
  }

  /**
   * Set the auth code.
   * 
   * @param authCode
   */
  protected void setFocusAuthCode(String authCode) {
    setField(FOCUS_AUTH_CODE_NBR, authCode);
  }

  /** {@inheritDoc} */
  @Override
  protected void setInvoiceNbr(IAuthorizableLineItem argLine, String argInvoiceNbr) {
    setField(POS_TRAN_NBR, argInvoiceNbr);
  }

  protected void setActionCode(String argCode) {
    setField(ACTION_CODE, argCode);
  }

  protected void setSpdhCode(String argCode) {
    setField(SPDH_CODE, argCode);
  }

}
