//$Id: AjbFiPaySignatureCaptureRequest.java 1056 2016-07-29 22:17:57Z olr.jgaughn $
package cri.ajb.fipay.reqeust;

import cri.ajb.fipay.response.AjbFiPaySignatureCaptureResponse;

import dtv.i18n.IFormattable;
import dtv.tenderauth.AuthRequestType;
import dtv.tenderauth.event.IAuthResponse;
import dtv.xst.dao.trl.IAuthorizableLineItem;

/**
 * AJB FiPay signature capture request.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1056 $
 */
public class AjbFiPaySignatureCaptureRequest
    extends AjbFiPayCreditDebitRequest {

  protected static final int SIG_CAP_MSG_NBR = 13;
  protected static final int TERMINAL_ID_NBR = 7;

  /**
   * 
   * @param argType
   * @param argLine
   */
  public AjbFiPaySignatureCaptureRequest(AuthRequestType argType, IAuthorizableLineItem argLine) {
    super(argType, argLine, null);

    //these fields are no needed for sig cap request
    setField(TERMINAL_ID_NBR, null);
    setField(POS_TRAN_NBR, null);
    setField(AMOUNT, null);

    //this filed will be setup in AuthConfig.xml
    setField(AUTH_REQUEST_TYPE, null);

    setField(SIG_CAP_MSG_NBR, "Cancel");

  }

  /** {@inheritDoc} */
  @Override
  public void loadSensitiveData() {
    /* Superclass is putting sensitive data such as the card number in a field that ends up being
     * displayed to the user. This is because field 13 on a credit request is the account number,
     * but is a message to the user on a signature request. Head that off here by overriding this
     * method and doing nothing. */
  }

  /** {@inheritDoc} */
  @Override
  public String getMessageString() {
    StringBuffer buf = new StringBuffer(100);

    for (int i = 1; i <= getMaxField(); i++ ) {
      if (getField(i) != null) {

        buf.append(getField(i));

      }

      if (i < getMaxField()) {
        buf.append(",");
      }
    }

    return buf.toString();
  }

  /** {@inheritDoc} */
  @Override
  public IAuthResponse getResponse(String[] argFields, String argAuthCode) {
    return new AjbFiPaySignatureCaptureResponse(this, argFields, null);
  }

  /** {@inheritDoc} */
  @Override
  public IFormattable getMessage(String argMessageId) {
    if ("SIG".equals(argMessageId)) {
      return FF.getTranslatable("_ajbCommunicatorSigCap");
    }
    else {
      return super.getMessage(argMessageId);
    }
  }

}
