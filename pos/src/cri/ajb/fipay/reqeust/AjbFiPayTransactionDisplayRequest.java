//$Id: AjbFiPayTransactionDisplayRequest.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.reqeust;

import cri.ajb.fipay.response.AjbFiPayTransactionDisplayResponse;
import cri.ajb.state.DeviceTranState;
import cri.pos.common.CriConfigurationMgr;

import dtv.i18n.FormattableFactory;
import dtv.i18n.OutputContextType;
import dtv.tenderauth.AuthRequestType;
import dtv.tenderauth.event.IAuthResponse;
import dtv.util.StringUtils;

/**
 * AJB transaction rolling receipt request.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayTransactionDisplayRequest
    extends AjbFiPayGenericRequest
    implements IAjbFiPayTransactionDisplayRequest {
  private DeviceTranState transState_;
  protected static final FormattableFactory FF = FormattableFactory.getInstance();

  protected static final int HEADER_NBR = 13;
  protected static final int SUMMARY_NBR = 20;
  protected static final int LIST_TEXT_NBR = 22;

  /**
   * Constructor method
   * 
   * @param argType
   * @param argData
   */
  public AjbFiPayTransactionDisplayRequest(AuthRequestType argType, Object[] argData) {
    super(argType, argData);
    if (argData != null) {
      transState_ = (DeviceTranState) argData[0];
    }

    if (transState_ != null) {

      int startIndex = LIST_TEXT_NBR;
      for (int i = 0; i < transState_.getItems().length; i++ ) {
        setField(startIndex++ , transState_.getItems()[i]);
      }
    }

    setField(HEADER_NBR,
        FF.getSimpleFormattable("_ajbReceiptHeader").toString(OutputContextType.POLE_DISPLAY));

    String transSummary = null;
    String subTotalText = FF.getSimpleFormattable("_subtotal").toString(OutputContextType.POLE_DISPLAY);
    String totalText = FF.getSimpleFormattable("_total").toString(OutputContextType.POLE_DISPLAY);
    String taxText = FF.getSimpleFormattable("_tax").toString(OutputContextType.POLE_DISPLAY);
    //AC#457634-FB#365600 - CRI - 9.1.5 - Pin pad displaying in wrong order.
    if (transState_ != null) {
      transSummary = subTotalText + "|" + convertFormat(transState_.getSubTotal()) + "|" + taxText + "|"
          + convertFormat(transState_.getTax()) + "|" + totalText + "|"
          + convertFormat(transState_.getTotal()) + "|";
    }
    else {
      transSummary = subTotalText + "||" + taxText + "||" + totalText + "||";
    }
    setField(SUMMARY_NBR, transSummary);

  }

  /** {@inheritDoc} */
  @Override
  public IAuthResponse getResponse(String[] argFields, String argResponseMessageId) {
    return new AjbFiPayTransactionDisplayResponse(this, argFields);
  }

  /** {@inheritDoc} */
  @Override
  public DeviceTranState getTransactionState() {
    return transState_;
  }

  /** {@inheritDoc} */
  @Override
  public void addResponse(IAuthResponse argResponse) {
    //don't fire the event
    addResponse(argResponse, false);
  }

  /**
   * Convert display format for the money on device.
   *
   * @param argValue
   * @return
   */
  protected String convertFormat(String argValue) {
    if (!StringUtils.isEmpty(argValue)) {
      return argValue.replace(',', getTokenToReplacement());
    }

    return argValue;
  }

  /**
   * Get the replacement token char.
   *
   * @return
   */
  protected char getTokenToReplacement() {
    return CriConfigurationMgr.getTokenToReplacementComma().charAt(0);
  }
}
