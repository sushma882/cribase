//$Id: IAjbFiPayTransactionDisplayRequest.java 875 2015-07-13 22:12:21Z suz.sxie $
package cri.ajb.fipay.reqeust;

import cri.ajb.state.DeviceTranState;

/**
 * AJB transaction rolling receipt request.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 875 $
 */
public interface IAjbFiPayTransactionDisplayRequest {

  /**
   * Get the rolling receipt transaction data.
   * 
   * @return
   */
  public DeviceTranState getTransactionState();

}
