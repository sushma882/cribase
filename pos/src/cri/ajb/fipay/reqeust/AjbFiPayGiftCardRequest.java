//$Id: AjbFiPayGiftCardRequest.java 1167 2017-09-26 16:41:46Z johgaug $
package cri.ajb.fipay.reqeust;

import static dtv.tenderauth.AuthRequestType.*;
import static dtv.util.NumberUtils.isZero;

import java.math.BigDecimal;

import org.apache.log4j.Logger;

import cri.ajb.fipay.response.AjbFiPayGiftCardResponse;
import cri.pos.tender.CriTenderHelper;

import dtv.i18n.FormattableFactory;
import dtv.i18n.IFormattable;
import dtv.pos.iframework.type.TenderUsageCodeType;
import dtv.pos.tender.TenderHelper;
import dtv.tenderauth.AuthRequestType;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.storedvalue.ajb.AjbGiftCardRequest;
import dtv.util.StringUtils;
import dtv.xst.dao.trl.IAuthorizableLineItem;
import dtv.xst.dao.trl.IVoucherSaleLineItem;

/**
 * AJB FiPay gift card request.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1167 $
 */
public class AjbFiPayGiftCardRequest
    extends AjbGiftCardRequest
    implements IAjbFiPayTenderAuthRequest, ISupportsManualEntry {

  protected static final FormattableFactory FF = FormattableFactory.getInstance();
  protected static final CriTenderHelper TENDER_HELPER = (CriTenderHelper) TenderHelper.getInstance();

  protected static final int OPERATOR_NBR = 24;
  protected static final String OPTION_MANUAL = "*CEM_Manual";

  private static final Logger logger_ = Logger.getLogger(AjbFiPayGiftCardRequest.class);

  /**
   * 
   * @param argType
   * @param argLine
   */
  public AjbFiPayGiftCardRequest(AuthRequestType argType, IAuthorizableLineItem argLine) {
    super(argType, argLine);
    setInvoiceNbr(argLine, TENDER_HELPER.buildInvoiceNbr(argLine));
    setAmount(argType, argLine);
    setOperatorId(argLine);
  }

  /**
   * 
   * @param argType
   * @param argLine
   * @param argTenderUsageCode
   */
  public AjbFiPayGiftCardRequest(AuthRequestType argType, IAuthorizableLineItem argLine,
      TenderUsageCodeType argTenderUsageCode) {
    this(argType, argLine, argTenderUsageCode, true);
  }

  /**
   * 
   * @param argType
   * @param argLine
   * @param argTenderUsageCode
   * @param argNewInvoice
   */
  public AjbFiPayGiftCardRequest(AuthRequestType argType, IAuthorizableLineItem argLine,
      TenderUsageCodeType argTenderUsageCode, boolean argNewInvoice) {
    super(argType, argLine, argTenderUsageCode);
    setOperatorId(argLine);

    if (argNewInvoice) {
      setInvoiceNbr(argLine, TENDER_HELPER.buildInvoiceNbr(argLine));
    }
  }

  protected void setAmount(AuthRequestType argType, IAuthorizableLineItem argLine) {
    if (((argType == ISSUE) || (argType == RELOAD)) && (argLine instanceof IVoucherSaleLineItem)) {
      IVoucherSaleLineItem vsl = (IVoucherSaleLineItem) argLine;
      BigDecimal faceValueAmount = vsl.getFaceValueAmount();

      if (!isZero(faceValueAmount)) {
        setField(AMOUNT, getScaledAmountString(faceValueAmount));
        setAmount(faceValueAmount);
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public IAuthResponse getResponse(String[] argFields, String argAuthCode) {
    return new AjbFiPayGiftCardResponse(this, argFields, argAuthCode);
  }

  /** {@inheritDoc} */
  @Override
  public IFormattable getMessage(String argMessageId) {
    if ("SWIPE".equals(argMessageId)) {
      return FF.getTranslatable("_ajbCommunicatorCardSwip");
    }
    else {
      return super.getMessage(argMessageId);
    }
  }

  /** {@inheritDoc} */
  @Override
  public String getCommand() {
    return getField(MESSAGE_ID);
  }

  /**
   * Set the AJB FiPay request invoice number. This is unique for each transaction.
   * 
   * @param argLine
   * @param argInvoiceNbr
   */
  protected void setInvoiceNbr(IAuthorizableLineItem argLine, String argInvoiceNbr) {
    setField(POS_TRAN_NBR, argInvoiceNbr);
    TENDER_HELPER.setOriginalInvoiceNbr(argLine, argInvoiceNbr);

  }

  /** {@inheritDoc} */
  @Override
  protected String getAuthRequestTypeString(AuthRequestType argType, TenderUsageCodeType argTenderUsageCode) {
    if ((ISSUE == argType) || (ACTIVATE == argType)) {
      return "Activate";
    }
    else if (RELOAD == argType) {
      return "Reload";
    }
    else if (INQUIRE_BALANCE == argType) {
      return "BalanceInquiry";
    }
    else if (TENDER == argType) {
      return "Sale";
    }
    else if (CASH_OUT == argType) {
      return "CashOut";
    }
    else if (AUTH_STATUS == argType) {
      return "AuthInquiry";
    }
    else if ((VOID_ACTIVATE == argType) || (VOID_RELOAD == argType)) {
      return "Sale";
    }
    else if (VOID_TENDER == argType) {
      return "Sale";
    }
    else if (VOID_CASH_OUT == argType) {
      return "CashOut";
    }
    else if (REVERSE == argType) {
      return "Cancel";
    }
    else {
      logger_.warn("unexpected request type: " + argType.getName() + "; using 'Cancel'");
      return "Cancel";
    }
  }

  protected void setOperatorId(IAuthorizableLineItem argLine) {
    if (argLine != null) {
      setField(OPERATOR_NBR, argLine.getCreateUserId());
    }
  }

  /** {@inheritDoc} */
  @Override
  public void setManual() {
    String options = StringUtils.nonNull(getField(AJB_OPTION_FIELD));
    if (!options.contains(OPTION_MANUAL)) {
      if (!options.isEmpty()) {
        options += ' ';
      }
      options += OPTION_MANUAL;
      setField(AJB_OPTION_FIELD, options);
    }
  }

}
