//$Id: AjbFiPayGiftCardBalanceRequest.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.reqeust;

import cri.ajb.fipay.response.AjbFiPayGiftCardResponse;

import dtv.i18n.FormattableFactory;
import dtv.i18n.IFormattable;
import dtv.tenderauth.AuthRequestType;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.storedvalue.ajb.AjbGiftCardBalanceRequest;
import dtv.xst.dao.trl.IAuthorizableLineItem;

/**
 * AJB gift card balance inquiry request.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayGiftCardBalanceRequest
    extends AjbGiftCardBalanceRequest
    implements IAjbFiPayTenderAuthRequest {

  protected static final FormattableFactory FF = FormattableFactory.getInstance();

  /**
   * Constructor
   * 
   * @param argType the type of authorization request
   * @param argLine the line item this request is related to, or <code>null</code> is there is no
   */
  public AjbFiPayGiftCardBalanceRequest(AuthRequestType argType, IAuthorizableLineItem argLine) {
    super(argType, argLine);
    setField(AMOUNT, null);
  }

  /** {@inheritDoc} */
  @Override
  public IFormattable getMessage(String argMessageId) {
    if ("SWIPE".equals(argMessageId)) {
      return FF.getTranslatable("_ajbCommunicatorCardSwip");
    }
    else {
      return super.getMessage(argMessageId);
    }
  }

  /** {@inheritDoc} */
  @Override
  public String getCommand() {
    return getField(MESSAGE_ID);
  }

  /** {@inheritDoc} */
  @Override
  public IAuthResponse getResponse(String[] argFields, String argAuthCode) {
    return new AjbFiPayGiftCardResponse(this, argFields, argAuthCode);
  }

}
