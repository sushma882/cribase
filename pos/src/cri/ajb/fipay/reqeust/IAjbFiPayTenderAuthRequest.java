//$Id: IAjbFiPayTenderAuthRequest.java 877 2015-07-15 18:38:01Z suz.sxie $
package cri.ajb.fipay.reqeust;

/**
 * AJB tender auth request.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 877 $
 */
public interface IAjbFiPayTenderAuthRequest {

  /**
   * Get the AJB command.
   * 
   * @return
   */
  String getCommand();
}
