//$Id: AjbFiPayGenericResponseConverter.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.response;

import dtv.tenderauth.IAuthRequest;
import dtv.tenderauth.IAuthResponseConverter;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.ajb.IAjbRequest;

/**
 * Response converter that converts the result string from AJB into an authorization response
 * object.<br>
 * Copyright (c) 2015 Datavantage Corporation
 * 
 * @author shy xie
 * @version $Revision: 1105 $
 * @created Jun 15, 2005
 */
public class AjbFiPayGenericResponseConverter
    implements IAuthResponseConverter {

  /** {@inheritDoc} */
  @Override
  public IAuthResponse convertResponse(Object argResponseObject, IAuthRequest argOriginalRequest) {
    String responseString = (String) argResponseObject;
    String[] fields = responseString.split(",");

    return ((IAjbRequest) argOriginalRequest).getResponse(fields, null);
  }
}
