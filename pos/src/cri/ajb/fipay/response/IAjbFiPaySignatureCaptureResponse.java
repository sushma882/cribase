//$Id: IAjbFiPaySignatureCaptureResponse.java 875 2015-07-13 22:12:21Z suz.sxie $
package cri.ajb.fipay.response;

/**
 * AJB signature capture response.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 875 $
 */
public interface IAjbFiPaySignatureCaptureResponse {

  /**
   * Get the the signature data.
   * 
   * @return
   */
  public String getSignature();
}
