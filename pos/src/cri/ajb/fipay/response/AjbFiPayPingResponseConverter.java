//$Id: AjbFiPayPingResponseConverter.java 875 2015-07-13 22:12:21Z suz.sxie $
package cri.ajb.fipay.response;

import dtv.tenderauth.IAuthRequest;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.ajb.IAjbRequest;

/**
 * AJB ping response converter.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 875 $
 */
public class AjbFiPayPingResponseConverter
    extends AjbFiPayGenericResponseConverter {

  /** {@inheritDoc} */
  @Override
  public IAuthResponse convertResponse(Object argResponseObject, IAuthRequest argOriginalRequest) {
    IAuthResponse response = null;
    if (argResponseObject == null) {
      response = ((IAjbRequest) argOriginalRequest).getResponse(null, null);
      if (response instanceof AjbFiPayPingResponse) {
        ((AjbFiPayPingResponse) response).setOffline(true);
      }
    }
    else {
      response = super.convertResponse(argResponseObject, argOriginalRequest);
    }

    return response;
  }
}
