//$Id: AjbFiPayTransactionDisplayResponse.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.response;

import dtv.tenderauth.IAuthRequest;

/**
 * AJB transaction rolling receipt response.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayTransactionDisplayResponse
    extends AjbFiPayGenericResponse {

  /**
   * 
   */
  private static final long serialVersionUID = -6537236011677660625L;

  /**
   * Constructor method.
   *
   * @param argRequest
   * @param argFields
   */
  public AjbFiPayTransactionDisplayResponse(IAuthRequest argRequest, String[] argFields) {
    super(argRequest, argFields);
  }
}
