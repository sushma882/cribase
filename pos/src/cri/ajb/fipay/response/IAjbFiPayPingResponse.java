//$Id: IAjbFiPayPingResponse.java 875 2015-07-13 22:12:21Z suz.sxie $
package cri.ajb.fipay.response;

import dtv.tenderauth.event.IAuthResponse;

/**
 * AJB ping response.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @version $Revision: 875 $
 * @created Jun 17, 2015
 */
public interface IAjbFiPayPingResponse
    extends IAuthResponse {

  /**
   * get offline flag
   * 
   * @return
   */
  public boolean isOffline();

  /**
   * set offline flag
   * 
   * @param argOffline
   */
  public void setOffline(boolean argOffline);
}
