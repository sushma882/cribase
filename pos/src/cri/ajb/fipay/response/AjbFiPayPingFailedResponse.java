//$Id: AjbFiPayPingFailedResponse.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.response;

import dtv.i18n.IFormattable;
import dtv.tenderauth.IAuthProcess;
import dtv.tenderauth.IAuthRequest;
import dtv.tenderauth.impl.event.AbstractAuthResponse;

/**
 * AJB ping failed response.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @version $Revision: 1105 $
 * @created Jun 17, 2015
 */
public class AjbFiPayPingFailedResponse
    extends AbstractAuthResponse {

  /**
   * 
   */
  private static final long serialVersionUID = -101002268640119826L;

  /**
   * Constructor for the AbstractAuthResponse object.
   * 
   * @param argAuthProcess the authorization process that generated this response.
   * @param argRequest the request that triggered this response.
   */
  public AjbFiPayPingFailedResponse(IAuthProcess argAuthProcess, IAuthRequest argRequest,
      IFormattable argMessage) {
    super(argAuthProcess, argRequest, null);
    setMessage(argMessage);
    allowCancel();
  }
}
