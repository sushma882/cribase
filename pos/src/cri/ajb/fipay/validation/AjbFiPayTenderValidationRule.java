//$Id: AjbFiPayTenderValidationRule.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.validation;

import java.util.List;

import com.micros_retail.util.StringUtils;

import cri.ajb.fipay.AjbUtils;

import dtv.i18n.IFormattable;
import dtv.pos.ejournal.postvoid.validation.TenderValidationRule;
import dtv.pos.iframework.validation.*;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;

/**
 * AJB tender void validation for post void transaction.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayTenderValidationRule
    extends TenderValidationRule {

  /** {@inheritDoc} */
  @Override
  public IValidationResult validate(IValidationData argObject) {
    IValidationResult result = super.validate(argObject);

    if (result.isValid()) {
      List<ICreditDebitTenderLineItem> lineItems =
          ((IPosTransaction) argObject.getObject()).getLineItems(ICreditDebitTenderLineItem.class);

      for (ICreditDebitTenderLineItem lineItem : lineItems) {
        if (lineItem.getVoid()) {
          continue;
        }

        if (StringUtils.isEmpty(lineItem.getAuthorizationToken())
            || AjbUtils.getInstance().isTempToken(lineItem.getAuthorizationToken())) {
          IFormattable tenderName = FF.getLiteral(lineItem.getTender().getDescription());
          IFormattable message = FF.getTranslatable("_tenderWithoutTokenNotSupported", tenderName);
          result = SimpleValidationResult.getFailed(message);
        }
      }

      return result;
    }
    else {
      return result;
    }
  }
}
