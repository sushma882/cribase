//$Id: AjbFiPayPromptOutgoingTenderExchangeAmountOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.exchange.op;

import java.util.ArrayList;
import java.util.List;

import dtv.i18n.*;
import dtv.pos.common.PromptKey;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.iframework.ui.config.IPromptKey;
import dtv.pos.iframework.validation.IValidationKey;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.pos.tender.exchange.PromptOutgoingTenderExchangeAmountOp;
import dtv.util.config.IConfigObject;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * Captures and validates the amount of the outgoing Tender in a Tender Exchange.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created June 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayPromptOutgoingTenderExchangeAmountOp
    extends PromptOutgoingTenderExchangeAmountOp {
  /**
   * 
   */
  private static final long serialVersionUID = -6641547434693943654L;
  private static final FormattableFactory FF = FormattableFactory.getInstance();
  private final List<String> tenderIds_ = new ArrayList<String>();

  /**
   * Is cash only allowed.
   * 
   * @param argCmd
   * @return
   */
  protected boolean isCashOnly(IXstCommand argCmd) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    IPosTransaction tran = cmd.getTransaction();
    List<ITenderLineItem> tenderLineItems = new ArrayList<ITenderLineItem>();

    for (ITenderLineItem tenderLineItem : tran.getLineItems(ITenderLineItem.class)) {
      if (!tenderLineItem.getVoid()) {
        tenderLineItems.add(tenderLineItem);
      }
    }

    return ((tenderLineItems.size() == 1) && tenderIds_.contains(tenderLineItems.get(0).getTenderId()));
  }

  /** {@inheritDoc} */
  @Override
  public IValidationKey[] getValidationKeys(IXstCommand argCmd) {
    if (isCashOnly(argCmd) && (((ISaleTenderCmd) argCmd).getTenderAmount() != null)) {
      return null;
    }
    else {
      return super.getValidationKeys(argCmd);
    }
  }

  /** {@inheritDoc} */
  @Override
  protected IPromptKey getPromptKey(IXstCommand argCmd) {

    if (isCashOnly(argCmd)) {
      return PromptKey.valueOf("OUTGOING_TENDER_EXCHANGE_AMOUNT_NOTIFY");
    }
    else {
      return super.getPromptKey(argCmd);
    }
  }

  /** {@inheritDoc} */
  @Override
  public void setParameter(String argName, IConfigObject argValue) {
    if ("TenderId".equalsIgnoreCase(argName)) {
      tenderIds_.add(argValue.toString());
    }
    else {
      super.setParameter(argName, argValue);
    }
  }

  @Override
  protected IOpResponse getPromptResponse(IXstCommand argCmd, IXstEvent argEvent, IFormattable[] promptArgs) {

    if (isCashOnly(argCmd)) {

      ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
      IPosTransaction tran = cmd.getTransaction();
      cmd.setTenderAmount(tran.getAmountDue());
      IFormattable amount = FF.getSimpleFormattable(cmd.getTenderAmount(), FormatterType.MONEY);

      return HELPER.getPromptResponse(getPromptKey(cmd), new IFormattable[] {amount});
    }
    else {
      return super.getPromptResponse(argCmd, argEvent, getPromptArgs(argCmd, argEvent));
    }

  }
}
