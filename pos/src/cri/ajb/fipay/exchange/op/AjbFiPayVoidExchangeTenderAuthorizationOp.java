//$Id: AjbFiPayVoidExchangeTenderAuthorizationOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.exchange.op;

import cri.pos.tender.exchange.CriVoidTenderAuthorizationOp;

import dtv.pos.tender.ISaleTenderCmd;
import dtv.tenderauth.*;
import dtv.tenderauth.op.IAuthorizationCmd;
import dtv.xst.dao.ttr.IAuthorizableTenderLineItem;
import dtv.xst.dao.ttr.IVoucherTenderLineItem;

/**
 * An operation which handles authorizing the void of voucher tenders involved in a tender exchange.
 * <br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created June 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayVoidExchangeTenderAuthorizationOp
    extends CriVoidTenderAuthorizationOp {

  /**
   * 
   */
  private static final long serialVersionUID = -4984856478720754836L;

  /** {@inheritDoc} */
  @Override
  protected IAuthRequest getAuthRequest(IAuthorizationCmd argCmd) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    IAuthorizableTenderLineItem tli = (IAuthorizableTenderLineItem) cmd.getTenderLineItem();
    String authMethodCode = getAuthMethodCode(tli);
    AuthRequestType authReqType = getAuthReqType(tli);

    boolean isVoucher = (tli instanceof IVoucherTenderLineItem);
    IAuthRequest request =
        AuthFactory.getInstance().makeAuthRequest(authMethodCode, authReqType, tli, isVoucher);

    return request;
  }
}
