//$Id: AjbFiPayOutgoingTenderRouteOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.exchange.op;

import java.util.ArrayList;
import java.util.List;

import dtv.pos.common.OpChainKey;
import dtv.pos.framework.action.type.XstChainActionType;
import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.*;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.util.NumberUtils;
import dtv.util.config.IConfigObject;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * Out going tender router.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created June 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayOutgoingTenderRouteOp
    extends Operation {
  /**
   * 
   */
  private static final long serialVersionUID = 4141083302154312848L;
  private final List<String> tenderIds_ = new ArrayList<String>();

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEVent) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    IPosTransaction trans = cmd.getTransaction();

    for (ITenderLineItem tenderLineItem : trans.getLineItems(ITenderLineItem.class)) {
      if (!tenderLineItem.getVoid() && tenderIds_.contains(tenderLineItem.getTenderId())
          && !NumberUtils.isZero(trans.getAmountDue())) {
        return HELPER.getRunChainResponse(OpStatus.COMPLETE_HALT,
            OpChainKey.valueOf("DIRECT_OUTGOING_TENDER_EXCHANGE_LOCAL_CURRENCY"), argCmd,
            XstChainActionType.START);

      }
    }

    return HELPER.completeResponse();
  }

  /** {@inheritDoc} */
  @Override
  public void setParameter(String argName, IConfigObject argValue) {
    if ("TenderId".equalsIgnoreCase(argName)) {
      tenderIds_.add(argValue.toString());
    }
    else {
      super.setParameter(argName, argValue);
    }
  }

}
