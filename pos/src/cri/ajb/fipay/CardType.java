//$Id: CardType.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * AJB FiPay tender card type.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class CardType {
  private static final Logger logger_ = Logger.getLogger(CardType.class);

  public static final CardType AMEX = new CardType("AMERICAN_EXPRESS", "AMEX");
  public static final CardType MASTERCARD = new CardType("MASTERCARD", "MASTERCARD");
  public static final CardType DISCOVER = new CardType("DISCOVER", "DISCOVER");
  public static final CardType VISA = new CardType("VISA", "VISA");
  public static final CardType JCB = new CardType("JCB", "JCB");

  private final String xstoreTender_;

  private final String ajbTender_;

  private static Map<String, CardType> values_;

  protected CardType(String argXstoreTender, String argAjbTender) {
    xstoreTender_ = argXstoreTender;
    ajbTender_ = argAjbTender;

    if (values_ == null) {
      values_ = new HashMap<String, CardType>();
    }
    values_.put(argAjbTender, this);
  }

  public static CardType forName(String argName) {
    if (argName == null) {
      return null;
    }
    CardType found = values_.get(argName.trim().toUpperCase());
    if (found == null) {
      logger_.warn("There is no instance of [" + CardType.class.getName() + "] named [" + argName + "].",
          new Throwable("STACK TRACE"));
    }
    return found;
  }

  protected String getTenderId() {
    return xstoreTender_;
  }

  protected String getAjbTenderId() {
    return ajbTender_;
  }

  public static String getXstoreTender(String argAjbTender) {
    CardType cardType = values_.get(argAjbTender);
    if (cardType != null) {
      return cardType.getTenderId();
    }
    else {
      return null;
    }
  }
}
