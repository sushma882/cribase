//$Id: AjbFiPayDisplayIdleOp.java 875 2015-07-13 22:12:21Z suz.sxie $
package cri.ajb.fipay;

import org.apache.log4j.Logger;

import dtv.util.StringUtils;

/**
 * AJB FiPay utility class.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Sep 24, 2015
 * @version $Revision: 948 $
 */
public class AjbUtils {
  private static final Logger logger_ = Logger.getLogger(AjbUtils.class);
  private static AjbUtils instance_;

  static {
    // Check the system configured factory to use...
    String className = System.getProperty(AjbUtils.class.getName());
    logger_.info("Creating AjbUtils for " + className);
    try {
      instance_ = (AjbUtils) Class.forName(className).newInstance();
    }
    catch (Exception ex) {
      logger_.info("Creating default AjbUtils");
      instance_ = new AjbUtils();
    }
  }

  /**
   * Get the the instance object.
   * 
   * @return
   */
  public static AjbUtils getInstance() {
    return instance_;
  }

  /**
   * Check the temp token key.
   * 
   * @param argToken
   * @return
   */
  public boolean isTempToken(String argToken) {
    return !StringUtils.isEmpty(argToken) && argToken.startsWith("TT");
  }

}
