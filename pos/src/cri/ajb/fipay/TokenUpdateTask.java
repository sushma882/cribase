//$Id: TokenUpdateTask.java 1245 2018-04-26 18:42:45Z johgaug $
package cri.ajb.fipay;

import static dtv.util.CompositeObject.make;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import cri.pos.common.CriConfigurationMgr;
import cri.pos.tender.CriTenderHelper;
import cri.xst.dao.ttr.CriTokenUpdateQueueId;
import cri.xst.dao.ttr.ICriTokenUpdateQueue;

import dtv.data2.access.DataFactory;
import dtv.pos.tender.TenderHelper;
import dtv.util.CompositeObject;
import dtv.util.StringUtils;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;

/**
 * Xstore token update task for the manually authorized tender.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author shy xie
 * @created June 30, 2015
 * @version $Revision: 1245 $
 */
public class TokenUpdateTask {
  private static final Logger logger_ = Logger.getLogger(TokenUpdateTask.class);
  protected static final CriTenderHelper TENDER_HELPER = (CriTenderHelper) TenderHelper.getInstance();

  /*  private int storeNbrField = 0;
  private int registerNbrField = 1;
  private int invoiceNbrField = 2;
  private int datestampField = 3;
  private int timestampField = 4;
  private int tempTokeField = 5;*/
  private int dateField = 3;
  private final int tokenField = 6;
  private final int tokenDataSetLength = 7;
  private final int fileNameDataSetLength = 5;
  
  private String businessDate = null;

  /**
   * Start to run the task.
   */
  public void run(String args[]) {
    if (CriConfigurationMgr.isAjbEnabled()) {
      String path = CriConfigurationMgr.getAjbTokenFilePath();
      if (isFolderExist(path)) {
        Collection<File> tokenFiles = getTokenFiles(path);

        Iterator<File> it = tokenFiles.iterator();
        while (it.hasNext()) {
          File tokenFile = it.next();
          CompositeObject.ThreePiece<String, String, String> token = getToken(tokenFile);
          if (token != null) {
            if (updateToken(token.a(), token.b(), token.c())) {
              logger_.debug("Token is updated in DB.");
              deleteTokenFile(tokenFile);
            }
          }
        }
      }
    }
  }

  /**
   * To check the token file folder.
   * 
   * @param argPath
   * @return
   */
  protected boolean isFolderExist(String argPath) {
    File dir = new File(argPath);

    if (!dir.exists() && !dir.isDirectory()) {
      logger_.error("Token file folder doesn't exist. Path->" + argPath);
      return false;
    }

    return true;
  }

  @SuppressWarnings("unchecked")
  protected Collection<File> getTokenFiles(String argPath) {
    File dir = new File(argPath);
    return FileUtils.listFiles(dir, new String[] {CriConfigurationMgr.getAjbTokenFileExtensionName()}, true);
  }

  /**
   * Get the token with the invoice number.
   * 
   * @param argFile
   * @return
   */
  protected CompositeObject.ThreePiece<String, String, String> getToken(File tokenFile) {

    BufferedReader reader = null;
    try {
      logger_.debug("Reading the token file -> " + tokenFile.getName());
      FileInputStream stream = new FileInputStream(tokenFile);
      reader = new BufferedReader(new InputStreamReader(stream));
      String data = reader.readLine();
      //String token = getToken(data);
      String dataSet[] = getFileDataSet(data);
      if(dataSet == null){
        return null;
      }
      String token = StringUtils.nonNull(dataSet[tokenField]).trim();
      businessDate = StringUtils.nonNull(dataSet[dateField]).trim();
      String fileName = FilenameUtils.getBaseName(tokenFile.getName());
      String invoiceNbr = getInvoiceNbr(fileName);
      
      if (!StringUtils.isEmpty(invoiceNbr)) {
        CompositeObject.ThreePiece<String, String, String> fileToken = make(invoiceNbr, token, fileName);
        return fileToken;
      }

    }
    catch (FileNotFoundException e) {
      logger_.error(e.getMessage());
    }
    catch (IOException e) {
      logger_.error(e.getMessage());
    }
    finally {
      if (reader != null) {
        try {
          reader.close();
        }
        catch (IOException e) {
          logger_.error(e.getMessage());
        }
      }
    }

    return null;
  }

  /**
   * Reading Invoice number from file name
   * @param argFileName
   * @return
   */
  protected String getInvoiceNbr(String argFileName) {
    String invoiceNbr = null;

    if (!StringUtils.isEmpty(argFileName)) {
      String dataSet[] = argFileName.split("_");
      if (dataSet.length == fileNameDataSetLength) {
        invoiceNbr = StringUtils.nonNull(dataSet[4]).trim();
      }
    }

    return invoiceNbr;
  }

  /**
   * Reading token from the file
   * @param argData
   * @return
   */
  protected String getToken(String argData) {
    String token = null;

    if (!StringUtils.isEmpty(argData)) {
      String dataSet[] = argData.split(",");
      if (dataSet.length == tokenDataSetLength) {
        token = StringUtils.nonNull(dataSet[tokenField]).trim();
      }
    }

    return token;
  }
  
  /**
   * Reading data set from the file
   * @param argData
   * @return
   */
  protected String[] getFileDataSet(String argData) {
    if (!StringUtils.isEmpty(argData)) {
      String dataSet[] = argData.split(",");
      if (dataSet.length == tokenDataSetLength) {
        return dataSet;
      }
    }
    return null;
  }

  /**
   * Update the token in DB.
   * 
   * @param invoiceNbr
   * @param argToken
   * @return
   */
  protected boolean updateToken(String invoiceNbr, String argToken, String fileName) {
    CriTokenUpdateQueueId id = new CriTokenUpdateQueueId();
    id.setInvoiceNbr(invoiceNbr);

    if (!StringUtils.isEmpty(argToken)) {
      ICriTokenUpdateQueue updateQueue = DataFactory.getObjectByIdNoThrow(id);
      if (updateQueue == null) {
        logger_.debug("No record found in cri_token_update_queue table for InvoiceNbr-->" + invoiceNbr);
        ICreditDebitTenderLineItem tenderLineItem = getCreditDebitTenderLineItem(invoiceNbr);
        if (tenderLineItem != null) {
          updateQueue = DataFactory.createObject(ICriTokenUpdateQueue.class);
          updateQueue.setOrganizationId(tenderLineItem.getOrganizationId());
          updateQueue.setInvoiceNbr(TENDER_HELPER.getOriginalInvoiceNbr(tenderLineItem));
          updateQueue.setRetailLocationId(tenderLineItem.getRetailLocationId());
          updateQueue.setWorkstationId(tenderLineItem.getWorkstationId());
          updateQueue.setTransactionSequence(tenderLineItem.getTransactionSequence());
          updateQueue.setBusinessDate(tenderLineItem.getBusinessDate());
          updateQueue.setRetailTransactionLineItemSequence(tenderLineItem
              .getRetailTransactionLineItemSequence());
          updateQueue.setUpdated(true);
          tenderLineItem.setAuthorizationToken(argToken);

          DataFactory.makePersistent(updateQueue);
          DataFactory.makePersistent(tenderLineItem);
          return true;
        }
        else {
          logger_.error("Token update failed : No record found in ttr_credit_debit_tndr_lineitm table for file-->" + fileName);
        }
      }
      else {
        logger_.debug("Record found in cri_token_update_queue table for InvoiceNbr-->" + invoiceNbr);
        ICreditDebitTenderLineItem creditDebitTenderLineItem = updateQueue.getCreditDebitTenderLineItem();
        if (creditDebitTenderLineItem != null) {
          creditDebitTenderLineItem.setAuthorizationToken(argToken);

          updateQueue.setUpdated(true);
          DataFactory.makePersistent(updateQueue);
          DataFactory.makePersistent(creditDebitTenderLineItem);
          return true;
        }
      }
    }

    return false;
  }

  /**
   * Delete the local token file.
   * 
   * @param argFile
   */
  protected void deleteTokenFile(File argFile) {
    if (argFile.delete()) {
      logger_.debug("File " + argFile.getName() + " has been deleted.");
    }
    else {
      logger_.debug("Delete operation is failed.");
    }
  }

  /**
   * Reads credit debit tender line item
   * @param invoiceNbr
   * @param fileName
   * @return
   */
  protected dtv.xst.dao.ttr.ICreditDebitTenderLineItem getCreditDebitTenderLineItem(String invoiceNbr) {

    long retailLocationId = TENDER_HELPER.getInvoiceId(invoiceNbr, CriTenderHelper.RETAIL_LOCATION_ID);
    long workStationId = TENDER_HELPER.getInvoiceId(invoiceNbr, CriTenderHelper.WORKSTATION_ID);
    long tranId = TENDER_HELPER.getInvoiceId(invoiceNbr, CriTenderHelper.TRANSACTION_ID);
    long lineItemSeqId = TENDER_HELPER.getInvoiceId(invoiceNbr, CriTenderHelper.LINE_ITEM_SEQ_ID);
    Date businessDateFormatted = getBusinessDate();
    logger_.debug("Reading ttr_credit_debit_tndr_lineitm for input-->RetailLocationId=" + retailLocationId
        + ",WorkStationId=" + workStationId + ",TranId=" + tranId + ",LineItemSeqId=" + lineItemSeqId
        + ",BusinessDate=" + businessDateFormatted);

    dtv.xst.dao.trl.RetailTransactionLineItemId id = new dtv.xst.dao.trl.RetailTransactionLineItemId();
    id.setRetailLocationId(retailLocationId);
    id.setWorkstationId(workStationId);
    id.setBusinessDate(businessDateFormatted);
    id.setTransactionSequence(tranId);
    id.setRetailTransactionLineItemSequence((int) lineItemSeqId);

    dtv.xst.dao.trl.IRetailTransactionLineItem lineItem =
        dtv.data2.access.DataFactory.getObjectByIdNoThrow(id);
    if (lineItem instanceof dtv.xst.dao.ttr.ICreditDebitTenderLineItem) {
      logger_.debug("Record found in ttr_credit_debit_tndr_lineitm table");
      return (dtv.xst.dao.ttr.ICreditDebitTenderLineItem) lineItem;
    }
    else {
      logger_.debug("No record found in ttr_credit_debit_tndr_lineitm table");
      return null;
    }
  }

  /**
   * Reads the business date from the file content
   * @return
   */
  protected Date getBusinessDate() {
    Date busDate = null;
    try {
      if (!StringUtils.isEmpty(businessDate)) {
        SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyy");
        busDate = sdf.parse(businessDate);
      }
    }
    catch (ParseException ex) {
      ex.printStackTrace();
    }
    return busDate;
  }
}
