//$Id: AjbFiPayGenericProcess.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.impl;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import cri.ajb.fipay.reqeust.AjbFiPayGenericRequest;

import dtv.hardware.auth.UserCancelledException;
import dtv.i18n.IFormattable;
import dtv.i18n.OutputContextType;
import dtv.tenderauth.*;
import dtv.tenderauth.config.AuthFailedActionTypesConfig;
import dtv.tenderauth.event.*;
import dtv.tenderauth.impl.AbstractAuthProcess;
import dtv.tenderauth.impl.event.AuthSuccessResponse;
import dtv.tenderauth.impl.event.AuthTenderFailedResponse;

/**
 * AJB generic process.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayGenericProcess
    extends AbstractAuthProcess
    implements Runnable {

  private static final Logger logger_ = Logger.getLogger(AjbFiPayGenericProcess.class);
  protected static final Logger adminLogger_ = Logger.getLogger("dtv.xstore.comm.paysys");

  private IAuthRequest request_;

  private static final int STAT_CLEAR = 0;
  private static final int STAT_LOCKED = 1;
  private static final int STAT_PROCESSING = 2;

  private static int threadIndex_ = 0;

  private int status_ = STAT_CLEAR;
  private final AtomicBoolean locked_ = new AtomicBoolean();
  private final Lock startLock_ = new ReentrantLock();

  private final IAuthStatusListener loggingStatusListener_ = new IAuthStatusListener() {

    /** {@inheritDoc} */
    @Override
    public void statusChanged(IAuthStatus argStatus) {
      getAuthLog().debug("STATUS(" + argStatus.getPercentComplete() + "):"
          + argStatus.getMessage().toString(OutputContextType.LOG));
    }
  };

  /** {@inheritDoc} */
  @Override
  public void cancelRequest(IAuthRequest argRequest) {
    //do nothing this AJB request cannot be cancelled.
  }

  /** {@inheritDoc} */
  @Override
  public IAuthRequest getManualAuthInfo(IAuthRequest argRequest) {
    //do noting this request is not available for manual process.
    return argRequest;
  }

  /** {@inheritDoc} */
  @Override
  public void processRequest(IAuthRequest argRequest) {
    if (argRequest == null) {
      throw new NullPointerException("null request");
    }

    if (locked_.get()
        && (!(argRequest instanceof ISilentForwardRequest) && !(request_ instanceof ISilentForwardRequest))
        && (status_ == STAT_PROCESSING)) {
      // throw new IllegalStateException("BUSY::" + status_);
      logger_.error("BUSY::" + status_);
    }
    startLock_.lock();
    try {
      if (locked_.get()) {
        synchronized (locked_) {
          try {
            locked_.wait(1000);
          }
          catch (InterruptedException ex) {
            // throw new IllegalStateException("BUSY", ex);
            logger_.error("BUSY::" + status_);
          }
        }
        if (locked_.get()) {
          // throw new IllegalStateException("BUSY");
          logger_.error("BUSY::" + status_);
        }
      }
      locked_.set(true);
      status_ = STAT_LOCKED;
      request_ = argRequest;
      request_.removeStatusListener(loggingStatusListener_);
      request_.addStatusListener(loggingStatusListener_);
      Thread t = new Thread(this, "AuthProcess[" + getAuthMethodCode() + "]-" + ++threadIndex_);
      t.start();
    }
    finally {
      startLock_.unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public final void run() {
    try {
      status_ = STAT_PROCESSING;
      doProcess(request_);
    }
    catch (Throwable ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
      request_.addResponse(handleException(request_));
    }
    finally {
      clearLock();
    }
  }

  protected void doProcess(final IAuthRequest argRequest) {
    IAuthResponse response = null;

    if (!isValidRequest(argRequest)) {
      logger_.warn("invalid request [" + argRequest + "] for " + getAuthMethodCode());
      response = handleException(argRequest);
    }
    else if (isOffline()) {
      logger_.info(getClass().getName() + ".handleOffline");
      response = handleOffline(argRequest);
    }
    else {
      logger_.info(getClass().getName() + ".handleOnline");
      response = handleOnline(argRequest);
    }

    logger_.info(getClass().getName() + "->response::" + toLogString(response));
    argRequest.addResponse(response);
  }

  protected IAuthResponse handleException(IAuthRequest argRequest) {
    return getFailedResponse(argRequest, argRequest, getMessageKeyException(argRequest));
  }

  protected IAuthResponse getFailedResponse(IAuthRequest argRequest, Object argMessageTarget,
      String... argMessageKey) {

    IFormattable message = getMessage(argMessageTarget, argMessageKey);
    IAuthResponse response = new AuthTenderFailedResponse(this, argRequest, message);
    AuthFailedActionTypesConfig c = getAuthFailedActionTypesConfig(argMessageKey);
    AuthFailedActionType[] availableActions = getAuthFailedActionTypes(c, response);
    response.setAvailableActions(availableActions);
    response.setDataActionGroup(c.getDataActionGroupKey());
    return response;
  }

  protected boolean isValidRequest(IAuthRequest argRequest) {
    return argRequest instanceof AjbFiPayGenericRequest;
  }

  protected IAuthResponse handleOffline(IAuthRequest argRequest) {
    return new AuthSuccessResponse(this, argRequest, getAuthMethodCode(), null);
  }

  protected IAuthResponse handleOnline(IAuthRequest argRequest) {
    try {
      logger_.info(getClass().getName() + ".handleOnline->sendRequest");
      return getAuthCommunicator().sendRequest(argRequest);
    }
    catch (ReceiveTimeoutException ex) {
      // log problem
      getAuthLog().warn("Timeout waiting for response: " + ex);
      adminLogger_.error("Timeout Waiting For Response: " + ex);
      return handleReceiveTimeout(argRequest);
    }
    catch (OfflineException ex) {
      // log problem
      getAuthLog().warn("Host offline: " + ex);
      adminLogger_.error("Payment Systems Host Offline: " + ex);
      return handleOffline(argRequest);
    }
    catch (UserCancelledException ex) {
      return handleUserCancel(argRequest);
    }
  }

  protected IAuthResponse handleReceiveTimeout(IAuthRequest argRequest) {
    return handleOffline(argRequest);
  }

  protected IAuthResponse handleUserCancel(IAuthRequest argRequest) {
    return getFailedResponse(argRequest, argRequest, getMessageKeyUserCancel(argRequest));
  }

  protected String toLogString(IAuthResponse argResponse) {
    StringBuilder sb = new StringBuilder();
    sb.append(argResponse.getClass().getName());
    appendNonNull(sb, "success", argResponse.isSuccess());
    appendNonNull(sb, "responseCode", argResponse.getResponseCode());
    appendNonNull(sb, "type", argResponse.getType());
    return sb.toString();
  }

  protected void appendNonNull(StringBuilder sb, String argTag, Object argValue) {
    if (argValue == null) {
      return;
    }
    sb.append(";");
    sb.append(argTag);
    sb.append("=");
    sb.append(safeToString(argValue));
  }

  protected String safeToString(Object o) {
    if (o instanceof IFormattable) {
      return ((IFormattable) o).toString(OutputContextType.LOG);
    }
    return String.valueOf(o);
  }

  protected void clearLock() {
    locked_.set(false);
    status_ = STAT_CLEAR;
    synchronized (locked_) {
      locked_.notify();
    }
  }

  /** {@inheritDoc} */
  @Override
  public boolean isManualAuthAllowed(IAuthResponse argResponse) {
    return false;
  }

}
