//$Id: AjbFiPayPingProcess.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.impl;

import org.apache.log4j.Logger;

import cri.ajb.fipay.response.AjbFiPayPingFailedResponse;
import cri.ajb.fipay.response.IAjbFiPayPingResponse;

import dtv.hardware.auth.UserCancelledException;
import dtv.i18n.IFormattable;
import dtv.tenderauth.*;
import dtv.tenderauth.config.AuthFailedActionTypesConfig;
import dtv.tenderauth.event.IAuthResponse;

/**
 * AJB ping process.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayPingProcess
    extends AjbFiPayGenericProcess {
  private static final Logger logger_ = Logger.getLogger(AjbFiPayPingProcess.class);

  /** {@inheritDoc} */
  @Override
  protected IAuthResponse handleOnline(IAuthRequest argRequest) {
    try {
      logger_.info(getClass().getName() + ".handleOnline->sendRequest");
      IAjbFiPayPingResponse response = (IAjbFiPayPingResponse) getAuthCommunicator().sendRequest(argRequest);
      if (response.isOffline()) {
        return handleOffline(argRequest);
      }
      else {
        return response;
      }
    }
    catch (ReceiveTimeoutException ex) {
      // log problem
      getAuthLog().warn("Timeout waiting for response: " + ex);
      adminLogger_.error("Timeout Waiting For Response: " + ex);
      return handleReceiveTimeout(argRequest);
    }
    catch (OfflineException ex) {
      // log problem
      getAuthLog().warn("Host offline: " + ex);
      adminLogger_.error("Payment Systems Host Offline: " + ex);
      return handleOffline(argRequest);
    }
    catch (UserCancelledException ex) {
      return handleUserCancel(argRequest);
    }
  }

  /** {@inheritDoc} */
  @Override
  protected IAuthResponse handleOffline(IAuthRequest argRequest) {
    return getFailedResponse(argRequest, argRequest, getMessageKeyOffline(argRequest));
  }

  /** {@inheritDoc} */
  @Override
  protected IAuthResponse getFailedResponse(IAuthRequest argRequest, Object argMessageTarget,
      String... argMessageKey) {

    IFormattable message = getMessage(argMessageTarget, argMessageKey);
    IAuthResponse response = new AjbFiPayPingFailedResponse(this, argRequest, message);
    AuthFailedActionTypesConfig c = getAuthFailedActionTypesConfig(argMessageKey);
    AuthFailedActionType[] availableActions = getAuthFailedActionTypes(c, response);
    response.setAvailableActions(availableActions);
    response.setDataActionGroup(c.getDataActionGroupKey());
    return response;
  }

}
