// $Id: AjbFiPaySignatureCaptureAuthProcess.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.impl;

import java.util.*;

import org.apache.log4j.Logger;

import cri.ajb.fipay.reqeust.AjbFiPaySignatureCaptureRequest;

import dtv.hardware.auth.UserCancelledException;
import dtv.tenderauth.*;
import dtv.tenderauth.config.AuthFailedActionTypesConfig;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.AbstractThreadedAuthProcess;
import dtv.tenderauth.impl.ajb.IAjbResponse;

/**
 * Signature capture processor.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created June 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPaySignatureCaptureAuthProcess
    extends AbstractThreadedAuthProcess {
  protected static final Logger adminLogger_ = Logger.getLogger("dtv.xstore.comm.paysys");

  private static final Logger logger_ = Logger.getLogger(AjbFiPaySignatureCaptureAuthProcess.class);

  /** {@inheritDoc} */
  @Override
  protected boolean isValidRequest(ITenderAuthRequest argRequest) {
    return argRequest instanceof AjbFiPaySignatureCaptureRequest;
  }

  /** {@inheritDoc} */
  @Override
  protected IAuthResponse handleOnline(ITenderAuthRequest argRequest) {
    try {
      logger_.info(getClass().getName() + ".handleOnline->sendRequest");
      IAuthResponse response = getAuthCommunicator().sendRequest(argRequest);

      if (!response.isSuccess()) {
        List<String> ids = new ArrayList<String>();
        ids.addAll(Arrays.asList(argRequest.getMessageKeys(response)));
        ids.add(getUnknownResponseMessageId());
        response.setMessage(getMessage(response, ids));
        getAuthFailedActions(response);
      }

      return response;
    }
    catch (ReceiveTimeoutException ex) {
      // log problem
      getAuthLog().warn("Timeout waiting for response: " + ex);
      adminLogger_.error("Timeout Waiting For Response: " + ex);
      return handleReceiveTimeout(argRequest);
    }
    catch (OfflineException ex) {
      // log problem
      getAuthLog().warn("Host offline: " + ex);
      adminLogger_.error("Payment Systems Host Offline: " + ex);
      return handleOffline(argRequest);
    }
    catch (UserCancelledException ex) {
      return handleUserCancel(argRequest);
    }
  }

  /** {@inheritDoc} */
  @Override
  protected void getAuthFailedActions(IAuthResponse response) {
    int actionCode = ((IAjbResponse) response).getActionCode();
    AuthFailedActionTypesConfig c = getAuthFailedActionTypesConfig(actionCode);
    AuthFailedActionType[] availableActions = getAuthFailedActionTypes(c, response);
    response.setAvailableActions(availableActions);
    response.setDataActionGroup(c.getDataActionGroupKey());
  }

}
