//$Id: AjbFiPayCreditDebitAuthProcess.java 1263 2018-10-11 23:00:21Z johgaug $
package cri.ajb.fipay.impl;

import java.io.ByteArrayInputStream;
import java.lang.reflect.Field;
import java.util.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.*;

import cri.ajb.fipay.response.AjbFiPayCreditDebitResponse;
import cri.pos.tender.CriTenderHelper;

import dtv.data2.access.DataFactory;
import dtv.hardware.auth.UserCancelledException;
import dtv.i18n.config.IFormattableConfig;
import dtv.tenderauth.*;
import dtv.tenderauth.config.AuthFailedActionTypesConfig;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.event.ICreditAuthResponse;
import dtv.tenderauth.impl.ajb.process.AjbCreditAuthProcess;
import dtv.tenderauth.impl.ajb.request.AbstractAjbRequest;
import dtv.tenderauth.impl.event.AuthSuccessResponse;
import dtv.tenderauth.impl.event.RepollResponse;
import dtv.util.ObjectUtils;
import dtv.util.StringUtils;
import dtv.xst.dao.trl.*;
import dtv.xst.dao.ttr.IAuthorizableTenderLineItem;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;

/**
 * Ajb FiPay credit/debit auth processor.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created June 17, 2015
 * @version $Revision: 1263 $
 */
public class AjbFiPayCreditDebitAuthProcess
    extends
    AjbCreditAuthProcess {

  private static final Logger adminLogger_ = Logger.getLogger("dtv.xstore.comm.paysys");
  private static final Logger logger_ = Logger.getLogger(AjbFiPayCreditDebitAuthProcess.class);
  protected static final CriTenderHelper TENDER_HELPER = CriTenderHelper.getInstance();
  private static final String NAME_PREFIX = "*NAME_";

  protected String merchantNum_ = null;

  /** {@inheritDoc} */
  @Override
  protected IAuthResponse handleOnline(ITenderAuthRequest argRequest) {
    IAuthorizableLineItem lineItem = argRequest.getLineItem();

    //remove the base floor limit amount check from here instead with the FiPay floor limit amount check

    try {
      logger_.info(getClass().getName() + ".handleOnline->sendRequest");
      // delaying send request process by half second
      Thread.sleep(500);

      IAuthResponse response = getAuthCommunicator().sendRequest(argRequest);

      if ((argRequest instanceof ICreditAuthRequest) && (response instanceof ICreditAuthResponse)) {
        ICreditAuthResponse resp = (ICreditAuthResponse) response;
        ICreditDebitTenderLineItem tenderLine = (ICreditDebitTenderLineItem) lineItem;
        tenderLine.setPs2000(resp.getPs2000());

        // If there is any PosDataCode, create a line item property to store the value.
        if (resp.getPosDataCode() != null) {
          tenderLine.setStringProperty("POS_DATA_CODE_VALUE", resp.getPosDataCode());
        }

        // update request by real card info.
        ((AbstractAjbRequest) argRequest).setTender(TENDER_HELPER.getTender(response));

        //added for CRI Act# 457634 FB# 369514 and FB# 369516
        //update AuthProcess by actual card brand's messageMap and MerchantNumber
        //updateAuthProcessForManualAuth(argRequest);

      }
      // check if the response requires us to prompt for the cashier to try typing the CID again
      if (response.isReenteringCID()) {
        logger_.info(getClass().getName() + ".getCidReentryResponse");
        return getCidReentryResponse(argRequest);
      }

      // pass the response to updateLineItemForOnline; if we get a RepollResponse
      // send the request back to the communicator; if not, return the response
      while ((response = updateLineItemForOnline(argRequest, response, lineItem)) instanceof RepollResponse) {
        logger_.info(getClass().getName() + ".handleOnline->sendRequest(repoll)");
        response = getAuthCommunicator().sendRequest(((RepollResponse) response).getRepollRequest());
      }
      if (argRequest instanceof ISuspendableAuthRequest) {
        ((ISuspendableAuthRequest) argRequest).setPending(false);
      }
      return response;
    }
    catch (ReceiveTimeoutException ex) {
      // log problem
      getAuthLog().warn("Timeout waiting for response: " + ex);
      adminLogger_.error("Timeout Waiting For Response: " + ex);
      return handleReceiveTimeout(argRequest);
    }
    catch (OfflineException ex) {
      // log problem
      getAuthLog().warn("Host offline: " + ex);
      adminLogger_.error("Payment Systems Host Offline: " + ex);
      return handleOffline(argRequest);
    }
    catch (UserCancelledException ex) {
      return handleUserCancel(argRequest);
    }
    catch (InterruptedException ex) {
      return handleOnline(argRequest);
    }
  }

  /** {@inheritDoc} */
  @Override
  protected IAuthResponse updateLineItemForOnline(ITenderAuthRequest argRequest, IAuthResponse argResponse,
      IAuthorizableLineItem argLineItem) {

    if (argLineItem != null) {
      if (argResponse.isSuccess()) {
        argLineItem.setAuthorizationCode(argResponse.getAuthorizationCode());
        updateLineAmount(argRequest, argResponse, argLineItem);
      }
      argLineItem.setAdjudicationCode(argResponse.getResponseCode());
      copyBankReferenceNumber(argLineItem, argResponse);
      copyTokenInformation(argLineItem, argResponse);
    }
    if (argResponse.isSuccess()) {
      setApprovedMessage(argResponse);
    }

    else {
      List<String> ids = new ArrayList<String>();
      ids.addAll(Arrays.asList(argRequest.getMessageKeys(argResponse)));
      ids.add(getUnknownResponseMessageId());
      argResponse.setMessage(getMessage(argResponse, ids));
      getAuthFailedActions(argResponse);

      /* some action codes indicate the somewhere downstream is offline */
      final List<AuthFailedActionType> types = Arrays.asList(argResponse.getAvailableActions());
      if (types.contains(AuthFailedActionType.AUTO_NEXT_HOST)) {
        argRequest.addResponse(argResponse, /* notify= */false);
        argRequest.setRetryCount(argRequest.getRetryCount() + 1);
        return handleOnline(argRequest);
      }
      else if (types.contains(AuthFailedActionType.MANUAL_AUTH)) {
        // log failure and retry offline
        //AC#457634-FB#365653 - CRI - 9.1.5 - Offline Debit.
        if ((argResponse instanceof AjbFiPayCreditDebitResponse)
            && ((AjbFiPayCreditDebitResponse) argResponse).getCardType().equalsIgnoreCase("Debit")) {
          argRequest.addResponse(argResponse, /* notify= */false);
        }
        else {
          argRequest.addResponse(argResponse, /* notify= */true);
        }
        return handleOffline(argRequest);
      }
      else if (types.contains(AuthFailedActionType.AUTO_REPOLL)) {
        if ((argRequest instanceof ISuspendableAuthRequest) && supportsSuspending()) {
          // start timer for auto repoll
          argRequest.addResponse(argResponse, /* notify= */false);
          return handlePending((ISuspendableAuthRequest) argRequest);
        }
        else {
          final String reason;
          if (supportsSuspending()) {
            // just the request class
            reason = ObjectUtils.getClassNameFromObject(argRequest) + " does not implement "
                + ISuspendableAuthRequest.class.getName();
          }
          else if (argRequest instanceof ISuspendableAuthRequest) {
            // just the processor
            reason = getClass().getName() + " does not support suspending.";
          }
          else {
            // the request class and the processor
            reason = ObjectUtils.getClassNameFromObject(argRequest) + " does not implement "
                + ISuspendableAuthRequest.class.getName() + " and " + getClass().getName()
                + " does not support suspending.";
          }
          logger_.warn(AuthFailedActionType.AUTO_REPOLL + " cannot be used because " + reason);
        }
      }
    }
    if ((argLineItem instanceof IAuthorizableTenderLineItem)
        && (argResponse instanceof AjbFiPayCreditDebitResponse)) {
      IAuthorizableTenderLineItem lineItem = (IAuthorizableTenderLineItem) argLineItem;
      AjbFiPayCreditDebitResponse response = (AjbFiPayCreditDebitResponse) argResponse;
      if (argLineItem instanceof ICreditDebitTenderLineItem) {
        String customerName = response.getCustomerName();
        if (!StringUtils.isEmpty(customerName)) {
          if (customerName.startsWith(NAME_PREFIX)) {
            customerName = customerName.substring(NAME_PREFIX.length());
          }
          ((ICreditDebitTenderLineItem) argLineItem).setCustomerName(customerName);
        }
      }
      parseXml(argRequest, lineItem, response.getProductInfo());
    }
    return argResponse;
  }

  private void parseXml(ITenderAuthRequest argRequest, IAuthorizableTenderLineItem argLineItem,
      String argEmvMessage) {
    try {
      // On a post void, the original tender line comes through. If we add duplicate properties, we
      // get a help desk error.
      AuthRequestType authType = argRequest.getRequestType();
      String type = ((authType == null) ? "" : authType.getName());
      boolean isVoid = type.contains("VOID");

      String emvBadMessage = argEmvMessage.replace("<", "<a").replace("<a/", "</a");
      String emvSource =
          "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><EMVData>" + emvBadMessage + "</EMVData>";
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      factory.setIgnoringElementContentWhitespace(true);
      DocumentBuilder builder = factory.newDocumentBuilder();
      Document doc = builder.parse(new ByteArrayInputStream(emvSource.getBytes()));

      NodeList childNodes = doc.getChildNodes().item(0).getChildNodes();
      for (int i = 0; i < childNodes.getLength(); i++ ) {
        Node currentNode = childNodes.item(i);
        String nodeName = currentNode.getNodeName().substring(1).trim().toUpperCase();
        FipayLineItemProperties property = FipayLineItemProperties.forCode(nodeName);
        if (!property.isEmvTag()) {
          continue;
        }
        NodeList valueNodes = currentNode.getChildNodes();
        for (int j = 0; j < valueNodes.getLength(); j++ ) {
          Node data = valueNodes.item(j);
          if (data.getNodeType() == Node.TEXT_NODE) {
            String nodeValue = data.getNodeValue();
            String name = property.getName();
            if (isVoid) {
              name += "_VOID";
            }
            IRetailTransLineItemProperty prop = createLineItemProperty(argLineItem, name);
            prop.setType("STRING");
            prop.setStringValue(nodeValue);
            argLineItem.addRetailTransLineItemProperty(prop);
          }
        }

      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  protected IRetailTransLineItemProperty createLineItemProperty(IRetailTransactionLineItem argLineItem,
      String argProperytName) {
    // See if the property already exists. If so, use it. Otherwise, create a new DAO.
    IRetailTransLineItemProperty existing = argLineItem.getProperty(argProperytName);
    if (existing != null) {
      return existing;
    }

    RetailTransLineItemPropertyId id = new RetailTransLineItemPropertyId();

    id.setRetailLocationId(Long.valueOf(argLineItem.getRetailLocationId()));
    id.setBusinessDate(argLineItem.getBusinessDate());
    id.setWorkstationId(Long.valueOf(argLineItem.getWorkstationId()));
    id.setTransactionSequence(Long.valueOf(argLineItem.getTransactionSequence()));
    id.setRetailTransactionLineItemSequence(
        Integer.valueOf(argLineItem.getRetailTransactionLineItemSequence()));
    id.setPropertyCode(argProperytName);

    return DataFactory.getInstance().createNewObject(id, IRetailTransLineItemProperty.class);
  }

  /** {@inheritDoc} */
  @Override
  protected IAuthResponse handleOffline(ITenderAuthRequest argRequest) {
    IAuthResponse response = argRequest.getResponses()[argRequest.getResponses().length - 1];
    IAuthResponse returnResponse = null;
    if (response instanceof AjbFiPayCreditDebitResponse) {

      //added for CRI Act# 457634 FB# 369514 and FB# 369516
      //update AuthProcess by actual card brand's messageMap and MerchantNumber
      updateAuthProcessForManualAuth(argRequest);

      if (((AjbFiPayCreditDebitResponse) response).isCreditCard()) {
        if (((AjbFiPayCreditDebitResponse) response).isAboveFloorLimit()) {
          returnResponse = handleCeilingLimitExceeded(argRequest, true);
        }
      }
      else {
        returnResponse = getFailedResponse(argRequest, argRequest, getMessageKeyOffline(argRequest));
        AuthFailedActionTypesConfig c = getAuthFailedActionTypesConfig("DEBIT_OFFLINE");
        AuthFailedActionType[] availableActions = getAuthFailedActionTypes(c, response);
        response.setAvailableActions(availableActions);
        response.setDataActionGroup(c.getDataActionGroupKey());

        List<String> ids = new ArrayList<String>();
        ids.addAll(Arrays.asList(argRequest.getMessageKeys(response)));
        ids.add(getUnknownResponseMessageId());
        returnResponse.setMessage(getMessage(response, ids));
        returnResponse.setAvailableActions(response.getAvailableActions());
      }
    }

    if (returnResponse != null) {
      return returnResponse;
    }
    logger_.info(getClass().getName() + ".getFailedResponse(offline)");
    return getFailedResponse(argRequest, argRequest, getMessageKeyOffline(argRequest));
  }

  /** {@inheritDoc} */
  @Override
  protected IAuthResponse handleCeilingLimitExceeded(ITenderAuthRequest argRequest, boolean argOffline) {
    //    IAuthResponse returnResponse = getFailedResponse(argRequest, argRequest, getMessageKeyOffline(argRequest));
    IAuthResponse response = argRequest.getResponses()[argRequest.getResponses().length - 1];
    List<String> ids = new ArrayList<String>();
    ids.addAll(Arrays.asList(argRequest.getMessageKeys(response)));
    ids.add(getUnknownResponseMessageId());
    response.setMessage(getMessage(response, ids));
    response.setAvailableActions(response.getAvailableActions());

    return response;
  }

  /** {@inheritDoc} */
  @Override
  protected IAuthResponse handleFloorLimitMet(ITenderAuthRequest argRequest, boolean argOffline) {
    IAuthResponse response = argRequest.getResponses()[argRequest.getResponses().length - 1];
    return new AuthSuccessResponse(this, argRequest, getAuthMethodCode(), response.getAuthorizationCode());
  }

  @SuppressWarnings("unchecked")
  protected void updateAuthProcessForManualAuth(ITenderAuthRequest argRequest) {
    String targetTender = argRequest.getTender().getTenderId();

    if (!targetTender.equalsIgnoreCase("DEBIT_CARD")) {
      //create AuthProcess for specific credit card types
      IAuthProcess targetAuthProcess = AuthFactory.getInstance().getAuthProcess("AJB_" + targetTender);
      String merchantNumber = null;
      Map<String, IFormattableConfig> messageMap = null;
      try {
        //get private field MerchantNumber from the target AuthProcess and set it for this AuthProcess
        Field merchantNumberField = null;
        merchantNumberField =
            dtv.tenderauth.impl.AbstractAuthProcess.class.getDeclaredField("merchantNumber_");
        merchantNumberField.setAccessible(true);
        merchantNumber = (String) merchantNumberField.get(targetAuthProcess);
        merchantNumberField.set(this, merchantNumber);

        //get private field MessageMap from the target AuthProcess and set it for this AuthProcess
        Field messageMapField = null;
        messageMapField = dtv.tenderauth.impl.AbstractAuthProcess.class.getDeclaredField("messageMap_");
        messageMapField.setAccessible(true);
        messageMap = (Map<String, IFormattableConfig>) messageMapField.get(targetAuthProcess);
        messageMapField.set(this, messageMap);

      }
      catch (SecurityException ex) {}
      catch (NoSuchFieldException ex) {}
      catch (IllegalArgumentException ex) {}
      catch (IllegalAccessException ex) {}
    }
  }

  /** {@inheritDoc} */
  @Override
  public boolean isManualAuthAllowed(IAuthResponse argResponse) {
    boolean result = super.isManualAuthAllowed(argResponse);
    if (result && (argResponse instanceof AjbFiPayCreditDebitResponse)) {
      result = ((AjbFiPayCreditDebitResponse) argResponse).isSAFAvailable();
    }
    return result;
  }

}
