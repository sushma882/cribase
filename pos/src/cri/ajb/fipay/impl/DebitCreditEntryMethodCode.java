//$Id: DebitCreditEntryMethodCode.java 1109 2017-03-01 21:38:19Z johgaug $
package cri.ajb.fipay.impl;

import dtv.util.StringUtils;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2016 OLR Retail
 *
 * @author johgaug
 * @created Jul 8, 2016
 * @version $Revision: 1109 $
 */
public enum DebitCreditEntryMethodCode {
  /** CEM_INERT */
  _CEM_INSERT("*CEM_Insert", "Chip"),

  /** CEM_SWIPED */
  _CEM_SWIPED("*CEM_Swiped", "Swipe"),

  /** CONTACTLESS */
  _CONTACTLESS("*Contactless", "NFC"),

  /** INVALID */
  _INVALID("INVALID", "Keyed");

  private final String code_;
  private final String mapCode_;

  public static DebitCreditEntryMethodCode forCode(String argCode) {
    DebitCreditEntryMethodCode result = null;
    for (DebitCreditEntryMethodCode v : values()) {
      if (v.matches(argCode)) {
        result = v;
        break;
      }
    }
    return result;
  }

  public static DebitCreditEntryMethodCode forOptions(String argOptions) {
    DebitCreditEntryMethodCode result = null;
    for (DebitCreditEntryMethodCode v : values()) {
      if (v.applicable(argOptions)) {
        result = v;
        break;
      }
    }
    return result;
  }

  private DebitCreditEntryMethodCode(String argCode, String argMapCode) {
    code_ = argCode;
    mapCode_ = argMapCode;
  }

  /** Get the code. */
  public String getCode() {
    return code_;
  }

  public String getMapping() {
    return mapCode_;
  }

  /** Get whether this instance is applicable to the given FIPay options. */
  public boolean applicable(String argOptions) {
    for (String token : StringUtils.split(argOptions, ' ')) {
      if (matches(token)) {
        return true;
      }
    }
    return false;
  }

  /** Get whether this instance matches the provided code. */
  public boolean matches(String argCode) {
    return code_.equalsIgnoreCase(argCode);
  }

  /** Gets Mapping code, this represents the database value */
  public boolean matchesMapping(String argCode) {
    return mapCode_.equals(argCode);
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    return code_;
  }
}
