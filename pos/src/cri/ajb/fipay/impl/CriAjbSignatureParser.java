//$Id$
package cri.ajb.fipay.impl;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dtv.hardware.sigcap.ISignature;
import dtv.hardware.sigcap.Signature;
import dtv.hardware.types.HardwareFamilyType;
import dtv.hardware.types.HardwareType;
import dtv.util.ByteUtils;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2016 MICROS Retail
 *
 * @author rahmoham
 * @created Aug 9, 2016
 * @version $Revision$
 */
public class CriAjbSignatureParser {

  private static final Logger logger_ = Logger.getLogger(CriAjbSignatureParser.class);
  private static byte[] HEADER_ = new byte[] {86, 70, 73, 83, 73, 71, 48, 48};
  /** A reusable constant for a point representing lifting the pen from the screen. */
  protected static final Point PEN_UP = new Point(-1, -1);

  public static ISignature processSignature(byte[] sigByteArr) {

    //strip off the header and convert into dataStream
    DataInputStream in = new DataInputStream(
        new ByteArrayInputStream(sigByteArr, HEADER_.length, sigByteArr.length - HEADER_.length));
    ISignature signature;
    try {
      Point[] points = new Point[in.readShort()];
      for (int i = 0; i < points.length; i++ ) {
        points[i] = new Point(in.readShort(), in.readShort());
      }
      //displayResults(points);
      signature = new Signature(true, HardwareType.forUse(HardwareFamilyType.SIG_CAP, "VERIFONE_SIGCAP"),
          points, true, false);

    }
    catch (IOException ex) {
      signature = null;
      logger_.error("Error encountered building the Signature. The byte stream is invalid");
    }
    return signature;

  }

  //parse 3 Byte ASSCII code .
  public List<Point> parse(final byte[] argBytes, int argMaxY) {
    try {
      final boolean fix = true;
      //if (_logger.isDebugEnabled()) {
      logger_.debug("Signature data=[" + ByteUtils.toString(argBytes) + "]");
      //}
      int x = 0;
      int y = 0;
      final List<Point> points = new ArrayList<Point>();

      points.add(PEN_UP);

      for (int ndx = 0; ndx < argBytes.length; ++ndx) {

        final byte b0 = argBytes[ndx];
        if (b0 == 0x70) {
          if (logger_.isDebugEnabled()) {
            logger_.debug("...pen up");
          }

          points.add(new Point(-1, -1));

          x = 0;
          y = 0;

          continue;
        }

        else if ((0x60 <= b0) && (b0 <= 0x6F)) {
          final byte b1 = argBytes[ndx + 1];
          final byte b2 = argBytes[ndx + 2];
          final byte b3 = argBytes[ndx + 3];

          x = (((b0 - 0x60) & 0x0c) << 7) | ((b1 - 0x20) << 3) | (((b3 - 0x20) & 0x38) >> 3);
          y = (((b0 - 0x60) & 0x03) << 9) | ((b2 - 0x20) << 3) | (((b3 - 0x20) & 0x07));

          ndx += 3;

          if (logger_.isDebugEnabled()) {
            logger_.debug("exact");
          }
        }
        else {
          final byte b1 = argBytes[ndx + 1];
          final byte b2 = argBytes[ndx + 2];

          int sDx = ((b0 - 0x20) << 3) | (((b2 - 0x20) & 0x38) >> 3);
          int sDy = ((b1 - 0x20) << 3) | (((b2 - 0x20) & 0x07));

          sDx = ((short) (sDx << 7) >> 7);
          sDy = ((short) (sDy << 7) >> 7);
          if (logger_.isDebugEnabled()) {
            logger_.debug("dX=[" + sDx + "],dY=[" + sDy + "]");
          }

          x += sDx;
          y += sDy;

          ndx += 2;
        }
        if (logger_.isDebugEnabled()) {
          logger_.debug("\t(" + x + ", " + y + ")");
        }
        if ((y <= argMaxY) || !fix) {
          points.add(new Point(x, y));
        }
        else if (logger_.isDebugEnabled()) {
          logger_.debug("..ignored");
        }
      }

      return points;
    }
    catch (final RuntimeException ex) {
      logger_.error("CAUGHT EXCEPTION", ex);
      return null;
    }
  }

  /**
   * Get the image string data.
   * 
   * @param argImage
   * @return
   */
  protected String getImagePointsString(BufferedImage argImage) {
    List<Point> points = getImagePoints(argImage);
    String str = new String();
    for (Point p : points) {
      str = str + (int) p.getX() + "," + (int) p.getY() + ":";
    }

    return str;
  }

  protected String getImagePointsString2(List<Point> argSigStr) {
    String str = new String();
    for (Point p : argSigStr) {
      str = str + (int) p.getX() + "," + (int) p.getY() + ":";
    }

    return str;
  }

  /**
   * Convert the image to the x and y points array.
   * 
   * @param argImage
   * @return
   */
  protected List<Point> getImagePoints(BufferedImage argImage) {
    List<Point> points = new ArrayList<Point>();
    points.add(new Point(argImage.getWidth(), argImage.getHeight()));
    points.add(new Point(-1, -1));
    int width = argImage.getWidth();
    int height = argImage.getHeight();

    for (int x = 0; x < width; x++ ) {
      for (int y = 0; y < height; y++ ) {
        int pixel = argImage.getRGB(x, y);

        int red = (pixel & 0x0000FFFF) >> 16;
        int green = (pixel & 0x0000FFFF) >> 8;
        int blue = pixel & 0x0000FFFF;

        //this pixel is not white
        if ((red == 0) && (green == 0) && (blue == 0)) {
          points.add(new Point(x, y));
        }
      }
    }

    return points;
  }
}
