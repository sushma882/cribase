//$Id: AjbFiPayVoidAuthorizationOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.op;

import org.apache.log4j.Logger;

import dtv.pos.iframework.type.VoucherActivityCodeType;
import dtv.pos.tender.voucher.IVoucherCmd;
import dtv.pos.tender.voucher.config.VoucherConfig;
import dtv.tenderauth.*;
import dtv.tenderauth.op.IAuthorizationCmd;
import dtv.tenderauth.storedvalue.IStoredValueAuthRequest;
import dtv.tenderauth.storedvalue.VoidAuthorizationOp;
import dtv.xst.dao.trl.IVoucherLineItem;
import dtv.xst.dao.trl.IVoucherSaleLineItem;

/**
 * Operation to void a previous authorization.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayVoidAuthorizationOp
    extends VoidAuthorizationOp {

  /**
   * 
   */
  private static final long serialVersionUID = -3171787771028021630L;
  private static final Logger logger_ = Logger.getLogger(AjbFiPayVoidAuthorizationOp.class);

  /** {@inheritDoc} */
  @Override
  protected IAuthRequest getAuthRequest(IAuthorizationCmd argCmd) {
    IVoucherCmd cmd = (IVoucherCmd) argCmd;

    IVoucherLineItem vli = cmd.getVoucherLineItem();
    IVoucherSaleLineItem line = (IVoucherSaleLineItem) vli;
    VoucherConfig voucherConfig = getVoucherConfig(line);

    final AuthRequestType reqType;
    VoucherActivityCodeType origType = VoucherActivityCodeType.valueOf(line.getActivityCode());
    if (origType == VoucherActivityCodeType.ISSUED) {
      reqType = AuthRequestType.VOID_ACTIVATE;
    }
    else if (origType == VoucherActivityCodeType.RELOAD) {
      reqType = AuthRequestType.VOID_RELOAD;
    }
    else if (origType == VoucherActivityCodeType.REDEEMED) {
      reqType = AuthRequestType.VOID_TENDER;
    }
    else if (origType == VoucherActivityCodeType.CASHOUT) {
      reqType = getVoidCashoutAuthRequestType();
    }
    else {
      logger_.warn("unexpected type " + origType);
      reqType = AuthRequestType.REVERSE;
    }

    IStoredValueAuthRequest request = (IStoredValueAuthRequest) AuthFactory.getInstance()
        .makeAuthRequest(voucherConfig.getAuthMethodCode(), reqType, line, true);

    request.setIgnoreFailure(getContinueOnFailure(cmd));
    return request;
  }
}
