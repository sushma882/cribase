//$Id: AjbFiPayAuthorizeCardOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.op;

import static dtv.util.NumberUtils.isNegative;
import static dtv.util.NumberUtils.nonNull;
import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;

import cri.ajb.fipay.response.AjbFiPayGiftCardResponse;
import cri.pos.tender.CriSaleTenderCmd;

import dtv.i18n.IFormattable;
import dtv.pos.common.PromptKey;
import dtv.pos.iframework.cmd.ITransactionCmd;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.*;
import dtv.pos.iframework.type.VoucherActivityCodeType;
import dtv.pos.tender.TenderHelper;
import dtv.pos.tender.voucher.IVoucherCmd;
import dtv.tenderauth.*;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.op.IAuthorizationCmd;
import dtv.tenderauth.storedvalue.AuthorizeCardOp;
import dtv.tenderauth.storedvalue.IStoredValueAuthRequest;
import dtv.tenderauth.storedvalue.ajb.AjbGiftCardResponse;
import dtv.xst.dao.trl.IAuthorizableLineItem;
import dtv.xst.dao.trl.IVoucherLineItem;
import dtv.xst.dao.ttr.IVoucher;

/**
 * An operation to authorize a voucher card.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayAuthorizeCardOp
    extends AuthorizeCardOp {

  /**
   * 
   */
  private static final long serialVersionUID = 5893985197829266551L;

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleSuccess(IAuthorizationCmd argCmd, IAuthResponse argResponse) {
    IFormattable msg = argResponse.getMessage();
    updateTenderLineItem(argCmd, argCmd.getAuthRequest().getLineItem(), argResponse);

    //Added for AC#457634 - FB#368476 - CRI - 9.1.9 - Unable to void GC tender that was processed offline.
    if ((argResponse instanceof AjbFiPayGiftCardResponse)
        && ((AjbFiPayGiftCardResponse) argResponse).isSAFAvailable()) {
      ((CriSaleTenderCmd) argCmd).setSAFAuthRequiredResponse(argResponse);
    }

    if (msg != null) {
      argCmd.setOpState(COMPLETE);
      return HELPER.getPromptResponse(OpStatus.INCOMPLETE_HALT, PromptKey.valueOf("AUTH_SUCCESS_MESSAGE"),
          msg);
    }
    else {
      return HELPER.completeResponse();
    }
  }

  /**
   * Update the tender line item data.
   * 
   * @param argLineItem
   * @param argResponse
   */
  protected void updateTenderLineItem(IXstCommand argCmd, IAuthorizableLineItem argLineItem,
      IAuthResponse argResponse) {
    //updateTenderLineItem for manual auth is handled separately
    //argResponse object for manual auth can't be down casted to AjbGiftCardResponse
    // and it argResponse object for manual auth doesn't have element CardNumber
    if (argLineItem instanceof IVoucherLineItem) {
      IAuthInfo authInfo = argResponse.getRequest().getMoreAuthInfo();
      if ((authInfo != null) && authInfo.getType().equals(AuthInfoType.MANUAL_AUTH)) {
        updateTenderLineItemForManualAuth(argCmd, argLineItem, argResponse);
      }
      else {
        IVoucherCmd cmd = (IVoucherCmd) argCmd;
        AjbGiftCardResponse response = (AjbGiftCardResponse) argResponse;
        IVoucherLineItem lineItem = (IVoucherLineItem) argLineItem;
        IVoucher voucher =
            TenderHelper.getInstance().lookupVoucher(cmd.getVoucherType(), response.getCardNumber());
        if (voucher == null) {
          IVoucherLineItem voucherLineItem = TenderHelper.getInstance().getVoucherFromCurrentTransaction(
              ((ITransactionCmd) argCmd).getTransaction(), cmd.getVoucherType(), response.getCardNumber());
          voucher = voucherLineItem.getVoucher();
        }

        lineItem.setSerialNumber(response.getCardNumber());
        lineItem.setActivityCode(cmd.getVoucherActivityCodeType().toString());
        lineItem.setVoucherTypeCode(cmd.getVoucherType());

        if (VoucherActivityCodeType.ISSUED.matches(lineItem.getActivityCode())) {

          voucher.setUnspentBalanceAmount(response.getAmount());
          voucher.setFaceValueAmount(response.getAmount());
        }

        else if (VoucherActivityCodeType.RELOAD.matches(lineItem.getActivityCode())) {
          //BigDecimal unspentBalance = nonNull(voucher.getUnspentBalanceAmount()).add(response.getAmount());
          //BigDecimal unspentBalance = nonNull(NumberUtils.nonNull(voucher.getUnspentBalanceAmount())).add(response.getAmount());
          BigDecimal unspentBalance = nonNull(response.getBalance());
          voucher.setUnspentBalanceAmount(unspentBalance);

        }

        else if (VoucherActivityCodeType.REDEEMED.matches(lineItem.getActivityCode())) {
          //BigDecimal unspentBalance = nonNull(voucher.getUnspentBalanceAmount()).add(response.getAmount().negate());
          //BigDecimal unspentBalance = nonNull(NumberUtils.nonNull(voucher.getUnspentBalanceAmount())).add(response.getAmount().negate());
          BigDecimal unspentBalance = nonNull(response.getBalance());
          unspentBalance = isNegative(unspentBalance) ? ZERO : unspentBalance;
          voucher.setUnspentBalanceAmount(unspentBalance);

        }
        else if (VoucherActivityCodeType.CASHOUT.matches(lineItem.getActivityCode())) {
          // BigDecimal unspentBalance = nonNull(NumberUtils.nonNull(voucher.getUnspentBalanceAmount())).add(response.getAmount().negate());
          BigDecimal unspentBalance = nonNull(response.getBalance());
          unspentBalance = isNegative(unspentBalance) ? ZERO : unspentBalance;

          voucher.setUnspentBalanceAmount(unspentBalance);
        }

        lineItem.setVoucher(voucher);
      }
    }

  }

  /**
   * Update the tender line item data for manual auth.
   * 
   * @param argLineItem
   * @param argResponse
   */
  protected void updateTenderLineItemForManualAuth(IXstCommand argCmd, IAuthorizableLineItem argLineItem,
      IAuthResponse argResponse) {
    IVoucherLineItem lineItem = (IVoucherLineItem) argLineItem;
    lineItem.setVoid(false);
    IVoucherCmd cmd = (IVoucherCmd) argCmd;
    IAuthResponse response = argResponse;
    String cardNumber = cmd.getVoucher().getSerialNumber();
    IVoucher voucher = TenderHelper.getInstance().lookupVoucher(cmd.getVoucherType(), cardNumber);
    if (voucher == null) {
      IVoucherLineItem voucherLineItem = TenderHelper.getInstance().getVoucherFromCurrentTransaction(
          ((ITransactionCmd) argCmd).getTransaction(), cmd.getVoucherType(), cardNumber);
      voucher = voucherLineItem.getVoucher();
    }

    lineItem.setSerialNumber(cardNumber);
    lineItem.setActivityCode(cmd.getVoucherActivityCodeType().toString());
    //lineItem.setVoucherTypeCode(cmd.getVoucherType());
    lineItem.setVoucherTypeCode(cmd.getVoucher().getVoucherTypeCode());

    if (VoucherActivityCodeType.ISSUED.matches(lineItem.getActivityCode())) {

      voucher.setUnspentBalanceAmount(response.getAmount());
      voucher.setFaceValueAmount(response.getAmount());
    }

    else if (VoucherActivityCodeType.RELOAD.matches(lineItem.getActivityCode())) {
      //BigDecimal unspentBalance = nonNull(voucher.getUnspentBalanceAmount()).add(response.getAmount());
      //BigDecimal unspentBalance = nonNull(NumberUtils.nonNull(voucher.getUnspentBalanceAmount())).add(response.getAmount());
      BigDecimal unspentBalance = nonNull(response.getBalance());
      voucher.setUnspentBalanceAmount(unspentBalance);

    }

    else if (VoucherActivityCodeType.REDEEMED.matches(lineItem.getActivityCode())) {
      //BigDecimal unspentBalance = nonNull(voucher.getUnspentBalanceAmount()).add(response.getAmount().negate());
      //BigDecimal unspentBalance = nonNull(NumberUtils.nonNull(voucher.getUnspentBalanceAmount())).add(response.getAmount().negate());
      BigDecimal unspentBalance = nonNull(response.getBalance());
      unspentBalance = isNegative(unspentBalance) ? ZERO : unspentBalance;
      voucher.setUnspentBalanceAmount(unspentBalance);

    }
    else if (VoucherActivityCodeType.CASHOUT.matches(lineItem.getActivityCode())) {
      //BigDecimal unspentBalance = nonNull(NumberUtils.nonNull(voucher.getUnspentBalanceAmount())).add(response.getAmount().negate());
      BigDecimal unspentBalance = nonNull(response.getBalance());

      unspentBalance = isNegative(unspentBalance) ? ZERO : unspentBalance;

      voucher.setUnspentBalanceAmount(unspentBalance);
    }

    lineItem.setVoucher(voucher);
  }

  /** {@inheritDoc} */
  @SuppressWarnings("null")
  @Override
  protected IAuthRequest getAuthRequest(IAuthorizationCmd argCmd) {
    IVoucherCmd cmd = (IVoucherCmd) argCmd;
    IVoucherLineItem lineItem = cmd.getVoucherLineItem();

    String serialNumber = cmd.getSerialNumber();
    if (serialNumber == null) {
      serialNumber = lineItem.getSerialNumber();
    }
    if ((serialNumber == null) && (lineItem != null)) {
      serialNumber = lineItem.getSerialNumber();
    }
    if (cmd.getAuthRequest() instanceof IStoredValueAuthRequest) {
      IStoredValueAuthRequest req = (IStoredValueAuthRequest) cmd.getAuthRequest();
      if (serialNumber.equals(req.getAccountId())) {
        IAuthResponse[] responses = req.getResponses();
        IAuthResponse lastResponse = responses[responses.length - 1];
        if (lastResponse.isSuccess()) {
          return req;
        }
      }
    }
    if (lineItem.getParentTransaction() == null) {
      throw new NullPointerException("parent transaction is null");
    }

    IStoredValueAuthRequest request = makeRequest(cmd);
    return request;
  }

  /**
   * {@inheritDoc} Overridden for AC#457634 - FB#368476.
   */
  @Override
  protected IOpResponse handleManualAuthInfoResponse(IAuthorizationCmd argCmd, IXstEvent argEvent) {
    IAuthRequest request = argCmd.getAuthRequest();
    IAuthResponse response = request.getResponses()[request.getResponses().length - 1];

    if ((response instanceof AjbFiPayGiftCardResponse)
        && ((AjbFiPayGiftCardResponse) response).isSAFAvailable()) {
      ((CriSaleTenderCmd) argCmd).setSAFAuthRequiredResponse(response);
    }

    return super.handleManualAuthInfoResponse(argCmd, argEvent);
  }

}
