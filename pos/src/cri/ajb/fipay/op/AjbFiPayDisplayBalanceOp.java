//$Id: AjbFiPayDisplayBalanceOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.op;

import dtv.i18n.*;
import dtv.pos.iframework.op.IXstCommand;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.storedvalue.DisplayBalanceOp;
import dtv.tenderauth.storedvalue.IBalanceCmd;
import dtv.tenderauth.storedvalue.ajb.AjbGiftCardResponse;

/**
 * Operation to display a stored value balance<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayDisplayBalanceOp
    extends DisplayBalanceOp {

  /**
   * 
   */
  private static final long serialVersionUID = -2166770673319310970L;

  /** {@inheritDoc} */
  @Override
  protected IFormattable[] getPromptArgs(IXstCommand argCmd) {
    IBalanceCmd cmd = (IBalanceCmd) argCmd;
    IAuthResponse[] responses = cmd.getAuthRequest().getResponses();

    AjbGiftCardResponse lastResponse = (AjbGiftCardResponse) responses[responses.length - 1];
    cmd.setBalance(lastResponse.getCardBalance());
    cmd.setSerialNumber(lastResponse.getCardNumber());
    return new IFormattable[] {
        FormattableFactory.getInstance().getSimpleFormattable(cmd.getBalance(), FormatterType.MONEY)};
  }

}
