//$Id: AjbFiPayInitializeDisplayOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.op;

import cri.ajb.fipay.impl.AjbFiPayDisplayDevice;

import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;

/**
 * Initialize the rolling receipt display device.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 *
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayInitializeDisplayOp
    extends Operation {

  /**
   * 
   */
  private static final long serialVersionUID = -3762373239931901973L;

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    AjbFiPayDisplayDevice.getInstance().initialize();
    return HELPER.completeResponse();
  }
}
