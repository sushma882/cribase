//$Id: AjbFiPayInsertTokenQueueOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.op;

import cri.pos.common.CriConfigurationMgr;
import cri.pos.tender.CriTenderHelper;
import cri.xst.dao.ttr.ICriTokenUpdateQueue;

import dtv.data2.access.DataFactory;
import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.pos.tender.TenderHelper;
import dtv.util.StringUtils;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;

/**
 * Create the token update queue for the manually authorized tender.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayInsertTokenQueueOp
    extends Operation {
  /**
   * 
   */
  private static final long serialVersionUID = 3327874746811988488L;
  protected static final CriTenderHelper TENDER_HELPER = (CriTenderHelper) TenderHelper.getInstance();

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;

    if ((cmd.getTenderLineItem() instanceof ICreditDebitTenderLineItem)
        && CriConfigurationMgr.isAjbEnabled()) {
      ICreditDebitTenderLineItem tenderLineItem = (ICreditDebitTenderLineItem) cmd.getTenderLineItem();
      return StringUtils.isEmpty(tenderLineItem.getAuthorizationToken())
          || tenderLineItem.getAuthorizationToken().startsWith("TT");
    }

    return false;
  }

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    ICreditDebitTenderLineItem tenderLineItem = (ICreditDebitTenderLineItem) cmd.getTenderLineItem();

    ICriTokenUpdateQueue queue = DataFactory.createObject(ICriTokenUpdateQueue.class);
    queue.setOrganizationId(tenderLineItem.getOrganizationId());
    queue.setInvoiceNbr(TENDER_HELPER.getOriginalInvoiceNbr(tenderLineItem));
    queue.setRetailLocationId(tenderLineItem.getRetailLocationId());
    queue.setWorkstationId(tenderLineItem.getWorkstationId());
    queue.setTransactionSequence(tenderLineItem.getTransactionSequence());
    queue.setBusinessDate(tenderLineItem.getBusinessDate());
    queue.setRetailTransactionLineItemSequence(tenderLineItem.getRetailTransactionLineItemSequence());

    cmd.addPersistable(queue);
    return HELPER.completeResponse();
  }
}
