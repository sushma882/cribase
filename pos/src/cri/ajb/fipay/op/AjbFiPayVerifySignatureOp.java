//$Id: AjbFiPayVerifySignatureOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.op;

import org.apache.log4j.Logger;

import dtv.hardware.sigcap.ISignature;
import dtv.pos.hardware.op.VerifySignatureOp;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.xst.dao.tnd.ITender;

/**
 * Verify the signature.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayVerifySignatureOp
    extends VerifySignatureOp {

  /**
   * 
   */
  private static final long serialVersionUID = -288806736972444778L;
  private static final Logger logger_ = Logger.getLogger(AjbFiPayVerifySignatureOp.class);

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    // check command class
    if (!(argCmd instanceof ISaleTenderCmd)) {
      logger_.warn("command [" + argCmd + "] does not implement " + ISaleTenderCmd.class.getName());
      return false;
    }
    // check tender on command
    ITender tender = ((ISaleTenderCmd) argCmd).getTender();
    if (tender == null) {
      logger_.warn("no tender on command");
      return false;
    }
    // check signature on command
    ISignature signature = ((ISaleTenderCmd) argCmd).getSignature();
    if (signature == null) {
      return false;
    }
    // passed all checks
    return true;
  }
}
