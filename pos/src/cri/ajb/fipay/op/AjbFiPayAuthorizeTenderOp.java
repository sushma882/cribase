//$Id: AjbFiPayAuthorizeTenderOp.java 1133 2017-06-14 09:17:39Z krimoha $
package cri.ajb.fipay.op;

import org.apache.log4j.Logger;

import cri.ajb.fipay.response.AjbFiPayCreditDebitResponse;
import cri.pos.common.CriConstants;
import cri.pos.tender.CriSaleTenderCmd;
import cri.pos.tender.CriTenderHelper;

import dtv.pos.iframework.action.IXstActionKey;
import dtv.pos.iframework.action.IXstDataAction;
import dtv.pos.iframework.cmd.ITransactionCmd;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.*;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.pos.tender.SaleTenderCmd;
import dtv.tenderauth.*;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.op.AuthorizeTenderOp;
import dtv.tenderauth.op.IAuthorizationCmd;
import dtv.xst.dao.tnd.TenderStatus;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.ttr.IAuthorizableTenderLineItem;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;

/**
 * An operation that authorizes a tender<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Sep 29, 2015
 * @version $Revision: 1133 $
 */
public class AjbFiPayAuthorizeTenderOp
    extends AuthorizeTenderOp {

  /**
   * 
   */
  private static final long serialVersionUID = 7871444243806718719L;
  protected static final AuthRequestType TENDER_MANUAL_ENTRY = AuthRequestType.forName("TENDER_MANUAL_ENTRY");
  private static final Logger logger_ = Logger.getLogger(AjbFiPayAuthorizeTenderOp.class);

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleSuccess(IAuthorizationCmd argCmd, IAuthResponse argResponse) {
    IOpResponse response = super.handleSuccess(argCmd, argResponse);

    if ((argResponse instanceof AjbFiPayCreditDebitResponse)
        && ((AjbFiPayCreditDebitResponse) argResponse).isSAFAvailable()) {
      ((CriSaleTenderCmd) argCmd).setSAFAuthRequiredResponse(argResponse);
    }

    if (isStoreOffline(argCmd)) {
      IPosTransaction posTrans = ((ITransactionCmd) argCmd).getTransaction();
      CriTenderHelper.setTransactionProperty(posTrans, CriConstants.CRI_STOREDOWN_ENTRYMODE, "YES");
    }

    return response;
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleManualAuthInfoResponse(IAuthorizationCmd argCmd, IXstEvent argEvent) {
    IAuthRequest request = argCmd.getAuthRequest();
    IAuthResponse response = request.getResponses()[request.getResponses().length - 1];

    if ((response instanceof AjbFiPayCreditDebitResponse)
        && ((AjbFiPayCreditDebitResponse) response).isSAFAvailable()) {
      ((CriSaleTenderCmd) argCmd).setSAFAuthRequiredResponse(response);
    }

    return super.handleManualAuthInfoResponse(argCmd, argEvent);
  }

  @Override
  protected IAuthRequest getAuthRequest(IAuthorizationCmd argCmd) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    IAuthorizableTenderLineItem tl = (IAuthorizableTenderLineItem) cmd.getTenderLineItem();
    if (tl.getParentTransaction() == null) {
      throw new NullPointerException("parent transaction is null");
    }
    IAuthRequest req = cmd.getAuthRequest();
    if ((req == null) || (req.getLineItem() != tl)) {
      final AuthRequestType type;
      final boolean requestRequired;
      if (TenderStatus.TENDER.getName().equals(tl.getTenderStatusCode())) {
        if (isStoreOffline(argCmd)) {
          type = TENDER_MANUAL_ENTRY;
        }
        else {
          type = AuthRequestType.TENDER;
        }

        requestRequired = true;
      }
      else {
        type = AuthRequestType.REFUND_TENDER;
        requestRequired = false;
      }
      req =
          AuthFactory.getInstance().makeAuthRequest(type, tl, cmd.getTenderUsageCodeType(), requestRequired);
    }
    return req;
  }

  private boolean isStoreOffline(IXstCommand argCmd) {
    return CriConstants.CRI_STOREDOWN_ENTRYMODE.equals(argCmd.getValue(CriConstants.CRI_STOREDOWN_ENTRYMODE));
  }

  /**
   * CRU INC0073386 - Not all SAF'd transactions are being processed and sent to Vantiv.
   * Overridden this method to call the customized private handleShowingFailedResponse method.
   */
  @Override
  protected IOpResponse handlePostPrompt(IAuthorizationCmd argCmd, IXstEvent argEvent, IOpState argState) {
    if (this.SHOWING_FAILED == argState) {
      return this.handleShowingFailedResponse(argCmd, argEvent);

    }
    else if (this.SHOWING_NEED_MORE_INFO == argState) {
      return this.handleMoreInfoResponse(argCmd, argEvent);

    }
    else if (this.SHOWING_GET_MANUAL_AUTH == argState) {
      return this.handleManualAuthInfoResponse(argCmd, argEvent);

    }
    else if (this.PROCESSING == argState) {
      logger_.warn("unexpected event " + argEvent);
      return HELPER.waitResponse();

    }
    else if (this.ASKING_TO_SUSPEND == argState) {
      return this.handleSuspendQuestionResponse(argCmd, argEvent);

    }
    else if (this.ASKING_TO_CANCEL == argState) {
      return this.handleCancelQuestionResponse(argCmd, argEvent);

    }
    else {
      logger_.warn("unexpected state " + argState);
      return HELPER.errorNotifyResponse();
    }
  }

  /**
   * CRU INC0073386 - Not all SAF'd transactions are being processed and sent to Vantiv.
   * When an associate hit the “retry” button, the system should require the customer to either insert or swipe their card again, 
   * and rebuild the message that is being sent, rather than resending the previous message with missing data points.
   */
  private IOpResponse handleShowingFailedResponse(IAuthorizationCmd argCmd, IXstEvent argEvent) {
    if (argEvent instanceof IXstDataAction) {
      IXstDataAction action = (IXstDataAction) argEvent;
      IXstActionKey actionKey = action.getActionKey();
      if (RETRY == actionKey) {
        ICreditDebitTenderLineItem tl =
            (ICreditDebitTenderLineItem) ((SaleTenderCmd) argCmd).getTenderLineItem();
        tl.setAccountNumber(null);
        tl.setExpirationDateString(null);
        IAuthRequest request = argCmd.getAuthRequest();
        HostList hostList = request.getHostList();
        if (hostList != null) {
          hostList.reset();
        }
        return this.handleProcess(argCmd, request);
      }
      else if (MANUAL_AUTH == actionKey) {
        return handleManualAuth(argCmd);
      }
      else {
        logger_.warn("unexpected data action key [" + actionKey + "]");
        return handleAbort(argCmd);
      }
    }
    else {
      logger_.warn("unexpected event " + argEvent);
      return HELPER.silentErrorResponse();
    }
  }

}
