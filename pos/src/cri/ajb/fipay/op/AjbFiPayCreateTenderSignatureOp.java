//$Id: AjbFiPayCreateTenderSignatureOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.op;

import org.apache.log4j.Logger;

import dtv.pos.hardware.op.CreateTenderSignatureOp;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.pos.tender.TenderHelper;
import dtv.util.ObjectUtils;
import dtv.xst.dao.tnd.ITender;

/**
 * Create tender signature.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayCreateTenderSignatureOp
    extends CreateTenderSignatureOp {
  /**
   * 
   */
  private static final long serialVersionUID = 6568798879219460606L;
  private static final Logger logger_ = Logger.getLogger(AjbFiPayCreateTenderSignatureOp.class);

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    if (!(argCmd instanceof ISaleTenderCmd)) {
      logger_.warn("unexpected command type " + ObjectUtils.getClassNameFromObject(argCmd));
      return false;
    }
    ITender tender = ((ISaleTenderCmd) argCmd).getTender();
    if (tender == null) {
      logger_.warn("no tender on command");
      return false;
    }
    // see if signature is required
    if (!TenderHelper.getInstance().isSignatureRequired(((ISaleTenderCmd) argCmd).getTenderLineItem())) {
      return false;
    }
    return true;
  }
}
