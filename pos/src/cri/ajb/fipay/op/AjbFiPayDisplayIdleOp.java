//$Id: AjbFiPayDisplayIdleOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.op;

import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.tenderauth.*;

/**
 * AJB IDLE screen display OP.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 *
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayDisplayIdleOp
    extends Operation {

  /**
   * 
   */
  private static final long serialVersionUID = -4147990471933315198L;

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    IAuthProcess process = AuthFactory.getInstance().getAuthProcess("AJB_ROLLING_RECEIPT");
    if (process != null) {
      IAuthRequest request = AuthFactory.getInstance().makeAuthRequest("AJB_ROLLING_RECEIPT",
          AuthRequestType.get("IDLE"), new Object[] {null});
      process.processRequest(request);
    }
    return HELPER.completeResponse();
  }
}
