//$Id: AjbFiPayCreateVoucherTenderLineItemOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.op;

import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.pos.tender.voucher.CreateVoucherTenderLineItemOp;

/**
 * AJB voucher tender line item creation OP.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayCreateVoucherTenderLineItemOp
    extends CreateVoucherTenderLineItemOp {

  /**
   * 
   */
  private static final long serialVersionUID = 6615514809054962370L;

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    IOpResponse response = super.handleOpExec(argCmd, argEvent);
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    cmd.getVoucherLineItem().setEntryMethodCode(null);

    //don't enable the voucher line until get the approved response from AJB FiPay.
    cmd.getVoucherLineItem().setVoid(true);
    return response;
  }
}
