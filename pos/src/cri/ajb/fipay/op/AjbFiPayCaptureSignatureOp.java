//$Id: AjbFiPayCaptureSignatureOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.op;

import org.apache.log4j.Logger;
import org.castor.util.Base64Decoder;

import cri.ajb.fipay.impl.CriAjbSignatureParser;
import cri.ajb.fipay.response.IAjbFiPaySignatureCaptureResponse;
import cri.pos.tender.CriTenderHelper;

import dtv.hardware.sigcap.ISignature;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.tenderauth.*;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.op.AbstractAuthorizeOp;
import dtv.tenderauth.op.IAuthorizationCmd;
import dtv.util.ObjectUtils;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * AJB sig cap op.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayCaptureSignatureOp
    extends AbstractAuthorizeOp {

  /**
   * 
   */
  private static final long serialVersionUID = 1432709637143397103L;
  private static final Logger _logger = Logger.getLogger(AjbFiPayCaptureSignatureOp.class);

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    if (!(argCmd instanceof ISaleTenderCmd)) {
      _logger.warn("Incorrect command type [" + ObjectUtils.getClassNameFromObject(argCmd) + "]");
      return false;
    }

    ITenderLineItem lineItem = ((ISaleTenderCmd) argCmd).getTenderLineItem();
    if (lineItem.getVoid()) {
      _logger
          .warn("Line item void is true: it should be false if field four of the AJB response is zero. ID: "
              + lineItem.getObjectIdAsString());
    }
    CriTenderHelper tndrHelper = CriTenderHelper.getInstance();
    return tndrHelper.needsSignatureCapture(lineItem);
  }

  /** {@inheritDoc} */
  @Override
  protected IAuthProcess getAuthProcessor(IAuthorizationCmd argCmd) {
    return TenderAuthHelper.getInstance().getAuthProcess(getAuthMethodCode(argCmd));
  }

  /** {@inheritDoc} */
  @Override
  protected IAuthRequest getAuthRequest(IAuthorizationCmd argCmd) {
    return AuthFactory.getInstance().makeAuthRequest(getAuthMethodCode(argCmd),
        AuthRequestType.forName("GET_SIGNATURE"), argCmd.getAuthorizableLineItem(), true);
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleSuccess(IAuthorizationCmd argCmd, IAuthResponse argResponse) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;

    String strSig = null;
    if (argResponse instanceof IAjbFiPaySignatureCaptureResponse) {
      strSig = ((IAjbFiPaySignatureCaptureResponse) argResponse).getSignature();
      ISignature signature = createSignature(strSig);
      if (signature != null) {
        cmd.setSignature(signature);
      }
    }
    return HELPER.completeResponse();
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleAuthResponse(IAuthorizationCmd argCmd, IAuthResponse argResponse) {
    if (argResponse.isSuccess()) {
      return handleSuccess(argCmd, argResponse);
    }
    if (isNeedingMoreInfo(argResponse)) {
      return handleNeedMoreInfo(argCmd, argResponse);
    }
    else {
      return handleFailed(argCmd, argResponse);
    }
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleTrainingMode(IAuthorizationCmd argCmd) {
    return HELPER.completeResponse();
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleVoid(IAuthorizationCmd argCmd) {
    return HELPER.completeResponse();
  }

  /** Get the AJB auth method code that is defined in AuthConfig.xml. */
  protected String getAuthMethodCode(IXstCommand argCmd) {
    return "AJB_SIGNATURE_CAPTURE";
  }

  /** Create the Signature object. */
  protected ISignature createSignature(String argSignature) {
    ISignature signature = null;
    if (argSignature != null) {
      byte[] rawData = Base64Decoder.decode(argSignature);

      signature = CriAjbSignatureParser.processSignature(rawData);
    }

    return signature;
  }

}
