//$Id: AjbFiPayPromptTenderAmtOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.op;

import dtv.i18n.FormattableFactory;
import dtv.i18n.IFormattable;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.PromptTenderAmtOp;

/**
 * Prompt FiPay tender amount.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayPromptTenderAmtOp
    extends PromptTenderAmtOp {
  /**
   * 
   */
  private static final long serialVersionUID = -2866651159959480329L;
  protected final FormattableFactory FF = FormattableFactory.getInstance();

  /** {@inheritDoc} */
  @Override
  protected IFormattable[] getPromptArgs(IXstCommand argCmd, IXstEvent argEvent) {
    return new IFormattable[] {getTenderDescFormattable()};
  }

  /**
   * Get the default tender description on the amount prompt.
   * 
   * @return
   */
  protected IFormattable getTenderDescFormattable() {
    return FF.getTranslatable("_tender");
  }

}
