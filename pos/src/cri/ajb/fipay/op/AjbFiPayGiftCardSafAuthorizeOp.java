//$Id: AjbFiPayGiftCardSafAuthorizeOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.op;

import cri.ajb.fipay.response.AjbFiPayGiftCardResponse;
import cri.pos.tender.CriSaleTenderCmd;
import cri.tenderauth.CriAuthFactory;

import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.tenderauth.*;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.op.AbstractAuthorizeOp;
import dtv.tenderauth.op.IAuthorizationCmd;

/**
 * AJB Gift Card SAF request approval when the bank is Offline.<br>
 * <br>
 * Copyright (c) 2016 MICROS Retail
 *
 * @author rkasasbe
 * @created Jan 6, 2016
 * @version $Revision: 1105 $
 */
public class AjbFiPayGiftCardSafAuthorizeOp
    extends AbstractAuthorizeOp {

  /**
   * 
   */
  private static final long serialVersionUID = 583594511511461918L;
  protected static final ITenderAuthHelper AUTH_HELPER = TenderAuthHelper.getInstance();

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    if (!(argCmd instanceof CriSaleTenderCmd)) {
      return false;
    }
    IAuthResponse response = getAuthResponse((CriSaleTenderCmd) argCmd);

    if (response instanceof AjbFiPayGiftCardResponse) {
      return ((AjbFiPayGiftCardResponse) response).isSAFAvailable();
    }

    return false;
  }

  /** {@inheritDoc} */
  @Override
  protected IAuthProcess getAuthProcessor(IAuthorizationCmd argCmd) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    return AUTH_HELPER.getAuthProcess(cmd.getTender());
  }

  /** {@inheritDoc} */
  @Override
  protected IAuthRequest getAuthRequest(IAuthorizationCmd argCmd) {
    CriSaleTenderCmd cmd = (CriSaleTenderCmd) argCmd;
    IAuthRequest preRequest = cmd.getAuthRequest();
    IAuthResponse response = getAuthResponse(cmd);

    String authCode = null;
    if (preRequest.getMoreAuthInfo() != null) {
      authCode = preRequest.getMoreAuthInfo().getInputFieldValue(AuthInfoFieldKey.AUTH_NUMBER, String.class);
    }

    return ((CriAuthFactory) AuthFactory.getInstance()).makeAuthRequest("AJB_GIFT_CARD",
        AuthRequestType.forName("SAF"), response, authCode);
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleTrainingMode(IAuthorizationCmd argCmd) {
    return HELPER.completeResponse();
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleVoid(IAuthorizationCmd argCmd) {
    return HELPER.completeResponse();
  }

  /**
   * Get the Auth Response.
   * 
   * @param argCmd
   * @return
   */
  protected IAuthResponse getAuthResponse(CriSaleTenderCmd argCmd) {
    return argCmd.getSAFAuthRequiredResponse();
  }
}
