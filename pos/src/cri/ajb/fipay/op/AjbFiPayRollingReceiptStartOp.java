//$Id: AjbFiPayRollingReceiptStartOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.op;

import dtv.event.*;
import dtv.event.eventor.DefaultEventor;
import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;

/**
 * AJB start rolling receipt without transaction data.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayRollingReceiptStartOp
    extends Operation
    implements IEventSource {
  private static final long serialVersionUID = 1L;

  public static final EventDescriptor EMPTY_TRANSACTION_UPDATE =
      new EventDescriptor("EMPTY_TRANSACTION_UPDATE");
  private final Eventor events_ = new DefaultEventor(EMPTY_TRANSACTION_UPDATE);
  private static final EventEnum EMPTY_TRANSACTION = new EventEnum("empty transaction");

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    events_.post(EMPTY_TRANSACTION, null);
    return HELPER.completeResponse();
  }
}
