//$Id: AjbFiPayCreateVoucherOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.fipay.op;

import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.TenderHelper;
import dtv.pos.tender.voucher.IVoucherCmd;
import dtv.xst.dao.ttr.IVoucher;

/**
 * AJB voucher item creation OP.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class AjbFiPayCreateVoucherOp
    extends Operation {

  /**
   * 
   */
  private static final long serialVersionUID = -464449727755039469L;

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    IVoucherCmd cmd = (IVoucherCmd) argCmd;

    //initial the temp gift card. In finally all the voucher info is returned form FiPay.
    IVoucher voucher = TenderHelper.getInstance().createVoucher(cmd.getVoucherType(), null);
    cmd.setVoucher(voucher);
    return HELPER.completeResponse();
  }
}
