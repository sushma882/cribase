//$Id: Objects.java 1104 2017-01-09 20:47:30Z olr.jgaughn $
package cri.ajb;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2017 MICROS Retail
 *
 * @author johgaug
 * @created Jan 3, 2017
 * @version $Revision: 1104 $
 */
public class Objects {

  // Delete this class during a base upgrade to 6.0 or later, since they use Java 7 which has the Objects class built-in.

  /** Compute a hash code for the provided object. */
  public static int hashCode(Object argObject) {
    return (argObject == null) ? 0 : argObject.hashCode();
  }

  /** Compute a hash code for the provided objects. */
  public static int hash(Object... argObjects) {
    int hash = 59;
    for (Object obj : argObjects) {
      hash *= 2539;
      hash += ((obj == null) ? 0 : obj.hashCode());
    }
    return hash;
  }

  /** Determine if two objects are equal, taking <code>null</code> into account. */
  public static boolean equals(Object argObjectA, Object argObjectB) {
    if (argObjectA == argObjectB) {
      return true;
    }
    return (argObjectA != null) && argObjectA.equals(argObjectB);
  }

}
