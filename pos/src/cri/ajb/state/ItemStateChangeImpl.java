//$Id: ItemStateChangeImpl.java 841 2015-06-18 15:47:50Z suz.sxie $
package cri.ajb.state;

/**
 * Line item state change impl.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 841 $
 */
public class ItemStateChangeImpl
    extends AbstractStateChange
    implements IItemStateChange {

  private final int itemIndex_;
  private final ChangeType changeType_;

  /**
   * 
   * @param argChangeType
   * @param argItemIndex
   * @param argNewText
   */
  public ItemStateChangeImpl(ChangeType argChangeType, int argItemIndex, String argNewText) {
    super(argNewText);
    itemIndex_ = argItemIndex;
    changeType_ = argChangeType;
  }

  /** {@inheritDoc} */
  @Override
  public ChangeType getChangeType() {
    return changeType_;
  }

  /** {@inheritDoc} */
  @Override
  public int getItemIndex() {
    return itemIndex_;
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    return "ItemStateChange:" + changeType_ + ":" + itemIndex_ + ":" + getNewText();
  }

}