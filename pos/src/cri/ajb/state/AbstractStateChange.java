//$Id: AbstractStateChange.java 841 2015-06-18 15:47:50Z suz.sxie $
package cri.ajb.state;

/**
 * State change.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 841 $
 */
public abstract class AbstractStateChange
    implements IStateChange {

  private final String newText_;

  /**
   * Constructor method.
   *
   * @param argNewText
   */
  protected AbstractStateChange(String argNewText) {
    newText_ = argNewText;
  }

  /** {@inheritDoc} */
  @Override
  public String getNewText() {
    return newText_;
  }

}