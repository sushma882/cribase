//$Id: IItemStateChange.java 841 2015-06-18 15:47:50Z suz.sxie $
package cri.ajb.state;

/**
 * Line item state change.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 841 $
 */
public interface IItemStateChange
    extends IStateChange {

  /**
   * Get the state change type.
   * 
   * @return
   */
  public ChangeType getChangeType();

  /**
   * Get the change line item index.
   * 
   * @return
   */
  public int getItemIndex();

}