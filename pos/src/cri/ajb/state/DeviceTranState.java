//$Id: DeviceTranState.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.ajb.state;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import dtv.hardware.sigcap.state.ILineItemRenderer;
import dtv.util.ObjectUtils;
import dtv.xst.dao.trn.IPosTransaction;

/**
 * Transaction state model.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class DeviceTranState {

  private final ILineItemRenderer lineItemRenderer_;
  private final String tranId_;
  private final List<String> items_;
  private String tax_;
  private String total_;
  private String subTotal_;

  /**
   * Constructor method.
   * 
   * @param argLineItemRenderer
   * @param argTran
   */
  public DeviceTranState(ILineItemRenderer argLineItemRenderer, IPosTransaction argTran) {
    if (argLineItemRenderer == null) {
      throw new NullPointerException("Renderer cannot be null");
    }
    lineItemRenderer_ = argLineItemRenderer;
    items_ = new ArrayList<String>();
    tranId_ = getTransactionId(argTran);
  }

  /**
   * Get the transaction ID string value.
   * 
   * @param tran
   * @return
   */
  protected String getTransactionId(IPosTransaction tran) {
    return (tran == null) ? "" : tran.getObjectIdAsString();
  }

  public void addLineItem(Object argLineItem) {
    String[] s = lineItemRenderer_.renderLine(argLineItem);
    if ((s != null) && (s.length > 0)) {
      for (String line : s) {
        items_.add(line);
      }
    }
  }

  /**
   * Get all the display items.
   * 
   * @return
   */
  public String[] getItems() {
    return items_.toArray(new String[0]);
  }

  /**
   * Get the change items.
   * 
   * @param argPreviousState
   * @return
   */
  public IStateChange[] getChanges(DeviceTranState argPreviousState) {
    List<IStateChange> changes = new ArrayList<IStateChange>();

    boolean isSameTran = tranId_.equals(argPreviousState.tranId_);
    // handle items
    String[] previousItems = (isSameTran) ? argPreviousState.getItems() : new String[0];
    String[] currentItems = getItems();

    // find any updates
    int commonSize = Math.min(previousItems.length, currentItems.length);
    for (int i = 0; i < commonSize; i++ ) {
      if (!ObjectUtils.equivalent(previousItems[i], currentItems[i])) {
        changes.add(new ItemStateChangeImpl(ChangeType.MODIFIED, i, currentItems[i]));
      }
    }

    // find any deletions
    for (int i = currentItems.length; i < previousItems.length; i++ ) {
      changes.add(new ItemStateChangeImpl(ChangeType.DELETED, i, null));
    }

    // find any new items
    for (int i = previousItems.length; i < currentItems.length; i++ ) {
      changes.add(new ItemStateChangeImpl(ChangeType.NEW, i, currentItems[i]));
    }

    // // handle labels
    if (!ObjectUtils.equivalent(argPreviousState.getSubTotal(), getSubTotal())) {
      changes.add(new LabelStateChangeImpl(getSubTotal()));
    }

    if (!ObjectUtils.equivalent(argPreviousState.getTotal(), getTotal())) {
      changes.add(new LabelStateChangeImpl(getTotal()));
    }

    if (!ObjectUtils.equivalent(argPreviousState.getTax(), getTax())) {
      changes.add(new LabelStateChangeImpl(getTax()));
    }

    return changes.toArray(new IStateChange[0]);
  }

  public void setSubtotal(BigDecimal argAmount) {
    subTotal_ = lineItemRenderer_.renderAmount(argAmount);
  }

  public void setTotal(BigDecimal argAmount) {
    total_ = lineItemRenderer_.renderAmount(argAmount);
  }

  public void setTax(BigDecimal argAmount) {
    tax_ = lineItemRenderer_.renderAmount(argAmount);
  }

  public String getTotal() {
    return total_;
  }

  public String getSubTotal() {
    return subTotal_;
  }

  public String getTax() {
    return tax_;
  }
}
