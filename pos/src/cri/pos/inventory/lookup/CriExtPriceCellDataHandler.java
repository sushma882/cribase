//$Id: CriExtPriceCellDataHandler.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.inventory.lookup;

import java.awt.Color;
import java.awt.Font;

import dtv.i18n.*;
import dtv.pos.framework.ui.listview.DefaultCellDataHandler;
import dtv.pos.framework.ui.listview.config.ListViewColumnConfig;
import dtv.ui.layout.ViewCellData;
import dtv.ui.layout.ViewCellData.CellColumn;
import dtv.util.config.IConfigObject;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 * 
 * @author Jerny.Liu
 * @created Mar 20, 2014
 * @version $Revision: 1105 $
 */
public class CriExtPriceCellDataHandler
    extends DefaultCellDataHandler {

  private static final String FORMATTER_PARAM = "Formatter";
  private FormatterType _formatterType = FormatterType.DEFAULT;

  @Override
  public CellColumn buildCellColumn(ListViewColumnConfig argColConfig, Object argModel,
      Color argDefaultRowTextColor, Font argDefaultRowFont) {
    StringBuffer displayText = new StringBuffer();

    if (argModel instanceof CriItemPromotionalData) {
      CriItemPromotionalData dealData = (CriItemPromotionalData) argModel;
      int lineNumWhereExtPriceDisplay = dealData.getLineNumWhereExtPriceDisplay();

      for (int i = 0; i < (lineNumWhereExtPriceDisplay - 1); i++ ) {
        displayText.append(" \n");
      }

      IFormatter formatter = FormatterFactory.getInstance().getFormatter(_formatterType);
      displayText.append(formatter.format(dealData.getExtPrice(), OutputContextType.VIEW));
    }

    return new ViewCellData.CellColumn(displayText.toString(), null, argDefaultRowTextColor,
        argDefaultRowFont, argColConfig.getAlignment().getSwingAlignment(), argColConfig.getStart(),
        argColConfig.getWidth(), argColConfig.getRenderer(), argColConfig.isTextWrapped(),
        argColConfig.isSearchedOn());

  }

  /** {@inheritDoc} */
  @Override
  public void setParameter(String argName, IConfigObject argValue) {
    if (FORMATTER_PARAM.equalsIgnoreCase(argName)) {
      _formatterType = FormatterType.forName(argValue.toString());
    }
    else {
      super.setParameter(argName, argValue);
    }
  }
}
