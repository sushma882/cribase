//$Id: CriDiscountAmtCellDataHandler.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.inventory.lookup;

import java.awt.Color;
import java.awt.Font;
import java.math.BigDecimal;
import java.util.List;

import dtv.i18n.*;
import dtv.pos.framework.ui.listview.DefaultCellDataHandler;
import dtv.pos.framework.ui.listview.config.ListViewColumnConfig;
import dtv.ui.layout.ViewCellData;
import dtv.ui.layout.ViewCellData.CellColumn;
import dtv.util.config.IConfigObject;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 * 
 * @author Jerny.Liu
 * @created Mar 19, 2014
 * @version $Revision: 1105 $
 */
public class CriDiscountAmtCellDataHandler
    extends DefaultCellDataHandler {

  private static final String FORMATTER_PARAM = "Formatter";
  private FormatterType _formatterType = FormatterType.DEFAULT;
  private int _maxLength;

  @Override
  public CellColumn buildCellColumn(ListViewColumnConfig argColConfig, Object argModel,
      Color argDefaultRowTextColor, Font argDefaultRowFont) {
    StringBuffer displayText = new StringBuffer();

    if (argModel instanceof CriItemPromotionalData) {
      CriItemPromotionalData dealData = (CriItemPromotionalData) argModel;
      List<BigDecimal> discountAmtList = dealData.getDiscountAmtList();
      for (int i = 0; i < discountAmtList.size(); i++ ) {
        BigDecimal discountAmt = discountAmtList.get(i);
        IFormatter formatter = FormatterFactory.getInstance().getFormatter(_formatterType);
        String formattedDiscountAmt = formatter.format(discountAmt, OutputContextType.VIEW);
        if (i > 0) {
          formattedDiscountAmt = formatAlignRight(formattedDiscountAmt);
        }
        displayText.append(formattedDiscountAmt);
      }

    }
    displayText.delete(0, 1);
    return new ViewCellData.CellColumn(displayText.toString(), null, argDefaultRowTextColor,
        argDefaultRowFont, argColConfig.getAlignment().getSwingAlignment(), argColConfig.getStart(),
        argColConfig.getWidth(), argColConfig.getRenderer(), argColConfig.isTextWrapped(),
        argColConfig.isSearchedOn());
  }

  /** {@inheritDoc} */
  @Override
  public void setParameter(String argName, IConfigObject argValue) {
    if (FORMATTER_PARAM.equalsIgnoreCase(argName)) {
      _formatterType = FormatterType.forName(argValue.toString());
    }
    else if ("maxLength".equalsIgnoreCase(argName)) {
      _maxLength = Integer.valueOf(argValue.toString()).intValue();
    }
    else {
      super.setParameter(argName, argValue);
    }
  }

  protected String formatAlignRight(String s) {
    StringBuffer where = new StringBuffer();
    int wantedLength = Math.min(s.length(), _maxLength);
    String wanted = s.substring(0, wantedLength);
    pad(where, _maxLength - wantedLength);
    where.append(wanted);
    return where.toString();
  }

  protected final void pad(StringBuffer to, int howMany) {
    for (int i = 0; i < (howMany + 2); i++ ) {
      to.append(" ");
    }
  }
}
