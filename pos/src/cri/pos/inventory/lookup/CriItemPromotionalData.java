//$Id: CriItemPromotionalData.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.inventory.lookup;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 *
 * @author Jerny.Liu
 * @created Mar 18, 2014
 * @version $Revision: 1105 $
 */
public class CriItemPromotionalData {

  private String sku_;
  private List<String> descriptionList_ = new ArrayList<String>();
  private List<BigDecimal> discountAmtList_ = new ArrayList<BigDecimal>();
  private BigDecimal extPrice_;
  private int lineNumWhereExtPriceDisplay_;

  public String getSku() {
    return sku_;
  }

  public void setSku(String argSku) {
    sku_ = argSku;
  }

  public List<String> getDescriptionList() {
    return descriptionList_;
  }

  public void setDescriptionList(List<String> argDescriptionList) {
    descriptionList_ = argDescriptionList;
  }

  public List<BigDecimal> getDiscountAmtList() {
    return discountAmtList_;
  }

  public void setDiscountAmtList(List<BigDecimal> argDiscountAmtList) {
    discountAmtList_ = argDiscountAmtList;
  }

  public BigDecimal getExtPrice() {
    return extPrice_;
  }

  public void setExtPrice(BigDecimal argExtPrice) {
    extPrice_ = argExtPrice;
  }

  public int getLineNumWhereExtPriceDisplay() {
    return lineNumWhereExtPriceDisplay_;
  }

  public void setLineNumWhereExtPriceDisplay(int argLineNumWhereExtPriceDisplay) {
    lineNumWhereExtPriceDisplay_ = argLineNumWhereExtPriceDisplay;
  }

}
