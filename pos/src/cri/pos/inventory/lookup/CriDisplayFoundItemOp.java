//$Id: CriDisplayFoundItemOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.inventory.lookup;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import cri.pos.pricing.CriPricingHelper;

import dtv.pos.common.ConfigurationMgr;
import dtv.pos.iframework.form.IEditModel;
import dtv.pos.iframework.form.IEditModelCmd;
import dtv.pos.inventory.lookup.DisplayFoundItemOp;
import dtv.pos.inventory.lookup.ItemResultsEditModel;
import dtv.pos.register.ILookupItemCmd;
import dtv.xst.dao.itm.IItem;
import dtv.xst.dao.trl.IRetailPriceModifier;

/**
 * Operation to display the item that is found from an item search.<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 * 
 * @author Jerny.Liu
 * @created Mar 25, 2014
 * @version $Revision: 1105 $
 */
public class CriDisplayFoundItemOp
    extends DisplayFoundItemOp {

  /**
   * 
   */
  private static final long serialVersionUID = -6647750395564059078L;

  @Override
  protected IEditModel getModel(IEditModelCmd argCmd) {
    ItemResultsEditModel model = (ItemResultsEditModel) super.getModel(argCmd);
    ILookupItemCmd cmd = (ILookupItemCmd) argCmd;
    IItem item = (IItem) cmd.getSelectedResult();

    if (ConfigurationMgr.getItemLookupLoadUpcs()) {
      List<CriItemPromotionalData> itemPromotions = new ArrayList<CriItemPromotionalData>();

      List<IRetailPriceModifier> conditionalMods =
          CriPricingHelper.getInstance().getConditionalRetailPriceModifiers(item);

      List<IRetailPriceModifier> nonConditionalMods =
          CriPricingHelper.getInstance().getNonConditionalRetailPriceModifiers(item);

      CriItemPromotionalData itemDeal = new CriItemPromotionalData();
      itemDeal.setExtPrice(model.getCurrentPrice());

      itemDeal.setSku(item.getItemId());

      updateItemPromotion(itemDeal, nonConditionalMods, false);
      updateItemPromotion(itemDeal, conditionalMods, true);

      itemDeal.setLineNumWhereExtPriceDisplay(nonConditionalMods.size());

      itemPromotions.add(itemDeal);
      model.setPromotionList(itemPromotions);
    }
    return model;
  }

  private void updateItemPromotion(CriItemPromotionalData itemDeal, List<IRetailPriceModifier> mods,
      boolean isConditional) {
    List<String> descriptionList = itemDeal.getDescriptionList();
    List<BigDecimal> discountAmtList = itemDeal.getDiscountAmtList();

    if (mods.isEmpty() && !isConditional) {
      itemDeal.setExtPrice(null);
    }

    for (IRetailPriceModifier mod : mods) {
      descriptionList.add(mod.getDescription());
      if (!isConditional) {
        discountAmtList.add(mod.getDealAmount().negate());
        itemDeal.setExtPrice(itemDeal.getExtPrice().subtract(mod.getDealAmount()));
      }
    }

    itemDeal.setDescriptionList(descriptionList);
    itemDeal.setDiscountAmtList(discountAmtList);
  }
}
