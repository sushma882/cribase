//$Id$
package cri.pos.inventory;

import static dtv.data2.access.DataFactory.createObject;
import static dtv.pos.common.ConfigurationMgr.getOrganizationId;
import static dtv.pos.common.ConfigurationMgr.getRetailLocationId;
import static dtv.pos.inventory.type.InventoryConstants.OPEN;
import static dtv.pos.inventory.type.InventoryDocumentType.SHIPPING;
import static dtv.pos.storecalendar.StoreCalendar.getCurrentBusinessDate;
import static dtv.util.sequence.SequenceFactory.getNextStringValue;
import static dtv.xst.dao.inv.IInventoryControlDocumentModel.RECORD_CREATION_TYPE_STORE;

import java.util.Date;

import dtv.pos.common.AddressHelper;
import dtv.pos.common.LocationFactory;
import dtv.pos.inventory.InventoryHelper;
import dtv.pos.inventory.type.InvDocType;
import dtv.util.DateUtils;
import dtv.util.StringUtils;
import dtv.xst.dao.com.IAddress;
import dtv.xst.dao.inv.IInventoryControlDocument;
import dtv.xst.dao.inv.InventoryControlDocumentId;
import dtv.xst.dao.loc.IRetailLocation;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author pgolli
 * @created Aug 13, 2012
 * @version $Revision$
 */
public class CriInventoryHelper
    extends InventoryHelper {

  @Override
  public IInventoryControlDocument createInventoryControlDocument(InvDocType argType, String documentId,
      long location) {

    InventoryControlDocumentId journalId = new InventoryControlDocumentId();
    journalId.setOrganizationId(getOrganizationId());
    journalId.setDocumentTypeCode(argType.getDocumentType());

    if (location == 0) {
      journalId.setRetailLocationId(new Long(getRetailLocationId()));
    }
    else {
      journalId.setRetailLocationId(new Long(location));
    }

    if (StringUtils.isEmpty(documentId)) {
      documentId = getNextStringValue(argType.getSequenceType());
    }

    if (argType.getDocumentType().equalsIgnoreCase(SHIPPING.getName())) {
      String documentIdPart1 = documentId.substring(0, 4);
      String documentIdPart3 = documentId.substring(4);

      Date date = new Date();
      int month = DateUtils.getMonth(date);
      int day = DateUtils.getDayOfMonth(date);

      String documentIdPart2 = StringUtils.leftPadZeros(new Integer(month).toString(), 2)
          + StringUtils.leftPadZeros(new Integer(day).toString(), 2);

      documentId = documentIdPart1 + documentIdPart2 + documentIdPart3;
    }
    journalId.setDocumentId(documentId);

    IInventoryControlDocument doc = createObject(journalId, IInventoryControlDocument.class);
    doc.setDocumentSubtypeCode(argType.getDocumentName());
    doc.setCreateDateTime(getCurrentBusinessDate());
    doc.setLastActivityDate(getCurrentBusinessDate());
    doc.setStatusCode(OPEN);
    doc.startTemp();
    doc.setRecordCreationType(RECORD_CREATION_TYPE_STORE);

    IRetailLocation loc = LocationFactory.getInstance().getMyLocation();
    doc.setOriginatorId(Long.toString(loc.getRetailLocationId()));
    doc.setOriginatorName(loc.getDescription());

    IAddress address = AddressHelper.getInstance().createNewAddress();
    address.setAddress1(loc.getAddress1());
    address.setAddress2(loc.getAddress2());
    address.setCity(loc.getCity());
    address.setState(loc.getState());
    address.setPostalCode(loc.getPostalCode());
    address.setCountry(loc.getCountry());
    doc.setOriginatorAddress(address);

    return doc;
  }
}
