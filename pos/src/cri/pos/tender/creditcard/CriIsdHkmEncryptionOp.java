//$Id: CriIsdHkmEncryptionOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.tender.creditcard;

import java.io.*;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.isd.jec.IsdEncryptionClient;
import com.isd.jec.crypto.CryptoException;
import com.isd.jec.crypto.EncryptionManager;
import com.isd.jec.util.ClassLoaderUtil;
import com.isd.jec.util.StreamUtil;

import cri.pos.isd.km.ISDKeyManagementHelper;

import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;

/**
 * Encrypte the credit card number and expiration date using ISD HKM encryption. Also stores the
 * value in the database and writes to poslog.xml <br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author dnedumpurath
 * @created Sep 7, 2012
 * @version $Revision: 1105 $
 */
public class CriIsdHkmEncryptionOp
    extends Operation {

  private static final long serialVersionUID = 1L;
  private static final Logger logger_ = Logger.getLogger(CriIsdHkmEncryptionOp.class);
  public static final String ISD_ENCRYPTED_INFO = "CRI_ISD_Encrypted_Text";
  private EncryptionManager crypto = null;
  private String IsdKeyId = null;
  // ** Corresponds to "ISSDIAKP", not a String to hide from decompile ** /
  private static byte[] keyFileBytes = {73, 83, 83, 68, 73, 65, 75, 80};
  // ** Corresponds to "hexISSDIAKP", not a String to hide from decompile ** /
  private static byte[] hexKeyFileBytes = {104, 101, 120, 73, 83, 83, 68, 73, 65, 75, 80};
  private static final boolean readKeyFromDatabase = true;

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    ICreditDebitTenderLineItem tenderLine = (ICreditDebitTenderLineItem) cmd.getTenderLineItem();

    String clearAcctNbr = StringUtils.leftPad(tenderLine.getAccountNumber(), 19, "0");
    String clearExpDt = StringUtils.leftPad(tenderLine.getExpirationDateString(), 4, "0");
    String clearTxt = clearAcctNbr.concat(clearExpDt);

    String encryptedText = getIsdEncrptedString(clearTxt);

    tenderLine.setStringProperty(ISD_ENCRYPTED_INFO, encryptedText);
    return HELPER.completeResponse();
  }

  private String getIsdEncrptedString(String argClearText) {
    byte[] encryptedBytes = null;
    String encryptedText = null;

    if (createIsdEncryptionManager()) {
      try {
        byte[] clearBytes = argClearText.getBytes();
        encryptedBytes = crypto.encrypt(clearBytes);
        encryptedText = IsdEncryptionClient.hexencode(encryptedBytes);

        IsdKeyId = StringUtils.leftPad(IsdKeyId, 9, "0");
        encryptedText = IsdKeyId.concat(encryptedText);
      }
      catch (CryptoException e) {
        logger_.error("Crypto Error in ISD encryption: " + e);
      }
      catch (Exception ex) {
        logger_.error("General Error in ISD encryption: " + ex);
      }
    }
    return encryptedText;
  }

  private boolean createIsdEncryptionManager() {
    boolean success = false;
    byte[] keyBytes = null;

    try {
      if (readKeyFromDatabase) {
        keyBytes = getKeyBytesFromDb();
      }
      else {
        keyBytes = getKeyBytes(true);
      }

      crypto = EncryptionManager.obtainInstanceFromHexEncodedBytes(keyBytes);
      if (crypto != null) {
        success = true;
      }
    }
    catch (CryptoException ex) {
      logger_.error("Crypto Error in creating ISD EncryptionManager object: " + ex);
    }
    catch (Exception ex) {
      logger_.error("General Error in creating ISD EncryptionManager object: " + ex);
    }
    return success;
  }

  private byte[] getKeyBytesFromDb() {
    byte[] keyBytes = null;
    try {
      IsdKeyId = ISDKeyManagementHelper.getInstance().getKeyId();
      String isdKey = ISDKeyManagementHelper.getInstance().getCurrKeyValue();

      keyBytes = isdKey.getBytes();
    }
    catch (Exception ex) {
      logger_.error("Error obtaining Encryption Key bytes: " + ex);
    }
    return keyBytes;
  }

  private byte[] getKeyBytes(boolean hexFlag) {
    byte[] keyBytes = null;

    try {
      if (hexFlag) {
        keyBytes = getHexEncryptedKey();
      }
      else {
        keyBytes = getEncryptedKey();
      }
    }
    catch (IOException ex) {
      logger_.error("IO Error obtaining Encryption Key bytes: " + ex);
    }
    catch (Exception ex) {
      logger_.error("Error obtaining Encryption Key bytes: " + ex);
    }
    return keyBytes;
  }

  private byte[] getEncryptedKey()
      throws IOException, Exception {
    String keyFile = new String(keyFileBytes);

    // Attempt to get the resource in the most generic way.
    //
    // Load a resource from the classpath using the classloader for this class.
    //
    // Finds a resource with a given name.  This method returns null if no
    // resource with this name is found
    java.net.URL url = ClassLoaderUtil.loadResource(keyFile);
    if (url == null) {
      throw new Exception(keyFile + " not found.");
    }
    // contents of the file are stored in bytes
    return StreamUtil.readBytes(url.openStream());
  }

  private byte[] getHexEncryptedKey()
      throws IOException, Exception {
    String keyFile = new String(hexKeyFileBytes);
    byte[] hexKey = null;

    // Parse the file for the hex encoded string.
    java.net.URL url = ClassLoaderUtil.loadResource(keyFile);
    if (url == null) {
      throw new Exception(keyFile + " not found.");
    }
    else {
      BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));

      // skip 3 lines before reading the actual key. 2 line for key ID
      reader.readLine();
      reader.readLine();

      IsdKeyId = reader.readLine();

      // now actually create the 96byte array and read in the hex bytes
      hexKey = new byte[96];
      for (int i = 0; i < 96; i++ ) {
        hexKey[i] = (byte) reader.read();
      }
    }

    return hexKey;
  }

}
