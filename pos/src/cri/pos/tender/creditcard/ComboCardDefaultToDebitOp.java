//$Id: ComboCardDefaultToDebitOp.java 52 2012-06-12 15:42:42Z dtvdomain\aabdul $
package cri.pos.tender.creditcard;

import org.apache.log4j.Logger;

import dtv.hardware.HardwareMgr;
import dtv.hardware.IHardwareMgr;
import dtv.hardware.events.IHardwareInputEvent;
import dtv.pos.common.ConfigurationMgr;
import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.*;
import dtv.xst.dao.tnd.TenderStatus;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * Customization for CRI to default to DEBIT when a combo card is used<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Jun 8, 2012
 * @version $Revision: 52 $
 */
public class ComboCardDefaultToDebitOp
    extends Operation {

  private static final long serialVersionUID = 1L;
  private static final Logger logger_ = Logger.getLogger(ComboCardDefaultToDebitOp.class);

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {

    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    ICreditDebitTenderLineItem tenderLine = (ICreditDebitTenderLineItem) cmd.getTenderLineItem();
    TenderHelper.getInstance().setTenderIdType(tenderLine, TenderConstants.DEBIT_CARD);
    return HELPER.completeResponse();
  }

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {

    if (ConfigurationMgr.isPinpadRequiredForDebit()) {
      IHardwareMgr hm = HardwareMgr.getCurrentHardwareMgr();
      if (!hm.getPinPad().isPresent()) {
        logger_.info("no PIN pad present ergo prompt not applicable");
        return false;
      }
    }

    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    ITenderLineItem tenderLine = cmd.getTenderLineItem();
    Object inputEvent = tenderLine.getInputEvent();
    if (!(inputEvent instanceof IHardwareInputEvent)) {
      logger_.info("no hardware input event ergo prompt not applicable");
      return false;
    }
    if (!TenderHelper.getInstance().isCreditDebitAmbiguous((IHardwareInputEvent<?>) inputEvent)) {
      logger_.info("not ambiguous ergo prompt not applicable");
      return false;
    }
    if (cmd.getTenderStatus().equals(TenderStatus.REFUND)) {
      return false;
    }
    return true;

  }

}
