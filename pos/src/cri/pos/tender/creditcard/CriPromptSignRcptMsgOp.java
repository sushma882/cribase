//$Id: CriPromptSignRcptMsgOp.java 1065 2016-08-22 20:21:07Z olr.jgaughn $
package cri.pos.tender.creditcard;

import cri.pos.tender.CriTenderHelper;

import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.creditcard.PromptSignRcptMsgOp;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * Overrides superclass behavior by using consistent "is receipt needed?" logic.<br>
 * <br>
 * Copyright (c) 2016 OLR Retail
 *
 * @author johgaug
 * @created Aug 22, 2016
 * @version $Revision: 1065 $
 */
public class CriPromptSignRcptMsgOp
    extends PromptSignRcptMsgOp {

  private static final long serialVersionUID = 1L;

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    boolean applicable = false;
    IPosTransaction trans = getTransaction(argCmd);

    for (ITenderLineItem lineItem : trans.getLineItems(ITenderLineItem.class)) {
      if (((CriTenderHelper) TENDER_HELPER).needsSignatureCapture(lineItem)) {
        applicable = true;
        break;
      }
    }
    return applicable;
  }

}
