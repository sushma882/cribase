//$Id: CriVoidTenderAuthorizationOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.tender.exchange;

import static dtv.pos.iframework.type.VoucherActivityCodeType.*;

import dtv.pos.tender.exchange.VoidTenderAuthorizationOp;
import dtv.tenderauth.AuthRequestType;
import dtv.xst.dao.tnd.TenderStatus;
import dtv.xst.dao.ttr.ITenderLineItem;
import dtv.xst.dao.ttr.IVoucherTenderLineItem;

/**
 * Customization to return VOID_CASH_OUT if the tender type is CASH_OUT<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author aabdul
 * @created Aug 10, 2012
 * @version $Revision: 1105 $
 */
public class CriVoidTenderAuthorizationOp
    extends VoidTenderAuthorizationOp {

  private static final long serialVersionUID = 1L;

  /**
   * getAuthReqType
   * 
   * @param argTndrLine
   * @return AuthRequestType
   */
  @Override
  protected AuthRequestType getAuthReqType(ITenderLineItem argTndrLine) {
    if (argTndrLine instanceof IVoucherTenderLineItem) {
      IVoucherTenderLineItem voucherTndrLine = (IVoucherTenderLineItem) argTndrLine;
      String activityCode = voucherTndrLine.getActivityCode();

      if (ISSUED.matches(activityCode)) {
        return AuthRequestType.VOID_ACTIVATE;
      }
      else if (RELOAD.matches(activityCode)) {
        return AuthRequestType.VOID_RELOAD;
      }
      else if (CASHOUT.matches(activityCode)) {
        return AuthRequestType.VOID_CASH_OUT;
      }
    }

    if (TenderStatus.TENDER.matches(argTndrLine.getTenderStatusCode())) {
      return AuthRequestType.VOID_TENDER;
    }
    return AuthRequestType.VOID_REFUND_TENDER;
  }
}
