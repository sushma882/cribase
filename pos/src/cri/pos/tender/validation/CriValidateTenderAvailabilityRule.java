//$Id$
package cri.pos.tender.validation;

import dtv.i18n.IFormattable;
import dtv.pos.iframework.validation.*;
import dtv.pos.tender.TenderAvailabilityCodeType;
import dtv.pos.tender.TenderHelper;
import dtv.pos.tender.validation.TenderAvailableValidationData;
import dtv.pos.util.validation.AbstractValidationRule;
import dtv.xst.dao.tnd.ITender;
import dtv.xst.dao.trl.IRetailTransaction;
import dtv.xst.dao.trl.ISaleReturnLineItem;

/**
 * Operation that validates that the credit card type can be used in the current context on the
 * current transaction.<br>
 * <br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author pgolli
 * @created Apr 11, 2012
 * @version $Revision$
 */
public class CriValidateTenderAvailabilityRule
    extends AbstractValidationRule {

  @Override
  public IValidationResult validate(IValidationData argObject) {
    TenderAvailableValidationData data = (TenderAvailableValidationData) argObject;

    /*TenderAvailabilityCodeType availabilityType =
        TenderAvailabilityCodeType.forTenderUsageCodeType(data.getTenderUsageCodeType());*/
    TenderAvailabilityCodeType availabilityType = data.getTenderAvailabilityCodeType();

    final TenderHelper th = TenderHelper.getInstance();
    ITender tenderAvailable = th.tenderAvailable(availabilityType, data.getTender().getTenderId(),
        "ValidateTenderAvailabilityRule");

    //2.8 Gift receipt return - Return to gift card, regardless of tender used to make purchase.
    boolean giftCardFlag = false, giftReceiptPresent = false;
    String tenderId = data.getTender().getTenderId();
    giftCardFlag = tenderId.equalsIgnoreCase("ISSUE_GIFT_CARD")
        || tenderId.equalsIgnoreCase("RELOAD_GIFT_CARD") || tenderId.equalsIgnoreCase("ISSUE_ISD_GIFT_CARD")
        || tenderId.equalsIgnoreCase("RELOAD_ISD_GIFT_CARD");

    if (data.getTransaction() instanceof IRetailTransaction) {
      for (ISaleReturnLineItem saleItem : data.getTransaction().getLineItems(ISaleReturnLineItem.class)) {
        if (saleItem.getReturn() && !saleItem.getVoid() && saleItem.getReturnedWithGiftReceipt()) {
          giftReceiptPresent = true;
          break;
        }
      }
    }

    if ((tenderAvailable == null) && !giftCardFlag && !giftReceiptPresent) {
      IFormattable tenderDescription = FF.getSimpleFormattable(data.getTender().getDescription());
      IFormattable msg = FF.getTranslatable("_tenderNotAvailable",
          new IFormattable[] {tenderDescription, data.getTenderUsageCodeType()});

      return SimpleValidationResult.getFailed(msg);
    }
    return SimpleValidationResult.getPassed();
  }

}
