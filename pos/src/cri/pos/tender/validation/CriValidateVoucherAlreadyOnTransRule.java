//$Id$
package cri.pos.tender.validation;

import java.util.List;

import dtv.i18n.IFormattable;
import dtv.pos.framework.StationModelMgr;
import dtv.pos.iframework.type.VoucherActivityCodeType;
import dtv.pos.iframework.ui.model.IStationModel;
import dtv.pos.iframework.validation.*;
import dtv.pos.tender.TenderHelper;
import dtv.pos.tender.voucher.config.VoucherConfig;
import dtv.pos.tender.voucher.config.VoucherRootConfig;
import dtv.pos.util.validation.AbstractValidationRule;
import dtv.xst.dao.trl.IRetailTransactionLineItem;
import dtv.xst.dao.trl.IVoucherLineItem;
import dtv.xst.dao.trn.IPosTransaction;

/**
 * Check if the specified item is a voucher line item and if it has already been used on the current
 * transaction. This validation is typically handled when processing the voucher number entry.
 * However, the number is never entered on a verified return, which is why this rule exists in the
 * return verification package. It is most likely the only place where it becomes necessary to use
 * this functionality as a separate rule. Copyright (c) 2006 Datavantage Corporation <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author malghzaw
 * @created Oct 14, 2015
 * @version $Revision$
 */
public class CriValidateVoucherAlreadyOnTransRule
    extends AbstractValidationRule {

  public IFormattable getVoucherTypeFormattable(String argType) {
    TenderHelper TH = TenderHelper.getInstance();
    VoucherRootConfig voucherRootConfig = TH.getVoucherRootConfig();
    VoucherConfig vc = voucherRootConfig.getVoucherConfig(argType);

    if (vc != null) {
      return vc.getDescription();
    }

    return FF.getTranslatable("_voucher");
  }

  @Override
  public IValidationResult validate(IValidationData argData) {
    IValidationResult result = IValidationResult.SUCCESS;

    IStationModel stationModel = StationModelMgr.getInstance().getStationModel();
    IPosTransaction trans = stationModel.getCurrentTransaction();
    List<IRetailTransactionLineItem> lineItems = trans.getRetailTransactionLineItems();

    // check that this voucher is not already on the transaction
    for (IRetailTransactionLineItem lineItem : lineItems) {

      if (lineItem.getVoid()) {
        continue;
      }

      //Updated condition for AC#457634 - FB#367869.
      if ((lineItem instanceof IVoucherLineItem)
          && (VoucherActivityCodeType.RELOAD.matches(((IVoucherLineItem) lineItem).getActivityCode())
              || VoucherActivityCodeType.ISSUED.matches(((IVoucherLineItem) lineItem).getActivityCode()))) {
        IFormattable msg = FF.getTranslatable("_criPromptMsgGCSaleTenderedWithGC");
        result = SimpleValidationResult.getFailed(msg);
      }
    }

    return result;
  }

}
