//$Id$
package cri.pos.tender.validation;

import java.util.List;

import dtv.pos.common.AbstractValidationWithoutPromptOp;
import dtv.pos.common.ValidationKey;
import dtv.pos.framework.StationModelMgr;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.iframework.ui.model.IStationModel;
import dtv.pos.iframework.validation.*;
import dtv.xst.dao.trl.IRetailTransactionLineItem;
import dtv.xst.dao.trl.IVoucherLineItem;
import dtv.xst.dao.trn.IPosTransaction;

/**
 * Non-prompting operation that applies the validation rule determining whether a specified voucher
 * is already on the current transaction.<br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author malghzaw
 * @created Oct 14, 2015
 * @version $Revision$
 */
public class CriValidateVoucherAlreadyOnTransOp
    extends AbstractValidationWithoutPromptOp {

  private static final long serialVersionUID = 1L;
  public static final IValidationKey CRI_VOUCHER_ALREADY_ON_TRANS_VALKEY =
      ValidationKey.valueOf("CRI_VOUCHER_ALREADY_ON_TRANS");

  @Override
  public IValidationData getValidationData(IXstCommand argCmd, IXstEvent argEvent, IValidationKey argKey) {
    IPosTransaction trans = argCmd.getStationModel().getCurrentTransaction();
    return new ValidationData(ValidationDataType.OBJECT, trans.getSaleLineItems());
  }

  @Override
  public IValidationKey[] getValidationKeys(IXstCommand argCmd) {
    return new IValidationKey[] {CRI_VOUCHER_ALREADY_ON_TRANS_VALKEY};
  }

  /**
   * {@inheritDoc} This operation is not applicable unless the original sale line item on the
   * command is not null and it is a voucher line item.
   */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    IStationModel stationModel = StationModelMgr.getInstance().getStationModel();
    IPosTransaction trans = stationModel.getCurrentTransaction();
    List<IRetailTransactionLineItem> lineItems = trans.getRetailTransactionLineItems();

    // check that this voucher is not already on the transaction
    for (IRetailTransactionLineItem lineItem : lineItems) {

      if (lineItem.getVoid()) {
        continue;
      }

      if (lineItem instanceof IVoucherLineItem) {
        return true;
      }
    }
    return false;

  }
}
