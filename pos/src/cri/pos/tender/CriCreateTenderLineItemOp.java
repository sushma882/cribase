//$Id: CriCreateTenderLineItemOp.java 785 2014-05-23 01:40:51Z suz.bshi $
package cri.pos.tender;

import static cri.pos.common.CriConstants.CLOTHING_VOUCHER_AMOUNT;
import static cri.pos.common.CriConstants.CLOTHING_VOUCHER_NUMBER;
import static dtv.pos.framework.action.type.XstChainActionType.START;
import static dtv.pos.iframework.op.OpStatus.COMPLETE_HALT;

import dtv.pos.common.OpChainKey;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.*;
import dtv.pos.tender.CreateTenderLineItemOp;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.util.config.IConfigObject;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * Back to Sale item screen.<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 * 
 * @author Bob.Shi
 * @created May 20, 2014
 * @version $Revision: 785 $
 */
public class CriCreateTenderLineItemOp
    extends CreateTenderLineItemOp {

  private static final long serialVersionUID = 1185603869747675394L;
  private IOpChainKey backChainKey_ = null;

  @Override
  public IOpResponse handleOpReverse(IXstCommand argCmd, IXstEvent argEvent) {
    return HELPER.getRunChainResponse(COMPLETE_HALT, getBackChainKey(argCmd), argCmd, argEvent, START);
  }

  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    IOpResponse response = super.handleOpExec(argCmd, argEvent);
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    ITenderLineItem line = cmd.getTenderLineItem();
    line.setStringProperty(CLOTHING_VOUCHER_NUMBER, (String) cmd.getValue(CLOTHING_VOUCHER_NUMBER));
    line.setStringProperty(CLOTHING_VOUCHER_AMOUNT, String.valueOf(cmd.getValue(CLOTHING_VOUCHER_AMOUNT)));
    return response;
  }

  @Override
  public void setParameter(String argName, IConfigObject argValue) {
    if ("BackChainKey".equalsIgnoreCase(argName)) {
      setBackChainKey(OpChainKey.valueOf(argValue.toString()));
    }
    else {
      super.setParameter(argName, argValue);
    }
  }

  public IOpChainKey getBackChainKey(IXstCommand argCmd) {
    return backChainKey_;
  }

  public void setBackChainKey(IOpChainKey argBackChainKey) {
    backChainKey_ = argBackChainKey;
  }

}
