//$Id: CriPromptPhoneNumberOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.tender.check;

import dtv.pos.common.*;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.iframework.ui.config.IPromptKey;
import dtv.pos.iframework.validation.*;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.pos.tender.TenderConstants;
import dtv.xst.dao.ttr.ICheckTenderLineItem;

/**
 * Prompt the user to enter the customer's phone number.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Aug 15, 2012
 * @version $Revision: 1105 $
 */
public class CriPromptPhoneNumberOp
    extends AbstractValidationOp {

  private static final long serialVersionUID = 1L;
  private static final IValidationKey[] VALIDATIONS =
      new IValidationKey[] {ValidationKey.valueOf("PHONE_NUMBER")};

  /** {@inheritDoc} */
  @Override
  public IValidationData getValidationData(IXstCommand argCmd, IXstEvent argEvent, IValidationKey argKey) {
    return new ValidationData((String) argEvent.getData());
  }

  /**
   * Return an array of validation keys to use for the validation process. The validation keys map
   * to rules which are applied to the data to validate.
   * 
   * @param argCmd current command
   * @return an array of validation keys to use for the validation process.
   */
  @Override
  public IValidationKey[] getValidationKeys(IXstCommand argCmd) {
    return VALIDATIONS;
  }

  /**
   * Return true if this operation is applicable for the current state of the command (and system).
   * 
   * @param argCmd the current command
   * @return true if this operation is applicable for the current state of the command (and system).
   */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    if (argCmd instanceof ISaleTenderCmd) {
      ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
      if (cmd.getTender() != null) {
        return cmd.getTender().getTenderTypecode().equals(TenderConstants.CHECK);
      }
    }
    return false;
  }

  /**
   * Return the prompt key to use for the initial prompt displayed to the user.
   * 
   * @param argCmd current command
   * @return the prompt key to use for the initial prompt displayed to the user.
   */
  @Override
  protected IPromptKey getPromptKey(dtv.pos.iframework.op.IXstCommand argCmd) {
    return PromptKey.valueOf("CRI.ENTER_PHONE_NUMBER");
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handlePromptResponse(IXstCommand argCmd, IXstEvent argEvent) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    ICheckTenderLineItem tender = ((ICheckTenderLineItem) cmd.getTenderLineItem());

    tender.setStringProperty("PHONE_NUMBER", (String) argEvent.getData());
    return HELPER.completeResponse();
  }
}
