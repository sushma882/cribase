//$Id$
package cri.pos.tender.check;

import java.io.File;

import cri.pos.common.CriConfigurationMgr;
import cri.pos.common.CriConstants;

import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2016 MICROS Retail
 *
 * @author aayco
 * @created Mar 17, 2016
 * @version $Revision$
 */
public class CriCheckStoreDownEntryModeOp
    extends Operation {

  private static final long serialVersionUID = 1L;

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argArg1) {
    String path = CriConfigurationMgr.getStoreDownEntryModeFlagFilePath();
    String flagfile = CriConfigurationMgr.getStoreDownEntryModeFlagFile();

    if (isFlagFileExist(path + "/" + flagfile)) {
      argCmd.setValue(CriConstants.CRI_STOREDOWN_ENTRYMODE, CriConstants.CRI_STOREDOWN_ENTRYMODE);
    }

    return HELPER.completeResponse();
  }

  private boolean isFlagFileExist(String argFile) {
    File flagfile = new File(argFile);

    if (flagfile.exists()) {
      return true;
    }

    return false;
  }
}
