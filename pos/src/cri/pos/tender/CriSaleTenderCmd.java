//$Id: CriSaleTenderCmd.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.tender;

import dtv.pos.tender.SaleTenderCmd;
import dtv.tenderauth.event.IAuthResponse;

/**
 * The command responsible for managing the data related to the tendering.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Sep 29, 2015
 * @version $Revision: 1105 $
 */
public class CriSaleTenderCmd
    extends SaleTenderCmd {
  /**
   * 
   */
  private static final long serialVersionUID = -7299738273509453996L;
  private IAuthResponse safAuthRequiredResponse_;

  public IAuthResponse getSAFAuthRequiredResponse() {
    return safAuthRequiredResponse_;
  }

  public void setSAFAuthRequiredResponse(IAuthResponse argResponse) {
    safAuthRequiredResponse_ = argResponse;
  }
}
