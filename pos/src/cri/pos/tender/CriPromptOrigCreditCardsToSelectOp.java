//$Id$
package cri.pos.tender;

import org.apache.log4j.Logger;

import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.PromptOrigCreditCardsToSelectOp;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 *
 * @author pgolli
 * @created Feb 6, 2014
 * @version $Revision$
 */
public class CriPromptOrigCreditCardsToSelectOp
    extends PromptOrigCreditCardsToSelectOp {

  private static final long serialVersionUID = 1L;
  private static final Logger logger_ = Logger.getLogger(CriPromptOrigCreditCardsToSelectOp.class);

  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    return false;
  }
}
