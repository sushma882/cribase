//$Id: CriRedirectToOutgoingTenderOp.java 108 2012-07-10 17:25:21Z dtvdomain\aabdul $
package cri.pos.tender.sv;

import java.util.List;

import dtv.pos.common.OpChainKey;
import dtv.pos.framework.action.type.XstChainActionType;
import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.*;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.xst.dao.trl.IRetailTransactionLineItem;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * Operation to automatically redirect to the LOCAL_CURRENCY tender for ISD Gift card CASH OUT<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Jul 2, 2012
 * @version $Revision: 108 $
 */
public class CriRedirectToOutgoingTenderOp
    extends Operation {

  private static final long serialVersionUID = 1L;
  private static final String ISD_GIFT_CARD = "ISD_GIFT_CARD";
  private static final String ISD_GIFT_ECARD = "ISD_GIFT_ECARD";
  //Command key used to identify whether to skip this operation.
  private static final String CRI_KEY_SKIP_PROMPT = "PromptOrigCreditCardsToSelectOp_Skip_Prompt";
  // Value used to identify whether to skip this operation.
  private static final String CRI_VALUE_SKIP_PROMPT = "PromptOrigCreditCardsToSelectOp_Skip_Prompt";

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argParamIXstEvent) {

    argCmd.setValue(CRI_KEY_SKIP_PROMPT, CRI_VALUE_SKIP_PROMPT);
    return HELPER.getRunChainResponse(OpStatus.COMPLETE_HALT,
        OpChainKey.valueOf("OUTGOING_TENDER_EXCHANGE_LOCAL_CURRENCY"), argCmd, XstChainActionType.START);
  }

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {

    // If this operation previously ran and configured itself not to run again...
    if (isSkipFlagSet(argCmd)) {
      return false;
    }

    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    IPosTransaction tran = cmd.getTransaction();
    List<IRetailTransactionLineItem> lineItems = tran.getTenderLineItems();

    //Currently single gift card is allowed for cash out in a transaction
    if ((lineItems != null) && (lineItems.size() == 1)) {
      ITenderLineItem tenderLine = (ITenderLineItem) lineItems.get(0);
      String tenderId = tenderLine.getTenderId();
      if ((ISD_GIFT_CARD.equals(tenderId) || ISD_GIFT_ECARD.equals(tenderId))) {
        return true;
      }
    }
    return false;
  }

  /**
   * Return whether the flag is set on the command signaling that this operation is done and should
   * not be run again.
   * 
   * @param argCmd the command to check.
   * @return <code>true</code> if the flag is set, <code>false</code> if it is not.
   */
  private static boolean isSkipFlagSet(IXstCommand argCmd) {
    return CRI_VALUE_SKIP_PROMPT.equals(argCmd.getValue(CRI_KEY_SKIP_PROMPT));
  }
}
