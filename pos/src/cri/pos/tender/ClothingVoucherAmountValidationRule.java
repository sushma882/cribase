//$Id: ClothingVoucherAmountValidationRule.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.tender;

import static dtv.pos.iframework.validation.IValidationResult.SUCCESS;

import java.math.BigDecimal;

import dtv.pos.iframework.validation.*;
import dtv.pos.util.validation.AbstractValidationRule;
import dtv.util.NumberUtils;

/**
 * Validate the entered the clothing voucher amount.<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 *
 * @author Bob.Shi
 * @created May 20, 2014
 * @version $Revision: 1105 $
 */
public class ClothingVoucherAmountValidationRule
    extends AbstractValidationRule {

  /** {@inheritDoc} */
  @Override
  public IValidationResult validate(IValidationData argObject) {
    IValidationResult result = SimpleValidationResult.getFailed("_criInvalidVoucherAmount");
    BigDecimal amount = argObject.getBigDecimal();
    if (NumberUtils.equivalent(amount, BigDecimal.valueOf(100))
        || NumberUtils.equivalent(amount, BigDecimal.valueOf(200))) {
      result = SUCCESS;
    }
    return result;
  }
}
