//$Id: CriTenderHelper.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.tender;

import org.apache.log4j.Logger;

import cri.ajb.fipay.CardType;
import cri.ajb.fipay.response.AjbFiPayCreditDebitResponse;

import dtv.data2.access.DataPropertyUtils;
import dtv.pos.tender.TenderHelper;
import dtv.tenderauth.event.IAuthResponse;
import dtv.util.StringUtils;
import dtv.xst.dao.tnd.ITender;
import dtv.xst.dao.trl.IAuthorizableLineItem;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.ttr.ITenderLineItem;
import dtv.xst.dao.ttr.ITenderSignature;

/**
 * Tender utility class.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 1105 $
 */
public class CriTenderHelper
    extends TenderHelper {

  private static final Logger _logger = Logger.getLogger(CriTenderHelper.class);

  protected static final String SIGN_FLAG = "SIGN_REQUIRED";
  protected static final String ORIGINAL_INVOICE = "ORIGINAL_INVOICE";

  public static final int RETAIL_LOCATION_ID = 1;
  public static final int WORKSTATION_ID = 2;
  public static final int TRANSACTION_ID = 3;
  public static final int LINE_ITEM_SEQ_ID = 4;
  public static final int INCREMENT_ID = 5;

  public static CriTenderHelper getInstance() {
    /* This really is unnecessary: this class overrides nothing. But it is not worth refactoring
     * until a base upgrade, when Spring changes how all of this works anyway. */
    return (CriTenderHelper) TenderHelper.getInstance();
  }

  public boolean needsSignatureCapture(ITenderLineItem argLineItem) {
    boolean lineVoid = argLineItem.getVoid();
    ITenderSignature sig = argLineItem.getSignature();
    boolean sigPresent = ((sig != null) && !StringUtils.isEmpty(sig.getSignature()));
    boolean sigRequiredByXstore = isSignatureRequired(argLineItem);
    boolean sigRequiredByAjb = getSignatureRequired(argLineItem);

    _logger.warn("Line item: " + argLineItem.getObjectIdAsString());

    _logger.warn("Void=" + lineVoid + ", XstoreSign=" + sigRequiredByXstore + ", AjbSign=" + sigRequiredByAjb
        + ", SigPresent=" + sigPresent);

    boolean result = !lineVoid && sigRequiredByXstore && sigRequiredByAjb && !sigPresent;

    _logger.warn("SignatureCaptureNeeded=" + result);

    return result;
  }

  /**
   * Get the xstore tender by the AJB BinLookup Response.
   * 
   * @param argResponse
   * @return
   */
  public ITender getTender(IAuthResponse argResponse) {
    if (argResponse instanceof AjbFiPayCreditDebitResponse) {
      String tenderId = CardType.getXstoreTender(((AjbFiPayCreditDebitResponse) argResponse).getCardType());
      if (tenderId == null) {
        tenderId = "DEBIT_CARD";
      }
      return super.getTender(tenderId, null);
    }
    else {
      return null;
    }
  }

  /**
   * Get the signature required flag.
   * 
   * @param argLineItem
   * @return
   */
  public boolean getSignatureRequired(ITenderLineItem argLineItem) {
    return (argLineItem != null) && argLineItem.getBooleanProperty(SIGN_FLAG);
  }

  /**
   * Set the singature required flag.
   * 
   * @param argLineItem
   * @param argRequired
   */
  public void setSignatureRequired(ITenderLineItem argLineItem, boolean argRequired) {
    if (argLineItem != null) {
      argLineItem.setBooleanProperty(SIGN_FLAG, argRequired);
    }
  }

  /**
   * Get the AJB FiPay original invoice nbr.
   * 
   * @param argLineItem
   * @return
   */
  public String getOriginalInvoiceNbr(IAuthorizableLineItem argLineItem) {
    return argLineItem == null ? null : argLineItem.getStringProperty(ORIGINAL_INVOICE);
  }

  /**
   * Set the AJB FiPay original invoice nbr.
   * 
   * @param argLineItem
   * @param argInvoiceNbr
   */
  public void setOriginalInvoiceNbr(IAuthorizableLineItem argLineItem, String argInvoiceNbr) {
    if (argLineItem != null) {
      argLineItem.setStringProperty(ORIGINAL_INVOICE, argInvoiceNbr);
    }
  }

  public long getInvoiceId(IAuthorizableLineItem argLineItem, int argSeqType) {
    String invoiceNbr = getOriginalInvoiceNbr(argLineItem);
    if (!StringUtils.isEmpty(invoiceNbr)) {
      int index = 0;
      String storeIdString = invoiceNbr.substring(index, index += getStoreIdLength());
      String registerIdString = invoiceNbr.substring(index, index += getRegisterIdLength());
      String tranSeqString = invoiceNbr.substring(index, index += getTranSeqLength());
      String lineItemSeqString = invoiceNbr.substring(index, index += getLineItemSeqLength());
      String incrementSeqString = invoiceNbr.substring(index, index += getIncrementSeqLength());

      switch (argSeqType) {
        case RETAIL_LOCATION_ID:
          return Long.valueOf(storeIdString);
        case WORKSTATION_ID:
          return Long.valueOf(registerIdString);
        case TRANSACTION_ID:
          return Long.valueOf(tranSeqString);
        case LINE_ITEM_SEQ_ID:
          return Long.valueOf(lineItemSeqString);
        case INCREMENT_ID:
          return Long.valueOf(incrementSeqString);
        default:
          return -1;
      }
    }

    return -1;
  }

  public long getInvoiceId(String argInvoiceNbr, int argSeqType) {
    if (!StringUtils.isEmpty(argInvoiceNbr)) {
      int index = 0;
      String storeIdString = argInvoiceNbr.substring(index, index += getStoreIdLength());
      String registerIdString = argInvoiceNbr.substring(index, index += getRegisterIdLength());
      String tranSeqString = argInvoiceNbr.substring(index, index += getTranSeqLength());
      String lineItemSeqString = argInvoiceNbr.substring(index, index += getLineItemSeqLength());
      String incrementSeqString = argInvoiceNbr.substring(index, index += getIncrementSeqLength());

      switch (argSeqType) {
        case RETAIL_LOCATION_ID:
          return Long.valueOf(storeIdString);
        case WORKSTATION_ID:
          return Long.valueOf(registerIdString);
        case TRANSACTION_ID:
          return Long.valueOf(tranSeqString);
        case LINE_ITEM_SEQ_ID:
          return Long.valueOf(lineItemSeqString);
        case INCREMENT_ID:
          return Long.valueOf(incrementSeqString);
        default:
          return -1;
      }
    }

    return -1;
  }

  /**
   * Generate the unique AJB FiPay request invoice number.
   * 
   * @param argLineItem
   * @return
   */
  public String buildInvoiceNbr(IAuthorizableLineItem argLineItem) {
    String storeIdString = encodeNumber(argLineItem.getRetailLocationId(), getStoreIdLength());
    String registerIdString = encodeNumber(argLineItem.getWorkstationId(), getRegisterIdLength());
    String tranSeqString = encodeNumber(argLineItem.getTransactionSequence(), getTranSeqLength());
    String lineItemSeqString =
        encodeNumber(argLineItem.getRetailTransactionLineItemSequence(), getLineItemSeqLength());
    String incrementSeqString = encodeNumber(1L, getIncrementSeqLength());

    StringBuilder builder = new StringBuilder();
    builder.append(storeIdString);
    builder.append(registerIdString);
    builder.append(tranSeqString);
    builder.append(lineItemSeqString);
    builder.append(incrementSeqString);
    return builder.toString();
  }

  /**
   * Generate the unique AJB FiPay request invoice number by increment.
   * 
   * @param argLineItem
   * @param argIncrement
   * @return
   */
  public String buildInvoiceNbr(IAuthorizableLineItem argLineItem, int argIncrement) {
    String invoiceNbr = getOriginalInvoiceNbr(argLineItem);
    if (StringUtils.isEmpty(invoiceNbr)) {
      invoiceNbr = buildInvoiceNbr(argLineItem);
    }

    int index = 0;
    String storeIdString = invoiceNbr.substring(index, index += getStoreIdLength());
    String registerIdString = invoiceNbr.substring(index, index += getRegisterIdLength());
    String tranSeqString = invoiceNbr.substring(index, index += getTranSeqLength());
    String lineItemSeqString = invoiceNbr.substring(index, index += getLineItemSeqLength());
    String incrementSeqString = invoiceNbr.substring(index, index += getIncrementSeqLength());

    long increment = Long.valueOf(incrementSeqString);
    increment = increment + argIncrement;
    incrementSeqString = encodeNumber(increment, getIncrementSeqLength());

    StringBuilder builder = new StringBuilder();
    builder.append(storeIdString);
    builder.append(registerIdString);
    builder.append(tranSeqString);
    builder.append(lineItemSeqString);
    builder.append(incrementSeqString);
    return builder.toString();
  }

  public String encodeNumber(long argNumber, int argLength) {
    StringBuffer sb = new StringBuffer(Long.toString(argNumber));
    StringUtils.leftPad(sb, argLength, '0');

    return sb.toString();
  }

  /**
   * Get the store ID length.
   * 
   * @return
   */
  public int getStoreIdLength() {
    return 4;
  }

  /**
   * Get the register ID length.
   * 
   * @return
   */
  public int getRegisterIdLength() {
    return 3;
  }

  /**
   * Get the transaction seq length.
   * 
   * @return
   */
  public int getTranSeqLength() {
    return 5;
  }

  /**
   * Get the line item seq length.
   * 
   * @return
   */
  public int getLineItemSeqLength() {
    return 3;
  }

  /**
   * Get the increment seq length.
   * 
   * @return
   */
  public int getIncrementSeqLength() {
    return 3;
  }

  /**
   * Set transaction property
   * @param argTrans
   * @param property
   * @param value
   */
  public static void setTransactionProperty(IPosTransaction argTrans, String property, String value) {
    DataPropertyUtils.setPropertyValue(argTrans, property, value);
  }
}
