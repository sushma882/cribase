//$Id: PromptClothingVoucherAmountOp.java 785 2014-05-23 01:40:51Z suz.bshi $
package cri.pos.tender;

import static cri.pos.common.CriConstants.CLOTHING_VOUCHER_AMOUNT;

import dtv.pos.common.AbstractValidationOp;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.iframework.validation.*;
import dtv.pos.tender.ISaleTenderCmd;

/**
 * Prompt clothing voucher amount.<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 * 
 * @author Bob.Shi
 * @created May 20, 2014
 * @version $Revision: 785 $
 */
public class PromptClothingVoucherAmountOp
    extends AbstractValidationOp {

  private static final long serialVersionUID = 1L;

  /** {@inheritDoc} */
  @Override
  public IValidationData getValidationData(IXstCommand argCmd, IXstEvent argEvent, IValidationKey argKey) {
    return new ValidationData(getBigDecimal(argEvent));
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handlePromptResponse(IXstCommand argCmd, IXstEvent argEvent) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    cmd.setValue(CLOTHING_VOUCHER_AMOUNT, getBigDecimal(argEvent));
    return HELPER.completeResponse();
  }
}
