//$Id: CriDepositBagRcptBean.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import java.math.BigDecimal;
import java.util.*;

import cri.xst.dao.tsn.ICriDepositBag;
import cri.xst.dao.tsn.ICriDepositBagDetailItem;

import dtv.xst.dao.trn.IPosTransaction;

/**
 * Java bean for the store deposit bag conveyed receipt.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Jul 29, 2012
 * @version $Revision: 1105 $
 */
public class CriDepositBagRcptBean {
  private final IPosTransaction relatedTrans_;
  private final Map<String, BigDecimal> checkDepositItem_ = new HashMap<String, BigDecimal>();
  private final Map<String, BigDecimal> cashDepositItem_ = new HashMap<String, BigDecimal>();
  private List<ICriDepositBag> depositBagList_ = new ArrayList<ICriDepositBag>();
  private final List<DetailItem> detailItemList_ = new ArrayList<DetailItem>();

  /**
   * Constructs a <code>CriDepositBagRcptBean</code>.
   * @param argTrans
   * @param argDepositBagList
   */
  public CriDepositBagRcptBean(IPosTransaction argTrans, List<ICriDepositBag> argDepositBagList) {
    relatedTrans_ = argTrans;
    depositBagList_ = argDepositBagList;

    for (ICriDepositBag bag : depositBagList_) {
      if (CriBagTypes.CASH.matches(bag.getType())) {
        for (ICriDepositBagDetailItem detailItem : bag.getDepositDetails()) {
          if (!cashDepositItem_.containsKey(detailItem.getDescription())) {
            cashDepositItem_.put(detailItem.getDescription(), detailItem.getAmount());
          }
          else {
            BigDecimal preAmount = cashDepositItem_.get(detailItem.getDescription());
            cashDepositItem_.put(detailItem.getDescription(), preAmount.add(detailItem.getAmount()));
          }
        }
      }

      if (CriBagTypes.CHECKS.matches(bag.getType())) {
        for (ICriDepositBagDetailItem detailItem : bag.getDepositDetails()) {
          if (!checkDepositItem_.containsKey(detailItem.getDescription())) {
            checkDepositItem_.put(detailItem.getDescription(), detailItem.getAmount());
          }
          else {
            BigDecimal preAmount = checkDepositItem_.get(detailItem.getDescription());
            checkDepositItem_.put(detailItem.getDescription(), preAmount.add(detailItem.getAmount()));
          }
        }
      }
    }

    if (!cashDepositItem_.values().isEmpty()) {
      for (String key : cashDepositItem_.keySet()) {
        detailItemList_.add(new DetailItem(key, cashDepositItem_.get(key)));
      }
    }

    if (!checkDepositItem_.values().isEmpty()) {
      for (String key : checkDepositItem_.keySet()) {
        detailItemList_.add(new DetailItem(key, checkDepositItem_.get(key)));
      }
    }

  }

  /**
   * 
   * @return
   */
  public List<ICriDepositBag> getDepositBagList() {
    return depositBagList_;
  }

  /**
   * 
   * @return
   */
  public IPosTransaction getRelatedTransaction() {
    return relatedTrans_;
  }

  /**
   * 
   * @return
   */
  public List<DetailItem> getDetailItemList() {
    return detailItemList_;
  }

  /**
   * 
   * @return
   */
  public BigDecimal getTotalAmount() {
    BigDecimal totalAmount = BigDecimal.ZERO;
    for (DetailItem detailItem : detailItemList_) {
      totalAmount = totalAmount.add(detailItem.getAmount());
    }

    return totalAmount;
  }

  /**
   * The class of the deposit bag receipt detail count items.<br>
   * <br>
   * Copyright (c) 2012 MICROS Retail
   *
   * @author shy xie
   * @created Jul 29, 2012
   * @version $Revision: 1105 $
   */
  protected class DetailItem {
    private final String description_;
    private final BigDecimal amount_;

    public DetailItem(String argDesc, BigDecimal argAmount) {
      description_ = argDesc;
      amount_ = argAmount;
    }

    public String getDescription() {
      return description_;
    }

    public BigDecimal getAmount() {
      return amount_;
    }
  }

}
