//$Id: CriDepositBagLogOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import java.util.List;

import cri.xst.dao.tsn.ICriDepositBag;

import dtv.logbuilder.ILogBuilder;
import dtv.logbuilder.LogBuilder;
import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;

public class CriDepositBagLogOp
    extends Operation {
  /**
   * 
   */
  private static final long serialVersionUID = -6810241516226642569L;

  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    ILogBuilder builder = LogBuilder.getInstance();
    for (ICriDepositBag bagItem : getDepositBags(argCmd)) {
      builder.saveLogEntry(bagItem);
    }
    return HELPER.completeResponse();
  }

  protected List<ICriDepositBag> getDepositBags(IXstCommand argCmd) {
    ICriDepositBagCmd cmd = (ICriDepositBagCmd) argCmd;
    return cmd.getSelectedDepositItems();
  }
}
