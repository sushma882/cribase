//$Id: CriFilterPersistableDepositBagOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import java.util.ArrayList;
import java.util.List;

import cri.xst.dao.tsn.ICriDepositBag;

import dtv.data2.access.IPersistable;
import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;

/**
 * Get the persistable deposit bag from persistable queue.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Jul 26, 2012
 * @version $Revision: 1105 $
 */
public class CriFilterPersistableDepositBagOp
    extends Operation {
  /**
   * 
   */
  private static final long serialVersionUID = -7158622308537954269L;

  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    CriTillCountCmd cmd = (CriTillCountCmd) argCmd;
    IPersistable[] persistables = argCmd.getPersistables().getObjects();
    List<ICriDepositBag> bags = new ArrayList<ICriDepositBag>();
    if (persistables != null) {
      for (IPersistable object : persistables) {
        if (object instanceof ICriDepositBag) {
          bags.add((ICriDepositBag) object);
        }
      }
    }

    cmd.setSelectedDepositItems(bags);
    return HELPER.completeResponse();
  }
}
