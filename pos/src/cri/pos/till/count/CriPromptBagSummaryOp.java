//$Id: CriPromptBagSummaryOp.java 1106 2017-01-11 20:43:32Z olr.jgaughn $
package cri.pos.till.count;

import static dtv.pos.framework.action.type.XstChainActionType.START;
import static dtv.pos.framework.action.type.XstDataActionKey.YES;
import static dtv.pos.framework.form.FormConstants.ABORT_CHANGES;
import static dtv.pos.iframework.op.OpStatus.ERROR_HALT;
import static dtv.util.CompositeObject.make;

import java.math.BigDecimal;
import java.util.Arrays;

import org.apache.log4j.Logger;

import cri.pos.till.CriTillHelper;

import dtv.hardware.events.IHardwareInputEvent;
import dtv.i18n.IFormattable;
import dtv.i18n.OutputContextType;
import dtv.pos.common.*;
import dtv.pos.framework.action.type.XstDataActionKey;
import dtv.pos.framework.op.OpState;
import dtv.pos.framework.op.Operation;
import dtv.pos.framework.ui.config.DataFieldConfig;
import dtv.pos.framework.ui.config.PromptConfig;
import dtv.pos.iframework.action.*;
import dtv.pos.iframework.event.*;
import dtv.pos.iframework.form.IEditModel;
import dtv.pos.iframework.form.IEditModelCmd;
import dtv.pos.iframework.op.*;
import dtv.pos.iframework.ui.config.IPromptConfig;
import dtv.pos.iframework.ui.config.IPromptKey;
import dtv.pos.iframework.validation.IValidationResult;
import dtv.pos.iframework.validation.IValidationResultList;
import dtv.ui.UIResourceManager;
import dtv.ui.swing.text.DefaultStyler;
import dtv.ui.swing.text.IStyler;
import dtv.util.*;
import dtv.util.config.ConfigUtils;
import dtv.util.config.IConfigObject;

/**
 * Prompt to display the deposit bag summary view.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Jul 16, 2012
 * @version $Revision: 1106 $
 */
public class CriPromptBagSummaryOp
    extends Operation
    implements IXstEventObserver {

  /**
   * 
   */
  private static final long serialVersionUID = 5668891775992757728L;
  protected final IOpState VERIFY_PROMPT = new OpState(this, "VERIFY_PROMPT");
  protected final IOpState POST_PROMPT = new OpState(this, "POST_PROMPT");
  protected final IOpState ADD_BAG_PROMPT = new OpState(this, "ADD_BAG_PROMPT");
  protected static final IXstDataActionKey BAG_LOST = XstDataActionKey.valueOf("BAG_LOST");
  protected static final IXstDataActionKey DONE = XstDataActionKey.valueOf("DONE");
  protected static final IXstDataActionKey ACCEPT = XstDataActionKey.valueOf("ACCEPT");
  protected final IOpState VALIDATION_ERROR = new OpState(this, "VALIDATION_ERROR");

  protected CriTillHelper TILL_HELPER = CriTillHelper.getInstance();

  private final IPromptKey defaultCancelPromptKey_ = PromptKey.valueOf("CRI.BAG_CANCEL_VERIFICATION");
  private IPromptKey cancelPromptKey_;
  private boolean promptAddDeposit_ = false;
  private static final Logger logger_ = Logger.getLogger(CriPromptBagSummaryOp.class);
  private static final String headerPrefix_ = UIResourceManager.getInstance().getString("_formatHeader");
  private static final String detailPrefix_ = UIResourceManager.getInstance().getString("_formatDetail");

  @Override
  public Class<?>[] getObservedEventInterfaces() {
    return new Class<?>[] {IHardwareInputEvent.class};
  }

  @Override
  public IXstEventType[] getObservedEvents() {
    return new IXstEventType[0];
  }

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    IOpResponse response = HELPER.silentErrorResponse();
    IOpState state = argCmd.getOpState();
    CriTillCountCmd cmd = (CriTillCountCmd) argCmd;

    if (state == null) {
      CriBagSummaryModel model = null;

      if (cmd.getBagSummaryModel() == null) {
        model = createBagSummaryModel(argCmd);
      }
      else {
        model = cmd.getBagSummaryModel();
      }

      argCmd.setOpState(POST_PROMPT);
      response = displaySummary(model);
    }
    else if (state == VERIFY_PROMPT) {
      IXstActionKey da = ((IXstAction) argEvent).getActionKey();

      if (da == YES) {
        // User has confirmed that the count is to be cancelled. Cancel and persist the transaction.
        //        IPosTransaction transaction = cmd.getTransaction();
        //        completeTransaction(transaction, CANCEL);
        //        persistTransaction(transaction);
        //        StationModelMgr.getInstance().getStationModel().setAttribute(STORE_OPEN_COUNT_CANCELLED, true);

        response = HELPER.getRunChainResponse(ERROR_HALT, getCancelBagCountChainKey(argCmd), START);
      }
      else {
        argCmd.setOpState(POST_PROMPT);
        response = displaySummary(cmd.getBagSummaryModel());
      }
    }

    else if (state == POST_PROMPT) {
      // Call the appropriate displayView...() method now...
      if (argEvent instanceof IXstDataAction) {
        IXstActionKey key = ((IXstDataAction) argEvent).getActionKey();
        response = handleBagCountDataAction(argCmd, key);
      }
      else {
        response = displaySummary(cmd.getBagSummaryModel());
      }
    }
    else if (state == ADD_BAG_PROMPT) {
      if (argEvent instanceof IXstDataAction) {
        IXstActionKey key = ((IXstDataAction) argEvent).getActionKey();
        response = handleAddBagDataAction((CriTillCountCmd) argCmd, key);
      }
      else if (argEvent instanceof IHardwareInputEvent) {
        if (((CriTillCountCmd) argCmd).getDepositBagModel() != null) {
          CriDepositBagEditModel model = cmd.getDepositBagModel();
          model.setValue(model.getFocusRequestFieldKey(),
              ((IHardwareInputEvent<?>) argEvent).getInputData().getData());
          argCmd.setOpState(ADD_BAG_PROMPT);
          response = showAddBagFormResponse(argCmd);
        }

      }
      else {
        response = displaySummary(cmd.getBagSummaryModel());
      }
    }
    else if (state == VALIDATION_ERROR) {
      argCmd.setOpState(ADD_BAG_PROMPT);
      response = showAddBagFormResponse(argCmd);
    }
    else {
      logger_.warn("Unexpected OpStatus: " + cmd.getOpState());
    }

    return response;
  }

  /**
   * Create the bag summmary edit model.
   * 
   * @param argCmd
   * @return
   */
  protected CriBagSummaryModel createBagSummaryModel(IXstCommand argCmd) {
    CriTillCountCmd cmd = (CriTillCountCmd) argCmd;
    CriBagSummaryModel model = new CriBagSummaryModel(cmd.getCurrentSession());
    cmd.setBagSummaryModel(model);
    return model;
  }

  /**
   * Returns a show form request to display the bag count summary screen.
   * 
   * @param argModel the bag count model
   * @return a show form request to display the bag count summary screen
   */
  protected IOpResponse displaySummary(CriBagSummaryModel argModel) {
    return HELPER.getShowFormResponse(FormKey.valueOf("CRI.BAG_SUMMARY"), argModel, getDataActionGroupKey());
  }

  /**
   * Get the action group key on the form.
   * 
   * @return
   */
  protected DataActionGroupKey getDataActionGroupKey() {
    return DataActionGroupKey.DEFAULT;
  }

  /**
   * Handle to process the action on the summary view form.
   * 
   * @param argCmd
   * @param argActionKey
   * @return
   */
  protected IOpResponse handleBagCountDataAction(IXstCommand argCmd, IXstActionKey argActionKey) {
    CriTillCountCmd cmd = (CriTillCountCmd) argCmd;
    IOpResponse response = HELPER.silentErrorResponse();

    if (argActionKey == ABORT_CHANGES) {
      // The are you sure scenario, then get out for good!
      argCmd.setOpState(VERIFY_PROMPT);
      //      ITenderControlTransaction trans = cmd.getTenderControlTransaction();
      //      TenderControlTransTypeCode typeCode = TenderControlTransTypeCode.forName(trans.getTypeCode());
      //      IFormattable typeArg = FormattableFactory.getInstance().getLiteral(typeCode.getDescription());
      response = HELPER.getPromptResponse(getCancelCountPromptKey(argCmd));
    }
    else if (argActionKey == BAG_LOST) {
      CriBagSummaryItem[] objects = cmd.getBagSummaryModel().getCurrentObjects();
      cmd.setSelectedBagItems(Arrays.asList(objects));
      if (objects != null) {
        response = HELPER.getWaitStackChainResponse(OpChainKey.valueOf("CRI.BAG_LOST"), cmd);
      }
      else {
        response = HELPER.incompleteResponse();
      }
    }
    else if (argActionKey == DONE) {
      argCmd.setOpState(ADD_BAG_PROMPT);
      if (getPromptToAddDeposit(argCmd)) {
        response = showAddBagFormResponse(argCmd);
      }
      else {
        response = HELPER.completeResponse();
      }
    }
    else {
      logger_.warn("Unexpected action: " + argActionKey);
      response = HELPER.incompleteResponse();
    }

    return response;
  }

  /**
   * Returns the chain key to run when cancelling the till count.
   * 
   * @param argCmd the current command object
   * @return the chain key
   */
  protected IOpChainKey getCancelBagCountChainKey(IXstCommand argCmd) {
    return OpChainKey.valueOf("TILL_COUNTING_WAITFORCASHDRAWER");
  }

  /**
   * Get the cancel option prompt key.
   *
   * @param argCmd
   * @return
   */
  protected IPromptKey getCancelCountPromptKey(IXstCommand argCmd) {
    return (cancelPromptKey_ == null) ? defaultCancelPromptKey_ : cancelPromptKey_;
  }

  /**
   * Get the indicator to prompt to add new store deposit or not.
   *
   * @param argCmd
   * @return
   */
  protected boolean getPromptToAddDeposit(IXstCommand argCmd) {
    return promptAddDeposit_;
  }

  /** {@inheritDoc} */
  @Override
  public void setParameter(String argName, IConfigObject argValue) {
    if ("CancelPromptKey".equalsIgnoreCase(argName)) {
      cancelPromptKey_ = PromptKey.valueOf(argValue.toString());
    }
    else if ("AddDeposit".equalsIgnoreCase(argName)) {
      promptAddDeposit_ = ConfigUtils.asBool(argValue.toString());
    }
    else {
      super.setParameter(argName, argValue);
    }
  }

  /**
   * Display the deposit bag adding form.
   * 
   * @param argCmd
   * @return
   */
  protected IOpResponse showAddBagFormResponse(IXstCommand argCmd) {
    CriTillCountCmd cmd = (CriTillCountCmd) argCmd;
    BigDecimal cashDepositAmount =
        TILL_HELPER.getTotalDepositAmount(cmd.getCurrentSession(), cmd.getTillCountModel(), CriBagTypes.CASH);
    BigDecimal checkDepositAmount = TILL_HELPER.getTotalDepositAmount(cmd.getCurrentSession(),
        cmd.getTillCountModel(), CriBagTypes.CHECKS);

    CriDepositBagEditModel model = getDepositBagModel(argCmd);

    if (NumberUtils.isPositive(cashDepositAmount) || NumberUtils.isPositive(checkDepositAmount)) {
      if (NumberUtils.isPositive(cashDepositAmount) && (model.getFocusRequestFieldKey() == null)) {
        model.setFocusRequestFieldKey(CriDepositBagEditModel.CASH_BAG_NUMBER);
      }
      else if (NumberUtils.isPositive(checkDepositAmount)) {
        model.setFocusRequestFieldKey(CriDepositBagEditModel.CHECK_BAG_NUMBER);
      }
      return HELPER.getShowFormResponse(FormKey.valueOf("CRI.DEPOSIT_BAG"), model);
    }
    else {
      return HELPER.completeResponse();
    }
  }

  /**
   * Get the deposit bag adding model.
   * 
   * @param argCmd
   * @return
   */
  protected CriDepositBagEditModel getDepositBagModel(IXstCommand argCmd) {
    CriTillCountCmd cmd = (CriTillCountCmd) argCmd;
    CriDepositBagEditModel model = null;
    if (cmd.getDepositBagModel() == null) {
      model = new CriDepositBagEditModel(cmd.getTillCountModel());
      cmd.setDepositBagModel(model);
    }
    else {
      model = cmd.getDepositBagModel();
    }
    return model;
  }

  /**
   * Handle the data action on the deposit bag adding form.
   * 
   * @param argCmd
   * @param argActionKey
   * @return
   */
  protected IOpResponse handleAddBagDataAction(CriTillCountCmd argCmd, IXstActionKey argActionKey) {
    IOpResponse response = HELPER.silentErrorResponse();
    if (argActionKey == ACCEPT) {
      response = HELPER.completeResponse();
      if (argCmd.getDepositBagModel() != null) {
        IOpResponse validationResponse = getFormValidityResponse(argCmd, argCmd.getDepositBagModel());
        if (validationResponse != null) {
          response = validationResponse;
        }
      }
    }
    else if (argActionKey == ABORT_CHANGES) {
      argCmd.setOpState(POST_PROMPT);
      response = displaySummary(argCmd.getBagSummaryModel());
    }
    else {
      logger_.warn("Unexpected action for adding deposit bag: " + argActionKey);
    }

    return response;
  }

  /**
   * Call the validation framework to check the entered fields.
   * 
   * @param argCmd
   * @param argModel
   * @return
   */
  protected IOpResponse getFormValidityResponse(IEditModelCmd argCmd, IEditModel argModel) {
    IValidationResultList results = validateForm(argModel);
    if (results.isValid()) {
      return null;
    }
    argCmd.setOpState(VALIDATION_ERROR);

    String problemDetails = "";
    for (IValidationResult result : results.getInvalidResults()) {
      String problemDetail = detailPrefix_ + result.getMessage().toString(OutputContextType.VIEW);
      problemDetails = StringUtils.appendLine(problemDetails, problemDetail);
    }

    IPromptConfig promptConfig = new PromptConfig();
    ((PromptConfig) promptConfig).setDataFieldConfig(new DataFieldConfig());

    IPromptKey promptKey = PromptKey.valueOf("FORM_VALIDATION_FAILED_ALT");
    promptConfig = HELPER.getPromptConfig(promptKey, promptConfig);
    String problemHeader =
        headerPrefix_ + promptConfig.getMsgSectionConfig().getText(null).toString(OutputContextType.VIEW);

    promptConfig.getDataFieldConfig().setStyler(createStyler(problemHeader, problemDetails));

    return HELPER.getPromptResponse(promptKey, (IFormattable) null, promptConfig);
  }

  @SuppressWarnings("unchecked")
  protected IStyler createStyler(String argProblemHeader, String argProblemDetails) {
    CompositeObject.TwoPiece<String, String> headerStyle = make(argProblemHeader, "header");
    CompositeObject.TwoPiece<String, String> detailStyle = make(argProblemDetails, "detail");

    return new DefaultStyler(headerStyle, detailStyle);
  }

  /**
   * Get the validation result from form model.
   * 
   * @param argModel
   * @return
   */
  protected IValidationResultList validateForm(IEditModel argModel) {
    return argModel.validate();
  }
}
