//$Id: CriShortageAmountPromptOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import java.math.BigDecimal;

import cri.pos.till.CriTillHelper;

import dtv.i18n.FormattableFactory;
import dtv.i18n.IFormattable;
import dtv.pos.framework.ui.op.AbstractPromptOp;
import dtv.pos.i18n.format.MoneyFormatter;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.util.NumberUtils;

/**
 * Prompt the acknowledge message when the count amount is less than the configured amount.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author shy xie
 * @created Jul 16, 2012
 * @version $Revision: 1105 $
 */
public class CriShortageAmountPromptOp
    extends AbstractPromptOp {

  /**
   * 
   */
  private static final long serialVersionUID = -942793287712551835L;
  protected static final CriTillHelper TILL_HELPER = CriTillHelper.getInstance();
  protected static final FormattableFactory FF = FormattableFactory.getInstance();

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    // prompt notice message when the previous count amount is less than the configured value
    return NumberUtils.isLessThan(TILL_HELPER.getLastCountAmount(),
        TILL_HELPER.getConfiguredStoreSafeAmount());
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handlePromptResponse(IXstCommand argCmd, IXstEvent argEvent) {
    return HELPER.completeResponse();
  }

  /** {@inheritDoc} */
  @Override
  protected IFormattable[] getPromptArgs(IXstCommand argCmd) {
    BigDecimal thresholdAmt =
        TILL_HELPER.getConfiguredStoreSafeAmount().subtract(TILL_HELPER.getLastCountAmount());
    MoneyFormatter formatter = new MoneyFormatter();
    return new IFormattable[] {FF.getSimpleFormattable(formatter.format(thresholdAmt))};
  }
}
