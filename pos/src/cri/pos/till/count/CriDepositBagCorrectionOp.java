//$Id: CriDepositBagCorrectionOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import cri.xst.dao.tsn.ICriDepositBag;

import dtv.pos.framework.op.Operation;
import dtv.pos.framework.security.SecurityMgr;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.storecalendar.StoreCalendar;
import dtv.xst.dao.crm.IParty;

/**
 * Revert the deposit bag to conveyance available.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Jul 31, 2012
 * @version $Revision: 1105 $
 */
public class CriDepositBagCorrectionOp
    extends Operation {

  /**
   * 
   */
  private static final long serialVersionUID = 2737752808814098655L;

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    ICriDepositBagCmd cmd = (ICriDepositBagCmd) argCmd;
    return !cmd.getSelectedDepositItems().isEmpty();
  }

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    ICriDepositBagCmd cmd = (ICriDepositBagCmd) argCmd;
    IParty currentEmployee = SecurityMgr.getCurrentUser().getOperatorParty();

    for (ICriDepositBag bag : cmd.getSelectedDepositItems()) {
      bag.setStatus(CriBagStatus.AVAILABLE.getCode());
      bag.setConveyanceDateTime(null);
      bag.setCreateDateTime(StoreCalendar.getBusinessDateTimeStamp());
      bag.setLastChangeEmployeeId(currentEmployee.getEmployeeId());
      bag.setLostReasonCode(null);
      bag.setLostComments(null);
      bag.setBusinessDate(StoreCalendar.getCurrentBusinessDate());
    }

    cmd.addPersistable(cmd.getSelectedDepositItems());
    return HELPER.completeResponse();
  }
}
