//$Id: CriUpdateBagStatusOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import dtv.pos.framework.op.Operation;
import dtv.pos.framework.security.SecurityMgr;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.util.config.IConfigObject;

/**
 * Update the deposit bag status on the bag summary list view.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author shy xie
 * @created Jul 16, 2012
 * @version $Revision: 1105 $
 */
public class CriUpdateBagStatusOp
    extends Operation {

  /**
   * 
   */
  private static final long serialVersionUID = 3212266174489212958L;
  private CriBagStatus status_;

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    CriTillCountCmd cmd = (CriTillCountCmd) argCmd;
    if (cmd.getSelectedBagItems() != null) {
      for (CriBagSummaryItem item : cmd.getSelectedBagItems()) {
        item.setStatus(getStatus(argCmd).getCode());
        item.setLastChangeEmployeeId(getEmployeeId(argCmd));
      }
    }
    return HELPER.completeResponse();
  }

  /**
   * Get the employee ID.
   *
   * @param argCmd
   * @return
   */
  protected String getEmployeeId(IXstCommand argCmd) {
    return SecurityMgr.getCurrentUser().getOperatorParty().getEmployeeId();
  }

  /**
   * Get the status code.
   * 
   * @param argCmd
   * @return
   */
  protected CriBagStatus getStatus(IXstCommand argCmd) {
    return status_;
  }

  /** {@inheritDoc} */
  @Override
  public void setParameter(String argName, IConfigObject argValue) {
    if ("Status".equalsIgnoreCase(argName)) {
      status_ = CriBagStatus.forName(argValue.toString());
    }
    else {
      super.setParameter(argName, argValue);
    }
  }
}
