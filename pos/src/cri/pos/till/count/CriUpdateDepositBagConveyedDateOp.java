//$Id: CriUpdateDepositBagConveyedDateOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import java.util.Date;

import dtv.pos.framework.op.Operation;
import dtv.pos.framework.security.SecurityMgr;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.storecalendar.StoreCalendar;

/**
 * Setup the deposit bag conveyed date.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Jul 24, 2012
 * @version $Revision: 1105 $
 */
public class CriUpdateDepositBagConveyedDateOp
    extends Operation {
  /**
   * 
   */
  private static final long serialVersionUID = 3831760600698949146L;

  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {

    CriTillCountCmd cmd = (CriTillCountCmd) argCmd;
    if (cmd.getSelectedBagItems() != null) {
      for (CriBagSummaryItem summaryItem : cmd.getSelectedBagItems()) {
        summaryItem.setConveyanceDate(getConveyanceDate(argCmd));
        summaryItem.setLastChangeEmployeeId(getEmployeeId(argCmd));
      }
    }
    return HELPER.completeResponse();
  }

  /**
   * Get the employee ID.
   *
   * @param argCmd
   * @return
   */
  protected String getEmployeeId(IXstCommand argCmd) {
    return SecurityMgr.getCurrentUser().getOperatorParty().getEmployeeId();
  }

  /**
   * Get the deposit bag conveyed date.
   * @param argCmd
   * @return
   */
  protected Date getConveyanceDate(IXstCommand argCmd) {
    return StoreCalendar.getBusinessDateTimeStamp();
  }
}
