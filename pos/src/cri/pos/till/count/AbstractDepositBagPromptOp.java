//$Id: AbstractDepositBagPromptOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import static dtv.pos.framework.form.FormConstants.SELECT;

import java.util.ArrayList;
import java.util.List;

import cri.pos.till.CriTillHelper;
import cri.xst.dao.tsn.ICriDepositBag;

import dtv.i18n.IFormattable;
import dtv.pos.framework.ui.op.AbstractListPromptOp;
import dtv.pos.iframework.action.IXstDataAction;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.*;
import dtv.pos.iframework.ui.config.IPromptKey;

/**
 * Deposit bag list prompt.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Jul 31, 2012
 * @version $Revision: 1105 $
 */
public abstract class AbstractDepositBagPromptOp
    extends AbstractListPromptOp {
  /**
   * 
   */
  private static final long serialVersionUID = 2607270108369323190L;
  protected static final CriTillHelper TILL_HELPER = CriTillHelper.getInstance();

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handlePromptResponse(IXstCommand argCmd, IXstEvent argEvent) {
    CriDepositBagCmd cmd = (CriDepositBagCmd) argCmd;
    cmd.setSelectedDepositItems(getSelections(argCmd, argEvent));
    return HELPER.completeResponse();
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleDataAction(IXstCommand argCmd, IXstDataAction argEvent) {
    return (argEvent.getActionKey().equals(SELECT)) //
        ? selectAll(argCmd, argEvent) //
        : super.handleDataAction(argCmd, argEvent);
  }

  /**
   * Designates all items in the list as selected for gift receipt printing.
   *
   * @param argCmd the current operational command
   * @param argEvent the most recent operational event
   * @return a response which will select all items in the current list without closing the prompt
   */
  protected IOpResponse selectAll(IXstCommand argCmd, IXstEvent argEvent) {
    Integer[] selectedIndices = new Integer[getPromptList(argCmd, argEvent).length];
    for (int i = 0; i < selectedIndices.length; i++ ) {
      selectedIndices[i] = i;
    }
    return getRePromptResponse(argCmd, argEvent, selectedIndices);
  }

  /**
   * Get the selections data.
   *
   * @param argCmd
   * @param argEvent
   * @return
   */
  protected List<ICriDepositBag> getSelections(IXstCommand argCmd, IXstEvent argEvent) {
    List<ICriDepositBag> selections = new ArrayList<ICriDepositBag>();
    if (argEvent.getDataSet() != null) {
      for (Object obj : argEvent.getDataSet()) {
        if (obj instanceof ICriDepositBag) {
          selections.add((ICriDepositBag) obj);
        }
      }
    }

    return selections;
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse getEmptyListPromptResponse(IXstCommand argCmd) {
    IPromptKey promptKey = getEmptyListPromptKey(argCmd);
    if (promptKey == null) {
      return HELPER.completeResponse();
    }
    else {
      argCmd.setOpState(SHOWING_ERROR);
      return HELPER.getPromptResponse(OpStatus.INCOMPLETE_HALT, promptKey,
          new IFormattable[] {ff_.getSimpleFormattable(getBagStatus(argCmd).getDescription())});
    }
  }

  /**
   * Get the deposit bag status.
   *
   * @param argCmd
   * @return
   */
  protected abstract CriBagStatus getBagStatus(IXstCommand argCmd);
}
