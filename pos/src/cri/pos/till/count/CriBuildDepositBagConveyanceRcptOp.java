//$Id: CriBuildDepositBagConveyanceRcptOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import dtv.hardware.rcptbuilding.IRcpt;
import dtv.hardware.rcptbuilding.RcptBuilder;
import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;

/**
 * Build the deposit bag conveyance receipt.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Jul 29, 2012
 * @version $Revision: 1105 $
 */
public class CriBuildDepositBagConveyanceRcptOp
    extends Operation {

  /**
   * 
   */
  private static final long serialVersionUID = -2341578639572064737L;

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    ICriDepositBagCmd cmd = (ICriDepositBagCmd) argCmd;
    return ((cmd.getTransaction() != null) && !cmd.getSelectedDepositItems().isEmpty());
  }

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    ICriDepositBagCmd cmd = (ICriDepositBagCmd) argCmd;
    CriDepositBagRcptBean bean =
        new CriDepositBagRcptBean(cmd.getTransaction(), cmd.getSelectedDepositItems());
    IRcpt rcpt = RcptBuilder.getInstance().getRcpt(bean, "CRI.DEPOSIT_BAG_CONVEYANCE");
    cmd.addReceipt(rcpt);

    return HELPER.completeResponse();
  }
}
