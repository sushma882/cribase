//$Id: CriBagSummaryItem.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import java.math.BigDecimal;
import java.util.Date;

import cri.xst.dao.tsn.ICriDepositBag;

import dtv.mvc.IModel;
import dtv.pos.storecalendar.StoreCalendar;

/**
 * The bag row items in the bag summary list view.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author shy xie
 * @created Jul 15, 2012
 * @version $Revision: 1105 $
 */
public class CriBagSummaryItem
    implements IModel, Comparable<CriBagSummaryItem> {

  private String bagNumber_;
  private Date conveyanceDate_;
  private Date createdDateTime_;
  private BigDecimal amount_ = BigDecimal.ZERO;
  private String status_;
  private String type_;
  private String typeDescription_;
  private String lostReasonCode_;
  private String lostReasonDescription_;
  private String lastChangeEmplohyeeId_;
  private String createdEmployeeId_;
  private ICriDepositBag bag_;

  /**
   * Constructs a <code>CriBagSummaryItem</code>.
   * @param argBag
   */
  public CriBagSummaryItem(ICriDepositBag argBag) {
    bag_ = argBag;
    bagNumber_ = argBag.getNumber();
    createdDateTime_ = argBag.getCreateDateTime();
    amount_ = argBag.getAmount();
    status_ = argBag.getStatus();
    lostReasonCode_ = argBag.getLostReasonCode();
    conveyanceDate_ = argBag.getConveyanceDateTime();
    createdEmployeeId_ = argBag.getEmployeeId();
    lastChangeEmplohyeeId_ = argBag.getLastChangeEmployeeId();
    if (argBag.getLostReasonCodeObject() != null) {
      lostReasonDescription_ = argBag.getLostReasonCodeObject().getDescription();
    }
    type_ = argBag.getType();

  }

  /**
   * Get the bag number.
   * @return
   */
  public String getBagNumber() {
    return bagNumber_;
  }

  /**
   * Set the bag number.
   * @param argBagNumber
   */
  public void setBagNumber(String argBagNumber) {
    bagNumber_ = argBagNumber;
  }

  /**
   * Get the bag created datetime.
   * @return
   */
  public Date getCreateDateTime() {
    return createdDateTime_;
  }

  /**
   * Set the bag created datetime.
   * @param argCreatedDateTime
   */
  public void setCreateDateTime(Date argCreatedDateTime) {
    createdDateTime_ = argCreatedDateTime;
  }

  /**
   * Get the total amount in the bag.
   * @return
   */
  public BigDecimal getAmount() {
    return amount_;
  }

  /**
   * Set the total amount.
   * @param argAmount
   */
  public void setAmount(BigDecimal argAmount) {
    amount_ = argAmount;
  }

  /**
   * Get the bag status.
   * @return
   */
  public String getStatus() {
    return status_;
  }

  /**
   * Set the bag status.
   * @param argStatus
   */
  public void setStatus(String argStatus) {
    bag_.setStatus(argStatus);
    status_ = argStatus;
  }

  /**
   * Get the bag type.
   * @return
   */
  public String getType() {
    return type_;
  }

  /**
   * Set the bag type.
   * @param argType
   */
  public void setType(String argType) {
    type_ = argType;
  }

  /**
   * Get the description of type.
   * @return
   */
  public String getTypeDescription() {
    return typeDescription_;
  }

  /**
   * Set the description of type.
   * @param argTypeDescription
   */
  public void setTypeDescription(String argTypeDescription) {
    typeDescription_ = argTypeDescription;
  }

  /**
   * Get the bag lost reason code.
   * @return
   */
  public String getLostReasonCode() {
    return lostReasonCode_;
  }

  /**
   * Set the bag lost reason code.
   * @param argLostReasonCode
   */
  public void setLostReasonCode(String argLostReasonCode) {
    bag_.setLostReasonCode(argLostReasonCode);
    lostReasonCode_ = argLostReasonCode;
  }

  /**
   * Get the bag lost reason description.
   * @return
   */
  public String getLostReasonDescription() {
    return lostReasonDescription_;
  }

  /**
   * Set the bag lost reason description.
   * @param argLostReasonDescription
   */
  public void setLostReasonDescription(String argLostReasonDescription) {
    lostReasonDescription_ = argLostReasonDescription;
  }

  /**
   * Get the DAO object related to the view model..
   * @return
   */
  public ICriDepositBag getCountBag() {
    return bag_;
  }

  /**
   * Set the DAO object which related to the view model
   * @param argBag
   */
  public void setCountBag(ICriDepositBag argBag) {
    bag_ = argBag;
  }

  /**
   * Get the employee ID for the last bag action.
   * @return
   */
  public String getLastChangeEmployeeId() {
    return lastChangeEmplohyeeId_;
  }

  /**
   * Set the employee ID for the last bag action.
   * @param argEmoloyeeId
   */
  public void setLastChangeEmployeeId(String argEmoloyeeId) {
    bag_.setLastChangeEmployeeId(argEmoloyeeId);
    bag_.setBusinessDate(StoreCalendar.getCurrentBusinessDate());
    lastChangeEmplohyeeId_ = argEmoloyeeId;
  }

  /**
   * Get the employee ID for the bag creation.
   * @return
   */
  public String getCreatedEmployeeId() {
    return createdEmployeeId_;
  }

  /**
   * Set the employee ID for the bag creation.
   * @param argEmoloyeeId
   */
  public void setCreatedEmployeeId(String argEmoloyeeId) {
    bag_.setEmployeeId(argEmoloyeeId);
    createdEmployeeId_ = argEmoloyeeId;
  }

  /**
   * Get the bag created date.
   * @return
   */
  public Date getConveyanceDate() {
    return conveyanceDate_;
  }

  /**
   * Set the bag created date.
   * @param argCreatedDate
   */
  public void setConveyanceDate(Date argDate) {
    bag_.setConveyanceDateTime(argDate);
    conveyanceDate_ = argDate;
  }

  @Override
  public int compareTo(CriBagSummaryItem obj) {
    if (getCreateDateTime().before(obj.getCreateDateTime())) {
      return -1;
    }
    else if (getCreateDateTime().after(obj.getCreateDateTime())) {
      return 1;
    }
    else {
      return 0;
    }
  }
}
