//$Id: CriDepositBagConveyancePromptOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import static cri.pos.till.count.CriBagStatus.AVAILABLE;

import dtv.pos.common.PromptKey;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.iframework.ui.config.IPromptKey;

/**
 * List the all the deposit bags which is available for conveyed.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Jul 29, 2012
 * @version $Revision: 1105 $
 */
public class CriDepositBagConveyancePromptOp
    extends AbstractDepositBagPromptOp {

  /**
   * 
   */
  private static final long serialVersionUID = 5926544145492713874L;

  /** {@inheritDoc} */
  @Override
  protected Object[] getPromptList(IXstCommand argCmd, IXstEvent argEvent) {
    return TILL_HELPER.getDepositBags(getBagStatus(argCmd).getCode()).toArray();
  }

  /** {@inheritDoc} */
  @Override
  protected IPromptKey getEmptyListPromptKey(IXstCommand argCmd) {
    return PromptKey.valueOf("CRI.NO_CONVEYANCE_DEPOSIT_BAGS");
  }

  /** {@inheritDoc} */
  @Override
  protected CriBagStatus getBagStatus(IXstCommand argCmd) {
    return AVAILABLE;
  }
}
