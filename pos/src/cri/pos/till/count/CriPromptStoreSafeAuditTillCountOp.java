//$Id: CriPromptStoreSafeAuditTillCountOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import java.util.List;

import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.TenderHelper;
import dtv.pos.till.TillHelper;
import dtv.pos.till.count.*;
import dtv.xst.dao.tnd.ITender;
import dtv.xst.dao.tsn.ISession;

/**
 * Prompt the store sale audit count.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Sep 26, 2012
 * @version $Revision: 1105 $
 */
public class CriPromptStoreSafeAuditTillCountOp
    extends PromptAuditTillCountOp {

  /**
   * 
   */
  private static final long serialVersionUID = -64760351202940825L;

  /** {@inheritDoc} */
  @Override
  protected TillCountModel createTillCountModel(IXstCommand argCmd) {
    TillCountModel model = super.createTillCountModel(argCmd);
    List<ISession> tillsToEndCount = TillHelper.getInstance().getTillsToEndCount();

    for (CountSummaryItem countItem : model.getDepositableCountSummaryItems()) {
      if (TenderHelper.getInstance().isLocalCurrency((ITender) countItem.getCountSummaryObject())) {
        countItem.updateSystemValues(true, tillsToEndCount);
      }
    }

    return model;
  }
}
