//$Id: CriDepositBagCmd.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import java.util.ArrayList;
import java.util.List;

import cri.xst.dao.tsn.ICriDepositBag;

import dtv.pos.framework.op.XstCommand;

/**
 * Store deposit bag count command.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Jul 29, 2012
 * @version $Revision: 1105 $
 */
public class CriDepositBagCmd
    extends XstCommand
    implements ICriDepositBagCmd {

  /**
   * 
   */
  private static final long serialVersionUID = 191380572271118565L;
  private CriBagSummaryModel bagSummaryModel_;
  private List<ICriDepositBag> selectedDepositBag_ = new ArrayList<ICriDepositBag>();

  public CriDepositBagCmd() {
    super("DEPOSIT_BAG");
  }

  @Override
  public CriBagSummaryModel getBagSummaryModel() {
    return bagSummaryModel_;
  }

  @Override
  public void setBagSummaryModel(CriBagSummaryModel argBagSummaryModel) {
    bagSummaryModel_ = argBagSummaryModel;
  }

  @Override
  public List<ICriDepositBag> getSelectedDepositItems() {
    return selectedDepositBag_;
  }

  @Override
  public void setSelectedDepositItems(List<ICriDepositBag> argSelectedDepositBag) {
    selectedDepositBag_ = argSelectedDepositBag;
  }

  @Override
  public List<CriBagSummaryItem> getSelectedBagItems() {
    return null;
  }

  @Override
  public void setSelectedBagItems(List<CriBagSummaryItem> argSelectedBagItems) {

  }

  @Override
  public CriDepositBagEditModel getDepositBagModel() {
    return null;
  }

  @Override
  public void setDepositBagModel(CriDepositBagEditModel argDepositBagModel) {

  }
}
