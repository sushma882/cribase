//$Id: CriPromptStoreSafeCountOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import dtv.pos.common.ConfigurationMgr;
import dtv.pos.common.PromptKey;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.iframework.ui.config.IPromptKey;
import dtv.pos.till.count.*;
import dtv.pos.till.types.TenderControlTransTypeCode;

/**
 * Prompt the store safe count form.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Jul 16, 2012
 * @version $Revision: 1105 $
 */
public class CriPromptStoreSafeCountOp
    extends AbstractPromptCashCountOp {

  /**
   * 
   */
  private static final long serialVersionUID = -8675197259390963638L;

  /** {@inheritDoc} */
  @Override
  public CountViewSummaryType getCountViewSummaryType() {
    return CountViewSummaryType.forName(ConfigurationMgr.getCountSummaryViewTypeStartCount());
  }

  /** {@inheritDoc} */
  @Override
  protected boolean displayBalanceStatusMessage() {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  protected String getCashCountMethod() {
    return TillCountConstants.getStoreBankOpeningCountMethod();
  }

  @Override
  protected IPromptKey getPromptKey(IXstCommand argCmd) {
    return PromptKey.valueOf("CRI.STORE_SAFE_COUNTING_CASH");
  }

  /** {@inheritDoc} */
  @Override
  protected TenderControlTransTypeCode getTenderCountTransactionType() {
    return TenderControlTransTypeCode.BEGINCOUNT;
  }

  /** {@inheritDoc} */
  @Override
  protected IPromptKey getTillOutOfBalancePromptKey() {
    return PromptKey.valueOf("CRI.STORE_SAFE_AMOUNT_DIFF_FROM_ENDCOUNT");
  }

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handlePostPrompt(IXstCommand argCmd, IXstEvent argEvent) {
    ITillCountCmd cmd = (ITillCountCmd) argCmd;
    return super.handleCompleteCount(argCmd, cmd.getTillCountModel());
  }
}
