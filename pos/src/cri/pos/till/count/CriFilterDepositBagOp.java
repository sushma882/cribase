//$Id: CriFilterDepositBagOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import java.util.ArrayList;
import java.util.List;

import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.util.config.IConfigObject;

/**
 * Get the count deposit bag by bag status.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Jul 24, 2012
 * @version $Revision: 1105 $
 */
public class CriFilterDepositBagOp
    extends Operation {

  /**
   * 
   */
  private static final long serialVersionUID = 2477325675717534020L;
  private CriBagStatus status_;

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {

    CriTillCountCmd cmd = (CriTillCountCmd) argCmd;
    cmd.setSelectedBagItems(filterImpl(argCmd));
    return HELPER.completeResponse();
  }

  /**
   * Filter the count bags.
   * @param argCmd
   * @return
   */
  protected List<CriBagSummaryItem> filterImpl(IXstCommand argCmd) {
    List<CriBagSummaryItem> result = new ArrayList<CriBagSummaryItem>();
    CriTillCountCmd cmd = (CriTillCountCmd) argCmd;
    if (cmd.getSelectedBagItems() != null) {
      for (CriBagSummaryItem item : cmd.getSelectedBagItems()) {
        if (status_.matches(item.getStatus())) {
          result.add(item);
        }
      }
    }

    return result;

  }

  /**
   * Get the status code.
   * 
   * @param argCmd
   * @return
   */
  protected CriBagStatus getStatus(IXstCommand argCmd) {
    return status_;
  }

  /** {@inheritDoc} */
  @Override
  public void setParameter(String argName, IConfigObject argValue) {
    if ("Status".equalsIgnoreCase(argName)) {
      status_ = CriBagStatus.forName(argValue.toString());
    }
    else {
      super.setParameter(argName, argValue);
    }
  }
}
