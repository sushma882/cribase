//$Id: CriProcessBagCountOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import java.math.BigDecimal;
import java.util.List;

import cri.pos.common.CriConfigurationMgr;
import cri.pos.till.CriTillHelper;
import cri.xst.dao.tsn.ICriDepositBag;

import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.TenderHelper;
import dtv.util.NumberUtils;
import dtv.xst.dao.tsn.ISessionTender;

/**
 * Process the deposit bag summary model.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Jul 16, 2012
 * @version $Revision: 1105 $
 */
public class CriProcessBagCountOp
    extends Operation {

  /**
   * 
   */
  private static final long serialVersionUID = 7039337779117934328L;
  protected CriTillHelper TILL_HELPER = CriTillHelper.getInstance();

  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    CriTillCountCmd cmd = (CriTillCountCmd) argCmd;

    BigDecimal totalCashDepositAmount =
        TILL_HELPER.getTotalDepositAmount(cmd.getCurrentSession(), cmd.getTillCountModel(), CriBagTypes.CASH);
    BigDecimal totalCheckDepositAmount = TILL_HELPER.getTotalDepositAmount(cmd.getCurrentSession(),
        cmd.getTillCountModel(), CriBagTypes.CHECKS);
    if ((cmd.getDepositBagModel() != null) && (NumberUtils.isPositive(totalCashDepositAmount)
        || NumberUtils.isPositive(totalCheckDepositAmount))) {
      placeDeposit(argCmd);
    }

    return HELPER.completeResponse();
  }

  protected void placeDeposit(IXstCommand argCmd) {
    CriTillCountCmd cmd = (CriTillCountCmd) argCmd;
    CriDepositBagEditModel model = cmd.getDepositBagModel();

    // Create the deposit bag
    List<ICriDepositBag> depositBags = TILL_HELPER.createDepositBags(model);
    cmd.addPersistable(depositBags);

    // Take of the deposit amount from the store safe cash tender
    ISessionTender storeSafeCash = TILL_HELPER.getSessionTender(cmd.getCurrentSession(),
        TenderHelper.getInstance().getLocalCurrencyTenderId(), getSourceDescription());
    storeSafeCash.setMediaAmount(storeSafeCash.getMediaAmount().subtract(model.getTotalCashAmount()));
    cmd.addPersistable(storeSafeCash);

    // The checks should always be deposited all
    for (String checkTenderId : CriConfigurationMgr.getCheckTenderIds()) {
      ISessionTender storeSafeCheck =
          TILL_HELPER.getSessionTender(cmd.getCurrentSession(), checkTenderId, getSourceDescription());
      storeSafeCheck.setMediaAmount(BigDecimal.ZERO);
      cmd.addPersistable(storeSafeCheck);
    }
  }

}
