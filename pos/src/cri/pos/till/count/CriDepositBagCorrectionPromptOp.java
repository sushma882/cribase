//$Id: CriDepositBagCorrectionPromptOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import static cri.pos.till.count.CriBagStatus.CONVEYED;

import java.util.Date;

import dtv.pos.common.PromptKey;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.iframework.ui.config.IPromptKey;
import dtv.pos.storecalendar.StoreCalendar;
import dtv.util.CalendarField;
import dtv.util.DateUtils;

/**
 * Prompt the deposit list which can be corrected.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Jul 31, 2012
 * @version $Revision: 1105 $
 */
public class CriDepositBagCorrectionPromptOp
    extends AbstractDepositBagPromptOp {

  /**
   * 
   */
  private static final long serialVersionUID = 3600071960837463931L;

  /** {@inheritDoc} */
  @Override
  protected Object[] getPromptList(IXstCommand argCmd, IXstEvent argEvent) {
    Date dateFrom = StoreCalendar.getCurrentBusinessDate();
    Date dateTo = DateUtils.dateAdd(CalendarField.DAY, 1, dateFrom);

    return TILL_HELPER.getConveyedDepositBags(dateFrom, dateTo).toArray();
  }

  /** {@inheritDoc} */
  @Override
  protected IPromptKey getEmptyListPromptKey(IXstCommand argCmd) {
    return PromptKey.valueOf("CRI.NO_CORRECTION_DEPOSIT_BAGS");
  }

  /** {@inheritDoc} */
  @Override
  protected CriBagStatus getBagStatus(IXstCommand argCmd) {
    return CONVEYED;
  }
}
