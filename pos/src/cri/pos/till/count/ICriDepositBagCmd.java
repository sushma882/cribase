//$Id: ICriDepositBagCmd.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import java.util.List;

import cri.xst.dao.tsn.ICriDepositBag;

import dtv.pos.hardware.op.IPrintReceiptsCmd;
import dtv.pos.iframework.cmd.ITransactionCmd;

public interface ICriDepositBagCmd
    extends IPrintReceiptsCmd, ITransactionCmd {

  public CriBagSummaryModel getBagSummaryModel();

  public void setBagSummaryModel(CriBagSummaryModel argBagSummaryModel);

  public List<CriBagSummaryItem> getSelectedBagItems();

  public void setSelectedBagItems(List<CriBagSummaryItem> argSelectedBagItems);

  public List<ICriDepositBag> getSelectedDepositItems();

  public void setSelectedDepositItems(List<ICriDepositBag> argSelectedDepositBag);

  public CriDepositBagEditModel getDepositBagModel();

  public void setDepositBagModel(CriDepositBagEditModel argDepositBagModel);
}
