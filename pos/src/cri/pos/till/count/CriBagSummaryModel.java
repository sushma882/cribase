//$Id: CriBagSummaryModel.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import java.util.*;

import javax.swing.ListSelectionModel;

import cri.pos.common.CriConfigurationMgr;
import cri.pos.till.CriTillHelper;
import cri.xst.dao.tsn.ICriDepositBag;

import dtv.pos.framework.form.BasicListEditModel;
import dtv.pos.framework.security.SecurityMgr;
import dtv.pos.iframework.type.ModelKey;
import dtv.pos.storecalendar.StoreCalendar;
import dtv.util.DateUtils;
import dtv.util.StringUtils;
import dtv.xst.dao.tsn.ISession;
import dtv.xst.dao.tsn.ITenderRepository;

/**
 * A view model for the deposit bags.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author shy xie
 * @created Jul 15, 2012
 * @version $Revision: 1105 $
 */
public class CriBagSummaryModel
    extends BasicListEditModel {

  protected static final CriTillHelper TILL_HELPER = CriTillHelper.getInstance();

  private static final String CASHIER_FIELD = "cashierId";
  private static final String TILL_NAME_FIELD = "tillName";
  private String cashierId_;
  private String tillName_;
  private final ISession session_;

  private List<CriBagSummaryItem> bagSummaryList_;
  private int currentIndex_ = 0;

  /**
   * Get the specified summary item.
   * @param argIndex the index of the item.
   * @return the requested item.
   */
  public CriBagSummaryItem getBagSummaryItem(int argIndex) {
    return bagSummaryList_.get(argIndex);
  }

  /**
   * Return the complete list of summary objects created in this model.
   * @return the complete list of summary objects created in this model.
   */
  public List<CriBagSummaryItem> getBagSummaryList() {
    return bagSummaryList_;
  }

  public List<CriBagSummaryItem> getDueToPickupBagSummaryList() {
    List<CriBagSummaryItem> result = new ArrayList<CriBagSummaryItem>();
    int days = CriConfigurationMgr.getStoreSafeDepositPickupThresholdDays();
    Date currentDate = StoreCalendar.getCurrentBusinessDate();
    Calendar cal = Calendar.getInstance();
    cal.setTime(currentDate);
    cal.add(Calendar.DATE, days * -1);

    for (CriBagSummaryItem item : bagSummaryList_) {
      if (CriBagStatus.AVAILABLE.matches(item.getStatus())) {
        Date createDate = DateUtils.clearTime(item.getCreateDateTime());
        if (!createDate.after(cal.getTime())) {
          result.add(item);
        }
      }
    }
    Collections.sort(result);
    return result;
  }

  /**
   * Get the current index.
   * @return the index of the current item.
   */
  public int getCurrentIndex() {
    return currentIndex_;
  }

  /**
   * Return the current cashier id
   * @return the current cashier id
   */
  public String getCashierId() {
    return cashierId_;
  }

  /**
   * Set the current cashier id.
   * @param argCashierId the current cashier id.
   */
  public void setCashierId(String argCashierId) {
    cashierId_ = argCashierId;
  }

  /**
   * Set the current till name.
   * @param argTillName the current till name.
   */
  public void setTillName(String argTillName) {
    tillName_ = argTillName;
  }

  /**
   * Return the current till name
   * @return the current till name
   */
  public String getTillName() {
    return tillName_;
  }

  /**
   * Return the current till session data object
   * @return the current till session data object
   */
  public ISession getCurrentTillSession() {
    return session_;
  }

  public CriBagSummaryModel(ISession argSession) {
    super(ModelKey.valueOf("BAG_SUMMARY"));
    session_ = argSession;
    init();
  }

  /**
   * Return the current selected summary item object
   * @return the current selected summary item object
   */
  public CriBagSummaryItem[] getCurrentObjects() {
    List<Object> selected = getSelectedElements();
    return selected.toArray(new CriBagSummaryItem[] {});
  }

  protected void init() {
    initialValue();
    addFields();
    getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    Collections.sort(bagSummaryList_);
    getModel().setElements(bagSummaryList_);
  }

  protected void initialValue() {
    bagSummaryList_ = new ArrayList<CriBagSummaryItem>();

    if (getCurrentTillSession().getParty() != null) {
      cashierId_ = getCurrentTillSession().getParty().getEmployeeId();
    }

    if (StringUtils.isEmpty(cashierId_)) {
      cashierId_ = SecurityMgr.getCurrentUser().getOperatorParty().getEmployeeId();
    }

    ITenderRepository tndrRepository = getCurrentTillSession().getTenderRepository();
    if (tndrRepository != null) {
      tillName_ = tndrRepository.getName();
    }

    //Get all the bags belongs to the current retail store.
    List<ICriDepositBag> depositBags = TILL_HELPER.getDepositBags(null);
    for (ICriDepositBag bag : depositBags) {
      CriBagSummaryItem item = new CriBagSummaryItem(bag);
      if (CriBagStatus.AVAILABLE.matches(bag.getStatus())) {
        bagSummaryList_.add(item);
      }
    }
  }

  protected void addFields() {
    addField(CASHIER_FIELD, String.class);
    addField(TILL_NAME_FIELD, String.class);
    initializeFieldState();
  }

  /**
   * This method advances the count summary item index by one and resets the list selection on the
   * list view.
   */
  public void nextCount() {
    if (currentIndex_ < (bagSummaryList_.size() - 1)) {
      currentIndex_++ ;
      getSelectionModel().setSelectionInterval(currentIndex_, currentIndex_);
    }
  }

}
