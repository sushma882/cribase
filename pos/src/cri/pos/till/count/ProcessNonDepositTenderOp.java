//$Id: ProcessNonDepositTenderOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import java.math.BigDecimal;

import dtv.data2.access.DataFactory;
import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.till.ITillHelper;
import dtv.pos.till.TillHelper;
import dtv.pos.till.count.ITillCountCmd;
import dtv.xst.dao.tsn.ISession;
import dtv.xst.dao.tsn.ISessionTender;

/**
 * Reset the tender amount and count amount.<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 *
 * @author Shy Xie
 * @created Jun 10, 2014
 * @version $Revision: 1105 $
 */
public class ProcessNonDepositTenderOp
    extends Operation {
  /**
   * 
   */
  private static final long serialVersionUID = 6943079518070185889L;
  protected static final ITillHelper TILL_HELPER = TillHelper.getInstance();

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    ITillCountCmd cmd = (ITillCountCmd) argCmd;
    ISession storeBankSession = cmd.getCurrentSession();

    for (String tenderId : getNonDepositTenderIds()) {
      ISessionTender sessionTender = TILL_HELPER.getSessionTender(storeBankSession, tenderId, null);
      if (sessionTender != null) {
        sessionTender.setMediaAmount(BigDecimal.ZERO);
        sessionTender.setMediaCount(0);
        DataFactory.makePersistent(sessionTender);
      }
    }

    return HELPER.completeResponse();
  }

  /**
   * Get tender ID list which are not be deposited.
   * 
   * @return
   */
  protected String[] getNonDepositTenderIds() {
    return new String[] {"CLOTHING_VOUCHER"};
  }
}
