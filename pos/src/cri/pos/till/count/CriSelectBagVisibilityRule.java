//$Id: CriSelectBagVisibilityRule.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import static dtv.pos.iframework.visibilityrules.AccessLevel.DENIED;
import static dtv.pos.iframework.visibilityrules.AccessLevel.GRANTED;

import java.util.ArrayList;
import java.util.List;

import dtv.pos.framework.visibilityrules.AbstractVisibilityRule;
import dtv.pos.iframework.form.IEditModel;
import dtv.pos.iframework.visibilityrules.IAccessLevel;
import dtv.util.config.IConfigObject;

/**
 * Deposit bag selection rule.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Jul 16, 2012
 * @version $Revision: 1105 $
 */
public class CriSelectBagVisibilityRule
    extends AbstractVisibilityRule {

  private final List<CriBagStatus> bagStatus_ = new ArrayList<CriBagStatus>();

  /** {@inheritDoc} */
  @Override
  protected IAccessLevel checkVisibilityImpl()
      throws Exception {
    IAccessLevel result = DENIED;
    IEditModel model = getPrimaryFormEditModel();
    if (model instanceof CriBagSummaryModel) {
      CriBagSummaryModel countModel = (CriBagSummaryModel) model;
      result = hasAvailableBag(countModel) ? GRANTED : DENIED;
    }
    return result;
  }

  /** {@inheritDoc} */
  @Override
  public void setParameter(String argName, IConfigObject argValue) {
    if ("Status".equalsIgnoreCase(argName)) {
      bagStatus_.add(CriBagStatus.forName(argValue.toString()));
    }
    else {
      super.setParameter(argName, argValue);
    }
  }

  /**
   * Check has the status is AVAIABLE deposit bags.
   * 
   * @param argModel
   * @return
   */
  protected boolean hasAvailableBag(CriBagSummaryModel argModel) {
    for (CriBagSummaryItem countItem : argModel.getBagSummaryList()) {
      if (CriBagStatus.AVAILABLE.matches(countItem.getStatus())) {
        return true;
      }
    }

    return false;

  }
}
