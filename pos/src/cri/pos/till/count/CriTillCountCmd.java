//$Id: CriTillCountCmd.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import java.util.ArrayList;
import java.util.List;

import cri.xst.dao.tsn.ICriDepositBag;

import dtv.pos.till.count.TillCountCmd;

/**
 * The till command object for the till count options.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author shy xie
 * @created Jul 16, 2012
 * @version $Revision: 1105 $
 */
public class CriTillCountCmd
    extends TillCountCmd
    implements ICriDepositBagCmd {
  /**
   * 
   */
  private static final long serialVersionUID = -40233698754035271L;
  private CriBagSummaryModel bagSummaryModel_;
  private List<CriBagSummaryItem> selectedBagItem_ = new ArrayList<CriBagSummaryItem>();
  private CriDepositBagEditModel depositBagModel_;
  private List<ICriDepositBag> selectedDepositBag_ = new ArrayList<ICriDepositBag>();

  @Override
  public CriBagSummaryModel getBagSummaryModel() {
    return bagSummaryModel_;
  }

  @Override
  public void setBagSummaryModel(CriBagSummaryModel argBagSummaryModel) {
    bagSummaryModel_ = argBagSummaryModel;
  }

  @Override
  public List<CriBagSummaryItem> getSelectedBagItems() {
    return selectedBagItem_;
  }

  @Override
  public void setSelectedBagItems(List<CriBagSummaryItem> argSelectedBagItems) {
    selectedBagItem_ = argSelectedBagItems;
  }

  @Override
  public List<ICriDepositBag> getSelectedDepositItems() {
    return selectedDepositBag_;
  }

  @Override
  public void setSelectedDepositItems(List<ICriDepositBag> argSelectedDepositBag) {
    selectedDepositBag_ = argSelectedDepositBag;
  }

  @Override
  public CriDepositBagEditModel getDepositBagModel() {
    return depositBagModel_;
  }

  @Override
  public void setDepositBagModel(CriDepositBagEditModel argDepositBagModel) {
    depositBagModel_ = argDepositBagModel;
  }
}
