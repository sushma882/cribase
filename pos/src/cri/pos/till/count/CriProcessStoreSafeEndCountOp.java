//$Id: CriProcessStoreSafeEndCountOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import java.math.BigDecimal;

import cri.pos.till.CriTillHelper;

import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.TenderHelper;
import dtv.pos.till.TillHelper;
import dtv.pos.till.count.AbstractCreateTillCountObjectsOp;
import dtv.pos.till.count.ITillCountCmd;
import dtv.pos.till.types.SessionStatusCode;
import dtv.pos.till.types.TenderRepositoryTypeCode;
import dtv.util.NumberUtils;
import dtv.xst.dao.tsn.*;

/**
 * Update the money for the store safe by the counted amount.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author shy xie
 * @created Jul 17, 2012
 * @version $Revision: 1105 $
 */
public class CriProcessStoreSafeEndCountOp
    extends AbstractCreateTillCountObjectsOp {

  /**
   * 
   */
  private static final long serialVersionUID = -7642682295451838095L;
  protected static final CriTillHelper TILL_HELPER = CriTillHelper.getInstance();

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    IOpResponse response = super.handleOpExec(argCmd, argEvent);
    ITillCountCmd cmd = (ITillCountCmd) argCmd;
    ITenderControlTransaction transaction = cmd.getTenderControlTransaction();
    ISession storeSafeSession = cmd.getCurrentSession();
    transaction.setInboundSession(storeSafeSession);
    transaction.setInboundTenderRepository(storeSafeSession.getTenderRepository());
    String localCurrencyTenderId = TenderHelper.getInstance().getLocalCurrencyTenderId();
    BigDecimal totalRegisterFloatAmount = TILL_HELPER.getTotalRegisterCashRemaining();

    for (ITenderTypeCount tenderTypeCount : transaction.getTenderTypeCounts()) {
      for (ITenderCount tenderCount : tenderTypeCount.getTenderCounts()) {
        BigDecimal totalAmount = tenderCount.getAmount();
        BigDecimal storeSafeAmount = tenderCount.getAmount();
        if (localCurrencyTenderId.equals(tenderCount.getTenderId())) {
          totalAmount = totalAmount.add(totalRegisterFloatAmount);
        }
        ISessionTender sessionTender =
            TillHelper.getInstance().getSessionTender(storeSafeSession, tenderCount.getTenderId(), null);
        sessionTender.setMediaAmount(totalAmount);

        // Set the last closing cash amount for the store bank
        if (localCurrencyTenderId.equals(tenderCount.getTenderId())) {
          storeSafeAmount =
              NumberUtils.isGreaterThan(TILL_HELPER.getConfiguredStoreSafeAmount(), storeSafeAmount)
                  ? storeSafeAmount : TILL_HELPER.getConfiguredStoreSafeAmount();
          storeSafeSession.getTenderRepository().setLastClosingCashAmt(storeSafeAmount);
        }
      }
    }

    ITenderRepository depositBankRepository =
        TillHelper.getInstance().getTenderRepositoryListByType(TenderRepositoryTypeCode.BANK).get(0);
    transaction.setInboundTenderRepository(depositBankRepository);
    transaction.setOutboundSession(storeSafeSession);
    transaction.setOutboundTenderRepository(storeSafeSession.getTenderRepository());

    // Don't change the store safe float amount, just revert the changes in base class.
    //    storeSafeSession.getTenderRepository().setLastClosingCashAmt(floatAmount);
    return response;
  }

  /** {@inheritDoc} */
  @Override
  protected SessionStatusCode getSessionStatusCode() {
    return SessionStatusCode.ENDCOUNT;
  }

  /** {@inheritDoc} */
  @Override
  protected boolean updateSessionTender() {
    return false;
  }
}
