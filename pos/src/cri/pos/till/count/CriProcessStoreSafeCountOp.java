//$Id: CriProcessStoreSafeCountOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.till.count;

import java.math.BigDecimal;

import dtv.pos.till.ITillHelper;
import dtv.pos.till.TillHelper;
import dtv.pos.till.count.AbstractProcessStoreBankCashCountOp;
import dtv.xst.dao.tsn.ISessionTender;

/**
 * Process the store safe total count amount.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Jul 16, 2012
 * @version $Revision: 1105 $
 */
public class CriProcessStoreSafeCountOp
    extends AbstractProcessStoreBankCashCountOp {

  /**
   * 
   */
  private static final long serialVersionUID = 7109847979350937531L;
  protected static final ITillHelper TILL_HELPER = TillHelper.getInstance();

  @Override
  protected void adjustStoreBankCash(ISessionTender argStoreBankCash, BigDecimal argCashAmount) {
    BigDecimal totalCashRemainInRegisters = TILL_HELPER.getTotalRegisterCashRemaining();
    BigDecimal totalCashAmount = totalCashRemainInRegisters.add(argCashAmount);
    argStoreBankCash.setMediaAmount(totalCashAmount);
    argStoreBankCash.getParentSession().getTenderRepository().setLastClosingCashAmt(argCashAmount);
  }
}
