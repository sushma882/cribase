//$Id: ClothingVoucherTenderVisibilityRule.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.shared.visibilityrules;

import static cri.pos.common.CriCommonHelper.getClothingVoucherNumber;
import static dtv.pos.iframework.visibilityrules.AccessLevel.DENIED;
import static dtv.pos.iframework.visibilityrules.AccessLevel.GRANTED;

import dtv.pos.framework.visibilityrules.AbstractVisibilityRule;
import dtv.pos.iframework.visibilityrules.IAccessLevel;
import dtv.pos.register.returns.ReturnManager;
import dtv.util.StringUtils;
import dtv.xst.dao.trn.IPosTransaction;

/**
 * Check original transaction tender by clothing voucher.<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 * 
 * @author Bob.Shi
 * @created May 21, 2014
 * @version $Revision: 1105 $
 */
public class ClothingVoucherTenderVisibilityRule
    extends AbstractVisibilityRule {

  /** {@inheritDoc} */
  @Override
  protected IAccessLevel checkVisibilityImpl()
      throws Exception {
    IAccessLevel result = GRANTED;
    IPosTransaction trans = ReturnManager.getInstance().getCurrentOrigTransaction();
    if ((trans != null) && !StringUtils.isEmpty(getClothingVoucherNumber(trans))) {
      result = DENIED;
    }
    return result;
  }
}
