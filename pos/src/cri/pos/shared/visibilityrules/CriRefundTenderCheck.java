//$Id: RefundTenderCheck.java 55414 2010-12-07 23:00:05Z dtvdomain\jweiss $
package cri.pos.shared.visibilityrules;

import static dtv.pos.tender.TenderAvailabilityCodeType.*;

import java.util.Date;
import java.util.List;

import dtv.pos.common.TransactionHelper;
import dtv.pos.framework.visibilityrules.AbstractVisibilityRule;
import dtv.pos.iframework.visibilityrules.AccessLevel;
import dtv.pos.iframework.visibilityrules.IAccessLevel;
import dtv.pos.register.returns.ReturnType;
import dtv.pos.register.type.LineItemType;
import dtv.pos.storecalendar.StoreCalendar;
import dtv.pos.tender.TenderConstants;
import dtv.pos.tender.TenderHelper;
import dtv.pos.tender.voucher.config.ActivityConfig;
import dtv.pos.tender.voucher.config.VoucherRootConfig;
import dtv.util.CalendarField;
import dtv.util.DateUtils;
import dtv.util.config.IConfigObject;
import dtv.xst.dao.tnd.ITender;
import dtv.xst.dao.trl.*;

/**
 * This access check is for refund tenders only. This class contains all the validation checks on
 * minimum and maximum number of days and whether return with or without receipt, etc. To determine
 * whether or not the particular refund tender is accessable or not. <br>
 * Copyright (c) 2013 MICROS Retail
 *
 * @author pgolli
 * @created Feb 13, 2014
 * @version $Revision$
 */
public class CriRefundTenderCheck
    extends AbstractVisibilityRule {

  private ITender refundTenderWithReceipt_ = null;
  private ITender refundTenderWithGiftReceipt_ = null;
  private boolean noReceiptAccessSetting_ = true;
  private boolean remoteSendRefund_ = true;
  String param = null;

  /**
   * Set the parameter value that is needed for this access check. If the parameter name is tender,
   * the refund tender id is the parameter value.
   * @param argName the parameter name
   * @param argValue the parameter value
   */
  @Override
  public void setParameter(String argName, IConfigObject argValue) {
    if ("tender".equals(argName)) {
      param = argValue.toString();
      if (argValue.toString().equalsIgnoreCase(TenderConstants.LOCAL_CURRENCY)) {
        param = TenderHelper.getInstance().getLocalCurrencyTenderId();
      }

      TenderHelper helper = TenderHelper.getInstance();

      // look up if this tender can be used for a refund without a receipt
      if (null == helper.tenderAvailable(RETURN_WITHOUT_RECEIPT, param, argValue.getSourceDescription())) {
        // not available without a receipt
        noReceiptAccessSetting_ = false;
      }
      else {
        // is available without a receipt
        noReceiptAccessSetting_ = true;
      }

      // look up if this tender can be used for a refund with a receipt or gift receipt
      refundTenderWithReceipt_ =
          helper.tenderAvailable(RETURN_WITH_RECEIPT, param, argValue.getSourceDescription());
      refundTenderWithGiftReceipt_ =
          helper.tenderAvailable(RETURN_WITH_GIFT_RECEIPT, param, argValue.getSourceDescription());

      if (null == helper.tenderAvailable(REMOTE_SEND_REFUND, param, argValue.getSourceDescription())) {
        remoteSendRefund_ = false;
      }
      else {
        remoteSendRefund_ = true;
      }

    }
    else {
      super.setParameter(argName, argValue);
    }
  }

  /**
   * Check tender user settings for min and max number of days return and return with or without
   * receipt for the particular tender to determine whether or not this refund tender should be
   * accessable in this transaction.
   * 
   * @return {@inheritDoc}
   */
  @Override
  protected IAccessLevel checkVisibilityImpl() {
    IRetailTransaction retailTran = getCurrentRetailTransaction();
    if (retailTran == null) {
      return AccessLevel.DENIED;
    }

    List<IRetailTransactionLineItem> saleReturnItems =
        retailTran.getLineItemsByTypeCode(LineItemType.ITEM.getName());

    if ((saleReturnItems == null) || (saleReturnItems.size() == 0)) {
      return AccessLevel.DENIED;
    }

    if (TransactionHelper.isRemoteSendRefund(saleReturnItems)) {
      /*
       * We are refunding a Remote Send.  Use a different availability code.
       * Activity 162928.  - C Dusseau 4/10/06
       */
      if (remoteSendRefund_) {
        return AccessLevel.GRANTED;
      }
      else {
        return AccessLevel.DENIED;
      }
    }

    boolean hasReturnItems = false;
    Date youngestItemDate = null;

    for (int i = 0; i < saleReturnItems.size(); i++ ) {
      ISaleReturnLineItem line = (ISaleReturnLineItem) saleReturnItems.get(i);

      if (line.getReturn() && !line.getVoid()) {
        hasReturnItems = true;
        Date d = line.getOriginalBusinessDate();

        boolean cashoutGiftCard = false;

        if (line.getReturn() && (line instanceof IVoucherSaleLineItem)) {
          VoucherRootConfig rootConfig = TenderHelper.getInstance().getVoucherRootConfig();
          ActivityConfig actConfig = rootConfig.getActivityConfigForItemId(line.getItem().getItemId());

          if ((actConfig != null) && actConfig.getVoucherItemCategory().equalsIgnoreCase("GIFT_CARD")) {
            youngestItemDate = StoreCalendar.getCurrentBusinessDate();
            d = StoreCalendar.getCurrentBusinessDate();
            cashoutGiftCard = true;
          }
        }

        //As long as one return item doesn't have receipt, consider no receipt at all.
        if ((!cashoutGiftCard)
            && (/*(d == null) ||*/ !ReturnType.valueOf(line.getReturnTypeCode()).isReceipted())) {

          youngestItemDate = null;
          break;
        }

        if ((youngestItemDate == null) || ((d != null) && d.after(youngestItemDate))) {

          /*It is possible even though the return is receipted return  original business date is Null. This happens for cross channel returns.
           * CRI would like to offer credit/debit tender if it is Ecom return.
           */
          if (ReturnType.valueOf(line.getReturnTypeCode()).isReceipted() && (d == null)) {
            d = StoreCalendar.getCurrentBusinessDate();
          }
          youngestItemDate = d;

        }
      }
    }

    if (!hasReturnItems) {
      return AccessLevel.DENIED;
    }
    if (youngestItemDate == null) {
      // we must not have any receipts
      if (noReceiptAccessSetting_) {

        return AccessLevel.GRANTED;
      }
      else {

        return AccessLevel.DENIED;
      }
    }

    // Unfortunately, need to trek through items again to determine if a gift receipt
    // was used when returning any.  Loop above potentially does not hit each item, so
    // need to process entire loop again.
    boolean giftReceiptPresent = false;

    for (IRetailTransactionLineItem item : saleReturnItems) {
      ISaleReturnLineItem saleItem = (ISaleReturnLineItem) item;

      if (saleItem.getReturn() && !saleItem.getVoid() && saleItem.getReturnedWithGiftReceipt()) {
        giftReceiptPresent = true;
      }
    }

    long daysSinceMostRecentPurchase =
        DateUtils.dateDiff(CalendarField.DAY, StoreCalendar.getCurrentBusinessDate(), youngestItemDate);

    if (daysSinceMostRecentPurchase < 0) {
      // a return from the future... shouldn't happen in practice, but if a register's business date is off &
      // we pulled this transaction from somewhere else, daysSinceMostRecentPurchase could be negative.
      daysSinceMostRecentPurchase = 0;
    }

    boolean giftCardFlag = false;
    if (param != null) {
      giftCardFlag = param.equalsIgnoreCase("ISSUE_GIFT_CARD") || param.equalsIgnoreCase("RELOAD_GIFT_CARD")
          || param.equalsIgnoreCase("ISSUE_ISD_GIFT_CARD") || param.equalsIgnoreCase("RELOAD_ISD_GIFT_CARD");
    }

    if (giftCardFlag && giftReceiptPresent) {
      return AccessLevel.GRANTED;
    }
    else if ((refundTenderWithReceipt_ != null)
        && (refundTenderWithReceipt_.getMinDaysForReturn() <= daysSinceMostRecentPurchase)
        && (daysSinceMostRecentPurchase <= refundTenderWithReceipt_.getMaxDaysForReturn())) {

      if (refundTenderWithGiftReceipt_ == null) {
        if ((refundTenderWithGiftReceipt_ == null) && giftReceiptPresent) {

          return AccessLevel.DENIED;
        }
      }
      //allowed

      return AccessLevel.GRANTED;
    }
    else {
      //not allowed

      return AccessLevel.DENIED;
    }
  }
}
