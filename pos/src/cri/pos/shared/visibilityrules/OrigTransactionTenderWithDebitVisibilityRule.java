//$Id: OrigTransactionTenderWithDebitVisibilityRule.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.shared.visibilityrules;

import java.util.List;

import dtv.pos.framework.visibilityrules.AbstractVisibilityRule;
import dtv.pos.iframework.visibilityrules.AccessLevel;
import dtv.pos.iframework.visibilityrules.IAccessLevel;
import dtv.pos.register.returns.ReturnManager;
import dtv.xst.dao.tnd.ITender;
import dtv.xst.dao.trl.IRetailTransaction;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Sep 27, 2012
 * @version $Revision: 1105 $
 */
public class OrigTransactionTenderWithDebitVisibilityRule
    extends AbstractVisibilityRule {

  /** {@inheritDoc} */
  @Override
  protected IAccessLevel checkVisibilityImpl()
      throws Exception {

    List<IRetailTransaction> origTransactions = ReturnManager.getInstance().getAllOrigTransaction();

    if ((origTransactions == null) || origTransactions.isEmpty()) {
      return AccessLevel.GRANTED;
    }

    boolean isAllDebitTransaction = false;

    for (IRetailTransaction transaction : origTransactions) {
      List<ITenderLineItem> tenderLineItems = transaction.getLineItems(ITenderLineItem.class);

      if ((tenderLineItems == null) || tenderLineItems.isEmpty()) {
        continue;
      }

      isAllDebitTransaction = true; //Assuming the current transaction has all DEBIT tenderlineitems

      for (ITenderLineItem tndrLineItem : tenderLineItems) {
        if (!tndrLineItem.getVoid()) {
          ITender tender = tndrLineItem.getTender();
          if (!"DEBIT_CARD".equalsIgnoreCase(tender.getTenderId())) {
            isAllDebitTransaction = false;
            break;
          }
        }
      }

      if (!isAllDebitTransaction) {
        break;
      }
    }
    if (isAllDebitTransaction) {
      return AccessLevel.DENIED;
    }
    else {
      return AccessLevel.GRANTED;
    }
  }
}
