//$Id: CriCustSignRule.java 1063 2016-08-22 18:58:00Z olr.jgaughn $
package cri.pos.hardware.rcptbuilding.copyrules;

import cri.pos.tender.CriTenderHelper;

import dtv.pos.hardware.rcptbuilding.copyrules.CustSignRule;
import dtv.pos.tender.TenderHelper;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * Print Store copy receipt when card is not swipe<br>
 * <br>
 * Copyright (c) 2013 MICROS Retail
 *
 * @author abalane
 * @created Apr 24, 2013
 * @version $Revision: 1063 $
 */
public class CriCustSignRule
    extends CustSignRule {

  /** {@inheritDoc} */
  @Override
  protected boolean doesRuleApply(Object argSource) {
    if (argSource instanceof IPosTransaction) {
      CriTenderHelper th = (CriTenderHelper) TenderHelper.getInstance();
      for (ITenderLineItem lineItem : ((IPosTransaction) argSource).getLineItems(ITenderLineItem.class)) {
        if (th.needsSignatureCapture(lineItem)) {
          return true;
        }
      }
    }

    return false;
  }

}
