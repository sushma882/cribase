//$Id: CriPricingHelper.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.pricing;

import static dtv.data2.access.DataFactory.createObject;
import static dtv.pos.storecalendar.StoreCalendar.getCurrentBusinessDate;
import static dtv.util.NumberUtils.ZERO;
import static java.math.BigDecimal.ONE;
import static java.util.Calendar.DATE;

import java.math.BigDecimal;
import java.util.*;

import org.apache.log4j.Logger;

import cri.pos.common.CriConfigurationMgr;
import cri.pos.register.returns.SalePriceHistorySearchResult;

import dtv.data2.access.*;
import dtv.hardware.types.HardwareType;
import dtv.pos.pricing.*;
import dtv.pos.register.ItemLocator;
import dtv.pricing2.PricingDeal;
import dtv.util.DateUtils;
import dtv.util.NumberUtils;
import dtv.xst.dao.itm.IItem;
import dtv.xst.dao.trl.*;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.pricing.XSTPricingDescriptor;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 * 
 * @author Jerny.Liu
 * @created Mar 25, 2014
 * @version $Revision: 1105 $
 */
public class CriPricingHelper
    implements ICriPricingHelper {

  private static final Logger _logger = Logger.getLogger(CriPricingHelper.class);
  private static final ICriPricingHelper _instance;
  private static final IQueryKey<SalePriceHistorySearchResult> CRI_SALE_PRICE_HISTORY =
      new QueryKey<SalePriceHistorySearchResult>("CRI_SALE_PRICE_HISTORY",
          SalePriceHistorySearchResult.class);

  static {
    // Check the system configured adjuster to use...
    String className = System.getProperty(PricingHelper.class.getName());
    ICriPricingHelper temp = null;
    try {
      temp = (ICriPricingHelper) Class.forName(className).newInstance();
    }
    catch (Exception ex) {
      // Custom one was not configured; use the default.
      temp = new CriPricingHelper();
    }
    _instance = temp;
  }

  /**
   * Get the singleton instance of the class implementing ICriPricingHelper.
   * @return the class implementing the interface ICriPricingHelper.
   */
  public static ICriPricingHelper getInstance() {
    return _instance;
  }

  /** {@inheritDoc} */
  @Override
  public BigDecimal getLowestSalePrice(IItem argItem) {
    Map<String, Object> params = new HashMap<String, Object>();

    params.put("argReturnCutOffDate", getReturnCutOffDate());
    params.put("argItemId", argItem.getItemId());
    List<SalePriceHistorySearchResult> result =
        DataFactory.getObjectByQueryNoThrow(CRI_SALE_PRICE_HISTORY, params);

    if (result.isEmpty()) {
      _logger.debug("No sales history for item" + argItem.getItemId());
      return null;
    }
    return result.get(0).getPrice();
  }

  /** {@inheritDoc} */
  @Override
  public BigDecimal getLowestPrice(IItem argItem) {
    BigDecimal lowestPrice = ZERO;

    BigDecimal lowestSalePrice = getLowestSalePrice(argItem);
    if ((lowestSalePrice != null) && NumberUtils.isGreaterThan(lowestSalePrice, BigDecimal.ZERO)) {
      lowestPrice = lowestSalePrice;
    }
    else {
      lowestPrice = PricingHelper.getInstance().getPriceExcludingMultipleQuantityDeals(argItem);
    }

    return lowestPrice;
  }

  /** {@inheritDoc} */
  @Override
  public List<IRetailPriceModifier> getConditionalRetailPriceModifiers(IItem argItem) {
    List<IRetailPriceModifier> mods = new ArrayList<IRetailPriceModifier>();

    IRetailTransaction dummyTrans = DataFactory.createObject(IRetailTransaction.class);

    //add dummy line item
    ISaleReturnLineItem dummyLineItem =
        ItemLocator.getLocator().getSaleLineItem(argItem, SaleItemType.SALE, HardwareType.KEYBOARD);
    dummyLineItem.setQuantity(ONE);
    dummyTrans.addRetailTransactionLineItem(dummyLineItem);

    PricingDeal<XSTPricingDescriptor>[] workingDS = dummyTrans.getEligibleConditionalDeals();

    for (PricingDeal<XSTPricingDescriptor> ds : workingDS) {
      IRetailPriceModifier mod = createObject(IRetailPriceModifier.class);
      XSTPricingDescriptor deal = ds.getNativeDeal();

      mod.setDealId(deal.getDealId());
      mod.setTaxabilityCode(deal.getTaxabilityCode());
      mod.setRetailPriceModifierReasonCode(deal.getReason().getName());
      mod.setNotes(deal.getDescription());
      mod.setDescription(deal.getDescription());
      mods.add(mod);
    }

    return mods;
  }

  /** {@inheritDoc} */
  @Override
  public List<IRetailPriceModifier> getNonConditionalRetailPriceModifiers(IItem argItem) {
    AbstractDealCalculator calc = new Pricing2Calculator();
    List<IRetailTransactionLineItem> lineItems = getLineItemsFromDummyTrans(argItem);

    IRetailTransactionLineItem[] postCalcLines =
        calc.handleLineItemEvent(lineItems.toArray(new IRetailTransactionLineItem[0]));

    // Calculate applicable quantity 1 deals.
    ThresholdDealCalculator calcDeal = new ThresholdDealCalculator();
    postCalcLines = calcDeal.handleLineItemEvent(lineItems.toArray(new IRetailTransactionLineItem[0]));

    ISaleReturnLineItem saleItem = (ISaleReturnLineItem) postCalcLines[0];

    return saleItem.getRetailPriceModifiers();
  }

  protected static Date getReturnCutOffDate() {
    Calendar cal = Calendar.getInstance();

    cal.setTime(getCurrentBusinessDate());
    cal.add(DATE, (-1) * CriConfigurationMgr.getBlindReturnMaxDaysLimit());

    Date cutoffDate = cal.getTime();
    if (cal.getTime().before(DateUtils.getFarPastDate())) {
      cutoffDate = DateUtils.getFarPastDate();
    }
    return cutoffDate;
  }

  protected List<IRetailTransactionLineItem> getLineItemsFromDummyTrans(IItem argItem) {
    BigDecimal currentPrice = PriceProvider.getActualPrice(argItem.getItemId());
    IPosTransaction dummyTrans = DataFactory.createObject(IPosTransaction.class);

    ISaleReturnLineItem dummyLineItem =
        ItemLocator.getLocator().getSaleLineItem(argItem, SaleItemType.SALE, HardwareType.KEYBOARD);

    dummyLineItem.setBaseUnitPrice(currentPrice);
    dummyTrans.addRetailTransactionLineItem(dummyLineItem);

    return dummyTrans.getRetailTransactionLineItems();
  }

}
