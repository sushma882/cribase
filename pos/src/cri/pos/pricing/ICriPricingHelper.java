//$Id: ICriPricingHelper.java 777 2014-03-27 02:15:29Z suz.jliu $
package cri.pos.pricing;

import java.math.BigDecimal;
import java.util.List;

import dtv.xst.dao.itm.IItem;
import dtv.xst.dao.trl.IRetailPriceModifier;

/**
 * Interface representing a helper class for getting various pricing based attributes for items.<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 * 
 * @author Jerny.Liu
 * @created Mar 25, 2014
 * @version $Revision: 777 $
 */
public interface ICriPricingHelper {

  public BigDecimal getLowestSalePrice(IItem argItem);

  public BigDecimal getLowestPrice(IItem argItem);

  /**
   * Get all eligible conditional deals for the item.
   * 
   * @param argItem
   * @return
   */
  public List<IRetailPriceModifier> getConditionalRetailPriceModifiers(IItem argItem);

  /**
   * Get non-conditional deals for the item.
   * 
   * @param argItem
   * @return
   */
  public List<IRetailPriceModifier> getNonConditionalRetailPriceModifiers(IItem argItem);

}
