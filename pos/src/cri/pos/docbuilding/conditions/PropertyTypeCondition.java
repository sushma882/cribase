//$Id: PropertyTypeCondition.java 908 2015-07-31 17:16:56Z suz.sxie $
package cri.pos.docbuilding.conditions;

import dtv.data2.access.IDataProperty;
import dtv.docbuilding.conditions.AbstractInvertableCondition;

/**
 * Properties doc build condition.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision: 908 $
 */
public class PropertyTypeCondition
    extends AbstractInvertableCondition {
  private String propertyType_;

  /** {@inheritDoc} */
  @Override
  protected boolean conditionMetImpl(Object o) {
    boolean result = false;
    if (o instanceof IDataProperty) {
      IDataProperty property = (IDataProperty) o;
      if (property.getType().equals(propertyType_)) {
        result = true;
      }
    }

    return result;
  }

  /** {@inheritDoc} */
  @Override
  public void setParameter(String argName, Object argValue) {
    if ("value".equalsIgnoreCase(argName)) {
      propertyType_ = argValue.toString().toUpperCase();
    }
    else {
      super.setParameter(argName, argValue);
    }
  }
}
