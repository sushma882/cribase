//$Id: CriCacheRefreshOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.cacherefresh;

import static cri.pos.common.CriConstants.DATALOADER_PATH;
import static cri.pos.common.CriConstants.XSTORE_REFRESH_FLAG_FILE;
import static java.lang.System.getProperty;

import java.io.File;

import org.apache.log4j.Logger;

import dtv.data2.cache.CacheManager;
import dtv.pos.common.LocationFactory;
import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.xst.pricing.XSTPricingAdapter;

/**
 * Operation responsible for reloading Xstore cache<br>
 * <br>
 * Copyright (c) 2011 MICROS Retail
 * 
 * @author pgolli
 * @created May 5, 2011
 * @version $Revision: 1105 $
 */
public class CriCacheRefreshOp
    extends Operation {

  /**
   * 
   */
  private static final long serialVersionUID = 6635100116552859258L;
  //private static final long serialVersionUID = 1L;
  private final static Logger logger_ = Logger.getLogger(CriCacheRefreshOp.class);

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    String flagPath = getProperty(DATALOADER_PATH);
    File refreshFlagFile = new File(flagPath + XSTORE_REFRESH_FLAG_FILE);
    logger_.debug("refreshFlagFile.getPath " + refreshFlagFile.getAbsolutePath());

    long cacheRefreshTime = CriCacheRefreshHelper.getInstance().getReloadTime();

    logger_.debug("cacheRefreshTime " + cacheRefreshTime);
    logger_.debug("refreshFlagFile.lastModified() " + refreshFlagFile.lastModified());

    if (refreshFlagFile.exists() && refreshFlagFile.isFile()) {
      //clear DTX cache
      CacheManager.getInstance().clear();
      logger_.debug("CacheManager cleared");
      //reinitialize
      LocationFactory.getInstance().getMyOrganizationHierarchyNode();
      logger_.debug("LocationFactory reloaded");
      //Reload deals
      XSTPricingAdapter.getInstance().getMidnightTask().run();
      logger_.debug("Deals reloaded");

      CriCacheRefreshHelper.getInstance().setReloadTime(refreshFlagFile.lastModified());

      logger_.debug("Cache refreshed");

      if (refreshFlagFile.delete()) {
        logger_.debug("refreshFlagFile deleted");
      }
      else {
        logger_.warn("refreshFlagFile could not be deleted");
      }
    }

    return HELPER.completeResponse();
  }

}
