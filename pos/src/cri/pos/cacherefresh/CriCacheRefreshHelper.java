//$Id: CriCacheRefreshHelper.java 119 2012-07-16 15:12:24Z suz.sxie $
package cri.pos.cacherefresh;

import java.io.File;

/**
 * A helper class to store RELOAD_CACHE.flg attributes<br>
 * <br>
 * Copyright (c) 2011 MICROS Retail
 * 
 * @author pgolli
 * @created May 5, 2011
 * @version $Revision: 119 $
 */
public class CriCacheRefreshHelper {

  private static CriCacheRefreshHelper INSTANCE;
  private File file_ = null;
  private long reloadTime_;

  private CriCacheRefreshHelper() {}

  static {
    INSTANCE = new CriCacheRefreshHelper();
  }

  public static CriCacheRefreshHelper getInstance() {
    return INSTANCE;
  }

  public void setFile(File f) {
    file_ = f;
  }

  public File getFile() {
    return file_;
  }

  public long getReloadTime() {
    return reloadTime_;
  }

  public void setReloadTime(long l) {
    reloadTime_ = l;
  }

}
