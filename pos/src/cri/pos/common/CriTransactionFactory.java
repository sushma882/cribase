//$Id: CriTransactionFactory.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.common;

import java.math.BigDecimal;

import dtv.data2.access.DataFactory;
import dtv.pos.common.ConfigurationMgr;
import dtv.pos.common.TransactionFactory;
import dtv.pos.iframework.ITransactionType;
import dtv.pos.storecalendar.StoreCalendar;
import dtv.util.DateUtils;
import dtv.util.sequence.SequenceFactory;
import dtv.xst.dao.crm.IParty;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.trn.PosTransactionId;

/**
 * Transaction factory CRI extension to skip trans_seq that ending with 0000. Because this is
 * invalid number to send on settlement file. IE: 10000 will skip to 10001 or 20000 will skip to
 * 20001 or 100000 will skip to 100001<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 * 
 * @author Zeal.Zong
 * @created Sep 4, 2014
 * @version $Revision: 1105 $
 */
public class CriTransactionFactory
    extends TransactionFactory {
  protected static final BigDecimal DEFAULT_SKIPPED_TRANSACTION_SEQUENCE_DIVISOR = BigDecimal.valueOf(10000);

  @Override
  public <T extends IPosTransaction> T createTransaction(ITransactionType<T> argType, IParty argOperator) {
    // Get the interface class that maps to the specified type
    Class<T> interfaceClass = argType.getInterfaceClass();

    // Create the transaction id from the interface class
    PosTransactionId tranId = new PosTransactionId();

    // tms using new storecalendars to track the business date
    tranId.setBusinessDate(DateUtils.clearTime(StoreCalendar.getCurrentBusinessDate()));
    tranId.setRetailLocationId(new Long(ConfigurationMgr.getRetailLocationId()));
    tranId.setWorkstationId(new Long(ConfigurationMgr.getWorkstationId()));

    long transSeq = SequenceFactory.getNextLongValue(TRANSACTION_SEQUENCE_TYPE);
    /*
     * CRI 437640 ----SS-140 Sending Transaction number of 0000 in settlement file
     *   When the system reaches 10000 for transaction sequence, or any increment of 0000,
     *   the system will skip that sequence number and move to the next sequence number. This is due to settlement file requirement.
     */
    if ((transSeq != 0) && (BigDecimal.valueOf(transSeq)
        .remainder(DEFAULT_SKIPPED_TRANSACTION_SEQUENCE_DIVISOR) == BigDecimal.ZERO)) {
      //Skip this one and get next
      transSeq = SequenceFactory.getNextLongValue(TRANSACTION_SEQUENCE_TYPE);
    }

    tranId.setTransactionSequence(transSeq);
    T tran = DataFactory.createObject(tranId, interfaceClass);

    updateNewTransaction(tran, argType, argOperator);

    // Initialize all the pricing data and ready to be used.
    tran.getPricing();

    return tran;
  }
}
