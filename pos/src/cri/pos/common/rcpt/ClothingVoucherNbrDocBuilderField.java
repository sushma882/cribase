//$Id: ClothingVoucherNbrDocBuilderField.java 785 2014-05-23 01:40:51Z suz.bshi $
package cri.pos.common.rcpt;

import static cri.pos.common.CriCommonHelper.getClothingVoucherNumber;

import dtv.docbuilding.AbstractDocBuilderField;
import dtv.docbuilding.IDocElementFactory;
import dtv.docbuilding.types.DocBuilderAlignmentType;
import dtv.i18n.formatter.output.IOutputFormatter;
import dtv.xst.dao.trl.IRetailTransaction;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 * 
 * @author Bob.Shi
 * @created May 21, 2014
 * @version $Revision: 785 $
 */
public class ClothingVoucherNbrDocBuilderField
    extends AbstractDocBuilderField {

  /**
   * Constructs a <code>ClothingVoucherNbrDocBuilderField</code>.
   * @param argContents
   * @param argStyle
   * @param argLocation
   * @param argAlignment
   * @param argPriority
   * @param argFormatter
   */
  public ClothingVoucherNbrDocBuilderField(String argContents, String argStyle, Integer argLocation,
      DocBuilderAlignmentType argAlignment, int argPriority, IOutputFormatter argFormatter) {
    super(argContents, argStyle, argLocation, argAlignment, argPriority, argFormatter);
  }

  /** {@inheritDoc} */
  @Override
  public String getContents(Object argSource, IDocElementFactory argFactory) {
    IRetailTransaction trans = (IRetailTransaction) argSource;
    return getClothingVoucherNumber(trans);
  }

}
