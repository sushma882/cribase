//$Id: CriEmvReceiptIterator.java 1049 2016-07-08 17:38:37Z olr.jgaughn $
package cri.pos.common.rcpt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cri.ajb.fipay.impl.FipayLineItemProperties;

import dtv.docbuilding.*;
import dtv.xst.dao.trl.IRetailTransLineItemProperty;
import dtv.xst.dao.trl.IRetailTransactionLineItem;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2016 OLR Retail
 *
 * @author johgaug
 * @created Jul 5, 2016
 * @version $Revision: 1049 $
 */
public class CriEmvReceiptIterator
    extends DocBuilderIterator {

  /** {@inheritDoc} */
  @Override
  protected void iterate(IPosDocument argDoc, IDocElementFactory argElementFactory, Object argObject)
      throws IOException {
    IRetailTransactionLineItem lineItem = (IRetailTransactionLineItem) argObject;
    List<IRetailTransLineItemProperty> applicableTags = new ArrayList<IRetailTransLineItemProperty>();

    for (FipayLineItemProperties property : FipayLineItemProperties.listEmvReceipt()) {
      IRetailTransLineItemProperty eval = lineItem.getProperty(property.getName());
      if (eval != null) {
        applicableTags.add(eval);
      }
    }
    super.iterateList(argDoc, argElementFactory, applicableTags);
  }

}
