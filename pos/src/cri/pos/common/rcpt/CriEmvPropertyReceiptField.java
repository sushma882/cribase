//$Id: CriEmvPropertyReceiptField.java 1049 2016-07-08 17:38:37Z olr.jgaughn $
package cri.pos.common.rcpt;

import dtv.docbuilding.AbstractDocBuilderField;
import dtv.docbuilding.IDocElementFactory;
import dtv.docbuilding.types.DocBuilderAlignmentType;
import dtv.i18n.formatter.output.IOutputFormatter;
import dtv.xst.dao.trl.IRetailTransLineItemProperty;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2016 OLR Retail
 *
 * @author johgaug
 * @created Jul 5, 2016
 * @version $Revision: 1049 $
 */
public class CriEmvPropertyReceiptField
    extends AbstractDocBuilderField {

  public CriEmvPropertyReceiptField(String argContents, String argStyle, Integer argLocation,
      DocBuilderAlignmentType argAlignment, int argPriority, IOutputFormatter argFormatter) {
    super(argContents, argStyle, argLocation, argAlignment, argPriority, argFormatter);
  }

  /** {@inheritDoc} */
  @Override
  public String getContents(Object argSource, IDocElementFactory argFactory) {
    IRetailTransLineItemProperty property = (IRetailTransLineItemProperty) argSource;
    String receiptCode = property.getPropertyCode() + ": " + property.getStringValue();
    return receiptCode;
  }

}
