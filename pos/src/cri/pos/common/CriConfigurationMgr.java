//$Id: CriConfigurationMgr.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.common;

import java.util.ArrayList;
import java.util.List;

import dtv.pos.common.ConfigurationMgr;
import dtv.util.StringUtils;

/**
 * A utility class responsible for accessing a wide variety of configurable application parameters
 * and returning their runtime values to the requesting object.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author shy xie
 * @created Jul 15, 2012
 * @version $Revision: 1105 $
 */
public class CriConfigurationMgr
    extends ConfigurationMgr {
  public static final String STORE_SAFE_TAG = "StoreSafe";
  public static final String RETURN_SETTING_TAG = "ReturnSetting";
  public static final String CRI_CONFIG_TAG = "CRIConfig";
  public static final String STOREDOWN_ENTRYMODE_TAG = "StoreDownEntryMode";

  /**
   * The configured number of days between the bag’s creation date and the business date.
   * 
   * @return
   */
  public static int getStoreSafeDepositPickupThresholdDays() {
    return getHelper()
        .getInt(new String[] {STORE_TAG, SYSTEM_CONFIG_TAG, STORE_SAFE_TAG, "DepositPickupThresholdDays"});
  }

  /**
   * Get the configured check tender IDs.
   * 
   * @return
   */
  public static List<String> getCheckTenderIds() {
    List<String> checkTenderIdList = new ArrayList<String>();
    String tenderIds =
        getHelper().getString(new String[] {STORE_TAG, SYSTEM_CONFIG_TAG, STORE_SAFE_TAG, "CheckTenderIds"});
    if (!StringUtils.isEmpty(tenderIds)) {
      String[] idArray = tenderIds.split(",");
      for (String id : idArray) {
        checkTenderIdList.add(id.trim());
      }
    }

    return checkTenderIdList;

  }

  public static Integer getBlindReturnMaxDaysLimit() {
    return getHelper()
        .getInt(new String[] {STORE_TAG, SYSTEM_CONFIG_TAG, RETURN_SETTING_TAG, "BlindReturnMaxDaysLimit"});
  }

  public static boolean enableClothingVoucherTender() {
    return getHelper().getBoolean(
        new String[] {STORE_TAG, SYSTEM_CONFIG_TAG, CRI_CONFIG_TAG, "EnableClothingVoucherTender"});
  }

  public static boolean isAjbEnabled() {
    return getHelper().getBoolean(new String[] {STORE_TAG, REGISTER_CONFIG_TAG, "AjbFiPay", "Enabled"});
  }

  public static String getAjbTokenFilePath() {
    return getHelper().getString(new String[] {STORE_TAG, REGISTER_CONFIG_TAG, "AjbFiPay", "TokenFilePath"});
  }

  public static String getAjbTokenFileExtensionName() {
    return getHelper()
        .getString(new String[] {STORE_TAG, REGISTER_CONFIG_TAG, "AjbFiPay", "TokenFileExtensionName"});
  }

  public static String getTokenToReplacementComma() {
    return getHelper()
        .getString(new String[] {STORE_TAG, REGISTER_CONFIG_TAG, "AjbFiPay", "TokenToReplacementComma"});
  }

  public static int getMaxVerifoneDisplayItemCount() {
    return getHelper()
        .getInt(new String[] {STORE_TAG, REGISTER_CONFIG_TAG, "AjbFiPay", "MaxVerifoneDisplayItemCount"});
  }

  public static String getStoreDownEntryModeFlagFile() {
    return getHelper()
        .getString(new String[] {STORE_TAG, REGISTER_CONFIG_TAG, STOREDOWN_ENTRYMODE_TAG, "FlagFile"});
  }

  public static String getStoreDownEntryModeFlagFilePath() {
    return getHelper()
        .getString(new String[] {STORE_TAG, REGISTER_CONFIG_TAG, STOREDOWN_ENTRYMODE_TAG, "FlagFilePath"});
  }
}
