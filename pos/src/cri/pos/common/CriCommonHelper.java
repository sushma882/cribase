//$Id: CriCommonHelper.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.common;

import static cri.pos.common.CriConstants.*;
import static dtv.util.StringUtils.EMPTY;

import java.math.BigDecimal;
import java.util.List;

import dtv.data2.access.DataFactory;
import dtv.pos.common.CommonHelper;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.util.StringUtils;
import dtv.xst.dao.tax.ITaxExemption;
import dtv.xst.dao.tax.TaxExemptionId;
import dtv.xst.dao.trl.ISaleReturnLineItem;
import dtv.xst.dao.trl.ISaleTaxModifier;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * Methods that are useful throughout the customer overlay but do not belong to a specific helper.
 * <br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author Bob Shi
 * @created May 9, 2011
 * @version $Revision: 1105 $
 */
public class CriCommonHelper
    extends CommonHelper {

  /**
   * Get instance.
   * 
   * @return
   */
  public static CriCommonHelper getInstance() {
    return (CriCommonHelper) CommonHelper.getInstance();
  }

  public static String getClothingVoucherNumber(IPosTransaction argTrans) {
    String result = EMPTY;

    List<ITenderLineItem> lineItems = argTrans.getLineItems(ITenderLineItem.class);

    if ((lineItems == null) || lineItems.isEmpty()) {
      return result;
    }

    for (ITenderLineItem line : lineItems) {

      if (!line.getVoid()) {
        result = line.getStringProperty(CLOTHING_VOUCHER_NUMBER);
      }
    }
    return result;
  }

  public static BigDecimal getClothingVoucherAmount(IPosTransaction argTrans) {
    BigDecimal result = null;
    List<ITenderLineItem> lineItems = argTrans.getLineItems(ITenderLineItem.class);

    if ((lineItems == null) || lineItems.isEmpty()) {
      return result;
    }

    for (ITenderLineItem line : lineItems) {
      if (!line.getVoid()) {
        result = line.getDecimalProperty(CLOTHING_VOUCHER_AMOUNT);
      }
    }
    return result;
  }

  public static boolean hasWestVirginiaTaxExempt(IXstCommand argCmd) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    ITaxExemption taxExemption = cmd.getRetailTransaction().getTaxExemption();
    if (isWestVirginiaTaxExempt(taxExemption)) {
      return true;
    }

    for (ISaleReturnLineItem line : cmd.getRetailTransaction().getLineItems(ISaleReturnLineItem.class)) {
      if (!line.getVoid()) {
        for (ISaleTaxModifier taxMod : line.getTaxModifiers()) {
          if (!taxMod.getVoid() && !StringUtils.isEmpty(taxMod.getTaxExemptionId())) {
            TaxExemptionId id = new TaxExemptionId();
            id.setOrganizationId(Long.valueOf(taxMod.getOrganizationId()));
            id.setTaxExemptionId(taxMod.getTaxExemptionId());
            taxExemption = DataFactory.getObjectByIdNoThrow(id);
            if (isWestVirginiaTaxExempt(taxExemption)) {
              return true;
            }
          }
        }
      }
    }
    return false;
  }

  public static boolean isWestVirginiaTaxExempt(ITaxExemption argTaxExemption) {
    boolean result = false;
    if (argTaxExemption != null) {
      if (WEST_VIRGINIA_TAX_EXEMPT_REASON_CODE.equals(argTaxExemption.getReasonCode())) {
        result = true;
      }
    }
    return result;
  }

}
