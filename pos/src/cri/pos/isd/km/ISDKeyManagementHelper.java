//$Id: ISDKeyManagementHelper.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.isd.km;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Sep 20, 2012
 * @version $Revision: 1105 $
 */
public class ISDKeyManagementHelper {

  public static final String SYSTEM_PROPERTY = ISDKeyManagementHelper.class.getName();
  private static final ISDKeyManagementHelper INSTANCE;

  private String keyId;
  private String currKeyValue;

  static {
    // Check the system configured factory to use...
    String className = System.getProperty(SYSTEM_PROPERTY);
    ISDKeyManagementHelper tempInstance = null;

    try {
      tempInstance = (ISDKeyManagementHelper) Class.forName(className).newInstance();
    }
    catch (Exception ex) {
      tempInstance = new ISDKeyManagementHelper();
    }

    INSTANCE = tempInstance;
  }

  protected ISDKeyManagementHelper() {

  }

  public static ISDKeyManagementHelper getInstance() {
    return INSTANCE;
  }

  public String getKeyId() {
    return keyId;
  }

  public void setKeyId(String argKeyId) {
    keyId = argKeyId;
  }

  public String getCurrKeyValue() {
    return currKeyValue;
  }

  public void setCurrKeyValue(String argCurrKeyValue) {
    currKeyValue = argCurrKeyValue;
  }

}
