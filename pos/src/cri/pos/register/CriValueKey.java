//$Id: CriValueKey.java 1101 2016-09-29 18:47:38Z olr.jgaughn $
package cri.pos.register;

/**
 * Customer value key container class.<br>
 * <br>
 * Copyright (c) 2016 MICROS Retail
 *
 * @author johgaug
 * @created Sep 29, 2016
 * @version $Revision: 1101 $
 */
public final class CriValueKey {

  /** Accesses a flag determining whether to prompt for a customer. */
  public static final ValueKey<Boolean> PROMPT_CUSTOMER =
      new ValueKey<Boolean>(Boolean.class, "CRI_PROMPT_CUSTOMER");

  /** Prevent instantiation: this class is used as a namespace only. */
  private CriValueKey() {
    throw new UnsupportedOperationException();
  }

}
