//$Id: CriAddItemAdditionalPropertiesForPosLogOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.register;

import org.apache.commons.lang.StringUtils;

import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.cmd.ITransactionCmd;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pricing2.*;
import dtv.pricing2.DealRefreshStrategy.PricingAction;
import dtv.xst.dao.trl.*;
import dtv.xst.pricing.XSTPricingDescriptor;

/**
 * Writes new LineItem properties to be added to the PosLog.xml<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author dnedumpurath
 * @created Dec 10, 2012
 * @version $Revision: 1105 $
 */
public class CriAddItemAdditionalPropertiesForPosLogOp
    extends Operation {

  /**
   * 
   */
  private static final long serialVersionUID = -5123772407214229463L;

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    addPromoTypeToPosLog(argCmd);

    addStyleStatusToPosLog(argCmd);

    return HELPER.completeResponse();
  }

  private void addStyleStatusToPosLog(IXstCommand argCmd) {
    ITransactionCmd cmd = (ITransactionCmd) argCmd;
    for (IRetailTransactionLineItem lineItem : cmd.getTransaction().getSaleLineItems()) {
      if (lineItem.getVoid()) {
        continue;
      }

      ISaleReturnLineItem saleLine = (ISaleReturnLineItem) lineItem;
      if (!saleLine.getVoid()) {
        lineItem.setStringProperty("CRI_STYLE_STATUS", getCriStyleStatusValue(saleLine));
      }
    }
  }

  private String getCriStyleStatusValue(ISaleReturnLineItem saleLine) {
    String stockStatus = saleLine.getItem().getStockStatus();
    if ((stockStatus == null) || StringUtils.equalsIgnoreCase(stockStatus, "ORIG")) {
      return "A";
    }
    else {
      return "C";
    }
  }

  private void addPromoTypeToPosLog(IXstCommand argCmd) {
    ITransactionCmd cmd = (ITransactionCmd) argCmd;
    for (IRetailTransactionLineItem lineItem : cmd.getTransaction().getSaleLineItems()) {

      if (lineItem.getVoid()) {
        continue;
      }

      ISaleReturnLineItem saleLine = (ISaleReturnLineItem) lineItem;

      if (!saleLine.getVoid()) {
        for (IRetailPriceModifier priceMod : saleLine.getRetailPriceModifiers()) {
          if (!priceMod.getVoid() && (priceMod.getRetailPriceModifierReasonCode() != null)) {
            String reasonCode = priceMod.getRetailPriceModifierReasonCode();
            String dealId = priceMod.getDealId();

            if (reasonCode.equalsIgnoreCase("DEAL")) {
              lineItem.setStringProperty("CRI_PROMO_TYPE", getCriPromoTypeValue(argCmd, dealId));
            }
          }
        }
      }
    }

  }

  private String getCriPromoTypeValue(IXstCommand argCmd, String dealId) {
    if (isPercentOffDeal(argCmd, dealId)) {
      return PricingAction.PERCENT_OFF.toString();
    }
    else {
      return PricingAction.CURRENCY_OFF.toString();
    }
  }

  private boolean isPercentOffDeal(IXstCommand argCmd, String dealId) {
    ITransactionCmd cmd = (ITransactionCmd) argCmd;

    boolean percentOff = false;

    for (PricingDeal<XSTPricingDescriptor> deal : cmd.getTransaction().getCurrentPossibleDeals()) {
      if (deal.getNativeDeal().getDealId().equals(dealId)) {
        if (isPercentOffDealAction(deal)) {
          percentOff = true;
        }
      }
    }
    return percentOff;
  }

  private boolean isPercentOffDealAction(PricingDeal<XSTPricingDescriptor> deal) {
    boolean percentOff = false;

    for (DealAction da : deal.getDealAction()) {
      if (da instanceof ActionPercentOff) {
        percentOff = true;
      }
    }

    return percentOff;
  }
}
