//$Id: CriPromptReturnPriceHistoryOp.java 777 2014-03-27 02:15:29Z suz.jliu $
package cri.pos.register.returns;

import static dtv.pos.register.returns.ReturnType.BLIND;

import java.math.BigDecimal;

import cri.pos.pricing.CriPricingHelper;

import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.register.ISaleItemCmd;
import dtv.pos.register.returns.PromptReturnPriceHistoryOp;
import dtv.xst.dao.itm.IItem;
import dtv.xst.dao.trl.ISaleReturnLineItem;

/**
 * If current line is blind return line item, return with lowest sale price direct.<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 * 
 * @author David.Hu
 * @created Mar 14, 2014
 * @version $Revision: 777 $
 */
public class CriPromptReturnPriceHistoryOp
    extends PromptReturnPriceHistoryOp {

  private static final long serialVersionUID = 1L;

  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    ISaleItemCmd cmd = (ISaleItemCmd) argCmd;
    ISaleReturnLineItem lineItem = cmd.getLineItem();
    if (BLIND.matches(lineItem.getReturnTypeCode())) {
      cmd.setNewPrice(getLowestPrice(argCmd, argEvent));
      return HELPER.completeResponse();
    }
    return super.handleOpExec(argCmd, argEvent);
  }

  protected BigDecimal getLowestPrice(IXstCommand argCmd, IXstEvent argEvent) {
    ISaleItemCmd cmd = (ISaleItemCmd) argCmd;
    IItem item = cmd.getLineItem().getItem();

    return CriPricingHelper.getInstance().getLowestPrice(item);
  }
}
