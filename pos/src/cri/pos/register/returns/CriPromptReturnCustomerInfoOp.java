//$Id: CriPromptReturnCustomerInfoOp.java 1101 2016-09-29 18:47:38Z olr.jgaughn $
package cri.pos.register.returns;

import cri.pos.register.CriValueKey;

import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.register.IRetailTransactionCmd;
import dtv.pos.tender.PromptCustAsscRequiredForTenderOp;
import dtv.util.NumberUtils;
import dtv.xst.dao.trl.IRetailTransaction;

/**
 * Prompt for customer association only on returns. This op is designed to be used during tendering,
 * not during return initialization when base customer association takes place.<br>
 * <br>
 * Copyright (c) 2016 MICROS Retail
 *
 * @author johgaug
 * @created Sep 23, 2016
 * @version $Revision: 1101 $
 */
public class CriPromptReturnCustomerInfoOp
    extends PromptCustAsscRequiredForTenderOp {

  private static final long serialVersionUID = 1L;

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    IRetailTransaction trans = ((IRetailTransactionCmd) argCmd).getRetailTransaction();

    boolean applicable = false;
    if (trans != null) {
      boolean hasCustomer = (trans.getCustomerParty() != null);
      boolean refundDue = NumberUtils.isNegative(trans.getTotal());
      Boolean flag = CriValueKey.PROMPT_CUSTOMER.get(argCmd);
      boolean prompt = ((flag == null) || flag.booleanValue());

      applicable = !hasCustomer && refundDue && prompt;
    }

    return applicable;
  }

}
