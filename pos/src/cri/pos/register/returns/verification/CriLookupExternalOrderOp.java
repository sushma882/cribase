//$Id: CriLookupExternalOrderOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.register.returns.verification;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.register.returns.verification.LookupExternalOrderOp;

/**
 * CRI version of Operation responsible for looking up a web order from an external system and
 * placing it on the Xstore command. This was originally implemented for PTS 290753 Cross-Channel
 * Return support with the CwSerenade system. <br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author dnedumpurath
 * @created Oct 29, 2012
 * @version $Revision: 1105 $
 */
public class CriLookupExternalOrderOp
    extends LookupExternalOrderOp {

  /**
   * 
   */
  private static final long serialVersionUID = 1341787417200508994L;
  private static final Logger _logger = Logger.getLogger(CriLookupExternalOrderOp.class);

  /**
   * @param argCmd
   */
  @Override
  protected IOpResponse lookpupExternalOrder(IXstCommand argCmd) {
    String orderId = getSearchModel(argCmd).getExternalOrderId();
    //if the barcode scanned does cotain "-", then add it.
    //"-" will be added before the last 3 digits.
    if (!StringUtils.containsIgnoreCase(orderId, "-")) {
      String webOrderId = orderId.substring(0, (orderId.length() - 3));
      webOrderId = webOrderId.concat("-");
      String shipToId = orderId.substring(orderId.length() - 3);
      orderId = webOrderId.concat(shipToId);
      getSearchModel(argCmd).setExternalOrderId(orderId);
    }
    _logger.info("Cross-channel Id: [" + orderId + "]");
    return super.lookpupExternalOrder(argCmd);
  }
}
