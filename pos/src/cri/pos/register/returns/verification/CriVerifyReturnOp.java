//$Id: CriVerifyReturnOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.register.returns.verification;

import static cri.pos.common.CriCommonHelper.isWestVirginiaTaxExempt;

import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.register.ISaleItemCmd;
import dtv.pos.register.returns.verification.VerifyReturnOp;
import dtv.xst.dao.trl.IRetailTransaction;

/**
 * If original sale transaction has West Virginia Tax Exempt then return transaction should remove
 * this tax exempt.<br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 * 
 * @author Bob.Shi
 * @created May 21, 2014
 * @version $Revision: 1105 $
 */
public class CriVerifyReturnOp
    extends VerifyReturnOp {

  private static final long serialVersionUID = 5384387268768647225L;

  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    IOpResponse response = super.handleOpExec(argCmd, argEvent);
    ISaleItemCmd cmd = (ISaleItemCmd) argCmd;
    IRetailTransaction trans = cmd.getRetailTransaction();
    if ((trans != null) && isWestVirginiaTaxExempt(trans.getTaxExemption())) {
      trans.setTaxExemption(null);
    }
    return response;
  }
}
