//$Id$
package cri.pos.register.itemmessages;

import cri.pos.common.CriConstants;

import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.register.infomessage.InfoMessage;
import dtv.util.config.IConfigObject;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2016 MICROS Retail
 *
 * @author aayco
 * @created Mar 17, 2016
 * @version $Revision$
 */
public class CriDisplayInstructionsOp
    extends Operation {

  private static final long serialVersionUID = 1L;
  private String storeOffline_ = "";
  private String storeOnline_ = "";

  /**
   * Display a register instruction message.
   * 
   * @param argCmd the current command
   * @param argEvent the event that occurred
   * @return {@inheritDoc}
   */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    if (isStoreOffline(argCmd)) {
      return HELPER.getShowMessageResponse(InfoMessage.makeRegisterInstructionMessage(storeOffline_));
    }
    else if ("default".equalsIgnoreCase(storeOnline_)) {
      return HELPER.getClearAndShowMessageResponse(InfoMessage.getDefaultMessage());
    }
    else {
      return HELPER.getShowMessageResponse(InfoMessage.makeRegisterInstructionMessage(storeOnline_));
    }
  }

  @Override
  public void setParameter(String argName, IConfigObject argValue) {
    if ("STORE_OFFLINE".equalsIgnoreCase(argName)) {
      storeOffline_ = argValue.toString();
    }
    if ("STORE_ONLINE".equalsIgnoreCase(argName)) {
      storeOnline_ = argValue.toString();
    }
    else {
      super.setParameter(argName, argValue);
    }
  }

  private boolean isStoreOffline(IXstCommand argCmd) {
    return CriConstants.CRI_STOREDOWN_ENTRYMODE.equals(argCmd.getValue(CriConstants.CRI_STOREDOWN_ENTRYMODE));
  }
}
