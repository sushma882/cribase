//$Id: $
package cri.pos.register.tax;

import java.math.BigDecimal;
import java.util.List;

import dtv.docbuilding.IDocElementFactory;
import dtv.docbuilding.types.DocBuilderAlignmentType;
import dtv.i18n.formatter.output.IOutputFormatter;
import dtv.pos.register.tax.TaxPercentageBuilderField;
import dtv.pos.register.type.LineItemType;
import dtv.util.NumberUtils;
import dtv.xst.dao.trl.*;

/**
 * Return appropriate tax rate string for printing on the receipt. This is the max percent used in
 * transaction.
 * 
 * <br>
 * Copyright (c) 2014 MICROS Retail
 *
 * @author pgolli
 * @created Mar 14, 2014
 * @version $Revision$
 */
public class CriTaxPercentageBuilderField
    extends TaxPercentageBuilderField {

  public CriTaxPercentageBuilderField(String argContents, String argStyle, Integer argLocation,
      DocBuilderAlignmentType argAlignment, int argPriority, IOutputFormatter argFormatter) {

    super(argContents, argStyle, argLocation, argAlignment, argPriority, argFormatter);
  }

  /**
   * Return appropriate tax rate string for printing on the receipt.<br>
   * <br>
   * Specifically, returns the total tax percentage of an item which has max percetage applied <br>
   * For Example:
   * 
   * <pre>
   * item1 total tax percetage 7.75%
   * item2 total tax percetage 8.0%
   * 
   * ----------------------------------------------------
   * Return value is "8%"
   * </pre>
   *
   * @param argSource the object currently being worked on by the doc builder
   * @param argFactory not currently used here
   * @return Appropriate tax rate string for printing on the receipt.
   */

  @Override
  public String getContents(Object argSource, IDocElementFactory argFactory) {

    BigDecimal maxRate = NumberUtils.ZERO;
    if (argSource instanceof IRetailTransaction) {
      IRetailTransaction trans = (IRetailTransaction) argSource;

      List<ISaleReturnLineItem> saleLineItems =
          trans.getLineItemsByTypeCode(LineItemType.ITEM.name(), ISaleReturnLineItem.class);

      if ((saleLineItems != null) && !saleLineItems.isEmpty()) {

        for (ISaleReturnLineItem saleItem : saleLineItems) {
          BigDecimal taxRate = BigDecimal.ZERO;
          if (!saleItem.getVoid()) {
            List<ISaleTaxModifier> taxMods = saleItem.getTaxModifiers();
            if ((taxMods != null) && !taxMods.isEmpty()) {

              for (ISaleTaxModifier taxModifier : taxMods) {
                if (!taxModifier.getVoid()) {
                  taxRate = taxRate.add(NumberUtils.nonNull(taxModifier.getTaxPercentage()));
                }
              }
            }

            if (NumberUtils.isGreaterThan(taxRate, maxRate)) {
              maxRate = taxRate;
            }

          }
        }
      }
    }

    //return maxRate.toString(); /*+ "%";*/
    if (!NumberUtils.isZeroOrNull(maxRate)) {
      return getFormatter().format(maxRate);
    }

    return null;
  }

}
