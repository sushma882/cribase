//$Id: CriPromptTaxExemptReasonOp.java 1106 2017-01-11 20:43:32Z olr.jgaughn $
package cri.pos.register.tax;

import static cri.pos.common.CriConfigurationMgr.enableClothingVoucherTender;
import static cri.pos.common.CriConstants.WEST_VIRGINIA_TAX_EXEMPT_REASON_CODE;
import static dtv.pos.register.tax.TaxConstants.TAX_EXEMPT;

import java.util.*;

import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.register.tax.PromptTaxExemptReasonOp;
import dtv.xst.dao.com.CodeLocator;
import dtv.xst.dao.com.IReasonCode;

/**
 * According the setting in the SystemConfig.xml to display the "West Virginia Tax Exempt" or not.
 * <br>
 * <br>
 * Copyright (c) 2014 MICROS Retail
 *
 * @author Bob.Shi
 * @created May 19, 2014
 * @version $Revision: 1106 $
 */
public class CriPromptTaxExemptReasonOp
    extends PromptTaxExemptReasonOp {

  private static final long serialVersionUID = 1L;

  @Override
  protected Object[] getPromptList(IXstCommand argCmd, IXstEvent argEvent) {
    Object[] taxExempts = super.getPromptList(argCmd, argEvent);
    if (!enableClothingVoucherTender()) {
      IReasonCode reasonCode = CodeLocator.getReasonCode(TAX_EXEMPT, WEST_VIRGINIA_TAX_EXEMPT_REASON_CODE);
      List<?> list = new ArrayList<Object>(Arrays.asList(taxExempts));
      if (list.contains(reasonCode)) {
        list.remove(reasonCode);
        taxExempts = list.toArray();
      }
    }
    return taxExempts;
  }
}
