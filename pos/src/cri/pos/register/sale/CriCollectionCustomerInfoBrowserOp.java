// $Id: CriCollectionCustomerInfoBrowserOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.register.sale;

import dtv.pos.browser.BrowserOp;
import dtv.pos.common.FormKey;
import dtv.pos.framework.ui.UIController;
import dtv.pos.iframework.form.IFormKey;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.ui.UIServices;

/**
 * Open browser to access to collection customer info. <br>
 * <br>
 * Copyright (c) 2013 MICROS Retail
 *
 * @author shy xie
 * @created Mar 8, 2013
 * @version $Revision: 1105 $
 */
public class CriCollectionCustomerInfoBrowserOp
    extends BrowserOp {

  /**
   * 
   */
  private static final long serialVersionUID = -7850164874228010936L;

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handleExit(IXstCommand argCmd) {
    UIServices.invoke(new Runnable() {
      /** {@inheritDoc} */
      @Override
      public void run() {
        UIController.getInstance().getFrame().getFocusComponent().requestFocus();
        UIController.getInstance().getFrame().getFocusComponent().transferFocus();
      }
    }, true);

    return HELPER.completeResponse();
  }

  /** {@inheritDoc} */
  @Override
  protected IFormKey getFormKey(IXstCommand argCmd) {
    return FormKey.valueOf("CRI.COLLECTION_CUST_INFO_BROWSER");
  }
}
