//$Id: PromptCollectCustomerInfoUrlOp.java 612 2013-03-06 14:13:02Z suz.sxie $
package cri.pos.register.sale;

import dtv.pos.common.OpChainKey;
import dtv.pos.common.PromptKey;
import dtv.pos.framework.action.type.XstDataActionKey;
import dtv.pos.framework.op.XstCommand;
import dtv.pos.framework.ui.op.AbstractPromptOp;
import dtv.pos.iframework.action.IXstAction;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.iframework.ui.config.IPromptKey;
import dtv.util.config.IConfigObject;

/**
 * Prompt to ask collection customer info.<br>
 * <br>
 * Copyright (c) 2013 MICROS Retail
 *
 * @author shy xie
 * @created Mar 6, 2013
 * @version $Revision: 612 $
 */
public class PromptCollectCustomerInfoUrlOp
    extends AbstractPromptOp {

  private static final long serialVersionUID = 1L;
  private String url_;
  private String forceClose_;

  @Override
  protected IPromptKey getPromptKey(IXstCommand argCmd) {
    return PromptKey.valueOf("CRI.COLLECT_CUSTOMER_INFO_URL");
  }

  /**
   * 
   * {@inheritDoc}
   */
  @Override
  protected IOpResponse handlePromptResponse(IXstCommand argCmd, IXstEvent argEvent) {
    if ((argEvent instanceof IXstAction)
        && ((IXstAction) argEvent).getActionKey().equals(XstDataActionKey.YES)) {
      ((XstCommand) argCmd).setProperty("url", url_);
      ((XstCommand) argCmd).setProperty("forceClose", forceClose_);

      return HELPER.getCompleteStackChainResponse(OpChainKey.valueOf("CRI.COLLECTION_CUSTOMER_INFO"), argCmd,
          argEvent);
    }
    else {
      return HELPER.completeResponse();
    }
  }

  /** {@inheritDoc} */
  @Override
  public void setParameter(String argName, IConfigObject argValue) {
    if ("url".equalsIgnoreCase(argName)) {
      url_ = argValue.toString();
    }
    else if ("forceClose".equalsIgnoreCase(argName)) {
      forceClose_ = argValue.toString();
    }
    else {
      super.setParameter(argName, argValue);
    }
  }
}
