//$Id: BackOfficeMenuOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.appmanagement.op;

import dtv.pos.framework.StationModelMgr;
import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.iframework.type.*;
import dtv.pos.iframework.ui.IMenuItem;

/**
 * Remember the last selected back office menu.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author Bob Shi
 * @created Sep 4, 2015
 * @version $Revision: 1105 $
 */
public class BackOfficeMenuOp
    extends Operation {

  /**
   * 
   */
  private static final long serialVersionUID = -5171477054645132015L;
  public static final IAttributeKey<IMenuItem> BACKOFFICE_MENU =
      new AttributeKey<IMenuItem>("BACKOFFICE_MENU", ScopeType.GLOBAL, IMenuItem.class);

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    IMenuItem menuItem = StationModelMgr.getInstance().getStationModel().getMenuModel().getCurrentMenu();

    StationModelMgr.getInstance().getStationModel().setAttribute(BACKOFFICE_MENU, menuItem);
    return HELPER.completeResponse();
  }
}
