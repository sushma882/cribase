//$Id: RetrieveBackOfficeMenuOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.pos.appmanagement.op;

import dtv.pos.appmanagement.ApplicationData;
import dtv.pos.appmanagement.op.ValidateBiDirectionalLoginCmd;
import dtv.pos.framework.StationModelMgr;
import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.iframework.ui.IMenuItem;

/**
 * Restore the last selected back office menu.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author Bob Shi
 * @created Sep 4, 2015
 * @version $Revision: 1105 $
 */
public class RetrieveBackOfficeMenuOp
    extends Operation {

  /**
   * 
   */
  private static final long serialVersionUID = -8092262225606223757L;

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {

    if (argCmd instanceof ValidateBiDirectionalLoginCmd) {
      ValidateBiDirectionalLoginCmd changeAppCmd = (ValidateBiDirectionalLoginCmd) argCmd;
      ApplicationData newAppData = changeAppCmd.getNewAppData();
      if ((newAppData != null) && "REGISTER".equals(newAppData.getKey())) {
        return true;
      }
    }

    return false;
  }

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    IMenuItem menuItem =
        StationModelMgr.getInstance().getStationModel().getAttribute(BackOfficeMenuOp.BACKOFFICE_MENU);

    if (menuItem != null) {
      StationModelMgr.getInstance().getStationModel().getMenuModel().setCurrentMenu(menuItem);
    }
    return HELPER.completeResponse();
  }
}
