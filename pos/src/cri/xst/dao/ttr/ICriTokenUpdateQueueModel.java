// $Id: ICriTokenUpdateQueueModel.java 902 2015-07-30 19:26:41Z suz.sxie $
package cri.xst.dao.ttr;

/**
 * DTX extension interface.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author shy xie
 * @created June 30, 2015
 * @version $Revision: 902 $
 */
public interface ICriTokenUpdateQueueModel {

  /**
   * Get the related credit tender line item.
   * 
   * @return
   */
  public dtv.xst.dao.ttr.ICreditDebitTenderLineItem getCreditDebitTenderLineItem();
}
