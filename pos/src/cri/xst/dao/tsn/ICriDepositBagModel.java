// $Id: ICriDepositBagModel.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package cri.xst.dao.tsn;

/**
 * DTX extension interface.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author micros
 * @created Jul 16, 2012
 * @version $Revision: 1105 $
 */
public interface ICriDepositBagModel {

  /**
   * Get the bag lost reason code object.
   * 
   * @return
   */
  public dtv.xst.dao.com.IReasonCode getLostReasonCodeObject();

  /**
   * Add the counted details to the deposit bag model.
   *
   * @param argCriDepositBagDetailItem
   */
  public void addCriDepositBagDetailItem(cri.xst.dao.tsn.ICriDepositBagDetailItem argCriDepositBagDetailItem);
}
