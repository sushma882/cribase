// $Id: ReplicationQueueAccessor.java 27590 2011-11-22 15:32:48Z dtvdomain\rsable $
package dtv.data2.replication.dtximpl;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dtv.data2.AbstractPersistenceDefaults;
import dtv.data2.access.exception.FailoverException;
import dtv.data2.access.impl.jdbc.*;
import dtv.data2.replication.ReplicationException;
import dtv.data2.replication.dtximpl.config.DtxReplicationConfigHelper;
import dtv.data2.replication.dtximpl.dispatcher.IDtxReplicationDispatcher.DispatchResult;
import dtv.util.StringUtils;

/**
 * Manages access to the replication log (jdbc data source) The replication log contains pending
 * replication transactions. Anything that has been sucessfully replication should be queued for
 * deletion from the log.<br>
 * 
 * Additionally, if an object has already been sucessfully replicated, there is no need to add it to
 * the replication log.<br>
 * 
 * Copyright (c) 2005 Datavantage Corporation
 * 
 * @author mmichalek
 * @version $Revision: 27590 $
 * @created Oct 19, 2005
 */
public class ReplicationQueueAccessor {
  public static final int UNSPECIFIED = -1;

  protected static final int MAX_RETRIES;
  protected static final int RETRY_WAIT;
  protected static final int NO_FAILS_LIMIT;
  protected static final int ERROR_FAILURE_INTERVAL_LIMIT;
  protected static ReplicationQueueAccessor instance_;

  private static final Logger logger_ = Logger.getLogger(ReplicationQueueAccessor.class);
  private static final Logger auditLogger_ = Logger.getLogger(DtxReplicationStrategy.REPLICATION_AUDIT_LOG);
  private static final Logger adminLogger_ = Logger.getLogger("dtv.sysadmin.data.repqueue.readerror");
  private static final Logger repErrorLogger_ = Logger.getLogger("dtv.sysadmin.data.repqueue.errors");
  private static final Logger repNoFailLogger_ = Logger.getLogger("dtv.sysadmin.data.repqueue.nofails");

  private static final String SQL_INSERT =
      "insert into ctl_replication_queue (organization_id, rtl_loc_id, wkstn_id, db_trans_id, service_name, date_time, expires_after, expires_immediately_flag, never_expires_flag, offline_failures, error_failures, replication_data) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
  private static final String SQL_DELETE =
      "delete from ctl_replication_queue where organization_id = ? and rtl_loc_id = ? and wkstn_id = ? and db_trans_id = ? and service_name = ?";
  private static final String SQL_SELECT =
      "select organization_id, rtl_loc_id, wkstn_id, db_trans_id, service_name, date_time, expires_after, expires_immediately_flag, never_expires_flag, offline_failures, error_failures, replication_data from ctl_replication_queue where organization_id = ? and rtl_loc_id = ? and wkstn_id = ?";
  private static final String SQL_FAILURE_MIN = "(offline_failures + error_failures) >=";
  private static final String SQL_FAILURE_MAX = "(offline_failures + error_failures) <";
  private static final String SQL_FAILURE_EQ = "(offline_failures + error_failures) =";
  private static final String SQL_ORDER_BY = "order by date_time asc";

  private static final String SQL_UPDATE_OFFLINE_FAILURES =
      "update ctl_replication_queue set offline_failures = ? where organization_id = ? and rtl_loc_id = ? and wkstn_id = ? and db_trans_id = ? and service_name = ?";
  private static final String SQL_UPDATE_ERROR_FAILURES =
      "update ctl_replication_queue set error_failures = ? where organization_id = ? and rtl_loc_id = ? and wkstn_id = ? and db_trans_id = ? and service_name = ?";
  private static final String SQL_RESET_OFFLINE_FAILURES =
      "update ctl_replication_queue set offline_failures = 0 where organization_id = ? and rtl_loc_id = ? and wkstn_id = ? and offline_failures > 0";

  private static final String SQL_ERROR_FAILURE_COUNT =
      "select count(*) from ctl_replication_queue where error_failures > 0";
  private static final String SQL_NO_FAILURE_COUNT =
      "select count(*) from ctl_replication_queue where offline_failures = 0 and error_failures = 0";

  private static int currentErrorFailureCycles = 0;

  // Allow the class loader to maintain thread safety.
  static {
    MAX_RETRIES = Integer.getInteger("dtv.data2.replication.queue.maxretries", 4).intValue();
    RETRY_WAIT = Integer.getInteger("dtv.data2.replication.queue.retrywait", 3000).intValue();
    NO_FAILS_LIMIT = Integer.getInteger("dtv.data2.replication.queue.nofailslimit", 100).intValue();
    ERROR_FAILURE_INTERVAL_LIMIT =
        DtxReplicationConfigHelper.getReplicationQueueConfig().getErrorFailureInterval();

    String sysProp = System.getProperty("dtv.data2.replication.dtximpl.ReplicationQueueAccessor");

    if (!StringUtils.isEmpty(sysProp)) {
      try {
        instance_ = (ReplicationQueueAccessor) Class.forName(sysProp).newInstance();
      }
      catch (Exception ee) {
        logger_.error("exception while loading customer implementation of ReplicationQueueAccessor", ee);
      }
    }

    if (instance_ == null) {
      instance_ = new ReplicationQueueAccessor();
    }

  }

  /**
   * Obtain the singleton instance of the factory...
   * @return DtxReplicationServiceFactory
   */
  public static ReplicationQueueAccessor getInstance() {
    return instance_;
  }

  private final String myDataSource_;
  private final int maxRecordsPerCycle_;

  /**
   * Constructs a <code>ReplicationQueueAccessor</code>.
   */
  protected ReplicationQueueAccessor() {
    super();

    String myDataSource = "Local";
    try {
      myDataSource = DtxReplicationConfigHelper.getReplicationQueueConfig().getDataSource();
    }
    finally {
      myDataSource_ = myDataSource;
    }
    auditLogger_.info("Replication datasaource is: " + myDataSource_);

    int maxRecordsPerCycle = 50;
    try {
      maxRecordsPerCycle = DtxReplicationConfigHelper.getReplicationQueueConfig().getMaxRecsPerCycle();
    }
    finally {
      maxRecordsPerCycle_ = maxRecordsPerCycle;
    }
  }

  /**
   * Add the given replication transaction to the log. If an error occurs here, this is pretty bad,
   * we will THROW.
   * 
   * @param argTransaction The transaction to log to be replicated later.
   */
  public void addObject(ReplicationTransaction argTransaction) {
    final boolean SAVE_REQUIRED = true;

    try {
      boolean success = addObjectImpl(argTransaction, myDataSource_, null, SAVE_REQUIRED);
      if (!success) {
        String msg =
            "An unexpected error occurred while adding a transaction to the replication queue. No exception "
                + "thrown, just failure reported. XML SQL data:\n" + argTransaction.getPersistablesAsXml();
        logger_.error(msg, new Throwable("STACK TRACE"));
        throw new ReplicationException(msg);
      }
    }
    catch (Exception ee) {
      logger_.error("Serious replication queue error - could not save save entry to replication queue. "
          + argTransaction.getPersistablesAsXml(), ee);
      if (ee instanceof RuntimeException) {
        throw (RuntimeException) ee;
      }
      else {
        throw new ReplicationException("An error occurred while saving to the replication queue.", ee);
      }
    }
  }

  /**
   * Add the given replication transaction to the replication log at the specific location. This
   * method will report success or failure, and NOT throw an exception like the addObject
   * 
   * @param argTransaction The transaction to log to be replicated later.
   * @param argDataSource The data source to write this replication log to. (e.g. "Direct")
   * @param argDestinationService The value for the "service_name" field for this replicaiton log
   * entry.
   * 
   * @return true - this replication log entry was successfully written
   */
  public boolean addObjectRemote(ReplicationTransaction argTransaction, String argDataSource,
      String argDestinationService) {

    final boolean SAVE_NOT_REQUIRED = false;

    return addObjectImpl(argTransaction, argDataSource, argDestinationService, SAVE_NOT_REQUIRED);
  }

  // *******************************************************************
  //
  // INTERNAL
  //
  // *******************************************************************

  /**
   * This method just updates the given transaction's failure count.
   * 
   * @param argTransaction A transaction that should exist in the queue already.
   */
  public void existingTransactionFailed(DispatchResult argResult, ReplicationTransaction argTransaction) {
    auditLogger_.debug("BEGIN existingTransactionFailed");

    Connection dbConnection = getConnection();

    try {
      int index = 1;

      final String SQL;
      final int failureCount;

      if (DispatchResult.DISPATCH_OFFLINE_FAILURE == argResult) {
        SQL = SQL_UPDATE_OFFLINE_FAILURES;
        failureCount = argTransaction.getFailedOfflineAttempts();
      }
      else if (DispatchResult.DISPATCH_ERROR_FAILURE == argResult) {
        SQL = SQL_UPDATE_ERROR_FAILURES;
        failureCount = argTransaction.getFailedErrorAttempts();
      }
      else {
        throw new ReplicationException("Unkown DispatchResult: " + argResult);
      }

      PreparedStatement ps = dbConnection.prepareStatement(SQL);

      ps.setLong(index++ , failureCount);
      ps.setLong(index++ , argTransaction.getOrganizationId());
      ps.setLong(index++ , argTransaction.getRetailLocationId());
      ps.setLong(index++ , argTransaction.getWorkstationId());
      ps.setString(index++ , argTransaction.getTransactionId());
      ps.setString(index++ , argTransaction.getServiceName());
      auditLogger_.debug("BEGIN ps.execute() existingTransactionFailed");
      ps.execute();
      auditLogger_.debug("END ps.execute() existingTransactionFailed");
    }
    catch (Exception ee) {
      if (FailoverException.isFailover(ee)) {
        if (dbConnection instanceof DBConnection) {
          ((DBConnection) dbConnection).setCausedFailover(true);
        }
        logger_.warn("Failed to update a failure count in replication queue for " + "db trans id: "
            + argTransaction.getTransactionId() + " because of failover: " + ee);
      }
      else {
        String msg = "An unexpected error occured while updating an entry from the "
            + "replication log. db_trans_id = " + argTransaction.getTransactionId() + " service_name = "
            + argTransaction.getServiceName();
        logger_.warn(msg, ee);
      }
    }
    finally {
      try {
        dbConnection.close();
      }
      catch (Exception ee) {
        logger_.error("An error occured while closing connection.", ee);
      }
    }

    auditLogger_.debug("END existingTransactionFailed");
  }

  /**
   * Get transactions that need to be replicated.
   * 
   * @param argMinFailureLevel Only pull records with failures >= to this value. Specify -1 to
   * ignore the minimum
   * @param argMaxFailureLevel Only pull transactions whose failure count <= this value.
   * 
   * @return a list of ReplicationTransactions that need to be replicated.
   */
  public List<ReplicationTransaction> getReplicationTransactions(int argMinFailureLevel,
      int argMaxFailureLevel) {

    if (auditLogger_.isDebugEnabled()) {
      auditLogger_.debug(
          "BEGIN getReplicationTransactions min: " + argMinFailureLevel + "max: " + argMaxFailureLevel);
    }

    // ---------------------------------------------------------------
    // : Error checking.
    // ---------------------------------------------------------------

    if ((argMaxFailureLevel != UNSPECIFIED) && (argMinFailureLevel != UNSPECIFIED)
        && (argMaxFailureLevel < argMinFailureLevel)) {
      throw new ReplicationException("Invalid min & max " + "provided to getReplicationTransactions.  "
          + "max < min. Min = " + argMinFailureLevel + " Max = " + argMaxFailureLevel);
    }

    if ((argMinFailureLevel < UNSPECIFIED) || (argMaxFailureLevel < UNSPECIFIED)) {
      throw new ReplicationException(
          "Invalid min & max " + "provided to getReplicationTransactions.  " + "min or max is < "
              + UNSPECIFIED + ". " + "Min = " + argMinFailureLevel + " Max = " + argMaxFailureLevel);
    }

    Connection dbConnection = getConnection();

    List<ReplicationTransaction> resultObjects = null;

    try {

      // ---------------------------------------------------------------
      // : Build a sql statement based on the min & max failure counts
      // : desired.
      // ---------------------------------------------------------------

      StringBuilder sql = new StringBuilder(192);
      sql.append(SQL_SELECT); // start with the base select

      if (argMinFailureLevel == argMaxFailureLevel) {
        if (argMaxFailureLevel != UNSPECIFIED) {
          sql.append(" and ");
          sql.append(SQL_FAILURE_EQ);
          sql.append(" ");
          sql.append(argMinFailureLevel);
        }
      }
      else {
        if (argMinFailureLevel > UNSPECIFIED) {
          sql.append(" and ");
          sql.append(SQL_FAILURE_MIN);
          sql.append(" ");
          sql.append(argMinFailureLevel);
        }
        if (argMaxFailureLevel > UNSPECIFIED) {
          sql.append(" and ");
          sql.append(SQL_FAILURE_MAX);
          sql.append(" ");
          sql.append(argMaxFailureLevel);
        }
      }

      sql.append(" ");
      sql.append(SQL_ORDER_BY);

      PreparedStatement ps = dbConnection.prepareStatement(sql.toString());

      ps.setMaxRows(maxRecordsPerCycle_);
      ps.setFetchSize(maxRecordsPerCycle_);

      int index = 1;

      ps.setLong(index++ , getOrganizationId());
      ps.setInt(index++ , getRetailLocationId());
      ps.setLong(index++ , getWorkstationId());
      auditLogger_.debug("BEGIN ps.executeQuery() getReplicationTransactions");
      ResultSet results = ps.executeQuery();
      auditLogger_.debug("END ps.executeQuery() getReplicationTransactions");

      if (results != null) {
        while (results.next()) {

          if (resultObjects == null) {
            resultObjects = new ArrayList<ReplicationTransaction>(maxRecordsPerCycle_);
          }

          if (auditLogger_.isDebugEnabled()) {
            auditLogger_.debug("BEGIN Process result " + (resultObjects.size() + 1));
          }

          index = 1;

          ReplicationTransaction trans = new ReplicationTransaction();

          trans.setOrganizationId(results.getLong(index++ ));
          trans.setRetailLocationId(results.getInt(index++ ));
          trans.setWorkstationId(results.getLong(index++ ));
          trans.setTransactionId(results.getString(index++ ));
          trans.setServiceName(results.getString(index++ ));
          trans.setCreatedTime(results.getLong(index++ ));

          long expAfters = results.getLong(index++ );
          if (expAfters != 0) {
            trans.setExpiresAfter(expAfters);
          }

          int expiresImmediately = results.getInt(index++ );
          if (expiresImmediately != 0) {
            trans.setExpiresImmediately(true);
          }

          int neverExpires = results.getInt(index++ );
          if (neverExpires != 0) {
            trans.setNeverExpires(true);
          }

          trans.setFailedOfflineAttempts(results.getInt(index++ ));
          trans.setFailedErrorAttempts(results.getInt(index++ ));
          trans.setDatasAsXml(JDBCHelper.clobToString(results, index++ ));

          trans.setNewTransaction(false);

          if (auditLogger_.isDebugEnabled()) {
            auditLogger_.debug("END Process result " + (resultObjects.size() + 1));
          }

          resultObjects.add(trans);
        }
      }

      if ( ++currentErrorFailureCycles == ERROR_FAILURE_INTERVAL_LIMIT) {
        // Check for records in the queue that have error failures
        // If so, create an event log entry
        PreparedStatement ps2 = dbConnection.prepareStatement(SQL_ERROR_FAILURE_COUNT);
        ResultSet results2 = ps2.executeQuery();
        int errorFails_ = 0;

        if (results2 != null) {
          while (results2.next()) {
            errorFails_ = results2.getInt(1);
          }
          if (errorFails_ > 0) {
            repErrorLogger_.error("REPLICATION ENTRIES WITH ERROR FAILURES: " + errorFails_);
          }
        }
      }

      // Check for records in the queue that have no failures
      // If greater than the no fails limit, create an event log entry
      PreparedStatement ps3 = dbConnection.prepareStatement(SQL_NO_FAILURE_COUNT);
      ResultSet results3 = ps3.executeQuery();
      int noFails_ = 0;

      if (results3 != null) {
        while (results3.next()) {
          noFails_ = results3.getInt(1);
        }
        if (noFails_ > NO_FAILS_LIMIT) {
          repNoFailLogger_.warn("REPLICATION ENTRIES WITH NO FAILURES: " + noFails_);
        }
      }

    }
    catch (Exception ee) {
      if (FailoverException.isFailover(ee)) {
        logger_.warn("A failover exception occured while querying the replication queue: " + ee.toString());
        if (dbConnection instanceof DBConnection) {
          ((DBConnection) dbConnection).setCausedFailover(true);
        }
      }
      else {
        logger_.error("An unexpected error occured while querying the replication queue.", ee);
        adminLogger_.error("ERROR OCCURRED WHILE QUERYING REP QUEUE", ee);
      }
    }
    finally {
      try {
        dbConnection.close();
      }
      catch (Exception ee) {
        logger_.error("An error occured while closing connection.", ee);
      }
    }

    String resultObjectCount = (resultObjects == null) ? "null" : String.valueOf(resultObjects.size());

    if (auditLogger_.isDebugEnabled()) {
      auditLogger_.debug("END getReplicationTransactions min: " + argMinFailureLevel + "max: "
          + argMaxFailureLevel + " Returning " + resultObjectCount + " replication entries");
    }

    return resultObjects;
  }

  /**
   * Remove the given transaction from the log. This implies that the transaction was replicated
   * successfully.
   * 
   * @param argTransaction Transaction to remove the replication log.
   */
  public void removeObject(ReplicationTransaction argTransaction) {
    if (auditLogger_.isDebugEnabled()) {
      auditLogger_.debug("BEGIN removeObject trans: " + argTransaction.getTransactionId());
    }

    Connection dbConnection = getConnection();
    int retries = 0;

    while (retries++ < MAX_RETRIES) {
      try {
        int index = 1;

        PreparedStatement ps = dbConnection.prepareStatement(SQL_DELETE);

        ps.setLong(index++ , argTransaction.getOrganizationId());
        ps.setLong(index++ , argTransaction.getRetailLocationId());
        ps.setLong(index++ , argTransaction.getWorkstationId());
        ps.setString(index++ , argTransaction.getTransactionId());
        ps.setString(index++ , argTransaction.getServiceName());

        if (auditLogger_.isDebugEnabled()) {
          auditLogger_.debug("BEGIN ps.execute() removeObject trans: " + argTransaction.getTransactionId());
        }

        ps.execute();

        if (auditLogger_.isDebugEnabled()) {
          auditLogger_.debug("END ps.execute() removeObject trans: " + argTransaction.getTransactionId());
        }

        break;
      }
      catch (Exception ee) {

        auditLogger_.debug("removeObject exception caught " + ee + " retries: " + retries);

        if (FailoverException.isFailover(ee)) {
          if (dbConnection instanceof DBConnection) {
            ((DBConnection) dbConnection).setCausedFailover(true);
          }
          try {
            Thread.sleep(RETRY_WAIT);
          }
          catch (Exception notImportant) {
            // ignored
          }
          if (retries == MAX_RETRIES) {
            logger_.warn("Failed to delete replication from queue because queue "
                + "datasource is offline. Transation id: " + argTransaction.getTransactionId()
                + " Exception: " + ee);
          }
        }
        else {
          String msg = "An unexpected error occured while removing an entry from the "
              + "replication queue. db_trans_id = " + argTransaction.getTransactionId() + " service_name = "
              + argTransaction.getServiceName();
          logger_.warn(msg, ee);
          break;
        }
      }
      finally {
        try {
          dbConnection.close();
        }
        catch (Exception ee) {
          logger_.error("An error occured while closing connection.", ee);
        }
      }
    }

    if (auditLogger_.isDebugEnabled()) {
      auditLogger_.debug("END removeObject trans: " + argTransaction.getTransactionId());
    }
  }

  /**
   * Resets offline failure counts for all q entries for this register. This is used to jump start
   * replication when a datasource has been offline for a while and has now come back on line.
   */
  public void resetOfflineFailureCounts() {
    auditLogger_.debug("BEGIN resetOfflineFailureCounts");
    Connection dbConnection = getConnection();

    try {
      int index = 1;
      PreparedStatement ps = dbConnection.prepareStatement(SQL_RESET_OFFLINE_FAILURES);
      ps.setLong(index++ , getOrganizationId());
      ps.setLong(index++ , getRetailLocationId());
      ps.setLong(index++ , getWorkstationId());
      auditLogger_.debug("BEGIN ps.execute() resetOfflineFailureCounts");
      ps.execute();
      auditLogger_.debug("END ps.execute() resetOfflineFailureCounts");
    }
    catch (Exception ee) {
      if (FailoverException.isFailover(ee)) {
        if (dbConnection instanceof DBConnection) {
          ((DBConnection) dbConnection).setCausedFailover(true);
        }
        logger_.warn("Failed to reset failures counts in replication queue " + " because of failover: " + ee);
      }
      else {
        String msg = "An unexpected error occurred while reseting failure counts.";
        logger_.warn(msg, ee);
      }
    }
    finally {
      try {
        dbConnection.close();
      }
      catch (Exception ee) {
        logger_.error("An error occured while closing connection.", ee);
      }
    }

    auditLogger_.debug("END resetOfflineFailureCounts");
  }

  // *******************************************************************
  //
  // Singleton pattern below this point.
  //
  // *******************************************************************

  protected boolean addObjectImpl(ReplicationTransaction argTransaction, String argDataSource,
      String destinationService, boolean argRequiredSave) {

    if (auditLogger_.isDebugEnabled()) {
      auditLogger_.debug("BEGIN addObjectImpl for trans: " + argTransaction.getTransactionId());
    }

    boolean done = false;
    int retries = 0;

    while (!done) {

      Connection dbConnection = getConnection();

      try {
        int index = 1;

        PreparedStatement ps = dbConnection.prepareStatement(SQL_INSERT);

        ps.setLong(index++ , argTransaction.getOrganizationId());
        ps.setLong(index++ , argTransaction.getRetailLocationId());
        ps.setLong(index++ , argTransaction.getWorkstationId());
        ps.setString(index++ , argTransaction.getTransactionId());

        if (destinationService != null) {
          ps.setString(index++ , destinationService);
        }
        else {
          ps.setString(index++ , argTransaction.getServiceName());
        }

        ps.setLong(index++ , argTransaction.getCreatedTime());

        if (!argTransaction.getExpiresImmediately() && !argTransaction.getNeverExpires()
            && (argTransaction.getExpiresAfter() > 0)) {
          ps.setString(index++ , String.valueOf(argTransaction.getExpiresAfter()));
        }
        else {
          ps.setNull(index++ , Types.INTEGER);
        }

        if (argTransaction.getExpiresImmediately()) {
          ps.setInt(index++ , 1);
        }
        else {
          ps.setInt(index++ , 0);
        }

        if (argTransaction.getNeverExpires()) {
          ps.setInt(index++ , 1);
        }
        else {
          ps.setInt(index++ , 0);
        }

        ps.setInt(index++ , argTransaction.getFailedOfflineAttempts());
        ps.setInt(index++ , argTransaction.getFailedErrorAttempts());

        ps.setString(index++ , argTransaction.getPersistablesAsXml());

        auditLogger_.debug("BEGIN addObjectImpl ps.execute() for INSERT");
        ps.execute();
        auditLogger_.debug("END addObjectImpl ps.execute() for INSERT - returning true");
        return true;
      }
      catch (Exception ee) { // this is pretty bad.
        done = false;

        auditLogger_.debug("Exception caught during addObjectImpl " + ee);

        if (!FailoverException.isFailover(ee) || (retries >= MAX_RETRIES)) {
          if (argRequiredSave) {
            String msg = "An unexpected error occured while saving to the "
                + "replication queue. Replication data may have been lost.";
            logger_.fatal(msg, ee);
            throw new ReplicationException(msg, ee);
          }
          else {
            String msg = "Failed to save data to replication queue.";
            throw new ReplicationException(msg, ee);
          }
        }
        else {
          ++retries;
          if (dbConnection instanceof DBConnection) {
            ((DBConnection) dbConnection).setCausedFailover(true);
          }
          if (logger_.isDebugEnabled()) {
            logger_.debug("Failover detected on replication queue datasource: [" + argDataSource
                + "], will retry (retry count: + " + retries + " MAX_RETRIES: " + MAX_RETRIES, ee);
          }

          auditLogger_
              .debug("addObjectImpl will retry.  going to sleep " + RETRY_WAIT + " retries: " + retries);

          try {
            Thread.sleep(RETRY_WAIT);
          }
          catch (Exception notImportant) {
            // ignored
          }
        }
      }
      finally {
        try {
          dbConnection.close();
        }
        catch (Exception ee) {
          logger_.error("An error occured while closing connection.", ee);
        }
      }
    }

    auditLogger_.warn("addObjectImpl could not save, preparing to throw.");
    throw new ReplicationException(
        "Invalid state in ReplicationQueueAccessor - this code should NOT be hit - "
            + "Replication queue entry NOT saved. Service:  " + argTransaction.getServiceName() + " Data: "
            + argTransaction.getPersistablesAsXml());
  }

  protected Connection getConnection() {
    Connection dbConnection = null;

    int retries = 0;

    while ((dbConnection == null) && (retries++ < MAX_RETRIES)) {
      try {
        dbConnection = JDBCDataSourceMgr.getInstance().getConnection(myDataSource_);
      }
      catch (Exception ee) {
        if (FailoverException.isFailover(ee)) {
          logger_.warn("A failover occurred while obtaining replication queue datasource. [" + myDataSource_
              + "] " + ee);
          try {
            Thread.sleep(RETRY_WAIT);
          }
          catch (InterruptedException ex) {
            // ignored
          }
        }
        else {
          String msg =
              "An exception occurred while obtaining the datasource " + "used for the replication queue: ["
                  + myDataSource_ + "]. " + " This datasource MUST be enabled, available, and use "
                  + "JDBCPersistenceStrategy for replication to function.";

          logger_.error(msg, ee);

          throw new ReplicationException(msg, ee);
        }
      }
    }

    if (dbConnection == null) {
      throw new ReplicationException(
          "Unable to obtain datasource for " + "replication queue [" + myDataSource_ + "]");
    }

    return dbConnection;
  }

  protected long getOrganizationId() {
    return AbstractPersistenceDefaults.getInstance().getOrganizationId();
  }

  protected int getRetailLocationId() {
    return AbstractPersistenceDefaults.getInstance().getRetailLocationId();
  }

  protected long getWorkstationId() {
    return AbstractPersistenceDefaults.getInstance().getWorkstationId();
  }
}
