// $Id: DebitCardRefundRule.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package dtv.pos.tender;

import org.apache.log4j.Logger;

import dtv.hardware.events.CreditCardEvent;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.validation.*;
import dtv.pos.util.validation.AbstractSecuredValidationRule;

/**
 * Checks if the Original Debit card used for the purchase is a combo card.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Feb 13, 2012
 * @version $Revision: 1105 $
 */
public class DebitCardRefundRule
    extends AbstractSecuredValidationRule {

  private static final Logger logger_ = Logger.getLogger(DebitCardRefundRule.class);
  private static final boolean _debugLogging = logger_.isDebugEnabled();

  /** {@inheritDoc} */
  @Override
  public IValidationResult validate(IValidationData argParamIValidationData) {

    final IXstEvent event = (IXstEvent) argParamIValidationData.getObjectArray()[0];

    if (!(event instanceof CreditCardEvent)) {
      // probably no PINpad working on this system...was unable to match, so is probably DEBIT-only
      return SimpleValidationResult.getFailed("_criDebitRefundMessage");
    }

    CreditCardEvent evt = (CreditCardEvent) event;
    if (TenderConstants.DEBIT_CARD.equals(evt.getAccountType())) {
      // is definitely DEBIT-only
      return SimpleValidationResult.getFailed("_criDebitRefundMessage");
    }

    // was mapped to some other type of credit card, so it definitely not DEBIT-only
    return IValidationResult.SUCCESS;
  }
}
