//$Id: ValidateDebitTenderOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package dtv.pos.tender.debitcard;

import dtv.pos.framework.ui.op.AbstractPromptOp;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.tender.ISaleTenderCmd;
import dtv.pos.tender.TenderConstants;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author aabdul
 * @created Oct 4, 2012
 * @version $Revision: 1105 $
 */
public class ValidateDebitTenderOp
    extends AbstractPromptOp {

  private static final long serialVersionUID = 1L;

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handlePromptResponse(IXstCommand argCmd, IXstEvent argEvent) {
    return HELPER.silentErrorResponse();
  }

  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {

    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    ITenderLineItem tndrLineItem = cmd.getTenderLineItem();
    boolean result =
        ((tndrLineItem != null) && (TenderConstants.DEBIT_CARD.equalsIgnoreCase(tndrLineItem.getTenderId())));

    return result;
  }
}
