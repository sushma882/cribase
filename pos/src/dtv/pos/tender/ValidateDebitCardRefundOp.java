// $Id: ValidateDebitCardRefundOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package dtv.pos.tender;

import org.apache.log4j.Logger;

import dtv.hardware.events.CreditCardEvent;
import dtv.hardware.inputrules.EventDiscriminator;
import dtv.hardware.msr.MsrSwipe;
import dtv.pos.common.AbstractValidationWithoutPromptOp;
import dtv.pos.common.ValidationKey;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.iframework.validation.*;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * Check if the current tenderLineItem is a DEBIT_CARD. If so validate if it is a combo card and
 * refund using the credit card request<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Feb 13, 2012
 * @version $Revision: 1105 $
 */
public class ValidateDebitCardRefundOp
    extends AbstractValidationWithoutPromptOp {

  private static final long serialVersionUID = 1L;
  private static final Logger logger_ = Logger.getLogger(ValidateDebitCardRefundOp.class);
  private static final boolean _debugLogging = logger_.isDebugEnabled();

  private final IValidationKey[] VALIDATIONS =
      new IValidationKey[] {ValidationKey.valueOf("CRI.DEBITCARD_RESTRICTED_REFUND")};
  private IXstEvent event;

  /** {@inheritDoc} */
  @Override
  public IValidationData getValidationData(IXstCommand argCmd, IXstEvent argEvent, IValidationKey argKey) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    ICreditDebitTenderLineItem lineItm = (ICreditDebitTenderLineItem) cmd.getTenderLineItem();

    final MsrSwipe swipe =
        MsrSwipe.makeFromTrack2(lineItm.getAccountNumber() + "=" + lineItm.getExpirationDateString());
    event = EventDiscriminator.getInstance().translateEvent(swipe);

    return new ValidationData(new Object[] {event});
  }

  /** {@inheritDoc} */
  @Override
  public IValidationKey[] getValidationKeys(IXstCommand argCmd) {
    return VALIDATIONS;
  }

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    ITenderLineItem tndrLineItem = cmd.getTenderLineItem();
    boolean result =
        ((tndrLineItem != null) && (TenderConstants.DEBIT_CARD.equalsIgnoreCase(tndrLineItem.getTenderId())));

    return (result && super.isOperationApplicable(argCmd));
  }

  /** Removing the tender line from the transaction if its not valid tender (DEBIT_ONLY) */
  @Override
  protected IOpResponse handleAfterErrorMsgPrompt(IXstCommand argCmd, IXstEvent argEvent) {

    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    cmd.getTransaction().removeRetailTransactionLineItem(cmd.getTenderLineItem());
    return super.handleAfterErrorMsgPrompt(argCmd, argEvent);
  }

  @Override
  protected void handleValid(IXstCommand argCmd, IXstEvent argEvent) {

    ISaleTenderCmd cmd = (ISaleTenderCmd) argCmd;
    ICreditDebitTenderLineItem argLineItm = (ICreditDebitTenderLineItem) cmd.getTenderLineItem();
    CreditCardEvent evt = (CreditCardEvent) event;
    String argTenderId = TenderHelper.getInstance().getNonDebitTenderId(evt);

    TenderHelper.getInstance().setTenderIdType(argLineItm, argTenderId);
  }
}
