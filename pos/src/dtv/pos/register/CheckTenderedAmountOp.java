//$Id: CheckTenderedAmountOp.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package dtv.pos.register;

import static dtv.util.NumberUtils.ZERO;
import static dtv.util.NumberUtils.isNegative;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.*;

import org.apache.log4j.Logger;

import dtv.data2.access.config.RetailTransactionMgrConfigHelper;
import dtv.pos.framework.op.Operation;
import dtv.pos.iframework.cmd.ITransactionCmd;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.pricing.AbstractCalculator;
import dtv.pos.tender.SaleTenderCmd;
import dtv.xst.dao.trl.IRetailTransaction;
import dtv.xst.dao.trl.IRetailTransactionLineItem;
import dtv.xst.dao.trn.IPosTransaction;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * Checking current tendered amount in the transaction as a safe guard if the calculation methods
 * are run in hardware thread which will not update the tender totals correctly. Re-running the
 * calculators again<br>
 * <br>
 * Copyright (c) 2013 MICROS Retail
 * 
 * @author aabdul
 * @created Apr 9, 2013
 * @version $Revision: 1105 $
 */
public class CheckTenderedAmountOp
    extends Operation {

  private static final long serialVersionUID = 1L;
  static final Logger logger_ = Logger.getLogger(CheckTenderedAmountOp.class);
  static final boolean DEBUG = logger_.isDebugEnabled();

  private List<AbstractCalculator> calculators_ = null;
  private final boolean bInitialized = false;

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    if (argCmd instanceof ITransactionCmd) {
      ITransactionCmd cmd = (ITransactionCmd) argCmd;
      final IPosTransaction trans = cmd.getTransaction();
      BigDecimal tenderAmt = trans.getAmountTendered();
      BigDecimal totalTenders = ZERO;
      for (IRetailTransactionLineItem tnd : trans.getTenderLineItems()) {
        if (tnd instanceof ITenderLineItem) {
          if (!tnd.getVoid()) {
            ITenderLineItem tndrLine = (ITenderLineItem) tnd;
            BigDecimal amtTender = tndrLine.getAmount();
            totalTenders = totalTenders.add(amtTender);
          }
        }
      }
      if (isNegative(tenderAmt.subtract(totalTenders))) {
        return true;
      }
    }
    return false;

  }

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    init();
    if (argCmd instanceof SaleTenderCmd) {
      runCalculators(((SaleTenderCmd) argCmd).getRetailTransaction());
    }
    return HELPER.completeResponse();
  }

  private void runCalculators(IRetailTransaction argTrans) {
    if (argTrans != null) {
      long startTime = DEBUG ? System.currentTimeMillis() : 0;

      // Get the line items from the extracted transaction.
      IRetailTransactionLineItem[] lines = argTrans.getRetailTransactionLineItems()
          .toArray(new IRetailTransactionLineItem[argTrans.getRetailTransactionLineItems().size()]);

      // Now make the calculators do their stuff!
      for (AbstractCalculator calc : calculators_) {
        /* Eat exceptions thrown by any individual calculator so that
         * subsequent calculators will still run. */
        try {
          calc.handleLineItemEvent(lines);
        }
        catch (Exception ex) {
          logger_.error("CAUGHT EXCEPTION for transaction with ID [" + argTrans.getObjectIdAsString() + "]",
              ex);
        }
      }

      if (DEBUG) {
        long endTime = System.currentTimeMillis();
        logger_.debug(".--==< CALCULATORS!!! >==--. Time to run: " + (endTime - startTime) + " ms.");
      }
    }
  }

  protected void init() {
    if (!bInitialized) {

      RetailTransactionMgrConfigHelper config = new RetailTransactionMgrConfigHelper();
      config.initialize();

      for (Class<?> initializationTarget : config.getInitializationTargets()) {
        try {
          Method method = initializationTarget.getMethod("initialize", new Class[0]);
          method.invoke(initializationTarget, new Object[0]);
        }
        catch (Exception e) {
          try {
            Method method = initializationTarget.getMethod("init", new Class[0]);
            method.invoke(initializationTarget, new Object[0]);
          }
          catch (Exception e2) {
            logger_.warn("Error occured initializing " + initializationTarget, e2);
          }
        }
      }
      // We recieve the calculator templates in order already.
      // Then make the list immutable and assign it.
      List<AbstractCalculator> calcs = new ArrayList<AbstractCalculator>();

      for (Class<?> listenerClass : config.getInitialListeners()) {
        try {
          AbstractCalculator tempHandler = (AbstractCalculator) listenerClass.newInstance();
          calcs.add(tempHandler);
        }
        catch (Exception ex) {
          logger_.warn("CAUGHT EXCEPTION", ex);
        }
      }
      calculators_ = Collections.unmodifiableList(calcs);
    }
  }
}
