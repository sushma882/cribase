//$Id: WaitForm.java 1105 2017-01-11 20:41:13Z olr.jgaughn $
package dtv.hardware.hypercom.forms;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Aug 3, 2012
 * @version $Revision: 1105 $
 */
public class WaitForm
    extends AbstractDeviceForm {

  /**
   * Constructs a <code>WaitForm</code>.
   * @param argFormId
   */
  public WaitForm() {
    super("FNWAITFRM");
  }
}
