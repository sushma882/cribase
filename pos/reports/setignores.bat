@echo off
:: $Id: setignores.bat 4 2012-03-05 19:05:43Z dtvdomain\mschreiber $
:: $URL: https://node1000.gspann.local:8443/svn/20181115-cri/branches/fipay_cust_name/pos/reports/setignores.bat $
setlocal
setlocal enableextensions
IF ERRORLEVEL 1 echo Unable to enable extensions
pushd %~dp0
set _MY_TEMP=%temp%\~%~n0%RANDOM%.tmp
dir /ad /b /s | find /V ".svn">%_MY_TEMP%
FOR /f %%i IN ('type %_MY_TEMP%') DO (
  svn ps svn:ignore -F ignores.txt %%i
)
del %_MY_TEMP%
pause
