@echo off
:: $Id: setignores.bat 4 2012-03-05 19:05:43Z dtvdomain\mschreiber $
:: $URL: https://node1000.gspann.local:8443/svn/20181115-cri/branches/fipay_cust_name/pos/setignores.bat $
setlocal
setlocal enableextensions
IF ERRORLEVEL 1 echo Unable to enable extensions
pushd %~dp0

svn ps svn:ignore -F ignores.txt .

echo *>~all-ignores.tmp
svn ps svn:ignore -F ~all-ignores.tmp gen
svn ps svn:ignore -F ~all-ignores.tmp log
del ~all-ignores.tmp

call reports\setignores.bat
