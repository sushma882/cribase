@echo off
:: $Id: setignores.bat 7556 2014-01-21 20:50:20Z dtvdomain\dberkland $
:: $URL: https://dtvsource/svn/cst_swp/branches/upgrade65_1/projects/setignores.bat $

setlocal
setlocal enableextensions
IF ERRORLEVEL 1 echo Unable to enable extensions
pushd %~dp0

svn ps svn:ignore -F ignores.txt .
