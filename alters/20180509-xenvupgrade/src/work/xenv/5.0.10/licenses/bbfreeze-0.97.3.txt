[Copied from http://pypi.python.org/pypi/bbfreeze/]


bbfreeze contains a modified copy of modulegraph, which is distributed under the MIT license and is copyrighted by Bob Ippolito.

bbfreeze contains a modified copy of getpath.c from the python distribution, which is distributed under the python software foundation license version 2 and copyrighted by the python software foundation.

bbfreeze includes a module 'bdist_bbfreeze.py' which is

Copyright 2008-2012 by Hartmut Goebel <h.goebel@goebel-consult.de>
The 'bdist_bbfreeze' module may be distributed under the same licence as bbfreeze itself.

The remaining part is distributed under the zlib/libpng license:

Copyright (c) 2007-2012 brainbot technologies AG

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
This notice may not be removed or altered from any source distribution.
