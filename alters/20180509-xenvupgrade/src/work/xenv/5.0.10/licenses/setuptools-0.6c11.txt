No license file was made available with the setuptools distribution.


According to http://pypi.python.org/pypi/setuptools (retrieved on 2012-05-18),
it is distributed under the PSF or ZPL licenses, which are available at the
following URLs:

PSF: http://docs.python.org/license.html
ZPL: http://foundation.zope.org/agreements/ZPL_2.1.pdf
 

