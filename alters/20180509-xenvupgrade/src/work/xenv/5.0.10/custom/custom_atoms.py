""" Custom atom module. """

""" The following line is required """
#from dtv.env.atoms import Atom, SUCCESS, WARNING, ERROR


""" An example atom """
 
#import time
#class noop_delay_custom(Atom):
#    def runAction(self):
#        delay = int(self.args[0])
#        if delay <> 1:
#            word = "seconds"
#        else:
#            word = "second"
#        self.log.info("Waiting %d %s." % (delay, word))
#        time.sleep(delay)
#        return SUCCESS, [None]
