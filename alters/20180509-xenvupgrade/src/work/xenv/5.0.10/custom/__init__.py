"""environment package"""
# $Id: __init__.py 1067 2008-10-20 18:38:11Z dtvdomain\dandrzejewski $
__all__ = ['custom_atoms', 'closehook']
__version__ = "1.0.0"
