""" Custom close hook script. """


#import logging
#import string
#import subprocess
#import sys
#import win32process
#
#
#WARNING = 20
#SUCCESS = 99
#ERROR = 50
#
#
#def predbback2():
#    
#    command_to_execute = "dir c:/environment /s"
#    cwd = "c:/"
#    
#    
#    log = logging.getLogger("task")
#    
#    if sys.platform == "win32":
#        startInfo = win32process.STARTUPINFO()
#        startInfo.dwFlags = win32process.STARTF_USESHOWWINDOW
#        startInfo.wShowWindow = False
#        close_fds = False
#    else:
#        startInfo = None
#        close_fds = False
#
#    update_log = logging.getLogger("update")
#
#    log.debug("Command: %s" % command_to_execute)
#
#    the_subprocess = subprocess.Popen(
#                         args = command_to_execute,
#                         stdout = subprocess.PIPE,
#                         stderr = subprocess.STDOUT,
#                         close_fds = close_fds,
#                         cwd = cwd,
#                         startupinfo = None
#                         )
#
#    while the_subprocess.poll() is None:
#        
#        line = the_subprocess.stdout.readline()
#        line = string.rstrip(line[:-1])
#
#        if line is not None:
#            update_log.info("[OUTPUT] %s" % line)
#
#
#    return_code = the_subprocess.poll()
#
#    log.info("Return Code: %s" % return_code)
#    
#
#    return True
