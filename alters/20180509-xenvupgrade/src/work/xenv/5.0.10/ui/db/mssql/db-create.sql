-------------------------------------------------------------------------------------------------------------------
--                                                                                                              
-- Script           : db-create.sql                                                                    
-- Description      : Creates the Tablespaces, Roles, Profiles and Users for the XTools application.               
-- Author           : Todd Senauskas
-- DB platform:     : MS SQL Server
-- Version          : 2.5                                                                                       
-------------------------------------------------------------------------------------------------------------------
--                            CHANGE HISTORY                                                                    
-------------------------------------------------------------------------------------------------------------------
-- WHO DATE      DESCRIPTION                                                                                    
-------------------------------------------------------------------------------------------------------------------
-- ... .....     Initial Version
-------------------------------------------------------------------------------------------------------------------
print '************************************************************************';
print '* Assign actual database for USE statement before running this script! *';
print '************************************************************************';
use [xstore]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ---------------------------------------------------------
-- If the Schema xtool does not exists, create it
-- ---------------------------------------------------------
IF NOT EXISTS (SELECT * 
				FROM INFORMATION_SCHEMA.SCHEMATA 
				WHERE SCHEMA_NAME = 'xtool')
	Exec('CREATE SCHEMA [xtool]');
GO

-- ---------------------------------------------------------
-- If the role xenv_app does not exists, create it
-- ---------------------------------------------------------
IF DATABASE_PRINCIPAL_ID('xtool_app') IS NULL 
	Exec('CREATE ROLE xtool_app');
GO
	  
-- ---------------------------------------------------------
-- Allows the role xtool_app to view the Meta Data in the
-- current database.
-- ---------------------------------------------------------
GRANT VIEW DEFINITION TO xtool_app;	  

-- ---------------------------------------------------------
-- If the need login does not exists, create it
-- ---------------------------------------------------------
IF (SUSER_ID('xtoolusers')) IS NULL 
	CREATE LOGIN [xtoolusers] WITH PASSWORD=N'password', 
		        DEFAULT_DATABASE=[xstore], 
                DEFAULT_LANGUAGE=[us_english], 
                CHECK_EXPIRATION=OFF, 
                CHECK_POLICY=OFF
GO

--
-- If the need user ID does not exists in the current database, create it
--
IF  (user_id('xtoolusers')) IS NULL
	CREATE USER [xtoolusers] 
		FOR LOGIN [xtoolusers] 
		WITH DEFAULT_SCHEMA=[dbo]
GO

EXEC sp_addrolemember N'xtool_app', N'xtoolusers'
GO

--
-- Gives the user access to view the system catalog views
--
use master;
GO

GRANT VIEW SERVER STATE to xtoolusers;
GO
