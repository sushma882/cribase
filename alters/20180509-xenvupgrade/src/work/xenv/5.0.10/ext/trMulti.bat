:: ************************************************
:: trMulti.bat
:: 	Launches and configures the shell environment
::  for the Tender Retail Multi.exe program.
::
::	TMS Created 2005/05/27
:: ************************************************

echo (((((((((((((((((( trMulti LOG - Start ))))))))))))))))) >> ..\log\trMulti.log
date /t >> ..\log\trMulti.log
time /t >> ..\log\trMulti.log
echo. >> ..\log\trMulti.log

set TenderRetail=\merchantconnectmulti\
set path=%path%;c:%TenderRetail%

echo c:%TenderRetail%multi.exe >> ..\log\trMulti.log
echo ))))))))))))))))))) trMulti LOG - Complete (((((((((((((((( >> ..\log\trMulti.log
echo. >> ..\log\trMulti.log

cd %TenderRetail%
c:%TenderRetail%multi.exe

